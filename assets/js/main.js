require.config({
	baseUrl: '/assets/js',
	paths: {
		bootstrap: 'bootstrap',
		fileupload:'bootstrap-fileupload',
		fileuploadui:'jquery-fileupload-ui',
		blueimp:'jquery.fileupload',
		transport:'jquery.iframe-transport',
		product_manager:'product_manager',
		scrollTo:'jquery.scrollTo-min'
	}
});

define(
	'main',
	[
		'jquery',
        'bootstrap',
        'fileupload',
        'product_manager',
        'transport',
        'scrollTo',
        'blueimp',
        'plugins'
	],
	function($){
		var thisEl = this;

		//jquery and bootstrap have been loaded
		// $('.carousel').carousel();

		var makeUploader = function(el){
			$(el).fileupload({
				uploadtype: 'file',
				name: 'upload[]'
			});

			$(el).fileuploader({
				dataType:'json',
				start:function(e) {
					$('.fileupload-exists', this).show();
					$('.fileupload-new', this).removeAttr('style');

					$('#product_preview .progress').show();
					$('.upload-alert').addClass('hide');
					$('.upload-info').addClass('hide');
				},
				submit:function(e) {
					return true;
				},
				done:function(e, data){
					if(data.result.success) {
						var file = data.result.file;
						var img = $('<img />', {
							src:file,
							load:function(){
								$('.preview-text').remove();

								$('.preview', this).attr('id', 'preview_' + data.result.fileuploadid);

								var _height = $('#product_size').find(':selected').attr('data-h');
								var _width = $('#product_size').find(':selected').attr('data-w');

								$('#product_preview')
									.css('background', 'transparent url("' + file + '") no-repeat center center')
									.css('height', _height)
									.css('width', _width);

								$('#product_preview .progress').hide();
								$('#product_preview .progress .bar').css('width', '0%');

								$('.continue-order').removeClass('disabled');
							}
						});

						$(this).attr('id', 'fileupload_' + data.result.fileuploadid);

						//check to see if another select file is already there
						var _select = $('.fileupload-new', '.new-select').last().css('display');
						if(_select != 'inline'){
							//get the next file to show
							var newDiv = $(this).clone();
							$(newDiv).addClass('new-select');
							$(newDiv).removeAttr('id');

							$('.fileupload-exists', newDiv).hide();
							$('.fileupload-new', newDiv).show();
							$('.fileupload-preview', newDiv).html('');

							makeUploader(newDiv);

							$(this).after(newDiv);
						}
					} else if(data.result.error){
						var message = data.result.error;
						if(data.result.upload_success === 1){
							message += " Your image DID upload successfully.";
						}

						$('.upload-alert')
							.html(data.result.error)
							.removeClass('hide');
					}
				},
				progressall: function (e, data) {
					$('#product_preview .progress').show();

			        var progress = parseInt(data.loaded / data.total * 100, 10);
			        $('#product_preview .progress .bar').css(
			            'width',
			            progress + '%'
			        );
			    }
			});
		};

		makeUploader('.fileupload');

		$('.fileupload-exists', '.uploaded').show();
		$('.fileupload-new', '.uploaded').hide();

		$('form[name="uploadForm"]').on('click', '.fileupload .preview', function(){
			var parent = $(this).closest('.fileupload');
			var fileid = parent.attr('id').replace('fileupload_','');
			var puid = $('input[name="product_unique"]').val();
			var size = $('input[name="preview_size"]').val();

			var _src = '/uploads/preview/' + puid + '/' + fileid + '/' + size;

			$('#product_preview .progress .bar').css('width', '80%');
			$('#product_preview .progress').show();

			$('<img />', {
				src:_src,
				load:function(){

					$('.preview-text').remove();

					var _height = $('#product_size').find(':selected').attr('data-h');
					var _width = $('#product_size').find(':selected').attr('data-w');

					$('#product_preview')
						.css('background', 'transparent url("' + _src + '") no-repeat center center')
						.css('height', _height)
						.css('width', _width);

					$('#product_preview .progress').hide();
					$('#product_preview .progress .bar').css('width', '0%');
				}
			});
		});

		$('form[name="uploadForm"]').on('click', '.close.fileupload-exists', function(){
			var fileuploadid = $(this).closest('.fileupload').attr('id').replace('fileupload_', '');
			var puid = $('input[name="product_unique"]').val();

			$.ajax({
				url:'/uploads/remove',
				data:{product_unique:puid,fileupload:fileuploadid},
				dataType:'json',
				type:'post',
				success:function(data){
					$('.upload-info').html(data.message).removeClass('hide');
				}
			})
		});

		var preview_img = $('.preview-image');

		if(preview_img.length){
			//show progress
			$('#product_preview .progress .bar').css('width', '80%');
			$('#product_preview .progress').show();

			//get img src from attr
			var _src = $(preview_img).attr('data-src');

			if(_src == ""){
				_src = "/uploads/nopreview/" + $('#product_size').val();
			}

			$(preview_img).remove();

			$('<img />',{
				src:_src,
				load:function(){
					$('.preview-text').remove();

					var _height = $('#product_size').find(':selected').attr('data-h');
					var _width = $('#product_size').find(':selected').attr('data-w');

					$('#product_preview')
						.css('background', 'transparent url("' + _src + '") no-repeat center center')
						.css('height', _height)
						.css('width', _width);

					$('#product_preview .progress').hide();
					$('#product_preview .progress .bar').css('width', '0%');

					//$('#product_preview').append(this);
				}
			});
		}

		$('#product_size').change(function(){
			var _size = this.value;
			var _product_unique = $('input[name="product_unique"]').val();
			var _src = '/uploads/nopreview/' + _size;

			$('<img />', {
				src:_src,
				load:function(){
					$('.preview-text').remove();

					var _height = $('#product_size').find(':selected').attr('data-h');
					var _width = $('#product_size').find(':selected').attr('data-w');

					$('#product_preview')
						.css('background', 'transparent url("' + _src + '") no-repeat center center')
						.css('height', _height)
						.css('width', _width);

					$('#product_preview .progress').hide();
					$('#product_preview .progress .bar').css('width', '0%');
				}
			});

			$('input[name="preview_size"]').val($(this).val());
		});

		$('input[name="preview_size"]').val($('#product_size').val());

		$('.continue-order').click(function(e){
			e.preventDefault();

			if($(this).hasClass('disabled')){
				return false;
			}

			var _required = "";

			$('.required').each(function(){
				if($.trim($(this).val()) == ""){
					var _field = $(this).attr('name');
					_field = _field
						.split('_')
						.join(' ')
						.toUpperCase();

					_required += (_required == "" ? "" : ",") + _field;
				}
			});

			if(_required != ""){
				$('.quote-alert')
					.html('The ' + _required + ' field(s) is required.')
					.removeClass('hide');
				return false;
			}

			/*if(!$('.quote-alert').hasClass('hide')){
				alert('Please resolve all errors before continuing');
				return false;
			}*/

			$('form[name="detailsForm"]').submit();
		});

		/*$('.next-step').click(function(e){
			e.preventDefault();

			if($(this).hasClass('disabled')){
				return false;
			}

			var _current = window.location.hash;

			if(!_current){
				_current = '#product-group1';
			}

			if($(this).html() == 'Next'){
				var _next = $(_current.replace('product-','')).next('.attribute-group');
			} else {
				var _next = $(_current.replace('product-','')).prev('.attribute-group');
			}

			if(_next.length){
				$('#details_container').scrollTo(
					_next,
					{
						duration:500,
						onAfter:function(){
							window.location.hash = 'product-' + $(_next).attr('id');
							thisEl.updateButtons();
						}
					}
				);
			}

			return false;
		});*/

		$('input[name="no_upload"]').change(function(){
			if($(this).prop('checked')){
				$('.continue-order').removeClass('disabled');
			}
		});

		this.updateButtons = function(){
			var _hash = window.location.hash;

			if(!_hash){
				_hash = '#product-group1';
			}

			if(_hash != '#product-group5'){
				$('#product_next').removeClass('disabled');
				$('#add_to_cart').addClass('disabled');
			} else {
				$('#product_next').addClass('disabled');
				$('#product_prev').removeClass('disabled');
				$('#add_to_cart').removeClass('disabled');
			}

			if(_hash != '#product-group1'){
				$('#product_prev').removeClass('disabled');
			} else {
				$('#product_prev').addClass('disabled');
			}

			$('#details_container').scrollTo(
				$(_hash.replace('product-','')),
				{
					duration:500,
					onAfter:function(){
						window.location.hash = _hash;
					}
				}
			);
		}

		//this needs to move but for now check for existence
		/*if($('#details_container').length){
			this.updateButtons();
		}*/

		var product = $('input[name="product"]');

		if(product.length){
			var productId = $(product).val();

			var product_manager = $.ProductManager('/ajax/get_product_data', {
				productId:productId
			});
		}

		$('.nav-tabs a:first').tab('show');

		$('a[data-toggle="tooltip"]').tooltip();

		$('select[name="paymentType"]').change(function(e){
			e.preventDefault();

			if($(this).val() == '1'){
				$('#cc-payment').show(300);
			} else {
				$('#cc-payment').hide();
			}

			if($(this).val() == '3'){
				$('#paypal-payment').show(300);
			} else {
				$('#paypal-payment').hide();
			}
		});

		var shippingOptions = $('select[name="shippingOptions"]');

		if($(shippingOptions).length){
			$('select[name="shippingOptions"]').change(function(e){
				var cVal = parseFloat($('.shippingtotal').html());

				if($(this).val() == ""){
					var val = 0;
				} else {
					var val = $('option:selected', this).html();

					val = val.split('-');
					val = val[1];
					val = $.trim(val.replace('$', ''));
				}

				if(val == 'FREE!'){
					val = 0;
				}

				val = parseFloat(val);

				$('.shippingtotal').html(val.toFixed(2));

				var total = parseFloat($('.totalPrice').html());
				total -= cVal;

				total += val;

				$('.totalPrice').html(total.toFixed(2));
			});

			$(shippingOptions).trigger('change');
		}

		var paymentType = $('select[name="paymentType"]');

		if($(paymentType).length){
			$(paymentType).trigger('change');
		}

		$('.product-attribute label.radio.inline').click(function(e){
			e.preventDefault();

			$(this).siblings('label').find('img').removeClass('img-polaroid');
			$(this).siblings('label').find('input').prop('checked', false);
			$(this).find('input').prop('checked', true);
			$(this).find('img').addClass('img-polaroid');
			$(this).find('input').trigger('change');
		});

		$('input[type="radio"]:checked').siblings('img').addClass('img-polaroid');

		$('#show_additional').click(function(e){
			e.preventDefault();

			$('.attribute-group.additional').toggle();
		});

		var rush = $('input[name="rushorder"]');
		if($(rush).length){
			$(rush).click(function(e){
				var rushPrice = parseFloat($('input[name="rushPrice"]').val());
				var totalPrice = parseFloat($('.totalPrice').html());

				if($('input[name="rushorder"]:checked').length > 0){
					$('.rushtotal').html(rushPrice.toFixed(2));
					$('#rush_row').show(100);

					//increase the total price
					totalPrice +=  rushPrice;
				} else {
					totalPrice -= rushPrice;
					$('#rush_row').hide(100);
				}

				$('.totalPrice').html(totalPrice.toFixed(2));
			});

			if($(rush).prop('checked') == true){
				var totalPrice = parseFloat($('.totalPrice').html());
				var rushPrice = parseFloat($('input[name="rushPrice"]').val());

				totalPrice += rushPrice;

				$('.totalPrice').html(totalPrice.toFixed(2));

				$('.rushtotal').html(rushPrice.toFixed(2));
				$('#rush_row').show(100);
			}
		}

		var proof = $('input[name="hardcopyproof"]');
		if($(proof).length){
			$(proof).click(function(){
				var totalPrice = parseFloat($('.totalPrice').html());

				if($('input[name="hardcopyproof"]:checked').length > 0){
					totalPrice += 15;
				} else {
					totalPrice -= 15;
				}

				$('.totalPrice').html(totalPrice.toFixed(2));
			});

			if($(proof).prop('checked') == true){
				var totalPrice = parseFloat($('.totalPrice').html());
				totalPrice += 15;

				$('.totalPrice').html(totalPrice.toFixed(2));
			}
		}

		/*$('input', '.inkcolorpages').keyup(function(e){
			var numPages = $('.numSheets').val();

			if(numPages){
				var val = $(this).val();

				if(val > numPages){
					$('.quote-alert')
						.html('Your number of color/b&w pages can not exceed your total number of pages.')
						.removeClass('hide');
					return false;
				}
				var difference = numPages - val;

				var sibling = $(this).siblings('input');
				$(sibling).val(difference);
			}
		});*/

		$('.product-attribute .toggle').click(function(e){
			e.preventDefault();
			$(this).next('.controls').first().toggle();
		});

		return $;
	}
);