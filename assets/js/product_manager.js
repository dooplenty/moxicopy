(function($){
	$.extend({
		ProductManager:function(productUrl, options){
			var base = this;

			var defaults = {
				productId: null,
				attributesClass: 'product-attribute',
				product:null,
				orderContainerId:'#order',
				quantityEl:'quantity',
				priceDisplay:'.price',
				cart:null,
				uniqueEl:'product_unique',
				priceEl:'subtotal'
			};

			var options = $.extend({}, defaults, options);

			if(options.productId === null){
				console.log('No productID to manage fool!');
				return;
			}

			if(productUrl === null || typeof productUrl != 'string'){
				console.log('Seems that your url is missing. Where will I get my product data?');
				return;
			}

			base.init = function(options){
				$.ajax(productUrl, {
					type:'post',
					dataType:'json',
					data:{productId:options.productId},
					success:function(data){
						options.product = data;
						//update price in case we have a started session

						base.updatePrice();
					}
				});
			};

			base.init(options);

			//lets define our other methods.
			base.updatePrice = function(e){
				if(e){
					e.stopImmediatePropagation();
				}
				$('.quote-alert').addClass('hide');

				//current total price
				var price = 0;

				var category = options.product.ProductCategoryID;

				//we always need quantity
				var quantity = $('input[name="' + options.quantityEl + '"]').val();
				quantity = parseInt(quantity);

				var baseQuantity = quantity;

				//check to see if we're working with a multipage doc
				if($('.numSheets').length > 0){
					var numSheets = $('.numSheets').val();

					if($.trim(numSheets) == ""){
						$('.quote-alert')
							.html('Please select the number of pages in your document')
							.removeClass('hide');

						// $('.numSheets').focus();

						return false;
					}

					//if multipage-folded
					if(category == '2'){
						numSheets = numSheets / 4;
					}

					if(category == '3' && $('.doublesided').val() == 'double') {
						numSheets = Math.ceil(numSheets / 2);
					}

					quantity *= numSheets;
				}

				//calculate our base price from the product
				var i = 0;
				var priceCount = $(options.product.Pricing).length;

				do {
					price = quantity * (options.product.Pricing[i].UnitPrice);
					i++;
				} while(quantity >= (parseInt(options.product.Pricing[i].Quantity)) && i < (priceCount - 1));

				var basePrice = price;

				var paperMult = 1;
				var sizeMult = 1;
				var coverPaperMult = 1;

				//now loop through to set attribute pricing
				$(options.product.Attributes).each(function(i, attribute){
					//find our attr input
					var attrName = attribute.AttributeKey + '_' + attribute.ProductAttributeID;
					var attrEl = $('input[name="' + attrName + '"], select[name="' + attrName + '"]');

					var useQuantity = quantity;

					if(attribute.AttributeKey == 'papertypes'){
						var el = $('select[name="' + attribute.AttributeKey + '_' + attribute.ProductAttributeID + '"]');
						var val = $(el).val();

						$(options.product.PaperTypes).each(function(i, papertype){
							if(papertype.PaperTypeID == val){
								paperMult = papertype.Multiplier;
							}
						});

						return;
					}

					if(attribute.AttributeKey == 'sizes'){
						var size = $('select[name="' + attribute.AttributeKey + '_' + attribute.ProductAttributeID + '"]').val();

						$(options.product.Sizes).each(function(i, sizeObj){
							if(sizeObj.ProductSizeID == size){
								sizeMult = sizeObj.Multiplier;
								return;
							}
						});

						return;
					}

					if(attribute.AttributeKey == 'numpagesother'){
						base.validator(attribute.AttributeKey, $('.numSheets').val());
						return;
					}

					if(attribute.AttributeKey == 'holepunchingmulti'){

						if(numSheets > 0){
							useQuantity = quantity / numSheets;
						}
					}

					if(attribute.AttributeKey == 'inkcolorpages'){
						if(numSheets > 0){
							useQuantity = quantity * numSheets;

							var bwVal = $('input[name="numSheetsBW"]').val();
							var colorVal = $('input[name="numSheetsColor"]').val();

							if($.trim(bwVal) != ""){
								$(attribute.Pricing).each(function(i, pricing){
									//color is the base so no need to do anything there
									var pQuantity = $('input[name="' + options.quantityEl + '"]').val();
									price += bwVal * pQuantity * pricing.ValuePricePerUnit;
								});
							}
						} else {
							return;
						}
					}

					if(attribute.AttributeKey == 'bindings'){
						var val = $('input[name="' + attrName + '"]:checked').val();
						switch(val){
							case 'holepunched':
								$('.spiralcoveroptions').hide();
								$('.threeringoptions').show();
								useQuantity = baseQuantity;
								break;
							case 'gbc':
							case 'spiral':
							case 'spiralcalendar':
								$('.spiralcoveroptions').show();
								$('.threeringoptions').hide();
								useQuantity = baseQuantity;
								break;
							default:
								$('.spiralcoveroptions').hide();
								$('.threeringoptions').hide();
								break;

						}
					}

					if(attribute.AttributeKey == 'spiralcoveroptions'){
						useQuantity = baseQuantity;
					}

					if(attribute.AttributeKey == 'coverpapertypes'){
						var el = $('select[name="' + attribute.AttributeKey + '_' + attribute.ProductAttributeID + '"]');
						var val = $(el).val();

						$(options.product.PaperTypes).each(function(i, papertype){
							if(papertype.PaperTypeID == val){
								coverPaperMult = papertype.Multiplier;
							}
						});

						return;
					}

					if(attribute.AttributeKey == 'doublesided'){
						var val = $('select[name="' + attrName + '"]').val();

						//if double we need to show 2 ink color options
						if(val == 'double'){
							$('#side2').show(200);
						} else {
							$('#side2').hide(200);
						}
					}

					if($(attrEl).attr('type') == 'radio' || $(attrEl).attr('type') == 'checkbox'){
						attrEl = $('input[name="' + attrName + '"]:checked');
					}

					if(attrEl.length){
						var val = $(attrEl).val();

						$(attribute.Pricing).each(function(j, pricing){
							//some products attribute value will be based off another attr i.e. quantity
							if(pricing.ProductAttributeValue.substring(0,2) == "!!"){
								if(pricing.ValuePricePerUnit){
									switch(pricing.ProductAttributeValue.replace('!!','')){
										case 'quantity':
											if(val != ""){
												price += val * pricing.ValuePricePerUnit;
											}
											break;
										case 'number':
											if(val != ""){
												price += quantity * val * pricing.ValuePricePerUnit;
											}
											break;
									}
								}
							} else {
								if(pricing.ProductAttributeValue === val){
									if(pricing.Multiplier && pricing.ValuePricePerUnit){
										price += (pricing.ValuePricePerUnit * (useQuantity * pricing.Multiplier));
									} else if(pricing.Multiplier){
										price += (basePrice * pricing.Multiplier);
									} else {
										price += (useQuantity * pricing.ValuePricePerUnit);
									}

									return false;
								}
							}
						});

						base.validator(attribute.AttributeKey, val);
					}

					base.validator(attribute.AttributeKey, val);
				});

				if(numSheets > 0 && category == '2'){
					var baseQuantity =  $('input[name="' + options.quantityEl + '"]').val();
					price += baseQuantity * (numSheets * (sizeMult - 1));
					price += baseQuantity * (numSheets * (paperMult - 1));

					price += baseQuantity * (coverPaperMult - 1);
				} else {
					price *= paperMult;
					price *= sizeMult;
				}

				$(options.priceDisplay).html(price.toFixed(2));
				$(options.priceEl).val(price.toFixed(2));
			};

			/**
			 * Validate options selected based on key
			 * @return {[type]} [description]
			 */
			base.validator = function(key, val){
				switch(key){
					case 'doublesided':
						//numpages needs to be 2 if double is selected
						//otherwise it's the same thing on both sides
						var dependent = $('.numpages').val();

						//applies to single page quote
						if(val == 'double' && options.product.ProductCategoryID == '1'){
							if(dependent != '2'){
								$('.quote-alert')
									.html('The number of pages in your original should be 2 if you are printing double sided. Otherwise the same thing will be on both sides.')
									.removeClass('hide');
							}
						} else if(val == 'single'){
							if(dependent == '2'){
								$('.quote-alert')
									.html('You need to choose doublesided if your document will contain more than one page.')
									.removeClass('hide');
							}
						}
						break;
					case 'secondaryfold':
						if(val == 'yes'){
							$('.secondaryfold').siblings('.alert-info').removeClass('hide');
						} else {
							$('.secondaryfold').siblings('.alert-info').addClass('hide');
						}
						break;
					case 'numpagesother':
						var bwcolor = $('input', '.inkcolorpages');

						var totalbwcolor = 0;
						$(bwcolor).each(function(i,el){
							totalbwcolor += parseInt($(this).val());
						});

						if(totalbwcolor != val){
							$('.quote-alert')
								.html('The number of color pages + the number of B&W pages should add up to the number of pages in your document.')
								.removeClass('hide');
						}
						break;
					default:

						break;
				}
			}

			base.setDefaultPrice = function(){

			}

			base.orderContainer = $(options.orderContainerId);

			$('input[type="text"]', base.orderContainer).bind('blur', base.updatePrice);
			$('select, input[type="radio"], input[type="checkbox"]', base.orderContainer).change(base.updatePrice);




			return base;
		}

	});

})(jQuery);