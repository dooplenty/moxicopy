require.config({
	baseUrl: '/assets/js',
	paths: {
		bootstrap: 'bootstrap'
	}
});

define(
	'admin',
	[
		'jquery',
        'bootstrap',
        'plugins'
	],
	function($){
		//handle pricing stuff
		$('.add-price').click(function(e){
			e.preventDefault();

			var p = $('<p>');
			var quant = $('<input>');
			var unit = $('<input>');

			var currentnum = $('.pricing-block').length;
			var productid = $('input[name="product_id"]').val();

			$(quant)
				.attr('name', 'quantity[new' + (currentnum + 1) + ']')
				.attr('type', 'text');

			$(unit)
				.attr('name', 'perunit[new' + (currentnum + 1) + ']')
				.attr('type', 'text');

			$(p)
				.addClass('pricing-block')
				.append("Quantity: ")
				.append(quant)
				.append("Per Unit: ")
				.append(unit);

			$('.pricing').append(p);
		});

		$('.pop-over').popover({html:true,trigger:'hover',animation:true});
	}
);