<?php

class Order_Products_model extends MY_Model {
	public $OrderProductID;

	public $OrderID;

	public $ProductID;

	public $ProductUID;

	public $Quantity;

	public $ProductSizeID;

	public $FileUploadID;

	public $NumSheets;

	public $NumSheetsColor;

	public $NumSheetsBW;

	public $ProductNotes;

	public $ProjectName;

	protected $_primaryID = 'OrderProductID';

	protected $_tableName = 'Order_Products';

	public function is_double_sided(){
		$query = "
			SELECT *
			FROM Order_Attributes OA
			JOIN ProductAttributes PA ON PA.ProductAttributeID = OA.AttributeID
			WHERE PA.AttributeKey = 'doublesided'
			AND OA.ProductUID = '" . $this->ProductUID . "'";

		$result = $this->db->query($query);

		if($result->num_rows() > 0){
			$OrderAttribute = $result->row();
			if($OrderAttribute->Value == 'double'){
				return true;
			}
		}
		return false;
	}
}