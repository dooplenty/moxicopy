<?php

class Order_ShippingAddresses_model extends MY_Model {
	public $OrderShippingAddressID;

	public $OrderID;

	public $Address1;

	public $Address2;

	public $City;

	public $State;

	public $Zip;

	public $FirstName;

	public $LastName;

	protected $_tableName = 'Order_ShippingAddresses';
}