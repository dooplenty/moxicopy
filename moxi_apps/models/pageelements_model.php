<?php

class PageElements_Model extends MY_Model {

	public $PageElementID;

	public $Title;

	public $Text;

	public $Header;

	public $Key;

	public $SubHeader;

	public $MetaDescription;

	public $MetaKeywords;

	protected $_primaryID = 'PageElementID';

	protected $_tableName = 'PageElements';
}