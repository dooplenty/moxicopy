<?php

class Order_FileUploads_model extends MY_Model {
	public $OrderFileUploadID;

	public $FileUploadID;

	public $OrderProductID;

	public $PreviewPath;

	public $RootPath;

	protected $_primaryID = 'OrderFileUploadID';

	protected $_tableName = 'Order_FileUploads';
}