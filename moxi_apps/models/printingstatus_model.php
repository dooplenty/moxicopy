<?php

class PrintingStatus_model extends MY_Model {
	public $PrintingStatusID;

	public $PrintingStatusKey;

	public $PrintingStatusDescription;

	protected $_primaryID = "PrintingStatusID";

	protected $_tableName = "PrintingStatus";
}