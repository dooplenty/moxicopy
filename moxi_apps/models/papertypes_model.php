<?php

class PaperTypes_model extends MY_Model {

	public $PaperTypeID;

	public $PaperTypeName;

	public $PaperTypeDescription;

	public $PaperTypeCategory;

	public $Weight;

	public $Multiplier;

	protected $_tableName = 'PaperTypes';
}