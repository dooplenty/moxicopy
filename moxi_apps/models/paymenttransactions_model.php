<?php

class PaymentTransactions_model extends MY_Model {
	public $PaymentTransactionID;

	public $OrderID;

	public $Result;

	public $ResultText;

	public $Amount;

	public $TransactionData;

	public $DateTimeAdded;

	public $TransactionLength;

	public $PaymentTypeID;

	public $TrackingID;

	protected $_primaryKey = 'PaymentTransactionID';

	protected $_tableName = 'PaymentTransactions';

	public function save(){
		if(!empty($this->PaymentTransactionID)){
			$this->update();
		} else {
			$this->insert();
		}
	}

	public function update(){
		$this->db->where('PaymentTransactionID', $this->PaymentTransactionID);
		$this->db->update($this->_tableName, $this->to_array());
	}

	public function insert(){
		$this->db->insert($this->_tableName, $this->to_array());
		$this->PaymentTransactionID = $this->db->insert_id();
	}
}