<?php
class ProductPricing_model extends MY_Model {
	public $ProductPricingID;

	public $ProductID;

	public $Quantity;

	public $UnitPrice;

	protected $_primaryID = 'ProductPricingID';

	protected $_tableName = 'ProductPricing';

	public function save(){
		if(!empty($this->ProductPricingID)){
			$this->update();
		} else {
			$this->insert();
		}
	}

	public function update(){
		$this->db->where('ProductPricingID', $this->ProductPricingID);
		$this->db->update($this->_tableName, $this->to_array());
	}

	public function insert(){
		$this->db->insert($this->_tableName, $this->to_array());
		$this->ProductPricingID = $this->db->insert_id();
	}
}