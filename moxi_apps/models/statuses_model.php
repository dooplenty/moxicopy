<?php

class Statuses_model extends MY_Model {
	public $StatusID;

	public $Status;

	public $Description;

	protected $_primaryID = 'StatusID';

	protected $_tableName = 'Statuses';
}