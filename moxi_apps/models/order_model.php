<?php

class Order_model extends MY_Model {
	public $OrderID;

	public $StatusID;

	public $OrderDate;

	public $OrderDateTime;

	public $CancelDate;

	public $CancelDateTime;

	public $UserID;

	public $DateAdded;

	public $DateTimeAdded;

	public $ProjectName;

	public $RushOrder;

	public $Total;

	public $ShippingKey;

	public $Subtotal;

	public $PaymentTypeID;

	public $DateTimeUpdated;

	public $PrintingStatusID;

	public $HardCopyProof;

	protected $_primaryID = 'OrderID';

	protected $_tableName = 'Orders';

	/**
	 * Retrieve order by id
	 * @param  int $id the order id
	 * @return Object single row query result
	 */
	public function get_order($id){
		$this->db->where('OrderID', $id);
		$query = $this->db->get('Orders');

		return $query->row();
	}

	public function save_order($order_data){
		if(isset($order_data['OrderID']) && !empty($order_data['OrderID'])){
			$this->db->where('OrderID', $order_data['OrderID']);
			$method = 'update';
		} else {
			$order_data['DateAdded'] = date('Y-m-d');
			$order_data['DateTimeAdded'] = date('Y-m-d H:i:s');
			$method = 'insert';
		}

		$order_data['DateTimeUpdated'] = date('Y-m-d H:i:s');

		//get the vars to set
		$vars = get_object_vars($this);
		unset($vars['OrderID']);

		foreach($vars as $field => $val){
			if(isset($order_data[$field])){
				$this->db->set($field, $order_data[$field]);
			}
		}

		$this->db->set('NumProducts', count($order_data['Products']));

		$this->db->{$method}('Orders');

		$order_id = isset($order_data['OrderID']) ? $order_data['OrderID'] : $this->db->insert_id();
		$this->_clear_products($order_id);

		foreach($order_data['Products'] as $uid => $product){
			$product['ProductUID'] = $uid;
			$product['OrderID'] = $order_id;
			$this->save_order_product($product);
		}

		return $order_id;
	}

	protected function _clear_products($order_id){
		$this->db->where('OrderID', $order_id);
		$this->db->delete('Order_Products');

		$this->db->where('OrderID', $order_id);
		$this->db->delete('Order_Attributes');
	}

	public function save_order_product($product_data){
		$data = array(
			'OrderID' => $product_data['OrderID'],
			'ProductID' => $product_data['ProductID'],
			'ProductUID' => $product_data['ProductUID'],
			'Quantity' => $product_data['quantity'],
			'ProductSizeID' => $product_data['ProductSizeID'],
			'NumSheets' => $product_data['numSheets'],
			'ProductNotes' => $product_data['ProductNotes']
		);

		if(isset($product_data['FileUploadID'])){
			$data['FileUploadID'] = $product_data['FileUploadID'];
		}

		$this->db->insert('Order_Products', $data);

		$order_product_id = $this->db->insert_id();

		$this->save_order_attributes($product_data['Attributes'], $product_data['OrderID'], $product_data['ProductUID']);
	}

	public function save_order_attributes($attrs, $order_id, $product_uid){
		foreach($attrs as $attr_id => $val){
			$attrData = array(
				'OrderID' => $order_id,
				'ProductUID' => $product_uid,
				'AttributeID' => $attr_id,
				'Value' => $val
			);

			$this->db->insert('Order_Attributes', $attrData);
		}
	}

	public function save_order_billing($billing, $order_id){
		$this->db->where('OrderID', $order_id);

		$billing['OrderID'] = $order_id;

		if($this->db->count_all_results('Order_BillingAddresses') > 0){
			$this->db->where('OrderID', $order_id);
			$this->db->update('Order_BillingAddresses', $billing);
		} else {
			$this->db->insert('Order_BillingAddresses', $billing);
		}
	}

	public function save_order_shipping($shipping, $order_id){
		$this->db->where('OrderID', $order_id);

		$shipping['OrderID'] = $order_id;

		if($this->db->count_all_results('Order_ShippingAddresses') > 0){
			$this->db->where('OrderID', $order_id);
			$this->db->update('Order_ShippingAddresses', $shipping);
		} else {
			$this->db->insert('Order_ShippingAddresses', $shipping);
		}
	}

	public function get_shipping_info($order_id){
		$this->db->where('OrderID', $order_id);
		$query = $this->db->get('Order_ShippingAddresses');

		return $query->row();
	}

	public function save_shipping_rates($rate){
		$this->db->insert('Order_ShippingRates', $rate);
	}

	public function get_shipping_rates($order_id, $key = null){
		if(!is_null($key)){
			$this->db->where('ServiceType', $key);
		}
		$this->db->where('OrderID', $order_id);
		$this->db->order_by('TotalNetCharge ASC');
		$query = $this->db->get('Order_ShippingRates');

		if($query->num_rows() === 1){
			return $query->row();
		}

		return $query->result_object();
	}

	public function get_card_types(){
		$query = $this->db->get('CardTypes');
		return $query->result_object();

	}

	public function get_billing_address($id){
		$this->db->where('OrderID', $id);
		$query = $this->db->get('Order_BillingAddresses');

		return $query->row();

	}

	public function get_order_attributes(){

		$this->db->where('AG.AttributeGroupName', 'Order');
		$this->db->join('ProductAttributeValues PAV', 'PAV.ProductAttributeID = PA.ProductAttributeID');
		$this->db->join('ProductAttributes_AttributeGroups PAG', 'PAG.ProductAttributeID = PA.ProductAttributeID');
		$this->db->join('AttributeGroups AG', 'AG.AttributeGroupID = PAG.AttributeGroupID');
		$query = $this->db->get('ProductAttributes PA');

		return $query->result_object();
	}

	public function get_order_status($status_id){
		$this->db->where('StatusID', $status_id);
		$query = $this->db->get('Statuses');

		return $query->row();
	}

	public function get_orders_by_user($user_id){
		$this->db->where('UserID', $user_id);
		$query = $this->db->get('Orders');

		$Orders = $query->result_object();

		foreach($Orders as &$Order){
			$Order->Status= $this->get_order_status($Order->StatusID);
			$Order->User = $this->User_model->get_user($Order->UserID);
		}

		return $Orders;
	}
}

?>