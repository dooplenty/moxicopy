<?php

class Products_model extends MY_Model {

	public $ProductID;

	public $ProductName;

	public $ProductCategoryID;

	public $FeaturedProduct;

	public $ShowInNav;

	public $DefaultQuantity;

	protected $_primaryID = 'ProductID';

	protected $_tableName = 'Products';

	protected $_CI;

	public function __construct() {
		parent::__construct();

		$this->_CI =& get_instance();
	}

	public function get_products() {
		$this->db->join('ProductCategories', 'ProductCategories.ProductCategoryID = Products.ProductCategoryID', 'left');
		$query = $this->db->get('Products');

		return $query->result_object();
	}

	public function get_product_categories() {
		$query = $this->db->get('ProductCategories');

		return $query->result_object();
	}

	public function get_product_category($id) {
		$this->db->where('ProductCategoryID', $id);
		$query = $this->db->get('ProductCategories');

		return $query->row();
	}

	public function get_product_attributes($id){
		$this->db->join('Product_ProductAttributes PPA', 'PPA.ProductAttributeID = PA.ProductAttributeID');
		$this->db->join('ProductAttributes_AttributeGroups PAG', 'PAG.ProductAttributeID = PPA.ProductAttributeID', 'LEFT');
		$this->db->join('AttributeGroups AG', 'AG.AttributeGroupID = PAG.AttributeGroupID', 'LEFT');
		$this->db->where('PPA.ProductID', $id);
		$query = $this->db->get('ProductAttributes PA');

		$Attributes = $query->result_object();

		foreach($Attributes as &$Attribute){
			$Attribute->Pricing = $this->get_attribute_pricing($Attribute->ProductAttributeID);
		}

		return $Attributes;
	}

	public function get_product_attributes_without_groups($id){
		$this->db->join('Product_ProductAttributes PPA', 'PPA.ProductAttributeID = PA.ProductAttributeID');
		$this->db->where('PPA.ProductID', $id);
		$query = $this->db->get('ProductAttributes PA');

		$Attributes = $query->result_object();

		foreach($Attributes as &$Attribute){
			$Attribute->Pricing = $this->get_attribute_pricing($Attribute->ProductAttributeID);
		}

		return $Attributes;
	}

	public function get_attribute_pricing($attr_id){
		$this->db->where('ProductAttributeID', $attr_id);
		$query = $this->db->get('ProductAttributeValues');

		return $query->result_object();
	}

	public function get_product_info($id){
		$this->db->where('Products.ProductID', $id);
		$query = $this->db->get('Products');

		return $query->row();
	}

	public function get_product($id) {
		$this->db->where('Products.ProductID', $id);
		$query = $this->db->get('Products');

		$product = $query->row();

		$product->Attributes = $this->get_product_attributes_without_groups($id);
		$product->Category = $this->get_product_category($product->ProductCategoryID);
		$product->Sizes = $this->get_product_sizes($id);
		$product->Pricing = $this->get_product_pricing($id);
		$product->PaperTypes = $this->get_paper_types();

		$this->_CI->load->model('ProductMeta_model');
		$product->Meta = $this->ProductMeta_model->get_one_where('ProductID', $id);

		return $product;
	}

	public function get_product_sizes($product_id = null) {
		if(!is_null($product_id)) {
			$this->db->join('Product_ProductSizes', 'Product_ProductSizes.ProductSizeID = ProductSizes.ProductSizeID');
			$this->db->where('ProductID', $product_id);
		}

		$query = $this->db->get('ProductSizes');

		return $query->result_object();
	}

	public function get_product_size($product_size_id) {
		$this->db->where('ProductSizeID', $product_size_id);
		$query = $this->db->get('ProductSizes');

		return $query->row();
	}

	public function get_paper_types($id = null){
		if(!is_null($id)){
			$this->db->where('PaperTypeID', $id);
		}

		$this->db->order_by('PaperTypeCategory ASC, PaperTypeDescription ASC');
		$query = $this->db->get('PaperTypes');

		return $query->result_object();
	}

	public function get_product_pricing($product_id = null){
		if(!is_null($product_id)){
			$this->db->where('ProductID', $product_id);
		}

		$this->db->order_by('Quantity', 'ASC');
		$query = $this->db->get('ProductPricing');

		return $query->result_object();
	}

	public function get_attributes($id = null){
		if($id){
			$this->db->where('ProductAttributeID', $id);
		}
		$query  = $this->db->get('ProductAttributes');
		return $query->result_object();
	}

	public function get_sizes(){
		$query = $this->db->get('ProductSizes');
		return $query->result_object();
	}
}

?>