<?php

class OrderNotes_model extends MY_Model {
	public $OrderNoteID;

	public $Note;

	public $UserID;

	public $DateTimeAdded;

	public $OrderID;

	protected $_primaryID = 'OrderNoteID';

	protected $_tableName = 'OrderNotes';
}