<?php
class Contact_model extends MY_Model {
	public $PersonName;

	public $CompanyName;

	public $PhoneNumber;

	public $StreetLines;

	public $City;

	public $StateOrProvinceCode;

	public $Residential;

	public $CountryCode = 'US';
}