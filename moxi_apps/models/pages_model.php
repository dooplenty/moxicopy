<?php

class Pages_model extends MY_Model {

	public function get_pages($pagekey){
		$this->db->where_in('Key', $pagekey);
		$query = $this->db->get('PageElements');

		return $query->result_object();
	}
}