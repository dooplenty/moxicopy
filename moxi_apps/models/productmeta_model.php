<?php class ProductMeta_model extends MY_Model {

	public $ProductMetaID;

	public $ProductID;

	public $MetaDescription;

	public $MetaKeywords;

	protected $_primaryID = 'ProductMetaID';

	protected $_tableName = "ProductMeta";

}