<?php

class Order_ShippingRates_model extends MY_Model {
	public $OrderShippingRateID;

	public $OrderID;

	public $HighestSeverity;

	public $Notifications;

	public $ServiceType;

	public $PackagingType;

	public $ActualRateType;

	public $TotalBillingWeight;

	public $TotalBaseCharge;

	public $TotalSurcharges;

	public $TotalNetFedexCharge;

	public $TotalTaxes;

	public $TotalNetCharge;

	public $TotalRebates;

	public $Surcharges;

	public $ShippingPaid;

	protected $_primaryKey = 'OrderShippingRateID';

	public $_tableName = 'Order_ShippingRates';
}