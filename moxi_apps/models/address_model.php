<?php

class Address_model extends MY_Model {

	/**
	 * Array of street lines array('address 1', 'address 2')
	 * @var array
	 */
	public $StreetLines;

	public $City;

	public $StateOrProvinceCode;

	public $PostalCode;

	public $CountryCode;

	public $Residential;
}