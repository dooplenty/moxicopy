<?php

class RequestedShipment_model extends MY_Model {

	public $PackagingType;

	/**
	 * Where the shipment originates from
	 * @var array
	 */
	public $Shipper;

	/**
	 * Descriptive data for the physical location of shipment
	 * @var array
	 */
	public $Recipient;

	/**
	 * Details about the image format and printer type for the returned label
	 * @var LabelSpecification_model
	 */
	public $LabelSpecification;


	/**
	 * [$RequestedPackageLineItems description]
	 * @var [type]
	 */
	public $RequestedPackageLineItems;

	public $DropoffType = 'REGULAR_PICKUP';

	public $ShipTimestamp;

	public $ShippingChargesPayment;

	public $PackageCount;

	public $TotalWeight;
}