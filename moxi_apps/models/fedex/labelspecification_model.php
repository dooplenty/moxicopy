<?php

class LabelSpecification_model extends MY_Model {

	/**
     * The LabelFormatType
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var FedexRateServiceEnumLabelFormatType
     */
    public $LabelFormatType;
    /**
     * The ImageType
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var FedexRateServiceEnumShippingDocumentImageType
     */
    public $ImageType;
    /**
     * The LabelStockType
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var FedexRateServiceEnumLabelStockType
     */
    public $LabelStockType;
    /**
     * The LabelPrintingOrientation
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var FedexRateServiceEnumLabelPrintingOrientationType
     */
    public $LabelPrintingOrientation;
    /**
     * The LabelRotation
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var FedexRateServiceEnumLabelRotationType
     */
    public $LabelRotation;
    /**
     * The PrintedLabelOrigin
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var FedexRateServiceStructContactAndAddress
     */
    public $PrintedLabelOrigin;
    /**
     * The CustomerSpecifiedDetail
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var FedexRateServiceStructCustomerSpecifiedLabelDetail
     */
    public $CustomerSpecifiedDetail;
}