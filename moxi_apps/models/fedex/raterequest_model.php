<?php

class RateRequest_model extends MY_Model {

	public $WebAuthenticationDetail;

	public $ClientDetail;

	/**
	 * The shipment for which a rate quote is desired
	 * @var RequestedShipment_model
	 */
	public $RequestedShipment;

	public $TransactionDetail;

	public $Version;
}