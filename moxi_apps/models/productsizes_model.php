<?php

class ProductSizes_model extends MY_Model {

	public $ProductSizeID;

	public $SizeX;

	public $SizeY;

	public $SizeName;

	public $PreviewWidth;

	public $PreviewHeight;

	public $Multiplier;

	public $WeightFactor;

	protected $_tableName = 'ProductSizes';
}