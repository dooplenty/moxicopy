<?php class ProductAttributeValues_model extends MY_Model {

	public $ProductAttributeValueID;

	public $ProductAttributeID;

	public $ProductAttributeValue;

	public $ValuePricePerUnit;

	public $Multiplier;

	protected $_primaryID = 'ProductAttributeValueID';

	protected $_tableName = "ProductAttributeValues";

}