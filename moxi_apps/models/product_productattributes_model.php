<?php

class Product_ProductAttributes_model extends MY_Model {
	public $Product_ProductAttributeID;

	public $ProductID;

	public $ProductAttributeID;

	public $Active;

	protected $_tableName = 'Product_ProductAttributes';

	public function save(){
		if(!empty($this->ProductPricingID)){
			$this->update();
		} else {
			$this->insert();
		}
	}

	public function update(){
		$this->db->where('Product_ProductAttributeID', $this->Product_ProductAttributeID);
		$this->db->update($this->_tableName, $this->to_array());
	}

	public function insert(){
		$this->db->insert($this->_tableName, $this->to_array());
		$this->Product_ProductAttributeID = $this->db->insert_id();
	}
}