<?php

class User_model extends MY_Model {

	public function save_user($user_data){
		//does the user name already exist
		$this->db->where('Username', $user_data['Username']);
		$query = $this->db->get('Users');

		if($query->num_rows() > 0){
			$this->session->set_flashdata('warning', 'This username already exists.');
			return false;
		}

		$query->free_result();

		$data = array(
			'Username' => $user_data['Username'],
			'Password' => md5($user_data['Password'])
		);

		$this->db->insert('Users', $data);
		$UserID = $this->db->insert_id();

		unset($user_data['Username']);
		unset($user_data['Password']);
		unset($user_data['PassConf']);
		$user_data['UserID'] = $UserID;

		$this->db->insert('UserInfo', $user_data);

		return true;
	}

	public function login($username, $password){
		$password = md5($password);

		$this->db->where('Username', $username);
		$this->db->where('Password', $password);
		$this->db->join('UserInfo', 'UserInfo.UserID = Users.UserID', 'left');
		$query = $this->db->get('Users');

		if($query->num_rows() !== 0){
			$user =  $query->row();

			$query->free_result();

			$this->db->where('UserID', $user->UserID);
			$this->db->set('LastLoggedIn', date('Y-m-d H:i:s'));
			$this->db->update('Users', array('Username' => $username));

			return $user;
		}

		return false;
	}

	public function get_user($user_id){
		$this->db->where('U.UserID', $user_id);
		$this->db->join('UserInfo UI', 'UI.UserID = U.UserID', 'LEFT');
		$query = $this->db->get('Users U');

		return $query->row();
	}
}