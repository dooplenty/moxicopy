<?php

class ProductAttributes_model extends MY_Model {

	public $ProductAttributeID;

	public $AttributeName;

	public  $AttributeKey;

	public $Required;

	protected $_tableName = 'ProductAttributes';

	protected $_primaryID = 'ProductAttributeID';

	public function add_groups(){

		$this->db->where('ProductAttributeID', $this->ProductAttributeID);
		$this->db->join('AttributeGroups AG', 'AG.AttributeGroupID = PAG.AttributeGroupID');
		$query = $this->db->get('ProductAttributes_AttributeGroups PAG');

		$group = $query->row();

		if(!empty($group)){
			$this->populate($group);
		}
	}
}