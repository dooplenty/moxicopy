<?php

class Fileupload_model extends MY_Model {
	public $FileUploadID;

	public $UserID;

	public $UserIP;

	public $FileLocation;

	public $FileHash;

	public $FileSize;

	public $OriginalFilename;

	public $NewFilename;

	public $Deleted;

	public $UploadDateTime;

	public $DeletedDateTime;

	protected $_primaryID = 'FileUploadID';

	protected $_tableName = 'FileUploads';

	public function save(){
		if(!empty($this->FileUploadID)){
			$this->update();
		} else {
			$this->insert();
		}
	}

	public function update(){
		$this->db->where('FileUploadID', $this->FileUploadID);
		$this->db->update($this->_tableName, $this->to_array());
	}

	public function insert(){
		$this->db->insert($this->_tableName, $this->to_array());
		$this->FileUploadID = $this->db->insert_id();
	}
}
?>