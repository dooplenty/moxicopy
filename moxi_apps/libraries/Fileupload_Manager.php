<?php

class Fileupload_Manager {

	protected $_FileUpload;

	protected $_CI;

	protected $_rootPath;

	public function __construct(){
		$this->_CI =& get_instance();

		$this->_CI->load->model('Fileupload_model');
	}

	public function __get($prop){
		if(isset($this->_FileUpload) && isset($this->_FileUpload->{$prop})){
			return $this->_FileUpload->{$prop};
		}

		return false;
	}

	public function get_upload_location($dir){
		$path = './fileuploads/';

		$user = $this->_CI->session->userdata('user');

		if($user){
			$path .= 'users/' . $user->UserID . '/' . $dir . '/';
		} else {
			$usersession = $this->_CI->session->userdata('ip_address');

			$path .= 'tmp/' . $usersession . '/' . $dir . '/';
		}

		$this->_rootPath = $path;

		if(!is_dir($path)) {
			mkdir($path, 0777, true);
		}


		return $path;
	}

	public function init($fileid){
		$this->_FileUpload = $this->_CI->Fileupload_model->get_one_where('FileUploadID', $fileid);
	}

	public function save_file($data){
		$this->_CI->Fileupload_model->populate($data);
		$this->_CI->Fileupload_model->save();

		$this->_FileUpload = $this->_CI->Fileupload_model;
	}

	public function get_rootpath(){
		return $this->_rootPath;
	}

	public function no_preview(){

	}

	public function get_existing($hash){
		$Fileupload = $this->_CI->Fileupload_model->get_one_where('FileHash', $hash);

		return $Fileupload;
	}

	public function get_file(){
		return $this->_FileUpload;
	}
}