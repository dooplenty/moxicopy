<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Fedex {

	/**
	 * Fedex wsdl location
	 * @var string
	 */
	public $wsdl;

	protected $_SoapClient;

	protected $_accountNum;

	protected $_meterNum;

	protected $_CI;

	private $_key;

	private $_password;

	public function __construct($config) {
		$this->_CI =& get_instance();

		if(!isset($config['wsdl'])){
			throw new Exception('You are required to pass the wsdl location in your config object.');
		}

		$this->wsdl = $config['wsdl'];
		$this->_key = $config['key'];
		$this->_password = $config['password'];
		$this->_accountNum = $config['accountNum'];
		$this->_meterNum = $config['meterNum'];

		$this->_SoapClient = new SoapClient(site_url('/libs/fedex/prod/RateService_v13.wsdl'), array('trace' => 1, 'soap_version' => 'SOAP_1_2'));
	}

	public function getShippingRates($request){
		$RateRequest = $this->_CI->load->model('fedex/RateRequest_model');

		$credentials = $this->_get_credentials();
		$this->_CI->RateRequest_model->populate($credentials);

		$this->_CI->load->model('Contact_model');

		$RecipientContact = clone $this->_CI->Contact_model;
		$ShipperContact = clone $this->_CI->Contact_model;
		$ShipperContact->populate($request['Shipper']);
		$RecipientContact->populate($request['Recipient']);

		$this->_CI->load->model('fedex/RequestedShipment_model');
		$this->_CI->RequestedShipment_model->ShipTimestamp = date('c');
		$this->_CI->RequestedShipment_model->Shipper = $ShipperContact->to_array();
		$this->_CI->RequestedShipment_model->Recipient = $RecipientContact->to_array();

		$this->_CI->RequestedShipment_model->PackagingType = $request['Packaging'];

		/*$this->_CI->RequestedShipment_model->ShippingChargesPayment = array(
			'SequenceNumber' => 1,
			'PaymentType' => $request['ShippingPaymentType'],
			'Payor' => array(
				'ResponsibleParty' => array(
					'CountryCode' => 'US',
					'AccountNumber' => $request['AccountNumber']
				)
			)
		);*/

		/*$this->_CI->load->model('fedex/LabelSpecification_model');
		$this->_CI->LabelSpecification_model->LabelFormatType = 'LABEL_DATA_ONLY';
		$this->_CI->LabelSpecification_model->LabelStockType = 'PAPER_4X6';
		$this->_CI->LabelSpecification_model->ImageType = 'DPL';
		$this->_CI->LabelSpecification_model->LabelPrintingOrientation = 'TOP_EDGE_OF_TEXT_FIRST';
		$this->_CI->LabelSpecification_model->LabelRotation = 'NONE';*/
		// $LabelSpecification->PrintedLabelOrigin =

		// $this->_CI->RequestedShipment_model->LabelSpecification = $this->_CI->LabelSpecification_model->to_array();

		$this->_CI->RequestedShipment_model->RequestedPackageLineItems = array(
			'GroupPackageCount' => $request['packageCount'],
			'Weight' => array(
				'Value' => $request['Weight'],
				'Units' => 'LB'
			)
		);

		/*$this->_CI->RequestedShipment_model->RequestedPackageLineItems = array(
			'SequenceNumber'=>1,
			'GroupPackageCount'=>1,
			'Weight' => array(
				'Value' => 50.0,
				'Units' => 'LB'
			),
			'Dimensions' => array(
				'Length' => 108,
				'Width' => 5,
				'Height' => 5,
				'Units' => 'IN'
			)
		);*/

/*		$this->_CI_RateRequest_model->RequestedShipment->TotalWeight = '5';

		$this->_CI->RateRequest_model->RequestedShipment->PackageCount = $request['packageCount'];*/

		$this->_CI->RateRequest_model->RequestedShipment = $this->_CI->RequestedShipment_model->to_array();

		$this->_CI->RateRequest_model->TransactionDetail = 'Fedex Rate Request';

		$this->_CI->RateRequest_model->Version = array(
			'ServiceId' => 'crs',
			'Major' => '13',
			'Intermediate' => '0',
			'Minor' => '0'
		);

		$Response = $this->_SoapClient->getRates($this->_CI->RateRequest_model->to_array());

		return $Response;
	}

	/**
	 * Get api credentials
	 * @return array request array for api credentials
	 */
	protected function _get_credentials(){
		return array(
			'WebAuthenticationDetail' => array (
				'UserCredential' => array(
					'Key' => $this->_key,
					'Password' => $this->_password
				)
			),
			'ClientDetail' => array(
				'AccountNumber' => $this->_accountNum,
				'MeterNumber' => $this->_meterNum
			)
		);
	}
}

/* End of file Fedex.php */