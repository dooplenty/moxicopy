<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paypal {

	/**
	 * Url to connect to
	 * @var string
	 */
	protected $_url;

	/**
	 * whether not errors are being logged
	 * @var boolean
	 */
	protected $_logErrors;

	/**
	 * Fields for processing through paypal
	 * @var array
	 */
	protected $_fields = array();

	/**
	 * Codeigniter instance
	 * @var Codeigniter
	 */
	protected $CI;

	/**
	 * Data returned from paypal ipn post
	 * @var array
	 */
	public $ipn_data;

	/**
	 * Last error received from transaction
	 * @var string
	 */
	public $errorMessage;

	/**
	 * Response data from paypal
	 * @var string
	 */
	public $ipn_response;

	/**
	 * Set up base config for sending data to paypal
	 * @param array $config initial config items for paypal post
	 */
	public function Paypal(){
		$this->CI =& get_instance();

		if(!$this->CI->config->item('paypal')){
			throw new Exception('No config file loaded for paypal checkout');
		}

		$config = $this->CI->config->item('paypal');

		if($config['paypal_is_test_mode'] === TRUE){
			$this->_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
		} else {
			$this->_url = 'https://www.paypal.com/cgi-bin/webscr';
		}

		if(isset($config['fields'])){
			foreach($config['fields'] as $field => $val){
				$this->add_field($field, $val);
			}
		}
	}

	public function add_field($field, $val){
		$this->_fields[$field] = $val;
	}

	public function submit_payment(){
		$config = $this->CI->config->item('paypal');
		$postView = $config['postView'];
		$data = array();
		$data['fields'] = $this->_fields;
		$data['url'] = $this->_url;

		$view = $this->CI->load->view($postView, $data);
	}

	public function validate_ipn(){
		$config = $this->CI->config->item('paypal');
		/*$url_parsed = parse_url($this->_url);

		$postString = '';
		foreach($_POST as $field => $value){
			$this->ipn_data[$field] = $value;
			$postString .= $field . '=' . urlencode(stripslashes($value)) . '&';
		}
		$postString .= "cmd=_notify-validate";*/

		$this->ipn_data = array();

		$req = 'cmd=_notify-validate';
		foreach($_POST AS $key => $value){
			$value = urlencode(stripslashes($value));
			$req .= "&$key=$value";

			$this->ipn_data[$key] = $value;
		}

		//post back to Paypal to validate
		$header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
		$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
		$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";

		if($config['paypal_is_test_mode'] === TRUE){
			$sandbox = "sandbox.";
		} else {
			$sandbox = "";
		}

		//open connection to paypal
		$fp = fsockopen("ssl://www.{$sandbox}paypal.com","443",$err_num,$err_str,30);
		if(!$fp){
			$this->errorMessage = "fsockopen error no. $err_num: $err_str";

			if($config['ipn_log'] === true){
				$this->_log_results(false);
			}

			return false;
		} else {

			fputs($fp, $header . $req);
			/*//post data back to paypal
			// Post the data back to paypal
	        fputs($fp, "POST {$url_parsed['path']} HTTP/1.1\r\n");
	        fputs($fp, "Host: {$url_parsed['host']}\r\n");
	        fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
	        fputs($fp, "Content-length: ".strlen($postString)."\r\n");
	        fputs($fp, "Connection: close\r\n\r\n");
	        fputs($fp, $postString . "\r\n\r\n");*/

	        while(!feof($fp)){
	        	$this->ipn_response .= fgets($fp, 1024);
	        }

	        fclose($fp);
		}

		//store this payment transaction
		$PayModel = $this->CI->PaymentTransactions_model;
        $PayModel->OrderID = $this->ipn_data['item_number'];
        $PayModel->Result = trim(substr($this->ipn_response, -8));
        $PayModel->ResultText = $this->ipn_response;
        $PayModel->Amount = $this->ipn_data['payment_gross'];
        $PayModel->TransactionData = json_encode($this->ipn_data);
        $PayModel->DateTimeAdded = date('Y-m-d H:i:s');
        $PayModel->PaymentTypeID = 3;
        $PayModel->save();

		if(strcmp("VERIFIED", $this->ipn_response)){

			//this was a valid transaction
			$this->_log_results(true);
			return true;
		} else {

			//invalid ipn transaction
			$this->errorMessage = "IPN Validation Failed.";
			$this->_log_results(false);
			return false;
		}
	}

	public function send_payment_notification(){
		$config = $this->CI->config->item('paypal');

		if(isset($this->ipn_data['payer_email'])){
			$subject = "Paypal Instant Payment Notification - Received Payment";

			if($config['paypal_is_test_mode'] === TRUE){
				$subject .= " ******* TEST PAYMENT ******* ";
			}

			$message = $this->CI->load->view($config['paypal_payment'], $this->ipn_data, true);

		}
	}

	public function get_data(){
		return $this->ipn_data;
	}

	/**
	 * Log ipn results to file
	 * @param  boolean $success Whether the results from ipn post were success or fail
	 * @return void
	 */
	protected function _log_results($success = true){
		$config = $this->CI->config->item('paypal');

		$log = '[' . date('Y-m-d H:i:s') . '] - ';

		$log .= ($success === true ? 'SUCCESS: ' : 'FAIL: ') . $this->errorMessage . "\n";

		$log .= "IPN POST VARS from Paypal: \n";
		if(is_array($this->ipn_data)){
			foreach($this->ipn_data as $key => $val){
				$log .= "$key=$val, ";
			}
		}

		$log .= "\nIPN Response from Paypal: \n" . $this->ipn_response . "\n\n";
		mail(DEVELOPER_URL, 'Log', $log);
		//write to log
		$fp=fopen($config['ipn_log_file'], 'a');
		fwrite($fp, $log);

		fclose($fp);
	}
}