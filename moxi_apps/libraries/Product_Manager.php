<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product_Manager {
	protected $_CI;

	/**
	 * Instance of product model
	 * @var Products_model
	 */
	protected $_Product;

	/**
	 * Unique key so that we can have multiple versions of one product
	 * @var string
	 */
	protected $_uniqueKey;

	public function __construct(){
		$this->_CI =& get_instance();
	}

	public function __get($prop){
		if(isset($this->_Product) && isset($this->_Product->{$prop})){
			return $this->_Product->{$prop};
		}

		return false;
	}

	public function init($productID, $uniqueKey = null){
		$this->_Product = $this->_CI->Products_model->get_one_where('ProductID', $productID);

		if(!is_null($uniqueKey)){
			$this->_uniqueKey = $uniqueKey;
		} else {
			$this->_uniqueKey = md5(date('Y-m-d H:i:s') . $this->_Product->ProductID);
		}
	}

	public function get_product(){
		return $this->_Product;
	}

	public function get_product_key(){
		return $this->_uniqueKey;
	}

	public function attach_meta(){
		$this->_Product->Attributes = $this->get_product_attributes();

		$this->_Product->Sizes = $this->get_product_sizes();

		$this->_Product->Meta = $this->get_product_meta();

		$this->_Product->Pricing = $this->get_product_pricing();

		$this->_Product->PaperTypes = $this->get_paper_types();
	}

	public function get_product_pricing(){
		$this->_CI->ProductPricing_model->OrderBy = 'Quantity ASC';
		$Pricing = $this->_CI->ProductPricing_model->get_where('ProductID', $this->_Product->ProductID);

		return $Pricing;
	}

	public function get_product_meta(){
		$Meta = $this->_CI->ProductMeta_model->get_one_where('ProductID', $this->_Product->ProductID);

		return $Meta;
	}

	public function get_product_attributes(){
		$this->_CI->Product_ProductAttributes_model->OrderBy = 'SortOrder ASC';

		$Attributes = $this->_CI->Product_ProductAttributes_model->get_where('ProductID', $this->_Product->ProductID);

		foreach($Attributes as &$Attribute){
			$Attribute = $this->_CI->ProductAttributes_model->get_one_where('ProductAttributeID', $Attribute->ProductAttributeID);
			$Attribute->add_groups();

			$Attribute->Pricing = $this->_CI->ProductAttributeValues_model->get_where('ProductAttributeID', $Attribute->ProductAttributeID);
		}

		return $Attributes;
	}

	public function get_product_sizes(){
		$Sizes = $this->_CI->Product_ProductSizes_model->get_where('ProductID', $this->_Product->ProductID);

		foreach($Sizes as &$Size){
			$Size = $this->_CI->ProductSizes_model->get_one_where('ProductSizeID', $Size->ProductSizeID);
		}

		return $Sizes;
	}

	public function get_paper_types(){
		$PaperTypes = $this->_CI->PaperTypes_model->fetch_all();

		return $PaperTypes;
	}
}