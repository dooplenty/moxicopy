<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Order_Manager {
	protected $_Order;

	protected $_CI;

	protected $_Products;

	public function __construct(){
		$this->_CI =& get_instance();

		$this->_CI->load->model('Order_FileUploads_model');
		$this->_CI->load->model('Order_Products_model');
	}

	public function __get($prop){
		if(isset($this->_Order) && isset($this->_Order->{$prop})){
			return $this->_Order->{$prop};
		}

		return false;
	}

	public function __set($prop, $value){
		if(property_exists('Order_model', $prop)){
			$this->_Order->$prop = $value;
			$this->save($this->_Order);
		}
	}

	public function init($orderID = null){
		if($orderID){
			$this->_Order = $this->_CI->Order_model->get_one_where('OrderID', $orderID);
		} else {
			$order = array(
				'StatusID' => 8, //initialized
				'DateAdded' => date('Y-m-d'),
				'DateTimeAdded' => date('Y-m-d H:i:s')
			);

			$this->_CI->Order_model->populate($order);
			$this->_CI->Order_model->save();

			$this->_Order = $this->_CI->Order_model;
		}
	}

	public function order_created(){
		return $this->_Order ? TRUE : FALSE;
	}

	public function get_order(){
		return $this->_Order;
	}

	public function get_active_orders(){
		$query = "
			SELECT
				DISTINCT
				O.*,
				SA.*,
				U.*,
				PT.*,
				S.*,
				PS.*,
				UI.Address1 AS UserAddress,
				UI.Address2 AS UserAddress2,
				UI.City AS UserCity,
				UI.State AS UserState,
				UI.Zip AS UserZip,
				UI.Organization,
				UI.Phone,
				UI.FirstName AS UserFirstName,
				UI.LastName AS UserLastName,
				O.OrderID AS OrderID
			FROM Orders O
			LEFT JOIN Order_ShippingAddresses SA USING(OrderID)
			LEFT JOIN PaymentTypes PT USING(PaymentTypeID)
			LEFT JOIN Users U USING(UserID)
			LEFT JOIN UserInfo UI USING(UserID)
			LEFT JOIN Statuses S USING(StatusID)
			LEFT JOIN PrintingStatus PS USING(PrintingStatusID)
			WHERE O.StatusID IN(2,3,6,7);
		";

		$results = $this->_CI->db->query($query);

		return $results->result_object();
	}

	public function attach_meta(){
		if(!$this->_Order){
			return false;
		}

		$this->_Order->Shipping = $this->get_shipping_info();
		$this->_Order->Billing = $this->get_billing_info();
		$this->_Order->Products = $this->get_products();
		$this->_Order->User = $this->get_user_info();
		$this->_Order->Notes = $this->get_order_notes();
		$this->_Order->ShippingRate = $this->get_shipping_rate();
	}

	public function get_shipping_rate(){
		$ShippingRate = array();

		if(!is_null($this->_Order->ShippingKey)){
			$ShippingRate = $this->_CI->Order_ShippingRates_model->get_one_where(array(
				'OrderID' => $this->_Order->OrderID,
				'ServiceType' => $this->_Order->ShippingKey
			));
		}

		return $ShippingRate;
	}

	public function get_shipping_info(){
		$Shipping = $this->_CI->Order_ShippingAddresses_model->get_one_where('OrderID', $this->_Order->OrderID);

		return $Shipping;
	}

	public function get_billing_info(){
		$Billing = $this->_CI->Order_BillingAddresses_model->get_one_where('OrderID', $this->_Order->OrderID);

		return $Billing;
	}

	public function get_user_info(){
		$query = "
			SELECT *
			FROM Users U
			JOIN UserInfo UI USING(UserID)
			WHERE UserID = ?
		";

		$results = $this->_CI->db->query($query, $this->_Order->UserID);
		return $results->row();
	}

	public function get_order_notes(){
		$Notes = $this->_CI->OrderNotes_model->get_one_where('OrderID', $this->_Order->OrderID);

		return $Notes;
	}

	public function get_product($productUid, $productId = null, $create = true){
		$Products = $this->get_products();

		if(empty($Products) || !isset($Products[$productUid])){
			if($create === true){
				$product = array(
					'OrderID' => $this->_Order->OrderID,
					'ProductID' => $productId,
					'ProductUID' => $productUid
				);

				$this->_CI->Order_Products_model->populate($product);
				$this->_CI->Order_Products_model->save();

				//we saved a new product so make this load again
				$this->_Products = null;

				return $this->_CI->Order_Products_model;
			} else {
				return new stdClass();
			}
		} else {
			return $Products[$productUid];
		}
	}

	public function get_fileuploads($orderProdId){
		$Uploads = $this->_CI->Order_FileUploads_model->get_where('OrderProductID', $orderProdId);

		foreach($Uploads as &$Upload){
			$Upload->FileInfo = $this->_CI->FileUpload_model->get_one_where('FileUploadID', $Upload->FileUploadID);
		}
		return $Uploads;
	}

	public function get_fileupload($orderProdId, $fileUploadId, $rootPath = null){
		$Uploads = $this->_CI->Order_FileUploads_model->get_one_where(array(
			'OrderProductID' => $orderProdId,
			'FileUploadID' => $fileUploadId
		));

		if(empty($Uploads)){
			$this->_CI->Order_FileUploads_model->OrderProductID = $orderProdId;
			$this->_CI->Order_FileUploads_model->FileUploadID = $fileUploadId;
			$this->_CI->Order_FileUploads_model->RootPath = $rootPath;
			$this->_CI->Order_FileUploads_model->save();

			$Uploads = $this->_CI->Order_FileUploads_model;
		}

		return $Uploads;
	}

	public function get_products(){
		if($this->_Products){
			return $this->_Products;
		}

		$productsInfo = array();

		$Products = $this->_CI->Order_Products_model->get_where('OrderID', $this->_Order->OrderID);

		foreach($Products as &$Product){
			$Product->Info = $this->_CI->Products_model->get_one_where('ProductID', $Product->ProductID);

			$this->_CI->ProductPricing_model->OrderBy = 'Quantity ASC';
			$Product->Pricing = $this->_CI->ProductPricing_model->get_where('ProductID', $Product->ProductID);
			$Product->Size = $this->_CI->ProductSizes_model->get_one_where('ProductSizeID', $Product->ProductSizeID);
			$Product->Attributes = $this->_CI->Order_Attributes_model->get_where(array(
				'ProductUID' => $Product->ProductUID,
				'OrderID' => $this->_Order->OrderID
			));

			foreach($Product->Attributes as &$Attribute){
				$Attribute->Info = $this->_CI->ProductAttributes_model->get_one_where('ProductAttributeID', $Attribute->AttributeID);
				if($Attribute->Info->AttributeKey == 'papertypes' || $Attribute->Info->AttributeKey == 'coverpapertypes'){
					$PaperType = $this->_CI->PaperTypes_model->get_one_where('PaperTypeID', $Attribute->Value);
					$Attribute->Info->PaperType = $PaperType;
				}
				$Attribute->Pricing = $this->_CI->ProductAttributeValues_model->get_where('ProductAttributeID', $Attribute->AttributeID);
			}

			$Product->FileUpload = $this->_CI->Order_FileUploads_model->get_where('OrderProductID', $Product->OrderProductID);

			$productsInfo[$Product->ProductUID] = $Product;
		}

		$this->_Products = $productsInfo;

		return $productsInfo;
	}

	public function save($Order){
		$fields = $this->_CI->Order_model->to_array();

		$OrderArray = $Order->to_array();

		$mergedArray = array_intersect_key($OrderArray, $fields);

		$this->_CI->Order_model->populate($mergedArray);
		$this->_CI->Order_model->save();
	}

	public function save_product($Product){
		//return the public fields from this array
		$fields = $this->_CI->Order_Products_model->to_array();

		//get my product array
		$ProductArray = $Product->to_array();

		//combine the two
		$mergedArray = array_intersect_key($ProductArray, $fields);

		$this->_CI->Order_Products_model->populate($mergedArray);
		$this->_CI->Order_Products_model->save();
	}

	public function get_total(){
		$total = 0;

		foreach($this->Products as $Product){
			$pTotal = 0;

			$quantity = $useQuantity = $Product->Quantity;

			if($Product->NumSheets){
				$NumSheets = $Product->NumSheets;

				if($Product->Info->ProductCategoryID == 2){
					$NumSheets = $NumSheets / 4;
				} else {
					if($Product->is_double_sided()){
						$NumSheets = $NumSheets / 2;
					}
				}
			} else {
				$NumSheets = 0;
			}

			if($NumSheets){
				$useQuantity *= $NumSheets;
			}

			$i = 0;
			foreach($Product->Pricing as $Price){
				if($Price->Quantity > $useQuantity && ($pTotal > 0 || $i === 0)){
					break;
				} else {
					$pTotal = $useQuantity * $Price->UnitPrice;
				}
				$i++;
			}

			$paperMult = 1;
			$coverPaperMult = 1;
			$sizeMult = 1;

			$attributeTotal = 0;

			foreach($Product->Attributes as $Attribute){
				switch($Attribute->Info->AttributeKey){
					case 'papertypes':
						$paperMult = $Attribute->Info->PaperType->Multiplier;
						break;
					case 'sizes':
						$sizeMult = $Product->Size->Multiplier;
						break;
					case 'holepunchmulti':
						if($NumSheets > 0){
							$useQuantity = $quantity / $NumSheets;
						}
						break;
					case 'coverpapertypes':
						$coverPaperMult = $Attribute->Info->PaperType->Multiplier;
						break;
					case 'bindings':
						switch($Attribute->Value){
							case 'holepunched':
							case 'gbc':
							case 'spiral':
							case 'spiralcalendar':
								$useQuantity = $quantity;
						}
						break;
					case 'inkcolorpages':
						if($NumSheets > 0){
							$useQuantity = $quantity * $NumSheets;
						}
						break;

				}

				foreach($Attribute->Pricing as $AttrPrice){

					//this is a special case we apply for certain attributes
					if(strstr($AttrPrice->ProductAttributeValue, '!!')){
						$case = str_replace('!!', '', $AttrPrice->ProductAttributeValue);
						switch($case){
							//if quantity then just multiply quantity by unit price
							case 'quantity':
								$attributeTotal += ($Attribute->Value * $AttrPrice->ValuePricePerUnit);
								break;
						}
					} else if($AttrPrice->ProductAttributeValue == $Attribute->Value){
						if($AttrPrice->Multiplier && $AttrPrice->ValuePricePerUnit){
							$attributeTotal += $AttrPrice->ValuePricePerUnit * ($useQuantity * $AttrPrice->Multiplier);
						} else if($AttrPrice->Multiplier){
							$attributeTotal += ($pTotal * $AttrPrice->Multiplier);
						} else {
							$attributeTotal += $useQuantity * $AttrPrice->ValuePricePerUnit;
						}
					}
				}
			}

			//check for bw only pages
			if($Product->NumSheetsBW && $Product->NumSheetsBW > 0){
				$bwVal = $Product->NumSheetsBW;
				//hating this hardcoded value below.. it'll do for now
				$attributeTotal += $bwVal * $quantity * -.02;
			}

			$pTotal += $attributeTotal;

			if($Product->Info->ProductCategoryID == 2 && $Product->NumSheets && $Product->NumSheets > 0){
				$pTotal += $quantity * ($NumSheets * ($sizeMult - 1));
				$pTotal += $quantity * ($NumSheets * ($paperMult - 1));

				$pTotal += ($quantity * ($coverPaperMult - 1));
			} else {
				$pTotal *= $paperMult;
				$pTotal *= $sizeMult;
			}

			$total += $pTotal;
		}

		return $total;
	}

	public function save_attributes($attributes, $puid){
		$this->_CI->Order_Attributes_model->delete_where(array(
			'OrderID' => $this->_Order->OrderID,
			'ProductUID' => $puid
		));

		$this->_CI->Order_Attributes_model->insert_batch($attributes);
	}

	public function get_order_product_attributes(){
		return $this->_CI->Order_Attributes_model->get_where('OrderID', $this->_Order->OrderID);
	}

	public function get_weight(){
		$Products = $this->get_products();
		$weight = 0;

		foreach($Products as $Product){
			foreach($Product->Attributes as $Attribute){
				if($Attribute->Info->AttributeKey != 'papertypes' && $Attribute->Info->AttributeKey != 'coverpapertypes'){
					continue;
				}

				$weight += $Attribute->Info->PaperType->Weight * $Product->Size->WeightFactor * $Product->Quantity;
			}
		}

		return $weight;
	}

	public function get_shipping_rates(){
		$ShippingRates = $this->_CI->Order_ShippingRates_model->get_where('OrderID', $this->_Order->OrderID);

		return $ShippingRates;
	}

	public function get_attribute_value($puid, $attrid){
		if(!$this->order_created()){
			return false;
		}

		$Product = $this->get_product($puid);

		foreach($Product->Attributes as $Attribute){
			if($Attribute->AttributeID == $attrid){
				return $Attribute->Value;
			}
		}

		return false;
	}
}
