<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Moxi404 extends MY_Controller {

	public function index(){
		$this->load->model('PageElements_model');

		$endpoint = $this->uri->segment(1);

		if($endpoint == 'admin'){
			redirect('admin/upgrading');
			exit;
		}

		/*if(in_array(substr_replace($endpoint, '', -4), array('singlepage-quote', 'instant-quote', 'quote-single-page', 'viewcart', 'upload', 'quote-multipage-other', 'checkout_payment', 'quote-multipage-folded'))){
			redirect('order', 'location', 301);
		}

		if(substr_replace($endpoint, '', -4) == 'account'){
			redirect('users/login', 'location', 301);
		}

		if(substr($endpoint, -3) == 'php'){
			$newpage = $this->PageElements_model->get_one_where('PreviousUrl', $endpoint);

			$redirect = 'page/' . strtolower($newpage->Key);

			redirect($redirect, 'location', 301);
			exit;
		}*/

		header("HTTP/1.0 404 Not Found");

		$this->_data['products'] = $this->Products_model->get_products();

		$this->_data['product_categories'] = $this->Products_model->get_product_categories();

		$this->_data['content'] = $this->load->view('404', $this->_data, true);
		$this->_data['content'] .= $this->load->view('quote', $this->_data, true);

		$this->load->view('template', $this->_data);
	}
}