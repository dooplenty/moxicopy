<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends CI_Controller {

    public $Order;

    public function Payment(){
        parent::__construct();

        $this->load->model('PaymentTransactions_model');

        $orderid = $this->session->userdata('OrderID');
        $this->order_manager->init($orderid);

        if(!empty($this->order_manager->OrderDate) && $this->order_manager->StatusID != 7){
            $this->session->set_flashdata('warning', 'This order has already been completed. You should receive a confirmation email shortly. If you do not please contact customer service at ' . SUPPORT_NUMBER . ' to verify your order completed successfully.');
            redirect('order/confirmation');
        }
    }

    public function index(){
        $this->Order = $this->order_manager->get_order();

        if(!$this->input->post('paymentType')){
            $this->session->set_flashdata('error', 'Please select a payment type.');
            redirect('order/checkout');
            exit;
        }

        if($this->input->post('rushorder')){
            $this->Order->RushOrder = 1;
        } else {
            $this->Order->RushOrder = 0;
        }

        if($this->input->post('hardcopyproof')){
            $this->Order->HardCopyProof = 1;
        } else {
            $this->Order->HardCopyProof = 0;
        }

        $this->Order->PaymentTypeID = $this->input->post('paymentType');
        $this->Order->ShippingKey = $this->input->post('shippingOptions');
        $this->Order->Subtotal = $this->input->post('subtotal');
        $this->Order->StatusID = 7; //payment attempted

        switch($this->input->post('paymentType')){
            case '1':
                $this->payment_auth();
                break;
            case '3':
                $this->payment_paypal();
                break;
            case '2':
                $this->payment_later();
                break;
        }
    }

	public function payment_auth(){
		$config = $this->load->config('authorize');
		$this->load->library('Authorize_net');

        $user = $this->session->userdata('user');

        $exp = $this->input->post('ccExpMonth') . $this->input->post('ccExpYear');

        $total = $this->_get_total_amount();
        $this->Order->Total = $total;

        $this->order_manager->save($this->Order);

		// Lets do a test transaction
        $this->authorize_net->add_x_field('x_first_name', $this->input->post('CCFirstName'));
        $this->authorize_net->add_x_field('x_last_name', $this->input->post('CCLastName'));
        $this->authorize_net->add_x_field('x_address', $this->input->post('Address'));
        $this->authorize_net->add_x_field('x_city', $this->input->post('City'));
        $this->authorize_net->add_x_field('x_state', $this->input->post('State'));
        $this->authorize_net->add_x_field('x_zip', $this->input->post('Zip'));
        $this->authorize_net->add_x_field('x_country', 'US');
        $this->authorize_net->add_x_field('x_card_num', $this->input->post('CardNumber'));
        $this->authorize_net->add_x_field('x_amount', $total);
        $this->authorize_net->add_x_field('x_exp_date', $exp);    // MMYY
        $this->authorize_net->add_x_field('x_card_code', $this->input->post('CVV'));

        // $user = $this->session->userdata('user');
        // if(!empty($user->Email)){
        //     $this->authorize_net->add_x_field('x_email', $user->Email);
        // }

        // if(!empty($user->Phone)){
        //     $this->authorize_net->add_x_field('x_phone', $user->Phone);
        // }

        /**
         * Use credit card number 4111111111111111 for a god transaction
         * Use credit card number 4111111111111122 for a bad card
         */
        $startTime = microtime(true);
        $this->authorize_net->process_payment();
        $endTime = microtime(true);

        $authnetresponse = $this->authorize_net->get_all_response_codes();

        try {
            $PayModel = $this->PaymentTransactions_model;
            $PayModel->OrderID = $this->Order->OrderID;
            $PayModel->Result = $authnetresponse['Response_Code'];
            $PayModel->ResultText = $authnetresponse['Response_Reason_Text'];
            $PayModel->Amount = $authnetresponse['Amount'];
            $PayModel->TransactionData = json_encode($authnetresponse);
            $PayModel->DateTimeAdded = date('Y-m-d H:i:s');
            $PayModel->TransactionLength = $endTime - $startTime;
            $PayModel->PaymentTypeID = $this->Order->PaymentTypeID;
            $PayModel->save();
        } catch(Exception $e){
            echo $e->getMessage();
        }

        if($authnetresponse['Response_Code'] == '1') {
            $this->Order->StatusID = 3; //complete

            $this->Order->OrderDate = date('Y-m-d');
            $this->Order->OrderDateTime = date('Y-m-d H:i:s');
            $this->order_manager->save($this->Order);

            mail(DEVELOPER_URL, 'submitting payment', print_r($this->Order,1));
            $this->session->unset_userdata('OrderID');
            redirect('order/confirmation/' . $this->Order->OrderID);
            exit;

        } elseif($authnetresponse['Response_Code'] == '2' || $authnetresponse['Response_Code'] == '3') {
            $this->order_manager->save($this->Order);
            $this->session->set_flashdata('error', 'There was an error processing your payment: ' . $authnetresponse['Response_Reason_Text']);
            redirect('order/checkout');
            exit;
        }
	}

    /*public function payment_offline(){
        $orderID = $this->input->post('orderID');

        $Order = $this->Order_model->get_one_where('OrderID', $orderID);

        switch($this->input->post('paymentType')){
            case '1':
                $config = $this->load->config('authorize');
                $this->load->library('Authorize_net');

                // Lets do a test transaction
                $this->authorize_net->add_x_field('x_first_name', $this->input->post('CCFirstName'));
                $this->authorize_net->add_x_field('x_last_name', $this->input->post('CCLastName'));
                $this->authorize_net->add_x_field('x_address', $this->input->post('Address'));
                $this->authorize_net->add_x_field('x_city', $this->input->post('City'));
                $this->authorize_net->add_x_field('x_state', $this->input->post('State'));
                $this->authorize_net->add_x_field('x_zip', $this->input->post('Zip'));
                $this->authorize_net->add_x_field('x_country', 'US');
                $this->authorize_net->add_x_field('x_card_num', $this->input->post('CardNumber'));
                $this->authorize_net->add_x_field('x_amount', $Order->Total);
                $this->authorize_net->add_x_field('x_exp_date', $this->input->post('ccExpMonth') . $this->input->post('ccExpYear'));    // MMYY
                $this->authorize_net->add_x_field('x_card_code', $this->input->post('CVV'));

                $startTime = microtime(true);
                $this->authorize_net->process_payment();
                $endTime = microtime(true);

                $authnetresponse = $this->authorize_net->get_all_response_codes();

                try {
                    $PayModel = $this->PaymentTransactions_model;
                    $PayModel->OrderID = $Order->OrderID;
                    $PayModel->Result = $authnetresponse['Response_Code'];
                    $PayModel->ResultText = $authnetresponse['Response_Reason_Text'];
                    $PayModel->Amount = $authnetresponse['Amount'];
                    $PayModel->TransactionData = json_encode($authnetresponse);
                    $PayModel->DateTimeAdded = date('Y-m-d H:i:s');
                    $PayModel->TransactionLength = $endTime - $startTime;
                    $PayModel->PaymentTypeID = $this->input->post('paymentType');
                    $PayModel->save();
                } catch(Exception $e){
                    // echo $e->getMessage();
                }

                if($authnetresponse['Response_Code'] == '1') {
                    $Order->StatusID = 3; //complete

                    $Order->OrderDate = date('Y-m-d');
                    $Order->OrderDateTime = date('Y-m-d H:i:s');

                    $this->Order_model->save_order($Order->to_array());

                    redirect('order/offline_confirmation');
                    exit;

                } elseif($authnetresponse['Response_Code'] == '2' || $authnetresponse['Response_Code'] == '3') {
                    $this->session->set_flashdata('error', 'There was an error processing your payment: ' . $authnetresponse['Response_Reason_Text']);
                    redirect('order/checkout/' . $Order->OrderID);
                    exit;
                }

                break;
            case '3':
               $this->config->load('paypal', TRUE);

                $config = $this->config->item('paypal');

                //set some default fields
                $fields = array(
                    'rm' => '2',
                    'cmd' => '_xclick',
                    'business' => PAYPAL_BUSINESS_EMAIL,
                    'return' => PAYPAL_RETURN_URL,
                    'cancel_return' => PAYPAL_CANCEL_URL,
                    'notify_url' => PAYPAL_NOTIFY_URL,
                    'item_name' => 'Moxicopy.com - Printing Project',
                    'item_number' => $Order->OrderID,
                    'amount' => $Order->Total,
                    'payer_email' => $this->input->post('paypalEmail')
                );

                $config['fields'] = $fields;
                $config['postView'] = 'paypal_post';

                $this->config->set_item('paypal', $config);

                $this->load->library('Paypal');

                $this->paypal->submit_payment();
                break;
        }
    }*/

	public function payment_paypal(){
        $this->config->load('paypal', TRUE);

        $config = $this->config->item('paypal');

        $total = $this->_get_total_amount();
        $this->Order->Total = $total;

        //set some default fields
        $fields = array(
            'rm' => '2',
            'cmd' => '_xclick',
            'business' => PAYPAL_BUSINESS_EMAIL,
            'return' => PAYPAL_RETURN_URL,
            'cancel_return' => PAYPAL_CANCEL_URL,
            'notify_url' => PAYPAL_NOTIFY_URL,
            'item_name' => 'Moxicopy.com - Printing Project',
            'item_number' => $this->Order->OrderID,
            'amount' => $total,
            'payer_email' => $this->input->post('paypalEmail')
        );

        $config['fields'] = $fields;
        $config['postView'] = 'paypal_post';

        $this->config->set_item('paypal', $config);

        $this->load->library('Paypal');

        $this->order_manager->save($this->Order);

        $this->paypal->submit_payment();
	}

    public function paypal_notify(){
        $this->config->load('paypal', TRUE);
        $this->config->set_item('paypal_payment', 'paypal_payment');

        $this->config->set_item('notification_email', PAYPAL_BUSINESS_EMAIL);

        $this->load->library('Paypal');

        if($this->paypal->validate_ipn()){
            $ipndata = $this->paypal->get_data();

            if(!empty($ipndata) && !empty($ipndata['item_number'])){
                $Order = $this->Order_model->get_one_where('OrderID', $ipndata['item_number']);

                if(empty($Order->OrderDate)){
                    $Order->OrderDate = date('Y-m-d');
                    $Order->OrderDateTime = date('Y-m-d H:i:s');
                }
                $Order->StatusID = 3;
                $Order->save();

                mail(DEVELOPER_URL, 'Saving Paypal Order', print_r($Order,1));
            }
            //TODO  send conf email
            $this->paypal->send_payment_notification();
        } else {
            mail(DEVELOPER_URL, 'Notify DID NOT Validate', print_r($_POST,1));
        }
    }

    public function paypal_cancel() {
        $this->Order->StatusID = 5; //cancelled
        $this->Order->CancelDate = date('Y-m-d');
        $this->Order->CancelDateTime = date('Y-m-d H:i:s');

        $this->order_manager->save($this->Order);
        redirect('order/cancel/' . $this->Order->OrderID);
        exit;
    }

    public function paypal_return(){
        if($orderid = $this->session->userdata('OrderID')){
            $this->session->unset_userdata('OrderID');
            //right now we'll just redirect to confirmation since we completed order at ipn validation
            redirect('order/confirmation/' . $orderid);
            exit;
        } else {
            $this->session->set_flashdata('notification', 'Please start here by selecting your product(s).');
            redirect('order');
        }
    }

	public function payment_google(){

	}

	public function payment_later(){
        $this->_get_total_amount();
        $this->Order->StatusID = 2; //paylater
        $this->order_manager->save($this->Order);

        $this->session->set_flashdata('notification', 'Please let us know how we can help you complete your order!');
        redirect('order/confirmation/' . $this->Order->OrderID);
	}

    public function _get_total_amount(){
        $subtotal = $this->Order->Subtotal;

        $rush = 0;
        if($this->Order->RushOrder === 1){
            $percRush = $subtotal * .25;
            if($percRush < 50){
                $rush = 50;
            } else {
                $rush = $percRush;
            }
        }

        if($this->Order->ShippingKey == 'LOCALPICKUP' || in_array($this->Order->ShippingKey,array('GROUND_HOME_DELIVERY', 'FEDEX_GROUND'))){
            $shippingPrice = 0;
        } else {
            $shipping = $this->Order_model->get_shipping_rates($this->Order->OrderID, $this->Order->ShippingKey);
            if($shipping){
                $shippingPrice = $shipping->TotalNetCharge + $shipping->TotalTaxes;
            } else {
                $this->session->set_flashdata('warning', 'Make sure that you chose a shipping option. If you continue to have trouble, please call customer service so that we may complete your order accurately.');
                redirect('order/checkout');
                exit;
            }
        }

        $proof = 0;
        if($this->Order->HardCopyProof === 1){
            $proof = 15;
        }

        $total = $rush + $subtotal + $shippingPrice + $proof;

        return number_format($total, 2);

    }
}