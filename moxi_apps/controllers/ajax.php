<?php
class Ajax extends CI_Controller {
	public function index() {

	}

	public function get_product_data(){
		$productId = $this->input->post('productId');
		$this->product_manager->init($productId);
		$this->product_manager->attach_meta();

		echo json_encode($this->product_manager->get_product()->to_array());
	}
}

?>