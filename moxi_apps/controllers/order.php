<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends MY_Controller {

	public $OrderID;

	public function Order() {
		parent::__construct();

		$this->OrderID = $this->session->userdata('OrderID');

		$this->_data['keywords'] = "color printing, commercial printing, business printing, printing service, printing services, color copies, color copy, full color printing, cheap printing";
		$this->_data['description'] = "Online Service. Free Shipping, Free Proof, Free Sample Kit, Online printing does not have to be difficult or complicated, and you can save hundreds of dollars!";
	}

	public function index() {
		$this->_data['products'] = $this->Products_model->get_products();

		$this->_data['product_categories'] = $this->Products_model->get_product_categories();

		$this->_data['content'] = $this->load->view('quote', $this->_data, true);

		$this->load->view('template', $this->_data);
	}

	public function product($product_id, $uid = null) {
		if(!$product_id) {
			$this->redirect('order');
		}

		if(!is_null($uid)){
			$this->_data['product_unique'] = $uid;
		}

		//load product from model
		$this->product_manager->init($product_id, $uid);

		if(is_null($uid)){
			redirect('order/product/' . $product_id . '/' . $this->product_manager->get_product_key());
			exit;
		}
		$this->product_manager->attach_meta();

		$Product = $this->product_manager->get_product();
		$this->_data['papertypes'] = $this->PaperTypes_model->get_where("Active", "1");

		//if there is a uid we need to load the order
		if(!is_null($uid) && $this->session->userdata('OrderID')){
			$this->order_manager->init($this->session->userdata('OrderID'));
			$this->session->set_userdata('OrderID', $this->order_manager->OrderID);

			$OrderProduct = $this->order_manager->get_product($uid, $product_id);
			$FileUploads = $this->order_manager->get_fileuploads($OrderProduct->OrderProductID);
			$this->_data['FileUploads'] = $FileUploads;
			$this->_data['OrderProduct'] = $OrderProduct;
		}

		//set quote form system
		if($Product->ProductCategoryID == 1){
			$this->_data['additional_info'] = $this->load->view('_blocks/singlepage_info', $this->_data, true);
		}

		//load views
		$this->_data['preview'] = $this->load->view('_blocks/preview', $this->_data, true);
		$this->_data['pricing'] = $this->load->view('_blocks/pricing', $this->_data, true);
		$this->_data['step'] = $this->load->view('_steps/details', $this->_data, true);
		$content = $this->load->view('order', $this->_data, true);

		if(empty($this->product_manager->Meta)){
			//set meta
			switch($Product->ProductCategoryID){
				case '1':
					$keywords = "color printing, commercial printing, business printing, printing service, printing services, color copies, color copy, full color printing, cheap printing";
					$description = "Getting a quote for a print job does not have to be complicated, see how eas it can be. Instant quoting and ordering for single-page documents.";

					break;
				case '2':
					$keywords = "color printing, commercial printing, business printing, printing service, printing services, color copies, color copy, full color printing, cheap printing";
					$description = "Instant quoting and ordering for multipage stapled documents.";

					break;
				case '3':
					$keywords = "color printing, commercial printing, business printing, printing service, printing services, color copies, color copy, full color printing, cheap printing";
					$description = "Instant quoting and ordering for multipage documents. Thousands of other print customers use us, why not you?";

					break;
				case '4':
					$keywords = "color printing, commercial printing, business printing, printing service, printing services, color copies, color copy, full color printing, cheap printing";
					$description = "Instant quoting and ordering for envelopes. Find out why Thousands of print customers choose Moxicopy.com every day.";

					break;
			}
		} else {
			$keywords = $this->product_manager->Meta->MetaKeywords;
			$description = $this->product_manager->Meta->MetaDescription;
		}

		$this->_data['keywords'] = $keywords;
		$this->_data['description'] = $description;

		$this->_data['content'] = $content;
		$this->load->view('template', $this->_data);
	}

	/*public function details($product_id) {
		if(!$product_id || !$this->session->userdata('Order')) {
			$this->session->set_flashdata('notification', 'Start by selecting your product!');
			redirect('order');
		}

		$product = $this->Products_model->get_product($product_id);

		$this->_data['product'] = $product;

		$this->_data['papertypes'] = $this->Products_model->get_paper_types();

		$this->_data['preview'] = $this->load->view('_blocks/preview', $this->_data, true);

		$this->_data['pricing'] = $this->load->view('_blocks/pricing', $this->_data, true);

		$this->_data['step'] = $this->load->view('_steps/details', $this->_data, true);

		$content = $this->load->view('order', $this->_data, true);

		$this->_data['content'] = $content;

		$this->load->view('template', $this->_data);
	}*/

	public function save_details() {
		//session cart

		$product_unique = $this->input->post('product_unique');
		$product_id = $this->input->post('product');

		$this->order_manager->init($this->session->userdata('OrderID'));

		$Product = $this->order_manager->get_product($product_unique, $product_id);

		$Product->ProjectName = $this->input->post('project_name');
		$Product->ProductNotes = $this->input->post('product_notes');
		$Product->Quantity = $this->input->post('quantity');
		$Product->NumSheets = $this->input->post('numSheets');
		$Product->NumSheetsColor = $this->input->post('numSheetsColor');
		$Product->NumSheetsBW = $this->input->post('numSheetsBW');

		unset($_POST['subtotal']);
		unset($_POST['product_unique']);
		unset($_POST['quantity']);
		unset($_POST['product']);
		unset($_POST['project_name']);
		unset($_POST['numSheets']);
		unset($_POST['numSheetsColor']);
		unset($_POST['numSheetsBW']);
		unset($_POST['product_notes']);

		$product_size = 0;
		$attr_info = array();
		$batchAttr = array();

		foreach($_POST AS $key => $val){
			$attrKey = explode("_", $key);
			$attrID = array_pop($attrKey);
			$key = array_pop($attrKey);

			// we had a special case
			if(count($attrKey) > 0){
				$attrID = $attrID . '_' . $key;
				$key = array_pop($attrKey);
			}

			if(!empty($val)){
				$attr_info['AttributeID'] = $attrID;
				$attr_info['ProductUID'] = $Product->ProductUID;
				$attr_info['OrderID'] = $Product->OrderID;

				if(is_array($val)){
					foreach($val as $nVal){
						$attr_info['Value'] = $nVal;
					}
				} else {
					$attr_info['Value'] = $val;
				}

				if($key == 'sizes'){
					$product_size = $val;
				}

				$batchAttr[] = $attr_info;
			}
		}

		$Product->ProductSizeID = $product_size;
		$this->order_manager->save_product($Product);

		$this->order_manager->save_attributes($batchAttr, $Product->ProductUID);

		$Order = $this->order_manager->get_order();
		$Order->StatusID = 1;

		$this->session->set_userdata('OrderID', $Order->OrderID);

		if(!$this->_logged_in()){
			$Order->save();
			$this->session->set_flashdata('custom_refer', 'order/address');
			$this->session->set_flashdata('notification', 'Please login to continue your order.');
			redirect('users/login');
		} else {
			$Order->UserID = $this->user->UserID;
			$Order->save();

			redirect('order/address');
		}
	}

	public function address(){
		if(!$this->session->userdata('OrderID')){
			$this->session->set_flashdata('notification', 'Please start by selecting your product(s).');
			redirect('order');
		}

		//user should be logged in now
		if(!$this->_logged_in()){
			redirect('/users/login');
		}

		$this->_data['shipping'] = $this->load->view('_blocks/shipping', $this->_data, true);
		$this->_data['billing'] = $this->load->view('_blocks/billing', $this->_data, true);
		$this->_data['content'] = $this->load->view('address', $this->_data, true);
		$this->load->view('template', $this->_data);
	}

	public function addresspost(){
		if(!empty($_POST)){
			$billing = array();
			$shipping = array();

			foreach($_POST as $field => $val){
				if(stristr($field, 'billing')){
					$billing[str_replace('Billing', '', $field)] = $val;
				} else if(stristr($field, 'shipping')){
					$shipping[str_replace('Shipping', '', $field)] = $val;
				}
			}

			$orderId = $this->session->userdata('OrderID');

			$this->Order_model->save_order_shipping($shipping, $orderId);
			$this->Order_model->save_order_billing($billing, $orderId);

			redirect('order/checkout');
			exit;
		}

		redirect('order/address');
	}

	public function checkout($order_id = null){
		if(!is_null($order_id)){
			$this->session->set_userdata('OrderID', $order_id);
		}

		if(!$this->session->userdata('OrderID')){
			$this->session->set_flashdata('notification', 'Please start by selecting your product(s).');
			redirect('order');
			exit;
		}

		//user should be logged in now
		if(!$this->_logged_in()){
			$linkExtra = !is_null($order_id) ? '/' .$order_id : '';

			$this->session->set_userdata('custom_refer', 'order/checkout' . $linkExtra);
			$this->session->set_flashdata('notification', 'Please login to continue checking out.');
			redirect('/users/login');
			exit;
		}

		$this->order_manager->init($this->session->userdata('OrderID'));

		$this->order_manager->attach_meta();

		$total = $this->order_manager->get_total();

		//get any order attributes to be added
		// $orderAttributes = $this->Order_model->get_order_attributes();
		// $this->Order['Attributes'] = $orderAttributes;

		// $this->_get_totals();

		$totalWeight = $this->order_manager->get_weight();

		$Order = $this->order_manager->get_order();
		$Order->Subtotal = $total;
		$Order->Weight = $totalWeight;

		$this->order_manager->save($Order);

		$this->Order = $Order;

		$shippingRates = $this->_getShippingRates();

		$this->_data['ShippingRates'] = $shippingRates;
		$this->_data['cardTypes'] = $this->Order_model->get_card_types();
		$this->_data['BillingAddress'] = $this->Order_model->get_billing_address($this->Order->OrderID);
		$this->_data['Attributes'] = $this->Products_model->get_attributes();
		$this->_data['shipping'] = $this->load->view('_blocks/shipping', $this->_data, true);
		$this->_data['billing'] = $this->load->view('_blocks/billing', $this->_data, true);

		$this->_data['summary'] = $this->load->view('_blocks/summary', $this->_data, true);
		$this->_data['content'] = $this->load->view('checkout', $this->_data, true);
		$this->load->view('template', $this->_data);
	}

	public function update() {
		$post_data = $_POST;

		if(empty($post_data)){
			$this->session->set_flashdata('warning', 'There was no data submitted for processing.');


		}
	}

	public function confirmation_offline(){
		$this->session->set_flashdata('Your order completed successfully');
		redirect('order');
	}

	public function confirmation($orderid){
		$this->session->unset_userdata('OrderID');
		if(empty($orderid)){
			$this->session->set_flashdata('notification', 'Please start by selecting your product(s).');
			redirect('order');
			exit;
		}

		$this->_data['final'] = true;

		$this->order_manager->init($orderid);
		$this->order_manager->attach_meta();

		$this->Order = $this->order_manager->get_order();

		$this->Order->ShippingInfo = $this->Order_model->get_shipping_info($this->Order->OrderID);
		$this->Order->BillingInfo = $this->Order_model->get_billing_address($this->Order->OrderID);

		$this->_data['Order'] = $this->Order;

		$this->_data['summary'] = $this->load->view('_blocks/summary', $this->_data, true);
		$this->_data['content'] = $this->load->view('confirmation', $this->_data, true);
		$this->load->view('template', $this->_data);
	}

	public function cancel(){
		$this->session->set_flashdata('warning', 'Your order has been cancelled. Login and visit your order manager at any time to complete your order.');
		redirect('order');
		exit;
	}

	public function remove($product_uid){
		$this->Order_Products_model->delete_where(array(
			'OrderID' => $this->session->userdata('OrderID'),
			'ProductUID' => $product_uid
		));

		$this->session->set_flashdata('notification', 'Your product has been removed.');

		$this->load->library('user_agent');
		redirect($this->agent->referrer());

	}

	protected function _getShippingRates(){

		$rates = $this->order_manager->get_shipping_rates();

		if(!empty($rates)){
			return $rates;
		}

		$loc = ENVIRONMENT == 'production' ? 'prod' : 'dev';
		$config['wsdl'] = site_url("libs/fedex/{$loc}/RateService_v13.wsdl");
		$config['password'] = FEDEX_PASSWORD;
		$config['key'] = FEDEX_AUTH_KEY;
		$config['accountNum'] = FEDEX_ACCT_NUMBER;
		$config['meterNum'] = FEDEX_METER_NUMBER;
		$config['opts'] = null;

		$shipper = array(
			'PersonName' => COMPANY_NAME,
			'CompanyName' => COMPANY_NAME,
			'PhoneNumber' => COMPANY_PHONE,
			'Address' => array(
				'StreetLines' => array(COMPANY_ADDRESS,COMPANY_ADDRESS2),
				'City' => COMPANY_CITY,
				'StateOrProvinceCode' => COMPANY_STATE,
				'PostalCode' => COMPANY_ZIP,
				'CountryCode' => 'US'
			)
		);

		$user = $this->session->userdata('user');
		$orderRecipient = $this->Order_model->get_shipping_info($this->Order->OrderID);

		$recipientAddress = array($orderRecipient->Address1);

		if(!empty($orderRecipient->Address2)){
			array_push($recipientAddress, $orderRecipient->Address2);
		}

		$recipient = array(
			'PersonName' => $orderRecipient->FirstName . (!empty($orderRecipient->LastName) ? ' ' . $orderRecipient->LastName : ''),
			'CompanyName' => $user->Organization,
			'PhoneNumber' => $user->Phone,
			'Address' => array(
				'StreetLines' => $recipientAddress,
				'City' => $orderRecipient->City,
				'State' => $orderRecipient->State,
				'PostalCode' => $orderRecipient->Zip,
				'Residential' => true,
				'CountryCode' => 'US'
			)
		);

		$request = array();
		$request['Shipper'] = $shipper;
		$request['Recipient'] = $recipient;
		$request['Packaging'] = 'YOUR_PACKAGING';
		$request['AccountNumber'] = FEDEX_ACCT_NUMBER;
		$request['ShippingPaymentType'] = 'SENDER';
		$request['packageCount'] = count($this->order_manager->get_products());
		$request['Weight'] = $this->Order->Weight;

		try {
			$this->load->library('Fedex', $config);
			$response = $this->fedex->getShippingRates($request);
		} catch(Exception $e){
			//handle exception
			mail('brantley@dooplenty.com', 'Fedex Error', $e->getMessage());
		}

		$rateReplyDetails = $response->RateReplyDetails;

		if(!empty($rateReplyDetails)){
			$rate = array();
			$rate['OrderID'] = $this->Order->OrderID;
			$rate['HighestSeverity'] = $response->HighestSeverity;
			$rate['Notifications'] = json_encode($response->Notifications);

			if(count($rateReplyDetails) === 1){
				$rateReplyDetails = array($rateReplyDetails);
			}

			foreach($rateReplyDetails as $detail){
				$rate['ServiceType'] = $detail->ServiceType;
				$rate['PackagingType'] = $detail->PackagingType;
				$rate['ActualRateType'] = $detail->ActualRateType;

				foreach($detail->RatedShipmentDetails as $shipmentDetails){
					if(isset($shipmentDetails->ShipmentRateDetail)){
						if($shipmentDetails->ShipmentRateDetail->RateType == $rate['ActualRateType']){
							$shipmentDetails = $shipmentDetails->ShipmentRateDetail;
						} else {
							continue;
						}
					} else if(!(isset($shipmentDetails->RateType) && $shipmentDetails->RateType == $rate['ActualRateType'])){
						continue;
					}

					$rate['TotalBillingWeight'] = $shipmentDetails->TotalBillingWeight->Value . $shipmentDetails->TotalBillingWeight->Units;
					$rate['TotalBaseCharge'] = $shipmentDetails->TotalBaseCharge->Amount;
					$rate['TotalSurcharges'] = $shipmentDetails->TotalSurcharges->Amount;
					$rate['TotalNetFedexCharge'] = $shipmentDetails->TotalNetFedExCharge->Amount;
					$rate['TotalTaxes'] = $shipmentDetails->TotalTaxes->Amount;
					$rate['TotalNetCharge'] = $shipmentDetails->TotalNetCharge->Amount;
					$rate['TotalRebates'] = $shipmentDetails->TotalRebates->Amount;
					$rate['Surcharges'] = json_encode($shipmentDetails->Surcharges);
				}

				$this->Order_model->save_shipping_rates($rate);

			}
		}

		//now get all the rates
		$rates = $this->order_manager->get_shipping_rates();

		return $rates;
	}

	/**
	 * Calculate sub total for order
	 * @return void
	 */
	protected function _get_totals(){
		$total = 0;

		//first add up product prices
		foreach($this->Order->Products as $Product){
			$pTotal= 0;

			$productInfo = $Product->Info;
			$quantity = $Product->Quantity;

			if($Product->NumSheets){

				$numSheets = $Product->NumSheets;

				if($productInfo->ProductCategoryID == '2'){
					$numSheets = $numSheets / 4;
				}
			} else {
				$numSheets = 0;
			}

			if($numSheets){
				$useQuantity *= $numSheets;
			}

			$i =0;
			foreach($productInfo->Pricing as $price){
				//if our quantity range exceeds our quantity break out to have latest price
				if($price->Quantity > $useQuantity && ($pTotal > 0 || $i === 0)){
					break;
				} else {
					$pTotal = $useQuantity * $price->UnitPrice;
				}
				$i++;
			}

			$paperMult = 1;
			$coverPaperMult = 1;
			$sizeMult = 1;

			//now loop through our attributes and apply pricing for those selected
			$attributeTotal = 0;

			foreach($productInfo->Attributes as $attribute){
				if(in_array($attribute->ProductAttributeID, array_keys($product['Attributes']))){

					switch($attribute->AttributeKey){
						case 'papertypes':
							$paperType = $this->PaperTypes_model->get_one_where('PaperTypeID', $product['Attributes'][$attribute->ProductAttributeID]);
							$paperMult = $paperType->Multiplier;
							break;
						case 'sizes':
							$size = $this->ProductSizes_model->get_one_where('ProductSizeID', $product['Attributes'][$attribute->ProductAttributeID]);
							$sizeMult = $size->Multiplier;
							break;
						case 'holepunchmulti':
							if($numSheets > 0){
								$useQuantity = $quantity / $numSheets;
							}
							break;
						case 'coverpapertypes':
							$coverpapertype = $this->PaperTypes_model->get_one_where('PaperTypeID', $product['Attributes'][$attribute->ProductAttributeID]);
							$coverPaperMult = $coverpapertype->Multiplier;
							break;
						case 'bindings':
							$val = $product['Attributes'][$attribute->ProductAttributeID];
							switch($val){
								case 'holepunched':
								case 'gbc':
								case 'spiral':
								case 'spiralcalendar':
									$useQuantity = $quantity;
							}
							break;
						case 'inkcolorpages':
							if($numSheets > 0){
								$useQuantity = $quantity * $numSheets;
								$val = $product['Attributes'][$attribute->ProductAttributeID];
							}
							break;
					}

					foreach($attribute->Pricing as $attrPrice){

						//this is a special case we apply for certain attributes
						if(strstr($attrPrice->ProductAttributeValue, '!!')){
							$case = str_replace('!!', '', $attrPrice->ProductAttributeValue);
							switch($case){
								//if quantity then just multiply quantity by unit price
								case 'quantity':
									$attributeTotal += ($product['Attributes'][$attribute->ProductAttributeID] * $attrPrice->ValuePricePerUnit);
									break;
							}
						} else if($attrPrice->ProductAttributeValue == $product['Attributes'][$attribute->ProductAttributeID]){
							if($attrPrice->Multiplier && $attrPrice->ValuePricePerUnit){
								$attributeTotal += $attrPrice->ValuePricePerUnit * ($useQuantity * $attrPrice->Multiplier);
							} else if($attrPrice->Multiplier){
								$attributeTotal += ($pTotal * $attrPrice->Multiplier);
							} else {
								$attributeTotal += $useQuantity * $attrPrice->ValuePricePerUnit;
							}
						}
					}
				} else if($attribute->AttributeKey == 'inkcolorpages'){
					// $colorVal = $product['Attributes']['color_' . $attribute->ProductAttributeID];
					$bwVal = $product['Attributes']['bw_' . $attribute->ProductAttributeID];

					// $useQuantity = $numSheets * $quantity;
					$attributeTotal += $bwVal * $quantity * -.02;
				}
			}

			$pTotal += $attributeTotal;

			if($numSheets > 0 && $productInfo->ProductCategoryID == '2'){
				$pTotal += $quantity * ($numSheets * ($sizeMult - 1));
				$pTotal += $quantity * ($numSheets * ($paperMult - 1));

				$pTotal += ($quantity * ($coverPaperMult - 1));
			} else {
				$pTotal *= $paperMult;
				$pTotal *= $sizeMult;
			}

			$total += $pTotal;
		}

		$this->Order['SubTotal'] = $total;
	}
}
