<?php

class Uploads extends MY_Controller {
	protected $_fileTypes = array(
		'images' => array(
			'png',
			'jpg',
			'jpeg'
		)
	);

	protected $_previewFileTypes = array(
		'png',
		'jpg',
		'jpeg',
		'pdf'
	);

	public function Uploads() {

		parent::__construct();

		$this->load->helper('file');

		$this->load->library('image_lib');

		$this->load->model('fileupload_model');
	}

	public function index() {
		redirect('/');
	}

	public function file() {
		$file = $this->input->file('upload');
		$user_ip = $this->session->userdata('ip_address');

		//let's see if this file already exists before we upload again
		$checkFileModel = clone $this->fileupload_model;
		$hash = hash_file('md5', $file['tmp_name']);

		//session cart
		$orderID = $this->session->userdata('OrderID') ? $this->session->userdata('OrderID') : null;

		$this->order_manager->init($orderID);
		$Order = $this->order_manager->get_order();

		//save our new order to session
		$this->session->set_userdata('OrderID', $this->order_manager->OrderID);

		//the current product being manipulated
		$product_unique = $this->input->post('product_unique');
		$productId = $this->input->post('preview_product');

		$Product = $this->order_manager->get_product($product_unique, $productId);

		$return = array('upload_info' => $_POST);

		//get the current size
		$product_size = $this->input->post('preview_size');
		$size = $this->Products_model->get_product_size($product_size);
		$Product->ProductSizeID = $product_size;

		$this->order_manager->save_product($Product);

		$existing = $this->fileupload_manager->get_existing($hash);

		if(empty($existing) || !file_exists($existing->FileLocation . $existing->NewFilename)){
			$folder = $this->fileupload_manager->get_upload_location($Product->ProductUID);

			//set up our configs
			$config['upload_path'] = $folder;
			$config['allowed_types'] = 'png|jpg|gif|pdf|doc|docx|odt';
			$config['max_size'] = 0;
			$config['encrypt_name'] = true;

			$this->load->library('upload', $config);

			//if the upload worked
			if($this->upload->do_upload('upload')) {
				$data = $this->upload->data();

				$fileuploaddata = array(
					'OriginalFilename' => $data['orig_name'],
					'Filetype' => $data['file_type'],
					'FileSize' => $data['file_size'],
					'FileLocation' => $data['file_path'],
					'NewFilename' => $data['file_name'],
					'UserIP' => $this->session->userdata('ip_address'),
					'UserID' => $this->user ? $this->user->UserID : null,
					'UploadDateTime' => date('Y-m-d H:i:s'),
					'FileHash' => $hash
				);

				$this->fileupload_manager->save_file($fileuploaddata);

				$FileUploads = $this->order_manager->get_fileupload($Product->OrderProductID, $this->fileupload_manager->FileUploadID, $this->fileupload_manager->get_rootpath());

				$return['file_upload_data'] = $data;

				//add time to end of preview for cacheing - very low res file so hit should be minimal
				$return['file'] = '/uploads/preview/' . $Product->ProductUID . '/' . $FileUploads->FileUploadID . '/' . $Product->ProductSizeID . '/' . time();

				$create_preview = $this->_create_preview_images($data['file_name'], $this->fileupload_manager->get_upload_location($Product->ProductUID), array($size));
				if(!$create_preview['success']) {
					$return['error'] = 'Error creating preview image. ' . $create_preview['error_message'];
				} else {

					$return['success'] = 'image upload successful';
					$return['fileuploadid'] = $FileUploads->FileUploadID;
					$FileUploads->PreviewPath = $folder . $create_preview['file_name'];
					$FileUploads->save();
				}

			} else {
				$return['error'] = $this->upload->display_errors();
			}
		} else {
			$FileUploads = $this->order_manager->get_fileupload($Product->OrderProductID, $existing->FileUploadID, $existing->FileLocation);

			$create_preview = $this->_create_preview_images($existing->NewFilename, $existing->FileLocation, array($size));
			if(!$create_preview['success']) {
				$return['file'] = $product_info['PreviewPath'] = '/uploads/preview_unavailable/' . $product_info['ProductSizeID'];
				$return['error'] = 'Error creating preview image. ' . $create_preview['error_message'];
				$return['upload_success'] = 1;
			} else {

				$FileUploads->PreviewPath = $existing->FileLocation . $create_preview['file_name'];

				$return['file'] = '/uploads/preview/' . $Product->ProductUID . '/' . $FileUploads->FileUploadID . '/' . $product_size . '/' . time();
				$return['success'] = 'successfully found existing file';
				$return['fileuploadid'] = $FileUploads->FileUploadID;
			}
		}

		echo json_encode($return);
	}

	public function preview($product_unique, $fileupload_id, $size){
		$orderId = $this->session->userdata('OrderID');

		$this->order_manager->init($orderId);

		//get product
		$Product = $this->order_manager->get_product($product_unique);

		if(!empty($Product)) {
			$fileUpload = $this->order_manager->get_fileupload($Product->OrderProductID, $fileupload_id);

			$size = $this->Products_model->get_product_size($size);

			if(is_null($fileUpload->PreviewPath)){
				$this->fileupload_manager->init($fileUpload->FileUploadID);

				$File = $this->fileupload_manager->get_file();

				$create_preview = $this->_create_preview_images($File->NewFilename, $File->FileLocation, array($size));

				if($create_preview['success']){
					$fileUpload->PreviewPath = $File->FileLocation . $create_preview['file_name'];
					$fileUpload->save();
				}
			}

			if(is_null($fileUpload->PreviewPath)){
				$this->preview_unavailable($size);
				return;
			}

			if(!is_file($fileUpload->PreviewPath)){
				$this->preview_unavailable($size->ProductSizeID);
				return;
			}

			$width = $size->PreviewWidth;
			$height = $size->PreviewHeight;

			$fileParts = explode('/', $fileUpload->PreviewPath);

			$filename = array_pop($fileParts);

			$filenameSplit = explode('.', $filename);
			$ext = $filenameSplit[1];

			header('Content-type: image/jpeg');
			header("Cache-Control: no-store, no-cache, must-revalidate");

			if(in_array($ext, $this->_previewFileTypes)){

				$path = implode('/', $fileParts) . "/";

				$previewPath = $path . $width . "_" . $height . "/";
				if(!file_exists($previewPath.$filename)){
					$this->_create_preview_images($filename, $path, array($size));
				}

				readfile($previewPath.$filename);
			} else {
				$this->preview_unavailable();
			}
		}
	}

	public function remove(){
		$return = array();

		$product_unique = $this->input->post('product_unique');
		$fileid = $this->input->post('fileupload');

		$this->order_manager->init($this->session->userdata('OrderID'));
		$Product = $this->order_manager->get_product($product_unique);

		$this->Order_FileUploads_model->delete_where(array(
			'OrderProductID' => $Product->OrderProductID,
			'FileUploadID' => $fileid
		));

		$return['message'] = 'Your file has been removed from this product.';

		echo json_encode($return);

	}

	public function preview_unavailable($size){
		$size = $this->Products_model->get_product_size($size);

		$width = $size->PreviewWidth;
		$height = $size->PreviewHeight;

		$img = imagecreate($width, $height);
		$background = imagecolorallocate($img, 200, 200, 200);
		$text_color = imagecolorallocate( $img, 128, 128, 128);
		imagettftext($img, 24, 0, ($width / 2) - 90, $height / 2, $text_color, "./assets/fonts/Calibri.ttf", "Preview Unavailable");

		imagejpeg($img);

		imagecolordeallocate($img, $background);
		imagecolordeallocate($img, $text_color);
		imagedestroy($img);
	}

	public function nopreview($size){
		$size = $this->Products_model->get_product_size($size);

		$width = $size->PreviewWidth;
		$height = $size->PreviewHeight;

		$img = imagecreate($width, $height);
		$background = imagecolorallocate($img, 200, 200, 200);
		$text_color = imagecolorallocate( $img, 128, 128, 128);
		imagettftext($img, 24, 0, ($width / 2) - 90, $height / 2, $text_color, "./assets/fonts/Calibri.ttf", "Upload File");

		header('Content-type: image/jpeg');
		header("Cache-Control: no-store, no-cache, must-revalidate");
		imagejpeg($img);

		imagecolordeallocate($img, $background);
		imagecolordeallocate($img, $text_color);
		imagedestroy($img);
	}

	protected function _create_preview_images($filename, $path, $sizes) {
		$return_data = array('success' => false);

		//are we working with a pdf
		if(strtolower(substr($filename, -3)) == 'pdf'){
			//set the file to be the new jpg created from the pdf
			if(!$file = $this->_create_img_from_pdf($filename, $path)){
				$return_data['error_message'] = 'There was an error converting your pdf to a jpg';

				return $return_data;
			}
		} else {

			$file = $path . $filename;

		}

		$fileParts = explode('/', $file);

		$filename = array_pop($fileParts);

		$path = implode('/', $fileParts) . "/";

		$config['image_library'] = 'gd2';
		$config['source_image'] = $file;
		$config['quality'] = '100%';

		foreach($sizes as $size) {
			$folder = $size->PreviewWidth . "_" . $size->PreviewHeight;
			$size_path = $path . $folder . "/";

			if(!is_dir($size_path)){
				mkdir($size_path, 0777, true);
			}

			$config['new_image'] = $size_path . $filename;
			$config['width'] = $size->PreviewWidth;
			$config['height'] = $size->PreviewHeight;
			$config['maintain_ratio'] = TRUE;
			$config['master_dim'] = $size->PreviewWidth < $size->PreviewHeight ? 'height' : 'width';

			$this->image_lib->initialize($config);
			if(!$this->image_lib->resize()) {
				$return_data['error_message'] = $this->image_lib->display_errors();
				return $return_data;
			}

			$return_data['success'] = true;
			$return_data['file_name'] = $filename;
			return $return_data;
		}

	}

	protected function _create_img_from_pdf($file, $path) {
		$pdffile = $path . $file;
		try {
			//only create preview from first page
			$im = new Imagick($pdffile . '[0]');
			$im->setImageColorspace(255);
			$im->setCompression(Imagick::COMPRESSION_JPEG);
			$im->setCompressionQuality(100);
			$im->setResolution(300,300);
			$im->setImageFormat('jpeg');

			$newimage = substr_replace($pdffile, 'jpg', -3);
			$im->writeImage($newimage);
		} catch (Exception $e) {
			echo $e->getMessage();
			return false;
		}

		//no "." at beginning
		return $newimage;
	}
}