<?php

class Download extends MY_Controller {

	public function Download(){
		parent::__construct();

		$this->load->helper('download');
	}
	public function index(){

	}

	public function file($fileID){
		$fileupload = $this->FileUpload_model->get_one_where('FileUploadID', $fileID);

		$loc = $fileupload->FileLocation . $fileupload->NewFilename;

		$data = file_get_contents($loc);
		$name = $fileupload->OriginalFilename;

		force_download($name, $data);
	}
}