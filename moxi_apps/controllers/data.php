<?php

class Data extends CI_Controller {
	public function wsdl($file){
		$wsdl_location = $_SERVER['DOCUMENT_ROOT'] . '/libs/fedex/';

		if(ENVIRONMENT != 'production'){
			$folder = 'prod';
		} else {
			$folder = 'prod';
		}

		$wsdl_location .= $folder . '/' . $file . '.wsdl';

		$wsdl = file_get_contents($wsdl_location);

		echo $wsdl;
	}
}