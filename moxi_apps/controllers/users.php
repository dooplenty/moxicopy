<?php

class Users extends MY_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('User_model');
	}

	public function index(){

	}

	public function login(){
		$this->_data['content'] = $this->load->view('login', $this->_data, true);

		$this->load->view('template', $this->_data);
	}

	public function logout(){
		$this->session->sess_destroy();

		// $this->session->set_userdata('notification', 'You have been logged out successfully');

		redirect('/');
	}

	public function signup(){
		if($this->User_model->save_user($_POST)){
			$this->session->set_flashdata('message', 'You have been signed up successfully. Please login using your username and password. <a href="' . site_url('users/login') . '">Login</a>');
			redirect('main/message');
		} else {
			$this->session->set_flashdata('error', 'There was an error ocurred while signing up. Please verify all form data and if you continue to experience issues contact us at ' . SUPPORT_NUMBER);
			header("Location: " . $_SERVER['HTTP_REFERER']);
			exit;
		}
	}

	public function loginpost(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$custom_refer = $this->session->userdata('custom_refer');

		if($user = $this->User_model->login($username, $password)){

			$this->_log_user($user);

			$this->_move_files();

			if($this->session->userdata('OrderID')){
				$this->order_manager->init($this->session->userdata('OrderID'));
				$Order = $this->order_manager->get_order();

				$Order->UserID = $user->UserID;

				$Order->save();

				if($custom_refer){
					$this->session->unset_userdata('custom_refer');
					redirect($custom_refer);
				} else {
					redirect('order/address');
				}
				exit;
			}

			if($custom_refer) {
				$this->session->unset_userdata('custom_refer');

				if(!$custom_refer){
					$custom_refer = 'order';
				}

				redirect($custom_refer);
				exit;
			}
			redirect('order');
		} else {
			$this->session->set_flashdata('error', 'We were unable to find an account with that username/password combination.');
			header("Location:" . $_SERVER['HTTP_REFERER']);
		}
	}
}