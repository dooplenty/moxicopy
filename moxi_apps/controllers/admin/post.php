<?php

class Post extends MY_Controller {

	public function index($post){

	}

	public function product(){
		$product_id = $this->input->post('product_id');

		if($product_id){
			//remove current attributes and we'll just insert the new ones
			$this->Product_ProductAttributes_model->delete_where('ProductID', $product_id);

			$attributes = $this->input->post('attributes');

			$sortOrder = $this->input->post('SortOrder');

			$attr_insert = array();
			foreach($attributes as $attr) {
				$attr_insert[] = array(
					'ProductAttributeID' => $attr,
					'ProductID' => $product_id,
					'SortOrder' => isset($sortOrder[$attr]) ? $sortOrder[$attr] : null
				);
			}

			if(!empty($attr_insert)){
				$this->Product_ProductAttributes_model->insert_batch($attr_insert);
			}

			$quantities = $this->input->post('quantity');
			$units = $this->input->post('perunit');

			foreach($quantities as $key => $quantity){
				if(stristr($key, 'new')){
					//this is an insert
					$PriceModel = clone $this->ProductPricing_model;
					$PriceModel->ProductID = $product_id;
					$PriceModel->Quantity = $quantity;
					$PriceModel->UnitPrice = $units[$key];

					$PriceModel->save();
				} else {
					if(empty($quantity)){
						$this->ProductPricing_model->delete_where('ProductPricingID', $key);
						continue;
					}

					//retrieve our model and update
					$PriceModel = $this->ProductPricing_model->get_one_where('ProductPricingID', $key);
					$PriceModel->Quantity = $quantity;
					$PriceModel->UnitPrice = $units[$key];

					$PriceModel->save();
				}
			}
		}

		$this->session->set_flashdata('notification', 'Product Updated Successfully');
		redirect('admin/products/' . $product_id);
		exit;
	}

	public function order(){
		$orderid = $this->input->post('orderid');

		$notes = $this->input->post('OrderNotes');

		$total = $this->input->post('Total');
		$statusid = $this->input->post('StatusID');
		$printingstatusid = $this->input->post('PrintingStatusID');

		//get hte order
		$Order = $this->Order_model->get_one_where('OrderID', $orderid);
		$Order->Total = $total;
		$Order->StatusID = $statusid;
		$Order->PrintingStatusID = $printingstatusid;
		$Order->save();

		$OrderNotes = $this->OrderNotes_model->get_one_where('OrderID', $orderid);

		if(!$OrderNotes){
			$OrderNotes = clone $this->OrderNotes_model;
		}

		$OrderNotes->Note = $notes;
		$OrderNotes->DateTimeAdded = date('Y-m-d H:i:s');
		$OrderNotes->UserID = $this->user->UserID;
		$OrderNotes->OrderID = $orderid;

		$OrderNotes->save();

		$this->session->set_flashdata('notification', 'Order Updated successfully');
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function attributes(){
		$AttributeNames = $this->input->post('AttributeName');

		foreach($AttributeNames as $AttributeID => $AttributeName){
			$ProductAttribute = $this->ProductAttributes_model->get_one_where('ProductAttributeID', $AttributeID);
			$ProductAttribute->AttributeName = $AttributeName;

			$ProductAttribute->save();

			$Values = $_POST['AttributeUnitValue'][$AttributeID];

			foreach($Values as $ValueID => $Value){
				$ProductAttributeValues = $this->ProductAttributeValues_model->get_one_where(
					array(
						'ProductAttributeID'=>$AttributeID,
						'ProductAttributeValueID' => $ValueID
					)
				);

				$ProductAttributeValues->ValuePricePerUnit = !empty($Value) ? $Value : null;
				$ProductAttributeValues->Multiplier = !empty($_POST['AttributeMultiplier'][$AttributeID][$ValueID]) ? $_POST['AttributeMultiplier'][$AttributeID][$ValueID] : null;

				$ProductAttributeValues->save();
			}

		}

		$this->session->set_flashdata('notification', 'Attributes Updated successfully');
		redirect($_SERVER['HTTP_REFERER']);
	}
}