<?php

class Dashboard extends MY_Controller {

	public function Dashboard(){
		parent::__construct();

		if(!$this->_logged_in()){
			$this->session->set_flashdata('notification', 'You must login to access the admin area.');

			$this->session->set_userdata('custom_refer', 'admin');
			redirect('users/login');
		}

		$this->load->model('User_model');
		$this->load->helper('date');

		$this->_data['css_files'][] = 'admin.css';
	}

	public function index(){
		$this->_data['content'] = $this->load->view('admin/dashboard/admin', $this->_data, true);
		$this->load->view('admin/dashboard', $this->_data);
	}

	public function orders(){
		//get orders for this user
		$Orders = $this->Order_model->get_orders_by_user($this->_data['user']->UserID);

		$this->_data['Orders'] = $Orders;

		$this->_data['content'] = $this->load->view('admin/dashboard/orders', $this->_data, true);
		$this->load->view('admin/dashboard', $this->_data);
	}

	public function all_orders(){
		if($this->_data['user']->Level < 8){
			$this->session->set_flashdata('warning', 'You do not have the correct permissions to visit that page.');
			redirect('/admin');
			exit;
		}

		$Orders = $this->order_manager->get_active_orders();

		$this->_data['Orders'] = $Orders;

		$this->_data['content'] = $this->load->view('admin/dashboard/orders', $this->_data, true);
		$this->load->view('admin/dashboard', $this->_data);
	}

	public function view_order($id){
		if(empty($id)){
			redirect('admin');
		}

		$this->order_manager->init($id);
		$this->order_manager->attach_meta();

		$Order = $this->order_manager->get_order();

		$this->Statuses_model->OrderBy = 'Status ASC';
		$Statuses = $this->Statuses_model->fetch_all();
		$PrintingStatuses = $this->PrintingStatus_model->fetch_all();

		$this->_data['Order'] = $Order;
		$this->_data['Statuses'] = $Statuses;
		$this->_data['PrintingStatuses'] = $PrintingStatuses;
		$this->_data['content'] = $this->load->view('/admin/order_summary', $this->_data, true);
		$this->load->view('admin/dashboard', $this->_data);
	}

	public function products($pid = null){
		if($this->_data['user']->Level < 9){
			$this->session->set_flashdata('warning', 'You do not have the correct permissions to visit that page.');
			redirect('/admin');
			exit;
		}

		if(!is_null($pid)){
			$product = $this->Products_model->get_one_where('ProductID', $pid);
			$product->Attributes = $this->Product_ProductAttributes_model->get_where('ProductID', $pid);
			$this->ProductPricing_model->OrderBy = 'Quantity';
			$product->Pricing = $this->ProductPricing_model->get_where('ProductID', $pid);

			$this->_data['product'] = $product;
			$attributes = $this->ProductAttributes_model->fetch_all();

			foreach($attributes as &$attribute){
				$attribute->AttributeMeta = $this->Product_ProductAttributes_model->get_one_where(array('ProductAttributeID' => $attribute->ProductAttributeID, 'ProductID' => $pid));
			}

			$this->_data['attributes'] = $attributes;

			$this->_data['content'] = $this->load->view('admin/dashboard/product', $this->_data, true);
		} else {
			$this->_data['products'] = $this->Products_model->fetch_all();
			$this->_data['product_categories'] = $this->Products_model->get_product_categories();
			$this->_data['content'] = $this->load->view('admin/dashboard/products', $this->_data, true);
		}

		$this->load->view('admin/dashboard', $this->_data);
	}

	public function attributes(){
		$attributes = $this->ProductAttributes_model->fetch_all();

		foreach($attributes as &$attribute){
			$attribute->Pricing = $this->ProductAttributeValues_model->get_where('ProductAttributeID', $attribute->ProductAttributeID);
		}

		$this->_data['attributes'] = $attributes;

		$this->_data['content'] = $this->load->view('admin/dashboard/attributes', $this->_data, true);
		$this->load->view('admin/dashboard', $this->_data);
	}

	public function upgrading(){
		$this->_data['content'] = $this->load->view('admin/upgrading', $this->_data, true);
		$this->load->view('admin/dashboard', $this->_data);
	}
}