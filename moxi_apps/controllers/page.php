<?php

class Page extends MY_Controller {
	
	public function index($page = null) {
		if(method_exists($this, $page)){
			$this->$page();
		} else {
			if(!is_null($page)) {
				$this->load->model('PageElements_model');

				$page = strtoupper($page);

				$page = $this->PageElements_model->get_one_where('Key', $page);

				$content = "";

				if(!empty($page)){
					$this->_data['page'] = $page;

					if(!empty($page->MetaTitle)){
						$this->_data['title'] = $page->MetaTitle;
					}
					$this->_data['keywords'] = $page->MetaKeywords;
					$this->_data['description'] = $page->MetaDescription;

					$content  .= $this->load->view('page', $this->_data, true);
				}

				$this->_data['products'] = $this->Products_model->get_products();

				$this->_data['product_categories'] = $this->Products_model->get_product_categories();

				$content .= $this->load->view('quote', $this->_data, true);

				$this->_data['content'] = $content;

				$this->load->view('template', $this->_data);
			}
		}
	}

	public function tools(){
		$this->load->model('PageElements_model');
		$page = $this->PageElements_model->get_one_where('Key', 'TOOLS');

		$this->_data['keywords'] = $page->MetaKeywords;
		$this->_data['description'] = $page->MetaDescription;
		$this->_data['title'] = $page->MetaTitle;

		$this->_data['content'] = $this->load->view('tools', $this->_data, true);
		$this->load->view('template', $this->_data);
	}

	public function support(){
		$this->_data['content'] = $this->load->view('support', $this->_data, true);
		$this->load->view('template', $this->_data);
	}

}