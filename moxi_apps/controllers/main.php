<?php

class Main extends MY_Controller {

	public function Main() {
		parent::__construct();

		$this->_data['products'] = $this->Products_model->get_products();

		$this->_data['product_categories'] = $this->Products_model->get_product_categories();

	}

	public function index() {
		$this->_data['pages'] = $this->PageElements_model->get_where('Key', array(
			'WHY_MOXICOPY',
			'WHY_PAY_COPY',
			'NEW_PRINTING_ERA'
		));

		$content = $this->load->view('_blocks/featured', $this->_data, true);
		$content .= $this->load->view('main', $this->_data, true);

		$this->_data['content'] = $content;

		$this->load->view('template', $this->_data);

	}

	public function message(){
		$this->_data['content'] = $this->load->view('message', $this->_data, true);
		$this->load->view('template', $this->_data);
	}
}