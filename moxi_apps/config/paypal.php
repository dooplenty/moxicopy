<?php

/**
 * Paypal config items
 */
$config['paypal_is_test_mode'] = FALSE;
$config['ipn_log_file'] = 'paypal_ipn_results.log';
$config['ipn_log'] = TRUE;