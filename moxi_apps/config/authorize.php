<?php
/*
|————————————————————————————————————-
| Authorize.net Credentials and Info
|————————————————————————————————————-
*/
$config['authorize_net_test_mode'] = FALSE; // Set this to FALSE for live processing

$config['authorize_net_live_x_login'] = AUTH_NET_LOGIN_ID;
$config['authorize_net_live_x_tran_key'] = AUTH_NET_TRANS_KEY;
$config['authorize_net_live_api_host'] = AUTH_NET_URL;


$config['authorize_net_test_x_login'] = AUTH_NET_TEST_LOGIN_ID;
$config['authorize_net_test_x_tran_key'] = AUTH_NET_TEST_TRANS_KEY;
$config['authorize_net_test_api_host'] = AUTH_NET_TEST_URL;

// Lets setup some other values so we dont have to do it everytime
// we process a transaction
$config['authorize_net_x_version'] = AUTH_NET_VERSION;
$config['authorize_net_x_type'] = AUTH_NET_TYPE;
$config['authorize_net_x_relay_response'] = 'FALSE';
$config['authorize_net_x_delim_data'] = 'TRUE';
$config['authorize_net_x_delim_char'] = '|';
$config['authorize_net_x_encap_char'] = '';
$config['authorize_net_x_url'] = 'FALSE';

$config['authorize_net_x_method'] = 'CC';

/*
—————————————————————————————————————-
*/

?>