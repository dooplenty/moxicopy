<div class='container confirmation'>
	<div class='text-info well lead'>Thank you for your order!</div>

	<div class='span10'>
		<fieldset>
			<legend>Order Info</legend>

			<div class='row'>
				<div class='span2'><h3>OrderID:</h3></div>
				<div class='span6'><h3><?= $Order->OrderID ?></h3></div>
				<div class='span2 text-right'><h5>Total: $<?= $Order->Total; ?></h5></div>
			</div>

			<div class='row'>
				<div class='span5 center'>
					<h4>Shipping Info</h4>
					<address>
						<?= $Order->ShippingInfo->Address1 ?><br />
						<?= !empty($Order->ShippingInfo->Address2) ? $Order->ShippingInfo->Address2 . '<br />': '';  ?>
						<?= $Order->ShippingInfo->City ?>, <?= $Order->ShippingInfo->State ?> <?= $Order->ShippingInfo->Zip ?>
					</address>
				</div>
				<div class='span5 center'>
					<h4>Billing Info</h4>
					<address>
						<?= $Order->BillingInfo->Address1 ?><br />
						<?= !empty($Order->BillingInfo->Address2) ? $Order->BillingInfo->Address2 . '<br />': '';  ?>
						<?= $Order->BillingInfo->City ?>, <?= $Order->BillingInfo->State ?> <?= $Order->BillingInfo->Zip ?>
					</address>
				</div>
			</div>
		</fieldset>
	</div>

	<div class='span10'>
		<?= $summary ?>
	</div>
</div>