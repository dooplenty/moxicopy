<div class='container-fluid'>
	<h3>Welcome to our Tools Center</h3>
	<div class='row-fluid'>
		<div class='span7'>
			<div class='row-fluid'>
				<div class='span12'>
					<h4>Downloads</h4>
				</div>
			</div>
			<div class='row-fluid'>
				<div class='span12'>
					<h5>PDF Creator</h5>
					<a href='http://primopdf.com'>Click Here</a> to download a PDF Creator. <?= br(2) ?>
					<span class='label label-important'>Important</span> Please be sure to embed all ICC profiles in your PDF File.
				</div>
			</div>
			<div class='row-fluid'>
				<div class='span12'>
					<h4>Easy to use print templates</h4>
					<h5 class='text-info'>8.5 X 11 Flyer</h5>
					<h5 class='muted'>EPS</h5>
					<p>
						<a href="/downloads/images/8_5x11_template.eps">
							<img src="/assets/img/illustrator.png" width="51" height="50" alt="Illustrator_button">
						</a>
						<a href="/downloads/images/8_5x11_template.eps">
							<img src="/assets/img/indesign.png" width="51" height="50" alt="indesign">
						</a>
						<a href="/downloads/images/8_5x11_template.eps">
							<img src="/assets/img/photoshop.png" width="51" height="50" alt="photoshop">
						</a>
						<a href="/downloads/images/8_5x11_template.jpg">
							<img src="/assets/img/jpg.png" width="51" height="50" alt="jpeg">
						</a>
						<a href="/downloads/images/8_5x11_template.pdf">
							<img src="/assets/img/pdf.png" width="51" height="50" alt="pdf">
						</a>
					</p>
				</div>
			</div>
			<div class='row-fluid'>
				<div class='span12'>
					<h5 class='text-info'>8.5 X 11 Tri-Fold Brochure</h5>
					<h5 class='muted'>EPS</h5>
					<p>
						<a href="/downloads/images/8.5x11_trifold(bk).eps">
							<img src="/assets/img/illustrator.png" width="51" height="50" alt="Illustrator_button">
						</a>

						<a href="/downloads/images/8.5x11_trifold(bk).eps">
							<img src="/assets/img/indesign.png" width="51" height="50" alt="indesign">
						</a>

						<a href="/downloads/images/8.5x11_trifold(bk).eps">
							<img src="/assets/img/photoshop.png" width="51" height="50" alt="photoshop">
						</a>

						<a href="/downloads/images/8.5x11_trifold(fr).jpg">
							<img src="/assets/img/jpg.png" width="51" height="50" alt="jpeg">
						</a>

						<a href="/downloads/images/8.5x11_trifold(fr).pdf">
							<img src="/assets/img/pdf.png" width="51" height="50" alt="pdf">
						</a>
					</p>
				</div>
			</div>
			<div class='row-fluid'>
				<div class='span12'>
					<h5 class='text-info'>11 X 17 Tri-Fold Brochure</h5>
					<h5 class='muted'>EPS</h5>
					<p>
						<a href="/downloads/images/trifold_17_inside.eps">
							<img src="/assets/img/illustrator.png" width="51" height="50" alt="Illustrator_button">
						</a>

						<a href="/downloads/images/trifold_17_inside.eps">
							<img src="/assets/img/indesign.png" width="51" height="50" alt="indesign">
						</a>

						<a href="/downloads/images/trifold_17_inside.eps">
							<img src="/assets/img/photoshop.png" width="51" height="50" alt="photoshop">
						</a>
						<a href="/downloads/images/trifold_17_inside.jpg">
							<img src="/assets/img/jpg.png" width="51" height="50" alt="jpeg">
						</a>

						<a href="/downloads/images/trifold_17_inside.pdf">
							<img src="/assets/img/pdf.png" width="51" height="50" alt="pdf">
						</a>
					</p>
				</div>
			</div>
			<div class='row-fluid'>
				<div class='span12'>
					<h5 class='text-info'>8.5 X 11 Gate-Fold Brochure</h5>
					<h5 class='muted'>EPS</h5>
					<p>
						<a href="/downloads/images/gatefold_11_inside.eps">
							<img src="/assets/img/illustrator.png" width="51" height="50" alt="Illustrator_button">
						</a>

						<a href="/downloads/images/gatefold_11_inside.eps">
							<img src="/assets/img/indesign.png" width="51" height="50" alt="indesign">
						</a>

						<a href="/downloads/images/gatefold_11_inside.eps">
							<img src="/assets/img/photoshop.png" width="51" height="50" alt="photoshop">
						</a>

						<a href="/downloads/images/gatefold_11_inside.jpg">
							<img src="/assets/img/jpg.png" width="51" height="50" alt="jpeg">
						</a>

						<a href="/downloads/images/gatefold_11_inside.pdf">
							<img src="/assets/img/pdf.png" width="51" height="50" alt="pdf">
						</a>
					</p>
				</div>
			</div>
			<div class='row-fluid'>
				<div class='span12'>
					<h5 class='text-info'>11 X 17 Gate-Fold Brochure</h5>
					<h5 class='muted'>EPS</h5>
					<p>
						<a href="/downloads/images/GateFold11x17-02.eps">
							<img src="/assets/img/illustrator.png" width="51" height="50" alt="Illustrator_button">
						</a>

						<a href="/downloads/images/GateFold11x17-02.eps">
							<img src="/assets/img/indesign.png" width="51" height="50" alt="indesign">
						</a>

						<a href="/downloads/images/GateFold11x17-02.eps">
							<img src="/assets/img/photoshop.png" width="51" height="50" alt="photoshop">
						</a>

						<a href="/downloads/images/GateFold11x17-02.jpg">
							<img src="/assets/img/jpg.png" width="51" height="50" alt="jpeg">
						</a>

						<a href="/downloads/images/GateFold11x17.pdf">
							<img src="/assets/img/pdf.png" width="51" height="50" alt="pdf">
						</a>
					</p>
				</div>
			</div>
			<div class='row-fluid'>
				<div class='span12'>
					<h5 class='text-info'>11 X 17 Poster</h5>
					<h5 class='muted'>EPS</h5>
					<p>
						<a href="/downloads/images/11x17_flyer_template.eps">
							<img src="/assets/img/illustrator.png" width="51" height="50" alt="Illustrator_button">
						</a>

						<a href="cdownloads/images/11x17_flyer_template.eps">
							<img src="/assets/img/indesign.png" width="51" height="50" alt="indesign">
						</a>

						<a href="/downloads/images/11x17_flyer_template.eps">
							<img src="/assets/img/photoshop.png" width="51" height="50" alt="photoshop">
						</a>

						<a href="/downloads/images/11x17_flyer_template.jpg">
							<img src="/assets/img/jpg.png" width="51" height="50" alt="jpeg">
						</a>

						<a href="/downloads/images/11x17_flyer_template.pdf">
							<img src="/assets/img/pdf.png" width="51" height="50" alt="pdf">
						</a>
					</p>
				</div>
			</div>
		</div>

		<div class='span5'>
			<div class='row-fluid'>
				<div class='span12'>
					<h4>Documentation</h4>

					<div class='row-fluid'>
						<div class='span12'>
							<h5>Full Bleed Printing</h5>
							<a href='#fullBleedModal' data-toggle='modal'><span class='act-primary'>Click Here</span></a> to learn more about Full Bleed Printing and Moxicopy.com's requirements.
						</div>
					</div>
				</div>
			</div>
			<div class='row-fluid'>
				<div class='span12'>
					<h4>Information</h4>

					<div class='row-fluid'>
						<div class='span12'>
							<p><a href='<?= site_url('page/policy') ?>'>Company Policy</a></p>
							<p><a href='<?= site_url('downloads/sitemap.xml') ?>'>Site Map</a></p>
						</div>
					</div>
				</div>
			</div>
			<div class='row-fluid'
				<div class='span12'>
					<h4>Printing Articles</h4>

					<div class='row-fluid'>
						<div class='span12'>
							<h5>Services</h5>
							<p><a href='<?= site_url('page/catalog_print_services') ?>'>Catalog Print Services</a></p>
							<p><a href='<?= site_url('page/printing_and_copying') ?>'>Printing and Copying Services</a></p>
							<p><a href='<?= site_url('page/newsletter_print_services') ?>'>Newsletter Print Services</a></p>
							<p><a href='<?= site_url('page/brochure_print_services') ?>'>Brochure Printing Services</a></p>
							<p><a href='<?= site_url('page/online_printing_service') ?>'>Online Printing Service</a></p>
							<p><a href='<?= site_url('page/digest_printing') ?>'>Digest Printing</a></p>
							<p><a href='<?= site_url('page/script_printing') ?>'>Script Printing</a></p>
							<p><a href='<?= site_url('page/manual_printing') ?>'>Manual Printing</a></p>
							<p><a href='<?= site_url('page/release_printing') ?>'>Release Printing</a></p>
							<p><a href='<?= site_url('page/pamphlet_printing') ?>'>Pamphlet Printing</a></p>
							<p><a href='<?= site_url('page/tract_printing') ?>'>Tract Printing</a></p>
							<p><a href='<?= site_url('page/circular_printing') ?>'>Circular Printing</a></p>
							<p><a href='<?= site_url('page/flyer_printing_company') ?>'>Flyer Printing Company</a></p>
							<p><a href='<?= site_url('page/printing_flyers') ?>'>Printing Flyers</a></p>
							<h5>General Info</h5>
							<p><a href='<?= site_url('page/commercial_printing_company') ?>'>Commercial Printing Company</a></p>
							<p><a href='<?= site_url('page/cheap_color_copy') ?>'>Cheap Color Copies</a></p>
							<p><a href='<?= site_url('page/copy_shop') ?>'>Copy Shop</a></p>
							<p><a href='<?= site_url('page/quality_commercial_printing') ?>'>Quality Commercial Printing</a></p>
							<p><a href='<?= site_url('page/additional_resources') ?>'>Additional Resources</a></p>
							<p><a href='<?= site_url('page/printing_capabilities') ?>'>Printing Capabilities</a></p>
							<p><a href='<?= site_url('page/offset_printing') ?>'>Offset Printing</a></p>
							<p><a href='<?= site_url('page/offset_digital') ?>'>Offset vs Digital Printing</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id='fullBleedModal' class='modal hide fade'>
	<div class='modal-header'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    	<h3>Full Bleed Requirements</h3>
	</div>

	<div class='modal-body'>
		<p>Full bleed printing is where the printing goes all the way to the edge of the page.</p>
		<p>Standard printing has a 1/8 inch border around the edge/edges of the page.   A printed document can have 1 edge up to 4 edges printed full bleed.</p>
		<p>Most commercial digital printing machines do not print all the way to the edge of the page. Full bleed printing is achieved by cutting or trimming the printed document to the finished size.  This requires the original document be designed with all edges extended 1/8 inch beyond the finished size, and printing onto oversize paper.  This allows for the trimming, without cutting into any design elements or text that needs to be retained.</p>
		<p>Full bleed printing also needs to have visible crop marks. These are marks that let the printer know where the designer wants the document to be trimmed.</p>
		<p><span class='label label-info'>Example</span> If you want a full bleed, one sided flyer with a finished size of 8.5x11 (standard paper size).  The original document needs to be designed 8.75x11.25 This allows 1/8th inch on each side of the flyer to be trimmed to a finished size of 8.5x11.</p>

		<?= img('assets/img/8_5x11_fullbleed.jpg') ?>
	</div>

	<div class='modal-footer'>
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>
</div>