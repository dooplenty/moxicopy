<section class='container'>
	<div class='center alert alert-error hide'></div>

	<div class='span10 text-center'>
		<h3>Address Information</h3>

		<form name='checkout' method='post' action='<?= site_url('order/addresspost') ?>'>
			<div class='span5 text-left'>
				<?= $billing ?>
			</div>

			<div class='span5 text-left'>
				<?= $shipping ?>
			</div>

			<?= br(2) ?>

			<input type='submit' value='Save Address' class='btn'/>
		</form>
	</div>
</section>