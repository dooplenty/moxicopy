<div class='span12'>
	<?php foreach($product_categories as $category): ?>
		<div class='span3'>
			<div class='category-title'><?= $category->ProductCategoryName ?></div>

			<ul class='product-list'>
			<?php foreach($products as $product): ?>

				<?php if($product->ProductCategoryID == $category->ProductCategoryID): ?>

					<li><a href='<?= site_url('admin/products/' . $product->ProductID) ?>'><?= $product->ProductName ?></a></li>

				<?php endif; ?>

			<?php endforeach; ?>
			</ul>

		</div>

	<?php endforeach; ?>
</div>