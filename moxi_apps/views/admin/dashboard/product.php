<form name='productManagerForm' action='<?= site_url('admin/post/product') ?>' method='post'>
	<input type='hidden' name='product_id' value='<?= $product->ProductID ?>' />

	<div id='productManager' class='span12 product-manager'>
		<div class='span3'>
			Something will go here eventually
		</div>
		<div class='span9'>
			<h3><?= $product->ProductName ?></h3>

			<h4>Attributes</h4>

			<div class='container-fluid well'>
				<div class='row-fluid'>
					<div class='span12'>
						<?php
							//how many attrs
							$attrCount = count($attributes);

							//how many cols you want them in
							$columns = 3;

							//how many will there be in each column
							$perColumn = ceil($attrCount / $columns);

							//start our counter
							$i = 0;
							$used = 1;

							//this will be our span for each column
							$span = 12 / $columns;

							//echo first span div
							echo "<div class='span{$span} form-inline'>";

							foreach($attributes as $attribute) {
								$i++;
								$checked = "";
								foreach($product->Attributes as $pAttr){
									if($pAttr->ProductAttributeID == $attribute->ProductAttributeID){
										$checked = 'checked';
									}
								}
								?>

								<label class='checkbox'>
									<input type='checkbox' name='attributes[]' value='<?= $attribute->ProductAttributeID ?>' <?= $checked ?>/>
									<?= $attribute->AttributeName ?>
								</label>

								<?php
									if(!empty($attribute->AttributeMeta)){
										?>
										<input type='text' class='input-small span2' name='SortOrder[<?= $attribute->ProductAttributeID ?>]' value='<?= $attribute->AttributeMeta->SortOrder ?>' />
										<?php
									}
								?>

								<?= br(1) ?>
								<?php

								//if our "i" count equals the per column count then end span div
								if($i == $perColumn){

									//reset i
									$i = 0;

									//if our used count is still less than total start a new span div
									if($used < $attrCount){
										echo "</div>";
										echo "<div class='span{$span} form-inline'>";
									}
								}

								$used++;
							}

							//end first span div
							echo "</div>";
						?>
					</div>
				</div>
			</div>

			<h4>Pricing <span class='btn btn-primary add-price btn-small'>Add Price</span></h4>

			<div class='container-fluid well'>
				<div class='row-fluid pricing'>
					<?php foreach($product->Pricing as $price): ?>
					<p class='pricing-block'>
						Quantity: <input type='text' name='quantity[<?= $price->ProductPricingID ?>]' value='<?= $price->Quantity ?>'/>
						Per Unit: <input type='text' name='perunit[<?= $price->ProductPricingID ?>]' value='<?= $price->UnitPrice ?>'/>
					</p>
					<?php endforeach; ?>
				</div>
			</div>

			<input type='submit' value='Save Product' class='btn btn-primary btn-large' />
		</div>
	</div>
</form>