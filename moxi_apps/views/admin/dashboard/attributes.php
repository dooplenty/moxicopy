<div class='span3'>
	<h4>Attributes Manager</h4>
</div>
<div class='span9'>
	<form name='attributesManager' action='<?= site_url('admin/post/attributes') ?>' method='post'>
		<div class='container-fluid'>
			<h6>Set Attribute Options/Pricing</h6>

			<div class='row-fluid'>
				<div class='span12'><h4>Attribute Name</h4></div>
			</div>
			<div class='row-fluid'>
				<div class='span3 text-right'><h4>Attribute Value</h4></div>
				<div class='span3'><h4>Value Per Unit Price</div>
				<div class='span6'><h4>Value Price Multiplier</div>
			</div>

			<?php foreach($attributes as $attribute): ?>
				<div class='row-fluid'>
					<div class='span12'><input type='text' name='AttributeName[<?= $attribute->ProductAttributeID ?>]' value='<?= $attribute->AttributeName ?>' class='span3' /></div>
				</div>
				<?php foreach($attribute->Pricing as $Pricing): ?>
					<div class='row-fluid'>
						<div class='span3 text-right'><?= $Pricing->ProductAttributeValue ?></div>
						<div class='span3'><input type='text' name='AttributeUnitValue[<?= $attribute->ProductAttributeID ?>][<?= $Pricing->ProductAttributeValueID ?>]' value='<?= $Pricing->ValuePricePerUnit ?>' class='span5' /></div>
						<div class='span6'><input type='text' name='AttributeMultiplier[<?= $attribute->ProductAttributeID ?>][<?= $Pricing->ProductAttributeValueID ?>]' value='<?= $Pricing->Multiplier ?>' class='span3' /></div>
					</div>
				<?endforeach; ?>
			<?php endforeach; ?>

			<input type='submit' class='btn btn-primary' value='Save Changes' />
		</div>
	</form>
</div>