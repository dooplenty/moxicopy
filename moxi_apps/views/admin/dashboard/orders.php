<div class='span2'>

</div>
<div class='span8 well'>
	<table class='table table-hover'>
		<thead>
			<th>Ordered</th>
			<th>OrderID</th>
			<th>Ordered By</th>
			<th>Payment Type</th>
			<th>Payment Status</th>
			<th>Amount</th>
			<th>Status</th>
		</thead>
		<tbody>
			<?php
			foreach($Orders as $Order){
				$current = now();
				$orderDate = !is_null($Order->OrderDateTime) ? $Order->OrderDateTime : $Order->DateTimeAdded;
				$orderDate = strtotime($orderDate);

				$diff = timespan($orderDate, $current);

				$rush = $Order->RushOrder === '1' ? " <span class='label label-important'><b>Rush Order!</b></span>" : "";

				?>
				<tr>
					<td><?= $diff ?>  ago<?= $rush ?></td>
					<td><a href='<?= site_url('admin/view_order/' . $Order->OrderID) ?>'><?= $Order->OrderID ?></a>&nbsp;</td>
					<td><?= $Order->UserFirstName . ' ' . $Order->UserLastName ?>&nbsp;</td>
					<td><?= $Order->PaymentTypeID ?>&nbsp;</td>
					<td><?= $Order->StatusKey ?>&nbsp;</td>
					<td>$<?= number_format($Order->Total, 2) ?>&nbsp;</td>
					<td><?= $Order->PrintingStatusKey ?>&nbsp;</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<div class='span2'>

</div>