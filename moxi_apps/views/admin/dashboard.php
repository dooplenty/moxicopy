<?= doctype() ?>
<html>
	<head>
		<!-- Prevent Updowner from indexing site -->
		<meta name="Updownerbot" content="noindex">

		<!-- tell the bots NOT to follow admin pages -->
		<meta name="robots" content="no-index, no-follow" />
		<meta name="googlebot" content="no-index, no-follow" />

		<!-- fit viewport to screen size - important for mobile devices -->
		<meta name="viewport" content="width=device-width">

		<?php
			if(isset($css_files)) {
				foreach($css_files as $css) {
					echo link_tag(CSS_LOCATION . $css);
				}
			}
		?>

		<title>Moxicopy.com - Admin</title>

		<script src="<?= SCRIPTS_LOCATION ?>modernizr.js"></script>
	</head>
	<body class='admin'>
		<div class='navbar navbar-fixed-top'>
			<div class='navbar-inner'>
				<div class='container-fluid'>
					<div class='span12'>
						<div class='nav-collapse collapse'>
							<ul class='nav'>
								<li class='dropdown'>
									<a class='dropdown-toggle admin-logo' data-toggle='dropdown' href='#'>
										<?= img('assets/img/logo_small.png') ?>
										<b class='caret'></b>
									</a>

									<ul class='dropdown-menu'>
										<li>
											<a href='<?= site_url() ?>'>Return to site</a>
										</li>
										<li>
											<a href='<?= site_url('admin') ?>'>Dashboard</a>
										</li>
									</ul>
								</li>

								<li class='dropdown'>
									<a class='dropdown-toggle' data-toggle='dropdown' href='#'>
										Orders
										<b class='caret'></b>
									</a>

									<ul class='dropdown-menu'>
										<li>
											<a href='<?= site_url('admin/orders') ?>'>View My Orders</a>
										</li>
										<?php if($user->Level >= '8'): ?>
										<li>
											<a href='<?= site_url('admin/all_orders') ?>'>View All Orders</a>
										</li>
										<?php endif; ?>
									</ul>
								</li>
								<li>
									<a href='<?= site_url('admin/account') ?>'>View Account</a>
								</li>
								<li>
									<a href='<?= site_url('admin/files') ?>'>Manage Files</a>
								</li>
								<?php if($user->Level === '9'): ?>
								<li>
									<a href='<?= site_url('admin/products') ?>'>Manage Products</a>
								</li>
								<?php endif; ?>
								<?php if($user->Level === '9'): ?>
								<li>
									<a href='<?= site_url('admin/attributes') ?>'>Manage Attributes</a>
								</li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="main_container" class='container-fluid'>

			<?php $this->load->view('notifications') ?>

			<div class='row-fluid'>
				<?= $content ?>
			</div>
		</div>
		<script data-main='/assets/js/admin.js' type='text/javascript' src='/assets/js/require-jquery.js'></script>
	</body>
</html>