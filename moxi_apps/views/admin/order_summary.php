<form name="viewOrderForm" action="<?= site_url('admin/post/order') ?>" method='post'>
	<input type='hidden' name='orderid' value='<?= $Order->OrderID ?>' />

	<div id='main_container' class='container-fluid'>
		<div class='span3'>
			<div class='row-fluid'>
				<div class='span12'>
					<p class='text-info'>Click below to save order changes</p>
					<input type='submit' value='Save Changes' class='btn btn-success' />
				</div>
			</div>
			<div class='row-fluid'>
				<div class='span12'>
					<?= br(2) ?>
					<p class='text-info'>This link takes you to offline payments. Right-click and copy to send</p>
					<a href='<?= site_url('order/checkout/' . $Order->OrderID) ?>' class='btn btn-primary'>Payment Link</a>
				</div>
			</div>
		</div>
		<div class='span9'>
			<div class='container'>
				<div class='row-fluid well'>
					<div class='span12'>
						<ul class='nav nav-tabs'>
							<li class='active'><a data-toggle="tab" href='#orderInfo'>Order Info</a></li>
							<li><a data-toggle="tab" href='#productInfo'>Product Info</a></li>
							<li><a data-toggle="tab" href='#contactInfo'>Contact Info</a></li>
							<li><a data-toggle="tab" href='#shippingInfo'>Shipping Info</a></li>
						</ul>
						<div class='tab-content'>
							<div class='tab-pane active container' id='orderInfo' class='clearfix'>
								<div class='row-fluid'>
									<div class='span10'>
										<p class='lead'><?= $Order->ProjectName ?></p>
									</div>
									<div class='span2 text-right'>
										<p class='lead'><b>Total: <?= number_format($Order->Total, 2) ?></b></p>
									</div>
								</div>
								<div class='row-fluid'>
									<div class='span10'>
										<div class='container'>
											<div class='row-fluid'>
												<div class='span6 text-center'>
													<h5 class='muted'>Admin Order Notes</h5>
													<textarea class='span10' rows='12' name="OrderNotes" placeholder="Enter Admin Notes"><?= $Order->Notes ? $Order->Notes->Note : '' ?></textarea>
												</div>
											</div>
										</div>
									</div>
									<div class='span2 text-right'>
										<p><small>Edit Total:</small> <input class='span5'type='text' name='Total' value='<?= $Order->Total ?>' /></p>
										<p><b>SubTotal:</b> <?= number_format($Order->Subtotal, 2) ?></p>

										<?php if(!empty($Order->ShippingRate)): ?>
											<p><a href='#' class='act-primary pop-over' data-content="**Total doesn't show FREE shipping** <br /><b>Type</b>: <?= $Order->ShippingRate->ServiceType ?><br /><b>Weight:</b> <?= $Order->ShippingRate->TotalBillingWeight ?><br /><b>Net Charge</b> $<?= $Order->ShippingRate->TotalNetCharge ?>" data-placement="top" data-toggle="popover" data-original-title="Shipping Rate Info">Shipping:</a> $<?= number_format($Order->ShippingRate->TotalNetCharge, 2) ?></p>
										<?php endif; ?>
										<p>
											<b>Order Status</b><?= br(1) ?>
											<select name='StatusID' class='span12'>
												<?php foreach($Statuses as $Status): ?>
												<option value='<?= $Status->StatusID ?>' <?= $Status->StatusID == $Order->StatusID ? 'selected' : '' ?>><?= $Status->StatusKey ?></option>
												<?php endforeach; ?>
											</select>
										</p>
										<p>
											<b>Print Status</b><?= br(1) ?>
											<select name='PrintingStatusID' class='span12'>
												<option value=''>Printing Status...</option>
												<?php foreach($PrintingStatuses as $PrintingStatus): ?>
												<option value='<?= $PrintingStatus->PrintingStatusID ?>' <?= $PrintingStatus->PrintingStatusID == $Order->PrintingStatusID ? 'selected' : '' ?>><?= $PrintingStatus->PrintingStatusKey ?></option>
												<?php endforeach; ?>
											</select>
										</p>
									</div>
								</div>
							</div>
							<div class='tab-pane' id='productInfo' class='clearfix'>
								<?php foreach($Order->Products as $product): ?>
									<fieldset>
										<legend><?= $product->Info->ProductName ?></legend>

										<p><b>Quantity:</b> <?= $product->Quantity ?></p>
										<p><b>Size:</b> <?= $product->Size->SizeX ?> X <?= $product->Size->SizeY ?> </p>
										<?php if(!empty($product->NumSheets)): ?>
											<p><b>Number of Sheets:</b> <?= $product->NumSheets ?>
											<p><b>Number of Sheets Color:</b> <?= $product->NumSheetsColor ?>
											<p><b>Number of Sheets BW:</b> <?= $product->NumSheetsBW ?>
										<?php endif; ?>

										<h4>Attributes</h4>
										<?php foreach($product->Attributes as $attribute): ?>
											<?php if($attribute->Info->AttributeKey == 'sizes'){
												continue;
											}
											?>
											<p><b><?= $attribute->Info->AttributeName ?>: </b><?= isset($attribute->Info->PaperType) ? $attribute->Info->PaperType->PaperTypeName . ' ' . $attribute->Info->PaperType->PaperTypeCategory . ' ' . $attribute->Info->PaperType->PaperTypeDescription : $attribute->Value ?></p>
										<?php endforeach; ?>

										<h4>Product Notes</h4>
										<p><?= $product->ProductNotes ?></p>

										<?php foreach($product->FileUpload as $File): ?>
											<p>
												<a class='btn btn-primary btn-small' href='<?= site_url('download/file/' . $File->FileUploadID) ?>' target='_blank' />Download File</a>
											</p>
										<?php endforeach; ?>
									</fieldset>
								<?php endforeach; ?>
							</div>
							<div class='tab-pane' id='contactInfo'>
								<div class='row-fluid'>
									<div class='span4'>
										<b>Name:</b>
									</div>
									<div class='span8'>
										<?= $Order->User->FirstName ?> <?= $Order->User->LastName ?>
									</div>
								</div>
								<div class='row-fluid'>
									<div class='span4'>
										<b>Address 1:</b>
									</div>
									<div class='span8'>
										<?= $Order->User->Address1 ?>
									</div>
								</div>
								<div class='row-fluid'>
									<div class='span4'>
										<b>Address 2:</b>
									</div>
									<div class='span8'>
										<?= $Order->User->Address2 ?>
									</div>
								</div>
								<div class='row-fluid'>
									<div class='span4'>
										<b>City:</b>
									</div>
									<div class='span8'>
										<?= $Order->User->City ?>
									</div>
								</div>
								<div class='row-fluid'>
									<div class='span4'>
										<b>State:</b>
									</div>
									<div class='span8'>
										<?= $Order->User->State ?>
									</div>
								</div>
								<div class='row-fluid'>
									<div class='span4'>
										<b>Zip:</b>
									</div>
									<div class='span8'>
										<?= $Order->User->Zip ?>
									</div>
								</div>
								<div class='row-fluid'>
									<div class='span4'>
										<b>Organization:</b>
									</div>
									<div class='span8'>
										<?= $Order->User->Organization ?>
									</div>
								</div>
								<div class='row-fluid'>
									<div class='span4'>
										<b>Phone:</b>
									</div>
									<div class='span8'>
										<?= $Order->User->Phone ?>
									</div>
								</div>
								<div class='row-fluid'>
									<div class='span4'>
										<b>Email:</b>
									</div>
									<div class='span8'>
										<?= $Order->User->Email ?>
									</div>
								</div>
							</div>
							<div class='tab-pane' id='shippingInfo'>
								<div class='row-fluid'>
									<div class='span4'>
										<b>Name:</b>
									</div>
									<div class='span8'>
										<?= $Order->Shipping->FirstName ?> <?= $Order->Shipping->LastName ?>
									</div>
								</div>
								<div class='row-fluid'>
									<div class='span4'>
										<b>Address 1:</b>
									</div>
									<div class='span8'>
										<?= $Order->Shipping->Address1 ?>
									</div>
								</div>
								<div class='row-fluid'>
									<div class='span4'>
										<b>Address 2:</b>
									</div>
									<div class='span8'>
										<?= $Order->Shipping->Address2 ?>
									</div>
								</div>
								<div class='row-fluid'>
									<div class='span4'>
										<b>City:</b>
									</div>
									<div class='span8'>
										<?= $Order->Shipping->City ?>
									</div>
								</div>
								<div class='row-fluid'>
									<div class='span4'>
										<b>State:</b>
									</div>
									<div class='span8'>
										<?= $Order->Shipping->State ?>
									</div>
								</div>
								<div class='row-fluid'>
									<div class='span4'>
										<b>Zip:</b>
									</div>
									<div class='span8'>
										<?= $Order->Shipping->Zip ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>