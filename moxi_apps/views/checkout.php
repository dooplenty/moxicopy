<div class='container'>
	<form name='checkoutForm' action="<?= site_url('payment') ?>" method="post">
		<div class='container'>
			<div class='span10 checkout'>
				<?= $summary ?>

				<fieldset>
					<legend>Additional Items</legend>
					<?php
						$rushPrice = 50;

						if($rushPrice < ($this->order_manager->SubTotal * .25) && $rushPrice > 0){
							$rushPrice = $this->order_manager->SubTotal * .25;
						}

						if($rushPrice > 0){
							?>
							<label class='checkbox'>
								<input type='checkbox' name='hardcopyproof' <?= $this->order_manager->HardCopyProof === '1' ? 'checked=true' : '' ?> />
								Hard Copy Proof?
							</label>
							<label class='checkbox'>
								<input type='checkbox' name='rushorder' <?= $this->order_manager->RushOrder === '1' ? 'checked="true"' : '' ?> />
								Rush Order?
							</label>

							<div class='alert'>
								<p><span class='label label-default'>24HR RUSH SERVICES</span><br /> Fully Paid and accurately submitted RUSH SERVICE jobs will be printed within 24 hours of job submission and shipped the following day. EXAMPLE; Job submitted any time on Monday will be printed and shipped by Tuesday. RUSH JOBS submitted on weekends will be considered Monday submissions and will be shipped on Tuesday.</p>
								<p><span class='label label-important'>Please Note</span> <br />Rush services are not available for all products and quantities. Products with extended finishing options or complex jobs may not be eligible. Please contact a customer service representative to verify rush capability.</p>
							</div>

							<input type='hidden' name='rushPrice' value='<?= $rushPrice ?>' />

							<?= br(1) ?>
							<?php
						}
					?>
				</fieldset>

				<fieldset>
					<legend>Shipping Info</legend>

					<label for='shippingOptions' class='select'><b>Shipping Option</b></label>
					<select name='shippingOptions' class='span3'>
						<option value=''>Choose shipping option...</option>
						<option value='LOCALPICKUP' <?= 'LOCALPICKUP' == $this->order_manager->ShippingKey ? 'selected ' : '' ?>>LOCAL PICKUP - $0.00</option>
						<?php
							foreach($ShippingRates as $shippingRate){
								if(in_array($shippingRate->ServiceType, array('FIRST_OVERNIGHT', 'PRIORITY_OVERNIGHT', 'FEDEX_2_DAY_AM'))){
									continue;
								}

								if(in_array($shippingRate->ServiceType,array('GROUND_HOME_DELIVERY','FEDEX_GROUND'))){
									$shippingPrice = 'FREE!';
								} else {
									$shippingPrice = $shippingRate->TotalNetCharge;
								}
								?>
								<option <?= $shippingRate->ServiceType == $this->order_manager->ShippingKey ? 'selected ' : '' ?>value='<?= $shippingRate->ServiceType ?>'><?= str_replace('_', ' ', $shippingRate->ServiceType) . (is_numeric($shippingPrice) ? ' - $' . number_format($shippingPrice, 2) : ' - ' . $shippingPrice) ?></option>
								<?php
							}
						?>
					</select>
				</fieldset>

				<fieldset>
					<legend>Payment Information</legend>

					<select name='paymentType'>
						<option value=''>Choose Payment Type</option>
						<?php
							$payTypes = array(
								'3' => 'PayPal',
								'1' => 'Credit Card',
								'2' => 'Pay Later'
							);

							foreach($payTypes as $payKey => $payVal) {
								?>
								<option <?= $this->order_manager->PaymentTypeID == $payKey ? 'selected ' : '' ?>value='<?= $payKey ?>'><?= $payVal ?></option>
								<?php
							}
						?>
					</select>
					<?= br(1) ?>
					<div id='paypal-payment' class='form-horizontal well' style='display:none'>
						<div class='control-group'>
							<label class='control-label' for='paypalEmail'>Paypal Email</label>

							<div class='controls'>
								<input type='text' name='paypalEmail' placeholder='Paypal Email Address' />
							</div>
						</div>
					</div>
					<div id='cc-payment' class='form-horizontal well' style='display:none'>
						<div class='control-group'>
							<label class='control-label' for='CCName'>Credit Card Name</label>
							<div class='controls'>
								<input type='text' name='CCFirstName' placeholder='First Name' />
								<input type='text' name='CCLastName' placeholder='Last Name' />
							</div>
						</div>

						<div class='control-group'>
							<label class='control-label' for='cardType'>Card Type</label>
							<div class='controls'>
								<select name='cardType'>
									<option value=''>Choose card type...</option>
									<?php foreach($cardTypes as $card): ?>
									<option value='<?= $card->Type ?>'><?= $card->Display ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class='control-group'>
							<label class='control-label' for='CardNumber'>Card Number</label>
							<div class='controls'>
								<input type='text' name='CardNumber' placeholder='Card Number' autocomplete="off" />
							</div>
						</div>
						<div class='control-group'>
							<label class='control-label' for='CCExp'>Expiration</label>
							<div class='controls'>
								<select name='ccExpMonth'>
									<option value='01'>January</option>
									<option value='02'>February</option>
									<option value='03'>March</option>
									<option value='04'>April</option>
									<option value='05'>May</option>
									<option value='06'>June</option>
									<option value='07'>July</option>
									<option value='08'>August</option>
									<option value='09'>September</option>
									<option value='10'>October</option>
									<option value='11'>November</option>
									<option value='12'>December</option>
								</select>
								<select name='ccExpYear' class='span1'>
									<?php
										$year = date('Y');
										$i = 0;

										while($i < 13){
											?>
											<option value='<?= $year + $i ?>'><?= $year + $i ?></option>
											<?php
											$i++;
										}
									?>
								</select>
							</div>
						</div>
						<div class='control-group'>
							<label for='CVV' class='control-label'>CVV <a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Card Verification Value:3 digit number found on the back of your card"><i class="iconic-info"></i></a></label>
							<div class='controls'>
								<input class='span1' name='CVV' type='text' />
							</div>
						</div>

						<h4>Billing Address</h4>
						<div class='control-group'>
							<label class='control-label'>Address</label>
							<div class='controls'>
								<input type='text' value='<?= $BillingAddress->Address1 ?>' name='Address' /><?= br(1) ?>
								<input type='text' value='<?= $BillingAddress->Address2 ?>' name='Address2' />
							</div>
						</div>
						<div class='control-group'>
							<label for='City' class='control-label'>City</label>
							<div class='controls'>
								<input type='text' name='City' value='<?= $BillingAddress->City ?>' />
							</div>
						</div>
						<div class='control-group'>
							<div class='controls'>
								<input type='text' name='State' value='<?= $BillingAddress->State ?>' class='span1' placeholder='State'/>
								<input type='text' name='Zip' value='<?= $BillingAddress->Zip ?>' class='span1' placeholder='Zip'/>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
		<div class='container'>
			<div class='span10'>
				<div class='pull-right span3 well'>
					<div class='row'>
						<div class='span2 text-right'>
							<b>Cart Subtotal</b>
						</div>
						<div class='span1 text-right'>
							$<span class='subtotal'><?= number_format($this->order_manager->Subtotal,2) ?></span>
						</div>
					</div>
					<div class='row'>
						<div class='span2 text-right'>
							Shipping
						</div>
						<div class='span1 text-right'>
							$<span class='shippingtotal'>0.00</span>
						</div>
					</div>
					<div id='rush_row' class='row' style='display:none'>
						<div class='span2 text-right'>
							Rush Order
						</div>
						<div class='span1 text-right'>
							$<span class='rushtotal'>0.00</span>
						</div>
					</div>
					<div class='row'>
						<div class='span2 text-right'>
							<h4><b>Total</b></h4>
						</div>
						<div class='span1 text-right'>
							<h4>$<span class='totalPrice'><?= $this->order_manager->Subtotal ?></span></h4>
						</div>
					</div>
					<?= br(1) ?>
					<div class='row'>
						<div class='span3 text-right'>
							<input type='submit' value='Submit Order & Payment' class='btn btn-primary'/>
						</div>
					</div>
				</div>
			</div>
		</div>

		<input type='hidden' name='subtotal' value='<?= $this->order_manager->Subtotal ?>' />
	</form>
</div>