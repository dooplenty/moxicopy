<div class='masthead'>
	<div class='logo container text-center'>
		<a class='' href='<?= site_url() ?>'>

			<span class='title'>Moxicopy.com</span>

			<img src='<?= IMAGES_LOCATION ?>logo.jpg' alt='Moxicopy' />
		</a>

		<h6>Quality Cheap Color Copies</h6>
	</div>
	<nav class='navbar'>
		<div class='navbar-inner'>
			<div class='container'>
				<ul class='nav'>
					<li<?= $active == 'home' ? " class='active'" : ""?>>
						<a href='<?= site_url() ?>'>Home</a>
					</li>

					<li<?= $this->uri->segment(1) == 'order' ? " class='active'" : ""?>>
						<a href='<?= site_url('order') ?>'>Instant Quote</a>
					</li>

					<li<?= $active == 'sample' ? " class='active'" : ""?>>
						<a href='<?= site_url('page/sample') ?>'>Sample Kit</a>
					</li>

					<li<?= $active == 'tools' ? " class='active'" : ""?>>
						<a href='<?= site_url('page/tools') ?>'>Tools</a>
					</li>

					<li<?= $active == 'support' ? " class='active'" : ""?>>
						<a href='<?= site_url('page/support') ?>'>Support</a>
					</li>

					<li<?= $active == 'blog' ? " class='active'" : ""?>>
						<a href='<?= site_url('blog') ?>'>Blog</a>
					</li>

					<li class="dropdown<?= $active == 'account' ? ' active' : ''?>">
						<a class='dropdown-toggle' data-toggle='dropdown' href='#'>
							My Account
							<b class="caret"></b>
						</a>

						<ul class='dropdown-menu'>
							<?php if($logged_in === true): ?>
								<li><a href='<?= site_url('admin/account') ?>'>Account Info</a></li>
								<li><a href='<?= site_url('users/logout') ?>'>Logout</a></li>
							<?php else: ?>
								<li><a href='<?= site_url('users/login') ?>'>Login</a></li>
							<?php endif; ?>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	
		<div class='container clearfix'>
			<div class='pull-left span6 text-left'>
				<?= img('assets/img/free-shipping-banner.jpg') ?>
			</div>
			<div class='pull-right span4 text-right questions'>
				<h5><small>Questions? Call TOLL FREE <?= SUPPORT_NUMBER ?></small></h5>
			</div>
		</div>
	
</div>