<fieldset>
	<legend>Billing Info</legend>

	<label>First Name</label>
	<input type='text' name='BillingFirstName' value='<?= $user->FirstName ?>' class='required'/>

	<label>Last Name</label>
	<input type='text' name='BillingLastName' value='<?= $user->LastName ?>'  class='required'/>

	<label>Address</label>
	<input type='text' name='BillingAddress1' value='<?= $user->Address1 ?>'  class='required'/>

	<label>Address 2</label>
	<input type='text' name='BillingAddress2' value='<?= $user->Address2 ?>' placeholder='Apt, Unit, Suite, etc....' />

	<label>City</label>
	<input type='text' name='BillingCity' value='<?= $user->City ?>'  class='required'/>

	<label>State</label>
	<?php
		$states = array(
	        'AL' => 'Alabama',
	        'AK' => 'Alaska',
	        'AZ' => 'Arizona',
	        'AR' => 'Arkansas',
	        'CA' => 'California',
	        'CO' => 'Colorado',
	        'CT' => 'Connecticut',
	        'DE' => 'Delaware',
	        'DC' => 'District of Columbia',
	        'FL' => 'Florida',
	        'GA' => 'Georgia',
	        'HI' => 'Hawaii',
	        'ID' => 'Idaho',
	        'IL' => 'Illinois',
	        'IN' => 'Indiana',
	        'IA' => 'Iowa',
	        'KS' => 'Kansas',
	        'KY' => 'Kentucky',
	        'LA' => 'Louisiana',
	        'ME' => 'Maine',
	        'MD' => 'Maryland',
	        'MA' => 'Massachusetts',
	        'MI' => 'Michigan',
	        'MN' => 'Minnesota',
	        'MS' => 'Mississippi',
	        'MO' => 'Missouri',
	        'MT' => 'Montana',
	        'NE' => 'Nebraska',
	        'NV' => 'Nevada',
	        'NH' => 'New Hampshire',
	        'NJ' => 'New Jersey',
	        'NM' => 'New Mexico',
	        'NY' => 'New York',
	        'NC' => 'North Carolina',
	        'ND' => 'North Dakota',
	        'OH' => 'Ohio',
	        'OK' => 'Oklahoma',
	        'OR' => 'Oregon',
	        'PA' => 'Pennsylvania',
	        'RI' => 'Rhode Island',
	        'SC' => 'South Carolina',
	        'SD' => 'South Dakota',
	        'TN' => 'Tennessee',
	        'TX' => 'Texas',
	        'UT' => 'Utah',
	        'VT' => 'Vermont',
	        'VA' => 'Virginia',
	        'WA' => 'Washington',
	        'WV' => 'West Virginia',
	        'WI' => 'Wisconsin',
	        'WY' => 'Wyoming'
		);
	?>
	<select name="BillingState">
		<option>Choose State...</option>
		<?php foreach($states as $abbr => $state): ?>
		<option value='<?= $abbr ?>' <?= $abbr == $user->State ? 'selected' : '' ?>><?= $state ?></option>
		<?php endforeach; ?>
	</select>

	<label>Zip Code</label>
	<input type='text' name='BillingZip' value='<?= $user->Zip ?>' class='input-small required' />
</fieldset>