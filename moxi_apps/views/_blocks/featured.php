<div id='featured' class='container featured'>
	<ul class='featured-list clearfix'>
		<li class='product-box'>
			<a href="<?= site_url('order/product/1') ?>" class='product-link'>
				<div class='image'>
					<img alt='flyers' src="<?= IMAGES_LOCATION ?>flyers.png" />
				</div>
				<div class='featured-text'>
					Order Flyers
				</div>
			</a>
		</li>
		<li class='product-box'>
			<a href="<?= site_url('order/product/10') ?>" class='product-link'>
				<div class='image'>
					<img alt='magazines' src="<?= IMAGES_LOCATION ?>newsletters.png" />
				</div>
				<div class='featured-text'>
					Order Newsletters
				</div>
			</a>
		</li>
		<li class='product-box'>
			<a href="<?= site_url('order/product/3') ?>" class='product-link'>
				<div class='image'>
					<img alt='flyers' src="<?= IMAGES_LOCATION ?>brochures.png" />
				</div>
				<div class='featured-text'>
					Order Brochures
				</div>
			</a>
		</li>
		<li class='product-box'>
			<a href="<?= site_url('order/product/9') ?>" class='product-link'>
				<div class='image'>
					<img alt='flyers' src="<?= IMAGES_LOCATION ?>samples/flyer-sample.png" />
				</div>
				<div class='featured-text'>
					Order Magazines
				</div>
			</a>
		</li>
	</ul>
</div>
