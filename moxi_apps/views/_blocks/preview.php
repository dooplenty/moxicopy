<h4 class='lead text-center'><small>Moxicopy.com's "File Preview" System</small><a href='#digitalProofModal' data-toggle='modal' class='act-primary'><i class='icon-info-sign'></i></a></h4>

<div id='product_preview' class='preview-box'>
	<div style='display:none' class="progress progress-striped active">
		<div class="bar" style="width: 0%;"></div>
	</div>

	<div class='preview-text'>
		<p>
			This system is to allow you to insure that you have uploaded the correct file for printing.
			Click on the "Select File" button below to upload your file. You can upload as many files as you wish.
			If you require a "free digital proof" or a "hard copy proof" you can make these selections at the
			checkout page.
			Once all of your files are uploaded, click the "Add to Cart" button to continue with your order.
		</p>

		<p>
			To bypass this system click "Add to cart" to continue your order.
		</p>
	</div>

</div>

<!-- <p class='text-warning text-center'><small><span class='label label-info'>Heads up!</span> Important content should be inside the dotted line</small></p> -->

<p class='center alert alert-error upload-alert hide'></p>
<p class='center alert alert-info upload-info hide'></p>

<form action='<?= site_url("uploads/file") ?>' method="POST" enctype="multipart/form-data" name="uploadForm">
	<p class='muted text-center'><small>Locate the file(s) you want to use</small></p>
	<?php if(isset($FileUploads)): ?>
		<?php foreach($FileUploads as $FileUpload): ?>
			<div class='fileupload fileupload-new text-left uploaded' data-provides="fileupload" id='fileupload_<?= $FileUpload->FileUploadID ?>'>
				<span class='fileupload-exists preview btn btn-success' id='preview_<?= $FileUpload->FileUploadID ?>'>Preview</span>
				<span class='btn btn-file fileupload-new'>
					<span class="fileupload-new">Select file</span>
					<input type="file" data-url="/uploads/file" />
				</span>
				<span class='fileupload-preview'><?= $FileUpload->FileInfo->OriginalFilename ?></span>
				<a href='#' class='close fileupload-exists' data-dismiss="fileupload" aria-hidden="true" style="float:none">x</a>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
	<div class="fileupload fileupload-new text-left" data-provides="fileupload">
		<span class='fileupload-exists preview btn btn-success'>Preview</span>
		<span class="btn btn-file fileupload-new">
			<span class="fileupload-new">Select file</span>
			<input type="file" data-url="/uploads/file" />
		</span>
		<span class='fileupload-preview'></span>
		<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float:none">x</a>
	</div>

	<input type='hidden' name='product_unique' value='<?= $this->product_manager->get_product_key() ?>' />
	<input type='hidden' name='preview_size' />
	<input type='hidden' name='preview_product' value='<?= $this->product_manager->ProductID ?>' />
</form>

<div id='digitalProofModal' class='hide modal fade'>
	<div class='modal-header'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    	<h3>Instant Proof System</h3>
	</div>

	<div class='modal-body'>
		<p>
			To insure correct printing PDF files work best. We have provided 2 free systems that you can
			download and use to convert your file to a PDF <a href='http://www.freepdfconvert.com/'>Free PDF Convert</a> or <a href='http://www.primopdf.com/'>Primo PDF</a>
			Simply click on the link and you will be taken to the free PDF software site. They are very simple to use and only take a few seconds.
		</p>
		<p>
			Remember, if you would
			like a free digital proof or a hard copy proof you can make your selection on the checkout page. Please call if you have any questions. <?= SUPPORT_NUMBER ?>
			9-5 est. M-F or send an email to <a href='mailto:<?= SUPPORT_EMAIL ?>'>support@moxicopy.com</a>
		</p>
	</div>

	<!-- <div class='modal-body'>
		<p>Welcome to Moxicopy.com's exclusive “Instant Digital Proof” system. Upload your pdf file and
		within seconds/minutes you will see a digital proof of your file as it will actually print. This new
		system gives you the opportunity to review an actual digital proof of your file so you can insure that
		your fonts are correct and that all graphic elements designed into the document are visible as intended.
		No more waiting days for “customer service” to create your file and send it back to you for review only
		to find problems which need correcting, adding days to the process. No more taking chances by
		ordering without a proof only to receive an incorrect print job. Get the print job you expect, every
		time.</p>
		<p>If the digital file looks correct, approve the proof and we will proceed with processing your print job.
		If you want to make changes, delete the file from the proof system, go back to your original file and
		make the changes in the file as needed, and upload the new file. You can repeat the process as often as
		needed until you see the results you want. This new digital proof system is a Moxicopy.com exclusive
		and is intended to save you time and money.</p>
		<p>If you need a free system to convert your file to a pdf prior to uploading to Moxicopy.com, try
		<a href='http://www.freepdfconvert.com/'>Free PDF Convert</a> or <a href='http://www.primopdf.com/'>Primo PDF</a>
		these are two pdf converter programs which seem to do a very good job of converting most common
		file types to an accurate printable pdf.</p>
		<p>Moxicopy.com will always be happy to provide a hard copy proof if required for $15.00 including
		ground shipping. Just select “Hard Copy Proof” where indicated in the quote system.</p>
	</div> -->

	<div class='modal-footer'>
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>
</div>