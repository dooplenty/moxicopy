<footer class='footer'>
	<div class='row span10'>
		<div class='container'>
			<small>
				<div class='span3'>Copyright &copy; 2013 Moxicopy.com  All rights reserved.</div>
				<div class='span1'><a href='terms.php' target='_blank'>Terms of Use</a></div>
				<div class='span1'><a href='<?= site_url('page/policy') ?>' target='_blank'>Privacy Policy</a></div>
				<div class='span2'><a href='money.php' target='_blank'>Money Back Guarantee</a></div>
			</small>
		</div>
	</div>

	<div class='row span10'>
		<div class='container'>
			<div class='row'>
				<h6>Services</h6>
				<small>
					<div class='pull-left'><a href='<?= site_url('page/catalog_print_services') ?>'>Catalog Print Services</a></div>
					<div class='pull-left'><a href='<?= site_url('page/printing_and_copying') ?>'>Printing and Copying Services</a></div>
					<div class='pull-left'><a href='<?= site_url('page/newsletter_print_services') ?>'>Newsletter Print Services</a></div>
					<div class='pull-left'><a href='<?= site_url('page/brochure_print_services') ?>'>Brochure Print Services</a></div>
					<div class='pull-left'><a href='<?= site_url('page/online_printing_service') ?>'>Online Printing Service</a></div>
					<div class='pull-left'><a href='<?= site_url('page/digest_printing') ?>'>Digest Printing</a></div>
					<div class='pull-left'><a href='<?= site_url('page/script_printing') ?>'>Script Printing</a></div>
					<div class='pull-left'><a href='<?= site_url('page/manual_printing') ?>'>Manual Printing</a></div>
					<div class='pull-left'><a href='<?= site_url('page/release_printing') ?>'>Release Printing</a></div>
					<div class='pull-left'><a href='<?= site_url('page/pamphlet_printing') ?>'>Pamphlet Printing</a></div>
					<div class='pull-left'><a href='<?= site_url('page/tract_printing') ?>'>Tract Printing</a></div>
					<div class='pull-left'><a href='<?= site_url('page/circular_printing') ?>'>Circular Printing</a></div>
					<div class='pull-left'><a href='<?= site_url('page/flyer_printing_company') ?>'>Flyer Printing Company</a></div>
					<div class='pull-left'><a href='<?= site_url('page/printing_flyers') ?>'>Printing Flyers</a></div>
				</small>
			</div>

			<div class='row'>
				<h6>General Info</h6>
				<small>
					<div class='pull-left'><a href='<?= site_url('page/commercial_printng_company') ?>'>Commercial Printing Company</a></div>
					<div class='pull-left'><a href='<?= site_url('page/cheap_color_copy') ?>'>Cheap Color Copy</a></div>
					<div class='pull-left'><a href='<?= site_url('page/flyer_printing_company') ?>'>Flyer Printing Company</a></div>
					<div class='pull-left'><a href='<?= site_url('page/copy_shop') ?>'>Copy Shop</a></div>
					<div class='pull-left'><a href='<?= site_url('page/quality_commercial_printing') ?>'>Quality Commercial Printing</a></div>
					<div class='pull-left'><a href='<?= site_url('page/additional_resources') ?>'>Additional Resources</a></div>
				</small>
			</div>
		</div>
	</div>
</footer>