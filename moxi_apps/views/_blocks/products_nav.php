<div class='sidebar-nav pull-left'>

	<div class='well products-nav' id='product_nav'>

		<?php
			$class = ' first';
			foreach($product_categories as $category): 
		?>

			<div class='title-bar<?= $class ?>'>

				<h2><?= $category->ProductCategoryName ?></h2>

			</div>

			<div class='product-content clearfix'>

				<ul>

					<?php foreach($products as $product): ?>

						<?php if($product->ShowInNav === '1' && $product->ProductCategoryID == $category->ProductCategoryID): ?>

							<li<?= $product->FeaturedProduct == 1 ? " class='highlight'" : "" ?>>

								<a href='<?= site_url("order/product/" . $product->ProductID) ?>'><?= $product->ProductName ?></a>

							</li>

						<?php endif; ?>

					<?php endforeach; ?>

				</ul>

			</div>

		<?php
			$class = '';
			endforeach;
		?>

	</div>

</div>