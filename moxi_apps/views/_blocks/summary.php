<fieldset>
	<legend><?= $this->order_manager->OrderID ?> <?php if(!isset($final)): ?><span class='pull-right'><small><a href='<?= site_url('order') ?>'>Continue Shopping</a></small></span><?php endif; ?></legend>

	<?php foreach($this->order_manager->get_products() AS $Product): ?>
		<div class='container well'>
			<!-- product image preview -->
			<div class='span2'>
				<?= count($Product->FileUpload) ?> File(s) uploaded
			</div>

			<div class='span8 product-summary'>
				<?php if(!isset($final)): ?>
				<div class='text-right'>
					<a class='act-primary' href='<?= site_url('order/product/' . $Product->ProductID . '/' . $Product->ProductUID) ?>'>Edit</a> |
					<a class='act-danger' href='<?= site_url('order/remove/' . $Product->ProductUID) ?>'>Remove</a>
				</div>
				<?php endif ?>

				<div class='row'>
					<div class='span2 summary-item'><b>Product Name</b></div>
					<div class='span5 summary-item'><?= $Product->ProjectName . ' | ' . $Product->Info->ProductName ?></div>
				</div>

				<div class='row'>
					<div class='span2 summary-item'><b>Quantity</b></div>
					<div class='span5 summary-item'><?= $Product->Quantity ?></div>
				</div>
				<div class='row'>
					<div class='span2 summary-item'><b>Product Size</b></div>
					<div class='span5 summary-item'><?= $Product->Size->SizeX . ' x ' . $Product->Size->SizeY . ($Product->Info->ProductCategoryID == 2 ? ' - ' . $Product->Size->FinishedSize : '') ?></div>
				</div>

				<?php
					if(isset($Product->NumSheets) && $Product->NumSheets > 0){
						?>
						<div class='row'>
							<div class='span2 summary-item'><b>Pages in Original</b></div>
							<div class='span5 summary-item'><?= $Product->NumSheets ?></div>
						</div>
						<div class='row'>
							<div class='span2 summary-item'><b>Pages in Color</b></div>
							<div class='span5 summary-item'><?= $Product->NumSheetsColor ?></div>
						</div>
						<div class='row'>
							<div class='span2 summary-item'><b>Pages in BW</b></div>
							<div class='span5 summary-item'><?= $Product->NumSheetsBW ?></div>
						</div>
						<?php
					}
				?>

				<div class='row'>
					<div class='span2'><b>Attributes</b></div>
					<div class='span5'>&nbsp;</div>
					<div class='span2'>&nbsp;</div>
					<div class='span5'>
						<?php foreach($Product->Attributes as $Attribute): ?>

						<?php
							/*if(!is_numeric($attr_id) || $Attribute['Details']->AttributeKey == 'sizes'){
								continue;
							}*/
						?>
						<div class='row'>
							<div class='span3 summary-item'><b><?= $Attribute->Info->AttributeName ?></b></div>
							<?php if($Attribute->Info->AttributeKey == 'inkcolorpages'){ continue; } ?>
							<div class='span2 summary-item'>
								<?php if(isset($Attribute->Info->PaperType)): ?>
									<?= $Attribute->Info->PaperType->PaperTypeName . (!empty($Attribute->Info->PaperType->PaperTypeDescription) ? ' - ' . $Attribute->Info->PaperType->PaperTypeDescription : '') ?>
								<?php else: ?>
									<?= $Attribute->Value === '1' ? "<span class='badge badge-success'><i class='iconic-check'></i></span>" : $Attribute->Value ?>
								<?php endif; ?>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
</fieldset>