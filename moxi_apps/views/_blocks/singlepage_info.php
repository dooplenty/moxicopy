<a href='#singlepage_info' data-toggle='modal' role='button' class='act-primary'>When should I use the single quote form system?</a>

<div id='singlepage_info' class='modal hide fade'>
	<div class='modal-header'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Best Uses for Single Page forms</h3>
	</div>

	<div class='modal-body'>
		Single Sheet Quote forms are best used for print projects such as the items listed below...

		<ul>
			<li>Flyers</li>
			<li>Bookmarks</li>
			<li>Brochures</li>
			<li>Letterhead</li>
			<li>Postcards</li>
			<li>Greeting Cards</li>
			<li>Invitations</li>
			<li>Holiday Cards</li>
			<li>Raffle tickets</li>
			<li>Door hangers</li>
			<li>Sell Sheets</li>
			<li>Club Flyers</li>
			<li>Any other single sheet product you can think of.</li>
		</ul>

		<p>
			These quote forms are designed to give accurate pricing if you input where indicated the exact number
			of printed pieces that you want to purchase. We do this to save paper and keep prices
			low.
		</p>

		<p>
			<span class='label label-info'>EXAMPLE</span>

			If you are purchasing 4x6 postcards, input (where indicated on the quote form) how
			many cards you want, and follow the other directions on the quote form. We will usually
			configure/layout your product so that we can run them as efficiently as possible and will often gang up
			jobs for more efficient production, to save paper and print cost and keep prices low. We have many
			options for this, so it does not help us for you to to lay out projects of smaller items (2 up) or ( 4up)
			onto 8.5x11 or 11x17 paper, because we often then need to break down these layouts in order to fit as
			many pieces as we can onto our large sheets to print more efficiently, quickly and cost effectively.
			It is always best to send a single file set up in the size which you wish the end result to be printed. You
			can upload side one first, then side two, or send them together. It does not matter to us, as we will
			probably reorganize the image so we can print as many pieces per sheet as possible, depending upon if
			we use offset or digital printing.
		</p>

		<p>
			Use this quote system for any job for which the end result is a single sheet of paper. Printed single or
			double sided, of any size. Color or b&w or a combination of the two if you want to save money.
			You can refer to the <i class="icon-info-sign"></i> next to "Paper Types" for additional information for suggested uses for the
			different paper types and why these suggestions are made and other information.
		</p>
	</div>
	<div class='modal-footer'>
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>
</div>