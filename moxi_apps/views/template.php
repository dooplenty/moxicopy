<?= doctype() ?>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<html>

	<head>
		<!-- charset declaration, add early to avoid encoding related security issue -->
		<meta http-equiv="Content-Type" content="text/html;" charset="utf-8" />

		<!-- makes sure the latest version of IE is used in versions of IE that contain multiple rendering engines -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?= isset($title) ? $title : 'Add Title' ?></title>

		<!-- cache information should not be used - forward requests to origin server -->
		<meta http-equiv="Pragma" content="no-cache" />

		<!-- prevents cacheing in IE -->
		<META http-equiv="Expires" content="-1" />

		<!-- verify site with google -->
		<meta name="google-site-verification" content="cEtTJFubQmaAaw0rjIw91ScI8zvq5x2AggTciZMS4jM" />

		<!-- General meta information -->

		<meta name="keywords" content="<?= isset($keywords) ? $keywords : "" ?>" />
		<meta name="description" content="<?= isset($description) ? $description : "" ?>" />

		<!-- fit viewport to screen size - important for mobile devices -->
		<meta name="viewport" content="width=device-width">

		<!-- Prevent Updowner from indexing site -->
		<meta name="Updownerbot" content="noindex">

		<?php
			if(isset($css_files)) {
				foreach($css_files as $css) {
					echo link_tag(CSS_LOCATION . $css);
				}
			}
		?>

		<script src="<?= SCRIPTS_LOCATION ?>modernizr.js"></script>

	</head>

	<body class='main'>
		<div class='container'>

			<!--[if lt IE 7]>
            	<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        	<![endif]-->

			<?= isset($header) ? $header : "" ?>

			<?php $this->load->view('notifications') ?>

			<?= isset($content) ? $content : "" ?>

			<?= isset($footer) ? $footer : "" ?>

		</div>

		<script data-main='/assets/js/main.js' type='text/javascript' src='/assets/js/require-jquery.js'></script>

		<!-- throw in some analytics tracking -->
		<script type="text/javascript">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-24209759-1']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>

		<!-- Google Code for Purchase Conversion Page -->
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1005119141;
		var google_conversion_language = "en";
		var google_conversion_format = "1";
		var google_conversion_color = "ffffff";
		var google_conversion_label = "-gHzCMvN3wQQpc2j3wM";
		var google_conversion_value = 0;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1005119141/?value=0&amp;label=-gHzCMvN3wQQpc2j3wM&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
	</body>
</html>