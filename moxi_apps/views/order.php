<section class='container'>

	<div id="order" class='span4 content-box well'>
		<div class='span3 center alert alert-error quote-alert hide'></div>

		<h3><?= $this->product_manager->ProductName ?></h3>

		<?php
			if(isset($additional_info)){
				echo $additional_info;
			}
		?>

		<?php if($this->order_manager->ProjectName): ?>
			<label><h5 class='text-info'><?= $this->order_manager->ProjectName ?></h5></label>
		<?php endif; ?>

		<?= $step ?>

		<?= isset($pricing) ? "<hr />" . $pricing : "" ?>

	</div>

	<div class='span5 pull-right content-box content'>
		<?= $preview ?>
	</div>
</section>