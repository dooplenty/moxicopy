<div class='product-attribute clearfix <?= $key ?>'>
	<label for='<?= $key ?>_<?= $attrid ?>'>How many pages are in color?</label>
	<input class='span1 <?= $required ?>' type='text' name='numSheetsColor' value='<?= $this->order_manager->order_created() ? $this->order_manager->get_product($this->product_manager->get_product_key(), null, false)->NumSheetsColor : ''?>' />

	<label for='<?= $key ?>_<?= $attrid ?>'>How many pages are in B&W?</label>
	<input class='span1 <?= $required ?>' type='text' name='numSheetsBW' value='<?= $this->order_manager->order_created() ? $this->order_manager->get_product($this->product_manager->get_product_key(), null, false)->NumSheetsBW : ''?>' />
</div>