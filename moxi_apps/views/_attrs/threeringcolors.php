<div class='product-attribute' style='display:none'>
	<label class='toggle'>Hole Punch Colors</label>

	<div class='controls'>
		<label class='radio inline'>
			Black
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='black' <?= $defaultValue == 'black' ? 'checked' : ''?>/>
		</label>
		<label class='radio inline'>
			White
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='white' <?= $defaultValue == 'white' ? 'checked' : ''?>/>
		</label>
	</div>
</div>