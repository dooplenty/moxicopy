<div class='product-attribute'>
	<div id='side1'>
		<label>Ink Color - <?= $this->product_manager->ProductCategoryID == '2' ? 'Inside Pages' : 'Side 1' ?></label>
		<select name='<?= $key ?>_<?= $attrid ?>' class='<?= $required ?>'>
			<option <?= $defaultValue == 'color' ? 'selected' : '' ?> value='color'>Color</option>
			<option <?= $defaultValue == 'bw' ? 'selected' : '' ?> value='bw'>Black and White</option>
		</select>
	</div>
</div>