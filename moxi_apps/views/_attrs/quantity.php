<div class='product-attribute'>
	<label for='quantity'>Enter Quantity Here</label>

	<input class='input-small span1 <?= $required ?>' type='text' value='<?= $this->order_manager->order_created() ? $this->order_manager->get_product($this->product_manager->get_product_key())->Quantity : $this->product_manager->DefaultQuantity ?>' name='quantity' placeholder='Quantity...'/>
</div>