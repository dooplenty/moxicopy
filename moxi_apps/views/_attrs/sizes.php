<div class='product-attribute'>
	<label for='product_size'>
		Paper Sizes
	</label>

	<select name='<?= $key ?>_<?= $attrid ?>' id='product_size' class='<?= $required ?>'>
		<?php foreach($this->product_manager->Sizes as $size): ?>
			<option <?= ($this->order_manager->order_created() && $this->order_manager->get_product($this->product_manager->get_product_key())->ProductSizeID == $size->ProductSizeID) || ($this->product_manager->ProductCategoryID == 3 && $size->ProductSizeID == 1) ? 'selected' : '' ?> data-h="<?= $size->PreviewHeight ?>" data-w="<?= $size->PreviewWidth ?>" value="<?= $size->ProductSizeID ?>">
				<?php if($this->product_manager->ProductCategoryID == 2): ?>
					<?= "{$size->SizeX}\" x {$size->SizeY}\" - {$size->FinishedSize}" ?>
				<?php else: ?>
					<?= "{$size->SizeName} ({$size->SizeX}\" x {$size->SizeY}\")" ?>
				<?php endif; ?>
			</option>
		<?php endforeach; ?>
	</select>
	<a href='#paperSizeModal' data-toggle='modal' class='btn' role='button'><i class='icon-info-sign'></i></a>
</div>

<div id='paperSizeModal' class='fade modal hide'>
	<div class='modal-header'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    	<h3>Paper Sizes</h3>
	</div>
	<div class='modal-body'>
		We produce other sizes than those listed, please contact customer service for information about other
		sizes and prices. If full bleed is required see full bleed information in “Tools” regarding proper set up
		for full bleed printing
	</div>
	<div class='modal-footer'>
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>
</div>