<div class='product-attribute show-radio'>
	<label>
		Full Bleeding
		<a href='#fullBleedModal' data-toggle='modal' class='btn' role='button'><i class='icon-info-sign'></i></a>
	</label>

	<label class='radio' for='<?= $key ?>_<?= $attrid ?>'>
		<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='0' <?= $defaultValue == 0 || $defaultValue == "" ? "checked" : "" ?> /> This document <span class='label label-info'>DOES NOT</span> have full bleed
	</label>
	<label class='radio' for='<?= $key ?>_<?= $attrid ?>'>
		<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='1' <?= $defaultValue == 1 ? "checked" : "" ?> /> This document <span class='label label-info'>DOES</span> have full bleed
	</label>
</div>

<div id='fullBleedModal' class='modal hide fade'>
	<div class='modal-header'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    	<h3>Full Bleed Requirements</h3>
	</div>

	<div class='modal-body'>
		<p>Full bleed printing is where the printing goes all the way to the edge of the page.</p>
		<p>Standard printing has a 1/8 inch border around the edge/edges of the page.   A printed document can have 1 edge up to 4 edges printed full bleed.</p>
		<p>Most commercial digital printing machines do not print all the way to the edge of the page. Full bleed printing is achieved by cutting or trimming the printed document to the finished size.  This requires the original document be designed with all edges extended 1/8 inch beyond the finished size, and printing onto oversize paper.  This allows for the trimming, without cutting into any design elements or text that needs to be retained.</p>
		<p>Full bleed printing also needs to have visible crop marks. These are marks that let the printer know where the designer wants the document to be trimmed.</p>
		<p><span class='label label-info'>Example</span> If you want a full bleed, one sided flyer with a finished size of 8.5x11 (standard paper size).  The original document needs to be designed 8.75x11.25 This allows 1/8th inch on each side of the flyer to be trimmed to a finished size of 8.5x11.</p>

		<?= img('assets/img/8_5x11_fullbleed.jpg') ?>
	</div>

	<div class='modal-footer'>
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>
</div>