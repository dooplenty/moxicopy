<div class='product-attribute'>
	<label>Paper Types - Cover Page</label>

	<select name='<?= $key ?>_<?= $attrid ?>' class='<?= $required ?>'>
		<?php
			$currentCat = null;
			foreach($papertypes as $papertype){

				if($currentCat != $papertype->PaperTypeCategory){
					if(!is_null($currentCat)){
						echo "</optgroup>";
					}
					echo "<optgroup label='{$papertype->PaperTypeCategory}'>";
				}

			?>
				<option <?= $papertype->PaperTypeID == $defaultValue ? 'selected ' : '' ?>value='<?= $papertype->PaperTypeID ?>'><?= $papertype->PaperTypeName . (!empty($papertype->PaperTypeDescription) ? (" - " . $papertype->PaperTypeDescription) : "") ?></option>
			<?php

				$currentCat = $papertype->PaperTypeCategory;
			}
			echo "</optgroup>";
		?>
	</select>
	<a href='#paperTypeModal' data-toggle='modal' class='btn' role='button'><i class='icon-info-sign'></i></a>
</div>