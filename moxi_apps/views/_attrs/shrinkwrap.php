<div class='product-attribute'>
	<label class='toggle'>Shrink Wrap</label>

	<select class='span1 <?= $required ?>' name='<?= $key ?>_<?= $attrid ?>'>
		<option value='no' <?= $defaultValue == "no" || $defaultValue == "" ? "checked" : "" ?>>No</option>
		<option value='yes' <?= $defaultValue == "yes" ? "checked" : "" ?>>Yes</option>
	</select>
</div>