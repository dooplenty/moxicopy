<?php
// pre($this->order_manager->get_product($this->product_manager->get_product_key()));
?>
<div class='product-attribute'>
	<label>
		How many pages are in your original?<br />
		<input type='text' class='span1 numSheets <?= $required ?>' name="numSheets" value='<?= $this->order_manager->order_created() ? $this->order_manager->get_product($this->product_manager->get_product_key(), null, false)->NumSheets : ''?>'>
		<a href='#numPagesModal' data-toggle='modal' class='btn' role='button'><i class='icon-question-sign'></i></a>
	</label>
</div>

<div id='numPagesModal' class='fade hide modal'>
	<div class='modal-header'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    	<h3>How to count your pages</h3>
	</div>
	<div class='modal-body'>
		<p>Count your pages as you would count them in a magazine (not by pieces of
		paper used)... To get an accurate count of pages for quote purposes refer to this example. Example:
		The outside front of the cover should be counted as page 1, the inside of the front cover would be page
		2, the next page would be page 3, the other side of page 3 is page 4 etc. Pages in will be in multiples
		of 4. This is because the documents being quoted here will be produced by printing then folding sheets
		of paper to a final finished size. Documents with 8 pages or more will be bound by staples applied at
		the spine as in a magazine, Documents with 4 pages will have no staples since a 4 page newsletter
		consists of a single sheet.</p>
	</div>
	<div class='modal-footer'>
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>
</div>