<div class='product-attribute show-radio'>
	<label class='toggle'>Binding Options <b class='caret'></b></label>

	<div class='controls' style='display:none'>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='none' <?= $defaultValue == 'none' || $defaultValue == "" ? 'checked' : ''?>/>
			None (just print it)
		</label>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='saddlestapled' <?= $defaultValue == 'saddlestapled' ? 'checked' : ''?>/>
			Saddle Stapled (FREE of charge)
		</label>
	</div>
</div>