<div class='product-attribute'>
	<label>Single/Double Sided</label>
	<select name='<?= $key ?>_<?= $attrid ?>' class='<?= $key ?> <?= $required ?>'>
		<option <?= $defaultValue == 'single' ? 'selected ' : '' ?>value='single'>Single</option>
		<option <?= $defaultValue == 'double' ? 'selected ' : '' ?>value='double'>Double</option>
	</select>
</div>