<div class='product-attribute' style='display:none'>
	<label class='toggle'>Cover Types</label>

	<div class='controls' style='display:none'>
		<label class='radio inline'>
			No Covers
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='none' <?= $defaultValue == 'none' ? 'checked' : ''?>/>
		</label>
		<label class='radio inline'>
			Add Standard Covers (front - clear, back - black cover stock)
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='standard' <?= $defaultValue == 'standard' ? 'checked' : ''?>/>
		</label>
		<label class='radio inline'>
			Add Premium Covers(front - clear, back - black vinyl)
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='premium' <?= $defaultValue == 'premium' ? 'checked' : ''?>/>
		</label>
		<label class='radio inline'>
			Add Printed Covers 100# Matte
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='printed100matte' <?= $defaultValue == 'printed100gloss' ? 'checked' : ''?>/>
		</label>
		<label class='radio inline'>
			Add Printed Covers 100# Gloss
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='printed100gloss' <?= $defaultValue == 'printed100gloss' ? 'checked' : ''?>/>
		</label>
	</div>
</div>