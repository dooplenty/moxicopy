<div class='product-attribute show-radio'>
	<label>Bulk Mailing</label>

	<label type='radio inline'>
		<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='0' <?= $defaultValue == "0" ? 'checked' : '' ?>/> No Bulk Mailing
	</label>
	<label type='radio inline'>
		<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='1' <?= $defaultValue == "1" ? 'checked' : '' ?>/> First Class Mail
	</label>
</div>