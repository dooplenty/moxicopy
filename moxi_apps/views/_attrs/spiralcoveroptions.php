<div class='product-attribute show-radio <?= $key ?>' style='display:none'>
	<label class='toggle'>Spiral Cover Options</label>

	<div class='controls'>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='none' <?= $defaultValue == 'none' || $defaultValue == "" ? 'checked' : ''?>/>
			No Covers
		</label>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='economical' <?= $defaultValue == 'economical' ? 'checked' : ''?>/>
			Add Economical Covers: (Front: Clear || Back: Black Cover Stock with discreet logo)
		</label>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='standardblackcoverstock' <?= $defaultValue == 'standardblackcoverstock' ? 'checked' : ''?>/>
			Add Premium Covers: (Front: Clear Plastic || Back: Black "Vinyl" texture)
		</label>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='standardclear' <?= $defaultValue == 'standardclear' ? 'checked' : ''?>/>
			Add Premium Covers: (Front: Clear Plastic || Back: Black "Vinyl" texture)
		</label>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='premiumblackvinyl' <?= $defaultValue == 'premiumblackvinyl' ? 'checked' : ''?>/>
			Add Premium Covers: (Front: Clear Plastic || Back: Black "Vinyl" texture)
		</label>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='printedmatte' <?= $defaultValue == 'printedmatte' ? 'checked' : ''?>/>
			Add Printed Covers: #100# Matte
		</label>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='printedgloss' <?= $defaultValue == 'printedgloss' ? 'checked' : ''?>/>
			Add Printed Covers: #100# Gloss
		</label>
	</div>
</div>