<div class='product-attribute'>
	<label>Padded Sets</label>

	<select name='<?= $key ?>_<?= $attrid ?>' class='<?= $required ?>'>
		<option value=''>No Padded Sets</option>
		<?php for($i = 1; $i <= 100; $i++){ ?>
			<option <?= $defaultValue == $i ? 'selected ' : '' ?>value='<?= $i ?>'><?= $i ?> Padded Sets</option>
		<?php } ?>
	</select>
</div>

<div class='modal hide fade'>
	<div class='modal-header'>

	</div>
	<div class='modal-body'>
		<h3>What is a Padded Set?</h3>

		<p>A padded set is like a "post-it" pad. One padded set means that all your pages will be glued together with padding glue. Each pad includes a chipboard backing. Each page can be easily removed from the pad just like a post-it note is removed from a post-it pad. If you choose two padded sets then your project would be divided into two separate pads. If you chose ten padded sets your project would be divided into 10 separate pads. Each padded set is an additional 50 cents. </p>
		<p>Please Note: Due to the glueing process add 1-2 days to your order processing time.</p>
	</div>
	<div class='modal-footer'>

	</div>
</div>