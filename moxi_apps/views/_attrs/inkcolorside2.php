<div class='product-attribute'>
	<div id='side2' style='display:none'>
		<label>Ink Color - Side 2</label>
		<select name='<?= $key ?>_<?= $attrid ?>' class='<?= $required ?>'>
			<option <?= $defaultValue == 'color' ? 'selected' : '' ?> value='color'>Color</option>
			<option <?= $defaultValue == 'bw' ? 'selected' : '' ?> value='bw'>Black and White</option>
		</select>
	</div>
</div>