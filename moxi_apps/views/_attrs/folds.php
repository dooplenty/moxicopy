<div class='product-attribute'>
	<label class='toggle'>Folds <b class='caret'></b></label>

	<div class='controls' style='display:none'>
		<label class='radio inline text-center'>
			<?= img('assets/img/no_fold.png') ?><br />
			<span class='muted'><small>None</small></span>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='none' <?= $defaultValue == 'none' || $defaultValue == "" ? 'checked' : ''?>/>
		</label>
		<label class='radio inline text-center'>
			<?= img('assets/img/half_fold.png') ?><br />
			<span class='muted'><small>Bi-Fold</small></span>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='bifold' <?= $defaultValue == 'bifold' ? 'checked' : ''?>/>
		</label>
		<label class='radio inline text-center'>
			<?= img('assets/img/z_fold.png') ?><br />
			<span class='muted'><small>Z-Fold</small></span>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='zfold' <?= $defaultValue == 'zfold' ? 'checked' : ''?>/>
		</label>
		<label class='radio inline text-center'>
			<?= img('assets/img/tri_fold.png') ?><br />
			<span class='muted'><small>Tri-Fold</small></span>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='trifold' <?= $defaultValue == 'trifold' ? 'checked' : ''?>/>
		</label>
	</div>
</div>