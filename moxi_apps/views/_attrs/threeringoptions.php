<div class='product-attribute show-radio <?= $key ?>' style='display:none'>
	<label class='toggle'>Hole Punch Sizes</label>

	<div class='controls'>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='none' <?= $defaultValue == 'none' ? 'checked' : ''?>/>
			No 3 ring binder
		</label>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='halfinch' <?= $defaultValue == 'halfinch' ? 'checked' : ''?>/>
			Add .5" 3 ring binder
		</label>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='oneinch' <?= $defaultValue == 'oneinch' ? 'checked' : ''?>/>
			Add 1" 3 ring binder
		</label>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='one_onehalfinch' <?= $defaultValue == 'one_onehalfinch' ? 'checked' : ''?>/>
			Add 1.5" 3 ring binder
		</label>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='twoinch' <?= $defaultValue == 'twoinch' ? 'checked' : ''?>/>
			Add 2" 3 ring binder
		</label>
	</div>
</div>