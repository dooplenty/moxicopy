<div class='product-attribute'>
	<label>
		Add Secondary Fold
	</label>

	<select class='span1 <?= $key ?> <?= $required ?>' name='<?= $key ?>_<?= $attrid ?>'>
		<option value='no' <?= $defaultValue == "no" || $defaultValue == "" ? "checked" : "" ?>>No</option>
		<option value='yes' <?= $defaultValue == "yes" ? "checked" : "" ?>>Yes</option>
	</select>
	<br />
	<div class='alert alert-info hide'>Please give details in your job notes at checkout.</div>
</div>