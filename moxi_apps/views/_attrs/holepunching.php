<div class='product-attribute'>
	<label class='toggle'>Hole Punching<b class='caret'></b></label>

	<div class='controls' style='display:none'>
		<label class='radio inline'>
			<?= img('assets/img/no_holes.png') ?> <br />
			<span class='muted'><small>None</small></span>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='none' <?= $defaultValue == 'none' || $defaultValue == "" ? 'checked' : '' ?>/>
		</label>
		<label class='radio inline'>
			<?= img('assets/img/left_holes.png') ?> <br />
			<span class='muted'><small>Left</small></span>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='left'  <?= $defaultValue == 'left' ? 'checked' : '' ?>//>
		</label>
		<label class='radio inline'>
			<?= img('assets/img/top_holes.png') ?> <br />
			<span class='muted'><small>Top</small></span>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='top'  <?= $defaultValue == 'top' ? 'checked' : '' ?>//>
		</label>
	</div>
</div>