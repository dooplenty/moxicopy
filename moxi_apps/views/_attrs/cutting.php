<div class='product-attribute'>
	<label class='toggle'>Cutting<b class='caret'></b></label>

	<div class='controls' style='display:none'>
		<label class='radio inline text-center'>
			<?= img('assets/img/no_fold.png'); ?> <br />
			<span class='muted'>None</span>
			<input type='radio' class='required' name='<?= $key ?>_<?= $attrid ?>' value='none' <?= $defaultValue == 'none' || $defaultValue == "" ? 'checked' : '' ?>/ />
		</label>

		<label class='radio inline'>
			<?= img('assets/img/vertical.png') ?> <br />
			<span class='muted'><small>Vertical</small></span>
			<input type='radio' class='required' name='<?= $key ?>_<?= $attrid ?>' value='vertical'  <?= $defaultValue == 'vertical' ? 'checked' : '' ?>//>
		</label>
		<label class='radio inline'>
			<?= img('assets/img/horizontal.png') ?> <br />
			<span class='muted'><small>Horizontal</small></span>
			<input type='radio' class='required' name='<?= $key ?>_<?= $attrid ?>' value='horizontal'  <?= $defaultValue == 'horizontal' ? 'checked' : '' ?>//>
		</label>
		<label class='radio inline'>
			<?= img('assets/img/quarter.png') ?> <br />
			<span class='muted'>Quarters</span>
			<input type='radio' class='required' name='<?= $key ?>_<?= $attrid ?>' value='quarter'  <?= $defaultValue == 'quarter' ? 'checked' : '' ?>//>
		</label>
	</div>
</div>