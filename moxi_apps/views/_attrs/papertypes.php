<div class='product-attribute'>
	<label>Paper Types<?= $this->product_manager->ProductCategoryID == '2' ? ' - Inside Pages' : '' ?></label>

	<select name='<?= $key ?>_<?= $attrid ?>' class='<?= $required ?>'>
		<?php
			$currentCat = null;
			foreach($papertypes as $papertype){

				if($currentCat != $papertype->PaperTypeCategory){
					if(!is_null($currentCat)){
						echo "</optgroup>";
					}
					echo "<optgroup label='{$papertype->PaperTypeCategory}'>";
				}

			?>
				<option <?= $papertype->PaperTypeID == $defaultValue ? 'selected ' : '' ?>value='<?= $papertype->PaperTypeID ?>'><?= $papertype->PaperTypeName . (!empty($papertype->PaperTypeDescription) ? (" - " . $papertype->PaperTypeDescription) : "") ?></option>
			<?php

				$currentCat = $papertype->PaperTypeCategory;
			}
			echo "</optgroup>";
		?>
	</select>
	<a href='#paperTypeModal' data-toggle='modal' class='btn' role='button'><i class='icon-info-sign'></i></a>
</div>

<div id='paperTypeModal' class='hide modal fade'>
	<div class='modal-header'>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    	<h3>Paper Type Descriptions</h3>
    </div>

    <div class='modal-body'>
		<h3>Uncoated Stocks</h3>


		<h4>20/50# - 92 Bright White</h4>
		<p>An economical option for single or 2 sided B/W low to high volume projects
		24 / 60# 96 Bright Opaque*; the standard for single sided or 2 sided B/W to full color flyers, brochures, direct
		mail, invoice stuffers, financial printing, reports, sell sheets, letterhead, newsletters, and note pads.
		*High Brightness and preferred white shade provide excellent print contrast.</p>

		<h4>24/60# - 96 Bright Opaque</h4>
		<p>When you need a little more substance for your single sided or 2 sided B/W to
		full color flyers, brochures, direct mail, invoice stuffers, financial printing, reports, sell sheets,
		letterhead, newsletters and note pads.
		*High Brightness and preferred white shade provide excellent print contrast.</p>

		<h4>28/70# - 96 Bright Opaque</h4>
		<p>When you need a little more substance for your 2 sided B/W to
		full color flyers, brochures, catalogs, direct mail, invoice stuffers, financial printing, reports,
		equipment manuals, newsletters, product warranties, and insurance policies.
		*High Brightness and preferred white shade provide excellent print contrast.</p>

		<h4>80# - 96 Bright White Opaque - Cover Stock</h4>
		<p>Suitable for business cards, brochures, tent cards, menus, sell sheets, posters,
		door hangers, greeting cards, invitations, hang tags, post cards, and direct mail.
		*High Brightness and preferred white shade provide excellent print contrast.</p>

		<h4>100# - 96 Bright White Opaque - Cover Stock</h4>
		<p>A perfect choice for business cards, brochures, tent cards, sell sheets, posters,
		door hangers, greeting cards, invitations, hang tags, trading cards, post cards, and direct mail.
		*High Brightness and preferred white shade provide excellent print contrast.</p>


		<h3>Coated Stocks</h3>

		<h4>80# - 96 Bright White Opaque - Text Gloss</h4>
		<p>white shade with an identical smooth gloss finish on both sides; suitable for flyers,
		brochures, direct mail, invoice stuffers, financial printing, commercial/residential real estate
		information sheets, sell sheets, reports, financial reports, and newsletters.</p>

		<h4>100# - 96 Bright White Opaque - Text Gloss</h4>
		<p>white shade with an identical smooth gloss finish on both sides; the standard for
		flyers, brochures, direct mail, invoice stuffers, financial printing, commercial/residential real estate
		information sheets, sell sheets, reports, financial reports, posters, and newsletters.</p>


		<h4>80# - Bright White - Cover Gloss</h4>
		<p>white shade with an identical smooth gloss finish on both sides; suitable for brochures,
		tent cards, menus, commercial/residential real estate information sheets, sell sheets, posters,
		greeting cards, invitations, hang tags, post cards, and direct mail.</p>

		<h4>100# - Bright White - Cover Gloss</h4>
		<p>white shade with an identical smooth gloss finish on both sides; the standard for
		brochures, tent cards, menus, commercial/residential real estate information sheets, sell sheets,
		posters, greeting cards, invitations, door hangers, hang tags, trading cards, post cards and direct
		mail.</p>
	</div>

	<div class="modal-footer">
	 	<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>
</div>