<div class='product-attribute'>
	<label>
		How many pages are in your original?<br />
		<select class='span1 <?= $key ?>' name="<?= $key ?>_<?= $attrid ?>">
			<option value='1' <?= $defaultValue == '1' ? 'selected' : '' ?>>1</option>
			<option value='2' <?= $defaultValue == '2' ? 'selected' : '' ?>>2</option>
		</select>
	</label>
</div>