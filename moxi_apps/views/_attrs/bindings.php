<div class='product-attribute show-radio'>
	<label class='toggle'>Binding Options <b class='caret'></b></label>

	<div class='controls' style='display:none'>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='none' <?= $defaultValue == 'none' || $defaultValue == "" ? 'checked' : ''?>/>
			None (just print it)
		</label>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='cornerstapled' <?= $defaultValue == 'cornerstapled' ? 'checked' : '' ?> />
			Corner Stapled
		</label>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='holepunched' <?= $defaultValue == 'holepunched' ? 'checked' : ''?>/>
			Hole Punched
		</label>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='gbc' <?= $defaultValue == 'gbc' ? 'checked' : ''?>/>
			GBC Binding
		</label>
		<label class='radio'>
			<input type='radio' name='<?= $key ?>_<?= $attrid ?>' value='spiral' <?= $defaultValue == 'spiral' ? 'checked' : ''?>/>
			Spiral Binding
		</label>
	</div>
</div>