<section class='container'>

	<!-- load products view -->
	<?= $this->load->view('_blocks/products_nav') ?>

	<div class='span6 content-box'>
		<div class='content'>
			<div id='hero_carousel' class='carousel slide'>
				<ol class="carousel-indicators">
					<li data-target="#hero_carousel" data-slide-to="0" class="active"></li>
					<li data-target="#hero_carousel" data-slide-to="1"></li>
					<li data-target="#hero_carousel" data-slide-to="2"></li>
				</ol>

				<div class='carousel-inner'>
					<div id='shannon_kristy' class='item active'>
						<img src='/assets/img/shannonchristy.png' alt='shannon christy' />

						<div class='carousel-caption shannon-christy text-center'>
							<a href='http://vimeo.com/70285367'>
								<span class='emphasis'>Remembering</span>
								<span class='emphasis'>Shannon Christy</span>
							</a>
						</div>
					</div>
					<div id='easy_steps' class='item'>
						<img src='/assets/img/3_steps.png' alt='3 easy steps' />

						<div class='carousel-caption three-steps'>
							<div class="clearfix">
						      <span class="three">3</span>
						      <span class="easy-steps"><i>Easy<br>Steps</i></span>
						    </div>

							<ol id="steps_list">
						      <li>Get your quote</li>
						      <li>Register</li>
						      <li>Upload your file</li>
						    </ol>
						</div>
					</div>
					<div id='rush' class='item'>
						<img src='/assets/img/24_hour.png' alt='24 hour rush' />

						<div class='carousel-caption text-center'>
							<span class='emphasis'>Need It Fast?</span>

							<span class='emphasis'>Order Today!</span>

							<span class='emphasis'>We Ship Tomorrow<a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Must Complete Order by 2PM Est."><i class="icon-info-sign"></i></a></span>
						</div>
					</div>
					<div id='money_back' class='item'>
						<img src='/assets/img/money_back.png' alt='Money back guarantee' />

						<div class='carousel-caption money-back'>
							We <a href='#'>guarantee</a> our work or we <a href='#'>refund</a> you 100%!
						</div>
					</div>
				</div>

				<a class="carousel-control left" href="#hero_carousel" data-slide="prev">&lsaquo;</a>
				<a class="carousel-control right" href="#hero_carousel" data-slide="next">&rsaquo;</a>
			</div>
		</div>
	</div>

	<div class='right-nav span2 content-box'>
		<div class='content'>
			<div class='chat-box'>
				<p class='lead'>Chat is</p>

				<div id="VolusionLiveChat" style="">
		          <!-- BEGIN PHP Live! code, (c) OSI Codes Inc. -->
		          <?  
		            // Reloads of not HTTPS
		            /*if ($_SERVER['SERVER_PORT'] != "443") {
		              echo '<script type="text/javascript" language="JavaScript" src="https://www.moxicopy.com/phplive/js/status_image.php?base_url=
		              http://www.moxicopy.com/phplive&amp;l=moxicopycom&amp;x=1&amp;deptid=1&amp;"></script>';
		            }else{
		              echo '<script type="text/javascript" language="JavaScript" src="https://www.moxicopy.com/phplive/js/status_image.php?base_url=
		              http://www.moxicopy.com/phplive&amp;l=moxicopycom&amp;x=1&amp;deptid=1&amp;"></script>';
		            }*/
		          ?>
		          <!-- END PHP Live! code : (c) OSI Codes Inc. -->

		        </div>
			</div>
		</div>

		<blockquote>
			<p>Extensive Quality Control</p>
		</blockquote>

		<blockquote>
			<p>New Print Equipment</p>
		</blockquote>

		<blockquote>
			<p>Offset Printing Available</p>
		</blockquote>

		<blockquote>
			<p>Low Price Guarantee</p>
		</blockquote>

		<blockquote>
			<p>Avg 3-day turnaround</p>
		</blockquote>
	</div>

	<div class='content-box clear span8'>
		<ul class='nav nav-tabs'>
			<?php foreach($pages as $page): ?>
				<li><a href='#<?= $page->Key ?>' data-toggle='tab'><h5><?= $page->Title ?></h5></a></li>
			<?php endforeach; ?>
		</ul>

		<div class='tab-content home-content'>
			<?php foreach($pages as $page): ?>
				<div class='tab-pane' id="<?= $page->Key ?>">
					<h4><?= !empty($page->Display) ? $page->Display : $page->Title ?></h4>
					<?= $page->Text ?></div>
			<?php endforeach; ?>
		</div>
	</div>
</section>