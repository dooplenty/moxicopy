<div class='container'>
	<form name='checkoutForm' action="<?= site_url('payment/payment_offline') ?>" method='post'>
		<div class='container'>
			<div class='row-fluid'>
				<div class='span9 offset3'>
					<h5>OrderID:</h5>
					<p><?= $Order->OrderID ?></p>
					<input type='hidden' name='orderID' value='<?= $Order->OrderID ?>' />

					<h5>Email:</h5>
					<p><?= $Order->User->Email ?></p>

					<h5>Total:</h5>
					<p><?= $Order->Total ?></p>

					<fieldset>
					<legend>Payment Information</legend>

					<select name='paymentType'>
						<option value=''>Choose Payment Type</option>
						<?php
							$payTypes = array(
								'3' => 'PayPal',
								'1' => 'Credit Card'
							);

							foreach($payTypes as $payKey => $payVal) {
								?>
								<option <?= isset($Order->PaymentTypeID) && $Order->PaymentTypeID == $payKey ? 'selected ' : '' ?>value='<?= $payKey ?>'><?= $payVal ?></option>
								<?php
							}
						?>
					</select>
					<?= br(1) ?>
					<div id='paypal-payment' class='form-horizontal well' style='display:none'>
						<div class='control-group'>
							<label class='control-label' for='paypalEmail'>Paypal Email</label>

							<div class='controls'>
								<input type='text' name='paypalEmail' placeholder='Paypal Email Address' />
							</div>
						</div>
					</div>
					<div id='cc-payment' class='form-horizontal well' style='display:none'>
						<div class='control-group'>
							<label class='control-label' for='CCName'>Credit Card Name</label>
							<div class='controls'>
								<input type='text' name='CCFirstName' placeholder='First Name' />
								<input type='text' name='CCLastName' placeholder='Last Name' />
							</div>
						</div>

						<div class='control-group'>
							<label class='control-label' for='cardType'>Card Type</label>
							<div class='controls'>
								<select name='cardType'>
									<option value=''>Choose card type...</option>
									<?php foreach($cardTypes as $card): ?>
									<option value='<?= $card->Type ?>'><?= $card->Display ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class='control-group'>
							<label class='control-label' for='CardNumber'>Card Number</label>
							<div class='controls'>
								<input type='text' name='CardNumber' placeholder='Card Number' autocomplete="off" />
							</div>
						</div>
						<div class='control-group'>
							<label class='control-label' for='CCExp'>Expiration</label>
							<div class='controls'>
								<select name='ccExpMonth'>
									<option value='01'>January</option>
									<option value='02'>February</option>
									<option value='03'>March</option>
									<option value='04'>April</option>
									<option value='05'>May</option>
									<option value='06'>June</option>
									<option value='07'>July</option>
									<option value='08'>August</option>
									<option value='09'>September</option>
									<option value='10'>October</option>
									<option value='11'>November</option>
									<option value='12'>December</option>
								</select>
								<select name='ccExpYear' class='span2'>
									<?php
										$year = date('Y');
										$i = 0;

										while($i < 13){
											?>
											<option value='<?= $year + $i ?>'><?= $year + $i ?></option>
											<?php
											$i++;
										}
									?>
								</select>
							</div>
						</div>
						<div class='control-group'>
							<label for='CVV' class='control-label'>CVV <a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Card Verification Value:3 digit number found on the back of your card"><i class="iconic-info"></i></a></label>
							<div class='controls'>
								<input class='span2' name='CVV' type='text' />
							</div>
						</div>

						<h4>Billing Address</h4>
						<div class='control-group'>
							<label class='control-label'>Address</label>
							<div class='controls'>
								<input type='text' value='<?= $Order->Billing->Address1 ?>' name='Address' /><?= br(1) ?>
								<input type='text' value='<?= $Order->Billing->Address2 ?>' name='Address2' />
							</div>
						</div>
						<div class='control-group'>
							<label for='City' class='control-label'>City</label>
							<div class='controls'>
								<input type='text' name='City' value='<?= $Order->Billing->City ?>' />
							</div>
						</div>
						<div class='control-group'>
							<div class='controls'>
								<select name="State">
									<option></option>
									<option value="none">Out of the USA</option>
									<option value="AL">Alabama</option>
									<option value="AK">Alaska</option>
									<option value="AZ">Arizona</option>
									<option value="AR">Arkansas</option>
									<option value="CA">California</option>
									<option value="CO">Colorado</option>
									<option value="CT">Connecticut</option>
									<option value="DE">Delaware</option>
									<option value="DC">District of Columbia</option>
									<option value="FL">Florida</option>
									<option value="GA">Georgia</option>
									<option value="HI">Hawaii</option>
									<option value="ID">Idaho</option>
									<option value="IL">Illinois</option>
									<option value="IN">Indiana</option>
									<option value="IA">Iowa</option>
									<option value="KS">Kansas</option>
									<option value="KY">Kentucky</option>
									<option value="LA">Louisiana</option>
									<option value="ME">Maine</option>
									<option value="MD">Maryland</option>
									<option value="MA">Massachusetts</option>
									<option value="MI">Michigan</option>
									<option value="MN">Minnesota</option>
									<option value="MS">Mississippi</option>
									<option value="MO">Missouri</option>
									<option value="MT">Montana</option>
									<option value="NE">Nebraska</option>
									<option value="NV">Nevada</option>
									<option value="NH">New Hampshire</option>
									<option value="NJ">New Jersey</option>
									<option value="NM">New Mexico</option>
									<option value="NY">New York</option>
									<option value="NC">North Carolina</option>
									<option value="ND">North Dakota</option>
									<option value="OH">Ohio</option>
									<option value="OK">Oklahoma</option>
									<option value="OR">Oregon</option>
									<option value="PA">Pennsylvania</option>
									<option value="RI">Rhode Island</option>
									<option value="SC">South Carolina</option>
									<option value="SD">South Dakota</option>
									<option value="TN">Tennessee</option>
									<option value="TX">Texas</option>
									<option value="UT">Utah</option>
									<option value="VT">Vermont</option>
									<option value="VA">Virginia</option>
									<option value="WA">Washington</option>
									<option value="WV">West Virginia</option>
									<option value="WI">Wisconsin</option>
									<option value="WY">Wyoming</option>
								</select>
								<input type='text' name='Zip' value='<?= $Order->Billing->Zip ?>' class='span2' placeholder='Zip'/>
							</div>
						</div>
					</div>
				</fieldset>
				</div>
			</div>
			<div class='container'>
				<div class='span10'>
					<div class='pull-right span3 well'>
						<div class='row'>
							<div class='span3 text-right'>
								<input type='submit' value='Submit Order & Payment' class='btn btn-primary'/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form
</div>