<?php if($this->session->flashdata('error')): ?>
	<div class='text-error well lead'><?= $this->session->flashdata('error') ?></div>
<?php elseif($this->session->flashdata('warning')): ?>
	<div class='text-warning well lead'><?= $this->session->flashdata('warning') ?></div>
<?php elseif($this->session->flashdata('notification')): ?>
	<div class='text-info well lead'><?= $this->session->flashdata('notification') ?></div>
<?php endif; ?>