An instant payment notification was successfully received
Project Number: <?= $item_number ?>

from <?= $payer_email ?> on <?= date('m/d/Y') ?> at <?= date('g:i A') ?>

Details: