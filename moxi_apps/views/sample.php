<section class='container'>

	<div class='span10'>

		<form class='horizontal-form'>

			<div class='control-group'>

				<label class='control-label' for='firstname'>First Name</label>

				<div class='controls'>
					<input type='text' name='firstname' id='firstname' placeholder='First Name' />
				</div>
			</div>

			<div class='control-group'>

				<label class='control-label' for='lastname'>Last Name</label>

				<div class='controls'>
					<input type='text' name='lastname' id='lastname' placeholder='Last Name' />
				</div>
			</div>

			<div class='control-group'>

				<label class='control-label' for='organization'>Organization</label>

				<div class='controls'>
					<input type='text' name='organization' id='organization' placeholder='Organization' />
				</div>
			</div>

			<div class='control-group'>

				<label class='control-label' for='address'>Address</label>

				<div class='controls'>
					<input type='text' name='address' id='address' placeholder='Address' />
				</div>
			</div>

			<div class='control-group'>

				<label class='control-label' for='city'>City</label>

				<div class='controls'>
					<input type='text' name='city' id='city' placeholder='City' />
				</div>
			</div>

			<div class='control-group'>

				<label class='control-label' for='state'>State</label>

				<div class='controls'>
					<input type='text' name='state' id='state' placeholder='State' />
				</div>
			</div>

			<div class='control-group'>

				<label class='control-label' for='zip'>Zip</label>

				<div class='controls'>
					<input type='text' name='zip' id='zip' placeholder='Zip' />
				</div>
			</div>

			<div class='control-group'>

				<label class='control-label' for='email'>Email</label>

				<div class='controls'>
					<input type='text' name='email' id='email' placeholder='Email' />
				</div>
			</div>

			<div class='control-group'>

				<label class='control-label' for='phone'>Phone</label>

				<div class='controls'>
					<input type='text' name='phone' id='phone' placeholder='Phone' />
				</div>
			</div>

		</form>

	</div>

</section>