<section class='container'>
	<h4>Sign In</h4>

	<form class='form-inline' method='post' action='<?= site_url('users/loginpost') ?>'>
		<input type="text" required class="input-small" placeholder="Username" name='username'>
		<input type="password" class="input-small" placeholder="Password" name='password'>

		<button type="submit" class="btn">Sign in</button>
	</form>

	<hr />

	<h2>Create Account</h2>

	<form name='createAccount' method='post' action='<?= site_url('users/signup') ?>' class='form-horizontal'>
		<div class='form-field-container clearfix'>
			<div class='span4'>
				<div class="control-group">
					<label class="control-label" for="FirstName">First Name</label>
					<div class="controls">
						<input type="text" required id="firstname" placeholder="First Name" name="FirstName">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="LastName">Last Name</label>
					<div class="controls">
						<input type="text" required id="lastname" placeholder="Last Name" name="LastName">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="Address1">Address</label>
					<div class="controls">
						<input type="text" required id="address" placeholder="Address" name="Address1">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="Address2">Address 2</label>
					<div class="controls">
						<input type="text" id="address2" placeholder="Address 2" name="Address2">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="City">City</label>
					<div class="controls">
						<input type="text" required id="city" placeholder="City" name="City">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="State">State</label>
					<div class="controls">
						<input type="text" class='input-small' maxlength='2' required id="state" placeholder="State" name="State">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="Zip">Zip</label>
					<div class="controls">
						<input type="text" required id="zip" placeholder="Zip" name="Zip" class='input-small' maxlength='5'>
					</div>
				</div>
			</div>

			<div class='span5'>
				<div class="control-group">
					<label class="control-label" for="Username">Username</label>
					<div class="controls">
						<input type="text" required id="username" placeholder="Username" name="Username">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="Password">Password</label>
					<div class="controls">
						<input type="password" required id="password" placeholder="Password" name="Password">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="PassConf">Confirm Password</label>
					<div class="controls">
						<input type="password" required id="passconf" placeholder="Confirm" name="PassConf">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="Email">Email</label>
					<div class="controls">
						<input type="email" required id="email" placeholder="Email" name="Email">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="Phone">Phone</label>
					<div class="controls">
						<input type="tel" required id="phone" placeholder="Phone" name="Phone" data-mask='999-999-9999'>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="Organization">Organization Name</label>
					<div class="controls">
						<input type="text" required id="organization" placeholder="Organization" name="Organization">
					</div>
				</div>
			</div>
		</div>

		<div class='text-center'>
			<button type='submit' class='btn'>Sign Up</button>
		</div>
	</form>
</div>