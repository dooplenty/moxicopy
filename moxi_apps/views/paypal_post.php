<html>
<head>
	<title>Processing payment...</title>
</head>
<body onload="document.forms['paypal_form'].submit();">
	<center>
		<h2>Please wait, your order is being processed and you will be redirected to the paypal website.</h2>
	</center>

	<form method='post' name='paypal_form' action='<?= $url ?>'>
		<?php foreach($fields as $field => $val): ?>
			<input type='hidden' name='<?= $field ?>' value='<?= $val ?>' />
		<?php endforeach; ?>
		<?= br(2) ?>
		<center>If you are not automatically redirected to paypal within 5 seconds <?= br(2) ?>
			<input type='submit' value='Click Here' />
		</center>
	</form>
</body>
</html>