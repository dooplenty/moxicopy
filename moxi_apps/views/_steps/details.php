<div id='details_container' class='product-details-container'>
	<form id='details_form' name='detailsForm' method='post' action='<?= site_url("order/save_details") ?>'>
		<input type='hidden' name='product_unique' value='<?= $this->product_manager->get_product_key() ?>' />
		<input type='hidden' name='product' value='<?= $this->product_manager->ProductID ?>' />

		<label><h6>Project Name</h6></label>
		<input type='text' name='project_name' placeholder='Enter project name...' class='required' value='<?= $this->order_manager->order_created() ? $this->order_manager->get_product($this->product_manager->get_product_key())->ProjectName : '' ?>'/>

		<?php
			$currentGroup = null;

			if($this->product_manager->Attributes) {
				foreach($this->product_manager->Attributes as $Attribute){
					if($Attribute->AttributeGroupName == 'Order'){
						continue;
					}

					if($Attribute->AttributeGroupID != $currentGroup){

						if(!is_null($currentGroup)){
							echo "</div>";
						}

						if(strtolower($Attribute->AttributeGroupName) == 'additional'){
							echo "<div class=' text-center'><a id='show_additional' class='act-primary' href='#'>Additional Options</a></div>";
						}
						echo "<div class='attribute-group " . strtolower($Attribute->AttributeGroupName) . "' id='group{$Attribute->AttributeGroupID}'>";

					}

					$defaultValue = $this->order_manager->get_attribute_value($this->product_manager->get_product_key(), $Attribute->ProductAttributeID);
					if(!$defaultValue){
						$defaultValue = $Attribute->DefaultValue;
					}

					/*if($Attribute->AttributeKey == 'inkcolorpages' && isset($orderProduct) && isset($orderProduct['Attributes']['color_' . $Attribute->ProductAttributeID])){
						$defaultValue = array(
							'color' => $orderProduct['Attributes']['color_' . $Attribute->ProductAttributeID],
							'bw' => $orderProduct['Attributes']['bw_' . $Attribute->ProductAttributeID]
						);
					}*/

					$attrData = array(
						'key' => $Attribute->AttributeKey,
						'attrid' => $Attribute->ProductAttributeID,
						'defaultValue' => $defaultValue,
						'required' => $Attribute->Required == 1 ? 'required' : ''
					);

					$this->load->view('_attrs/' . $Attribute->AttributeKey, $attrData);

					$currentGroup = $Attribute->AttributeGroupID;
				}

				echo "</div>";
			} else {
				echo "<h4 class='text-center'>Call for Special Pricing!</h4>";
				echo "<h5 class='text-center'>" . SUPPORT_NUMBER . "</h5>";
			}
		?>

		<h6>Product Notes</h6>
		<textarea name='product_notes' cols='7' rows='4' placeholder='Enter notes for this product...'><?= $this->product_manager->ProductNotes ?></textarea>
	</form>
</div>

<div class='text-center'>
	<!-- <span class='btn btn-primary next-step disabled' id='product_prev'>Prev</span> -->
	<span class='btn btn-primary add-to-cart continue-order' id='add_to_cart'>Add to cart</span>
	<!-- <span class='btn btn-primary next-step disabled' id='product_next'>Next</span> -->
</div>