<section class='container'>
	<div class='span9'>
		<?php if($this->session->flashdata('message')): ?>
			<p class='text-info muted well lead'><?= $this->session->flashdata('message') ?></p>
		<?php else: ?>
			<p class='text-info muted well lead'>It seems that you may have reached this page in error. Start <a href='<?= site_url() ?>'>here</a> to find the link you may have been looking for.</p>
		<?php endif; ?>
	</div>
</section>