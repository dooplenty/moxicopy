<?php

class MY_Model extends CI_Model {

    /**
     * DB Table name associated witht his model
     * @var string
     */
    protected $_tableName;

    /**
     * Codeigniter instance
     * @var CI_Controller
     */
    protected $_CI;

    public function MY_Model() {
        parent::__construct();

        $this->_CI =& get_instance();
    }

    /**
     * populate model with data in mixed $data
     * @param  mixed $data variables to populate
     * @return MY_Model  instance of populated model
     */
	public function populate($data) {
        // Allow objects to be populated
        if(is_object($data)) {
        	$data = get_object_vars($data);
        }

        //check to make sure an array was passed in
        if(is_array($data)) {
            // Populate Data
            foreach( $data as $key => $value ) {
                if (substr($key,0,1) == '_' ) continue;  // Filter Private/Protected

                $this->$key = $value;
            }
        } else {
            throw new RV_Model_Exception_NothingToPopulate(
                "No array could be derived from passed in data" );
        }

        // Method Chaining
        return $this;
	}

    /**
     * Convert object to array
     * @return array
     */
	public function to_array(){
		$me = $this;

        $publicOnly = function() use ($me) {
            return get_object_vars($me);
        };

        return $publicOnly();
	}

    /**
     * Save any dirty data or new data to db
     * @return void
     */
    public function save(){
        if(isset($this->_primaryID)){
            if($this->{$this->_primaryID}){
                $this->update();
            } else {
                $this->insert();
            }
        }
    }

    /**
     * Update database table associated with model
     * @return void
     */
    public function update(){
        $this->db->where($this->_primaryID, $this->{$this->_primaryID});
        $this->db->update($this->_tableName, $this->to_array());
    }

    /**
     * Insert new record into table
     * @return void
     */
    public function insert(){
        $this->db->insert($this->_tableName, $this->to_array());
        $this->{$this->_primaryID} = $this->db->insert_id();
    }

    /**
     * Perform active record get/where based on args passed in
     * Accepts two args as key/value or an array of key/value pairs
     * @return MY_Model instance of model populated with results
     */
    public function get_where(){
        $numArgs = func_num_args();

        if(isset($this->OrderBy)){
            $this->db->order_by($this->OrderBy);
        }

        if($numArgs == 2){
            $arg1 = func_get_arg(0);
            $arg2 = func_get_arg(1);

            if(is_array($arg2)){
                $this->db->where_in($arg1, $arg2);
            } else {
                $this->db->where($arg1, $arg2);
            }

        } else if($numArgs == 1){
            $arg = func_get_arg(0);

            //should be array
            if(is_array($arg)){
                foreach($arg as $key => $value){
                    $this->db->where($key, $value);
                }
            } else {
                throw new Exception('The get_where function expects 2 arguments of key, value or 1 array with 1 or more key value pairs');
            }
        } else {
            throw new Exception('The get_where function expects 2 arguments of key, value or 1 array with 1 or more key value pairs');
        }

        $query  = $this->db->get($this->_tableName);

        $result = $query->result_object();

        foreach($result as &$obj){
            $instance = clone $this;
            $instance->populate($obj);

            $obj = $instance;
        }

        return $result;
    }

    /**
     * Fetch all results form table
     * @return array array of results objects
     */
    public function fetch_all(){
        $query = $this->db->get($this->_tableName);
        return $query->result_object();
    }

    /**
     * Delete record passed on criteria
     * @return rows affected
     */
    public function delete_where(){
        $numArgs = func_num_args();

        if($numArgs == 2){
            $arg1 = func_get_arg(0);
            $arg2 = func_get_arg(1);

            if(is_array($arg2)){
                $this->db->where_in($arg1, $arg2);
            } else {
                $this->db->where($arg1, $arg2);
            }

        } else if($numArgs == 1){
            $arg = func_get_arg(0);

            //should be array
            if(is_array($arg)){
                foreach($arg as $key => $value){
                    $this->db->where($key, $value);
                }
            } else {
                throw new Exception('The get_where function expects 2 arguments of key, value or 1 array with 1 or more key value pairs');
            }
        } else {
            throw new Exception('The get_where function expects 2 arguments of key, value or 1 array with 1 or more key value pairs');
        }

        $query  = $this->db->delete($this->_tableName);

        return $this->db->affected_rows();
    }

    /**
     * Retrieve one record based on criteria
     * @return Object
     */
    public function get_one_where(){
        $numArgs = func_num_args();

        if($numArgs == 2){
            $result = $this->get_where(func_get_arg(0), func_get_arg(1));
        } else if($numArgs == 1){
            $result = $this->get_where(func_get_arg(0));
        } else {
            throw new Exception('The get_one_where function expects 2 arguments of key, value or 1 array with 1 or more key value pairs');
        }

        return !empty($result) ? $result[0] : $result;
    }

    /**
     * Perform batch insert of data
     * @param  array $data data to insert
     * @return int  affected rows
     */
    public function insert_batch($data){
        $query = $this->db->insert_batch($this->_tableName, $data);
        return $this->db->affected_rows();
    }

    /**
     * Merge one model with another
     * @param  MY_Model $Model instance of model to merge
     * @return MY_Model Merged model
     */
    public function merge($Model){

    }
}