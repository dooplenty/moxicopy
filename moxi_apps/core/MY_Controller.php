<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	protected $_data;

	public $user;

	public function __construct()
	{
		parent::__construct();

		// $this->output->enable_profiler(TRUE);

		// $this->_destroy_session();

		$this->_data['title'] = 'Moxicopy.com | Cheap Quality Copies';

		$this->_data['css_files'] = array(
			'bootstrap.css',
			'bootstrap-responsive.css',
			'main.css'
		);

		switch($this->uri->segment(1)) {
			case 'order':
				$this->_data['active'] = 'order';
				break;
			case 'admin':
				$this->_data['active'] = 'account';
				break;
			case 'page':
				$this->_data['active'] = $this->uri->segment(2);
				break;
			default:
				$this->_data['active'] = 'home';
		}

		$this->_data['logged_in'] = $this->_logged_in();

		$this->_data['header'] = $this->load->view('_blocks/header', $this->_data, true);
		$this->_data['footer'] = $this->load->view('_blocks/footer', null, true);

		if($this->uri->segment(3)){
			$this->_data['cart_row_id'] = $this->uri->segment(3);
		}

		if($this->_data['logged_in'] === true){
			$this->_data['user'] = $this->user = $this->session->userdata('user');
		}

		/**
		 * Yes duplicate descriptions are bad. This is set here so that you can fix your duplicates when you see them in web tools. 
		 * These should get overwritten in each of your rendering methods :).
		 */
		$this->_data['keywords'] = "color printing, commercial printing, business printing, printing service, printing services, color copies, color copy, full color printing, cheap printing";
		$this->_data['description'] = "Quality 5¢ Color Copies. Color Printing at the lowest prices in the industry. Fast & Friendly 24/7 Online Service. Free Shipping, Free Proof, Free Sample Kit!";
	}

	protected function _log_user($user){
		$this->session->set_userdata('user', $user);
		$this->_data['user'] = $user;
	}

	protected function _logged_in() {
		if($this->session->userdata('user')){
			return true;
		}

		return false;
	}

	protected function _destroy_session(){
		$this->session->sess_destroy();
	}

	protected function _move_files(){
		$ip = $this->session->userdata('ip_address');

		$target = $_SERVER['DOCUMENT_ROOT'] . "/fileuploads/users/" . $this->_data['user']->UserID . "/";

		//move all our products files over
		$orderid = $this->session->userdata('OrderID');

		if($orderid){
			$this->order_manager->init($orderid);
		}

		if($orderid){
			$Products = $this->order_manager->get_products();

			foreach($Products as $Product){
				$source = $_SERVER['DOCUMENT_ROOT'] . "/fileuploads/tmp/{$ip}/{$Product->ProductUID}";

				if (is_dir($source)) {
			    	if(!is_dir($target)){
			        	@mkdir($target, 0777, true);
			        }

			        /*$d = dir($source);
			        while (FALSE !== ( $entry = $d->read() )) {
			            if ($entry == '.' || $entry == '..') {
			                continue;
			            }
			            $Entry = $source . '/' . $entry;
			            if (is_dir($Entry)) {
			                $this->_move_files($Entry, $target . '/' . $entry);
			                continue;
			            }
			            copy($Entry, $target . '/' . $entry);
			        }

			        $d->close();*/

			        exec("mv $source $target");

			        foreach($Product->FileUpload as $File){
			        	$File->PreviewPath = str_replace("tmp/{$ip}", "users/" . $this->_data['user']->UserID, $File->PreviewPath);
			        	$File->save();
			        }
		    	}
			}
		}
	}
}