-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 10, 2014 at 02:47 AM
-- Server version: 5.1.44
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rmusk_moxicopy`
--

-- --------------------------------------------------------

--
-- Table structure for table `AttributeGroups`
--

CREATE TABLE IF NOT EXISTS `AttributeGroups` (
  `AttributeGroupID` int(11) NOT NULL AUTO_INCREMENT,
  `AttributeGroupName` varchar(50) NOT NULL,
  `AttributeGroupSortOrder` int(11) NOT NULL,
  PRIMARY KEY (`AttributeGroupID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `CardTypes`
--

CREATE TABLE IF NOT EXISTS `CardTypes` (
  `CardTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Type` varchar(20) DEFAULT NULL,
  `Display` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`CardTypeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `FileUploads`
--

CREATE TABLE IF NOT EXISTS `FileUploads` (
  `FileUploadID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL,
  `UserIP` varchar(30) DEFAULT NULL,
  `FileLocation` varchar(255) NOT NULL DEFAULT '',
  `FileSize` varchar(30) DEFAULT NULL,
  `OriginalFilename` varchar(255) DEFAULT NULL,
  `NewFilename` varchar(255) DEFAULT NULL,
  `Filetype` varchar(20) DEFAULT NULL,
  `FileHash` varchar(32) NOT NULL DEFAULT '',
  `Deleted` tinyint(4) DEFAULT '0',
  `UploadDateTime` datetime DEFAULT NULL,
  `DeletedDateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`FileUploadID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=163 ;

-- --------------------------------------------------------

--
-- Table structure for table `Foldings`
--

CREATE TABLE IF NOT EXISTS `Foldings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `moxi_sessions`
--

CREATE TABLE IF NOT EXISTS `moxi_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `OrderNotes`
--

CREATE TABLE IF NOT EXISTS `OrderNotes` (
  `OrderNoteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Note` text,
  `UserID` int(11) DEFAULT NULL,
  `DateTimeAdded` datetime DEFAULT NULL,
  `OrderID` int(11) DEFAULT NULL,
  PRIMARY KEY (`OrderNoteID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `Orders`
--

CREATE TABLE IF NOT EXISTS `Orders` (
  `OrderID` int(11) NOT NULL AUTO_INCREMENT,
  `StatusID` int(11) NOT NULL,
  `OrderDate` date DEFAULT NULL,
  `OrderDateTime` datetime DEFAULT NULL,
  `ProjectName` varchar(255) DEFAULT '',
  `UserID` int(11) DEFAULT NULL,
  `NumProducts` int(8) DEFAULT NULL,
  `DateAdded` date NOT NULL,
  `DateTimeAdded` datetime NOT NULL,
  `RushOrder` tinyint(4) DEFAULT '0',
  `Total` float DEFAULT NULL,
  `ShippingKey` varchar(50) DEFAULT NULL,
  `Subtotal` float DEFAULT NULL,
  `CancelDate` date DEFAULT NULL,
  `CancelDateTime` datetime DEFAULT NULL,
  `PaymentTypeID` int(4) DEFAULT NULL,
  `DateTimeUpdated` datetime DEFAULT NULL,
  `PrintingStatusID` int(11) DEFAULT NULL,
  `HardCopyProof` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`OrderID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28991 ;

-- --------------------------------------------------------

--
-- Table structure for table `Order_Attributes`
--

CREATE TABLE IF NOT EXISTS `Order_Attributes` (
  `OrderAttributeID` int(11) NOT NULL AUTO_INCREMENT,
  `OrderID` int(11) NOT NULL,
  `ProductUID` varchar(32) NOT NULL DEFAULT '',
  `AttributeID` int(11) NOT NULL,
  `Value` varchar(20) NOT NULL,
  PRIMARY KEY (`OrderAttributeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3360 ;

-- --------------------------------------------------------

--
-- Table structure for table `Order_BillingAddresses`
--

CREATE TABLE IF NOT EXISTS `Order_BillingAddresses` (
  `OrderBillingAddressID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `OrderID` int(11) NOT NULL,
  `Address1` varchar(255) NOT NULL DEFAULT '',
  `Address2` varchar(255) DEFAULT NULL,
  `City` varchar(80) NOT NULL DEFAULT '',
  `State` varchar(80) NOT NULL DEFAULT '',
  `Zip` varchar(10) NOT NULL DEFAULT '',
  `FirstName` varchar(255) DEFAULT NULL,
  `LastName` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`OrderBillingAddressID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=86 ;

-- --------------------------------------------------------

--
-- Table structure for table `Order_FileUploads`
--

CREATE TABLE IF NOT EXISTS `Order_FileUploads` (
  `OrderFileUploadID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FileUploadID` int(11) DEFAULT NULL,
  `OrderProductID` int(11) DEFAULT NULL,
  `PreviewPath` varchar(255) DEFAULT NULL,
  `RootPath` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OrderFileUploadID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=84 ;

-- --------------------------------------------------------

--
-- Table structure for table `Order_Products`
--

CREATE TABLE IF NOT EXISTS `Order_Products` (
  `OrderProductID` int(11) NOT NULL AUTO_INCREMENT,
  `OrderID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `ProductUID` varchar(32) NOT NULL DEFAULT '',
  `Quantity` int(11) DEFAULT NULL,
  `ProductSizeID` int(11) DEFAULT NULL,
  `FileUploadID` int(11) DEFAULT NULL,
  `ProductNotes` text,
  `NumSheets` int(11) DEFAULT NULL,
  `ProjectName` varchar(255) DEFAULT NULL,
  `NumSheetsColor` int(11) DEFAULT NULL,
  `NumSheetsBW` int(11) DEFAULT NULL,
  PRIMARY KEY (`OrderProductID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=414 ;

-- --------------------------------------------------------

--
-- Table structure for table `Order_ShippingAddresses`
--

CREATE TABLE IF NOT EXISTS `Order_ShippingAddresses` (
  `OrderShippingAddressID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `OrderID` int(11) NOT NULL,
  `Address1` varchar(255) NOT NULL DEFAULT '',
  `Address2` varchar(255) DEFAULT NULL,
  `City` varchar(80) NOT NULL DEFAULT '',
  `State` varchar(80) NOT NULL DEFAULT '',
  `Zip` varchar(10) NOT NULL DEFAULT '',
  `FirstName` varchar(255) DEFAULT NULL,
  `LastName` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`OrderShippingAddressID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

-- --------------------------------------------------------

--
-- Table structure for table `Order_ShippingRates`
--

CREATE TABLE IF NOT EXISTS `Order_ShippingRates` (
  `OrderShippingRateID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `OrderID` int(11) NOT NULL,
  `HighestSeverity` varchar(50) DEFAULT NULL,
  `Notifications` text,
  `ServiceType` varchar(50) DEFAULT NULL,
  `PackagingType` varchar(50) DEFAULT NULL,
  `ActualRateType` varchar(50) DEFAULT NULL,
  `TotalBillingWeight` varchar(20) DEFAULT NULL,
  `TotalBaseCharge` float DEFAULT NULL,
  `TotalSurcharges` float DEFAULT NULL,
  `TotalNetFedexCharge` float DEFAULT NULL,
  `TotalTaxes` float DEFAULT NULL,
  `TotalNetCharge` float DEFAULT NULL,
  `TotalRebates` float DEFAULT NULL,
  `Surcharges` text,
  `ShippingPaid` float DEFAULT NULL,
  PRIMARY KEY (`OrderShippingRateID`),
  UNIQUE KEY `OrderID` (`OrderID`,`PackagingType`,`ServiceType`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=420 ;

-- --------------------------------------------------------

--
-- Table structure for table `PageElements`
--

CREATE TABLE IF NOT EXISTS `PageElements` (
  `PageElementID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(225) DEFAULT NULL,
  `Text` text,
  `Header` varchar(225) DEFAULT NULL,
  `Key` varchar(80) NOT NULL DEFAULT '',
  `SubHeader` varchar(225) DEFAULT NULL,
  `PreviousUrl` varchar(225) DEFAULT NULL,
  `MetaDescription` text,
  `MetaKeywords` text,
  PRIMARY KEY (`PageElementID`),
  UNIQUE KEY `Key` (`Key`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

-- --------------------------------------------------------

--
-- Table structure for table `PaperTypes`
--

CREATE TABLE IF NOT EXISTS `PaperTypes` (
  `PaperTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PaperTypeName` varchar(10) DEFAULT NULL,
  `PaperTypeDescription` varchar(50) DEFAULT NULL,
  `PaperTypeCategory` enum('Uncoated Stocks','Coated Stocks','Color Paper Options') DEFAULT NULL,
  `Weight` float DEFAULT NULL,
  `Multiplier` float DEFAULT NULL,
  `Active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`PaperTypeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `PaymentTransactions`
--

CREATE TABLE IF NOT EXISTS `PaymentTransactions` (
  `PaymentTransactionID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `OrderID` int(11) DEFAULT NULL,
  `Result` tinyint(4) DEFAULT NULL,
  `ResultText` int(11) DEFAULT NULL,
  `Amount` float DEFAULT NULL,
  `TransactionData` text,
  `DateTimeAdded` datetime DEFAULT NULL,
  `TransactionLength` float DEFAULT NULL,
  `PaymentTypeID` int(11) DEFAULT NULL,
  `TrackingID` int(11) DEFAULT NULL,
  PRIMARY KEY (`PaymentTransactionID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

-- --------------------------------------------------------

--
-- Table structure for table `PaymentTypes`
--

CREATE TABLE IF NOT EXISTS `PaymentTypes` (
  `PaymentTypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PaymentType` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PaymentTypeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `PrintingStatus`
--

CREATE TABLE IF NOT EXISTS `PrintingStatus` (
  `PrintingStatusID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PrintingStatusKey` varchar(50) DEFAULT NULL,
  `PrintingStatusDisplay` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PrintingStatusID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

-- --------------------------------------------------------

--
-- Table structure for table `PrintingStatusHistory`
--

CREATE TABLE IF NOT EXISTS `PrintingStatusHistory` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ProductAttributes`
--

CREATE TABLE IF NOT EXISTS `ProductAttributes` (
  `ProductAttributeID` int(10) NOT NULL AUTO_INCREMENT,
  `AttributeName` varchar(30) NOT NULL,
  `AttributeKey` varchar(30) NOT NULL,
  `Required` tinyint(4) DEFAULT NULL,
  `DefaultValue` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ProductAttributeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

-- --------------------------------------------------------

--
-- Table structure for table `ProductAttributes_AttributeGroups`
--

CREATE TABLE IF NOT EXISTS `ProductAttributes_AttributeGroups` (
  `ProductAttributesGroupID` int(11) NOT NULL AUTO_INCREMENT,
  `AttributeGroupID` int(11) NOT NULL,
  `ProductAttributeID` int(11) NOT NULL,
  `SortOrder` float NOT NULL,
  PRIMARY KEY (`ProductAttributesGroupID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

-- --------------------------------------------------------

--
-- Table structure for table `ProductAttributeValues`
--

CREATE TABLE IF NOT EXISTS `ProductAttributeValues` (
  `ProductAttributeValueID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ProductAttributeID` int(11) NOT NULL,
  `ProductAttributeValue` varchar(50) DEFAULT NULL,
  `ValuePricePerUnit` float DEFAULT NULL,
  `Multiplier` float DEFAULT NULL,
  PRIMARY KEY (`ProductAttributeValueID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

-- --------------------------------------------------------

--
-- Table structure for table `ProductCategories`
--

CREATE TABLE IF NOT EXISTS `ProductCategories` (
  `ProductCategoryID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ProductCategoryName` varchar(50) NOT NULL DEFAULT '',
  `Active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ProductCategoryID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `ProductMeta`
--

CREATE TABLE IF NOT EXISTS `ProductMeta` (
  `ProductMetaID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ProductID` int(11) DEFAULT NULL,
  `MetaDescription` text,
  `MetaKeywords` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ProductMetaID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `ProductPricing`
--

CREATE TABLE IF NOT EXISTS `ProductPricing` (
  `ProductPricingID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductID` int(11) NOT NULL,
  `UnitPrice` float NOT NULL,
  `Quantity` int(11) NOT NULL,
  PRIMARY KEY (`ProductPricingID`),
  UNIQUE KEY `product_price_quantity` (`ProductID`,`UnitPrice`,`Quantity`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=360 ;

-- --------------------------------------------------------

--
-- Table structure for table `Products`
--

CREATE TABLE IF NOT EXISTS `Products` (
  `ProductID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ProductName` varchar(50) NOT NULL DEFAULT '',
  `ProductCategoryID` int(11) NOT NULL,
  `FeaturedProduct` tinyint(4) NOT NULL DEFAULT '0',
  `ShowInNav` tinyint(4) NOT NULL DEFAULT '0',
  `DefaultQuantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`ProductID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

-- --------------------------------------------------------

--
-- Table structure for table `ProductSizes`
--

CREATE TABLE IF NOT EXISTS `ProductSizes` (
  `ProductSizeID` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `SizeX` decimal(5,2) NOT NULL DEFAULT '0.00',
  `SizeY` decimal(5,2) NOT NULL DEFAULT '0.00',
  `SizeName` varchar(20) DEFAULT NULL,
  `PreviewWidth` int(8) DEFAULT NULL,
  `PreviewHeight` int(8) DEFAULT NULL,
  `Multiplier` float DEFAULT NULL,
  `WeightFactor` float DEFAULT NULL,
  `FinishedSize` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ProductSizeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `Product_PaperTypes`
--

CREATE TABLE IF NOT EXISTS `Product_PaperTypes` (
  `Product_PaperTypeID` int(10) NOT NULL AUTO_INCREMENT,
  `ProductID` int(10) NOT NULL,
  `PaperTypeID` int(10) NOT NULL,
  `Active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Product_PaperTypeID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Product_ProductAttributes`
--

CREATE TABLE IF NOT EXISTS `Product_ProductAttributes` (
  `Product_ProductAttributeID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductAttributeID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `Active` tinyint(4) NOT NULL DEFAULT '1',
  `SortOrder` int(4) DEFAULT NULL,
  PRIMARY KEY (`Product_ProductAttributeID`),
  UNIQUE KEY `Product_ProductAttributeUnique` (`ProductAttributeID`,`ProductID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2325 ;

-- --------------------------------------------------------

--
-- Table structure for table `Product_ProductSizes`
--

CREATE TABLE IF NOT EXISTS `Product_ProductSizes` (
  `Product_ProductSizeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ProductID` int(8) DEFAULT NULL,
  `ProductSizeID` int(8) DEFAULT NULL,
  PRIMARY KEY (`Product_ProductSizeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=98 ;

-- --------------------------------------------------------

--
-- Table structure for table `ShippingRates`
--

CREATE TABLE IF NOT EXISTS `ShippingRates` (
  `ShippingRateID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Key` varchar(50) NOT NULL DEFAULT '',
  `Display` varchar(225) NOT NULL DEFAULT '',
  PRIMARY KEY (`ShippingRateID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Statuses`
--

CREATE TABLE IF NOT EXISTS `Statuses` (
  `StatusID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `StatusKey` varchar(20) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`StatusID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `UserInfo`
--

CREATE TABLE IF NOT EXISTS `UserInfo` (
  `UserInfoID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) unsigned NOT NULL,
  `FirstName` varchar(50) DEFAULT '',
  `LastName` varchar(50) DEFAULT '',
  `Email` varchar(255) DEFAULT NULL,
  `Address1` varchar(255) DEFAULT NULL,
  `Address2` varchar(255) DEFAULT NULL,
  `City` varchar(50) DEFAULT NULL,
  `State` varchar(50) DEFAULT NULL,
  `Zip` varchar(15) DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `Notes` text,
  `Organization` varchar(255) DEFAULT '',
  PRIMARY KEY (`UserInfoID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19562 ;

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `UserID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Username` varchar(30) NOT NULL DEFAULT '',
  `Password` varchar(32) NOT NULL DEFAULT '',
  `Level` int(8) DEFAULT '1',
  `LastLoggedIn` datetime DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `unique_user` (`Username`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19577 ;
