<?php
/**
 * File for class MytestEnumReturnType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumReturnType originally named ReturnType
 * Documentation : The type of return shipment that is being requested.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumReturnType extends MytestWsdlClass
{
	/**
	 * Constant for value 'FEDEX_TAG'
	 * @return string 'FEDEX_TAG'
	 */
	const VALUE_FEDEX_TAG = 'FEDEX_TAG';
	/**
	 * Constant for value 'PENDING'
	 * @return string 'PENDING'
	 */
	const VALUE_PENDING = 'PENDING';
	/**
	 * Constant for value 'PRINT_RETURN_LABEL'
	 * @return string 'PRINT_RETURN_LABEL'
	 */
	const VALUE_PRINT_RETURN_LABEL = 'PRINT_RETURN_LABEL';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumReturnType::VALUE_FEDEX_TAG
	 * @uses MytestEnumReturnType::VALUE_PENDING
	 * @uses MytestEnumReturnType::VALUE_PRINT_RETURN_LABEL
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumReturnType::VALUE_FEDEX_TAG,MytestEnumReturnType::VALUE_PENDING,MytestEnumReturnType::VALUE_PRINT_RETURN_LABEL));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>