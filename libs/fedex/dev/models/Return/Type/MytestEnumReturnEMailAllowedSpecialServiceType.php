<?php
/**
 * File for class MytestEnumReturnEMailAllowedSpecialServiceType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumReturnEMailAllowedSpecialServiceType originally named ReturnEMailAllowedSpecialServiceType
 * Documentation : These values are used to control the availability of certain special services at the time when a customer uses the e-mail label link to create a return shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumReturnEMailAllowedSpecialServiceType extends MytestWsdlClass
{
	/**
	 * Constant for value 'SATURDAY_DELIVERY'
	 * @return string 'SATURDAY_DELIVERY'
	 */
	const VALUE_SATURDAY_DELIVERY = 'SATURDAY_DELIVERY';
	/**
	 * Constant for value 'SATURDAY_PICKUP'
	 * @return string 'SATURDAY_PICKUP'
	 */
	const VALUE_SATURDAY_PICKUP = 'SATURDAY_PICKUP';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumReturnEMailAllowedSpecialServiceType::VALUE_SATURDAY_DELIVERY
	 * @uses MytestEnumReturnEMailAllowedSpecialServiceType::VALUE_SATURDAY_PICKUP
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumReturnEMailAllowedSpecialServiceType::VALUE_SATURDAY_DELIVERY,MytestEnumReturnEMailAllowedSpecialServiceType::VALUE_SATURDAY_PICKUP));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>