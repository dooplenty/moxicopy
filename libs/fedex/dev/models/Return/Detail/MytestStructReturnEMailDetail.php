<?php
/**
 * File for class MytestStructReturnEMailDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructReturnEMailDetail originally named ReturnEMailDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructReturnEMailDetail extends MytestWsdlClass
{
	/**
	 * The MerchantPhoneNumber
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $MerchantPhoneNumber;
	/**
	 * The AllowedSpecialServices
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the allowed (merchant-authorized) special services which may be selected when the subsequent shipment is created. Only services represented in EMailLabelAllowedSpecialServiceType will be controlled by this list.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumReturnEMailAllowedSpecialServiceType
	 */
	public $AllowedSpecialServices;
	/**
	 * Constructor method for ReturnEMailDetail
	 * @see parent::__construct()
	 * @param string $_merchantPhoneNumber
	 * @param MytestEnumReturnEMailAllowedSpecialServiceType $_allowedSpecialServices
	 * @return MytestStructReturnEMailDetail
	 */
	public function __construct($_merchantPhoneNumber = NULL,$_allowedSpecialServices = NULL)
	{
		parent::__construct(array('MerchantPhoneNumber'=>$_merchantPhoneNumber,'AllowedSpecialServices'=>$_allowedSpecialServices));
	}
	/**
	 * Get MerchantPhoneNumber value
	 * @return string|null
	 */
	public function getMerchantPhoneNumber()
	{
		return $this->MerchantPhoneNumber;
	}
	/**
	 * Set MerchantPhoneNumber value
	 * @param string $_merchantPhoneNumber the MerchantPhoneNumber
	 * @return string
	 */
	public function setMerchantPhoneNumber($_merchantPhoneNumber)
	{
		return ($this->MerchantPhoneNumber = $_merchantPhoneNumber);
	}
	/**
	 * Get AllowedSpecialServices value
	 * @return MytestEnumReturnEMailAllowedSpecialServiceType|null
	 */
	public function getAllowedSpecialServices()
	{
		return $this->AllowedSpecialServices;
	}
	/**
	 * Set AllowedSpecialServices value
	 * @uses MytestEnumReturnEMailAllowedSpecialServiceType::valueIsValid()
	 * @param MytestEnumReturnEMailAllowedSpecialServiceType $_allowedSpecialServices the AllowedSpecialServices
	 * @return MytestEnumReturnEMailAllowedSpecialServiceType
	 */
	public function setAllowedSpecialServices($_allowedSpecialServices)
	{
		if(!MytestEnumReturnEMailAllowedSpecialServiceType::valueIsValid($_allowedSpecialServices))
		{
			return false;
		}
		return ($this->AllowedSpecialServices = $_allowedSpecialServices);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructReturnEMailDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>