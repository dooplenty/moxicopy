<?php
/**
 * File for class MytestStructReturnShipmentDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructReturnShipmentDetail originally named ReturnShipmentDetail
 * Documentation : Information relating to a return shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructReturnShipmentDetail extends MytestWsdlClass
{
	/**
	 * The ReturnType
	 * Meta informations extracted from the WSDL
	 * - documentation : The type of return shipment that is being requested. At present the only type of retrun shipment that is supported is PRINT_RETURN_LABEL. With this option you can print a return label to insert into the box of an outbound shipment. This option can not be used to print an outbound label.
	 * - minOccurs : 1
	 * @var MytestEnumReturnType
	 */
	public $ReturnType;
	/**
	 * The Rma
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructRma
	 */
	public $Rma;
	/**
	 * The ReturnEMailDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructReturnEMailDetail
	 */
	public $ReturnEMailDetail;
	/**
	 * The ReturnAssociation
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructReturnAssociationDetail
	 */
	public $ReturnAssociation;
	/**
	 * Constructor method for ReturnShipmentDetail
	 * @see parent::__construct()
	 * @param MytestEnumReturnType $_returnType
	 * @param MytestStructRma $_rma
	 * @param MytestStructReturnEMailDetail $_returnEMailDetail
	 * @param MytestStructReturnAssociationDetail $_returnAssociation
	 * @return MytestStructReturnShipmentDetail
	 */
	public function __construct($_returnType,$_rma = NULL,$_returnEMailDetail = NULL,$_returnAssociation = NULL)
	{
		parent::__construct(array('ReturnType'=>$_returnType,'Rma'=>$_rma,'ReturnEMailDetail'=>$_returnEMailDetail,'ReturnAssociation'=>$_returnAssociation));
	}
	/**
	 * Get ReturnType value
	 * @return MytestEnumReturnType
	 */
	public function getReturnType()
	{
		return $this->ReturnType;
	}
	/**
	 * Set ReturnType value
	 * @uses MytestEnumReturnType::valueIsValid()
	 * @param MytestEnumReturnType $_returnType the ReturnType
	 * @return MytestEnumReturnType
	 */
	public function setReturnType($_returnType)
	{
		if(!MytestEnumReturnType::valueIsValid($_returnType))
		{
			return false;
		}
		return ($this->ReturnType = $_returnType);
	}
	/**
	 * Get Rma value
	 * @return MytestStructRma|null
	 */
	public function getRma()
	{
		return $this->Rma;
	}
	/**
	 * Set Rma value
	 * @param MytestStructRma $_rma the Rma
	 * @return MytestStructRma
	 */
	public function setRma($_rma)
	{
		return ($this->Rma = $_rma);
	}
	/**
	 * Get ReturnEMailDetail value
	 * @return MytestStructReturnEMailDetail|null
	 */
	public function getReturnEMailDetail()
	{
		return $this->ReturnEMailDetail;
	}
	/**
	 * Set ReturnEMailDetail value
	 * @param MytestStructReturnEMailDetail $_returnEMailDetail the ReturnEMailDetail
	 * @return MytestStructReturnEMailDetail
	 */
	public function setReturnEMailDetail($_returnEMailDetail)
	{
		return ($this->ReturnEMailDetail = $_returnEMailDetail);
	}
	/**
	 * Get ReturnAssociation value
	 * @return MytestStructReturnAssociationDetail|null
	 */
	public function getReturnAssociation()
	{
		return $this->ReturnAssociation;
	}
	/**
	 * Set ReturnAssociation value
	 * @param MytestStructReturnAssociationDetail $_returnAssociation the ReturnAssociation
	 * @return MytestStructReturnAssociationDetail
	 */
	public function setReturnAssociation($_returnAssociation)
	{
		return ($this->ReturnAssociation = $_returnAssociation);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructReturnShipmentDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>