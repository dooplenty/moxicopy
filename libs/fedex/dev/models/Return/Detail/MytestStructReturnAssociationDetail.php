<?php
/**
 * File for class MytestStructReturnAssociationDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructReturnAssociationDetail originally named ReturnAssociationDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructReturnAssociationDetail extends MytestWsdlClass
{
	/**
	 * The TrackingNumber
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the tracking number of the master associated with the return shipment.
	 * - minOccurs : 0
	 * @var string
	 */
	public $TrackingNumber;
	/**
	 * The ShipDate
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var date
	 */
	public $ShipDate;
	/**
	 * Constructor method for ReturnAssociationDetail
	 * @see parent::__construct()
	 * @param string $_trackingNumber
	 * @param date $_shipDate
	 * @return MytestStructReturnAssociationDetail
	 */
	public function __construct($_trackingNumber = NULL,$_shipDate = NULL)
	{
		parent::__construct(array('TrackingNumber'=>$_trackingNumber,'ShipDate'=>$_shipDate));
	}
	/**
	 * Get TrackingNumber value
	 * @return string|null
	 */
	public function getTrackingNumber()
	{
		return $this->TrackingNumber;
	}
	/**
	 * Set TrackingNumber value
	 * @param string $_trackingNumber the TrackingNumber
	 * @return string
	 */
	public function setTrackingNumber($_trackingNumber)
	{
		return ($this->TrackingNumber = $_trackingNumber);
	}
	/**
	 * Get ShipDate value
	 * @return date|null
	 */
	public function getShipDate()
	{
		return $this->ShipDate;
	}
	/**
	 * Set ShipDate value
	 * @param date $_shipDate the ShipDate
	 * @return date
	 */
	public function setShipDate($_shipDate)
	{
		return ($this->ShipDate = $_shipDate);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructReturnAssociationDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>