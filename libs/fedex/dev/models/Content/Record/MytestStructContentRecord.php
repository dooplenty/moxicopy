<?php
/**
 * File for class MytestStructContentRecord
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructContentRecord originally named ContentRecord
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructContentRecord extends MytestWsdlClass
{
	/**
	 * The PartNumber
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $PartNumber;
	/**
	 * The ItemNumber
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $ItemNumber;
	/**
	 * The ReceivedQuantity
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $ReceivedQuantity;
	/**
	 * The Description
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Description;
	/**
	 * Constructor method for ContentRecord
	 * @see parent::__construct()
	 * @param string $_partNumber
	 * @param string $_itemNumber
	 * @param nonNegativeInteger $_receivedQuantity
	 * @param string $_description
	 * @return MytestStructContentRecord
	 */
	public function __construct($_partNumber = NULL,$_itemNumber = NULL,$_receivedQuantity = NULL,$_description = NULL)
	{
		parent::__construct(array('PartNumber'=>$_partNumber,'ItemNumber'=>$_itemNumber,'ReceivedQuantity'=>$_receivedQuantity,'Description'=>$_description));
	}
	/**
	 * Get PartNumber value
	 * @return string|null
	 */
	public function getPartNumber()
	{
		return $this->PartNumber;
	}
	/**
	 * Set PartNumber value
	 * @param string $_partNumber the PartNumber
	 * @return string
	 */
	public function setPartNumber($_partNumber)
	{
		return ($this->PartNumber = $_partNumber);
	}
	/**
	 * Get ItemNumber value
	 * @return string|null
	 */
	public function getItemNumber()
	{
		return $this->ItemNumber;
	}
	/**
	 * Set ItemNumber value
	 * @param string $_itemNumber the ItemNumber
	 * @return string
	 */
	public function setItemNumber($_itemNumber)
	{
		return ($this->ItemNumber = $_itemNumber);
	}
	/**
	 * Get ReceivedQuantity value
	 * @return nonNegativeInteger|null
	 */
	public function getReceivedQuantity()
	{
		return $this->ReceivedQuantity;
	}
	/**
	 * Set ReceivedQuantity value
	 * @param nonNegativeInteger $_receivedQuantity the ReceivedQuantity
	 * @return nonNegativeInteger
	 */
	public function setReceivedQuantity($_receivedQuantity)
	{
		return ($this->ReceivedQuantity = $_receivedQuantity);
	}
	/**
	 * Get Description value
	 * @return string|null
	 */
	public function getDescription()
	{
		return $this->Description;
	}
	/**
	 * Set Description value
	 * @param string $_description the Description
	 * @return string
	 */
	public function setDescription($_description)
	{
		return ($this->Description = $_description);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructContentRecord
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>