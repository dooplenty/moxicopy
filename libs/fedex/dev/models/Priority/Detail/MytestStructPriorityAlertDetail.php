<?php
/**
 * File for class MytestStructPriorityAlertDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructPriorityAlertDetail originally named PriorityAlertDetail
 * Documentation : Currently not supported.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructPriorityAlertDetail extends MytestWsdlClass
{
	/**
	 * The EnhancementTypes
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumPriorityAlertEnhancementType
	 */
	public $EnhancementTypes;
	/**
	 * The Content
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : 3
	 * - minOccurs : 0
	 * @var string
	 */
	public $Content;
	/**
	 * Constructor method for PriorityAlertDetail
	 * @see parent::__construct()
	 * @param MytestEnumPriorityAlertEnhancementType $_enhancementTypes
	 * @param string $_content
	 * @return MytestStructPriorityAlertDetail
	 */
	public function __construct($_enhancementTypes = NULL,$_content = NULL)
	{
		parent::__construct(array('EnhancementTypes'=>$_enhancementTypes,'Content'=>$_content));
	}
	/**
	 * Get EnhancementTypes value
	 * @return MytestEnumPriorityAlertEnhancementType|null
	 */
	public function getEnhancementTypes()
	{
		return $this->EnhancementTypes;
	}
	/**
	 * Set EnhancementTypes value
	 * @uses MytestEnumPriorityAlertEnhancementType::valueIsValid()
	 * @param MytestEnumPriorityAlertEnhancementType $_enhancementTypes the EnhancementTypes
	 * @return MytestEnumPriorityAlertEnhancementType
	 */
	public function setEnhancementTypes($_enhancementTypes)
	{
		if(!MytestEnumPriorityAlertEnhancementType::valueIsValid($_enhancementTypes))
		{
			return false;
		}
		return ($this->EnhancementTypes = $_enhancementTypes);
	}
	/**
	 * Get Content value
	 * @return string|null
	 */
	public function getContent()
	{
		return $this->Content;
	}
	/**
	 * Set Content value
	 * @param string $_content the Content
	 * @return string
	 */
	public function setContent($_content)
	{
		return ($this->Content = $_content);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructPriorityAlertDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>