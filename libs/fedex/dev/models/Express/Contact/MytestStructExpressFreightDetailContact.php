<?php
/**
 * File for class MytestStructExpressFreightDetailContact
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructExpressFreightDetailContact originally named ExpressFreightDetailContact
 * Documentation : Currently not supported. Delivery contact information for an Express freight shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructExpressFreightDetailContact extends MytestWsdlClass
{
	/**
	 * The Name
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Name;
	/**
	 * The Phone
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Phone;
	/**
	 * Constructor method for ExpressFreightDetailContact
	 * @see parent::__construct()
	 * @param string $_name
	 * @param string $_phone
	 * @return MytestStructExpressFreightDetailContact
	 */
	public function __construct($_name = NULL,$_phone = NULL)
	{
		parent::__construct(array('Name'=>$_name,'Phone'=>$_phone));
	}
	/**
	 * Get Name value
	 * @return string|null
	 */
	public function getName()
	{
		return $this->Name;
	}
	/**
	 * Set Name value
	 * @param string $_name the Name
	 * @return string
	 */
	public function setName($_name)
	{
		return ($this->Name = $_name);
	}
	/**
	 * Get Phone value
	 * @return string|null
	 */
	public function getPhone()
	{
		return $this->Phone;
	}
	/**
	 * Set Phone value
	 * @param string $_phone the Phone
	 * @return string
	 */
	public function setPhone($_phone)
	{
		return ($this->Phone = $_phone);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructExpressFreightDetailContact
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>