<?php
/**
 * File for class MytestEnumExpressRegionCode
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumExpressRegionCode originally named ExpressRegionCode
 * Documentation : Indicates a FedEx Express operating region.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumExpressRegionCode extends MytestWsdlClass
{
	/**
	 * Constant for value 'APAC'
	 * @return string 'APAC'
	 */
	const VALUE_APAC = 'APAC';
	/**
	 * Constant for value 'CA'
	 * @return string 'CA'
	 */
	const VALUE_CA = 'CA';
	/**
	 * Constant for value 'EMEA'
	 * @return string 'EMEA'
	 */
	const VALUE_EMEA = 'EMEA';
	/**
	 * Constant for value 'LAC'
	 * @return string 'LAC'
	 */
	const VALUE_LAC = 'LAC';
	/**
	 * Constant for value 'US'
	 * @return string 'US'
	 */
	const VALUE_US = 'US';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumExpressRegionCode::VALUE_APAC
	 * @uses MytestEnumExpressRegionCode::VALUE_CA
	 * @uses MytestEnumExpressRegionCode::VALUE_EMEA
	 * @uses MytestEnumExpressRegionCode::VALUE_LAC
	 * @uses MytestEnumExpressRegionCode::VALUE_US
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumExpressRegionCode::VALUE_APAC,MytestEnumExpressRegionCode::VALUE_CA,MytestEnumExpressRegionCode::VALUE_EMEA,MytestEnumExpressRegionCode::VALUE_LAC,MytestEnumExpressRegionCode::VALUE_US));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>