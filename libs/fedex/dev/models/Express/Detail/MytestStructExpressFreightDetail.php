<?php
/**
 * File for class MytestStructExpressFreightDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructExpressFreightDetail originally named ExpressFreightDetail
 * Documentation : Details specific to an Express freight shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructExpressFreightDetail extends MytestWsdlClass
{
	/**
	 * The PackingListEnclosed
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var boolean
	 */
	public $PackingListEnclosed;
	/**
	 * The ShippersLoadAndCount
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var positiveInteger
	 */
	public $ShippersLoadAndCount;
	/**
	 * The BookingConfirmationNumber
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $BookingConfirmationNumber;
	/**
	 * The ReferenceLabelRequested
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var boolean
	 */
	public $ReferenceLabelRequested;
	/**
	 * The BeforeDeliveryContact
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructExpressFreightDetailContact
	 */
	public $BeforeDeliveryContact;
	/**
	 * The UndeliverableContact
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructExpressFreightDetailContact
	 */
	public $UndeliverableContact;
	/**
	 * Constructor method for ExpressFreightDetail
	 * @see parent::__construct()
	 * @param boolean $_packingListEnclosed
	 * @param positiveInteger $_shippersLoadAndCount
	 * @param string $_bookingConfirmationNumber
	 * @param boolean $_referenceLabelRequested
	 * @param MytestStructExpressFreightDetailContact $_beforeDeliveryContact
	 * @param MytestStructExpressFreightDetailContact $_undeliverableContact
	 * @return MytestStructExpressFreightDetail
	 */
	public function __construct($_packingListEnclosed = NULL,$_shippersLoadAndCount = NULL,$_bookingConfirmationNumber = NULL,$_referenceLabelRequested = NULL,$_beforeDeliveryContact = NULL,$_undeliverableContact = NULL)
	{
		parent::__construct(array('PackingListEnclosed'=>$_packingListEnclosed,'ShippersLoadAndCount'=>$_shippersLoadAndCount,'BookingConfirmationNumber'=>$_bookingConfirmationNumber,'ReferenceLabelRequested'=>$_referenceLabelRequested,'BeforeDeliveryContact'=>$_beforeDeliveryContact,'UndeliverableContact'=>$_undeliverableContact));
	}
	/**
	 * Get PackingListEnclosed value
	 * @return boolean|null
	 */
	public function getPackingListEnclosed()
	{
		return $this->PackingListEnclosed;
	}
	/**
	 * Set PackingListEnclosed value
	 * @param boolean $_packingListEnclosed the PackingListEnclosed
	 * @return boolean
	 */
	public function setPackingListEnclosed($_packingListEnclosed)
	{
		return ($this->PackingListEnclosed = $_packingListEnclosed);
	}
	/**
	 * Get ShippersLoadAndCount value
	 * @return positiveInteger|null
	 */
	public function getShippersLoadAndCount()
	{
		return $this->ShippersLoadAndCount;
	}
	/**
	 * Set ShippersLoadAndCount value
	 * @param positiveInteger $_shippersLoadAndCount the ShippersLoadAndCount
	 * @return positiveInteger
	 */
	public function setShippersLoadAndCount($_shippersLoadAndCount)
	{
		return ($this->ShippersLoadAndCount = $_shippersLoadAndCount);
	}
	/**
	 * Get BookingConfirmationNumber value
	 * @return string|null
	 */
	public function getBookingConfirmationNumber()
	{
		return $this->BookingConfirmationNumber;
	}
	/**
	 * Set BookingConfirmationNumber value
	 * @param string $_bookingConfirmationNumber the BookingConfirmationNumber
	 * @return string
	 */
	public function setBookingConfirmationNumber($_bookingConfirmationNumber)
	{
		return ($this->BookingConfirmationNumber = $_bookingConfirmationNumber);
	}
	/**
	 * Get ReferenceLabelRequested value
	 * @return boolean|null
	 */
	public function getReferenceLabelRequested()
	{
		return $this->ReferenceLabelRequested;
	}
	/**
	 * Set ReferenceLabelRequested value
	 * @param boolean $_referenceLabelRequested the ReferenceLabelRequested
	 * @return boolean
	 */
	public function setReferenceLabelRequested($_referenceLabelRequested)
	{
		return ($this->ReferenceLabelRequested = $_referenceLabelRequested);
	}
	/**
	 * Get BeforeDeliveryContact value
	 * @return MytestStructExpressFreightDetailContact|null
	 */
	public function getBeforeDeliveryContact()
	{
		return $this->BeforeDeliveryContact;
	}
	/**
	 * Set BeforeDeliveryContact value
	 * @param MytestStructExpressFreightDetailContact $_beforeDeliveryContact the BeforeDeliveryContact
	 * @return MytestStructExpressFreightDetailContact
	 */
	public function setBeforeDeliveryContact($_beforeDeliveryContact)
	{
		return ($this->BeforeDeliveryContact = $_beforeDeliveryContact);
	}
	/**
	 * Get UndeliverableContact value
	 * @return MytestStructExpressFreightDetailContact|null
	 */
	public function getUndeliverableContact()
	{
		return $this->UndeliverableContact;
	}
	/**
	 * Set UndeliverableContact value
	 * @param MytestStructExpressFreightDetailContact $_undeliverableContact the UndeliverableContact
	 * @return MytestStructExpressFreightDetailContact
	 */
	public function setUndeliverableContact($_undeliverableContact)
	{
		return ($this->UndeliverableContact = $_undeliverableContact);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructExpressFreightDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>