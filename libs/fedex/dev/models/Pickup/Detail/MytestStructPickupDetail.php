<?php
/**
 * File for class MytestStructPickupDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructPickupDetail originally named PickupDetail
 * Documentation : This class describes the pickup characteristics of a shipment (e.g. for use in a tag request).
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructPickupDetail extends MytestWsdlClass
{
	/**
	 * The ReadyDateTime
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var dateTime
	 */
	public $ReadyDateTime;
	/**
	 * The LatestPickupDateTime
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var dateTime
	 */
	public $LatestPickupDateTime;
	/**
	 * The CourierInstructions
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $CourierInstructions;
	/**
	 * The RequestType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumPickupRequestType
	 */
	public $RequestType;
	/**
	 * The RequestSource
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumPickupRequestSourceType
	 */
	public $RequestSource;
	/**
	 * Constructor method for PickupDetail
	 * @see parent::__construct()
	 * @param dateTime $_readyDateTime
	 * @param dateTime $_latestPickupDateTime
	 * @param string $_courierInstructions
	 * @param MytestEnumPickupRequestType $_requestType
	 * @param MytestEnumPickupRequestSourceType $_requestSource
	 * @return MytestStructPickupDetail
	 */
	public function __construct($_readyDateTime = NULL,$_latestPickupDateTime = NULL,$_courierInstructions = NULL,$_requestType = NULL,$_requestSource = NULL)
	{
		parent::__construct(array('ReadyDateTime'=>$_readyDateTime,'LatestPickupDateTime'=>$_latestPickupDateTime,'CourierInstructions'=>$_courierInstructions,'RequestType'=>$_requestType,'RequestSource'=>$_requestSource));
	}
	/**
	 * Get ReadyDateTime value
	 * @return dateTime|null
	 */
	public function getReadyDateTime()
	{
		return $this->ReadyDateTime;
	}
	/**
	 * Set ReadyDateTime value
	 * @param dateTime $_readyDateTime the ReadyDateTime
	 * @return dateTime
	 */
	public function setReadyDateTime($_readyDateTime)
	{
		return ($this->ReadyDateTime = $_readyDateTime);
	}
	/**
	 * Get LatestPickupDateTime value
	 * @return dateTime|null
	 */
	public function getLatestPickupDateTime()
	{
		return $this->LatestPickupDateTime;
	}
	/**
	 * Set LatestPickupDateTime value
	 * @param dateTime $_latestPickupDateTime the LatestPickupDateTime
	 * @return dateTime
	 */
	public function setLatestPickupDateTime($_latestPickupDateTime)
	{
		return ($this->LatestPickupDateTime = $_latestPickupDateTime);
	}
	/**
	 * Get CourierInstructions value
	 * @return string|null
	 */
	public function getCourierInstructions()
	{
		return $this->CourierInstructions;
	}
	/**
	 * Set CourierInstructions value
	 * @param string $_courierInstructions the CourierInstructions
	 * @return string
	 */
	public function setCourierInstructions($_courierInstructions)
	{
		return ($this->CourierInstructions = $_courierInstructions);
	}
	/**
	 * Get RequestType value
	 * @return MytestEnumPickupRequestType|null
	 */
	public function getRequestType()
	{
		return $this->RequestType;
	}
	/**
	 * Set RequestType value
	 * @uses MytestEnumPickupRequestType::valueIsValid()
	 * @param MytestEnumPickupRequestType $_requestType the RequestType
	 * @return MytestEnumPickupRequestType
	 */
	public function setRequestType($_requestType)
	{
		if(!MytestEnumPickupRequestType::valueIsValid($_requestType))
		{
			return false;
		}
		return ($this->RequestType = $_requestType);
	}
	/**
	 * Get RequestSource value
	 * @return MytestEnumPickupRequestSourceType|null
	 */
	public function getRequestSource()
	{
		return $this->RequestSource;
	}
	/**
	 * Set RequestSource value
	 * @uses MytestEnumPickupRequestSourceType::valueIsValid()
	 * @param MytestEnumPickupRequestSourceType $_requestSource the RequestSource
	 * @return MytestEnumPickupRequestSourceType
	 */
	public function setRequestSource($_requestSource)
	{
		if(!MytestEnumPickupRequestSourceType::valueIsValid($_requestSource))
		{
			return false;
		}
		return ($this->RequestSource = $_requestSource);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructPickupDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>