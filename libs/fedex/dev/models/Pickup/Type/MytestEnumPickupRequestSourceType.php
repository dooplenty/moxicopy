<?php
/**
 * File for class MytestEnumPickupRequestSourceType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumPickupRequestSourceType originally named PickupRequestSourceType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumPickupRequestSourceType extends MytestWsdlClass
{
	/**
	 * Constant for value 'AUTOMATION'
	 * @return string 'AUTOMATION'
	 */
	const VALUE_AUTOMATION = 'AUTOMATION';
	/**
	 * Constant for value 'CUSTOMER_SERVICE'
	 * @return string 'CUSTOMER_SERVICE'
	 */
	const VALUE_CUSTOMER_SERVICE = 'CUSTOMER_SERVICE';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumPickupRequestSourceType::VALUE_AUTOMATION
	 * @uses MytestEnumPickupRequestSourceType::VALUE_CUSTOMER_SERVICE
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumPickupRequestSourceType::VALUE_AUTOMATION,MytestEnumPickupRequestSourceType::VALUE_CUSTOMER_SERVICE));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>