<?php
/**
 * File for class MytestEnumPickupRequestType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumPickupRequestType originally named PickupRequestType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumPickupRequestType extends MytestWsdlClass
{
	/**
	 * Constant for value 'FUTURE_DAY'
	 * @return string 'FUTURE_DAY'
	 */
	const VALUE_FUTURE_DAY = 'FUTURE_DAY';
	/**
	 * Constant for value 'SAME_DAY'
	 * @return string 'SAME_DAY'
	 */
	const VALUE_SAME_DAY = 'SAME_DAY';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumPickupRequestType::VALUE_FUTURE_DAY
	 * @uses MytestEnumPickupRequestType::VALUE_SAME_DAY
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumPickupRequestType::VALUE_FUTURE_DAY,MytestEnumPickupRequestType::VALUE_SAME_DAY));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>