<?php
/**
 * File for class MytestStructDeliveryOnInvoiceAcceptanceDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructDeliveryOnInvoiceAcceptanceDetail originally named DeliveryOnInvoiceAcceptanceDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructDeliveryOnInvoiceAcceptanceDetail extends MytestWsdlClass
{
	/**
	 * The Recipient
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructParty
	 */
	public $Recipient;
	/**
	 * The TrackingId
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the tracking id for the return, if preassigned.
	 * - minOccurs : 0
	 * @var MytestStructTrackingId
	 */
	public $TrackingId;
	/**
	 * Constructor method for DeliveryOnInvoiceAcceptanceDetail
	 * @see parent::__construct()
	 * @param MytestStructParty $_recipient
	 * @param MytestStructTrackingId $_trackingId
	 * @return MytestStructDeliveryOnInvoiceAcceptanceDetail
	 */
	public function __construct($_recipient = NULL,$_trackingId = NULL)
	{
		parent::__construct(array('Recipient'=>$_recipient,'TrackingId'=>$_trackingId));
	}
	/**
	 * Get Recipient value
	 * @return MytestStructParty|null
	 */
	public function getRecipient()
	{
		return $this->Recipient;
	}
	/**
	 * Set Recipient value
	 * @param MytestStructParty $_recipient the Recipient
	 * @return MytestStructParty
	 */
	public function setRecipient($_recipient)
	{
		return ($this->Recipient = $_recipient);
	}
	/**
	 * Get TrackingId value
	 * @return MytestStructTrackingId|null
	 */
	public function getTrackingId()
	{
		return $this->TrackingId;
	}
	/**
	 * Set TrackingId value
	 * @param MytestStructTrackingId $_trackingId the TrackingId
	 * @return MytestStructTrackingId
	 */
	public function setTrackingId($_trackingId)
	{
		return ($this->TrackingId = $_trackingId);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructDeliveryOnInvoiceAcceptanceDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>