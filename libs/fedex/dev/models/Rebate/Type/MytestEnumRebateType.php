<?php
/**
 * File for class MytestEnumRebateType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumRebateType originally named RebateType
 * Documentation : Identifies the type of discount applied to the shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumRebateType extends MytestWsdlClass
{
	/**
	 * Constant for value 'BONUS'
	 * @return string 'BONUS'
	 */
	const VALUE_BONUS = 'BONUS';
	/**
	 * Constant for value 'EARNED'
	 * @return string 'EARNED'
	 */
	const VALUE_EARNED = 'EARNED';
	/**
	 * Constant for value 'OTHER'
	 * @return string 'OTHER'
	 */
	const VALUE_OTHER = 'OTHER';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumRebateType::VALUE_BONUS
	 * @uses MytestEnumRebateType::VALUE_EARNED
	 * @uses MytestEnumRebateType::VALUE_OTHER
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumRebateType::VALUE_BONUS,MytestEnumRebateType::VALUE_EARNED,MytestEnumRebateType::VALUE_OTHER));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>