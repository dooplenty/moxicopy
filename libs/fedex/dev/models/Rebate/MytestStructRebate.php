<?php
/**
 * File for class MytestStructRebate
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructRebate originally named Rebate
 * Documentation : Identifies a discount applied to the shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructRebate extends MytestWsdlClass
{
	/**
	 * The RebateType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumRebateType
	 */
	public $RebateType;
	/**
	 * The Description
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Description;
	/**
	 * The Amount
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $Amount;
	/**
	 * The Percent
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var decimal
	 */
	public $Percent;
	/**
	 * Constructor method for Rebate
	 * @see parent::__construct()
	 * @param MytestEnumRebateType $_rebateType
	 * @param string $_description
	 * @param MytestStructMoney $_amount
	 * @param decimal $_percent
	 * @return MytestStructRebate
	 */
	public function __construct($_rebateType = NULL,$_description = NULL,$_amount = NULL,$_percent = NULL)
	{
		parent::__construct(array('RebateType'=>$_rebateType,'Description'=>$_description,'Amount'=>$_amount,'Percent'=>$_percent));
	}
	/**
	 * Get RebateType value
	 * @return MytestEnumRebateType|null
	 */
	public function getRebateType()
	{
		return $this->RebateType;
	}
	/**
	 * Set RebateType value
	 * @uses MytestEnumRebateType::valueIsValid()
	 * @param MytestEnumRebateType $_rebateType the RebateType
	 * @return MytestEnumRebateType
	 */
	public function setRebateType($_rebateType)
	{
		if(!MytestEnumRebateType::valueIsValid($_rebateType))
		{
			return false;
		}
		return ($this->RebateType = $_rebateType);
	}
	/**
	 * Get Description value
	 * @return string|null
	 */
	public function getDescription()
	{
		return $this->Description;
	}
	/**
	 * Set Description value
	 * @param string $_description the Description
	 * @return string
	 */
	public function setDescription($_description)
	{
		return ($this->Description = $_description);
	}
	/**
	 * Get Amount value
	 * @return MytestStructMoney|null
	 */
	public function getAmount()
	{
		return $this->Amount;
	}
	/**
	 * Set Amount value
	 * @param MytestStructMoney $_amount the Amount
	 * @return MytestStructMoney
	 */
	public function setAmount($_amount)
	{
		return ($this->Amount = $_amount);
	}
	/**
	 * Get Percent value
	 * @return decimal|null
	 */
	public function getPercent()
	{
		return $this->Percent;
	}
	/**
	 * Set Percent value
	 * @param decimal $_percent the Percent
	 * @return decimal
	 */
	public function setPercent($_percent)
	{
		return ($this->Percent = $_percent);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructRebate
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>