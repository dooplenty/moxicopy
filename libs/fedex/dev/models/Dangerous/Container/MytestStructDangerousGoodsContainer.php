<?php
/**
 * File for class MytestStructDangerousGoodsContainer
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructDangerousGoodsContainer originally named DangerousGoodsContainer
 * Documentation : Describes an approved container used to package dangerous goods commodities. This does not describe any individual inner receptacles that may be within this container.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructDangerousGoodsContainer extends MytestWsdlClass
{
	/**
	 * The PackingType
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates whether there are additional inner receptacles within this container.
	 * - minOccurs : 0
	 * @var MytestEnumHazardousContainerPackingType
	 */
	public $PackingType;
	/**
	 * The ContainerType
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates the type of this dangerous goods container, as specified by the IATA packing instructions. For example, steel cylinder, fiberboard box, plastic jerrican and steel drum.
	 * - minOccurs : 0
	 * @var string
	 */
	public $ContainerType;
	/**
	 * The RadioactiveContainerClass
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates the packaging type of the container used to package the radioactive materials.
	 * - minOccurs : 0
	 * @var MytestEnumRadioactiveContainerClassType
	 */
	public $RadioactiveContainerClass;
	/**
	 * The NumberOfContainers
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates the number of occurrences of this container with identical dangerous goods configuration.
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $NumberOfContainers;
	/**
	 * The HazardousCommodities
	 * Meta informations extracted from the WSDL
	 * - documentation : Documents the kinds and quantities of all hazardous commodities in the current container.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructHazardousCommodityContent
	 */
	public $HazardousCommodities;
	/**
	 * Constructor method for DangerousGoodsContainer
	 * @see parent::__construct()
	 * @param MytestEnumHazardousContainerPackingType $_packingType
	 * @param string $_containerType
	 * @param MytestEnumRadioactiveContainerClassType $_radioactiveContainerClass
	 * @param nonNegativeInteger $_numberOfContainers
	 * @param MytestStructHazardousCommodityContent $_hazardousCommodities
	 * @return MytestStructDangerousGoodsContainer
	 */
	public function __construct($_packingType = NULL,$_containerType = NULL,$_radioactiveContainerClass = NULL,$_numberOfContainers = NULL,$_hazardousCommodities = NULL)
	{
		parent::__construct(array('PackingType'=>$_packingType,'ContainerType'=>$_containerType,'RadioactiveContainerClass'=>$_radioactiveContainerClass,'NumberOfContainers'=>$_numberOfContainers,'HazardousCommodities'=>$_hazardousCommodities));
	}
	/**
	 * Get PackingType value
	 * @return MytestEnumHazardousContainerPackingType|null
	 */
	public function getPackingType()
	{
		return $this->PackingType;
	}
	/**
	 * Set PackingType value
	 * @uses MytestEnumHazardousContainerPackingType::valueIsValid()
	 * @param MytestEnumHazardousContainerPackingType $_packingType the PackingType
	 * @return MytestEnumHazardousContainerPackingType
	 */
	public function setPackingType($_packingType)
	{
		if(!MytestEnumHazardousContainerPackingType::valueIsValid($_packingType))
		{
			return false;
		}
		return ($this->PackingType = $_packingType);
	}
	/**
	 * Get ContainerType value
	 * @return string|null
	 */
	public function getContainerType()
	{
		return $this->ContainerType;
	}
	/**
	 * Set ContainerType value
	 * @param string $_containerType the ContainerType
	 * @return string
	 */
	public function setContainerType($_containerType)
	{
		return ($this->ContainerType = $_containerType);
	}
	/**
	 * Get RadioactiveContainerClass value
	 * @return MytestEnumRadioactiveContainerClassType|null
	 */
	public function getRadioactiveContainerClass()
	{
		return $this->RadioactiveContainerClass;
	}
	/**
	 * Set RadioactiveContainerClass value
	 * @uses MytestEnumRadioactiveContainerClassType::valueIsValid()
	 * @param MytestEnumRadioactiveContainerClassType $_radioactiveContainerClass the RadioactiveContainerClass
	 * @return MytestEnumRadioactiveContainerClassType
	 */
	public function setRadioactiveContainerClass($_radioactiveContainerClass)
	{
		if(!MytestEnumRadioactiveContainerClassType::valueIsValid($_radioactiveContainerClass))
		{
			return false;
		}
		return ($this->RadioactiveContainerClass = $_radioactiveContainerClass);
	}
	/**
	 * Get NumberOfContainers value
	 * @return nonNegativeInteger|null
	 */
	public function getNumberOfContainers()
	{
		return $this->NumberOfContainers;
	}
	/**
	 * Set NumberOfContainers value
	 * @param nonNegativeInteger $_numberOfContainers the NumberOfContainers
	 * @return nonNegativeInteger
	 */
	public function setNumberOfContainers($_numberOfContainers)
	{
		return ($this->NumberOfContainers = $_numberOfContainers);
	}
	/**
	 * Get HazardousCommodities value
	 * @return MytestStructHazardousCommodityContent|null
	 */
	public function getHazardousCommodities()
	{
		return $this->HazardousCommodities;
	}
	/**
	 * Set HazardousCommodities value
	 * @param MytestStructHazardousCommodityContent $_hazardousCommodities the HazardousCommodities
	 * @return MytestStructHazardousCommodityContent
	 */
	public function setHazardousCommodities($_hazardousCommodities)
	{
		return ($this->HazardousCommodities = $_hazardousCommodities);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructDangerousGoodsContainer
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>