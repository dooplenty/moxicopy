<?php
/**
 * File for class MytestStructDangerousGoodsDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructDangerousGoodsDetail originally named DangerousGoodsDetail
 * Documentation : The descriptive data required for a FedEx shipment containing dangerous goods (hazardous materials).
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructDangerousGoodsDetail extends MytestWsdlClass
{
	/**
	 * The Accessibility
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumDangerousGoodsAccessibilityType
	 */
	public $Accessibility;
	/**
	 * The CargoAircraftOnly
	 * Meta informations extracted from the WSDL
	 * - documentation : Shipment is packaged/documented for movement ONLY on cargo aircraft.
	 * - minOccurs : 0
	 * @var boolean
	 */
	public $CargoAircraftOnly;
	/**
	 * The Options
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates which kinds of hazardous content are in the current package.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumHazardousCommodityOptionType
	 */
	public $Options;
	/**
	 * The PackingOption
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates whether there is additional customer provided packaging enclosing the approved dangerous goods containers.
	 * - minOccurs : 0
	 * @var MytestEnumDangerousGoodsPackingOptionType
	 */
	public $PackingOption;
	/**
	 * The ReferenceId
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the configuration of this dangerous goods package. The common configuration is represented at the shipment level.
	 * - minOccurs : 0
	 * @var string
	 */
	public $ReferenceId;
	/**
	 * The Containers
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates one or more containers used to pack dangerous goods commodities.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructDangerousGoodsContainer
	 */
	public $Containers;
	/**
	 * The Packaging
	 * Meta informations extracted from the WSDL
	 * - documentation : Description of the packaging of this commodity, suitable for use on OP-900 and OP-950 forms.
	 * - minOccurs : 0
	 * @var MytestStructHazardousCommodityPackagingDetail
	 */
	public $Packaging;
	/**
	 * The Signatory
	 * Meta informations extracted from the WSDL
	 * - documentation : Name, title and place of the signatory for this shipment.
	 * - minOccurs : 0
	 * @var MytestStructDangerousGoodsSignatory
	 */
	public $Signatory;
	/**
	 * The EmergencyContactNumber
	 * Meta informations extracted from the WSDL
	 * - documentation : Telephone number to use for contact in the event of an emergency.
	 * - minOccurs : 0
	 * @var string
	 */
	public $EmergencyContactNumber;
	/**
	 * The Offeror
	 * Meta informations extracted from the WSDL
	 * - documentation : Offeror's name or contract number, per DOT regulation.
	 * - minOccurs : 0
	 * @var string
	 */
	public $Offeror;
	/**
	 * The InfectiousSubstanceResponsibleContact
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the contact of the party responsible for handling the infectious substances, if any, in the dangerous goods shipment.
	 * - minOccurs : 0
	 * @var MytestStructContact
	 */
	public $InfectiousSubstanceResponsibleContact;
	/**
	 * The AdditionalHandling
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies additional handling information for the current package.
	 * - minOccurs : 0
	 * @var string
	 */
	public $AdditionalHandling;
	/**
	 * The RadioactivityDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the radioactivity detail for the current package, if the package contains radioactive materials.
	 * - minOccurs : 0
	 * @var MytestStructRadioactivityDetail
	 */
	public $RadioactivityDetail;
	/**
	 * Constructor method for DangerousGoodsDetail
	 * @see parent::__construct()
	 * @param MytestEnumDangerousGoodsAccessibilityType $_accessibility
	 * @param boolean $_cargoAircraftOnly
	 * @param MytestEnumHazardousCommodityOptionType $_options
	 * @param MytestEnumDangerousGoodsPackingOptionType $_packingOption
	 * @param string $_referenceId
	 * @param MytestStructDangerousGoodsContainer $_containers
	 * @param MytestStructHazardousCommodityPackagingDetail $_packaging
	 * @param MytestStructDangerousGoodsSignatory $_signatory
	 * @param string $_emergencyContactNumber
	 * @param string $_offeror
	 * @param MytestStructContact $_infectiousSubstanceResponsibleContact
	 * @param string $_additionalHandling
	 * @param MytestStructRadioactivityDetail $_radioactivityDetail
	 * @return MytestStructDangerousGoodsDetail
	 */
	public function __construct($_accessibility = NULL,$_cargoAircraftOnly = NULL,$_options = NULL,$_packingOption = NULL,$_referenceId = NULL,$_containers = NULL,$_packaging = NULL,$_signatory = NULL,$_emergencyContactNumber = NULL,$_offeror = NULL,$_infectiousSubstanceResponsibleContact = NULL,$_additionalHandling = NULL,$_radioactivityDetail = NULL)
	{
		parent::__construct(array('Accessibility'=>$_accessibility,'CargoAircraftOnly'=>$_cargoAircraftOnly,'Options'=>$_options,'PackingOption'=>$_packingOption,'ReferenceId'=>$_referenceId,'Containers'=>$_containers,'Packaging'=>$_packaging,'Signatory'=>$_signatory,'EmergencyContactNumber'=>$_emergencyContactNumber,'Offeror'=>$_offeror,'InfectiousSubstanceResponsibleContact'=>$_infectiousSubstanceResponsibleContact,'AdditionalHandling'=>$_additionalHandling,'RadioactivityDetail'=>$_radioactivityDetail));
	}
	/**
	 * Get Accessibility value
	 * @return MytestEnumDangerousGoodsAccessibilityType|null
	 */
	public function getAccessibility()
	{
		return $this->Accessibility;
	}
	/**
	 * Set Accessibility value
	 * @uses MytestEnumDangerousGoodsAccessibilityType::valueIsValid()
	 * @param MytestEnumDangerousGoodsAccessibilityType $_accessibility the Accessibility
	 * @return MytestEnumDangerousGoodsAccessibilityType
	 */
	public function setAccessibility($_accessibility)
	{
		if(!MytestEnumDangerousGoodsAccessibilityType::valueIsValid($_accessibility))
		{
			return false;
		}
		return ($this->Accessibility = $_accessibility);
	}
	/**
	 * Get CargoAircraftOnly value
	 * @return boolean|null
	 */
	public function getCargoAircraftOnly()
	{
		return $this->CargoAircraftOnly;
	}
	/**
	 * Set CargoAircraftOnly value
	 * @param boolean $_cargoAircraftOnly the CargoAircraftOnly
	 * @return boolean
	 */
	public function setCargoAircraftOnly($_cargoAircraftOnly)
	{
		return ($this->CargoAircraftOnly = $_cargoAircraftOnly);
	}
	/**
	 * Get Options value
	 * @return MytestEnumHazardousCommodityOptionType|null
	 */
	public function getOptions()
	{
		return $this->Options;
	}
	/**
	 * Set Options value
	 * @uses MytestEnumHazardousCommodityOptionType::valueIsValid()
	 * @param MytestEnumHazardousCommodityOptionType $_options the Options
	 * @return MytestEnumHazardousCommodityOptionType
	 */
	public function setOptions($_options)
	{
		if(!MytestEnumHazardousCommodityOptionType::valueIsValid($_options))
		{
			return false;
		}
		return ($this->Options = $_options);
	}
	/**
	 * Get PackingOption value
	 * @return MytestEnumDangerousGoodsPackingOptionType|null
	 */
	public function getPackingOption()
	{
		return $this->PackingOption;
	}
	/**
	 * Set PackingOption value
	 * @uses MytestEnumDangerousGoodsPackingOptionType::valueIsValid()
	 * @param MytestEnumDangerousGoodsPackingOptionType $_packingOption the PackingOption
	 * @return MytestEnumDangerousGoodsPackingOptionType
	 */
	public function setPackingOption($_packingOption)
	{
		if(!MytestEnumDangerousGoodsPackingOptionType::valueIsValid($_packingOption))
		{
			return false;
		}
		return ($this->PackingOption = $_packingOption);
	}
	/**
	 * Get ReferenceId value
	 * @return string|null
	 */
	public function getReferenceId()
	{
		return $this->ReferenceId;
	}
	/**
	 * Set ReferenceId value
	 * @param string $_referenceId the ReferenceId
	 * @return string
	 */
	public function setReferenceId($_referenceId)
	{
		return ($this->ReferenceId = $_referenceId);
	}
	/**
	 * Get Containers value
	 * @return MytestStructDangerousGoodsContainer|null
	 */
	public function getContainers()
	{
		return $this->Containers;
	}
	/**
	 * Set Containers value
	 * @param MytestStructDangerousGoodsContainer $_containers the Containers
	 * @return MytestStructDangerousGoodsContainer
	 */
	public function setContainers($_containers)
	{
		return ($this->Containers = $_containers);
	}
	/**
	 * Get Packaging value
	 * @return MytestStructHazardousCommodityPackagingDetail|null
	 */
	public function getPackaging()
	{
		return $this->Packaging;
	}
	/**
	 * Set Packaging value
	 * @param MytestStructHazardousCommodityPackagingDetail $_packaging the Packaging
	 * @return MytestStructHazardousCommodityPackagingDetail
	 */
	public function setPackaging($_packaging)
	{
		return ($this->Packaging = $_packaging);
	}
	/**
	 * Get Signatory value
	 * @return MytestStructDangerousGoodsSignatory|null
	 */
	public function getSignatory()
	{
		return $this->Signatory;
	}
	/**
	 * Set Signatory value
	 * @param MytestStructDangerousGoodsSignatory $_signatory the Signatory
	 * @return MytestStructDangerousGoodsSignatory
	 */
	public function setSignatory($_signatory)
	{
		return ($this->Signatory = $_signatory);
	}
	/**
	 * Get EmergencyContactNumber value
	 * @return string|null
	 */
	public function getEmergencyContactNumber()
	{
		return $this->EmergencyContactNumber;
	}
	/**
	 * Set EmergencyContactNumber value
	 * @param string $_emergencyContactNumber the EmergencyContactNumber
	 * @return string
	 */
	public function setEmergencyContactNumber($_emergencyContactNumber)
	{
		return ($this->EmergencyContactNumber = $_emergencyContactNumber);
	}
	/**
	 * Get Offeror value
	 * @return string|null
	 */
	public function getOfferor()
	{
		return $this->Offeror;
	}
	/**
	 * Set Offeror value
	 * @param string $_offeror the Offeror
	 * @return string
	 */
	public function setOfferor($_offeror)
	{
		return ($this->Offeror = $_offeror);
	}
	/**
	 * Get InfectiousSubstanceResponsibleContact value
	 * @return MytestStructContact|null
	 */
	public function getInfectiousSubstanceResponsibleContact()
	{
		return $this->InfectiousSubstanceResponsibleContact;
	}
	/**
	 * Set InfectiousSubstanceResponsibleContact value
	 * @param MytestStructContact $_infectiousSubstanceResponsibleContact the InfectiousSubstanceResponsibleContact
	 * @return MytestStructContact
	 */
	public function setInfectiousSubstanceResponsibleContact($_infectiousSubstanceResponsibleContact)
	{
		return ($this->InfectiousSubstanceResponsibleContact = $_infectiousSubstanceResponsibleContact);
	}
	/**
	 * Get AdditionalHandling value
	 * @return string|null
	 */
	public function getAdditionalHandling()
	{
		return $this->AdditionalHandling;
	}
	/**
	 * Set AdditionalHandling value
	 * @param string $_additionalHandling the AdditionalHandling
	 * @return string
	 */
	public function setAdditionalHandling($_additionalHandling)
	{
		return ($this->AdditionalHandling = $_additionalHandling);
	}
	/**
	 * Get RadioactivityDetail value
	 * @return MytestStructRadioactivityDetail|null
	 */
	public function getRadioactivityDetail()
	{
		return $this->RadioactivityDetail;
	}
	/**
	 * Set RadioactivityDetail value
	 * @param MytestStructRadioactivityDetail $_radioactivityDetail the RadioactivityDetail
	 * @return MytestStructRadioactivityDetail
	 */
	public function setRadioactivityDetail($_radioactivityDetail)
	{
		return ($this->RadioactivityDetail = $_radioactivityDetail);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructDangerousGoodsDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>