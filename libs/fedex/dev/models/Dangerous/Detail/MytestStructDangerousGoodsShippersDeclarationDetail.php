<?php
/**
 * File for class MytestStructDangerousGoodsShippersDeclarationDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructDangerousGoodsShippersDeclarationDetail originally named DangerousGoodsShippersDeclarationDetail
 * Documentation : The instructions indicating how to print the 1421c form for dangerous goods shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructDangerousGoodsShippersDeclarationDetail extends MytestWsdlClass
{
	/**
	 * The Format
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies characteristics of a shipping document to be produced.
	 * - minOccurs : 0
	 * @var MytestStructShippingDocumentFormat
	 */
	public $Format;
	/**
	 * The CustomerImageUsages
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the usage and identification of customer supplied images to be used on this document.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructCustomerImageUsage
	 */
	public $CustomerImageUsages;
	/**
	 * Constructor method for DangerousGoodsShippersDeclarationDetail
	 * @see parent::__construct()
	 * @param MytestStructShippingDocumentFormat $_format
	 * @param MytestStructCustomerImageUsage $_customerImageUsages
	 * @return MytestStructDangerousGoodsShippersDeclarationDetail
	 */
	public function __construct($_format = NULL,$_customerImageUsages = NULL)
	{
		parent::__construct(array('Format'=>$_format,'CustomerImageUsages'=>$_customerImageUsages));
	}
	/**
	 * Get Format value
	 * @return MytestStructShippingDocumentFormat|null
	 */
	public function getFormat()
	{
		return $this->Format;
	}
	/**
	 * Set Format value
	 * @param MytestStructShippingDocumentFormat $_format the Format
	 * @return MytestStructShippingDocumentFormat
	 */
	public function setFormat($_format)
	{
		return ($this->Format = $_format);
	}
	/**
	 * Get CustomerImageUsages value
	 * @return MytestStructCustomerImageUsage|null
	 */
	public function getCustomerImageUsages()
	{
		return $this->CustomerImageUsages;
	}
	/**
	 * Set CustomerImageUsages value
	 * @param MytestStructCustomerImageUsage $_customerImageUsages the CustomerImageUsages
	 * @return MytestStructCustomerImageUsage
	 */
	public function setCustomerImageUsages($_customerImageUsages)
	{
		return ($this->CustomerImageUsages = $_customerImageUsages);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructDangerousGoodsShippersDeclarationDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>