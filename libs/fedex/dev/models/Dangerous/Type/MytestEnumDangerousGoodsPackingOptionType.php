<?php
/**
 * File for class MytestEnumDangerousGoodsPackingOptionType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumDangerousGoodsPackingOptionType originally named DangerousGoodsPackingOptionType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumDangerousGoodsPackingOptionType extends MytestWsdlClass
{
	/**
	 * Constant for value 'OVERPACK'
	 * @return string 'OVERPACK'
	 */
	const VALUE_OVERPACK = 'OVERPACK';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumDangerousGoodsPackingOptionType::VALUE_OVERPACK
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumDangerousGoodsPackingOptionType::VALUE_OVERPACK));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>