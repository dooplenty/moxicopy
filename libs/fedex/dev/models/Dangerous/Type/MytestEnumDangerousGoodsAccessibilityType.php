<?php
/**
 * File for class MytestEnumDangerousGoodsAccessibilityType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumDangerousGoodsAccessibilityType originally named DangerousGoodsAccessibilityType
 * Documentation : Identifies whether or not the products being shipped are required to be accessible during delivery.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumDangerousGoodsAccessibilityType extends MytestWsdlClass
{
	/**
	 * Constant for value 'ACCESSIBLE'
	 * @return string 'ACCESSIBLE'
	 */
	const VALUE_ACCESSIBLE = 'ACCESSIBLE';
	/**
	 * Constant for value 'INACCESSIBLE'
	 * @return string 'INACCESSIBLE'
	 */
	const VALUE_INACCESSIBLE = 'INACCESSIBLE';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumDangerousGoodsAccessibilityType::VALUE_ACCESSIBLE
	 * @uses MytestEnumDangerousGoodsAccessibilityType::VALUE_INACCESSIBLE
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumDangerousGoodsAccessibilityType::VALUE_ACCESSIBLE,MytestEnumDangerousGoodsAccessibilityType::VALUE_INACCESSIBLE));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>