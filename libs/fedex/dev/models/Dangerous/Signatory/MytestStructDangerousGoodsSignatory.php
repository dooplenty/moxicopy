<?php
/**
 * File for class MytestStructDangerousGoodsSignatory
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructDangerousGoodsSignatory originally named DangerousGoodsSignatory
 * Documentation : Specifies that name, title and place of the signatory responsible for the dangerous goods shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructDangerousGoodsSignatory extends MytestWsdlClass
{
	/**
	 * The ContactName
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $ContactName;
	/**
	 * The Title
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Title;
	/**
	 * The Place
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates the place where the form is signed.
	 * - minOccurs : 0
	 * @var string
	 */
	public $Place;
	/**
	 * Constructor method for DangerousGoodsSignatory
	 * @see parent::__construct()
	 * @param string $_contactName
	 * @param string $_title
	 * @param string $_place
	 * @return MytestStructDangerousGoodsSignatory
	 */
	public function __construct($_contactName = NULL,$_title = NULL,$_place = NULL)
	{
		parent::__construct(array('ContactName'=>$_contactName,'Title'=>$_title,'Place'=>$_place));
	}
	/**
	 * Get ContactName value
	 * @return string|null
	 */
	public function getContactName()
	{
		return $this->ContactName;
	}
	/**
	 * Set ContactName value
	 * @param string $_contactName the ContactName
	 * @return string
	 */
	public function setContactName($_contactName)
	{
		return ($this->ContactName = $_contactName);
	}
	/**
	 * Get Title value
	 * @return string|null
	 */
	public function getTitle()
	{
		return $this->Title;
	}
	/**
	 * Set Title value
	 * @param string $_title the Title
	 * @return string
	 */
	public function setTitle($_title)
	{
		return ($this->Title = $_title);
	}
	/**
	 * Get Place value
	 * @return string|null
	 */
	public function getPlace()
	{
		return $this->Place;
	}
	/**
	 * Set Place value
	 * @param string $_place the Place
	 * @return string
	 */
	public function setPlace($_place)
	{
		return ($this->Place = $_place);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructDangerousGoodsSignatory
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>