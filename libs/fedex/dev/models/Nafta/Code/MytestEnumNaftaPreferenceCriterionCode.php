<?php
/**
 * File for class MytestEnumNaftaPreferenceCriterionCode
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumNaftaPreferenceCriterionCode originally named NaftaPreferenceCriterionCode
 * Documentation : See instructions for NAFTA Certificate of Origin for code definitions.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumNaftaPreferenceCriterionCode extends MytestWsdlClass
{
	/**
	 * Constant for value 'A'
	 * @return string 'A'
	 */
	const VALUE_A = 'A';
	/**
	 * Constant for value 'B'
	 * @return string 'B'
	 */
	const VALUE_B = 'B';
	/**
	 * Constant for value 'C'
	 * @return string 'C'
	 */
	const VALUE_C = 'C';
	/**
	 * Constant for value 'D'
	 * @return string 'D'
	 */
	const VALUE_D = 'D';
	/**
	 * Constant for value 'E'
	 * @return string 'E'
	 */
	const VALUE_E = 'E';
	/**
	 * Constant for value 'F'
	 * @return string 'F'
	 */
	const VALUE_F = 'F';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumNaftaPreferenceCriterionCode::VALUE_A
	 * @uses MytestEnumNaftaPreferenceCriterionCode::VALUE_B
	 * @uses MytestEnumNaftaPreferenceCriterionCode::VALUE_C
	 * @uses MytestEnumNaftaPreferenceCriterionCode::VALUE_D
	 * @uses MytestEnumNaftaPreferenceCriterionCode::VALUE_E
	 * @uses MytestEnumNaftaPreferenceCriterionCode::VALUE_F
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumNaftaPreferenceCriterionCode::VALUE_A,MytestEnumNaftaPreferenceCriterionCode::VALUE_B,MytestEnumNaftaPreferenceCriterionCode::VALUE_C,MytestEnumNaftaPreferenceCriterionCode::VALUE_D,MytestEnumNaftaPreferenceCriterionCode::VALUE_E,MytestEnumNaftaPreferenceCriterionCode::VALUE_F));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>