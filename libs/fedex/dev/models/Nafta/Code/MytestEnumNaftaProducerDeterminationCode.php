<?php
/**
 * File for class MytestEnumNaftaProducerDeterminationCode
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumNaftaProducerDeterminationCode originally named NaftaProducerDeterminationCode
 * Documentation : See instructions for NAFTA Certificate of Origin for code definitions.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumNaftaProducerDeterminationCode extends MytestWsdlClass
{
	/**
	 * Constant for value 'NO_1'
	 * @return string 'NO_1'
	 */
	const VALUE_NO_1 = 'NO_1';
	/**
	 * Constant for value 'NO_2'
	 * @return string 'NO_2'
	 */
	const VALUE_NO_2 = 'NO_2';
	/**
	 * Constant for value 'NO_3'
	 * @return string 'NO_3'
	 */
	const VALUE_NO_3 = 'NO_3';
	/**
	 * Constant for value 'YES'
	 * @return string 'YES'
	 */
	const VALUE_YES = 'YES';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumNaftaProducerDeterminationCode::VALUE_NO_1
	 * @uses MytestEnumNaftaProducerDeterminationCode::VALUE_NO_2
	 * @uses MytestEnumNaftaProducerDeterminationCode::VALUE_NO_3
	 * @uses MytestEnumNaftaProducerDeterminationCode::VALUE_YES
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumNaftaProducerDeterminationCode::VALUE_NO_1,MytestEnumNaftaProducerDeterminationCode::VALUE_NO_2,MytestEnumNaftaProducerDeterminationCode::VALUE_NO_3,MytestEnumNaftaProducerDeterminationCode::VALUE_YES));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>