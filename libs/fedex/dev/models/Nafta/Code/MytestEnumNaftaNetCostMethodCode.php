<?php
/**
 * File for class MytestEnumNaftaNetCostMethodCode
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumNaftaNetCostMethodCode originally named NaftaNetCostMethodCode
 * Documentation : Net cost method used.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumNaftaNetCostMethodCode extends MytestWsdlClass
{
	/**
	 * Constant for value 'NC'
	 * @return string 'NC'
	 */
	const VALUE_NC = 'NC';
	/**
	 * Constant for value 'NO'
	 * @return string 'NO'
	 */
	const VALUE_NO = 'NO';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumNaftaNetCostMethodCode::VALUE_NC
	 * @uses MytestEnumNaftaNetCostMethodCode::VALUE_NO
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumNaftaNetCostMethodCode::VALUE_NC,MytestEnumNaftaNetCostMethodCode::VALUE_NO));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>