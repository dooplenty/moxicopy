<?php
/**
 * File for class MytestEnumNaftaProducerSpecificationType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumNaftaProducerSpecificationType originally named NaftaProducerSpecificationType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumNaftaProducerSpecificationType extends MytestWsdlClass
{
	/**
	 * Constant for value 'AVAILABLE_UPON_REQUEST'
	 * @return string 'AVAILABLE_UPON_REQUEST'
	 */
	const VALUE_AVAILABLE_UPON_REQUEST = 'AVAILABLE_UPON_REQUEST';
	/**
	 * Constant for value 'MULTIPLE_SPECIFIED'
	 * @return string 'MULTIPLE_SPECIFIED'
	 */
	const VALUE_MULTIPLE_SPECIFIED = 'MULTIPLE_SPECIFIED';
	/**
	 * Constant for value 'SAME'
	 * @return string 'SAME'
	 */
	const VALUE_SAME = 'SAME';
	/**
	 * Constant for value 'SINGLE_SPECIFIED'
	 * @return string 'SINGLE_SPECIFIED'
	 */
	const VALUE_SINGLE_SPECIFIED = 'SINGLE_SPECIFIED';
	/**
	 * Constant for value 'UNKNOWN'
	 * @return string 'UNKNOWN'
	 */
	const VALUE_UNKNOWN = 'UNKNOWN';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumNaftaProducerSpecificationType::VALUE_AVAILABLE_UPON_REQUEST
	 * @uses MytestEnumNaftaProducerSpecificationType::VALUE_MULTIPLE_SPECIFIED
	 * @uses MytestEnumNaftaProducerSpecificationType::VALUE_SAME
	 * @uses MytestEnumNaftaProducerSpecificationType::VALUE_SINGLE_SPECIFIED
	 * @uses MytestEnumNaftaProducerSpecificationType::VALUE_UNKNOWN
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumNaftaProducerSpecificationType::VALUE_AVAILABLE_UPON_REQUEST,MytestEnumNaftaProducerSpecificationType::VALUE_MULTIPLE_SPECIFIED,MytestEnumNaftaProducerSpecificationType::VALUE_SAME,MytestEnumNaftaProducerSpecificationType::VALUE_SINGLE_SPECIFIED,MytestEnumNaftaProducerSpecificationType::VALUE_UNKNOWN));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>