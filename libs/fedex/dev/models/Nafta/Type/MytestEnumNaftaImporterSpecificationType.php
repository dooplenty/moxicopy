<?php
/**
 * File for class MytestEnumNaftaImporterSpecificationType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumNaftaImporterSpecificationType originally named NaftaImporterSpecificationType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumNaftaImporterSpecificationType extends MytestWsdlClass
{
	/**
	 * Constant for value 'IMPORTER_OF_RECORD'
	 * @return string 'IMPORTER_OF_RECORD'
	 */
	const VALUE_IMPORTER_OF_RECORD = 'IMPORTER_OF_RECORD';
	/**
	 * Constant for value 'RECIPIENT'
	 * @return string 'RECIPIENT'
	 */
	const VALUE_RECIPIENT = 'RECIPIENT';
	/**
	 * Constant for value 'UNKNOWN'
	 * @return string 'UNKNOWN'
	 */
	const VALUE_UNKNOWN = 'UNKNOWN';
	/**
	 * Constant for value 'VARIOUS'
	 * @return string 'VARIOUS'
	 */
	const VALUE_VARIOUS = 'VARIOUS';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumNaftaImporterSpecificationType::VALUE_IMPORTER_OF_RECORD
	 * @uses MytestEnumNaftaImporterSpecificationType::VALUE_RECIPIENT
	 * @uses MytestEnumNaftaImporterSpecificationType::VALUE_UNKNOWN
	 * @uses MytestEnumNaftaImporterSpecificationType::VALUE_VARIOUS
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumNaftaImporterSpecificationType::VALUE_IMPORTER_OF_RECORD,MytestEnumNaftaImporterSpecificationType::VALUE_RECIPIENT,MytestEnumNaftaImporterSpecificationType::VALUE_UNKNOWN,MytestEnumNaftaImporterSpecificationType::VALUE_VARIOUS));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>