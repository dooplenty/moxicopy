<?php
/**
 * File for class MytestStructNaftaProducer
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructNaftaProducer originally named NaftaProducer
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructNaftaProducer extends MytestWsdlClass
{
	/**
	 * The Id
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Id;
	/**
	 * The Producer
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructParty
	 */
	public $Producer;
	/**
	 * Constructor method for NaftaProducer
	 * @see parent::__construct()
	 * @param string $_id
	 * @param MytestStructParty $_producer
	 * @return MytestStructNaftaProducer
	 */
	public function __construct($_id = NULL,$_producer = NULL)
	{
		parent::__construct(array('Id'=>$_id,'Producer'=>$_producer));
	}
	/**
	 * Get Id value
	 * @return string|null
	 */
	public function getId()
	{
		return $this->Id;
	}
	/**
	 * Set Id value
	 * @param string $_id the Id
	 * @return string
	 */
	public function setId($_id)
	{
		return ($this->Id = $_id);
	}
	/**
	 * Get Producer value
	 * @return MytestStructParty|null
	 */
	public function getProducer()
	{
		return $this->Producer;
	}
	/**
	 * Set Producer value
	 * @param MytestStructParty $_producer the Producer
	 * @return MytestStructParty
	 */
	public function setProducer($_producer)
	{
		return ($this->Producer = $_producer);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructNaftaProducer
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>