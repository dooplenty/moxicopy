<?php
/**
 * File for class MytestStructNaftaCertificateOfOriginDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructNaftaCertificateOfOriginDetail originally named NaftaCertificateOfOriginDetail
 * Documentation : Data required to produce a Certificate of Origin document. Remaining content (business data) to be defined once requirements have been completed.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructNaftaCertificateOfOriginDetail extends MytestWsdlClass
{
	/**
	 * The Format
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructShippingDocumentFormat
	 */
	public $Format;
	/**
	 * The BlanketPeriod
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructDateRange
	 */
	public $BlanketPeriod;
	/**
	 * The ImporterSpecification
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates which Party (if any) from the shipment is to be used as the source of importer data on the NAFTA COO form.
	 * - minOccurs : 0
	 * @var MytestEnumNaftaImporterSpecificationType
	 */
	public $ImporterSpecification;
	/**
	 * The SignatureContact
	 * Meta informations extracted from the WSDL
	 * - documentation : Contact information for "Authorized Signature" area of form.
	 * - minOccurs : 0
	 * @var MytestStructContact
	 */
	public $SignatureContact;
	/**
	 * The ProducerSpecification
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumNaftaProducerSpecificationType
	 */
	public $ProducerSpecification;
	/**
	 * The Producers
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructNaftaProducer
	 */
	public $Producers;
	/**
	 * The CustomerImageUsages
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructCustomerImageUsage
	 */
	public $CustomerImageUsages;
	/**
	 * Constructor method for NaftaCertificateOfOriginDetail
	 * @see parent::__construct()
	 * @param MytestStructShippingDocumentFormat $_format
	 * @param MytestStructDateRange $_blanketPeriod
	 * @param MytestEnumNaftaImporterSpecificationType $_importerSpecification
	 * @param MytestStructContact $_signatureContact
	 * @param MytestEnumNaftaProducerSpecificationType $_producerSpecification
	 * @param MytestStructNaftaProducer $_producers
	 * @param MytestStructCustomerImageUsage $_customerImageUsages
	 * @return MytestStructNaftaCertificateOfOriginDetail
	 */
	public function __construct($_format = NULL,$_blanketPeriod = NULL,$_importerSpecification = NULL,$_signatureContact = NULL,$_producerSpecification = NULL,$_producers = NULL,$_customerImageUsages = NULL)
	{
		parent::__construct(array('Format'=>$_format,'BlanketPeriod'=>$_blanketPeriod,'ImporterSpecification'=>$_importerSpecification,'SignatureContact'=>$_signatureContact,'ProducerSpecification'=>$_producerSpecification,'Producers'=>$_producers,'CustomerImageUsages'=>$_customerImageUsages));
	}
	/**
	 * Get Format value
	 * @return MytestStructShippingDocumentFormat|null
	 */
	public function getFormat()
	{
		return $this->Format;
	}
	/**
	 * Set Format value
	 * @param MytestStructShippingDocumentFormat $_format the Format
	 * @return MytestStructShippingDocumentFormat
	 */
	public function setFormat($_format)
	{
		return ($this->Format = $_format);
	}
	/**
	 * Get BlanketPeriod value
	 * @return MytestStructDateRange|null
	 */
	public function getBlanketPeriod()
	{
		return $this->BlanketPeriod;
	}
	/**
	 * Set BlanketPeriod value
	 * @param MytestStructDateRange $_blanketPeriod the BlanketPeriod
	 * @return MytestStructDateRange
	 */
	public function setBlanketPeriod($_blanketPeriod)
	{
		return ($this->BlanketPeriod = $_blanketPeriod);
	}
	/**
	 * Get ImporterSpecification value
	 * @return MytestEnumNaftaImporterSpecificationType|null
	 */
	public function getImporterSpecification()
	{
		return $this->ImporterSpecification;
	}
	/**
	 * Set ImporterSpecification value
	 * @uses MytestEnumNaftaImporterSpecificationType::valueIsValid()
	 * @param MytestEnumNaftaImporterSpecificationType $_importerSpecification the ImporterSpecification
	 * @return MytestEnumNaftaImporterSpecificationType
	 */
	public function setImporterSpecification($_importerSpecification)
	{
		if(!MytestEnumNaftaImporterSpecificationType::valueIsValid($_importerSpecification))
		{
			return false;
		}
		return ($this->ImporterSpecification = $_importerSpecification);
	}
	/**
	 * Get SignatureContact value
	 * @return MytestStructContact|null
	 */
	public function getSignatureContact()
	{
		return $this->SignatureContact;
	}
	/**
	 * Set SignatureContact value
	 * @param MytestStructContact $_signatureContact the SignatureContact
	 * @return MytestStructContact
	 */
	public function setSignatureContact($_signatureContact)
	{
		return ($this->SignatureContact = $_signatureContact);
	}
	/**
	 * Get ProducerSpecification value
	 * @return MytestEnumNaftaProducerSpecificationType|null
	 */
	public function getProducerSpecification()
	{
		return $this->ProducerSpecification;
	}
	/**
	 * Set ProducerSpecification value
	 * @uses MytestEnumNaftaProducerSpecificationType::valueIsValid()
	 * @param MytestEnumNaftaProducerSpecificationType $_producerSpecification the ProducerSpecification
	 * @return MytestEnumNaftaProducerSpecificationType
	 */
	public function setProducerSpecification($_producerSpecification)
	{
		if(!MytestEnumNaftaProducerSpecificationType::valueIsValid($_producerSpecification))
		{
			return false;
		}
		return ($this->ProducerSpecification = $_producerSpecification);
	}
	/**
	 * Get Producers value
	 * @return MytestStructNaftaProducer|null
	 */
	public function getProducers()
	{
		return $this->Producers;
	}
	/**
	 * Set Producers value
	 * @param MytestStructNaftaProducer $_producers the Producers
	 * @return MytestStructNaftaProducer
	 */
	public function setProducers($_producers)
	{
		return ($this->Producers = $_producers);
	}
	/**
	 * Get CustomerImageUsages value
	 * @return MytestStructCustomerImageUsage|null
	 */
	public function getCustomerImageUsages()
	{
		return $this->CustomerImageUsages;
	}
	/**
	 * Set CustomerImageUsages value
	 * @param MytestStructCustomerImageUsage $_customerImageUsages the CustomerImageUsages
	 * @return MytestStructCustomerImageUsage
	 */
	public function setCustomerImageUsages($_customerImageUsages)
	{
		return ($this->CustomerImageUsages = $_customerImageUsages);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructNaftaCertificateOfOriginDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>