<?php
/**
 * File for class MytestStructNaftaCommodityDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructNaftaCommodityDetail originally named NaftaCommodityDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructNaftaCommodityDetail extends MytestWsdlClass
{
	/**
	 * The PreferenceCriterion
	 * Meta informations extracted from the WSDL
	 * - documentation : Defined by NAFTA regulations.
	 * - minOccurs : 0
	 * @var MytestEnumNaftaPreferenceCriterionCode
	 */
	public $PreferenceCriterion;
	/**
	 * The ProducerDetermination
	 * Meta informations extracted from the WSDL
	 * - documentation : Defined by NAFTA regulations.
	 * - minOccurs : 0
	 * @var MytestEnumNaftaProducerDeterminationCode
	 */
	public $ProducerDetermination;
	/**
	 * The ProducerId
	 * Meta informations extracted from the WSDL
	 * - documentation : Identification of which producer is associated with this commodity (if multiple producers are used in a single shipment).
	 * - minOccurs : 0
	 * @var string
	 */
	public $ProducerId;
	/**
	 * The NetCostMethod
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumNaftaNetCostMethodCode
	 */
	public $NetCostMethod;
	/**
	 * The NetCostDateRange
	 * Meta informations extracted from the WSDL
	 * - documentation : Date range over which RVC net cost was calculated.
	 * - minOccurs : 0
	 * @var MytestStructDateRange
	 */
	public $NetCostDateRange;
	/**
	 * Constructor method for NaftaCommodityDetail
	 * @see parent::__construct()
	 * @param MytestEnumNaftaPreferenceCriterionCode $_preferenceCriterion
	 * @param MytestEnumNaftaProducerDeterminationCode $_producerDetermination
	 * @param string $_producerId
	 * @param MytestEnumNaftaNetCostMethodCode $_netCostMethod
	 * @param MytestStructDateRange $_netCostDateRange
	 * @return MytestStructNaftaCommodityDetail
	 */
	public function __construct($_preferenceCriterion = NULL,$_producerDetermination = NULL,$_producerId = NULL,$_netCostMethod = NULL,$_netCostDateRange = NULL)
	{
		parent::__construct(array('PreferenceCriterion'=>$_preferenceCriterion,'ProducerDetermination'=>$_producerDetermination,'ProducerId'=>$_producerId,'NetCostMethod'=>$_netCostMethod,'NetCostDateRange'=>$_netCostDateRange));
	}
	/**
	 * Get PreferenceCriterion value
	 * @return MytestEnumNaftaPreferenceCriterionCode|null
	 */
	public function getPreferenceCriterion()
	{
		return $this->PreferenceCriterion;
	}
	/**
	 * Set PreferenceCriterion value
	 * @uses MytestEnumNaftaPreferenceCriterionCode::valueIsValid()
	 * @param MytestEnumNaftaPreferenceCriterionCode $_preferenceCriterion the PreferenceCriterion
	 * @return MytestEnumNaftaPreferenceCriterionCode
	 */
	public function setPreferenceCriterion($_preferenceCriterion)
	{
		if(!MytestEnumNaftaPreferenceCriterionCode::valueIsValid($_preferenceCriterion))
		{
			return false;
		}
		return ($this->PreferenceCriterion = $_preferenceCriterion);
	}
	/**
	 * Get ProducerDetermination value
	 * @return MytestEnumNaftaProducerDeterminationCode|null
	 */
	public function getProducerDetermination()
	{
		return $this->ProducerDetermination;
	}
	/**
	 * Set ProducerDetermination value
	 * @uses MytestEnumNaftaProducerDeterminationCode::valueIsValid()
	 * @param MytestEnumNaftaProducerDeterminationCode $_producerDetermination the ProducerDetermination
	 * @return MytestEnumNaftaProducerDeterminationCode
	 */
	public function setProducerDetermination($_producerDetermination)
	{
		if(!MytestEnumNaftaProducerDeterminationCode::valueIsValid($_producerDetermination))
		{
			return false;
		}
		return ($this->ProducerDetermination = $_producerDetermination);
	}
	/**
	 * Get ProducerId value
	 * @return string|null
	 */
	public function getProducerId()
	{
		return $this->ProducerId;
	}
	/**
	 * Set ProducerId value
	 * @param string $_producerId the ProducerId
	 * @return string
	 */
	public function setProducerId($_producerId)
	{
		return ($this->ProducerId = $_producerId);
	}
	/**
	 * Get NetCostMethod value
	 * @return MytestEnumNaftaNetCostMethodCode|null
	 */
	public function getNetCostMethod()
	{
		return $this->NetCostMethod;
	}
	/**
	 * Set NetCostMethod value
	 * @uses MytestEnumNaftaNetCostMethodCode::valueIsValid()
	 * @param MytestEnumNaftaNetCostMethodCode $_netCostMethod the NetCostMethod
	 * @return MytestEnumNaftaNetCostMethodCode
	 */
	public function setNetCostMethod($_netCostMethod)
	{
		if(!MytestEnumNaftaNetCostMethodCode::valueIsValid($_netCostMethod))
		{
			return false;
		}
		return ($this->NetCostMethod = $_netCostMethod);
	}
	/**
	 * Get NetCostDateRange value
	 * @return MytestStructDateRange|null
	 */
	public function getNetCostDateRange()
	{
		return $this->NetCostDateRange;
	}
	/**
	 * Set NetCostDateRange value
	 * @param MytestStructDateRange $_netCostDateRange the NetCostDateRange
	 * @return MytestStructDateRange
	 */
	public function setNetCostDateRange($_netCostDateRange)
	{
		return ($this->NetCostDateRange = $_netCostDateRange);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructNaftaCommodityDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>