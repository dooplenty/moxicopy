<?php
/**
 * Test with Mytest
 * @package Mytest
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
ini_set('memory_limit','512M');
ini_set('display_errors', true);
error_reporting(-1);
/**
 * Load autoload
 */
require_once dirname(__FILE__) . '/MytestAutoload.php';
/**
 * Mytest Informations
 */
define('MYTEST_WSDL_URL','var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml');
define('MYTEST_USER_LOGIN','');
define('MYTEST_USER_PASSWORD','');
/**
 * Wsdl instanciation infos
 */
$wsdl = array();
$wsdl[MytestWsdlClass::WSDL_URL] = MYTEST_WSDL_URL;
$wsdl[MytestWsdlClass::WSDL_CACHE_WSDL] = WSDL_CACHE_NONE;
$wsdl[MytestWsdlClass::WSDL_TRACE] = true;
if(MYTEST_USER_LOGIN !== '')
	$wsdl[MytestWsdlClass::WSDL_LOGIN] = MYTEST_USER_LOGIN;
if(MYTEST_USER_PASSWORD !== '')
	$wsdl[MytestWsdlClass::WSDL_PASSWD] = MYTEST_USER_PASSWORD;
// etc....
/**
 * Examples
 */


/******************************
 * Example for MytestServiceGet
 */
$mytestServiceGet = new MytestServiceGet($wsdl);
// sample call for MytestServiceGet::getRates()
if($mytestServiceGet->getRates(new MytestStructRateRequest(/*** update parameters list ***/)))
	print_r($mytestServiceGet->getResult());
else
	print_r($mytestServiceGet->getLastError());
?>