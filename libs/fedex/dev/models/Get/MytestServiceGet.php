<?php
/**
 * File for class MytestServiceGet
 * @package Mytest
 * @subpackage Services
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestServiceGet originally named Get
 * @package Mytest
 * @subpackage Services
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestServiceGet extends MytestWsdlClass
{
	/**
	 * Method to call the operation originally named getRates
	 * @uses MytestWsdlClass::getSoapClient()
	 * @uses MytestWsdlClass::setResult()
	 * @uses MytestWsdlClass::getResult()
	 * @uses MytestWsdlClass::saveLastError()
	 * @uses MytestStructRateRequest::getWebAuthenticationDetail()
	 * @uses MytestStructRateRequest::getClientDetail()
	 * @uses MytestStructRateRequest::getVersion()
	 * @uses MytestStructRateRequest::getRequestedShipment()
	 * @uses MytestStructRateRequest::getTransactionDetail()
	 * @uses MytestStructRateRequest::getReturnTransitAndCommit()
	 * @uses MytestStructRateRequest::getCarrierCodes()
	 * @uses MytestStructRateRequest::getVariableOptions()
	 * @param MytestStructRateRequest $_mytestStructRateRequest
	 * @return MytestStructRateReply
	 */
	public function getRates(MytestStructRateRequest $_mytestStructRateRequest)
	{
		try
		{
			$this->setResult(new MytestStructRateReply(self::getSoapClient()->getRates(array('WebAuthenticationDetail'=>$_mytestStructRateRequest->getWebAuthenticationDetail(),'ClientDetail'=>$_mytestStructRateRequest->getClientDetail(),'Version'=>$_mytestStructRateRequest->getVersion(),'RequestedShipment'=>$_mytestStructRateRequest->getRequestedShipment(),'TransactionDetail'=>$_mytestStructRateRequest->getTransactionDetail(),'ReturnTransitAndCommit'=>$_mytestStructRateRequest->getReturnTransitAndCommit(),'CarrierCodes'=>$_mytestStructRateRequest->getCarrierCodes(),'VariableOptions'=>$_mytestStructRateRequest->getVariableOptions()))));
		}
		catch(SoapFault $soapFault)
		{
			return !$this->saveLastError(__METHOD__,$soapFault);
		}
		return $this->getResult();
	}
	/**
	 * Returns the result
	 * @see MytestWsdlClass::getResult()
	 * @return MytestStructRateReply
	 */
	public function getResult()
	{
		return parent::getResult();
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>