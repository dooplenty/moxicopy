<?php
/**
 * File for class MytestStructLiabilityCoverageDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructLiabilityCoverageDetail originally named LiabilityCoverageDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructLiabilityCoverageDetail extends MytestWsdlClass
{
	/**
	 * The CoverageType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumLiabilityCoverageType
	 */
	public $CoverageType;
	/**
	 * The CoverageAmount
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the Liability Coverage Amount. For Jan 2010 this value represents coverage amount per pound
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $CoverageAmount;
	/**
	 * Constructor method for LiabilityCoverageDetail
	 * @see parent::__construct()
	 * @param MytestEnumLiabilityCoverageType $_coverageType
	 * @param MytestStructMoney $_coverageAmount
	 * @return MytestStructLiabilityCoverageDetail
	 */
	public function __construct($_coverageType = NULL,$_coverageAmount = NULL)
	{
		parent::__construct(array('CoverageType'=>$_coverageType,'CoverageAmount'=>$_coverageAmount));
	}
	/**
	 * Get CoverageType value
	 * @return MytestEnumLiabilityCoverageType|null
	 */
	public function getCoverageType()
	{
		return $this->CoverageType;
	}
	/**
	 * Set CoverageType value
	 * @uses MytestEnumLiabilityCoverageType::valueIsValid()
	 * @param MytestEnumLiabilityCoverageType $_coverageType the CoverageType
	 * @return MytestEnumLiabilityCoverageType
	 */
	public function setCoverageType($_coverageType)
	{
		if(!MytestEnumLiabilityCoverageType::valueIsValid($_coverageType))
		{
			return false;
		}
		return ($this->CoverageType = $_coverageType);
	}
	/**
	 * Get CoverageAmount value
	 * @return MytestStructMoney|null
	 */
	public function getCoverageAmount()
	{
		return $this->CoverageAmount;
	}
	/**
	 * Set CoverageAmount value
	 * @param MytestStructMoney $_coverageAmount the CoverageAmount
	 * @return MytestStructMoney
	 */
	public function setCoverageAmount($_coverageAmount)
	{
		return ($this->CoverageAmount = $_coverageAmount);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructLiabilityCoverageDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>