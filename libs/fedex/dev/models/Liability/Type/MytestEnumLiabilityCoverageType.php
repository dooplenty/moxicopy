<?php
/**
 * File for class MytestEnumLiabilityCoverageType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumLiabilityCoverageType originally named LiabilityCoverageType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumLiabilityCoverageType extends MytestWsdlClass
{
	/**
	 * Constant for value 'NEW'
	 * @return string 'NEW'
	 */
	const VALUE_NEW = 'NEW';
	/**
	 * Constant for value 'USED_OR_RECONDITIONED'
	 * @return string 'USED_OR_RECONDITIONED'
	 */
	const VALUE_USED_OR_RECONDITIONED = 'USED_OR_RECONDITIONED';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumLiabilityCoverageType::VALUE_NEW
	 * @uses MytestEnumLiabilityCoverageType::VALUE_USED_OR_RECONDITIONED
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumLiabilityCoverageType::VALUE_NEW,MytestEnumLiabilityCoverageType::VALUE_USED_OR_RECONDITIONED));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>