<?php
/**
 * File for class MytestStructLabelSpecification
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructLabelSpecification originally named LabelSpecification
 * Documentation : Description of shipping label to be returned in the reply
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructLabelSpecification extends MytestWsdlClass
{
	/**
	 * The LabelFormatType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumLabelFormatType
	 */
	public $LabelFormatType;
	/**
	 * The ImageType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumShippingDocumentImageType
	 */
	public $ImageType;
	/**
	 * The LabelStockType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumLabelStockType
	 */
	public $LabelStockType;
	/**
	 * The LabelPrintingOrientation
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumLabelPrintingOrientationType
	 */
	public $LabelPrintingOrientation;
	/**
	 * The LabelRotation
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumLabelRotationType
	 */
	public $LabelRotation;
	/**
	 * The PrintedLabelOrigin
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructContactAndAddress
	 */
	public $PrintedLabelOrigin;
	/**
	 * The CustomerSpecifiedDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructCustomerSpecifiedLabelDetail
	 */
	public $CustomerSpecifiedDetail;
	/**
	 * Constructor method for LabelSpecification
	 * @see parent::__construct()
	 * @param MytestEnumLabelFormatType $_labelFormatType
	 * @param MytestEnumShippingDocumentImageType $_imageType
	 * @param MytestEnumLabelStockType $_labelStockType
	 * @param MytestEnumLabelPrintingOrientationType $_labelPrintingOrientation
	 * @param MytestEnumLabelRotationType $_labelRotation
	 * @param MytestStructContactAndAddress $_printedLabelOrigin
	 * @param MytestStructCustomerSpecifiedLabelDetail $_customerSpecifiedDetail
	 * @return MytestStructLabelSpecification
	 */
	public function __construct($_labelFormatType = NULL,$_imageType = NULL,$_labelStockType = NULL,$_labelPrintingOrientation = NULL,$_labelRotation = NULL,$_printedLabelOrigin = NULL,$_customerSpecifiedDetail = NULL)
	{
		parent::__construct(array('LabelFormatType'=>$_labelFormatType,'ImageType'=>$_imageType,'LabelStockType'=>$_labelStockType,'LabelPrintingOrientation'=>$_labelPrintingOrientation,'LabelRotation'=>$_labelRotation,'PrintedLabelOrigin'=>$_printedLabelOrigin,'CustomerSpecifiedDetail'=>$_customerSpecifiedDetail));
	}
	/**
	 * Get LabelFormatType value
	 * @return MytestEnumLabelFormatType|null
	 */
	public function getLabelFormatType()
	{
		return $this->LabelFormatType;
	}
	/**
	 * Set LabelFormatType value
	 * @uses MytestEnumLabelFormatType::valueIsValid()
	 * @param MytestEnumLabelFormatType $_labelFormatType the LabelFormatType
	 * @return MytestEnumLabelFormatType
	 */
	public function setLabelFormatType($_labelFormatType)
	{
		if(!MytestEnumLabelFormatType::valueIsValid($_labelFormatType))
		{
			return false;
		}
		return ($this->LabelFormatType = $_labelFormatType);
	}
	/**
	 * Get ImageType value
	 * @return MytestEnumShippingDocumentImageType|null
	 */
	public function getImageType()
	{
		return $this->ImageType;
	}
	/**
	 * Set ImageType value
	 * @uses MytestEnumShippingDocumentImageType::valueIsValid()
	 * @param MytestEnumShippingDocumentImageType $_imageType the ImageType
	 * @return MytestEnumShippingDocumentImageType
	 */
	public function setImageType($_imageType)
	{
		if(!MytestEnumShippingDocumentImageType::valueIsValid($_imageType))
		{
			return false;
		}
		return ($this->ImageType = $_imageType);
	}
	/**
	 * Get LabelStockType value
	 * @return MytestEnumLabelStockType|null
	 */
	public function getLabelStockType()
	{
		return $this->LabelStockType;
	}
	/**
	 * Set LabelStockType value
	 * @uses MytestEnumLabelStockType::valueIsValid()
	 * @param MytestEnumLabelStockType $_labelStockType the LabelStockType
	 * @return MytestEnumLabelStockType
	 */
	public function setLabelStockType($_labelStockType)
	{
		if(!MytestEnumLabelStockType::valueIsValid($_labelStockType))
		{
			return false;
		}
		return ($this->LabelStockType = $_labelStockType);
	}
	/**
	 * Get LabelPrintingOrientation value
	 * @return MytestEnumLabelPrintingOrientationType|null
	 */
	public function getLabelPrintingOrientation()
	{
		return $this->LabelPrintingOrientation;
	}
	/**
	 * Set LabelPrintingOrientation value
	 * @uses MytestEnumLabelPrintingOrientationType::valueIsValid()
	 * @param MytestEnumLabelPrintingOrientationType $_labelPrintingOrientation the LabelPrintingOrientation
	 * @return MytestEnumLabelPrintingOrientationType
	 */
	public function setLabelPrintingOrientation($_labelPrintingOrientation)
	{
		if(!MytestEnumLabelPrintingOrientationType::valueIsValid($_labelPrintingOrientation))
		{
			return false;
		}
		return ($this->LabelPrintingOrientation = $_labelPrintingOrientation);
	}
	/**
	 * Get LabelRotation value
	 * @return MytestEnumLabelRotationType|null
	 */
	public function getLabelRotation()
	{
		return $this->LabelRotation;
	}
	/**
	 * Set LabelRotation value
	 * @uses MytestEnumLabelRotationType::valueIsValid()
	 * @param MytestEnumLabelRotationType $_labelRotation the LabelRotation
	 * @return MytestEnumLabelRotationType
	 */
	public function setLabelRotation($_labelRotation)
	{
		if(!MytestEnumLabelRotationType::valueIsValid($_labelRotation))
		{
			return false;
		}
		return ($this->LabelRotation = $_labelRotation);
	}
	/**
	 * Get PrintedLabelOrigin value
	 * @return MytestStructContactAndAddress|null
	 */
	public function getPrintedLabelOrigin()
	{
		return $this->PrintedLabelOrigin;
	}
	/**
	 * Set PrintedLabelOrigin value
	 * @param MytestStructContactAndAddress $_printedLabelOrigin the PrintedLabelOrigin
	 * @return MytestStructContactAndAddress
	 */
	public function setPrintedLabelOrigin($_printedLabelOrigin)
	{
		return ($this->PrintedLabelOrigin = $_printedLabelOrigin);
	}
	/**
	 * Get CustomerSpecifiedDetail value
	 * @return MytestStructCustomerSpecifiedLabelDetail|null
	 */
	public function getCustomerSpecifiedDetail()
	{
		return $this->CustomerSpecifiedDetail;
	}
	/**
	 * Set CustomerSpecifiedDetail value
	 * @param MytestStructCustomerSpecifiedLabelDetail $_customerSpecifiedDetail the CustomerSpecifiedDetail
	 * @return MytestStructCustomerSpecifiedLabelDetail
	 */
	public function setCustomerSpecifiedDetail($_customerSpecifiedDetail)
	{
		return ($this->CustomerSpecifiedDetail = $_customerSpecifiedDetail);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructLabelSpecification
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>