<?php
/**
 * File for class MytestEnumLabelFormatType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumLabelFormatType originally named LabelFormatType
 * Documentation : Specifies the type of label to be returned.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumLabelFormatType extends MytestWsdlClass
{
	/**
	 * Constant for value 'COMMON2D'
	 * @return string 'COMMON2D'
	 */
	const VALUE_COMMON2D = 'COMMON2D';
	/**
	 * Constant for value 'LABEL_DATA_ONLY'
	 * @return string 'LABEL_DATA_ONLY'
	 */
	const VALUE_LABEL_DATA_ONLY = 'LABEL_DATA_ONLY';
	/**
	 * Constant for value 'MAILROOM'
	 * @return string 'MAILROOM'
	 */
	const VALUE_MAILROOM = 'MAILROOM';
	/**
	 * Constant for value 'NO_LABEL'
	 * @return string 'NO_LABEL'
	 */
	const VALUE_NO_LABEL = 'NO_LABEL';
	/**
	 * Constant for value 'OPERATIONAL_LABEL'
	 * @return string 'OPERATIONAL_LABEL'
	 */
	const VALUE_OPERATIONAL_LABEL = 'OPERATIONAL_LABEL';
	/**
	 * Constant for value 'PRE_COMMON2D'
	 * @return string 'PRE_COMMON2D'
	 */
	const VALUE_PRE_COMMON2D = 'PRE_COMMON2D';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumLabelFormatType::VALUE_COMMON2D
	 * @uses MytestEnumLabelFormatType::VALUE_LABEL_DATA_ONLY
	 * @uses MytestEnumLabelFormatType::VALUE_MAILROOM
	 * @uses MytestEnumLabelFormatType::VALUE_NO_LABEL
	 * @uses MytestEnumLabelFormatType::VALUE_OPERATIONAL_LABEL
	 * @uses MytestEnumLabelFormatType::VALUE_PRE_COMMON2D
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumLabelFormatType::VALUE_COMMON2D,MytestEnumLabelFormatType::VALUE_LABEL_DATA_ONLY,MytestEnumLabelFormatType::VALUE_MAILROOM,MytestEnumLabelFormatType::VALUE_NO_LABEL,MytestEnumLabelFormatType::VALUE_OPERATIONAL_LABEL,MytestEnumLabelFormatType::VALUE_PRE_COMMON2D));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>