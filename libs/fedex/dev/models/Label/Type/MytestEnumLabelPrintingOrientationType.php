<?php
/**
 * File for class MytestEnumLabelPrintingOrientationType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumLabelPrintingOrientationType originally named LabelPrintingOrientationType
 * Documentation : This indicates if the top or bottom of the label comes out of the printer first.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumLabelPrintingOrientationType extends MytestWsdlClass
{
	/**
	 * Constant for value 'BOTTOM_EDGE_OF_TEXT_FIRST'
	 * @return string 'BOTTOM_EDGE_OF_TEXT_FIRST'
	 */
	const VALUE_BOTTOM_EDGE_OF_TEXT_FIRST = 'BOTTOM_EDGE_OF_TEXT_FIRST';
	/**
	 * Constant for value 'TOP_EDGE_OF_TEXT_FIRST'
	 * @return string 'TOP_EDGE_OF_TEXT_FIRST'
	 */
	const VALUE_TOP_EDGE_OF_TEXT_FIRST = 'TOP_EDGE_OF_TEXT_FIRST';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumLabelPrintingOrientationType::VALUE_BOTTOM_EDGE_OF_TEXT_FIRST
	 * @uses MytestEnumLabelPrintingOrientationType::VALUE_TOP_EDGE_OF_TEXT_FIRST
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumLabelPrintingOrientationType::VALUE_BOTTOM_EDGE_OF_TEXT_FIRST,MytestEnumLabelPrintingOrientationType::VALUE_TOP_EDGE_OF_TEXT_FIRST));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>