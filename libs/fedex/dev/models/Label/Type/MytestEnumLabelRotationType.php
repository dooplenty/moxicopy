<?php
/**
 * File for class MytestEnumLabelRotationType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumLabelRotationType originally named LabelRotationType
 * Documentation : Relative to normal orientation for the printer.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumLabelRotationType extends MytestWsdlClass
{
	/**
	 * Constant for value 'LEFT'
	 * @return string 'LEFT'
	 */
	const VALUE_LEFT = 'LEFT';
	/**
	 * Constant for value 'NONE'
	 * @return string 'NONE'
	 */
	const VALUE_NONE = 'NONE';
	/**
	 * Constant for value 'RIGHT'
	 * @return string 'RIGHT'
	 */
	const VALUE_RIGHT = 'RIGHT';
	/**
	 * Constant for value 'UPSIDE_DOWN'
	 * @return string 'UPSIDE_DOWN'
	 */
	const VALUE_UPSIDE_DOWN = 'UPSIDE_DOWN';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumLabelRotationType::VALUE_LEFT
	 * @uses MytestEnumLabelRotationType::VALUE_NONE
	 * @uses MytestEnumLabelRotationType::VALUE_RIGHT
	 * @uses MytestEnumLabelRotationType::VALUE_UPSIDE_DOWN
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumLabelRotationType::VALUE_LEFT,MytestEnumLabelRotationType::VALUE_NONE,MytestEnumLabelRotationType::VALUE_RIGHT,MytestEnumLabelRotationType::VALUE_UPSIDE_DOWN));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>