<?php
/**
 * File for class MytestEnumLabelStockType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumLabelStockType originally named LabelStockType
 * Documentation : For thermal printer labels this indicates the size of the label and the location of the doc tab if present.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumLabelStockType extends MytestWsdlClass
{
	/**
	 * Constant for value 'PAPER_4X6'
	 * @return string 'PAPER_4X6'
	 */
	const VALUE_PAPER_4X6 = 'PAPER_4X6';
	/**
	 * Constant for value 'PAPER_4X8'
	 * @return string 'PAPER_4X8'
	 */
	const VALUE_PAPER_4X8 = 'PAPER_4X8';
	/**
	 * Constant for value 'PAPER_4X9'
	 * @return string 'PAPER_4X9'
	 */
	const VALUE_PAPER_4X9 = 'PAPER_4X9';
	/**
	 * Constant for value 'PAPER_7X4.75'
	 * @return string 'PAPER_7X4.75'
	 */
	const VALUE_PAPER_7X4_75 = 'PAPER_7X4.75';
	/**
	 * Constant for value 'PAPER_8.5X11_BOTTOM_HALF_LABEL'
	 * @return string 'PAPER_8.5X11_BOTTOM_HALF_LABEL'
	 */
	const VALUE_PAPER_8_5X11_BOTTOM_HALF_LABEL = 'PAPER_8.5X11_BOTTOM_HALF_LABEL';
	/**
	 * Constant for value 'PAPER_8.5X11_TOP_HALF_LABEL'
	 * @return string 'PAPER_8.5X11_TOP_HALF_LABEL'
	 */
	const VALUE_PAPER_8_5X11_TOP_HALF_LABEL = 'PAPER_8.5X11_TOP_HALF_LABEL';
	/**
	 * Constant for value 'STOCK_4X6'
	 * @return string 'STOCK_4X6'
	 */
	const VALUE_STOCK_4X6 = 'STOCK_4X6';
	/**
	 * Constant for value 'STOCK_4X6.75_LEADING_DOC_TAB'
	 * @return string 'STOCK_4X6.75_LEADING_DOC_TAB'
	 */
	const VALUE_STOCK_4X6_75_LEADING_DOC_TAB = 'STOCK_4X6.75_LEADING_DOC_TAB';
	/**
	 * Constant for value 'STOCK_4X6.75_TRAILING_DOC_TAB'
	 * @return string 'STOCK_4X6.75_TRAILING_DOC_TAB'
	 */
	const VALUE_STOCK_4X6_75_TRAILING_DOC_TAB = 'STOCK_4X6.75_TRAILING_DOC_TAB';
	/**
	 * Constant for value 'STOCK_4X8'
	 * @return string 'STOCK_4X8'
	 */
	const VALUE_STOCK_4X8 = 'STOCK_4X8';
	/**
	 * Constant for value 'STOCK_4X9_LEADING_DOC_TAB'
	 * @return string 'STOCK_4X9_LEADING_DOC_TAB'
	 */
	const VALUE_STOCK_4X9_LEADING_DOC_TAB = 'STOCK_4X9_LEADING_DOC_TAB';
	/**
	 * Constant for value 'STOCK_4X9_TRAILING_DOC_TAB'
	 * @return string 'STOCK_4X9_TRAILING_DOC_TAB'
	 */
	const VALUE_STOCK_4X9_TRAILING_DOC_TAB = 'STOCK_4X9_TRAILING_DOC_TAB';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumLabelStockType::VALUE_PAPER_4X6
	 * @uses MytestEnumLabelStockType::VALUE_PAPER_4X8
	 * @uses MytestEnumLabelStockType::VALUE_PAPER_4X9
	 * @uses MytestEnumLabelStockType::VALUE_PAPER_7X4_75
	 * @uses MytestEnumLabelStockType::VALUE_PAPER_8_5X11_BOTTOM_HALF_LABEL
	 * @uses MytestEnumLabelStockType::VALUE_PAPER_8_5X11_TOP_HALF_LABEL
	 * @uses MytestEnumLabelStockType::VALUE_STOCK_4X6
	 * @uses MytestEnumLabelStockType::VALUE_STOCK_4X6_75_LEADING_DOC_TAB
	 * @uses MytestEnumLabelStockType::VALUE_STOCK_4X6_75_TRAILING_DOC_TAB
	 * @uses MytestEnumLabelStockType::VALUE_STOCK_4X8
	 * @uses MytestEnumLabelStockType::VALUE_STOCK_4X9_LEADING_DOC_TAB
	 * @uses MytestEnumLabelStockType::VALUE_STOCK_4X9_TRAILING_DOC_TAB
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumLabelStockType::VALUE_PAPER_4X6,MytestEnumLabelStockType::VALUE_PAPER_4X8,MytestEnumLabelStockType::VALUE_PAPER_4X9,MytestEnumLabelStockType::VALUE_PAPER_7X4_75,MytestEnumLabelStockType::VALUE_PAPER_8_5X11_BOTTOM_HALF_LABEL,MytestEnumLabelStockType::VALUE_PAPER_8_5X11_TOP_HALF_LABEL,MytestEnumLabelStockType::VALUE_STOCK_4X6,MytestEnumLabelStockType::VALUE_STOCK_4X6_75_LEADING_DOC_TAB,MytestEnumLabelStockType::VALUE_STOCK_4X6_75_TRAILING_DOC_TAB,MytestEnumLabelStockType::VALUE_STOCK_4X8,MytestEnumLabelStockType::VALUE_STOCK_4X9_LEADING_DOC_TAB,MytestEnumLabelStockType::VALUE_STOCK_4X9_TRAILING_DOC_TAB));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>