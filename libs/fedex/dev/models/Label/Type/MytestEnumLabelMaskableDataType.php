<?php
/**
 * File for class MytestEnumLabelMaskableDataType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumLabelMaskableDataType originally named LabelMaskableDataType
 * Documentation : Names for data elements / areas which may be suppressed from printing on labels.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumLabelMaskableDataType extends MytestWsdlClass
{
	/**
	 * Constant for value 'CUSTOMS_VALUE'
	 * @return string 'CUSTOMS_VALUE'
	 */
	const VALUE_CUSTOMS_VALUE = 'CUSTOMS_VALUE';
	/**
	 * Constant for value 'DIMENSIONS'
	 * @return string 'DIMENSIONS'
	 */
	const VALUE_DIMENSIONS = 'DIMENSIONS';
	/**
	 * Constant for value 'DUTIES_AND_TAXES_PAYOR_ACCOUNT_NUMBER'
	 * @return string 'DUTIES_AND_TAXES_PAYOR_ACCOUNT_NUMBER'
	 */
	const VALUE_DUTIES_AND_TAXES_PAYOR_ACCOUNT_NUMBER = 'DUTIES_AND_TAXES_PAYOR_ACCOUNT_NUMBER';
	/**
	 * Constant for value 'FREIGHT_PAYOR_ACCOUNT_NUMBER'
	 * @return string 'FREIGHT_PAYOR_ACCOUNT_NUMBER'
	 */
	const VALUE_FREIGHT_PAYOR_ACCOUNT_NUMBER = 'FREIGHT_PAYOR_ACCOUNT_NUMBER';
	/**
	 * Constant for value 'PACKAGE_SEQUENCE_AND_COUNT'
	 * @return string 'PACKAGE_SEQUENCE_AND_COUNT'
	 */
	const VALUE_PACKAGE_SEQUENCE_AND_COUNT = 'PACKAGE_SEQUENCE_AND_COUNT';
	/**
	 * Constant for value 'SHIPPER_ACCOUNT_NUMBER'
	 * @return string 'SHIPPER_ACCOUNT_NUMBER'
	 */
	const VALUE_SHIPPER_ACCOUNT_NUMBER = 'SHIPPER_ACCOUNT_NUMBER';
	/**
	 * Constant for value 'SUPPLEMENTAL_LABEL_DOC_TAB'
	 * @return string 'SUPPLEMENTAL_LABEL_DOC_TAB'
	 */
	const VALUE_SUPPLEMENTAL_LABEL_DOC_TAB = 'SUPPLEMENTAL_LABEL_DOC_TAB';
	/**
	 * Constant for value 'TERMS_AND_CONDITIONS'
	 * @return string 'TERMS_AND_CONDITIONS'
	 */
	const VALUE_TERMS_AND_CONDITIONS = 'TERMS_AND_CONDITIONS';
	/**
	 * Constant for value 'TOTAL_WEIGHT'
	 * @return string 'TOTAL_WEIGHT'
	 */
	const VALUE_TOTAL_WEIGHT = 'TOTAL_WEIGHT';
	/**
	 * Constant for value 'TRANSPORTATION_CHARGES_PAYOR_ACCOUNT_NUMBER'
	 * @return string 'TRANSPORTATION_CHARGES_PAYOR_ACCOUNT_NUMBER'
	 */
	const VALUE_TRANSPORTATION_CHARGES_PAYOR_ACCOUNT_NUMBER = 'TRANSPORTATION_CHARGES_PAYOR_ACCOUNT_NUMBER';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumLabelMaskableDataType::VALUE_CUSTOMS_VALUE
	 * @uses MytestEnumLabelMaskableDataType::VALUE_DIMENSIONS
	 * @uses MytestEnumLabelMaskableDataType::VALUE_DUTIES_AND_TAXES_PAYOR_ACCOUNT_NUMBER
	 * @uses MytestEnumLabelMaskableDataType::VALUE_FREIGHT_PAYOR_ACCOUNT_NUMBER
	 * @uses MytestEnumLabelMaskableDataType::VALUE_PACKAGE_SEQUENCE_AND_COUNT
	 * @uses MytestEnumLabelMaskableDataType::VALUE_SHIPPER_ACCOUNT_NUMBER
	 * @uses MytestEnumLabelMaskableDataType::VALUE_SUPPLEMENTAL_LABEL_DOC_TAB
	 * @uses MytestEnumLabelMaskableDataType::VALUE_TERMS_AND_CONDITIONS
	 * @uses MytestEnumLabelMaskableDataType::VALUE_TOTAL_WEIGHT
	 * @uses MytestEnumLabelMaskableDataType::VALUE_TRANSPORTATION_CHARGES_PAYOR_ACCOUNT_NUMBER
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumLabelMaskableDataType::VALUE_CUSTOMS_VALUE,MytestEnumLabelMaskableDataType::VALUE_DIMENSIONS,MytestEnumLabelMaskableDataType::VALUE_DUTIES_AND_TAXES_PAYOR_ACCOUNT_NUMBER,MytestEnumLabelMaskableDataType::VALUE_FREIGHT_PAYOR_ACCOUNT_NUMBER,MytestEnumLabelMaskableDataType::VALUE_PACKAGE_SEQUENCE_AND_COUNT,MytestEnumLabelMaskableDataType::VALUE_SHIPPER_ACCOUNT_NUMBER,MytestEnumLabelMaskableDataType::VALUE_SUPPLEMENTAL_LABEL_DOC_TAB,MytestEnumLabelMaskableDataType::VALUE_TERMS_AND_CONDITIONS,MytestEnumLabelMaskableDataType::VALUE_TOTAL_WEIGHT,MytestEnumLabelMaskableDataType::VALUE_TRANSPORTATION_CHARGES_PAYOR_ACCOUNT_NUMBER));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>