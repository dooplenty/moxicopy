<?php
/**
 * File for class MytestEnumSurchargeType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumSurchargeType originally named SurchargeType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumSurchargeType extends MytestWsdlClass
{
	/**
	 * Constant for value 'ADDITIONAL_HANDLING'
	 * @return string 'ADDITIONAL_HANDLING'
	 */
	const VALUE_ADDITIONAL_HANDLING = 'ADDITIONAL_HANDLING';
	/**
	 * Constant for value 'ANCILLARY_FEE'
	 * @return string 'ANCILLARY_FEE'
	 */
	const VALUE_ANCILLARY_FEE = 'ANCILLARY_FEE';
	/**
	 * Constant for value 'APPOINTMENT_DELIVERY'
	 * @return string 'APPOINTMENT_DELIVERY'
	 */
	const VALUE_APPOINTMENT_DELIVERY = 'APPOINTMENT_DELIVERY';
	/**
	 * Constant for value 'BROKER_SELECT_OPTION'
	 * @return string 'BROKER_SELECT_OPTION'
	 */
	const VALUE_BROKER_SELECT_OPTION = 'BROKER_SELECT_OPTION';
	/**
	 * Constant for value 'CANADIAN_DESTINATION'
	 * @return string 'CANADIAN_DESTINATION'
	 */
	const VALUE_CANADIAN_DESTINATION = 'CANADIAN_DESTINATION';
	/**
	 * Constant for value 'CLEARANCE_ENTRY_FEE'
	 * @return string 'CLEARANCE_ENTRY_FEE'
	 */
	const VALUE_CLEARANCE_ENTRY_FEE = 'CLEARANCE_ENTRY_FEE';
	/**
	 * Constant for value 'COD'
	 * @return string 'COD'
	 */
	const VALUE_COD = 'COD';
	/**
	 * Constant for value 'CUT_FLOWERS'
	 * @return string 'CUT_FLOWERS'
	 */
	const VALUE_CUT_FLOWERS = 'CUT_FLOWERS';
	/**
	 * Constant for value 'DANGEROUS_GOODS'
	 * @return string 'DANGEROUS_GOODS'
	 */
	const VALUE_DANGEROUS_GOODS = 'DANGEROUS_GOODS';
	/**
	 * Constant for value 'DELIVERY_AREA'
	 * @return string 'DELIVERY_AREA'
	 */
	const VALUE_DELIVERY_AREA = 'DELIVERY_AREA';
	/**
	 * Constant for value 'DELIVERY_CONFIRMATION'
	 * @return string 'DELIVERY_CONFIRMATION'
	 */
	const VALUE_DELIVERY_CONFIRMATION = 'DELIVERY_CONFIRMATION';
	/**
	 * Constant for value 'DOCUMENTATION_FEE'
	 * @return string 'DOCUMENTATION_FEE'
	 */
	const VALUE_DOCUMENTATION_FEE = 'DOCUMENTATION_FEE';
	/**
	 * Constant for value 'DRY_ICE'
	 * @return string 'DRY_ICE'
	 */
	const VALUE_DRY_ICE = 'DRY_ICE';
	/**
	 * Constant for value 'EMAIL_LABEL'
	 * @return string 'EMAIL_LABEL'
	 */
	const VALUE_EMAIL_LABEL = 'EMAIL_LABEL';
	/**
	 * Constant for value 'EUROPE_FIRST'
	 * @return string 'EUROPE_FIRST'
	 */
	const VALUE_EUROPE_FIRST = 'EUROPE_FIRST';
	/**
	 * Constant for value 'EXCESS_VALUE'
	 * @return string 'EXCESS_VALUE'
	 */
	const VALUE_EXCESS_VALUE = 'EXCESS_VALUE';
	/**
	 * Constant for value 'EXHIBITION'
	 * @return string 'EXHIBITION'
	 */
	const VALUE_EXHIBITION = 'EXHIBITION';
	/**
	 * Constant for value 'EXPORT'
	 * @return string 'EXPORT'
	 */
	const VALUE_EXPORT = 'EXPORT';
	/**
	 * Constant for value 'EXTRA_SURFACE_HANDLING_CHARGE'
	 * @return string 'EXTRA_SURFACE_HANDLING_CHARGE'
	 */
	const VALUE_EXTRA_SURFACE_HANDLING_CHARGE = 'EXTRA_SURFACE_HANDLING_CHARGE';
	/**
	 * Constant for value 'EXTREME_LENGTH'
	 * @return string 'EXTREME_LENGTH'
	 */
	const VALUE_EXTREME_LENGTH = 'EXTREME_LENGTH';
	/**
	 * Constant for value 'FEDEX_INTRACOUNTRY_FEES'
	 * @return string 'FEDEX_INTRACOUNTRY_FEES'
	 */
	const VALUE_FEDEX_INTRACOUNTRY_FEES = 'FEDEX_INTRACOUNTRY_FEES';
	/**
	 * Constant for value 'FEDEX_TAG'
	 * @return string 'FEDEX_TAG'
	 */
	const VALUE_FEDEX_TAG = 'FEDEX_TAG';
	/**
	 * Constant for value 'FICE'
	 * @return string 'FICE'
	 */
	const VALUE_FICE = 'FICE';
	/**
	 * Constant for value 'FLATBED'
	 * @return string 'FLATBED'
	 */
	const VALUE_FLATBED = 'FLATBED';
	/**
	 * Constant for value 'FREIGHT_GUARANTEE'
	 * @return string 'FREIGHT_GUARANTEE'
	 */
	const VALUE_FREIGHT_GUARANTEE = 'FREIGHT_GUARANTEE';
	/**
	 * Constant for value 'FREIGHT_ON_VALUE'
	 * @return string 'FREIGHT_ON_VALUE'
	 */
	const VALUE_FREIGHT_ON_VALUE = 'FREIGHT_ON_VALUE';
	/**
	 * Constant for value 'FUEL'
	 * @return string 'FUEL'
	 */
	const VALUE_FUEL = 'FUEL';
	/**
	 * Constant for value 'HOLD_AT_LOCATION'
	 * @return string 'HOLD_AT_LOCATION'
	 */
	const VALUE_HOLD_AT_LOCATION = 'HOLD_AT_LOCATION';
	/**
	 * Constant for value 'HOME_DELIVERY_APPOINTMENT'
	 * @return string 'HOME_DELIVERY_APPOINTMENT'
	 */
	const VALUE_HOME_DELIVERY_APPOINTMENT = 'HOME_DELIVERY_APPOINTMENT';
	/**
	 * Constant for value 'HOME_DELIVERY_DATE_CERTAIN'
	 * @return string 'HOME_DELIVERY_DATE_CERTAIN'
	 */
	const VALUE_HOME_DELIVERY_DATE_CERTAIN = 'HOME_DELIVERY_DATE_CERTAIN';
	/**
	 * Constant for value 'HOME_DELIVERY_EVENING'
	 * @return string 'HOME_DELIVERY_EVENING'
	 */
	const VALUE_HOME_DELIVERY_EVENING = 'HOME_DELIVERY_EVENING';
	/**
	 * Constant for value 'INSIDE_DELIVERY'
	 * @return string 'INSIDE_DELIVERY'
	 */
	const VALUE_INSIDE_DELIVERY = 'INSIDE_DELIVERY';
	/**
	 * Constant for value 'INSIDE_PICKUP'
	 * @return string 'INSIDE_PICKUP'
	 */
	const VALUE_INSIDE_PICKUP = 'INSIDE_PICKUP';
	/**
	 * Constant for value 'INSURED_VALUE'
	 * @return string 'INSURED_VALUE'
	 */
	const VALUE_INSURED_VALUE = 'INSURED_VALUE';
	/**
	 * Constant for value 'INTERHAWAII'
	 * @return string 'INTERHAWAII'
	 */
	const VALUE_INTERHAWAII = 'INTERHAWAII';
	/**
	 * Constant for value 'LIFTGATE_DELIVERY'
	 * @return string 'LIFTGATE_DELIVERY'
	 */
	const VALUE_LIFTGATE_DELIVERY = 'LIFTGATE_DELIVERY';
	/**
	 * Constant for value 'LIFTGATE_PICKUP'
	 * @return string 'LIFTGATE_PICKUP'
	 */
	const VALUE_LIFTGATE_PICKUP = 'LIFTGATE_PICKUP';
	/**
	 * Constant for value 'LIMITED_ACCESS_DELIVERY'
	 * @return string 'LIMITED_ACCESS_DELIVERY'
	 */
	const VALUE_LIMITED_ACCESS_DELIVERY = 'LIMITED_ACCESS_DELIVERY';
	/**
	 * Constant for value 'LIMITED_ACCESS_PICKUP'
	 * @return string 'LIMITED_ACCESS_PICKUP'
	 */
	const VALUE_LIMITED_ACCESS_PICKUP = 'LIMITED_ACCESS_PICKUP';
	/**
	 * Constant for value 'METRO_DELIVERY'
	 * @return string 'METRO_DELIVERY'
	 */
	const VALUE_METRO_DELIVERY = 'METRO_DELIVERY';
	/**
	 * Constant for value 'METRO_PICKUP'
	 * @return string 'METRO_PICKUP'
	 */
	const VALUE_METRO_PICKUP = 'METRO_PICKUP';
	/**
	 * Constant for value 'NON_MACHINABLE'
	 * @return string 'NON_MACHINABLE'
	 */
	const VALUE_NON_MACHINABLE = 'NON_MACHINABLE';
	/**
	 * Constant for value 'OFFSHORE'
	 * @return string 'OFFSHORE'
	 */
	const VALUE_OFFSHORE = 'OFFSHORE';
	/**
	 * Constant for value 'ON_CALL_PICKUP'
	 * @return string 'ON_CALL_PICKUP'
	 */
	const VALUE_ON_CALL_PICKUP = 'ON_CALL_PICKUP';
	/**
	 * Constant for value 'OTHER'
	 * @return string 'OTHER'
	 */
	const VALUE_OTHER = 'OTHER';
	/**
	 * Constant for value 'OUT_OF_DELIVERY_AREA'
	 * @return string 'OUT_OF_DELIVERY_AREA'
	 */
	const VALUE_OUT_OF_DELIVERY_AREA = 'OUT_OF_DELIVERY_AREA';
	/**
	 * Constant for value 'OUT_OF_PICKUP_AREA'
	 * @return string 'OUT_OF_PICKUP_AREA'
	 */
	const VALUE_OUT_OF_PICKUP_AREA = 'OUT_OF_PICKUP_AREA';
	/**
	 * Constant for value 'OVERSIZE'
	 * @return string 'OVERSIZE'
	 */
	const VALUE_OVERSIZE = 'OVERSIZE';
	/**
	 * Constant for value 'OVER_DIMENSION'
	 * @return string 'OVER_DIMENSION'
	 */
	const VALUE_OVER_DIMENSION = 'OVER_DIMENSION';
	/**
	 * Constant for value 'PIECE_COUNT_VERIFICATION'
	 * @return string 'PIECE_COUNT_VERIFICATION'
	 */
	const VALUE_PIECE_COUNT_VERIFICATION = 'PIECE_COUNT_VERIFICATION';
	/**
	 * Constant for value 'PRE_DELIVERY_NOTIFICATION'
	 * @return string 'PRE_DELIVERY_NOTIFICATION'
	 */
	const VALUE_PRE_DELIVERY_NOTIFICATION = 'PRE_DELIVERY_NOTIFICATION';
	/**
	 * Constant for value 'PRIORITY_ALERT'
	 * @return string 'PRIORITY_ALERT'
	 */
	const VALUE_PRIORITY_ALERT = 'PRIORITY_ALERT';
	/**
	 * Constant for value 'PROTECTION_FROM_FREEZING'
	 * @return string 'PROTECTION_FROM_FREEZING'
	 */
	const VALUE_PROTECTION_FROM_FREEZING = 'PROTECTION_FROM_FREEZING';
	/**
	 * Constant for value 'REGIONAL_MALL_DELIVERY'
	 * @return string 'REGIONAL_MALL_DELIVERY'
	 */
	const VALUE_REGIONAL_MALL_DELIVERY = 'REGIONAL_MALL_DELIVERY';
	/**
	 * Constant for value 'REGIONAL_MALL_PICKUP'
	 * @return string 'REGIONAL_MALL_PICKUP'
	 */
	const VALUE_REGIONAL_MALL_PICKUP = 'REGIONAL_MALL_PICKUP';
	/**
	 * Constant for value 'RESIDENTIAL_DELIVERY'
	 * @return string 'RESIDENTIAL_DELIVERY'
	 */
	const VALUE_RESIDENTIAL_DELIVERY = 'RESIDENTIAL_DELIVERY';
	/**
	 * Constant for value 'RESIDENTIAL_PICKUP'
	 * @return string 'RESIDENTIAL_PICKUP'
	 */
	const VALUE_RESIDENTIAL_PICKUP = 'RESIDENTIAL_PICKUP';
	/**
	 * Constant for value 'RETURN_LABEL'
	 * @return string 'RETURN_LABEL'
	 */
	const VALUE_RETURN_LABEL = 'RETURN_LABEL';
	/**
	 * Constant for value 'SATURDAY_DELIVERY'
	 * @return string 'SATURDAY_DELIVERY'
	 */
	const VALUE_SATURDAY_DELIVERY = 'SATURDAY_DELIVERY';
	/**
	 * Constant for value 'SATURDAY_PICKUP'
	 * @return string 'SATURDAY_PICKUP'
	 */
	const VALUE_SATURDAY_PICKUP = 'SATURDAY_PICKUP';
	/**
	 * Constant for value 'SIGNATURE_OPTION'
	 * @return string 'SIGNATURE_OPTION'
	 */
	const VALUE_SIGNATURE_OPTION = 'SIGNATURE_OPTION';
	/**
	 * Constant for value 'TARP'
	 * @return string 'TARP'
	 */
	const VALUE_TARP = 'TARP';
	/**
	 * Constant for value 'THIRD_PARTY_CONSIGNEE'
	 * @return string 'THIRD_PARTY_CONSIGNEE'
	 */
	const VALUE_THIRD_PARTY_CONSIGNEE = 'THIRD_PARTY_CONSIGNEE';
	/**
	 * Constant for value 'TRANSMART_SERVICE_FEE'
	 * @return string 'TRANSMART_SERVICE_FEE'
	 */
	const VALUE_TRANSMART_SERVICE_FEE = 'TRANSMART_SERVICE_FEE';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumSurchargeType::VALUE_ADDITIONAL_HANDLING
	 * @uses MytestEnumSurchargeType::VALUE_ANCILLARY_FEE
	 * @uses MytestEnumSurchargeType::VALUE_APPOINTMENT_DELIVERY
	 * @uses MytestEnumSurchargeType::VALUE_BROKER_SELECT_OPTION
	 * @uses MytestEnumSurchargeType::VALUE_CANADIAN_DESTINATION
	 * @uses MytestEnumSurchargeType::VALUE_CLEARANCE_ENTRY_FEE
	 * @uses MytestEnumSurchargeType::VALUE_COD
	 * @uses MytestEnumSurchargeType::VALUE_CUT_FLOWERS
	 * @uses MytestEnumSurchargeType::VALUE_DANGEROUS_GOODS
	 * @uses MytestEnumSurchargeType::VALUE_DELIVERY_AREA
	 * @uses MytestEnumSurchargeType::VALUE_DELIVERY_CONFIRMATION
	 * @uses MytestEnumSurchargeType::VALUE_DOCUMENTATION_FEE
	 * @uses MytestEnumSurchargeType::VALUE_DRY_ICE
	 * @uses MytestEnumSurchargeType::VALUE_EMAIL_LABEL
	 * @uses MytestEnumSurchargeType::VALUE_EUROPE_FIRST
	 * @uses MytestEnumSurchargeType::VALUE_EXCESS_VALUE
	 * @uses MytestEnumSurchargeType::VALUE_EXHIBITION
	 * @uses MytestEnumSurchargeType::VALUE_EXPORT
	 * @uses MytestEnumSurchargeType::VALUE_EXTRA_SURFACE_HANDLING_CHARGE
	 * @uses MytestEnumSurchargeType::VALUE_EXTREME_LENGTH
	 * @uses MytestEnumSurchargeType::VALUE_FEDEX_INTRACOUNTRY_FEES
	 * @uses MytestEnumSurchargeType::VALUE_FEDEX_TAG
	 * @uses MytestEnumSurchargeType::VALUE_FICE
	 * @uses MytestEnumSurchargeType::VALUE_FLATBED
	 * @uses MytestEnumSurchargeType::VALUE_FREIGHT_GUARANTEE
	 * @uses MytestEnumSurchargeType::VALUE_FREIGHT_ON_VALUE
	 * @uses MytestEnumSurchargeType::VALUE_FUEL
	 * @uses MytestEnumSurchargeType::VALUE_HOLD_AT_LOCATION
	 * @uses MytestEnumSurchargeType::VALUE_HOME_DELIVERY_APPOINTMENT
	 * @uses MytestEnumSurchargeType::VALUE_HOME_DELIVERY_DATE_CERTAIN
	 * @uses MytestEnumSurchargeType::VALUE_HOME_DELIVERY_EVENING
	 * @uses MytestEnumSurchargeType::VALUE_INSIDE_DELIVERY
	 * @uses MytestEnumSurchargeType::VALUE_INSIDE_PICKUP
	 * @uses MytestEnumSurchargeType::VALUE_INSURED_VALUE
	 * @uses MytestEnumSurchargeType::VALUE_INTERHAWAII
	 * @uses MytestEnumSurchargeType::VALUE_LIFTGATE_DELIVERY
	 * @uses MytestEnumSurchargeType::VALUE_LIFTGATE_PICKUP
	 * @uses MytestEnumSurchargeType::VALUE_LIMITED_ACCESS_DELIVERY
	 * @uses MytestEnumSurchargeType::VALUE_LIMITED_ACCESS_PICKUP
	 * @uses MytestEnumSurchargeType::VALUE_METRO_DELIVERY
	 * @uses MytestEnumSurchargeType::VALUE_METRO_PICKUP
	 * @uses MytestEnumSurchargeType::VALUE_NON_MACHINABLE
	 * @uses MytestEnumSurchargeType::VALUE_OFFSHORE
	 * @uses MytestEnumSurchargeType::VALUE_ON_CALL_PICKUP
	 * @uses MytestEnumSurchargeType::VALUE_OTHER
	 * @uses MytestEnumSurchargeType::VALUE_OUT_OF_DELIVERY_AREA
	 * @uses MytestEnumSurchargeType::VALUE_OUT_OF_PICKUP_AREA
	 * @uses MytestEnumSurchargeType::VALUE_OVERSIZE
	 * @uses MytestEnumSurchargeType::VALUE_OVER_DIMENSION
	 * @uses MytestEnumSurchargeType::VALUE_PIECE_COUNT_VERIFICATION
	 * @uses MytestEnumSurchargeType::VALUE_PRE_DELIVERY_NOTIFICATION
	 * @uses MytestEnumSurchargeType::VALUE_PRIORITY_ALERT
	 * @uses MytestEnumSurchargeType::VALUE_PROTECTION_FROM_FREEZING
	 * @uses MytestEnumSurchargeType::VALUE_REGIONAL_MALL_DELIVERY
	 * @uses MytestEnumSurchargeType::VALUE_REGIONAL_MALL_PICKUP
	 * @uses MytestEnumSurchargeType::VALUE_RESIDENTIAL_DELIVERY
	 * @uses MytestEnumSurchargeType::VALUE_RESIDENTIAL_PICKUP
	 * @uses MytestEnumSurchargeType::VALUE_RETURN_LABEL
	 * @uses MytestEnumSurchargeType::VALUE_SATURDAY_DELIVERY
	 * @uses MytestEnumSurchargeType::VALUE_SATURDAY_PICKUP
	 * @uses MytestEnumSurchargeType::VALUE_SIGNATURE_OPTION
	 * @uses MytestEnumSurchargeType::VALUE_TARP
	 * @uses MytestEnumSurchargeType::VALUE_THIRD_PARTY_CONSIGNEE
	 * @uses MytestEnumSurchargeType::VALUE_TRANSMART_SERVICE_FEE
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumSurchargeType::VALUE_ADDITIONAL_HANDLING,MytestEnumSurchargeType::VALUE_ANCILLARY_FEE,MytestEnumSurchargeType::VALUE_APPOINTMENT_DELIVERY,MytestEnumSurchargeType::VALUE_BROKER_SELECT_OPTION,MytestEnumSurchargeType::VALUE_CANADIAN_DESTINATION,MytestEnumSurchargeType::VALUE_CLEARANCE_ENTRY_FEE,MytestEnumSurchargeType::VALUE_COD,MytestEnumSurchargeType::VALUE_CUT_FLOWERS,MytestEnumSurchargeType::VALUE_DANGEROUS_GOODS,MytestEnumSurchargeType::VALUE_DELIVERY_AREA,MytestEnumSurchargeType::VALUE_DELIVERY_CONFIRMATION,MytestEnumSurchargeType::VALUE_DOCUMENTATION_FEE,MytestEnumSurchargeType::VALUE_DRY_ICE,MytestEnumSurchargeType::VALUE_EMAIL_LABEL,MytestEnumSurchargeType::VALUE_EUROPE_FIRST,MytestEnumSurchargeType::VALUE_EXCESS_VALUE,MytestEnumSurchargeType::VALUE_EXHIBITION,MytestEnumSurchargeType::VALUE_EXPORT,MytestEnumSurchargeType::VALUE_EXTRA_SURFACE_HANDLING_CHARGE,MytestEnumSurchargeType::VALUE_EXTREME_LENGTH,MytestEnumSurchargeType::VALUE_FEDEX_INTRACOUNTRY_FEES,MytestEnumSurchargeType::VALUE_FEDEX_TAG,MytestEnumSurchargeType::VALUE_FICE,MytestEnumSurchargeType::VALUE_FLATBED,MytestEnumSurchargeType::VALUE_FREIGHT_GUARANTEE,MytestEnumSurchargeType::VALUE_FREIGHT_ON_VALUE,MytestEnumSurchargeType::VALUE_FUEL,MytestEnumSurchargeType::VALUE_HOLD_AT_LOCATION,MytestEnumSurchargeType::VALUE_HOME_DELIVERY_APPOINTMENT,MytestEnumSurchargeType::VALUE_HOME_DELIVERY_DATE_CERTAIN,MytestEnumSurchargeType::VALUE_HOME_DELIVERY_EVENING,MytestEnumSurchargeType::VALUE_INSIDE_DELIVERY,MytestEnumSurchargeType::VALUE_INSIDE_PICKUP,MytestEnumSurchargeType::VALUE_INSURED_VALUE,MytestEnumSurchargeType::VALUE_INTERHAWAII,MytestEnumSurchargeType::VALUE_LIFTGATE_DELIVERY,MytestEnumSurchargeType::VALUE_LIFTGATE_PICKUP,MytestEnumSurchargeType::VALUE_LIMITED_ACCESS_DELIVERY,MytestEnumSurchargeType::VALUE_LIMITED_ACCESS_PICKUP,MytestEnumSurchargeType::VALUE_METRO_DELIVERY,MytestEnumSurchargeType::VALUE_METRO_PICKUP,MytestEnumSurchargeType::VALUE_NON_MACHINABLE,MytestEnumSurchargeType::VALUE_OFFSHORE,MytestEnumSurchargeType::VALUE_ON_CALL_PICKUP,MytestEnumSurchargeType::VALUE_OTHER,MytestEnumSurchargeType::VALUE_OUT_OF_DELIVERY_AREA,MytestEnumSurchargeType::VALUE_OUT_OF_PICKUP_AREA,MytestEnumSurchargeType::VALUE_OVERSIZE,MytestEnumSurchargeType::VALUE_OVER_DIMENSION,MytestEnumSurchargeType::VALUE_PIECE_COUNT_VERIFICATION,MytestEnumSurchargeType::VALUE_PRE_DELIVERY_NOTIFICATION,MytestEnumSurchargeType::VALUE_PRIORITY_ALERT,MytestEnumSurchargeType::VALUE_PROTECTION_FROM_FREEZING,MytestEnumSurchargeType::VALUE_REGIONAL_MALL_DELIVERY,MytestEnumSurchargeType::VALUE_REGIONAL_MALL_PICKUP,MytestEnumSurchargeType::VALUE_RESIDENTIAL_DELIVERY,MytestEnumSurchargeType::VALUE_RESIDENTIAL_PICKUP,MytestEnumSurchargeType::VALUE_RETURN_LABEL,MytestEnumSurchargeType::VALUE_SATURDAY_DELIVERY,MytestEnumSurchargeType::VALUE_SATURDAY_PICKUP,MytestEnumSurchargeType::VALUE_SIGNATURE_OPTION,MytestEnumSurchargeType::VALUE_TARP,MytestEnumSurchargeType::VALUE_THIRD_PARTY_CONSIGNEE,MytestEnumSurchargeType::VALUE_TRANSMART_SERVICE_FEE));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>