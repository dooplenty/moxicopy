<?php
/**
 * File for class MytestEnumSurchargeLevelType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumSurchargeLevelType originally named SurchargeLevelType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumSurchargeLevelType extends MytestWsdlClass
{
	/**
	 * Constant for value 'PACKAGE'
	 * @return string 'PACKAGE'
	 */
	const VALUE_PACKAGE = 'PACKAGE';
	/**
	 * Constant for value 'SHIPMENT'
	 * @return string 'SHIPMENT'
	 */
	const VALUE_SHIPMENT = 'SHIPMENT';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumSurchargeLevelType::VALUE_PACKAGE
	 * @uses MytestEnumSurchargeLevelType::VALUE_SHIPMENT
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumSurchargeLevelType::VALUE_PACKAGE,MytestEnumSurchargeLevelType::VALUE_SHIPMENT));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>