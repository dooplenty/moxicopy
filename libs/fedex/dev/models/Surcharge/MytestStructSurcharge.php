<?php
/**
 * File for class MytestStructSurcharge
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructSurcharge originally named Surcharge
 * Documentation : Identifies each surcharge applied to the shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructSurcharge extends MytestWsdlClass
{
	/**
	 * The SurchargeType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumSurchargeType
	 */
	public $SurchargeType;
	/**
	 * The Level
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumSurchargeLevelType
	 */
	public $Level;
	/**
	 * The Description
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Description;
	/**
	 * The Amount
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $Amount;
	/**
	 * Constructor method for Surcharge
	 * @see parent::__construct()
	 * @param MytestEnumSurchargeType $_surchargeType
	 * @param MytestEnumSurchargeLevelType $_level
	 * @param string $_description
	 * @param MytestStructMoney $_amount
	 * @return MytestStructSurcharge
	 */
	public function __construct($_surchargeType = NULL,$_level = NULL,$_description = NULL,$_amount = NULL)
	{
		parent::__construct(array('SurchargeType'=>$_surchargeType,'Level'=>$_level,'Description'=>$_description,'Amount'=>$_amount));
	}
	/**
	 * Get SurchargeType value
	 * @return MytestEnumSurchargeType|null
	 */
	public function getSurchargeType()
	{
		return $this->SurchargeType;
	}
	/**
	 * Set SurchargeType value
	 * @uses MytestEnumSurchargeType::valueIsValid()
	 * @param MytestEnumSurchargeType $_surchargeType the SurchargeType
	 * @return MytestEnumSurchargeType
	 */
	public function setSurchargeType($_surchargeType)
	{
		if(!MytestEnumSurchargeType::valueIsValid($_surchargeType))
		{
			return false;
		}
		return ($this->SurchargeType = $_surchargeType);
	}
	/**
	 * Get Level value
	 * @return MytestEnumSurchargeLevelType|null
	 */
	public function getLevel()
	{
		return $this->Level;
	}
	/**
	 * Set Level value
	 * @uses MytestEnumSurchargeLevelType::valueIsValid()
	 * @param MytestEnumSurchargeLevelType $_level the Level
	 * @return MytestEnumSurchargeLevelType
	 */
	public function setLevel($_level)
	{
		if(!MytestEnumSurchargeLevelType::valueIsValid($_level))
		{
			return false;
		}
		return ($this->Level = $_level);
	}
	/**
	 * Get Description value
	 * @return string|null
	 */
	public function getDescription()
	{
		return $this->Description;
	}
	/**
	 * Set Description value
	 * @param string $_description the Description
	 * @return string
	 */
	public function setDescription($_description)
	{
		return ($this->Description = $_description);
	}
	/**
	 * Get Amount value
	 * @return MytestStructMoney|null
	 */
	public function getAmount()
	{
		return $this->Amount;
	}
	/**
	 * Set Amount value
	 * @param MytestStructMoney $_amount the Amount
	 * @return MytestStructMoney
	 */
	public function setAmount($_amount)
	{
		return ($this->Amount = $_amount);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructSurcharge
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>