<?php
/**
 * File for class MytestStructVersionId
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructVersionId originally named VersionId
 * Documentation : Identifies the version/level of a service operation expected by a caller (in each request) and performed by the callee (in each reply).
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructVersionId extends MytestWsdlClass
{
	/**
	 * The ServiceId
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies a system or sub-system which performs an operation.
	 * - fixed : crs
	 * - minOccurs : 1
	 * @var string
	 */
	public $ServiceId;
	/**
	 * The Major
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the service business level.
	 * - fixed : 13
	 * - minOccurs : 1
	 * @var int
	 */
	public $Major;
	/**
	 * The Intermediate
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the service interface level.
	 * - fixed : 0
	 * - minOccurs : 1
	 * @var int
	 */
	public $Intermediate;
	/**
	 * The Minor
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the service code level.
	 * - fixed : 0
	 * - minOccurs : 1
	 * @var int
	 */
	public $Minor;
	/**
	 * Constructor method for VersionId
	 * @see parent::__construct()
	 * @param string $_serviceId
	 * @param int $_major
	 * @param int $_intermediate
	 * @param int $_minor
	 * @return MytestStructVersionId
	 */
	public function __construct($_serviceId,$_major,$_intermediate,$_minor)
	{
		parent::__construct(array('ServiceId'=>$_serviceId,'Major'=>$_major,'Intermediate'=>$_intermediate,'Minor'=>$_minor));
	}
	/**
	 * Get ServiceId value
	 * @return string
	 */
	public function getServiceId()
	{
		return $this->ServiceId;
	}
	/**
	 * Set ServiceId value
	 * @param string $_serviceId the ServiceId
	 * @return string
	 */
	public function setServiceId($_serviceId)
	{
		return ($this->ServiceId = $_serviceId);
	}
	/**
	 * Get Major value
	 * @return int
	 */
	public function getMajor()
	{
		return $this->Major;
	}
	/**
	 * Set Major value
	 * @param int $_major the Major
	 * @return int
	 */
	public function setMajor($_major)
	{
		return ($this->Major = $_major);
	}
	/**
	 * Get Intermediate value
	 * @return int
	 */
	public function getIntermediate()
	{
		return $this->Intermediate;
	}
	/**
	 * Set Intermediate value
	 * @param int $_intermediate the Intermediate
	 * @return int
	 */
	public function setIntermediate($_intermediate)
	{
		return ($this->Intermediate = $_intermediate);
	}
	/**
	 * Get Minor value
	 * @return int
	 */
	public function getMinor()
	{
		return $this->Minor;
	}
	/**
	 * Set Minor value
	 * @param int $_minor the Minor
	 * @return int
	 */
	public function setMinor($_minor)
	{
		return ($this->Minor = $_minor);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructVersionId
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>