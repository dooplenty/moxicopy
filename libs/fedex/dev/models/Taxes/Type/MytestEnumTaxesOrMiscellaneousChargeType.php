<?php
/**
 * File for class MytestEnumTaxesOrMiscellaneousChargeType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumTaxesOrMiscellaneousChargeType originally named TaxesOrMiscellaneousChargeType
 * Documentation : Specifice the kind of tax or miscellaneous charge being reported on a Commercial Invoice.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumTaxesOrMiscellaneousChargeType extends MytestWsdlClass
{
	/**
	 * Constant for value 'COMMISSIONS'
	 * @return string 'COMMISSIONS'
	 */
	const VALUE_COMMISSIONS = 'COMMISSIONS';
	/**
	 * Constant for value 'DISCOUNTS'
	 * @return string 'DISCOUNTS'
	 */
	const VALUE_DISCOUNTS = 'DISCOUNTS';
	/**
	 * Constant for value 'HANDLING_FEES'
	 * @return string 'HANDLING_FEES'
	 */
	const VALUE_HANDLING_FEES = 'HANDLING_FEES';
	/**
	 * Constant for value 'OTHER'
	 * @return string 'OTHER'
	 */
	const VALUE_OTHER = 'OTHER';
	/**
	 * Constant for value 'ROYALTIES_AND_LICENSE_FEES'
	 * @return string 'ROYALTIES_AND_LICENSE_FEES'
	 */
	const VALUE_ROYALTIES_AND_LICENSE_FEES = 'ROYALTIES_AND_LICENSE_FEES';
	/**
	 * Constant for value 'TAXES'
	 * @return string 'TAXES'
	 */
	const VALUE_TAXES = 'TAXES';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumTaxesOrMiscellaneousChargeType::VALUE_COMMISSIONS
	 * @uses MytestEnumTaxesOrMiscellaneousChargeType::VALUE_DISCOUNTS
	 * @uses MytestEnumTaxesOrMiscellaneousChargeType::VALUE_HANDLING_FEES
	 * @uses MytestEnumTaxesOrMiscellaneousChargeType::VALUE_OTHER
	 * @uses MytestEnumTaxesOrMiscellaneousChargeType::VALUE_ROYALTIES_AND_LICENSE_FEES
	 * @uses MytestEnumTaxesOrMiscellaneousChargeType::VALUE_TAXES
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumTaxesOrMiscellaneousChargeType::VALUE_COMMISSIONS,MytestEnumTaxesOrMiscellaneousChargeType::VALUE_DISCOUNTS,MytestEnumTaxesOrMiscellaneousChargeType::VALUE_HANDLING_FEES,MytestEnumTaxesOrMiscellaneousChargeType::VALUE_OTHER,MytestEnumTaxesOrMiscellaneousChargeType::VALUE_ROYALTIES_AND_LICENSE_FEES,MytestEnumTaxesOrMiscellaneousChargeType::VALUE_TAXES));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>