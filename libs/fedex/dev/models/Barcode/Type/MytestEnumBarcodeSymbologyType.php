<?php
/**
 * File for class MytestEnumBarcodeSymbologyType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumBarcodeSymbologyType originally named BarcodeSymbologyType
 * Documentation : Identification of the type of barcode (symbology) used on FedEx documents and labels.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumBarcodeSymbologyType extends MytestWsdlClass
{
	/**
	 * Constant for value 'CODABAR'
	 * @return string 'CODABAR'
	 */
	const VALUE_CODABAR = 'CODABAR';
	/**
	 * Constant for value 'CODE128'
	 * @return string 'CODE128'
	 */
	const VALUE_CODE128 = 'CODE128';
	/**
	 * Constant for value 'CODE128B'
	 * @return string 'CODE128B'
	 */
	const VALUE_CODE128B = 'CODE128B';
	/**
	 * Constant for value 'CODE128C'
	 * @return string 'CODE128C'
	 */
	const VALUE_CODE128C = 'CODE128C';
	/**
	 * Constant for value 'CODE39'
	 * @return string 'CODE39'
	 */
	const VALUE_CODE39 = 'CODE39';
	/**
	 * Constant for value 'CODE93'
	 * @return string 'CODE93'
	 */
	const VALUE_CODE93 = 'CODE93';
	/**
	 * Constant for value 'I2OF5'
	 * @return string 'I2OF5'
	 */
	const VALUE_I2OF5 = 'I2OF5';
	/**
	 * Constant for value 'MANUAL'
	 * @return string 'MANUAL'
	 */
	const VALUE_MANUAL = 'MANUAL';
	/**
	 * Constant for value 'PDF417'
	 * @return string 'PDF417'
	 */
	const VALUE_PDF417 = 'PDF417';
	/**
	 * Constant for value 'POSTNET'
	 * @return string 'POSTNET'
	 */
	const VALUE_POSTNET = 'POSTNET';
	/**
	 * Constant for value 'UCC128'
	 * @return string 'UCC128'
	 */
	const VALUE_UCC128 = 'UCC128';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumBarcodeSymbologyType::VALUE_CODABAR
	 * @uses MytestEnumBarcodeSymbologyType::VALUE_CODE128
	 * @uses MytestEnumBarcodeSymbologyType::VALUE_CODE128B
	 * @uses MytestEnumBarcodeSymbologyType::VALUE_CODE128C
	 * @uses MytestEnumBarcodeSymbologyType::VALUE_CODE39
	 * @uses MytestEnumBarcodeSymbologyType::VALUE_CODE93
	 * @uses MytestEnumBarcodeSymbologyType::VALUE_I2OF5
	 * @uses MytestEnumBarcodeSymbologyType::VALUE_MANUAL
	 * @uses MytestEnumBarcodeSymbologyType::VALUE_PDF417
	 * @uses MytestEnumBarcodeSymbologyType::VALUE_POSTNET
	 * @uses MytestEnumBarcodeSymbologyType::VALUE_UCC128
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumBarcodeSymbologyType::VALUE_CODABAR,MytestEnumBarcodeSymbologyType::VALUE_CODE128,MytestEnumBarcodeSymbologyType::VALUE_CODE128B,MytestEnumBarcodeSymbologyType::VALUE_CODE128C,MytestEnumBarcodeSymbologyType::VALUE_CODE39,MytestEnumBarcodeSymbologyType::VALUE_CODE93,MytestEnumBarcodeSymbologyType::VALUE_I2OF5,MytestEnumBarcodeSymbologyType::VALUE_MANUAL,MytestEnumBarcodeSymbologyType::VALUE_PDF417,MytestEnumBarcodeSymbologyType::VALUE_POSTNET,MytestEnumBarcodeSymbologyType::VALUE_UCC128));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>