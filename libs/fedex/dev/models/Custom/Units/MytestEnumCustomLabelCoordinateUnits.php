<?php
/**
 * File for class MytestEnumCustomLabelCoordinateUnits
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumCustomLabelCoordinateUnits originally named CustomLabelCoordinateUnits
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumCustomLabelCoordinateUnits extends MytestWsdlClass
{
	/**
	 * Constant for value 'MILS'
	 * @return string 'MILS'
	 */
	const VALUE_MILS = 'MILS';
	/**
	 * Constant for value 'PIXELS'
	 * @return string 'PIXELS'
	 */
	const VALUE_PIXELS = 'PIXELS';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumCustomLabelCoordinateUnits::VALUE_MILS
	 * @uses MytestEnumCustomLabelCoordinateUnits::VALUE_PIXELS
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumCustomLabelCoordinateUnits::VALUE_MILS,MytestEnumCustomLabelCoordinateUnits::VALUE_PIXELS));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>