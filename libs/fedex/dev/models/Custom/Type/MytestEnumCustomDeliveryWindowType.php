<?php
/**
 * File for class MytestEnumCustomDeliveryWindowType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumCustomDeliveryWindowType originally named CustomDeliveryWindowType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumCustomDeliveryWindowType extends MytestWsdlClass
{
	/**
	 * Constant for value 'AFTER'
	 * @return string 'AFTER'
	 */
	const VALUE_AFTER = 'AFTER';
	/**
	 * Constant for value 'BEFORE'
	 * @return string 'BEFORE'
	 */
	const VALUE_BEFORE = 'BEFORE';
	/**
	 * Constant for value 'BETWEEN'
	 * @return string 'BETWEEN'
	 */
	const VALUE_BETWEEN = 'BETWEEN';
	/**
	 * Constant for value 'ON'
	 * @return string 'ON'
	 */
	const VALUE_ON = 'ON';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumCustomDeliveryWindowType::VALUE_AFTER
	 * @uses MytestEnumCustomDeliveryWindowType::VALUE_BEFORE
	 * @uses MytestEnumCustomDeliveryWindowType::VALUE_BETWEEN
	 * @uses MytestEnumCustomDeliveryWindowType::VALUE_ON
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumCustomDeliveryWindowType::VALUE_AFTER,MytestEnumCustomDeliveryWindowType::VALUE_BEFORE,MytestEnumCustomDeliveryWindowType::VALUE_BETWEEN,MytestEnumCustomDeliveryWindowType::VALUE_ON));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>