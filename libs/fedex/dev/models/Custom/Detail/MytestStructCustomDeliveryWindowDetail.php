<?php
/**
 * File for class MytestStructCustomDeliveryWindowDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCustomDeliveryWindowDetail originally named CustomDeliveryWindowDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCustomDeliveryWindowDetail extends MytestWsdlClass
{
	/**
	 * The Type
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates the type of custom delivery being requested.
	 * - minOccurs : 0
	 * @var MytestEnumCustomDeliveryWindowType
	 */
	public $Type;
	/**
	 * The RequestTime
	 * Meta informations extracted from the WSDL
	 * - documentation : Time by which delivery is requested.
	 * - minOccurs : 0
	 * @var time
	 */
	public $RequestTime;
	/**
	 * The RequestRange
	 * Meta informations extracted from the WSDL
	 * - documentation : Range of dates for custom delivery request; only used if type is BETWEEN.
	 * - minOccurs : 0
	 * @var MytestStructDateRange
	 */
	public $RequestRange;
	/**
	 * The RequestDate
	 * Meta informations extracted from the WSDL
	 * - documentation : Date for custom delivery request; only used for types of ON, BETWEEN, or AFTER.
	 * - minOccurs : 0
	 * @var date
	 */
	public $RequestDate;
	/**
	 * Constructor method for CustomDeliveryWindowDetail
	 * @see parent::__construct()
	 * @param MytestEnumCustomDeliveryWindowType $_type
	 * @param time $_requestTime
	 * @param MytestStructDateRange $_requestRange
	 * @param date $_requestDate
	 * @return MytestStructCustomDeliveryWindowDetail
	 */
	public function __construct($_type = NULL,$_requestTime = NULL,$_requestRange = NULL,$_requestDate = NULL)
	{
		parent::__construct(array('Type'=>$_type,'RequestTime'=>$_requestTime,'RequestRange'=>$_requestRange,'RequestDate'=>$_requestDate));
	}
	/**
	 * Get Type value
	 * @return MytestEnumCustomDeliveryWindowType|null
	 */
	public function getType()
	{
		return $this->Type;
	}
	/**
	 * Set Type value
	 * @uses MytestEnumCustomDeliveryWindowType::valueIsValid()
	 * @param MytestEnumCustomDeliveryWindowType $_type the Type
	 * @return MytestEnumCustomDeliveryWindowType
	 */
	public function setType($_type)
	{
		if(!MytestEnumCustomDeliveryWindowType::valueIsValid($_type))
		{
			return false;
		}
		return ($this->Type = $_type);
	}
	/**
	 * Get RequestTime value
	 * @return time|null
	 */
	public function getRequestTime()
	{
		return $this->RequestTime;
	}
	/**
	 * Set RequestTime value
	 * @param time $_requestTime the RequestTime
	 * @return time
	 */
	public function setRequestTime($_requestTime)
	{
		return ($this->RequestTime = $_requestTime);
	}
	/**
	 * Get RequestRange value
	 * @return MytestStructDateRange|null
	 */
	public function getRequestRange()
	{
		return $this->RequestRange;
	}
	/**
	 * Set RequestRange value
	 * @param MytestStructDateRange $_requestRange the RequestRange
	 * @return MytestStructDateRange
	 */
	public function setRequestRange($_requestRange)
	{
		return ($this->RequestRange = $_requestRange);
	}
	/**
	 * Get RequestDate value
	 * @return date|null
	 */
	public function getRequestDate()
	{
		return $this->RequestDate;
	}
	/**
	 * Set RequestDate value
	 * @param date $_requestDate the RequestDate
	 * @return date
	 */
	public function setRequestDate($_requestDate)
	{
		return ($this->RequestDate = $_requestDate);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCustomDeliveryWindowDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>