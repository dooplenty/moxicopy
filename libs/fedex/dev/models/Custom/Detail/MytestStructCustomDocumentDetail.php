<?php
/**
 * File for class MytestStructCustomDocumentDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCustomDocumentDetail originally named CustomDocumentDetail
 * Documentation : Data required to produce a custom-specified document, either at shipment or package level.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCustomDocumentDetail extends MytestWsdlClass
{
	/**
	 * The Format
	 * Meta informations extracted from the WSDL
	 * - documentation : Common information controlling document production.
	 * - minOccurs : 0
	 * @var MytestStructShippingDocumentFormat
	 */
	public $Format;
	/**
	 * The LabelPrintingOrientation
	 * Meta informations extracted from the WSDL
	 * - documentation : Applicable only to documents produced on thermal printers with roll stock.
	 * - minOccurs : 0
	 * @var MytestEnumLabelPrintingOrientationType
	 */
	public $LabelPrintingOrientation;
	/**
	 * The LabelRotation
	 * Meta informations extracted from the WSDL
	 * - documentation : Applicable only to documents produced on thermal printers with roll stock.
	 * - minOccurs : 0
	 * @var MytestEnumLabelRotationType
	 */
	public $LabelRotation;
	/**
	 * The SpecificationId
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the formatting specification used to construct this custom document.
	 * - minOccurs : 0
	 * @var string
	 */
	public $SpecificationId;
	/**
	 * Constructor method for CustomDocumentDetail
	 * @see parent::__construct()
	 * @param MytestStructShippingDocumentFormat $_format
	 * @param MytestEnumLabelPrintingOrientationType $_labelPrintingOrientation
	 * @param MytestEnumLabelRotationType $_labelRotation
	 * @param string $_specificationId
	 * @return MytestStructCustomDocumentDetail
	 */
	public function __construct($_format = NULL,$_labelPrintingOrientation = NULL,$_labelRotation = NULL,$_specificationId = NULL)
	{
		parent::__construct(array('Format'=>$_format,'LabelPrintingOrientation'=>$_labelPrintingOrientation,'LabelRotation'=>$_labelRotation,'SpecificationId'=>$_specificationId));
	}
	/**
	 * Get Format value
	 * @return MytestStructShippingDocumentFormat|null
	 */
	public function getFormat()
	{
		return $this->Format;
	}
	/**
	 * Set Format value
	 * @param MytestStructShippingDocumentFormat $_format the Format
	 * @return MytestStructShippingDocumentFormat
	 */
	public function setFormat($_format)
	{
		return ($this->Format = $_format);
	}
	/**
	 * Get LabelPrintingOrientation value
	 * @return MytestEnumLabelPrintingOrientationType|null
	 */
	public function getLabelPrintingOrientation()
	{
		return $this->LabelPrintingOrientation;
	}
	/**
	 * Set LabelPrintingOrientation value
	 * @uses MytestEnumLabelPrintingOrientationType::valueIsValid()
	 * @param MytestEnumLabelPrintingOrientationType $_labelPrintingOrientation the LabelPrintingOrientation
	 * @return MytestEnumLabelPrintingOrientationType
	 */
	public function setLabelPrintingOrientation($_labelPrintingOrientation)
	{
		if(!MytestEnumLabelPrintingOrientationType::valueIsValid($_labelPrintingOrientation))
		{
			return false;
		}
		return ($this->LabelPrintingOrientation = $_labelPrintingOrientation);
	}
	/**
	 * Get LabelRotation value
	 * @return MytestEnumLabelRotationType|null
	 */
	public function getLabelRotation()
	{
		return $this->LabelRotation;
	}
	/**
	 * Set LabelRotation value
	 * @uses MytestEnumLabelRotationType::valueIsValid()
	 * @param MytestEnumLabelRotationType $_labelRotation the LabelRotation
	 * @return MytestEnumLabelRotationType
	 */
	public function setLabelRotation($_labelRotation)
	{
		if(!MytestEnumLabelRotationType::valueIsValid($_labelRotation))
		{
			return false;
		}
		return ($this->LabelRotation = $_labelRotation);
	}
	/**
	 * Get SpecificationId value
	 * @return string|null
	 */
	public function getSpecificationId()
	{
		return $this->SpecificationId;
	}
	/**
	 * Set SpecificationId value
	 * @param string $_specificationId the SpecificationId
	 * @return string
	 */
	public function setSpecificationId($_specificationId)
	{
		return ($this->SpecificationId = $_specificationId);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCustomDocumentDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>