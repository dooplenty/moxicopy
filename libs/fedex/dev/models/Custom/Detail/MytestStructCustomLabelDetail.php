<?php
/**
 * File for class MytestStructCustomLabelDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCustomLabelDetail originally named CustomLabelDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCustomLabelDetail extends MytestWsdlClass
{
	/**
	 * The CoordinateUnits
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumCustomLabelCoordinateUnits
	 */
	public $CoordinateUnits;
	/**
	 * The TextEntries
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructCustomLabelTextEntry
	 */
	public $TextEntries;
	/**
	 * The GraphicEntries
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructCustomLabelGraphicEntry
	 */
	public $GraphicEntries;
	/**
	 * The BoxEntries
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructCustomLabelBoxEntry
	 */
	public $BoxEntries;
	/**
	 * The BarcodeEntries
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructCustomLabelBarcodeEntry
	 */
	public $BarcodeEntries;
	/**
	 * Constructor method for CustomLabelDetail
	 * @see parent::__construct()
	 * @param MytestEnumCustomLabelCoordinateUnits $_coordinateUnits
	 * @param MytestStructCustomLabelTextEntry $_textEntries
	 * @param MytestStructCustomLabelGraphicEntry $_graphicEntries
	 * @param MytestStructCustomLabelBoxEntry $_boxEntries
	 * @param MytestStructCustomLabelBarcodeEntry $_barcodeEntries
	 * @return MytestStructCustomLabelDetail
	 */
	public function __construct($_coordinateUnits = NULL,$_textEntries = NULL,$_graphicEntries = NULL,$_boxEntries = NULL,$_barcodeEntries = NULL)
	{
		parent::__construct(array('CoordinateUnits'=>$_coordinateUnits,'TextEntries'=>$_textEntries,'GraphicEntries'=>$_graphicEntries,'BoxEntries'=>$_boxEntries,'BarcodeEntries'=>$_barcodeEntries));
	}
	/**
	 * Get CoordinateUnits value
	 * @return MytestEnumCustomLabelCoordinateUnits|null
	 */
	public function getCoordinateUnits()
	{
		return $this->CoordinateUnits;
	}
	/**
	 * Set CoordinateUnits value
	 * @uses MytestEnumCustomLabelCoordinateUnits::valueIsValid()
	 * @param MytestEnumCustomLabelCoordinateUnits $_coordinateUnits the CoordinateUnits
	 * @return MytestEnumCustomLabelCoordinateUnits
	 */
	public function setCoordinateUnits($_coordinateUnits)
	{
		if(!MytestEnumCustomLabelCoordinateUnits::valueIsValid($_coordinateUnits))
		{
			return false;
		}
		return ($this->CoordinateUnits = $_coordinateUnits);
	}
	/**
	 * Get TextEntries value
	 * @return MytestStructCustomLabelTextEntry|null
	 */
	public function getTextEntries()
	{
		return $this->TextEntries;
	}
	/**
	 * Set TextEntries value
	 * @param MytestStructCustomLabelTextEntry $_textEntries the TextEntries
	 * @return MytestStructCustomLabelTextEntry
	 */
	public function setTextEntries($_textEntries)
	{
		return ($this->TextEntries = $_textEntries);
	}
	/**
	 * Get GraphicEntries value
	 * @return MytestStructCustomLabelGraphicEntry|null
	 */
	public function getGraphicEntries()
	{
		return $this->GraphicEntries;
	}
	/**
	 * Set GraphicEntries value
	 * @param MytestStructCustomLabelGraphicEntry $_graphicEntries the GraphicEntries
	 * @return MytestStructCustomLabelGraphicEntry
	 */
	public function setGraphicEntries($_graphicEntries)
	{
		return ($this->GraphicEntries = $_graphicEntries);
	}
	/**
	 * Get BoxEntries value
	 * @return MytestStructCustomLabelBoxEntry|null
	 */
	public function getBoxEntries()
	{
		return $this->BoxEntries;
	}
	/**
	 * Set BoxEntries value
	 * @param MytestStructCustomLabelBoxEntry $_boxEntries the BoxEntries
	 * @return MytestStructCustomLabelBoxEntry
	 */
	public function setBoxEntries($_boxEntries)
	{
		return ($this->BoxEntries = $_boxEntries);
	}
	/**
	 * Get BarcodeEntries value
	 * @return MytestStructCustomLabelBarcodeEntry|null
	 */
	public function getBarcodeEntries()
	{
		return $this->BarcodeEntries;
	}
	/**
	 * Set BarcodeEntries value
	 * @param MytestStructCustomLabelBarcodeEntry $_barcodeEntries the BarcodeEntries
	 * @return MytestStructCustomLabelBarcodeEntry
	 */
	public function setBarcodeEntries($_barcodeEntries)
	{
		return ($this->BarcodeEntries = $_barcodeEntries);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCustomLabelDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>