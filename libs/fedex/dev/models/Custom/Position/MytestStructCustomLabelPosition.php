<?php
/**
 * File for class MytestStructCustomLabelPosition
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCustomLabelPosition originally named CustomLabelPosition
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCustomLabelPosition extends MytestWsdlClass
{
	/**
	 * The X
	 * Meta informations extracted from the WSDL
	 * - documentation : Horizontal position, relative to left edge of custom area.
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $X;
	/**
	 * The Y
	 * Meta informations extracted from the WSDL
	 * - documentation : Vertical position, relative to top edge of custom area.
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $Y;
	/**
	 * Constructor method for CustomLabelPosition
	 * @see parent::__construct()
	 * @param nonNegativeInteger $_x
	 * @param nonNegativeInteger $_y
	 * @return MytestStructCustomLabelPosition
	 */
	public function __construct($_x = NULL,$_y = NULL)
	{
		parent::__construct(array('X'=>$_x,'Y'=>$_y));
	}
	/**
	 * Get X value
	 * @return nonNegativeInteger|null
	 */
	public function getX()
	{
		return $this->X;
	}
	/**
	 * Set X value
	 * @param nonNegativeInteger $_x the X
	 * @return nonNegativeInteger
	 */
	public function setX($_x)
	{
		return ($this->X = $_x);
	}
	/**
	 * Get Y value
	 * @return nonNegativeInteger|null
	 */
	public function getY()
	{
		return $this->Y;
	}
	/**
	 * Set Y value
	 * @param nonNegativeInteger $_y the Y
	 * @return nonNegativeInteger
	 */
	public function setY($_y)
	{
		return ($this->Y = $_y);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCustomLabelPosition
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>