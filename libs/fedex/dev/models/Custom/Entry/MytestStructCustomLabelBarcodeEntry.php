<?php
/**
 * File for class MytestStructCustomLabelBarcodeEntry
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCustomLabelBarcodeEntry originally named CustomLabelBarcodeEntry
 * Documentation : Constructed string, based on format and zero or more data fields, printed in specified barcode symbology.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCustomLabelBarcodeEntry extends MytestWsdlClass
{
	/**
	 * The Position
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructCustomLabelPosition
	 */
	public $Position;
	/**
	 * The Format
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Format;
	/**
	 * The DataFields
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var string
	 */
	public $DataFields;
	/**
	 * The BarHeight
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var int
	 */
	public $BarHeight;
	/**
	 * The ThinBarWidth
	 * Meta informations extracted from the WSDL
	 * - documentation : Width of thinnest bar/space element in the barcode.
	 * - minOccurs : 0
	 * @var int
	 */
	public $ThinBarWidth;
	/**
	 * The BarcodeSymbology
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumBarcodeSymbologyType
	 */
	public $BarcodeSymbology;
	/**
	 * Constructor method for CustomLabelBarcodeEntry
	 * @see parent::__construct()
	 * @param MytestStructCustomLabelPosition $_position
	 * @param string $_format
	 * @param string $_dataFields
	 * @param int $_barHeight
	 * @param int $_thinBarWidth
	 * @param MytestEnumBarcodeSymbologyType $_barcodeSymbology
	 * @return MytestStructCustomLabelBarcodeEntry
	 */
	public function __construct($_position = NULL,$_format = NULL,$_dataFields = NULL,$_barHeight = NULL,$_thinBarWidth = NULL,$_barcodeSymbology = NULL)
	{
		parent::__construct(array('Position'=>$_position,'Format'=>$_format,'DataFields'=>$_dataFields,'BarHeight'=>$_barHeight,'ThinBarWidth'=>$_thinBarWidth,'BarcodeSymbology'=>$_barcodeSymbology));
	}
	/**
	 * Get Position value
	 * @return MytestStructCustomLabelPosition|null
	 */
	public function getPosition()
	{
		return $this->Position;
	}
	/**
	 * Set Position value
	 * @param MytestStructCustomLabelPosition $_position the Position
	 * @return MytestStructCustomLabelPosition
	 */
	public function setPosition($_position)
	{
		return ($this->Position = $_position);
	}
	/**
	 * Get Format value
	 * @return string|null
	 */
	public function getFormat()
	{
		return $this->Format;
	}
	/**
	 * Set Format value
	 * @param string $_format the Format
	 * @return string
	 */
	public function setFormat($_format)
	{
		return ($this->Format = $_format);
	}
	/**
	 * Get DataFields value
	 * @return string|null
	 */
	public function getDataFields()
	{
		return $this->DataFields;
	}
	/**
	 * Set DataFields value
	 * @param string $_dataFields the DataFields
	 * @return string
	 */
	public function setDataFields($_dataFields)
	{
		return ($this->DataFields = $_dataFields);
	}
	/**
	 * Get BarHeight value
	 * @return int|null
	 */
	public function getBarHeight()
	{
		return $this->BarHeight;
	}
	/**
	 * Set BarHeight value
	 * @param int $_barHeight the BarHeight
	 * @return int
	 */
	public function setBarHeight($_barHeight)
	{
		return ($this->BarHeight = $_barHeight);
	}
	/**
	 * Get ThinBarWidth value
	 * @return int|null
	 */
	public function getThinBarWidth()
	{
		return $this->ThinBarWidth;
	}
	/**
	 * Set ThinBarWidth value
	 * @param int $_thinBarWidth the ThinBarWidth
	 * @return int
	 */
	public function setThinBarWidth($_thinBarWidth)
	{
		return ($this->ThinBarWidth = $_thinBarWidth);
	}
	/**
	 * Get BarcodeSymbology value
	 * @return MytestEnumBarcodeSymbologyType|null
	 */
	public function getBarcodeSymbology()
	{
		return $this->BarcodeSymbology;
	}
	/**
	 * Set BarcodeSymbology value
	 * @uses MytestEnumBarcodeSymbologyType::valueIsValid()
	 * @param MytestEnumBarcodeSymbologyType $_barcodeSymbology the BarcodeSymbology
	 * @return MytestEnumBarcodeSymbologyType
	 */
	public function setBarcodeSymbology($_barcodeSymbology)
	{
		if(!MytestEnumBarcodeSymbologyType::valueIsValid($_barcodeSymbology))
		{
			return false;
		}
		return ($this->BarcodeSymbology = $_barcodeSymbology);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCustomLabelBarcodeEntry
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>