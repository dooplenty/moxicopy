<?php
/**
 * File for class MytestStructCustomLabelBoxEntry
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCustomLabelBoxEntry originally named CustomLabelBoxEntry
 * Documentation : Solid (filled) rectangular area on label.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCustomLabelBoxEntry extends MytestWsdlClass
{
	/**
	 * The TopLeftCorner
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 1
	 * @var MytestStructCustomLabelPosition
	 */
	public $TopLeftCorner;
	/**
	 * The BottomRightCorner
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 1
	 * @var MytestStructCustomLabelPosition
	 */
	public $BottomRightCorner;
	/**
	 * Constructor method for CustomLabelBoxEntry
	 * @see parent::__construct()
	 * @param MytestStructCustomLabelPosition $_topLeftCorner
	 * @param MytestStructCustomLabelPosition $_bottomRightCorner
	 * @return MytestStructCustomLabelBoxEntry
	 */
	public function __construct($_topLeftCorner,$_bottomRightCorner)
	{
		parent::__construct(array('TopLeftCorner'=>$_topLeftCorner,'BottomRightCorner'=>$_bottomRightCorner));
	}
	/**
	 * Get TopLeftCorner value
	 * @return MytestStructCustomLabelPosition
	 */
	public function getTopLeftCorner()
	{
		return $this->TopLeftCorner;
	}
	/**
	 * Set TopLeftCorner value
	 * @param MytestStructCustomLabelPosition $_topLeftCorner the TopLeftCorner
	 * @return MytestStructCustomLabelPosition
	 */
	public function setTopLeftCorner($_topLeftCorner)
	{
		return ($this->TopLeftCorner = $_topLeftCorner);
	}
	/**
	 * Get BottomRightCorner value
	 * @return MytestStructCustomLabelPosition
	 */
	public function getBottomRightCorner()
	{
		return $this->BottomRightCorner;
	}
	/**
	 * Set BottomRightCorner value
	 * @param MytestStructCustomLabelPosition $_bottomRightCorner the BottomRightCorner
	 * @return MytestStructCustomLabelPosition
	 */
	public function setBottomRightCorner($_bottomRightCorner)
	{
		return ($this->BottomRightCorner = $_bottomRightCorner);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCustomLabelBoxEntry
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>