<?php
/**
 * File for class MytestStructCustomLabelGraphicEntry
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCustomLabelGraphicEntry originally named CustomLabelGraphicEntry
 * Documentation : Image to be included from printer's memory, or from a local file for offline clients.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCustomLabelGraphicEntry extends MytestWsdlClass
{
	/**
	 * The Position
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructCustomLabelPosition
	 */
	public $Position;
	/**
	 * The PrinterGraphicId
	 * Meta informations extracted from the WSDL
	 * - documentation : Printer-specific index of graphic image to be printed.
	 * - minOccurs : 0
	 * @var string
	 */
	public $PrinterGraphicId;
	/**
	 * The FileGraphicFullName
	 * Meta informations extracted from the WSDL
	 * - documentation : Fully-qualified path and file name for graphic image to be printed.
	 * - minOccurs : 0
	 * @var string
	 */
	public $FileGraphicFullName;
	/**
	 * Constructor method for CustomLabelGraphicEntry
	 * @see parent::__construct()
	 * @param MytestStructCustomLabelPosition $_position
	 * @param string $_printerGraphicId
	 * @param string $_fileGraphicFullName
	 * @return MytestStructCustomLabelGraphicEntry
	 */
	public function __construct($_position = NULL,$_printerGraphicId = NULL,$_fileGraphicFullName = NULL)
	{
		parent::__construct(array('Position'=>$_position,'PrinterGraphicId'=>$_printerGraphicId,'FileGraphicFullName'=>$_fileGraphicFullName));
	}
	/**
	 * Get Position value
	 * @return MytestStructCustomLabelPosition|null
	 */
	public function getPosition()
	{
		return $this->Position;
	}
	/**
	 * Set Position value
	 * @param MytestStructCustomLabelPosition $_position the Position
	 * @return MytestStructCustomLabelPosition
	 */
	public function setPosition($_position)
	{
		return ($this->Position = $_position);
	}
	/**
	 * Get PrinterGraphicId value
	 * @return string|null
	 */
	public function getPrinterGraphicId()
	{
		return $this->PrinterGraphicId;
	}
	/**
	 * Set PrinterGraphicId value
	 * @param string $_printerGraphicId the PrinterGraphicId
	 * @return string
	 */
	public function setPrinterGraphicId($_printerGraphicId)
	{
		return ($this->PrinterGraphicId = $_printerGraphicId);
	}
	/**
	 * Get FileGraphicFullName value
	 * @return string|null
	 */
	public function getFileGraphicFullName()
	{
		return $this->FileGraphicFullName;
	}
	/**
	 * Set FileGraphicFullName value
	 * @param string $_fileGraphicFullName the FileGraphicFullName
	 * @return string
	 */
	public function setFileGraphicFullName($_fileGraphicFullName)
	{
		return ($this->FileGraphicFullName = $_fileGraphicFullName);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCustomLabelGraphicEntry
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>