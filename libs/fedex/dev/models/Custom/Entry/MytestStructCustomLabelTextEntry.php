<?php
/**
 * File for class MytestStructCustomLabelTextEntry
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCustomLabelTextEntry originally named CustomLabelTextEntry
 * Documentation : Constructed string, based on format and zero or more data fields, printed in specified printer font (for thermal labels) or generic font/size (for plain paper labels).
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCustomLabelTextEntry extends MytestWsdlClass
{
	/**
	 * The Position
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructCustomLabelPosition
	 */
	public $Position;
	/**
	 * The Format
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Format;
	/**
	 * The DataFields
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var string
	 */
	public $DataFields;
	/**
	 * The ThermalFontId
	 * Meta informations extracted from the WSDL
	 * - documentation : Printer-specific font name for use with thermal printer labels.
	 * - minOccurs : 0
	 * @var string
	 */
	public $ThermalFontId;
	/**
	 * The FontName
	 * Meta informations extracted from the WSDL
	 * - documentation : Generic font name for use with plain paper labels.
	 * - minOccurs : 0
	 * @var string
	 */
	public $FontName;
	/**
	 * The FontSize
	 * Meta informations extracted from the WSDL
	 * - documentation : Generic font size for use with plain paper labels.
	 * - minOccurs : 0
	 * @var positiveInteger
	 */
	public $FontSize;
	/**
	 * Constructor method for CustomLabelTextEntry
	 * @see parent::__construct()
	 * @param MytestStructCustomLabelPosition $_position
	 * @param string $_format
	 * @param string $_dataFields
	 * @param string $_thermalFontId
	 * @param string $_fontName
	 * @param positiveInteger $_fontSize
	 * @return MytestStructCustomLabelTextEntry
	 */
	public function __construct($_position = NULL,$_format = NULL,$_dataFields = NULL,$_thermalFontId = NULL,$_fontName = NULL,$_fontSize = NULL)
	{
		parent::__construct(array('Position'=>$_position,'Format'=>$_format,'DataFields'=>$_dataFields,'ThermalFontId'=>$_thermalFontId,'FontName'=>$_fontName,'FontSize'=>$_fontSize));
	}
	/**
	 * Get Position value
	 * @return MytestStructCustomLabelPosition|null
	 */
	public function getPosition()
	{
		return $this->Position;
	}
	/**
	 * Set Position value
	 * @param MytestStructCustomLabelPosition $_position the Position
	 * @return MytestStructCustomLabelPosition
	 */
	public function setPosition($_position)
	{
		return ($this->Position = $_position);
	}
	/**
	 * Get Format value
	 * @return string|null
	 */
	public function getFormat()
	{
		return $this->Format;
	}
	/**
	 * Set Format value
	 * @param string $_format the Format
	 * @return string
	 */
	public function setFormat($_format)
	{
		return ($this->Format = $_format);
	}
	/**
	 * Get DataFields value
	 * @return string|null
	 */
	public function getDataFields()
	{
		return $this->DataFields;
	}
	/**
	 * Set DataFields value
	 * @param string $_dataFields the DataFields
	 * @return string
	 */
	public function setDataFields($_dataFields)
	{
		return ($this->DataFields = $_dataFields);
	}
	/**
	 * Get ThermalFontId value
	 * @return string|null
	 */
	public function getThermalFontId()
	{
		return $this->ThermalFontId;
	}
	/**
	 * Set ThermalFontId value
	 * @param string $_thermalFontId the ThermalFontId
	 * @return string
	 */
	public function setThermalFontId($_thermalFontId)
	{
		return ($this->ThermalFontId = $_thermalFontId);
	}
	/**
	 * Get FontName value
	 * @return string|null
	 */
	public function getFontName()
	{
		return $this->FontName;
	}
	/**
	 * Set FontName value
	 * @param string $_fontName the FontName
	 * @return string
	 */
	public function setFontName($_fontName)
	{
		return ($this->FontName = $_fontName);
	}
	/**
	 * Get FontSize value
	 * @return positiveInteger|null
	 */
	public function getFontSize()
	{
		return $this->FontSize;
	}
	/**
	 * Set FontSize value
	 * @param positiveInteger $_fontSize the FontSize
	 * @return positiveInteger
	 */
	public function setFontSize($_fontSize)
	{
		return ($this->FontSize = $_fontSize);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCustomLabelTextEntry
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>