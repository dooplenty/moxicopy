<?php
/**
 * File for class MytestStructDimensions
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructDimensions originally named Dimensions
 * Documentation : The dimensions of this package and the unit type used for the measurements.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructDimensions extends MytestWsdlClass
{
	/**
	 * The Length
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 1
	 * @var nonNegativeInteger
	 */
	public $Length;
	/**
	 * The Width
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 1
	 * @var nonNegativeInteger
	 */
	public $Width;
	/**
	 * The Height
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 1
	 * @var nonNegativeInteger
	 */
	public $Height;
	/**
	 * The Units
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 1
	 * @var MytestEnumLinearUnits
	 */
	public $Units;
	/**
	 * Constructor method for Dimensions
	 * @see parent::__construct()
	 * @param nonNegativeInteger $_length
	 * @param nonNegativeInteger $_width
	 * @param nonNegativeInteger $_height
	 * @param MytestEnumLinearUnits $_units
	 * @return MytestStructDimensions
	 */
	public function __construct($_length,$_width,$_height,$_units)
	{
		parent::__construct(array('Length'=>$_length,'Width'=>$_width,'Height'=>$_height,'Units'=>$_units));
	}
	/**
	 * Get Length value
	 * @return nonNegativeInteger
	 */
	public function getLength()
	{
		return $this->Length;
	}
	/**
	 * Set Length value
	 * @param nonNegativeInteger $_length the Length
	 * @return nonNegativeInteger
	 */
	public function setLength($_length)
	{
		return ($this->Length = $_length);
	}
	/**
	 * Get Width value
	 * @return nonNegativeInteger
	 */
	public function getWidth()
	{
		return $this->Width;
	}
	/**
	 * Set Width value
	 * @param nonNegativeInteger $_width the Width
	 * @return nonNegativeInteger
	 */
	public function setWidth($_width)
	{
		return ($this->Width = $_width);
	}
	/**
	 * Get Height value
	 * @return nonNegativeInteger
	 */
	public function getHeight()
	{
		return $this->Height;
	}
	/**
	 * Set Height value
	 * @param nonNegativeInteger $_height the Height
	 * @return nonNegativeInteger
	 */
	public function setHeight($_height)
	{
		return ($this->Height = $_height);
	}
	/**
	 * Get Units value
	 * @return MytestEnumLinearUnits
	 */
	public function getUnits()
	{
		return $this->Units;
	}
	/**
	 * Set Units value
	 * @uses MytestEnumLinearUnits::valueIsValid()
	 * @param MytestEnumLinearUnits $_units the Units
	 * @return MytestEnumLinearUnits
	 */
	public function setUnits($_units)
	{
		if(!MytestEnumLinearUnits::valueIsValid($_units))
		{
			return false;
		}
		return ($this->Units = $_units);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructDimensions
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>