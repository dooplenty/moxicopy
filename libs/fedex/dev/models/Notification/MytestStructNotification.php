<?php
/**
 * File for class MytestStructNotification
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructNotification originally named Notification
 * Documentation : The descriptive data regarding the result of the submitted transaction.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructNotification extends MytestWsdlClass
{
	/**
	 * The Severity
	 * Meta informations extracted from the WSDL
	 * - documentation : The severity of this notification. This can indicate success or failure or some other information about the request. The values that can be returned are SUCCESS - Your transaction succeeded with no other applicable information. NOTE - Additional information that may be of interest to you about your transaction. WARNING - Additional information that you need to know about your transaction that you may need to take action on. ERROR - Information about an error that occurred while processing your transaction. FAILURE - FedEx was unable to process your transaction at this time due to a system failure. Please try again later
	 * - minOccurs : 0
	 * @var MytestEnumNotificationSeverityType
	 */
	public $Severity;
	/**
	 * The Source
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates the source of this notification. Combined with the Code it uniquely identifies this notification
	 * - minOccurs : 0
	 * @var string
	 */
	public $Source;
	/**
	 * The Code
	 * Meta informations extracted from the WSDL
	 * - documentation : A code that represents this notification. Combined with the Source it uniquely identifies this notification.
	 * - minOccurs : 0
	 * @var string
	 */
	public $Code;
	/**
	 * The Message
	 * Meta informations extracted from the WSDL
	 * - documentation : Human-readable text that explains this notification.
	 * - minOccurs : 0
	 * @var string
	 */
	public $Message;
	/**
	 * The LocalizedMessage
	 * Meta informations extracted from the WSDL
	 * - documentation : The translated message. The language and locale specified in the ClientDetail. Localization are used to determine the representation. Currently only supported in a TrackReply.
	 * - minOccurs : 0
	 * @var string
	 */
	public $LocalizedMessage;
	/**
	 * The MessageParameters
	 * Meta informations extracted from the WSDL
	 * - documentation : A collection of name/value pairs that provide specific data to help the client determine the nature of an error (or warning, etc.) witout having to parse the message string.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructNotificationParameter
	 */
	public $MessageParameters;
	/**
	 * Constructor method for Notification
	 * @see parent::__construct()
	 * @param MytestEnumNotificationSeverityType $_severity
	 * @param string $_source
	 * @param string $_code
	 * @param string $_message
	 * @param string $_localizedMessage
	 * @param MytestStructNotificationParameter $_messageParameters
	 * @return MytestStructNotification
	 */
	public function __construct($_severity = NULL,$_source = NULL,$_code = NULL,$_message = NULL,$_localizedMessage = NULL,$_messageParameters = NULL)
	{
		parent::__construct(array('Severity'=>$_severity,'Source'=>$_source,'Code'=>$_code,'Message'=>$_message,'LocalizedMessage'=>$_localizedMessage,'MessageParameters'=>$_messageParameters));
	}
	/**
	 * Get Severity value
	 * @return MytestEnumNotificationSeverityType|null
	 */
	public function getSeverity()
	{
		return $this->Severity;
	}
	/**
	 * Set Severity value
	 * @uses MytestEnumNotificationSeverityType::valueIsValid()
	 * @param MytestEnumNotificationSeverityType $_severity the Severity
	 * @return MytestEnumNotificationSeverityType
	 */
	public function setSeverity($_severity)
	{
		if(!MytestEnumNotificationSeverityType::valueIsValid($_severity))
		{
			return false;
		}
		return ($this->Severity = $_severity);
	}
	/**
	 * Get Source value
	 * @return string|null
	 */
	public function getSource()
	{
		return $this->Source;
	}
	/**
	 * Set Source value
	 * @param string $_source the Source
	 * @return string
	 */
	public function setSource($_source)
	{
		return ($this->Source = $_source);
	}
	/**
	 * Get Code value
	 * @return string|null
	 */
	public function getCode()
	{
		return $this->Code;
	}
	/**
	 * Set Code value
	 * @param string $_code the Code
	 * @return string
	 */
	public function setCode($_code)
	{
		return ($this->Code = $_code);
	}
	/**
	 * Get Message value
	 * @return string|null
	 */
	public function getMessage()
	{
		return $this->Message;
	}
	/**
	 * Set Message value
	 * @param string $_message the Message
	 * @return string
	 */
	public function setMessage($_message)
	{
		return ($this->Message = $_message);
	}
	/**
	 * Get LocalizedMessage value
	 * @return string|null
	 */
	public function getLocalizedMessage()
	{
		return $this->LocalizedMessage;
	}
	/**
	 * Set LocalizedMessage value
	 * @param string $_localizedMessage the LocalizedMessage
	 * @return string
	 */
	public function setLocalizedMessage($_localizedMessage)
	{
		return ($this->LocalizedMessage = $_localizedMessage);
	}
	/**
	 * Get MessageParameters value
	 * @return MytestStructNotificationParameter|null
	 */
	public function getMessageParameters()
	{
		return $this->MessageParameters;
	}
	/**
	 * Set MessageParameters value
	 * @param MytestStructNotificationParameter $_messageParameters the MessageParameters
	 * @return MytestStructNotificationParameter
	 */
	public function setMessageParameters($_messageParameters)
	{
		return ($this->MessageParameters = $_messageParameters);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructNotification
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>