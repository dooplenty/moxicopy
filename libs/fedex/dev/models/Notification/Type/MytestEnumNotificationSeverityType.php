<?php
/**
 * File for class MytestEnumNotificationSeverityType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumNotificationSeverityType originally named NotificationSeverityType
 * Documentation : Identifies the set of severity values for a Notification.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumNotificationSeverityType extends MytestWsdlClass
{
	/**
	 * Constant for value 'ERROR'
	 * @return string 'ERROR'
	 */
	const VALUE_ERROR = 'ERROR';
	/**
	 * Constant for value 'FAILURE'
	 * @return string 'FAILURE'
	 */
	const VALUE_FAILURE = 'FAILURE';
	/**
	 * Constant for value 'NOTE'
	 * @return string 'NOTE'
	 */
	const VALUE_NOTE = 'NOTE';
	/**
	 * Constant for value 'SUCCESS'
	 * @return string 'SUCCESS'
	 */
	const VALUE_SUCCESS = 'SUCCESS';
	/**
	 * Constant for value 'WARNING'
	 * @return string 'WARNING'
	 */
	const VALUE_WARNING = 'WARNING';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumNotificationSeverityType::VALUE_ERROR
	 * @uses MytestEnumNotificationSeverityType::VALUE_FAILURE
	 * @uses MytestEnumNotificationSeverityType::VALUE_NOTE
	 * @uses MytestEnumNotificationSeverityType::VALUE_SUCCESS
	 * @uses MytestEnumNotificationSeverityType::VALUE_WARNING
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumNotificationSeverityType::VALUE_ERROR,MytestEnumNotificationSeverityType::VALUE_FAILURE,MytestEnumNotificationSeverityType::VALUE_NOTE,MytestEnumNotificationSeverityType::VALUE_SUCCESS,MytestEnumNotificationSeverityType::VALUE_WARNING));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>