<?php
/**
 * File for class MytestStructNotificationParameter
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructNotificationParameter originally named NotificationParameter
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructNotificationParameter extends MytestWsdlClass
{
	/**
	 * The Id
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the type of data contained in Value (e.g. SERVICE_TYPE, PACKAGE_SEQUENCE, etc..).
	 * - minOccurs : 0
	 * @var string
	 */
	public $Id;
	/**
	 * The Value
	 * Meta informations extracted from the WSDL
	 * - documentation : The value of the parameter (e.g. PRIORITY_OVERNIGHT, 2, etc..).
	 * - minOccurs : 0
	 * @var string
	 */
	public $Value;
	/**
	 * Constructor method for NotificationParameter
	 * @see parent::__construct()
	 * @param string $_id
	 * @param string $_value
	 * @return MytestStructNotificationParameter
	 */
	public function __construct($_id = NULL,$_value = NULL)
	{
		parent::__construct(array('Id'=>$_id,'Value'=>$_value));
	}
	/**
	 * Get Id value
	 * @return string|null
	 */
	public function getId()
	{
		return $this->Id;
	}
	/**
	 * Set Id value
	 * @param string $_id the Id
	 * @return string
	 */
	public function setId($_id)
	{
		return ($this->Id = $_id);
	}
	/**
	 * Get Value value
	 * @return string|null
	 */
	public function getValue()
	{
		return $this->Value;
	}
	/**
	 * Set Value value
	 * @param string $_value the Value
	 * @return string
	 */
	public function setValue($_value)
	{
		return ($this->Value = $_value);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructNotificationParameter
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>