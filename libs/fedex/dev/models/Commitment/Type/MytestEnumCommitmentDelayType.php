<?php
/**
 * File for class MytestEnumCommitmentDelayType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumCommitmentDelayType originally named CommitmentDelayType
 * Documentation : The type of delay this shipment will encounter.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumCommitmentDelayType extends MytestWsdlClass
{
	/**
	 * Constant for value 'HOLIDAY'
	 * @return string 'HOLIDAY'
	 */
	const VALUE_HOLIDAY = 'HOLIDAY';
	/**
	 * Constant for value 'NON_WORKDAY'
	 * @return string 'NON_WORKDAY'
	 */
	const VALUE_NON_WORKDAY = 'NON_WORKDAY';
	/**
	 * Constant for value 'NO_CITY_DELIVERY'
	 * @return string 'NO_CITY_DELIVERY'
	 */
	const VALUE_NO_CITY_DELIVERY = 'NO_CITY_DELIVERY';
	/**
	 * Constant for value 'NO_HOLD_AT_LOCATION'
	 * @return string 'NO_HOLD_AT_LOCATION'
	 */
	const VALUE_NO_HOLD_AT_LOCATION = 'NO_HOLD_AT_LOCATION';
	/**
	 * Constant for value 'NO_LOCATION_DELIVERY'
	 * @return string 'NO_LOCATION_DELIVERY'
	 */
	const VALUE_NO_LOCATION_DELIVERY = 'NO_LOCATION_DELIVERY';
	/**
	 * Constant for value 'NO_SERVICE_AREA_DELIVERY'
	 * @return string 'NO_SERVICE_AREA_DELIVERY'
	 */
	const VALUE_NO_SERVICE_AREA_DELIVERY = 'NO_SERVICE_AREA_DELIVERY';
	/**
	 * Constant for value 'NO_SERVICE_AREA_SPECIAL_SERVICE_DELIVERY'
	 * @return string 'NO_SERVICE_AREA_SPECIAL_SERVICE_DELIVERY'
	 */
	const VALUE_NO_SERVICE_AREA_SPECIAL_SERVICE_DELIVERY = 'NO_SERVICE_AREA_SPECIAL_SERVICE_DELIVERY';
	/**
	 * Constant for value 'NO_SPECIAL_SERVICE_DELIVERY'
	 * @return string 'NO_SPECIAL_SERVICE_DELIVERY'
	 */
	const VALUE_NO_SPECIAL_SERVICE_DELIVERY = 'NO_SPECIAL_SERVICE_DELIVERY';
	/**
	 * Constant for value 'NO_ZIP_DELIVERY'
	 * @return string 'NO_ZIP_DELIVERY'
	 */
	const VALUE_NO_ZIP_DELIVERY = 'NO_ZIP_DELIVERY';
	/**
	 * Constant for value 'WEEKEND'
	 * @return string 'WEEKEND'
	 */
	const VALUE_WEEKEND = 'WEEKEND';
	/**
	 * Constant for value 'WEEKEND_SPECIAL'
	 * @return string 'WEEKEND_SPECIAL'
	 */
	const VALUE_WEEKEND_SPECIAL = 'WEEKEND_SPECIAL';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumCommitmentDelayType::VALUE_HOLIDAY
	 * @uses MytestEnumCommitmentDelayType::VALUE_NON_WORKDAY
	 * @uses MytestEnumCommitmentDelayType::VALUE_NO_CITY_DELIVERY
	 * @uses MytestEnumCommitmentDelayType::VALUE_NO_HOLD_AT_LOCATION
	 * @uses MytestEnumCommitmentDelayType::VALUE_NO_LOCATION_DELIVERY
	 * @uses MytestEnumCommitmentDelayType::VALUE_NO_SERVICE_AREA_DELIVERY
	 * @uses MytestEnumCommitmentDelayType::VALUE_NO_SERVICE_AREA_SPECIAL_SERVICE_DELIVERY
	 * @uses MytestEnumCommitmentDelayType::VALUE_NO_SPECIAL_SERVICE_DELIVERY
	 * @uses MytestEnumCommitmentDelayType::VALUE_NO_ZIP_DELIVERY
	 * @uses MytestEnumCommitmentDelayType::VALUE_WEEKEND
	 * @uses MytestEnumCommitmentDelayType::VALUE_WEEKEND_SPECIAL
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumCommitmentDelayType::VALUE_HOLIDAY,MytestEnumCommitmentDelayType::VALUE_NON_WORKDAY,MytestEnumCommitmentDelayType::VALUE_NO_CITY_DELIVERY,MytestEnumCommitmentDelayType::VALUE_NO_HOLD_AT_LOCATION,MytestEnumCommitmentDelayType::VALUE_NO_LOCATION_DELIVERY,MytestEnumCommitmentDelayType::VALUE_NO_SERVICE_AREA_DELIVERY,MytestEnumCommitmentDelayType::VALUE_NO_SERVICE_AREA_SPECIAL_SERVICE_DELIVERY,MytestEnumCommitmentDelayType::VALUE_NO_SPECIAL_SERVICE_DELIVERY,MytestEnumCommitmentDelayType::VALUE_NO_ZIP_DELIVERY,MytestEnumCommitmentDelayType::VALUE_WEEKEND,MytestEnumCommitmentDelayType::VALUE_WEEKEND_SPECIAL));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>