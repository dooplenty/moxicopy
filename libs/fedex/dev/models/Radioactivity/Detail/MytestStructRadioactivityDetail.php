<?php
/**
 * File for class MytestStructRadioactivityDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructRadioactivityDetail originally named RadioactivityDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructRadioactivityDetail extends MytestWsdlClass
{
	/**
	 * The TransportIndex
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var decimal
	 */
	public $TransportIndex;
	/**
	 * The SurfaceReading
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var decimal
	 */
	public $SurfaceReading;
	/**
	 * The CriticalitySafetyIndex
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var decimal
	 */
	public $CriticalitySafetyIndex;
	/**
	 * The Dimensions
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructDimensions
	 */
	public $Dimensions;
	/**
	 * Constructor method for RadioactivityDetail
	 * @see parent::__construct()
	 * @param decimal $_transportIndex
	 * @param decimal $_surfaceReading
	 * @param decimal $_criticalitySafetyIndex
	 * @param MytestStructDimensions $_dimensions
	 * @return MytestStructRadioactivityDetail
	 */
	public function __construct($_transportIndex = NULL,$_surfaceReading = NULL,$_criticalitySafetyIndex = NULL,$_dimensions = NULL)
	{
		parent::__construct(array('TransportIndex'=>$_transportIndex,'SurfaceReading'=>$_surfaceReading,'CriticalitySafetyIndex'=>$_criticalitySafetyIndex,'Dimensions'=>$_dimensions));
	}
	/**
	 * Get TransportIndex value
	 * @return decimal|null
	 */
	public function getTransportIndex()
	{
		return $this->TransportIndex;
	}
	/**
	 * Set TransportIndex value
	 * @param decimal $_transportIndex the TransportIndex
	 * @return decimal
	 */
	public function setTransportIndex($_transportIndex)
	{
		return ($this->TransportIndex = $_transportIndex);
	}
	/**
	 * Get SurfaceReading value
	 * @return decimal|null
	 */
	public function getSurfaceReading()
	{
		return $this->SurfaceReading;
	}
	/**
	 * Set SurfaceReading value
	 * @param decimal $_surfaceReading the SurfaceReading
	 * @return decimal
	 */
	public function setSurfaceReading($_surfaceReading)
	{
		return ($this->SurfaceReading = $_surfaceReading);
	}
	/**
	 * Get CriticalitySafetyIndex value
	 * @return decimal|null
	 */
	public function getCriticalitySafetyIndex()
	{
		return $this->CriticalitySafetyIndex;
	}
	/**
	 * Set CriticalitySafetyIndex value
	 * @param decimal $_criticalitySafetyIndex the CriticalitySafetyIndex
	 * @return decimal
	 */
	public function setCriticalitySafetyIndex($_criticalitySafetyIndex)
	{
		return ($this->CriticalitySafetyIndex = $_criticalitySafetyIndex);
	}
	/**
	 * Get Dimensions value
	 * @return MytestStructDimensions|null
	 */
	public function getDimensions()
	{
		return $this->Dimensions;
	}
	/**
	 * Set Dimensions value
	 * @param MytestStructDimensions $_dimensions the Dimensions
	 * @return MytestStructDimensions
	 */
	public function setDimensions($_dimensions)
	{
		return ($this->Dimensions = $_dimensions);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructRadioactivityDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>