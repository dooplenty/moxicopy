<?php
/**
 * File for class MytestEnumRadioactivityUnitOfMeasure
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumRadioactivityUnitOfMeasure originally named RadioactivityUnitOfMeasure
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumRadioactivityUnitOfMeasure extends MytestWsdlClass
{
	/**
	 * Constant for value 'BQ'
	 * @return string 'BQ'
	 */
	const VALUE_BQ = 'BQ';
	/**
	 * Constant for value 'GBQ'
	 * @return string 'GBQ'
	 */
	const VALUE_GBQ = 'GBQ';
	/**
	 * Constant for value 'KBQ'
	 * @return string 'KBQ'
	 */
	const VALUE_KBQ = 'KBQ';
	/**
	 * Constant for value 'MBQ'
	 * @return string 'MBQ'
	 */
	const VALUE_MBQ = 'MBQ';
	/**
	 * Constant for value 'PBQ'
	 * @return string 'PBQ'
	 */
	const VALUE_PBQ = 'PBQ';
	/**
	 * Constant for value 'TBQ'
	 * @return string 'TBQ'
	 */
	const VALUE_TBQ = 'TBQ';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumRadioactivityUnitOfMeasure::VALUE_BQ
	 * @uses MytestEnumRadioactivityUnitOfMeasure::VALUE_GBQ
	 * @uses MytestEnumRadioactivityUnitOfMeasure::VALUE_KBQ
	 * @uses MytestEnumRadioactivityUnitOfMeasure::VALUE_MBQ
	 * @uses MytestEnumRadioactivityUnitOfMeasure::VALUE_PBQ
	 * @uses MytestEnumRadioactivityUnitOfMeasure::VALUE_TBQ
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumRadioactivityUnitOfMeasure::VALUE_BQ,MytestEnumRadioactivityUnitOfMeasure::VALUE_GBQ,MytestEnumRadioactivityUnitOfMeasure::VALUE_KBQ,MytestEnumRadioactivityUnitOfMeasure::VALUE_MBQ,MytestEnumRadioactivityUnitOfMeasure::VALUE_PBQ,MytestEnumRadioactivityUnitOfMeasure::VALUE_TBQ));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>