<?php
/**
 * File for class MytestEnumEMailNotificationRecipientType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumEMailNotificationRecipientType originally named EMailNotificationRecipientType
 * Documentation : Identifies the set of valid email notification recipient types. For SHIPPER, RECIPIENT and BROKER the email address asssociated with their definitions will be used, any email address sent with the email notification for these three email notification recipient types will be ignored.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumEMailNotificationRecipientType extends MytestWsdlClass
{
	/**
	 * Constant for value 'BROKER'
	 * @return string 'BROKER'
	 */
	const VALUE_BROKER = 'BROKER';
	/**
	 * Constant for value 'OTHER'
	 * @return string 'OTHER'
	 */
	const VALUE_OTHER = 'OTHER';
	/**
	 * Constant for value 'RECIPIENT'
	 * @return string 'RECIPIENT'
	 */
	const VALUE_RECIPIENT = 'RECIPIENT';
	/**
	 * Constant for value 'SHIPPER'
	 * @return string 'SHIPPER'
	 */
	const VALUE_SHIPPER = 'SHIPPER';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumEMailNotificationRecipientType::VALUE_BROKER
	 * @uses MytestEnumEMailNotificationRecipientType::VALUE_OTHER
	 * @uses MytestEnumEMailNotificationRecipientType::VALUE_RECIPIENT
	 * @uses MytestEnumEMailNotificationRecipientType::VALUE_SHIPPER
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumEMailNotificationRecipientType::VALUE_BROKER,MytestEnumEMailNotificationRecipientType::VALUE_OTHER,MytestEnumEMailNotificationRecipientType::VALUE_RECIPIENT,MytestEnumEMailNotificationRecipientType::VALUE_SHIPPER));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>