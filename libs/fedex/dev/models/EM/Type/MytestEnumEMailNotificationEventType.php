<?php
/**
 * File for class MytestEnumEMailNotificationEventType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumEMailNotificationEventType originally named EMailNotificationEventType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumEMailNotificationEventType extends MytestWsdlClass
{
	/**
	 * Constant for value 'ON_DELIVERY'
	 * @return string 'ON_DELIVERY'
	 */
	const VALUE_ON_DELIVERY = 'ON_DELIVERY';
	/**
	 * Constant for value 'ON_EXCEPTION'
	 * @return string 'ON_EXCEPTION'
	 */
	const VALUE_ON_EXCEPTION = 'ON_EXCEPTION';
	/**
	 * Constant for value 'ON_SHIPMENT'
	 * @return string 'ON_SHIPMENT'
	 */
	const VALUE_ON_SHIPMENT = 'ON_SHIPMENT';
	/**
	 * Constant for value 'ON_TENDER'
	 * @return string 'ON_TENDER'
	 */
	const VALUE_ON_TENDER = 'ON_TENDER';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumEMailNotificationEventType::VALUE_ON_DELIVERY
	 * @uses MytestEnumEMailNotificationEventType::VALUE_ON_EXCEPTION
	 * @uses MytestEnumEMailNotificationEventType::VALUE_ON_SHIPMENT
	 * @uses MytestEnumEMailNotificationEventType::VALUE_ON_TENDER
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumEMailNotificationEventType::VALUE_ON_DELIVERY,MytestEnumEMailNotificationEventType::VALUE_ON_EXCEPTION,MytestEnumEMailNotificationEventType::VALUE_ON_SHIPMENT,MytestEnumEMailNotificationEventType::VALUE_ON_TENDER));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>