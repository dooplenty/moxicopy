<?php
/**
 * File for class MytestEnumEMailNotificationFormatType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumEMailNotificationFormatType originally named EMailNotificationFormatType
 * Documentation : The format of the email
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumEMailNotificationFormatType extends MytestWsdlClass
{
	/**
	 * Constant for value 'HTML'
	 * @return string 'HTML'
	 */
	const VALUE_HTML = 'HTML';
	/**
	 * Constant for value 'TEXT'
	 * @return string 'TEXT'
	 */
	const VALUE_TEXT = 'TEXT';
	/**
	 * Constant for value 'WIRELESS'
	 * @return string 'WIRELESS'
	 */
	const VALUE_WIRELESS = 'WIRELESS';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumEMailNotificationFormatType::VALUE_HTML
	 * @uses MytestEnumEMailNotificationFormatType::VALUE_TEXT
	 * @uses MytestEnumEMailNotificationFormatType::VALUE_WIRELESS
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumEMailNotificationFormatType::VALUE_HTML,MytestEnumEMailNotificationFormatType::VALUE_TEXT,MytestEnumEMailNotificationFormatType::VALUE_WIRELESS));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>