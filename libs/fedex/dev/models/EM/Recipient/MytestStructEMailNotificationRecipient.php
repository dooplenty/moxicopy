<?php
/**
 * File for class MytestStructEMailNotificationRecipient
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructEMailNotificationRecipient originally named EMailNotificationRecipient
 * Documentation : The descriptive data for a FedEx email notification recipient.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructEMailNotificationRecipient extends MytestWsdlClass
{
	/**
	 * The EMailNotificationRecipientType
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the relationship this email recipient has to the shipment.
	 * - minOccurs : 1
	 * @var MytestEnumEMailNotificationRecipientType
	 */
	public $EMailNotificationRecipientType;
	/**
	 * The EMailAddress
	 * Meta informations extracted from the WSDL
	 * - documentation : The email address to send the notification to
	 * - minOccurs : 1
	 * @var string
	 */
	public $EMailAddress;
	/**
	 * The NotificationEventsRequested
	 * Meta informations extracted from the WSDL
	 * - documentation : The types of email notifications being requested for this recipient.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumEMailNotificationEventType
	 */
	public $NotificationEventsRequested;
	/**
	 * The Format
	 * Meta informations extracted from the WSDL
	 * - documentation : The format of the email notification.
	 * - minOccurs : 0
	 * @var MytestEnumEMailNotificationFormatType
	 */
	public $Format;
	/**
	 * The Localization
	 * Meta informations extracted from the WSDL
	 * - documentation : The language/locale to be used in this email notification.
	 * - minOccurs : 0
	 * @var MytestStructLocalization
	 */
	public $Localization;
	/**
	 * Constructor method for EMailNotificationRecipient
	 * @see parent::__construct()
	 * @param MytestEnumEMailNotificationRecipientType $_eMailNotificationRecipientType
	 * @param string $_eMailAddress
	 * @param MytestEnumEMailNotificationEventType $_notificationEventsRequested
	 * @param MytestEnumEMailNotificationFormatType $_format
	 * @param MytestStructLocalization $_localization
	 * @return MytestStructEMailNotificationRecipient
	 */
	public function __construct($_eMailNotificationRecipientType,$_eMailAddress,$_notificationEventsRequested = NULL,$_format = NULL,$_localization = NULL)
	{
		parent::__construct(array('EMailNotificationRecipientType'=>$_eMailNotificationRecipientType,'EMailAddress'=>$_eMailAddress,'NotificationEventsRequested'=>$_notificationEventsRequested,'Format'=>$_format,'Localization'=>$_localization));
	}
	/**
	 * Get EMailNotificationRecipientType value
	 * @return MytestEnumEMailNotificationRecipientType
	 */
	public function getEMailNotificationRecipientType()
	{
		return $this->EMailNotificationRecipientType;
	}
	/**
	 * Set EMailNotificationRecipientType value
	 * @uses MytestEnumEMailNotificationRecipientType::valueIsValid()
	 * @param MytestEnumEMailNotificationRecipientType $_eMailNotificationRecipientType the EMailNotificationRecipientType
	 * @return MytestEnumEMailNotificationRecipientType
	 */
	public function setEMailNotificationRecipientType($_eMailNotificationRecipientType)
	{
		if(!MytestEnumEMailNotificationRecipientType::valueIsValid($_eMailNotificationRecipientType))
		{
			return false;
		}
		return ($this->EMailNotificationRecipientType = $_eMailNotificationRecipientType);
	}
	/**
	 * Get EMailAddress value
	 * @return string
	 */
	public function getEMailAddress()
	{
		return $this->EMailAddress;
	}
	/**
	 * Set EMailAddress value
	 * @param string $_eMailAddress the EMailAddress
	 * @return string
	 */
	public function setEMailAddress($_eMailAddress)
	{
		return ($this->EMailAddress = $_eMailAddress);
	}
	/**
	 * Get NotificationEventsRequested value
	 * @return MytestEnumEMailNotificationEventType|null
	 */
	public function getNotificationEventsRequested()
	{
		return $this->NotificationEventsRequested;
	}
	/**
	 * Set NotificationEventsRequested value
	 * @uses MytestEnumEMailNotificationEventType::valueIsValid()
	 * @param MytestEnumEMailNotificationEventType $_notificationEventsRequested the NotificationEventsRequested
	 * @return MytestEnumEMailNotificationEventType
	 */
	public function setNotificationEventsRequested($_notificationEventsRequested)
	{
		if(!MytestEnumEMailNotificationEventType::valueIsValid($_notificationEventsRequested))
		{
			return false;
		}
		return ($this->NotificationEventsRequested = $_notificationEventsRequested);
	}
	/**
	 * Get Format value
	 * @return MytestEnumEMailNotificationFormatType|null
	 */
	public function getFormat()
	{
		return $this->Format;
	}
	/**
	 * Set Format value
	 * @uses MytestEnumEMailNotificationFormatType::valueIsValid()
	 * @param MytestEnumEMailNotificationFormatType $_format the Format
	 * @return MytestEnumEMailNotificationFormatType
	 */
	public function setFormat($_format)
	{
		if(!MytestEnumEMailNotificationFormatType::valueIsValid($_format))
		{
			return false;
		}
		return ($this->Format = $_format);
	}
	/**
	 * Get Localization value
	 * @return MytestStructLocalization|null
	 */
	public function getLocalization()
	{
		return $this->Localization;
	}
	/**
	 * Set Localization value
	 * @param MytestStructLocalization $_localization the Localization
	 * @return MytestStructLocalization
	 */
	public function setLocalization($_localization)
	{
		return ($this->Localization = $_localization);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructEMailNotificationRecipient
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>