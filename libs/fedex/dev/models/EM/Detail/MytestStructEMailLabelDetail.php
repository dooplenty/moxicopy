<?php
/**
 * File for class MytestStructEMailLabelDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructEMailLabelDetail originally named EMailLabelDetail
 * Documentation : Specific information about the delivery of the email and options for the shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructEMailLabelDetail extends MytestWsdlClass
{
	/**
	 * The NotificationEMailAddress
	 * Meta informations extracted from the WSDL
	 * - documentation : Email address to send the URL to.
	 * - minOccurs : 1
	 * @var string
	 */
	public $NotificationEMailAddress;
	/**
	 * The NotificationMessage
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $NotificationMessage;
	/**
	 * Constructor method for EMailLabelDetail
	 * @see parent::__construct()
	 * @param string $_notificationEMailAddress
	 * @param string $_notificationMessage
	 * @return MytestStructEMailLabelDetail
	 */
	public function __construct($_notificationEMailAddress,$_notificationMessage = NULL)
	{
		parent::__construct(array('NotificationEMailAddress'=>$_notificationEMailAddress,'NotificationMessage'=>$_notificationMessage));
	}
	/**
	 * Get NotificationEMailAddress value
	 * @return string
	 */
	public function getNotificationEMailAddress()
	{
		return $this->NotificationEMailAddress;
	}
	/**
	 * Set NotificationEMailAddress value
	 * @param string $_notificationEMailAddress the NotificationEMailAddress
	 * @return string
	 */
	public function setNotificationEMailAddress($_notificationEMailAddress)
	{
		return ($this->NotificationEMailAddress = $_notificationEMailAddress);
	}
	/**
	 * Get NotificationMessage value
	 * @return string|null
	 */
	public function getNotificationMessage()
	{
		return $this->NotificationMessage;
	}
	/**
	 * Set NotificationMessage value
	 * @param string $_notificationMessage the NotificationMessage
	 * @return string
	 */
	public function setNotificationMessage($_notificationMessage)
	{
		return ($this->NotificationMessage = $_notificationMessage);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructEMailLabelDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>