<?php
/**
 * File for class MytestStructEMailNotificationDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructEMailNotificationDetail originally named EMailNotificationDetail
 * Documentation : Information describing email notifications that will be sent in relation to events that occur during package movement
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructEMailNotificationDetail extends MytestWsdlClass
{
	/**
	 * The PersonalMessage
	 * Meta informations extracted from the WSDL
	 * - documentation : A message that will be included in the email notifications
	 * - minOccurs : 0
	 * @var string
	 */
	public $PersonalMessage;
	/**
	 * The Recipients
	 * Meta informations extracted from the WSDL
	 * - documentation : Information describing the destination of the email, format of the email and events to be notified on
	 * - maxOccurs : 6
	 * - minOccurs : 0
	 * @var MytestStructEMailNotificationRecipient
	 */
	public $Recipients;
	/**
	 * Constructor method for EMailNotificationDetail
	 * @see parent::__construct()
	 * @param string $_personalMessage
	 * @param MytestStructEMailNotificationRecipient $_recipients
	 * @return MytestStructEMailNotificationDetail
	 */
	public function __construct($_personalMessage = NULL,$_recipients = NULL)
	{
		parent::__construct(array('PersonalMessage'=>$_personalMessage,'Recipients'=>$_recipients));
	}
	/**
	 * Get PersonalMessage value
	 * @return string|null
	 */
	public function getPersonalMessage()
	{
		return $this->PersonalMessage;
	}
	/**
	 * Set PersonalMessage value
	 * @param string $_personalMessage the PersonalMessage
	 * @return string
	 */
	public function setPersonalMessage($_personalMessage)
	{
		return ($this->PersonalMessage = $_personalMessage);
	}
	/**
	 * Get Recipients value
	 * @return MytestStructEMailNotificationRecipient|null
	 */
	public function getRecipients()
	{
		return $this->Recipients;
	}
	/**
	 * Set Recipients value
	 * @param MytestStructEMailNotificationRecipient $_recipients the Recipients
	 * @return MytestStructEMailNotificationRecipient
	 */
	public function setRecipients($_recipients)
	{
		return ($this->Recipients = $_recipients);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructEMailNotificationDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>