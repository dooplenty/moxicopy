<?php
/**
 * File for class MytestStructConfigurableLabelReferenceEntry
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructConfigurableLabelReferenceEntry originally named ConfigurableLabelReferenceEntry
 * Documentation : Defines additional data to print in the Configurable portion of the label, this allows you to print the same type information on the label that can also be printed on the doc tab.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructConfigurableLabelReferenceEntry extends MytestWsdlClass
{
	/**
	 * The ZoneNumber
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var positiveInteger
	 */
	public $ZoneNumber;
	/**
	 * The Header
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Header;
	/**
	 * The DataField
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $DataField;
	/**
	 * The LiteralValue
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $LiteralValue;
	/**
	 * Constructor method for ConfigurableLabelReferenceEntry
	 * @see parent::__construct()
	 * @param positiveInteger $_zoneNumber
	 * @param string $_header
	 * @param string $_dataField
	 * @param string $_literalValue
	 * @return MytestStructConfigurableLabelReferenceEntry
	 */
	public function __construct($_zoneNumber = NULL,$_header = NULL,$_dataField = NULL,$_literalValue = NULL)
	{
		parent::__construct(array('ZoneNumber'=>$_zoneNumber,'Header'=>$_header,'DataField'=>$_dataField,'LiteralValue'=>$_literalValue));
	}
	/**
	 * Get ZoneNumber value
	 * @return positiveInteger|null
	 */
	public function getZoneNumber()
	{
		return $this->ZoneNumber;
	}
	/**
	 * Set ZoneNumber value
	 * @param positiveInteger $_zoneNumber the ZoneNumber
	 * @return positiveInteger
	 */
	public function setZoneNumber($_zoneNumber)
	{
		return ($this->ZoneNumber = $_zoneNumber);
	}
	/**
	 * Get Header value
	 * @return string|null
	 */
	public function getHeader()
	{
		return $this->Header;
	}
	/**
	 * Set Header value
	 * @param string $_header the Header
	 * @return string
	 */
	public function setHeader($_header)
	{
		return ($this->Header = $_header);
	}
	/**
	 * Get DataField value
	 * @return string|null
	 */
	public function getDataField()
	{
		return $this->DataField;
	}
	/**
	 * Set DataField value
	 * @param string $_dataField the DataField
	 * @return string
	 */
	public function setDataField($_dataField)
	{
		return ($this->DataField = $_dataField);
	}
	/**
	 * Get LiteralValue value
	 * @return string|null
	 */
	public function getLiteralValue()
	{
		return $this->LiteralValue;
	}
	/**
	 * Set LiteralValue value
	 * @param string $_literalValue the LiteralValue
	 * @return string
	 */
	public function setLiteralValue($_literalValue)
	{
		return ($this->LiteralValue = $_literalValue);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructConfigurableLabelReferenceEntry
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>