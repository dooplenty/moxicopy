<?php
/**
 * File for class MytestEnumRegulatoryControlType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumRegulatoryControlType originally named RegulatoryControlType
 * Documentation : FOOD_OR_PERISHABLE is required by FDA/BTA; must be true for food/perishable items coming to US or PR from non-US/non-PR origin
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumRegulatoryControlType extends MytestWsdlClass
{
	/**
	 * Constant for value 'EU_CIRCULATION'
	 * @return string 'EU_CIRCULATION'
	 */
	const VALUE_EU_CIRCULATION = 'EU_CIRCULATION';
	/**
	 * Constant for value 'FOOD_OR_PERISHABLE'
	 * @return string 'FOOD_OR_PERISHABLE'
	 */
	const VALUE_FOOD_OR_PERISHABLE = 'FOOD_OR_PERISHABLE';
	/**
	 * Constant for value 'NAFTA'
	 * @return string 'NAFTA'
	 */
	const VALUE_NAFTA = 'NAFTA';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumRegulatoryControlType::VALUE_EU_CIRCULATION
	 * @uses MytestEnumRegulatoryControlType::VALUE_FOOD_OR_PERISHABLE
	 * @uses MytestEnumRegulatoryControlType::VALUE_NAFTA
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumRegulatoryControlType::VALUE_EU_CIRCULATION,MytestEnumRegulatoryControlType::VALUE_FOOD_OR_PERISHABLE,MytestEnumRegulatoryControlType::VALUE_NAFTA));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>