<?php
/**
 * File for class MytestEnumCarrierCodeType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumCarrierCodeType originally named CarrierCodeType
 * Documentation : Identification of a FedEx operating company (transportation).
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumCarrierCodeType extends MytestWsdlClass
{
	/**
	 * Constant for value 'FDXC'
	 * @return string 'FDXC'
	 */
	const VALUE_FDXC = 'FDXC';
	/**
	 * Constant for value 'FDXE'
	 * @return string 'FDXE'
	 */
	const VALUE_FDXE = 'FDXE';
	/**
	 * Constant for value 'FDXG'
	 * @return string 'FDXG'
	 */
	const VALUE_FDXG = 'FDXG';
	/**
	 * Constant for value 'FXCC'
	 * @return string 'FXCC'
	 */
	const VALUE_FXCC = 'FXCC';
	/**
	 * Constant for value 'FXFR'
	 * @return string 'FXFR'
	 */
	const VALUE_FXFR = 'FXFR';
	/**
	 * Constant for value 'FXSP'
	 * @return string 'FXSP'
	 */
	const VALUE_FXSP = 'FXSP';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumCarrierCodeType::VALUE_FDXC
	 * @uses MytestEnumCarrierCodeType::VALUE_FDXE
	 * @uses MytestEnumCarrierCodeType::VALUE_FDXG
	 * @uses MytestEnumCarrierCodeType::VALUE_FXCC
	 * @uses MytestEnumCarrierCodeType::VALUE_FXFR
	 * @uses MytestEnumCarrierCodeType::VALUE_FXSP
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumCarrierCodeType::VALUE_FDXC,MytestEnumCarrierCodeType::VALUE_FDXE,MytestEnumCarrierCodeType::VALUE_FDXG,MytestEnumCarrierCodeType::VALUE_FXCC,MytestEnumCarrierCodeType::VALUE_FXFR,MytestEnumCarrierCodeType::VALUE_FXSP));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>