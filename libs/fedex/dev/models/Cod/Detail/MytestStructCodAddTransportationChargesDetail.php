<?php
/**
 * File for class MytestStructCodAddTransportationChargesDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCodAddTransportationChargesDetail originally named CodAddTransportationChargesDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCodAddTransportationChargesDetail extends MytestWsdlClass
{
	/**
	 * The RateTypeBasis
	 * Meta informations extracted from the WSDL
	 * - documentation : Select the type of rate from which the element is to be selected.
	 * - minOccurs : 0
	 * @var MytestEnumRateTypeBasisType
	 */
	public $RateTypeBasis;
	/**
	 * The ChargeBasis
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumCodAddTransportationChargeBasisType
	 */
	public $ChargeBasis;
	/**
	 * The ChargeBasisLevel
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumChargeBasisLevelType
	 */
	public $ChargeBasisLevel;
	/**
	 * Constructor method for CodAddTransportationChargesDetail
	 * @see parent::__construct()
	 * @param MytestEnumRateTypeBasisType $_rateTypeBasis
	 * @param MytestEnumCodAddTransportationChargeBasisType $_chargeBasis
	 * @param MytestEnumChargeBasisLevelType $_chargeBasisLevel
	 * @return MytestStructCodAddTransportationChargesDetail
	 */
	public function __construct($_rateTypeBasis = NULL,$_chargeBasis = NULL,$_chargeBasisLevel = NULL)
	{
		parent::__construct(array('RateTypeBasis'=>$_rateTypeBasis,'ChargeBasis'=>$_chargeBasis,'ChargeBasisLevel'=>$_chargeBasisLevel));
	}
	/**
	 * Get RateTypeBasis value
	 * @return MytestEnumRateTypeBasisType|null
	 */
	public function getRateTypeBasis()
	{
		return $this->RateTypeBasis;
	}
	/**
	 * Set RateTypeBasis value
	 * @uses MytestEnumRateTypeBasisType::valueIsValid()
	 * @param MytestEnumRateTypeBasisType $_rateTypeBasis the RateTypeBasis
	 * @return MytestEnumRateTypeBasisType
	 */
	public function setRateTypeBasis($_rateTypeBasis)
	{
		if(!MytestEnumRateTypeBasisType::valueIsValid($_rateTypeBasis))
		{
			return false;
		}
		return ($this->RateTypeBasis = $_rateTypeBasis);
	}
	/**
	 * Get ChargeBasis value
	 * @return MytestEnumCodAddTransportationChargeBasisType|null
	 */
	public function getChargeBasis()
	{
		return $this->ChargeBasis;
	}
	/**
	 * Set ChargeBasis value
	 * @uses MytestEnumCodAddTransportationChargeBasisType::valueIsValid()
	 * @param MytestEnumCodAddTransportationChargeBasisType $_chargeBasis the ChargeBasis
	 * @return MytestEnumCodAddTransportationChargeBasisType
	 */
	public function setChargeBasis($_chargeBasis)
	{
		if(!MytestEnumCodAddTransportationChargeBasisType::valueIsValid($_chargeBasis))
		{
			return false;
		}
		return ($this->ChargeBasis = $_chargeBasis);
	}
	/**
	 * Get ChargeBasisLevel value
	 * @return MytestEnumChargeBasisLevelType|null
	 */
	public function getChargeBasisLevel()
	{
		return $this->ChargeBasisLevel;
	}
	/**
	 * Set ChargeBasisLevel value
	 * @uses MytestEnumChargeBasisLevelType::valueIsValid()
	 * @param MytestEnumChargeBasisLevelType $_chargeBasisLevel the ChargeBasisLevel
	 * @return MytestEnumChargeBasisLevelType
	 */
	public function setChargeBasisLevel($_chargeBasisLevel)
	{
		if(!MytestEnumChargeBasisLevelType::valueIsValid($_chargeBasisLevel))
		{
			return false;
		}
		return ($this->ChargeBasisLevel = $_chargeBasisLevel);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCodAddTransportationChargesDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>