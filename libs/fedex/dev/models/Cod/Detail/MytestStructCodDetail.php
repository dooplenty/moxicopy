<?php
/**
 * File for class MytestStructCodDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCodDetail originally named CodDetail
 * Documentation : Descriptive data required for a FedEx COD (Collect-On-Delivery) shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCodDetail extends MytestWsdlClass
{
	/**
	 * The CollectionType
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the type of funds FedEx should collect upon package delivery
	 * - minOccurs : 1
	 * @var MytestEnumCodCollectionType
	 */
	public $CollectionType;
	/**
	 * The CodCollectionAmount
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $CodCollectionAmount;
	/**
	 * The AddTransportationChargesDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the details of the charges are to be added to the COD collect amount.
	 * - minOccurs : 0
	 * @var MytestStructCodAddTransportationChargesDetail
	 */
	public $AddTransportationChargesDetail;
	/**
	 * The CodRecipient
	 * Meta informations extracted from the WSDL
	 * - documentation : For Express this is the descriptive data that is used for the recipient of the FedEx Letter containing the COD payment. For Ground this is the descriptive data for the party to receive the payment that prints the COD receipt.
	 * - minOccurs : 0
	 * @var MytestStructParty
	 */
	public $CodRecipient;
	/**
	 * The FinancialInstitutionContactAndAddress
	 * Meta informations extracted from the WSDL
	 * - documentation : When the FedEx COD payment type is not CASH, indicates the contact and address of the financial institution used to service the payment of the COD.
	 * - minOccurs : 0
	 * @var MytestStructContactAndAddress
	 */
	public $FinancialInstitutionContactAndAddress;
	/**
	 * The RemitToName
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the name of person or company receiving the secured/unsecured funds payment
	 * - minOccurs : 0
	 * @var string
	 */
	public $RemitToName;
	/**
	 * The ReferenceIndicator
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates which type of reference information to include on the COD return shipping label.
	 * - minOccurs : 0
	 * @var MytestEnumCodReturnReferenceIndicatorType
	 */
	public $ReferenceIndicator;
	/**
	 * The ReturnTrackingId
	 * Meta informations extracted from the WSDL
	 * - documentation : Only used with multi-piece COD shipments sent in multiple transactions. Required on last transaction only.
	 * - minOccurs : 0
	 * @var MytestStructTrackingId
	 */
	public $ReturnTrackingId;
	/**
	 * Constructor method for CodDetail
	 * @see parent::__construct()
	 * @param MytestEnumCodCollectionType $_collectionType
	 * @param MytestStructMoney $_codCollectionAmount
	 * @param MytestStructCodAddTransportationChargesDetail $_addTransportationChargesDetail
	 * @param MytestStructParty $_codRecipient
	 * @param MytestStructContactAndAddress $_financialInstitutionContactAndAddress
	 * @param string $_remitToName
	 * @param MytestEnumCodReturnReferenceIndicatorType $_referenceIndicator
	 * @param MytestStructTrackingId $_returnTrackingId
	 * @return MytestStructCodDetail
	 */
	public function __construct($_collectionType,$_codCollectionAmount = NULL,$_addTransportationChargesDetail = NULL,$_codRecipient = NULL,$_financialInstitutionContactAndAddress = NULL,$_remitToName = NULL,$_referenceIndicator = NULL,$_returnTrackingId = NULL)
	{
		parent::__construct(array('CollectionType'=>$_collectionType,'CodCollectionAmount'=>$_codCollectionAmount,'AddTransportationChargesDetail'=>$_addTransportationChargesDetail,'CodRecipient'=>$_codRecipient,'FinancialInstitutionContactAndAddress'=>$_financialInstitutionContactAndAddress,'RemitToName'=>$_remitToName,'ReferenceIndicator'=>$_referenceIndicator,'ReturnTrackingId'=>$_returnTrackingId));
	}
	/**
	 * Get CollectionType value
	 * @return MytestEnumCodCollectionType
	 */
	public function getCollectionType()
	{
		return $this->CollectionType;
	}
	/**
	 * Set CollectionType value
	 * @uses MytestEnumCodCollectionType::valueIsValid()
	 * @param MytestEnumCodCollectionType $_collectionType the CollectionType
	 * @return MytestEnumCodCollectionType
	 */
	public function setCollectionType($_collectionType)
	{
		if(!MytestEnumCodCollectionType::valueIsValid($_collectionType))
		{
			return false;
		}
		return ($this->CollectionType = $_collectionType);
	}
	/**
	 * Get CodCollectionAmount value
	 * @return MytestStructMoney|null
	 */
	public function getCodCollectionAmount()
	{
		return $this->CodCollectionAmount;
	}
	/**
	 * Set CodCollectionAmount value
	 * @param MytestStructMoney $_codCollectionAmount the CodCollectionAmount
	 * @return MytestStructMoney
	 */
	public function setCodCollectionAmount($_codCollectionAmount)
	{
		return ($this->CodCollectionAmount = $_codCollectionAmount);
	}
	/**
	 * Get AddTransportationChargesDetail value
	 * @return MytestStructCodAddTransportationChargesDetail|null
	 */
	public function getAddTransportationChargesDetail()
	{
		return $this->AddTransportationChargesDetail;
	}
	/**
	 * Set AddTransportationChargesDetail value
	 * @param MytestStructCodAddTransportationChargesDetail $_addTransportationChargesDetail the AddTransportationChargesDetail
	 * @return MytestStructCodAddTransportationChargesDetail
	 */
	public function setAddTransportationChargesDetail($_addTransportationChargesDetail)
	{
		return ($this->AddTransportationChargesDetail = $_addTransportationChargesDetail);
	}
	/**
	 * Get CodRecipient value
	 * @return MytestStructParty|null
	 */
	public function getCodRecipient()
	{
		return $this->CodRecipient;
	}
	/**
	 * Set CodRecipient value
	 * @param MytestStructParty $_codRecipient the CodRecipient
	 * @return MytestStructParty
	 */
	public function setCodRecipient($_codRecipient)
	{
		return ($this->CodRecipient = $_codRecipient);
	}
	/**
	 * Get FinancialInstitutionContactAndAddress value
	 * @return MytestStructContactAndAddress|null
	 */
	public function getFinancialInstitutionContactAndAddress()
	{
		return $this->FinancialInstitutionContactAndAddress;
	}
	/**
	 * Set FinancialInstitutionContactAndAddress value
	 * @param MytestStructContactAndAddress $_financialInstitutionContactAndAddress the FinancialInstitutionContactAndAddress
	 * @return MytestStructContactAndAddress
	 */
	public function setFinancialInstitutionContactAndAddress($_financialInstitutionContactAndAddress)
	{
		return ($this->FinancialInstitutionContactAndAddress = $_financialInstitutionContactAndAddress);
	}
	/**
	 * Get RemitToName value
	 * @return string|null
	 */
	public function getRemitToName()
	{
		return $this->RemitToName;
	}
	/**
	 * Set RemitToName value
	 * @param string $_remitToName the RemitToName
	 * @return string
	 */
	public function setRemitToName($_remitToName)
	{
		return ($this->RemitToName = $_remitToName);
	}
	/**
	 * Get ReferenceIndicator value
	 * @return MytestEnumCodReturnReferenceIndicatorType|null
	 */
	public function getReferenceIndicator()
	{
		return $this->ReferenceIndicator;
	}
	/**
	 * Set ReferenceIndicator value
	 * @uses MytestEnumCodReturnReferenceIndicatorType::valueIsValid()
	 * @param MytestEnumCodReturnReferenceIndicatorType $_referenceIndicator the ReferenceIndicator
	 * @return MytestEnumCodReturnReferenceIndicatorType
	 */
	public function setReferenceIndicator($_referenceIndicator)
	{
		if(!MytestEnumCodReturnReferenceIndicatorType::valueIsValid($_referenceIndicator))
		{
			return false;
		}
		return ($this->ReferenceIndicator = $_referenceIndicator);
	}
	/**
	 * Get ReturnTrackingId value
	 * @return MytestStructTrackingId|null
	 */
	public function getReturnTrackingId()
	{
		return $this->ReturnTrackingId;
	}
	/**
	 * Set ReturnTrackingId value
	 * @param MytestStructTrackingId $_returnTrackingId the ReturnTrackingId
	 * @return MytestStructTrackingId
	 */
	public function setReturnTrackingId($_returnTrackingId)
	{
		return ($this->ReturnTrackingId = $_returnTrackingId);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCodDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>