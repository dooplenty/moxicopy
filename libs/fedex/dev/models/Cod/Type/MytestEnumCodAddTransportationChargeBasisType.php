<?php
/**
 * File for class MytestEnumCodAddTransportationChargeBasisType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumCodAddTransportationChargeBasisType originally named CodAddTransportationChargeBasisType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumCodAddTransportationChargeBasisType extends MytestWsdlClass
{
	/**
	 * Constant for value 'COD_SURCHARGE'
	 * @return string 'COD_SURCHARGE'
	 */
	const VALUE_COD_SURCHARGE = 'COD_SURCHARGE';
	/**
	 * Constant for value 'NET_CHARGE'
	 * @return string 'NET_CHARGE'
	 */
	const VALUE_NET_CHARGE = 'NET_CHARGE';
	/**
	 * Constant for value 'NET_FREIGHT'
	 * @return string 'NET_FREIGHT'
	 */
	const VALUE_NET_FREIGHT = 'NET_FREIGHT';
	/**
	 * Constant for value 'TOTAL_CUSTOMER_CHARGE'
	 * @return string 'TOTAL_CUSTOMER_CHARGE'
	 */
	const VALUE_TOTAL_CUSTOMER_CHARGE = 'TOTAL_CUSTOMER_CHARGE';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumCodAddTransportationChargeBasisType::VALUE_COD_SURCHARGE
	 * @uses MytestEnumCodAddTransportationChargeBasisType::VALUE_NET_CHARGE
	 * @uses MytestEnumCodAddTransportationChargeBasisType::VALUE_NET_FREIGHT
	 * @uses MytestEnumCodAddTransportationChargeBasisType::VALUE_TOTAL_CUSTOMER_CHARGE
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumCodAddTransportationChargeBasisType::VALUE_COD_SURCHARGE,MytestEnumCodAddTransportationChargeBasisType::VALUE_NET_CHARGE,MytestEnumCodAddTransportationChargeBasisType::VALUE_NET_FREIGHT,MytestEnumCodAddTransportationChargeBasisType::VALUE_TOTAL_CUSTOMER_CHARGE));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>