<?php
/**
 * File for class MytestEnumCodCollectionType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumCodCollectionType originally named CodCollectionType
 * Documentation : Identifies the type of funds FedEx should collect upon shipment delivery.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumCodCollectionType extends MytestWsdlClass
{
	/**
	 * Constant for value 'ANY'
	 * @return string 'ANY'
	 */
	const VALUE_ANY = 'ANY';
	/**
	 * Constant for value 'CASH'
	 * @return string 'CASH'
	 */
	const VALUE_CASH = 'CASH';
	/**
	 * Constant for value 'GUARANTEED_FUNDS'
	 * @return string 'GUARANTEED_FUNDS'
	 */
	const VALUE_GUARANTEED_FUNDS = 'GUARANTEED_FUNDS';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumCodCollectionType::VALUE_ANY
	 * @uses MytestEnumCodCollectionType::VALUE_CASH
	 * @uses MytestEnumCodCollectionType::VALUE_GUARANTEED_FUNDS
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumCodCollectionType::VALUE_ANY,MytestEnumCodCollectionType::VALUE_CASH,MytestEnumCodCollectionType::VALUE_GUARANTEED_FUNDS));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>