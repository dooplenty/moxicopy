<?php
/**
 * File for class MytestEnumCodReturnReferenceIndicatorType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumCodReturnReferenceIndicatorType originally named CodReturnReferenceIndicatorType
 * Documentation : Indicates which type of reference information to include on the COD return shipping label.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumCodReturnReferenceIndicatorType extends MytestWsdlClass
{
	/**
	 * Constant for value 'INVOICE'
	 * @return string 'INVOICE'
	 */
	const VALUE_INVOICE = 'INVOICE';
	/**
	 * Constant for value 'PO'
	 * @return string 'PO'
	 */
	const VALUE_PO = 'PO';
	/**
	 * Constant for value 'REFERENCE'
	 * @return string 'REFERENCE'
	 */
	const VALUE_REFERENCE = 'REFERENCE';
	/**
	 * Constant for value 'TRACKING'
	 * @return string 'TRACKING'
	 */
	const VALUE_TRACKING = 'TRACKING';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumCodReturnReferenceIndicatorType::VALUE_INVOICE
	 * @uses MytestEnumCodReturnReferenceIndicatorType::VALUE_PO
	 * @uses MytestEnumCodReturnReferenceIndicatorType::VALUE_REFERENCE
	 * @uses MytestEnumCodReturnReferenceIndicatorType::VALUE_TRACKING
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumCodReturnReferenceIndicatorType::VALUE_INVOICE,MytestEnumCodReturnReferenceIndicatorType::VALUE_PO,MytestEnumCodReturnReferenceIndicatorType::VALUE_REFERENCE,MytestEnumCodReturnReferenceIndicatorType::VALUE_TRACKING));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>