<?php
/**
 * File for class MytestEnumPurposeOfShipmentType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumPurposeOfShipmentType originally named PurposeOfShipmentType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumPurposeOfShipmentType extends MytestWsdlClass
{
	/**
	 * Constant for value 'GIFT'
	 * @return string 'GIFT'
	 */
	const VALUE_GIFT = 'GIFT';
	/**
	 * Constant for value 'NOT_SOLD'
	 * @return string 'NOT_SOLD'
	 */
	const VALUE_NOT_SOLD = 'NOT_SOLD';
	/**
	 * Constant for value 'PERSONAL_EFFECTS'
	 * @return string 'PERSONAL_EFFECTS'
	 */
	const VALUE_PERSONAL_EFFECTS = 'PERSONAL_EFFECTS';
	/**
	 * Constant for value 'REPAIR_AND_RETURN'
	 * @return string 'REPAIR_AND_RETURN'
	 */
	const VALUE_REPAIR_AND_RETURN = 'REPAIR_AND_RETURN';
	/**
	 * Constant for value 'SAMPLE'
	 * @return string 'SAMPLE'
	 */
	const VALUE_SAMPLE = 'SAMPLE';
	/**
	 * Constant for value 'SOLD'
	 * @return string 'SOLD'
	 */
	const VALUE_SOLD = 'SOLD';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumPurposeOfShipmentType::VALUE_GIFT
	 * @uses MytestEnumPurposeOfShipmentType::VALUE_NOT_SOLD
	 * @uses MytestEnumPurposeOfShipmentType::VALUE_PERSONAL_EFFECTS
	 * @uses MytestEnumPurposeOfShipmentType::VALUE_REPAIR_AND_RETURN
	 * @uses MytestEnumPurposeOfShipmentType::VALUE_SAMPLE
	 * @uses MytestEnumPurposeOfShipmentType::VALUE_SOLD
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumPurposeOfShipmentType::VALUE_GIFT,MytestEnumPurposeOfShipmentType::VALUE_NOT_SOLD,MytestEnumPurposeOfShipmentType::VALUE_PERSONAL_EFFECTS,MytestEnumPurposeOfShipmentType::VALUE_REPAIR_AND_RETURN,MytestEnumPurposeOfShipmentType::VALUE_SAMPLE,MytestEnumPurposeOfShipmentType::VALUE_SOLD));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>