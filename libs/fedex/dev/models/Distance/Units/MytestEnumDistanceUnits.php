<?php
/**
 * File for class MytestEnumDistanceUnits
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumDistanceUnits originally named DistanceUnits
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumDistanceUnits extends MytestWsdlClass
{
	/**
	 * Constant for value 'KM'
	 * @return string 'KM'
	 */
	const VALUE_KM = 'KM';
	/**
	 * Constant for value 'MI'
	 * @return string 'MI'
	 */
	const VALUE_MI = 'MI';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumDistanceUnits::VALUE_KM
	 * @uses MytestEnumDistanceUnits::VALUE_MI
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumDistanceUnits::VALUE_KM,MytestEnumDistanceUnits::VALUE_MI));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>