<?php
/**
 * File for class MytestStructDistance
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructDistance originally named Distance
 * Documentation : Driving or other transportation distances, distinct from dimension measurements.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructDistance extends MytestWsdlClass
{
	/**
	 * The Value
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the distance quantity.
	 * - minOccurs : 0
	 * @var decimal
	 */
	public $Value;
	/**
	 * The Units
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the unit of measure for the distance value.
	 * - minOccurs : 0
	 * @var MytestEnumDistanceUnits
	 */
	public $Units;
	/**
	 * Constructor method for Distance
	 * @see parent::__construct()
	 * @param decimal $_value
	 * @param MytestEnumDistanceUnits $_units
	 * @return MytestStructDistance
	 */
	public function __construct($_value = NULL,$_units = NULL)
	{
		parent::__construct(array('Value'=>$_value,'Units'=>$_units));
	}
	/**
	 * Get Value value
	 * @return decimal|null
	 */
	public function getValue()
	{
		return $this->Value;
	}
	/**
	 * Set Value value
	 * @param decimal $_value the Value
	 * @return decimal
	 */
	public function setValue($_value)
	{
		return ($this->Value = $_value);
	}
	/**
	 * Get Units value
	 * @return MytestEnumDistanceUnits|null
	 */
	public function getUnits()
	{
		return $this->Units;
	}
	/**
	 * Set Units value
	 * @uses MytestEnumDistanceUnits::valueIsValid()
	 * @param MytestEnumDistanceUnits $_units the Units
	 * @return MytestEnumDistanceUnits
	 */
	public function setUnits($_units)
	{
		if(!MytestEnumDistanceUnits::valueIsValid($_units))
		{
			return false;
		}
		return ($this->Units = $_units);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructDistance
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>