<?php
/**
 * File for class MytestEnumClearanceBrokerageType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumClearanceBrokerageType originally named ClearanceBrokerageType
 * Documentation : Specifies the type of brokerage to be applied to a shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumClearanceBrokerageType extends MytestWsdlClass
{
	/**
	 * Constant for value 'BROKER_INCLUSIVE'
	 * @return string 'BROKER_INCLUSIVE'
	 */
	const VALUE_BROKER_INCLUSIVE = 'BROKER_INCLUSIVE';
	/**
	 * Constant for value 'BROKER_INCLUSIVE_NON_RESIDENT_IMPORTER'
	 * @return string 'BROKER_INCLUSIVE_NON_RESIDENT_IMPORTER'
	 */
	const VALUE_BROKER_INCLUSIVE_NON_RESIDENT_IMPORTER = 'BROKER_INCLUSIVE_NON_RESIDENT_IMPORTER';
	/**
	 * Constant for value 'BROKER_SELECT'
	 * @return string 'BROKER_SELECT'
	 */
	const VALUE_BROKER_SELECT = 'BROKER_SELECT';
	/**
	 * Constant for value 'BROKER_SELECT_NON_RESIDENT_IMPORTER'
	 * @return string 'BROKER_SELECT_NON_RESIDENT_IMPORTER'
	 */
	const VALUE_BROKER_SELECT_NON_RESIDENT_IMPORTER = 'BROKER_SELECT_NON_RESIDENT_IMPORTER';
	/**
	 * Constant for value 'BROKER_UNASSIGNED'
	 * @return string 'BROKER_UNASSIGNED'
	 */
	const VALUE_BROKER_UNASSIGNED = 'BROKER_UNASSIGNED';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumClearanceBrokerageType::VALUE_BROKER_INCLUSIVE
	 * @uses MytestEnumClearanceBrokerageType::VALUE_BROKER_INCLUSIVE_NON_RESIDENT_IMPORTER
	 * @uses MytestEnumClearanceBrokerageType::VALUE_BROKER_SELECT
	 * @uses MytestEnumClearanceBrokerageType::VALUE_BROKER_SELECT_NON_RESIDENT_IMPORTER
	 * @uses MytestEnumClearanceBrokerageType::VALUE_BROKER_UNASSIGNED
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumClearanceBrokerageType::VALUE_BROKER_INCLUSIVE,MytestEnumClearanceBrokerageType::VALUE_BROKER_INCLUSIVE_NON_RESIDENT_IMPORTER,MytestEnumClearanceBrokerageType::VALUE_BROKER_SELECT,MytestEnumClearanceBrokerageType::VALUE_BROKER_SELECT_NON_RESIDENT_IMPORTER,MytestEnumClearanceBrokerageType::VALUE_BROKER_UNASSIGNED));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>