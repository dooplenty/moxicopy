<?php
/**
 * File for class MytestStructPayment
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructPayment originally named Payment
 * Documentation : The descriptive data for the monetary compensation given to FedEx for services rendered to the customer.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructPayment extends MytestWsdlClass
{
	/**
	 * The PaymentType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumPaymentType
	 */
	public $PaymentType;
	/**
	 * The Payor
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructPayor
	 */
	public $Payor;
	/**
	 * Constructor method for Payment
	 * @see parent::__construct()
	 * @param MytestEnumPaymentType $_paymentType
	 * @param MytestStructPayor $_payor
	 * @return MytestStructPayment
	 */
	public function __construct($_paymentType = NULL,$_payor = NULL)
	{
		parent::__construct(array('PaymentType'=>$_paymentType,'Payor'=>$_payor));
	}
	/**
	 * Get PaymentType value
	 * @return MytestEnumPaymentType|null
	 */
	public function getPaymentType()
	{
		return $this->PaymentType;
	}
	/**
	 * Set PaymentType value
	 * @uses MytestEnumPaymentType::valueIsValid()
	 * @param MytestEnumPaymentType $_paymentType the PaymentType
	 * @return MytestEnumPaymentType
	 */
	public function setPaymentType($_paymentType)
	{
		if(!MytestEnumPaymentType::valueIsValid($_paymentType))
		{
			return false;
		}
		return ($this->PaymentType = $_paymentType);
	}
	/**
	 * Get Payor value
	 * @return MytestStructPayor|null
	 */
	public function getPayor()
	{
		return $this->Payor;
	}
	/**
	 * Set Payor value
	 * @param MytestStructPayor $_payor the Payor
	 * @return MytestStructPayor
	 */
	public function setPayor($_payor)
	{
		return ($this->Payor = $_payor);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructPayment
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>