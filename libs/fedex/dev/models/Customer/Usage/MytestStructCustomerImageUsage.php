<?php
/**
 * File for class MytestStructCustomerImageUsage
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCustomerImageUsage originally named CustomerImageUsage
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCustomerImageUsage extends MytestWsdlClass
{
	/**
	 * The Type
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumCustomerImageUsageType
	 */
	public $Type;
	/**
	 * The Id
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumImageId
	 */
	public $Id;
	/**
	 * Constructor method for CustomerImageUsage
	 * @see parent::__construct()
	 * @param MytestEnumCustomerImageUsageType $_type
	 * @param MytestEnumImageId $_id
	 * @return MytestStructCustomerImageUsage
	 */
	public function __construct($_type = NULL,$_id = NULL)
	{
		parent::__construct(array('Type'=>$_type,'Id'=>$_id));
	}
	/**
	 * Get Type value
	 * @return MytestEnumCustomerImageUsageType|null
	 */
	public function getType()
	{
		return $this->Type;
	}
	/**
	 * Set Type value
	 * @uses MytestEnumCustomerImageUsageType::valueIsValid()
	 * @param MytestEnumCustomerImageUsageType $_type the Type
	 * @return MytestEnumCustomerImageUsageType
	 */
	public function setType($_type)
	{
		if(!MytestEnumCustomerImageUsageType::valueIsValid($_type))
		{
			return false;
		}
		return ($this->Type = $_type);
	}
	/**
	 * Get Id value
	 * @return MytestEnumImageId|null
	 */
	public function getId()
	{
		return $this->Id;
	}
	/**
	 * Set Id value
	 * @uses MytestEnumImageId::valueIsValid()
	 * @param MytestEnumImageId $_id the Id
	 * @return MytestEnumImageId
	 */
	public function setId($_id)
	{
		if(!MytestEnumImageId::valueIsValid($_id))
		{
			return false;
		}
		return ($this->Id = $_id);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCustomerImageUsage
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>