<?php
/**
 * File for class MytestEnumCustomerImageUsageType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumCustomerImageUsageType originally named CustomerImageUsageType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumCustomerImageUsageType extends MytestWsdlClass
{
	/**
	 * Constant for value 'LETTER_HEAD'
	 * @return string 'LETTER_HEAD'
	 */
	const VALUE_LETTER_HEAD = 'LETTER_HEAD';
	/**
	 * Constant for value 'SIGNATURE'
	 * @return string 'SIGNATURE'
	 */
	const VALUE_SIGNATURE = 'SIGNATURE';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumCustomerImageUsageType::VALUE_LETTER_HEAD
	 * @uses MytestEnumCustomerImageUsageType::VALUE_SIGNATURE
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumCustomerImageUsageType::VALUE_LETTER_HEAD,MytestEnumCustomerImageUsageType::VALUE_SIGNATURE));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>