<?php
/**
 * File for class MytestEnumCustomerReferenceType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumCustomerReferenceType originally named CustomerReferenceType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumCustomerReferenceType extends MytestWsdlClass
{
	/**
	 * Constant for value 'BILL_OF_LADING'
	 * @return string 'BILL_OF_LADING'
	 */
	const VALUE_BILL_OF_LADING = 'BILL_OF_LADING';
	/**
	 * Constant for value 'CUSTOMER_REFERENCE'
	 * @return string 'CUSTOMER_REFERENCE'
	 */
	const VALUE_CUSTOMER_REFERENCE = 'CUSTOMER_REFERENCE';
	/**
	 * Constant for value 'DEPARTMENT_NUMBER'
	 * @return string 'DEPARTMENT_NUMBER'
	 */
	const VALUE_DEPARTMENT_NUMBER = 'DEPARTMENT_NUMBER';
	/**
	 * Constant for value 'ELECTRONIC_PRODUCT_CODE'
	 * @return string 'ELECTRONIC_PRODUCT_CODE'
	 */
	const VALUE_ELECTRONIC_PRODUCT_CODE = 'ELECTRONIC_PRODUCT_CODE';
	/**
	 * Constant for value 'INTRACOUNTRY_REGULATORY_REFERENCE'
	 * @return string 'INTRACOUNTRY_REGULATORY_REFERENCE'
	 */
	const VALUE_INTRACOUNTRY_REGULATORY_REFERENCE = 'INTRACOUNTRY_REGULATORY_REFERENCE';
	/**
	 * Constant for value 'INVOICE_NUMBER'
	 * @return string 'INVOICE_NUMBER'
	 */
	const VALUE_INVOICE_NUMBER = 'INVOICE_NUMBER';
	/**
	 * Constant for value 'PACKING_SLIP_NUMBER'
	 * @return string 'PACKING_SLIP_NUMBER'
	 */
	const VALUE_PACKING_SLIP_NUMBER = 'PACKING_SLIP_NUMBER';
	/**
	 * Constant for value 'P_O_NUMBER'
	 * @return string 'P_O_NUMBER'
	 */
	const VALUE_P_O_NUMBER = 'P_O_NUMBER';
	/**
	 * Constant for value 'RMA_ASSOCIATION'
	 * @return string 'RMA_ASSOCIATION'
	 */
	const VALUE_RMA_ASSOCIATION = 'RMA_ASSOCIATION';
	/**
	 * Constant for value 'SHIPMENT_INTEGRITY'
	 * @return string 'SHIPMENT_INTEGRITY'
	 */
	const VALUE_SHIPMENT_INTEGRITY = 'SHIPMENT_INTEGRITY';
	/**
	 * Constant for value 'STORE_NUMBER'
	 * @return string 'STORE_NUMBER'
	 */
	const VALUE_STORE_NUMBER = 'STORE_NUMBER';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumCustomerReferenceType::VALUE_BILL_OF_LADING
	 * @uses MytestEnumCustomerReferenceType::VALUE_CUSTOMER_REFERENCE
	 * @uses MytestEnumCustomerReferenceType::VALUE_DEPARTMENT_NUMBER
	 * @uses MytestEnumCustomerReferenceType::VALUE_ELECTRONIC_PRODUCT_CODE
	 * @uses MytestEnumCustomerReferenceType::VALUE_INTRACOUNTRY_REGULATORY_REFERENCE
	 * @uses MytestEnumCustomerReferenceType::VALUE_INVOICE_NUMBER
	 * @uses MytestEnumCustomerReferenceType::VALUE_PACKING_SLIP_NUMBER
	 * @uses MytestEnumCustomerReferenceType::VALUE_P_O_NUMBER
	 * @uses MytestEnumCustomerReferenceType::VALUE_RMA_ASSOCIATION
	 * @uses MytestEnumCustomerReferenceType::VALUE_SHIPMENT_INTEGRITY
	 * @uses MytestEnumCustomerReferenceType::VALUE_STORE_NUMBER
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumCustomerReferenceType::VALUE_BILL_OF_LADING,MytestEnumCustomerReferenceType::VALUE_CUSTOMER_REFERENCE,MytestEnumCustomerReferenceType::VALUE_DEPARTMENT_NUMBER,MytestEnumCustomerReferenceType::VALUE_ELECTRONIC_PRODUCT_CODE,MytestEnumCustomerReferenceType::VALUE_INTRACOUNTRY_REGULATORY_REFERENCE,MytestEnumCustomerReferenceType::VALUE_INVOICE_NUMBER,MytestEnumCustomerReferenceType::VALUE_PACKING_SLIP_NUMBER,MytestEnumCustomerReferenceType::VALUE_P_O_NUMBER,MytestEnumCustomerReferenceType::VALUE_RMA_ASSOCIATION,MytestEnumCustomerReferenceType::VALUE_SHIPMENT_INTEGRITY,MytestEnumCustomerReferenceType::VALUE_STORE_NUMBER));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>