<?php
/**
 * File for class MytestStructCustomerSpecifiedLabelDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCustomerSpecifiedLabelDetail originally named CustomerSpecifiedLabelDetail
 * Documentation : Allows customer-specified control of label content.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCustomerSpecifiedLabelDetail extends MytestWsdlClass
{
	/**
	 * The DocTabContent
	 * Meta informations extracted from the WSDL
	 * - documentation : If omitted, no doc tab will be produced (i.e. default is former NONE type).
	 * - minOccurs : 0
	 * @var MytestStructDocTabContent
	 */
	public $DocTabContent;
	/**
	 * The CustomContent
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructCustomLabelDetail
	 */
	public $CustomContent;
	/**
	 * The ConfigurableReferenceEntries
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructConfigurableLabelReferenceEntry
	 */
	public $ConfigurableReferenceEntries;
	/**
	 * The MaskedData
	 * Meta informations extracted from the WSDL
	 * - documentation : Controls which data/sections will be suppressed.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumLabelMaskableDataType
	 */
	public $MaskedData;
	/**
	 * The SecondaryBarcode
	 * Meta informations extracted from the WSDL
	 * - documentation : For customers producing their own Ground labels, this field specifies which secondary barcode will be printed on the label; so that the primary barcode produced by FedEx has the correct SCNC.
	 * - minOccurs : 0
	 * @var MytestEnumSecondaryBarcodeType
	 */
	public $SecondaryBarcode;
	/**
	 * The TermsAndConditionsLocalization
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructLocalization
	 */
	public $TermsAndConditionsLocalization;
	/**
	 * The AdditionalLabels
	 * Meta informations extracted from the WSDL
	 * - documentation : Controls the number of additional copies of supplemental labels.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructAdditionalLabelsDetail
	 */
	public $AdditionalLabels;
	/**
	 * The AirWaybillSuppressionCount
	 * Meta informations extracted from the WSDL
	 * - documentation : This value reduces the default quantity of destination/consignee air waybill labels. A value of zero indicates no change to default. A minimum of one copy will always be produced.
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $AirWaybillSuppressionCount;
	/**
	 * Constructor method for CustomerSpecifiedLabelDetail
	 * @see parent::__construct()
	 * @param MytestStructDocTabContent $_docTabContent
	 * @param MytestStructCustomLabelDetail $_customContent
	 * @param MytestStructConfigurableLabelReferenceEntry $_configurableReferenceEntries
	 * @param MytestEnumLabelMaskableDataType $_maskedData
	 * @param MytestEnumSecondaryBarcodeType $_secondaryBarcode
	 * @param MytestStructLocalization $_termsAndConditionsLocalization
	 * @param MytestStructAdditionalLabelsDetail $_additionalLabels
	 * @param nonNegativeInteger $_airWaybillSuppressionCount
	 * @return MytestStructCustomerSpecifiedLabelDetail
	 */
	public function __construct($_docTabContent = NULL,$_customContent = NULL,$_configurableReferenceEntries = NULL,$_maskedData = NULL,$_secondaryBarcode = NULL,$_termsAndConditionsLocalization = NULL,$_additionalLabels = NULL,$_airWaybillSuppressionCount = NULL)
	{
		parent::__construct(array('DocTabContent'=>$_docTabContent,'CustomContent'=>$_customContent,'ConfigurableReferenceEntries'=>$_configurableReferenceEntries,'MaskedData'=>$_maskedData,'SecondaryBarcode'=>$_secondaryBarcode,'TermsAndConditionsLocalization'=>$_termsAndConditionsLocalization,'AdditionalLabels'=>$_additionalLabels,'AirWaybillSuppressionCount'=>$_airWaybillSuppressionCount));
	}
	/**
	 * Get DocTabContent value
	 * @return MytestStructDocTabContent|null
	 */
	public function getDocTabContent()
	{
		return $this->DocTabContent;
	}
	/**
	 * Set DocTabContent value
	 * @param MytestStructDocTabContent $_docTabContent the DocTabContent
	 * @return MytestStructDocTabContent
	 */
	public function setDocTabContent($_docTabContent)
	{
		return ($this->DocTabContent = $_docTabContent);
	}
	/**
	 * Get CustomContent value
	 * @return MytestStructCustomLabelDetail|null
	 */
	public function getCustomContent()
	{
		return $this->CustomContent;
	}
	/**
	 * Set CustomContent value
	 * @param MytestStructCustomLabelDetail $_customContent the CustomContent
	 * @return MytestStructCustomLabelDetail
	 */
	public function setCustomContent($_customContent)
	{
		return ($this->CustomContent = $_customContent);
	}
	/**
	 * Get ConfigurableReferenceEntries value
	 * @return MytestStructConfigurableLabelReferenceEntry|null
	 */
	public function getConfigurableReferenceEntries()
	{
		return $this->ConfigurableReferenceEntries;
	}
	/**
	 * Set ConfigurableReferenceEntries value
	 * @param MytestStructConfigurableLabelReferenceEntry $_configurableReferenceEntries the ConfigurableReferenceEntries
	 * @return MytestStructConfigurableLabelReferenceEntry
	 */
	public function setConfigurableReferenceEntries($_configurableReferenceEntries)
	{
		return ($this->ConfigurableReferenceEntries = $_configurableReferenceEntries);
	}
	/**
	 * Get MaskedData value
	 * @return MytestEnumLabelMaskableDataType|null
	 */
	public function getMaskedData()
	{
		return $this->MaskedData;
	}
	/**
	 * Set MaskedData value
	 * @uses MytestEnumLabelMaskableDataType::valueIsValid()
	 * @param MytestEnumLabelMaskableDataType $_maskedData the MaskedData
	 * @return MytestEnumLabelMaskableDataType
	 */
	public function setMaskedData($_maskedData)
	{
		if(!MytestEnumLabelMaskableDataType::valueIsValid($_maskedData))
		{
			return false;
		}
		return ($this->MaskedData = $_maskedData);
	}
	/**
	 * Get SecondaryBarcode value
	 * @return MytestEnumSecondaryBarcodeType|null
	 */
	public function getSecondaryBarcode()
	{
		return $this->SecondaryBarcode;
	}
	/**
	 * Set SecondaryBarcode value
	 * @uses MytestEnumSecondaryBarcodeType::valueIsValid()
	 * @param MytestEnumSecondaryBarcodeType $_secondaryBarcode the SecondaryBarcode
	 * @return MytestEnumSecondaryBarcodeType
	 */
	public function setSecondaryBarcode($_secondaryBarcode)
	{
		if(!MytestEnumSecondaryBarcodeType::valueIsValid($_secondaryBarcode))
		{
			return false;
		}
		return ($this->SecondaryBarcode = $_secondaryBarcode);
	}
	/**
	 * Get TermsAndConditionsLocalization value
	 * @return MytestStructLocalization|null
	 */
	public function getTermsAndConditionsLocalization()
	{
		return $this->TermsAndConditionsLocalization;
	}
	/**
	 * Set TermsAndConditionsLocalization value
	 * @param MytestStructLocalization $_termsAndConditionsLocalization the TermsAndConditionsLocalization
	 * @return MytestStructLocalization
	 */
	public function setTermsAndConditionsLocalization($_termsAndConditionsLocalization)
	{
		return ($this->TermsAndConditionsLocalization = $_termsAndConditionsLocalization);
	}
	/**
	 * Get AdditionalLabels value
	 * @return MytestStructAdditionalLabelsDetail|null
	 */
	public function getAdditionalLabels()
	{
		return $this->AdditionalLabels;
	}
	/**
	 * Set AdditionalLabels value
	 * @param MytestStructAdditionalLabelsDetail $_additionalLabels the AdditionalLabels
	 * @return MytestStructAdditionalLabelsDetail
	 */
	public function setAdditionalLabels($_additionalLabels)
	{
		return ($this->AdditionalLabels = $_additionalLabels);
	}
	/**
	 * Get AirWaybillSuppressionCount value
	 * @return nonNegativeInteger|null
	 */
	public function getAirWaybillSuppressionCount()
	{
		return $this->AirWaybillSuppressionCount;
	}
	/**
	 * Set AirWaybillSuppressionCount value
	 * @param nonNegativeInteger $_airWaybillSuppressionCount the AirWaybillSuppressionCount
	 * @return nonNegativeInteger
	 */
	public function setAirWaybillSuppressionCount($_airWaybillSuppressionCount)
	{
		return ($this->AirWaybillSuppressionCount = $_airWaybillSuppressionCount);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCustomerSpecifiedLabelDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>