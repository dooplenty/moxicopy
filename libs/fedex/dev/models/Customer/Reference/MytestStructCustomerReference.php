<?php
/**
 * File for class MytestStructCustomerReference
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCustomerReference originally named CustomerReference
 * Documentation : Reference information to be associated with this package.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCustomerReference extends MytestWsdlClass
{
	/**
	 * The CustomerReferenceType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 1
	 * @var MytestEnumCustomerReferenceType
	 */
	public $CustomerReferenceType;
	/**
	 * The Value
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 1
	 * @var string
	 */
	public $Value;
	/**
	 * Constructor method for CustomerReference
	 * @see parent::__construct()
	 * @param MytestEnumCustomerReferenceType $_customerReferenceType
	 * @param string $_value
	 * @return MytestStructCustomerReference
	 */
	public function __construct($_customerReferenceType,$_value)
	{
		parent::__construct(array('CustomerReferenceType'=>$_customerReferenceType,'Value'=>$_value));
	}
	/**
	 * Get CustomerReferenceType value
	 * @return MytestEnumCustomerReferenceType
	 */
	public function getCustomerReferenceType()
	{
		return $this->CustomerReferenceType;
	}
	/**
	 * Set CustomerReferenceType value
	 * @uses MytestEnumCustomerReferenceType::valueIsValid()
	 * @param MytestEnumCustomerReferenceType $_customerReferenceType the CustomerReferenceType
	 * @return MytestEnumCustomerReferenceType
	 */
	public function setCustomerReferenceType($_customerReferenceType)
	{
		if(!MytestEnumCustomerReferenceType::valueIsValid($_customerReferenceType))
		{
			return false;
		}
		return ($this->CustomerReferenceType = $_customerReferenceType);
	}
	/**
	 * Get Value value
	 * @return string
	 */
	public function getValue()
	{
		return $this->Value;
	}
	/**
	 * Set Value value
	 * @param string $_value the Value
	 * @return string
	 */
	public function setValue($_value)
	{
		return ($this->Value = $_value);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCustomerReference
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>