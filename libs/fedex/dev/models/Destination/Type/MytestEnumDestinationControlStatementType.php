<?php
/**
 * File for class MytestEnumDestinationControlStatementType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumDestinationControlStatementType originally named DestinationControlStatementType
 * Documentation : Used to indicate whether the Destination Control Statement is of type Department of Commerce, Department of State or both.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumDestinationControlStatementType extends MytestWsdlClass
{
	/**
	 * Constant for value 'DEPARTMENT_OF_COMMERCE'
	 * @return string 'DEPARTMENT_OF_COMMERCE'
	 */
	const VALUE_DEPARTMENT_OF_COMMERCE = 'DEPARTMENT_OF_COMMERCE';
	/**
	 * Constant for value 'DEPARTMENT_OF_STATE'
	 * @return string 'DEPARTMENT_OF_STATE'
	 */
	const VALUE_DEPARTMENT_OF_STATE = 'DEPARTMENT_OF_STATE';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumDestinationControlStatementType::VALUE_DEPARTMENT_OF_COMMERCE
	 * @uses MytestEnumDestinationControlStatementType::VALUE_DEPARTMENT_OF_STATE
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumDestinationControlStatementType::VALUE_DEPARTMENT_OF_COMMERCE,MytestEnumDestinationControlStatementType::VALUE_DEPARTMENT_OF_STATE));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>