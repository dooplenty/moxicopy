<?php
/**
 * File for class MytestStructDestinationControlDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructDestinationControlDetail originally named DestinationControlDetail
 * Documentation : Data required to complete the Destionation Control Statement for US exports.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructDestinationControlDetail extends MytestWsdlClass
{
	/**
	 * The StatementTypes
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumDestinationControlStatementType
	 */
	public $StatementTypes;
	/**
	 * The DestinationCountries
	 * Meta informations extracted from the WSDL
	 * - documentation : Comma-separated list of up to four country codes, required for DEPARTMENT_OF_STATE statement.
	 * - minOccurs : 0
	 * @var string
	 */
	public $DestinationCountries;
	/**
	 * The EndUser
	 * Meta informations extracted from the WSDL
	 * - documentation : Name of end user, required for DEPARTMENT_OF_STATE statement.
	 * - minOccurs : 0
	 * @var string
	 */
	public $EndUser;
	/**
	 * Constructor method for DestinationControlDetail
	 * @see parent::__construct()
	 * @param MytestEnumDestinationControlStatementType $_statementTypes
	 * @param string $_destinationCountries
	 * @param string $_endUser
	 * @return MytestStructDestinationControlDetail
	 */
	public function __construct($_statementTypes = NULL,$_destinationCountries = NULL,$_endUser = NULL)
	{
		parent::__construct(array('StatementTypes'=>$_statementTypes,'DestinationCountries'=>$_destinationCountries,'EndUser'=>$_endUser));
	}
	/**
	 * Get StatementTypes value
	 * @return MytestEnumDestinationControlStatementType|null
	 */
	public function getStatementTypes()
	{
		return $this->StatementTypes;
	}
	/**
	 * Set StatementTypes value
	 * @uses MytestEnumDestinationControlStatementType::valueIsValid()
	 * @param MytestEnumDestinationControlStatementType $_statementTypes the StatementTypes
	 * @return MytestEnumDestinationControlStatementType
	 */
	public function setStatementTypes($_statementTypes)
	{
		if(!MytestEnumDestinationControlStatementType::valueIsValid($_statementTypes))
		{
			return false;
		}
		return ($this->StatementTypes = $_statementTypes);
	}
	/**
	 * Get DestinationCountries value
	 * @return string|null
	 */
	public function getDestinationCountries()
	{
		return $this->DestinationCountries;
	}
	/**
	 * Set DestinationCountries value
	 * @param string $_destinationCountries the DestinationCountries
	 * @return string
	 */
	public function setDestinationCountries($_destinationCountries)
	{
		return ($this->DestinationCountries = $_destinationCountries);
	}
	/**
	 * Get EndUser value
	 * @return string|null
	 */
	public function getEndUser()
	{
		return $this->EndUser;
	}
	/**
	 * Set EndUser value
	 * @param string $_endUser the EndUser
	 * @return string
	 */
	public function setEndUser($_endUser)
	{
		return ($this->EndUser = $_endUser);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructDestinationControlDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>