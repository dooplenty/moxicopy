<?php
/**
 * File for class MytestStructCustomsClearanceDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCustomsClearanceDetail originally named CustomsClearanceDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCustomsClearanceDetail extends MytestWsdlClass
{
	/**
	 * The Brokers
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructBrokerDetail
	 */
	public $Brokers;
	/**
	 * The ClearanceBrokerage
	 * Meta informations extracted from the WSDL
	 * - documentation : Interacts both with properties of the shipment and contractual relationship with the shipper.
	 * - minOccurs : 0
	 * @var MytestEnumClearanceBrokerageType
	 */
	public $ClearanceBrokerage;
	/**
	 * The CustomsOptions
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructCustomsOptionDetail
	 */
	public $CustomsOptions;
	/**
	 * The ImporterOfRecord
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructParty
	 */
	public $ImporterOfRecord;
	/**
	 * The RecipientCustomsId
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies how the recipient is identified for customs purposes; the requirements on this information vary with destination country.
	 * - minOccurs : 0
	 * @var MytestStructRecipientCustomsId
	 */
	public $RecipientCustomsId;
	/**
	 * The DutiesPayment
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructPayment
	 */
	public $DutiesPayment;
	/**
	 * The DocumentContent
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumInternationalDocumentContentType
	 */
	public $DocumentContent;
	/**
	 * The CustomsValue
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $CustomsValue;
	/**
	 * The FreightOnValue
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies responsibilities with respect to loss, damage, etc.
	 * - minOccurs : 0
	 * @var MytestEnumFreightOnValueType
	 */
	public $FreightOnValue;
	/**
	 * The InsuranceCharges
	 * Meta informations extracted from the WSDL
	 * - documentation : Documents amount paid to third party for coverage of shipment content.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $InsuranceCharges;
	/**
	 * The PartiesToTransactionAreRelated
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var boolean
	 */
	public $PartiesToTransactionAreRelated;
	/**
	 * The CommercialInvoice
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructCommercialInvoice
	 */
	public $CommercialInvoice;
	/**
	 * The Commodities
	 * Meta informations extracted from the WSDL
	 * - documentation : For international multiple piece shipments, commodity information must be passed in the Master and on each child transaction. If this shipment cotains more than four commodities line items, the four highest valued should be included in the first 4 occurances for this request.
	 * - maxOccurs : 99
	 * - minOccurs : 0
	 * @var MytestStructCommodity
	 */
	public $Commodities;
	/**
	 * The ExportDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructExportDetail
	 */
	public $ExportDetail;
	/**
	 * The RegulatoryControls
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumRegulatoryControlType
	 */
	public $RegulatoryControls;
	/**
	 * Constructor method for CustomsClearanceDetail
	 * @see parent::__construct()
	 * @param MytestStructBrokerDetail $_brokers
	 * @param MytestEnumClearanceBrokerageType $_clearanceBrokerage
	 * @param MytestStructCustomsOptionDetail $_customsOptions
	 * @param MytestStructParty $_importerOfRecord
	 * @param MytestStructRecipientCustomsId $_recipientCustomsId
	 * @param MytestStructPayment $_dutiesPayment
	 * @param MytestEnumInternationalDocumentContentType $_documentContent
	 * @param MytestStructMoney $_customsValue
	 * @param MytestEnumFreightOnValueType $_freightOnValue
	 * @param MytestStructMoney $_insuranceCharges
	 * @param boolean $_partiesToTransactionAreRelated
	 * @param MytestStructCommercialInvoice $_commercialInvoice
	 * @param MytestStructCommodity $_commodities
	 * @param MytestStructExportDetail $_exportDetail
	 * @param MytestEnumRegulatoryControlType $_regulatoryControls
	 * @return MytestStructCustomsClearanceDetail
	 */
	public function __construct($_brokers = NULL,$_clearanceBrokerage = NULL,$_customsOptions = NULL,$_importerOfRecord = NULL,$_recipientCustomsId = NULL,$_dutiesPayment = NULL,$_documentContent = NULL,$_customsValue = NULL,$_freightOnValue = NULL,$_insuranceCharges = NULL,$_partiesToTransactionAreRelated = NULL,$_commercialInvoice = NULL,$_commodities = NULL,$_exportDetail = NULL,$_regulatoryControls = NULL)
	{
		parent::__construct(array('Brokers'=>$_brokers,'ClearanceBrokerage'=>$_clearanceBrokerage,'CustomsOptions'=>$_customsOptions,'ImporterOfRecord'=>$_importerOfRecord,'RecipientCustomsId'=>$_recipientCustomsId,'DutiesPayment'=>$_dutiesPayment,'DocumentContent'=>$_documentContent,'CustomsValue'=>$_customsValue,'FreightOnValue'=>$_freightOnValue,'InsuranceCharges'=>$_insuranceCharges,'PartiesToTransactionAreRelated'=>$_partiesToTransactionAreRelated,'CommercialInvoice'=>$_commercialInvoice,'Commodities'=>$_commodities,'ExportDetail'=>$_exportDetail,'RegulatoryControls'=>$_regulatoryControls));
	}
	/**
	 * Get Brokers value
	 * @return MytestStructBrokerDetail|null
	 */
	public function getBrokers()
	{
		return $this->Brokers;
	}
	/**
	 * Set Brokers value
	 * @param MytestStructBrokerDetail $_brokers the Brokers
	 * @return MytestStructBrokerDetail
	 */
	public function setBrokers($_brokers)
	{
		return ($this->Brokers = $_brokers);
	}
	/**
	 * Get ClearanceBrokerage value
	 * @return MytestEnumClearanceBrokerageType|null
	 */
	public function getClearanceBrokerage()
	{
		return $this->ClearanceBrokerage;
	}
	/**
	 * Set ClearanceBrokerage value
	 * @uses MytestEnumClearanceBrokerageType::valueIsValid()
	 * @param MytestEnumClearanceBrokerageType $_clearanceBrokerage the ClearanceBrokerage
	 * @return MytestEnumClearanceBrokerageType
	 */
	public function setClearanceBrokerage($_clearanceBrokerage)
	{
		if(!MytestEnumClearanceBrokerageType::valueIsValid($_clearanceBrokerage))
		{
			return false;
		}
		return ($this->ClearanceBrokerage = $_clearanceBrokerage);
	}
	/**
	 * Get CustomsOptions value
	 * @return MytestStructCustomsOptionDetail|null
	 */
	public function getCustomsOptions()
	{
		return $this->CustomsOptions;
	}
	/**
	 * Set CustomsOptions value
	 * @param MytestStructCustomsOptionDetail $_customsOptions the CustomsOptions
	 * @return MytestStructCustomsOptionDetail
	 */
	public function setCustomsOptions($_customsOptions)
	{
		return ($this->CustomsOptions = $_customsOptions);
	}
	/**
	 * Get ImporterOfRecord value
	 * @return MytestStructParty|null
	 */
	public function getImporterOfRecord()
	{
		return $this->ImporterOfRecord;
	}
	/**
	 * Set ImporterOfRecord value
	 * @param MytestStructParty $_importerOfRecord the ImporterOfRecord
	 * @return MytestStructParty
	 */
	public function setImporterOfRecord($_importerOfRecord)
	{
		return ($this->ImporterOfRecord = $_importerOfRecord);
	}
	/**
	 * Get RecipientCustomsId value
	 * @return MytestStructRecipientCustomsId|null
	 */
	public function getRecipientCustomsId()
	{
		return $this->RecipientCustomsId;
	}
	/**
	 * Set RecipientCustomsId value
	 * @param MytestStructRecipientCustomsId $_recipientCustomsId the RecipientCustomsId
	 * @return MytestStructRecipientCustomsId
	 */
	public function setRecipientCustomsId($_recipientCustomsId)
	{
		return ($this->RecipientCustomsId = $_recipientCustomsId);
	}
	/**
	 * Get DutiesPayment value
	 * @return MytestStructPayment|null
	 */
	public function getDutiesPayment()
	{
		return $this->DutiesPayment;
	}
	/**
	 * Set DutiesPayment value
	 * @param MytestStructPayment $_dutiesPayment the DutiesPayment
	 * @return MytestStructPayment
	 */
	public function setDutiesPayment($_dutiesPayment)
	{
		return ($this->DutiesPayment = $_dutiesPayment);
	}
	/**
	 * Get DocumentContent value
	 * @return MytestEnumInternationalDocumentContentType|null
	 */
	public function getDocumentContent()
	{
		return $this->DocumentContent;
	}
	/**
	 * Set DocumentContent value
	 * @uses MytestEnumInternationalDocumentContentType::valueIsValid()
	 * @param MytestEnumInternationalDocumentContentType $_documentContent the DocumentContent
	 * @return MytestEnumInternationalDocumentContentType
	 */
	public function setDocumentContent($_documentContent)
	{
		if(!MytestEnumInternationalDocumentContentType::valueIsValid($_documentContent))
		{
			return false;
		}
		return ($this->DocumentContent = $_documentContent);
	}
	/**
	 * Get CustomsValue value
	 * @return MytestStructMoney|null
	 */
	public function getCustomsValue()
	{
		return $this->CustomsValue;
	}
	/**
	 * Set CustomsValue value
	 * @param MytestStructMoney $_customsValue the CustomsValue
	 * @return MytestStructMoney
	 */
	public function setCustomsValue($_customsValue)
	{
		return ($this->CustomsValue = $_customsValue);
	}
	/**
	 * Get FreightOnValue value
	 * @return MytestEnumFreightOnValueType|null
	 */
	public function getFreightOnValue()
	{
		return $this->FreightOnValue;
	}
	/**
	 * Set FreightOnValue value
	 * @uses MytestEnumFreightOnValueType::valueIsValid()
	 * @param MytestEnumFreightOnValueType $_freightOnValue the FreightOnValue
	 * @return MytestEnumFreightOnValueType
	 */
	public function setFreightOnValue($_freightOnValue)
	{
		if(!MytestEnumFreightOnValueType::valueIsValid($_freightOnValue))
		{
			return false;
		}
		return ($this->FreightOnValue = $_freightOnValue);
	}
	/**
	 * Get InsuranceCharges value
	 * @return MytestStructMoney|null
	 */
	public function getInsuranceCharges()
	{
		return $this->InsuranceCharges;
	}
	/**
	 * Set InsuranceCharges value
	 * @param MytestStructMoney $_insuranceCharges the InsuranceCharges
	 * @return MytestStructMoney
	 */
	public function setInsuranceCharges($_insuranceCharges)
	{
		return ($this->InsuranceCharges = $_insuranceCharges);
	}
	/**
	 * Get PartiesToTransactionAreRelated value
	 * @return boolean|null
	 */
	public function getPartiesToTransactionAreRelated()
	{
		return $this->PartiesToTransactionAreRelated;
	}
	/**
	 * Set PartiesToTransactionAreRelated value
	 * @param boolean $_partiesToTransactionAreRelated the PartiesToTransactionAreRelated
	 * @return boolean
	 */
	public function setPartiesToTransactionAreRelated($_partiesToTransactionAreRelated)
	{
		return ($this->PartiesToTransactionAreRelated = $_partiesToTransactionAreRelated);
	}
	/**
	 * Get CommercialInvoice value
	 * @return MytestStructCommercialInvoice|null
	 */
	public function getCommercialInvoice()
	{
		return $this->CommercialInvoice;
	}
	/**
	 * Set CommercialInvoice value
	 * @param MytestStructCommercialInvoice $_commercialInvoice the CommercialInvoice
	 * @return MytestStructCommercialInvoice
	 */
	public function setCommercialInvoice($_commercialInvoice)
	{
		return ($this->CommercialInvoice = $_commercialInvoice);
	}
	/**
	 * Get Commodities value
	 * @return MytestStructCommodity|null
	 */
	public function getCommodities()
	{
		return $this->Commodities;
	}
	/**
	 * Set Commodities value
	 * @param MytestStructCommodity $_commodities the Commodities
	 * @return MytestStructCommodity
	 */
	public function setCommodities($_commodities)
	{
		return ($this->Commodities = $_commodities);
	}
	/**
	 * Get ExportDetail value
	 * @return MytestStructExportDetail|null
	 */
	public function getExportDetail()
	{
		return $this->ExportDetail;
	}
	/**
	 * Set ExportDetail value
	 * @param MytestStructExportDetail $_exportDetail the ExportDetail
	 * @return MytestStructExportDetail
	 */
	public function setExportDetail($_exportDetail)
	{
		return ($this->ExportDetail = $_exportDetail);
	}
	/**
	 * Get RegulatoryControls value
	 * @return MytestEnumRegulatoryControlType|null
	 */
	public function getRegulatoryControls()
	{
		return $this->RegulatoryControls;
	}
	/**
	 * Set RegulatoryControls value
	 * @uses MytestEnumRegulatoryControlType::valueIsValid()
	 * @param MytestEnumRegulatoryControlType $_regulatoryControls the RegulatoryControls
	 * @return MytestEnumRegulatoryControlType
	 */
	public function setRegulatoryControls($_regulatoryControls)
	{
		if(!MytestEnumRegulatoryControlType::valueIsValid($_regulatoryControls))
		{
			return false;
		}
		return ($this->RegulatoryControls = $_regulatoryControls);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCustomsClearanceDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>