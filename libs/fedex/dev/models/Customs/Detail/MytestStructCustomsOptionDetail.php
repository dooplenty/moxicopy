<?php
/**
 * File for class MytestStructCustomsOptionDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCustomsOptionDetail originally named CustomsOptionDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCustomsOptionDetail extends MytestWsdlClass
{
	/**
	 * The Type
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumCustomsOptionType
	 */
	public $Type;
	/**
	 * The Description
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies additional description about customs options. This is a required field when the customs options type is "OTHER".
	 * - minOccurs : 0
	 * @var string
	 */
	public $Description;
	/**
	 * Constructor method for CustomsOptionDetail
	 * @see parent::__construct()
	 * @param MytestEnumCustomsOptionType $_type
	 * @param string $_description
	 * @return MytestStructCustomsOptionDetail
	 */
	public function __construct($_type = NULL,$_description = NULL)
	{
		parent::__construct(array('Type'=>$_type,'Description'=>$_description));
	}
	/**
	 * Get Type value
	 * @return MytestEnumCustomsOptionType|null
	 */
	public function getType()
	{
		return $this->Type;
	}
	/**
	 * Set Type value
	 * @uses MytestEnumCustomsOptionType::valueIsValid()
	 * @param MytestEnumCustomsOptionType $_type the Type
	 * @return MytestEnumCustomsOptionType
	 */
	public function setType($_type)
	{
		if(!MytestEnumCustomsOptionType::valueIsValid($_type))
		{
			return false;
		}
		return ($this->Type = $_type);
	}
	/**
	 * Get Description value
	 * @return string|null
	 */
	public function getDescription()
	{
		return $this->Description;
	}
	/**
	 * Set Description value
	 * @param string $_description the Description
	 * @return string
	 */
	public function setDescription($_description)
	{
		return ($this->Description = $_description);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCustomsOptionDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>