<?php
/**
 * File for class MytestEnumCustomsOptionType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumCustomsOptionType originally named CustomsOptionType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumCustomsOptionType extends MytestWsdlClass
{
	/**
	 * Constant for value 'COURTESY_RETURN_LABEL'
	 * @return string 'COURTESY_RETURN_LABEL'
	 */
	const VALUE_COURTESY_RETURN_LABEL = 'COURTESY_RETURN_LABEL';
	/**
	 * Constant for value 'EXHIBITION_TRADE_SHOW'
	 * @return string 'EXHIBITION_TRADE_SHOW'
	 */
	const VALUE_EXHIBITION_TRADE_SHOW = 'EXHIBITION_TRADE_SHOW';
	/**
	 * Constant for value 'FAULTY_ITEM'
	 * @return string 'FAULTY_ITEM'
	 */
	const VALUE_FAULTY_ITEM = 'FAULTY_ITEM';
	/**
	 * Constant for value 'FOLLOWING_REPAIR'
	 * @return string 'FOLLOWING_REPAIR'
	 */
	const VALUE_FOLLOWING_REPAIR = 'FOLLOWING_REPAIR';
	/**
	 * Constant for value 'FOR_REPAIR'
	 * @return string 'FOR_REPAIR'
	 */
	const VALUE_FOR_REPAIR = 'FOR_REPAIR';
	/**
	 * Constant for value 'ITEM_FOR_LOAN'
	 * @return string 'ITEM_FOR_LOAN'
	 */
	const VALUE_ITEM_FOR_LOAN = 'ITEM_FOR_LOAN';
	/**
	 * Constant for value 'OTHER'
	 * @return string 'OTHER'
	 */
	const VALUE_OTHER = 'OTHER';
	/**
	 * Constant for value 'REJECTED'
	 * @return string 'REJECTED'
	 */
	const VALUE_REJECTED = 'REJECTED';
	/**
	 * Constant for value 'REPLACEMENT'
	 * @return string 'REPLACEMENT'
	 */
	const VALUE_REPLACEMENT = 'REPLACEMENT';
	/**
	 * Constant for value 'TRIAL'
	 * @return string 'TRIAL'
	 */
	const VALUE_TRIAL = 'TRIAL';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumCustomsOptionType::VALUE_COURTESY_RETURN_LABEL
	 * @uses MytestEnumCustomsOptionType::VALUE_EXHIBITION_TRADE_SHOW
	 * @uses MytestEnumCustomsOptionType::VALUE_FAULTY_ITEM
	 * @uses MytestEnumCustomsOptionType::VALUE_FOLLOWING_REPAIR
	 * @uses MytestEnumCustomsOptionType::VALUE_FOR_REPAIR
	 * @uses MytestEnumCustomsOptionType::VALUE_ITEM_FOR_LOAN
	 * @uses MytestEnumCustomsOptionType::VALUE_OTHER
	 * @uses MytestEnumCustomsOptionType::VALUE_REJECTED
	 * @uses MytestEnumCustomsOptionType::VALUE_REPLACEMENT
	 * @uses MytestEnumCustomsOptionType::VALUE_TRIAL
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumCustomsOptionType::VALUE_COURTESY_RETURN_LABEL,MytestEnumCustomsOptionType::VALUE_EXHIBITION_TRADE_SHOW,MytestEnumCustomsOptionType::VALUE_FAULTY_ITEM,MytestEnumCustomsOptionType::VALUE_FOLLOWING_REPAIR,MytestEnumCustomsOptionType::VALUE_FOR_REPAIR,MytestEnumCustomsOptionType::VALUE_ITEM_FOR_LOAN,MytestEnumCustomsOptionType::VALUE_OTHER,MytestEnumCustomsOptionType::VALUE_REJECTED,MytestEnumCustomsOptionType::VALUE_REPLACEMENT,MytestEnumCustomsOptionType::VALUE_TRIAL));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>