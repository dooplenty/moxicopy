<?php
/**
 * File for class MytestStructServiceSubOptionDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructServiceSubOptionDetail originally named ServiceSubOptionDetail
 * Documentation : Supporting detail for applied options identified in a rate quote.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructServiceSubOptionDetail extends MytestWsdlClass
{
	/**
	 * The FreightGuarantee
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the type of Freight Guarantee applied, if FREIGHT_GUARANTEE is applied to the rate quote.
	 * - minOccurs : 0
	 * @var MytestEnumFreightGuaranteeType
	 */
	public $FreightGuarantee;
	/**
	 * The SmartPostHubId
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the smartPostHubId used during rate quote, if SMART_POST_HUB_ID is a variable option on the rate request.
	 * - minOccurs : 0
	 * @var string
	 */
	public $SmartPostHubId;
	/**
	 * The SmartPostIndicia
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the indicia used during rate quote, if SMART_POST_ALLOWED_INDICIA is a variable option on the rate request.
	 * - minOccurs : 0
	 * @var MytestEnumSmartPostIndiciaType
	 */
	public $SmartPostIndicia;
	/**
	 * Constructor method for ServiceSubOptionDetail
	 * @see parent::__construct()
	 * @param MytestEnumFreightGuaranteeType $_freightGuarantee
	 * @param string $_smartPostHubId
	 * @param MytestEnumSmartPostIndiciaType $_smartPostIndicia
	 * @return MytestStructServiceSubOptionDetail
	 */
	public function __construct($_freightGuarantee = NULL,$_smartPostHubId = NULL,$_smartPostIndicia = NULL)
	{
		parent::__construct(array('FreightGuarantee'=>$_freightGuarantee,'SmartPostHubId'=>$_smartPostHubId,'SmartPostIndicia'=>$_smartPostIndicia));
	}
	/**
	 * Get FreightGuarantee value
	 * @return MytestEnumFreightGuaranteeType|null
	 */
	public function getFreightGuarantee()
	{
		return $this->FreightGuarantee;
	}
	/**
	 * Set FreightGuarantee value
	 * @uses MytestEnumFreightGuaranteeType::valueIsValid()
	 * @param MytestEnumFreightGuaranteeType $_freightGuarantee the FreightGuarantee
	 * @return MytestEnumFreightGuaranteeType
	 */
	public function setFreightGuarantee($_freightGuarantee)
	{
		if(!MytestEnumFreightGuaranteeType::valueIsValid($_freightGuarantee))
		{
			return false;
		}
		return ($this->FreightGuarantee = $_freightGuarantee);
	}
	/**
	 * Get SmartPostHubId value
	 * @return string|null
	 */
	public function getSmartPostHubId()
	{
		return $this->SmartPostHubId;
	}
	/**
	 * Set SmartPostHubId value
	 * @param string $_smartPostHubId the SmartPostHubId
	 * @return string
	 */
	public function setSmartPostHubId($_smartPostHubId)
	{
		return ($this->SmartPostHubId = $_smartPostHubId);
	}
	/**
	 * Get SmartPostIndicia value
	 * @return MytestEnumSmartPostIndiciaType|null
	 */
	public function getSmartPostIndicia()
	{
		return $this->SmartPostIndicia;
	}
	/**
	 * Set SmartPostIndicia value
	 * @uses MytestEnumSmartPostIndiciaType::valueIsValid()
	 * @param MytestEnumSmartPostIndiciaType $_smartPostIndicia the SmartPostIndicia
	 * @return MytestEnumSmartPostIndiciaType
	 */
	public function setSmartPostIndicia($_smartPostIndicia)
	{
		if(!MytestEnumSmartPostIndiciaType::valueIsValid($_smartPostIndicia))
		{
			return false;
		}
		return ($this->SmartPostIndicia = $_smartPostIndicia);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructServiceSubOptionDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>