<?php
/**
 * File for class MytestEnumServiceType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumServiceType originally named ServiceType
 * Documentation : Identifies the FedEx service to use in shipping the package. See ServiceType for list of valid enumerated values.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumServiceType extends MytestWsdlClass
{
	/**
	 * Constant for value 'EUROPE_FIRST_INTERNATIONAL_PRIORITY'
	 * @return string 'EUROPE_FIRST_INTERNATIONAL_PRIORITY'
	 */
	const VALUE_EUROPE_FIRST_INTERNATIONAL_PRIORITY = 'EUROPE_FIRST_INTERNATIONAL_PRIORITY';
	/**
	 * Constant for value 'FEDEX_1_DAY_FREIGHT'
	 * @return string 'FEDEX_1_DAY_FREIGHT'
	 */
	const VALUE_FEDEX_1_DAY_FREIGHT = 'FEDEX_1_DAY_FREIGHT';
	/**
	 * Constant for value 'FEDEX_2_DAY'
	 * @return string 'FEDEX_2_DAY'
	 */
	const VALUE_FEDEX_2_DAY = 'FEDEX_2_DAY';
	/**
	 * Constant for value 'FEDEX_2_DAY_AM'
	 * @return string 'FEDEX_2_DAY_AM'
	 */
	const VALUE_FEDEX_2_DAY_AM = 'FEDEX_2_DAY_AM';
	/**
	 * Constant for value 'FEDEX_2_DAY_FREIGHT'
	 * @return string 'FEDEX_2_DAY_FREIGHT'
	 */
	const VALUE_FEDEX_2_DAY_FREIGHT = 'FEDEX_2_DAY_FREIGHT';
	/**
	 * Constant for value 'FEDEX_3_DAY_FREIGHT'
	 * @return string 'FEDEX_3_DAY_FREIGHT'
	 */
	const VALUE_FEDEX_3_DAY_FREIGHT = 'FEDEX_3_DAY_FREIGHT';
	/**
	 * Constant for value 'FEDEX_EXPRESS_SAVER'
	 * @return string 'FEDEX_EXPRESS_SAVER'
	 */
	const VALUE_FEDEX_EXPRESS_SAVER = 'FEDEX_EXPRESS_SAVER';
	/**
	 * Constant for value 'FEDEX_FIRST_FREIGHT'
	 * @return string 'FEDEX_FIRST_FREIGHT'
	 */
	const VALUE_FEDEX_FIRST_FREIGHT = 'FEDEX_FIRST_FREIGHT';
	/**
	 * Constant for value 'FEDEX_FREIGHT_ECONOMY'
	 * @return string 'FEDEX_FREIGHT_ECONOMY'
	 */
	const VALUE_FEDEX_FREIGHT_ECONOMY = 'FEDEX_FREIGHT_ECONOMY';
	/**
	 * Constant for value 'FEDEX_FREIGHT_PRIORITY'
	 * @return string 'FEDEX_FREIGHT_PRIORITY'
	 */
	const VALUE_FEDEX_FREIGHT_PRIORITY = 'FEDEX_FREIGHT_PRIORITY';
	/**
	 * Constant for value 'FEDEX_GROUND'
	 * @return string 'FEDEX_GROUND'
	 */
	const VALUE_FEDEX_GROUND = 'FEDEX_GROUND';
	/**
	 * Constant for value 'FIRST_OVERNIGHT'
	 * @return string 'FIRST_OVERNIGHT'
	 */
	const VALUE_FIRST_OVERNIGHT = 'FIRST_OVERNIGHT';
	/**
	 * Constant for value 'GROUND_HOME_DELIVERY'
	 * @return string 'GROUND_HOME_DELIVERY'
	 */
	const VALUE_GROUND_HOME_DELIVERY = 'GROUND_HOME_DELIVERY';
	/**
	 * Constant for value 'INTERNATIONAL_ECONOMY'
	 * @return string 'INTERNATIONAL_ECONOMY'
	 */
	const VALUE_INTERNATIONAL_ECONOMY = 'INTERNATIONAL_ECONOMY';
	/**
	 * Constant for value 'INTERNATIONAL_ECONOMY_FREIGHT'
	 * @return string 'INTERNATIONAL_ECONOMY_FREIGHT'
	 */
	const VALUE_INTERNATIONAL_ECONOMY_FREIGHT = 'INTERNATIONAL_ECONOMY_FREIGHT';
	/**
	 * Constant for value 'INTERNATIONAL_FIRST'
	 * @return string 'INTERNATIONAL_FIRST'
	 */
	const VALUE_INTERNATIONAL_FIRST = 'INTERNATIONAL_FIRST';
	/**
	 * Constant for value 'INTERNATIONAL_PRIORITY'
	 * @return string 'INTERNATIONAL_PRIORITY'
	 */
	const VALUE_INTERNATIONAL_PRIORITY = 'INTERNATIONAL_PRIORITY';
	/**
	 * Constant for value 'INTERNATIONAL_PRIORITY_FREIGHT'
	 * @return string 'INTERNATIONAL_PRIORITY_FREIGHT'
	 */
	const VALUE_INTERNATIONAL_PRIORITY_FREIGHT = 'INTERNATIONAL_PRIORITY_FREIGHT';
	/**
	 * Constant for value 'PRIORITY_OVERNIGHT'
	 * @return string 'PRIORITY_OVERNIGHT'
	 */
	const VALUE_PRIORITY_OVERNIGHT = 'PRIORITY_OVERNIGHT';
	/**
	 * Constant for value 'SMART_POST'
	 * @return string 'SMART_POST'
	 */
	const VALUE_SMART_POST = 'SMART_POST';
	/**
	 * Constant for value 'STANDARD_OVERNIGHT'
	 * @return string 'STANDARD_OVERNIGHT'
	 */
	const VALUE_STANDARD_OVERNIGHT = 'STANDARD_OVERNIGHT';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumServiceType::VALUE_EUROPE_FIRST_INTERNATIONAL_PRIORITY
	 * @uses MytestEnumServiceType::VALUE_FEDEX_1_DAY_FREIGHT
	 * @uses MytestEnumServiceType::VALUE_FEDEX_2_DAY
	 * @uses MytestEnumServiceType::VALUE_FEDEX_2_DAY_AM
	 * @uses MytestEnumServiceType::VALUE_FEDEX_2_DAY_FREIGHT
	 * @uses MytestEnumServiceType::VALUE_FEDEX_3_DAY_FREIGHT
	 * @uses MytestEnumServiceType::VALUE_FEDEX_EXPRESS_SAVER
	 * @uses MytestEnumServiceType::VALUE_FEDEX_FIRST_FREIGHT
	 * @uses MytestEnumServiceType::VALUE_FEDEX_FREIGHT_ECONOMY
	 * @uses MytestEnumServiceType::VALUE_FEDEX_FREIGHT_PRIORITY
	 * @uses MytestEnumServiceType::VALUE_FEDEX_GROUND
	 * @uses MytestEnumServiceType::VALUE_FIRST_OVERNIGHT
	 * @uses MytestEnumServiceType::VALUE_GROUND_HOME_DELIVERY
	 * @uses MytestEnumServiceType::VALUE_INTERNATIONAL_ECONOMY
	 * @uses MytestEnumServiceType::VALUE_INTERNATIONAL_ECONOMY_FREIGHT
	 * @uses MytestEnumServiceType::VALUE_INTERNATIONAL_FIRST
	 * @uses MytestEnumServiceType::VALUE_INTERNATIONAL_PRIORITY
	 * @uses MytestEnumServiceType::VALUE_INTERNATIONAL_PRIORITY_FREIGHT
	 * @uses MytestEnumServiceType::VALUE_PRIORITY_OVERNIGHT
	 * @uses MytestEnumServiceType::VALUE_SMART_POST
	 * @uses MytestEnumServiceType::VALUE_STANDARD_OVERNIGHT
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumServiceType::VALUE_EUROPE_FIRST_INTERNATIONAL_PRIORITY,MytestEnumServiceType::VALUE_FEDEX_1_DAY_FREIGHT,MytestEnumServiceType::VALUE_FEDEX_2_DAY,MytestEnumServiceType::VALUE_FEDEX_2_DAY_AM,MytestEnumServiceType::VALUE_FEDEX_2_DAY_FREIGHT,MytestEnumServiceType::VALUE_FEDEX_3_DAY_FREIGHT,MytestEnumServiceType::VALUE_FEDEX_EXPRESS_SAVER,MytestEnumServiceType::VALUE_FEDEX_FIRST_FREIGHT,MytestEnumServiceType::VALUE_FEDEX_FREIGHT_ECONOMY,MytestEnumServiceType::VALUE_FEDEX_FREIGHT_PRIORITY,MytestEnumServiceType::VALUE_FEDEX_GROUND,MytestEnumServiceType::VALUE_FIRST_OVERNIGHT,MytestEnumServiceType::VALUE_GROUND_HOME_DELIVERY,MytestEnumServiceType::VALUE_INTERNATIONAL_ECONOMY,MytestEnumServiceType::VALUE_INTERNATIONAL_ECONOMY_FREIGHT,MytestEnumServiceType::VALUE_INTERNATIONAL_FIRST,MytestEnumServiceType::VALUE_INTERNATIONAL_PRIORITY,MytestEnumServiceType::VALUE_INTERNATIONAL_PRIORITY_FREIGHT,MytestEnumServiceType::VALUE_PRIORITY_OVERNIGHT,MytestEnumServiceType::VALUE_SMART_POST,MytestEnumServiceType::VALUE_STANDARD_OVERNIGHT));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>