<?php
/**
 * File for class MytestEnumServiceOptionType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumServiceOptionType originally named ServiceOptionType
 * Documentation : These values control the optional features of service that may be combined in a commitment/rate comparision transaction.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumServiceOptionType extends MytestWsdlClass
{
	/**
	 * Constant for value 'FREIGHT_GUARANTEE'
	 * @return string 'FREIGHT_GUARANTEE'
	 */
	const VALUE_FREIGHT_GUARANTEE = 'FREIGHT_GUARANTEE';
	/**
	 * Constant for value 'SATURDAY_DELIVERY'
	 * @return string 'SATURDAY_DELIVERY'
	 */
	const VALUE_SATURDAY_DELIVERY = 'SATURDAY_DELIVERY';
	/**
	 * Constant for value 'SMART_POST_ALLOWED_INDICIA'
	 * @return string 'SMART_POST_ALLOWED_INDICIA'
	 */
	const VALUE_SMART_POST_ALLOWED_INDICIA = 'SMART_POST_ALLOWED_INDICIA';
	/**
	 * Constant for value 'SMART_POST_HUB_ID'
	 * @return string 'SMART_POST_HUB_ID'
	 */
	const VALUE_SMART_POST_HUB_ID = 'SMART_POST_HUB_ID';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumServiceOptionType::VALUE_FREIGHT_GUARANTEE
	 * @uses MytestEnumServiceOptionType::VALUE_SATURDAY_DELIVERY
	 * @uses MytestEnumServiceOptionType::VALUE_SMART_POST_ALLOWED_INDICIA
	 * @uses MytestEnumServiceOptionType::VALUE_SMART_POST_HUB_ID
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumServiceOptionType::VALUE_FREIGHT_GUARANTEE,MytestEnumServiceOptionType::VALUE_SATURDAY_DELIVERY,MytestEnumServiceOptionType::VALUE_SMART_POST_ALLOWED_INDICIA,MytestEnumServiceOptionType::VALUE_SMART_POST_HUB_ID));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>