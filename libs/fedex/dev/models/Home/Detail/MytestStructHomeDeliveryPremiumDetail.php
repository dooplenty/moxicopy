<?php
/**
 * File for class MytestStructHomeDeliveryPremiumDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructHomeDeliveryPremiumDetail originally named HomeDeliveryPremiumDetail
 * Documentation : The descriptive data required by FedEx for home delivery services.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructHomeDeliveryPremiumDetail extends MytestWsdlClass
{
	/**
	 * The HomeDeliveryPremiumType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 1
	 * @var MytestEnumHomeDeliveryPremiumType
	 */
	public $HomeDeliveryPremiumType;
	/**
	 * The Date
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var date
	 */
	public $Date;
	/**
	 * The PhoneNumber
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $PhoneNumber;
	/**
	 * Constructor method for HomeDeliveryPremiumDetail
	 * @see parent::__construct()
	 * @param MytestEnumHomeDeliveryPremiumType $_homeDeliveryPremiumType
	 * @param date $_date
	 * @param string $_phoneNumber
	 * @return MytestStructHomeDeliveryPremiumDetail
	 */
	public function __construct($_homeDeliveryPremiumType,$_date = NULL,$_phoneNumber = NULL)
	{
		parent::__construct(array('HomeDeliveryPremiumType'=>$_homeDeliveryPremiumType,'Date'=>$_date,'PhoneNumber'=>$_phoneNumber));
	}
	/**
	 * Get HomeDeliveryPremiumType value
	 * @return MytestEnumHomeDeliveryPremiumType
	 */
	public function getHomeDeliveryPremiumType()
	{
		return $this->HomeDeliveryPremiumType;
	}
	/**
	 * Set HomeDeliveryPremiumType value
	 * @uses MytestEnumHomeDeliveryPremiumType::valueIsValid()
	 * @param MytestEnumHomeDeliveryPremiumType $_homeDeliveryPremiumType the HomeDeliveryPremiumType
	 * @return MytestEnumHomeDeliveryPremiumType
	 */
	public function setHomeDeliveryPremiumType($_homeDeliveryPremiumType)
	{
		if(!MytestEnumHomeDeliveryPremiumType::valueIsValid($_homeDeliveryPremiumType))
		{
			return false;
		}
		return ($this->HomeDeliveryPremiumType = $_homeDeliveryPremiumType);
	}
	/**
	 * Get Date value
	 * @return date|null
	 */
	public function getDate()
	{
		return $this->Date;
	}
	/**
	 * Set Date value
	 * @param date $_date the Date
	 * @return date
	 */
	public function setDate($_date)
	{
		return ($this->Date = $_date);
	}
	/**
	 * Get PhoneNumber value
	 * @return string|null
	 */
	public function getPhoneNumber()
	{
		return $this->PhoneNumber;
	}
	/**
	 * Set PhoneNumber value
	 * @param string $_phoneNumber the PhoneNumber
	 * @return string
	 */
	public function setPhoneNumber($_phoneNumber)
	{
		return ($this->PhoneNumber = $_phoneNumber);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructHomeDeliveryPremiumDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>