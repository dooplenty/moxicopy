<?php
/**
 * File for class MytestEnumHomeDeliveryPremiumType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumHomeDeliveryPremiumType originally named HomeDeliveryPremiumType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumHomeDeliveryPremiumType extends MytestWsdlClass
{
	/**
	 * Constant for value 'APPOINTMENT'
	 * @return string 'APPOINTMENT'
	 */
	const VALUE_APPOINTMENT = 'APPOINTMENT';
	/**
	 * Constant for value 'DATE_CERTAIN'
	 * @return string 'DATE_CERTAIN'
	 */
	const VALUE_DATE_CERTAIN = 'DATE_CERTAIN';
	/**
	 * Constant for value 'EVENING'
	 * @return string 'EVENING'
	 */
	const VALUE_EVENING = 'EVENING';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumHomeDeliveryPremiumType::VALUE_APPOINTMENT
	 * @uses MytestEnumHomeDeliveryPremiumType::VALUE_DATE_CERTAIN
	 * @uses MytestEnumHomeDeliveryPremiumType::VALUE_EVENING
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumHomeDeliveryPremiumType::VALUE_APPOINTMENT,MytestEnumHomeDeliveryPremiumType::VALUE_DATE_CERTAIN,MytestEnumHomeDeliveryPremiumType::VALUE_EVENING));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>