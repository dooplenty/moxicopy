<?php
/**
 * File for class MytestStructDocTabContentBarcoded
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructDocTabContentBarcoded originally named DocTabContentBarcoded
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructDocTabContentBarcoded extends MytestWsdlClass
{
	/**
	 * The Symbology
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumBarcodeSymbologyType
	 */
	public $Symbology;
	/**
	 * The Specification
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructDocTabZoneSpecification
	 */
	public $Specification;
	/**
	 * Constructor method for DocTabContentBarcoded
	 * @see parent::__construct()
	 * @param MytestEnumBarcodeSymbologyType $_symbology
	 * @param MytestStructDocTabZoneSpecification $_specification
	 * @return MytestStructDocTabContentBarcoded
	 */
	public function __construct($_symbology = NULL,$_specification = NULL)
	{
		parent::__construct(array('Symbology'=>$_symbology,'Specification'=>$_specification));
	}
	/**
	 * Get Symbology value
	 * @return MytestEnumBarcodeSymbologyType|null
	 */
	public function getSymbology()
	{
		return $this->Symbology;
	}
	/**
	 * Set Symbology value
	 * @uses MytestEnumBarcodeSymbologyType::valueIsValid()
	 * @param MytestEnumBarcodeSymbologyType $_symbology the Symbology
	 * @return MytestEnumBarcodeSymbologyType
	 */
	public function setSymbology($_symbology)
	{
		if(!MytestEnumBarcodeSymbologyType::valueIsValid($_symbology))
		{
			return false;
		}
		return ($this->Symbology = $_symbology);
	}
	/**
	 * Get Specification value
	 * @return MytestStructDocTabZoneSpecification|null
	 */
	public function getSpecification()
	{
		return $this->Specification;
	}
	/**
	 * Set Specification value
	 * @param MytestStructDocTabZoneSpecification $_specification the Specification
	 * @return MytestStructDocTabZoneSpecification
	 */
	public function setSpecification($_specification)
	{
		return ($this->Specification = $_specification);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructDocTabContentBarcoded
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>