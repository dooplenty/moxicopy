<?php
/**
 * File for class MytestStructDocTabContent
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructDocTabContent originally named DocTabContent
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructDocTabContent extends MytestWsdlClass
{
	/**
	 * The DocTabContentType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumDocTabContentType
	 */
	public $DocTabContentType;
	/**
	 * The Zone001
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructDocTabContentZone001
	 */
	public $Zone001;
	/**
	 * The Barcoded
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructDocTabContentBarcoded
	 */
	public $Barcoded;
	/**
	 * Constructor method for DocTabContent
	 * @see parent::__construct()
	 * @param MytestEnumDocTabContentType $_docTabContentType
	 * @param MytestStructDocTabContentZone001 $_zone001
	 * @param MytestStructDocTabContentBarcoded $_barcoded
	 * @return MytestStructDocTabContent
	 */
	public function __construct($_docTabContentType = NULL,$_zone001 = NULL,$_barcoded = NULL)
	{
		parent::__construct(array('DocTabContentType'=>$_docTabContentType,'Zone001'=>$_zone001,'Barcoded'=>$_barcoded));
	}
	/**
	 * Get DocTabContentType value
	 * @return MytestEnumDocTabContentType|null
	 */
	public function getDocTabContentType()
	{
		return $this->DocTabContentType;
	}
	/**
	 * Set DocTabContentType value
	 * @uses MytestEnumDocTabContentType::valueIsValid()
	 * @param MytestEnumDocTabContentType $_docTabContentType the DocTabContentType
	 * @return MytestEnumDocTabContentType
	 */
	public function setDocTabContentType($_docTabContentType)
	{
		if(!MytestEnumDocTabContentType::valueIsValid($_docTabContentType))
		{
			return false;
		}
		return ($this->DocTabContentType = $_docTabContentType);
	}
	/**
	 * Get Zone001 value
	 * @return MytestStructDocTabContentZone001|null
	 */
	public function getZone001()
	{
		return $this->Zone001;
	}
	/**
	 * Set Zone001 value
	 * @param MytestStructDocTabContentZone001 $_zone001 the Zone001
	 * @return MytestStructDocTabContentZone001
	 */
	public function setZone001($_zone001)
	{
		return ($this->Zone001 = $_zone001);
	}
	/**
	 * Get Barcoded value
	 * @return MytestStructDocTabContentBarcoded|null
	 */
	public function getBarcoded()
	{
		return $this->Barcoded;
	}
	/**
	 * Set Barcoded value
	 * @param MytestStructDocTabContentBarcoded $_barcoded the Barcoded
	 * @return MytestStructDocTabContentBarcoded
	 */
	public function setBarcoded($_barcoded)
	{
		return ($this->Barcoded = $_barcoded);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructDocTabContent
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>