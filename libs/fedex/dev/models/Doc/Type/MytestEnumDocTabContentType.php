<?php
/**
 * File for class MytestEnumDocTabContentType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumDocTabContentType originally named DocTabContentType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumDocTabContentType extends MytestWsdlClass
{
	/**
	 * Constant for value 'BARCODED'
	 * @return string 'BARCODED'
	 */
	const VALUE_BARCODED = 'BARCODED';
	/**
	 * Constant for value 'MINIMUM'
	 * @return string 'MINIMUM'
	 */
	const VALUE_MINIMUM = 'MINIMUM';
	/**
	 * Constant for value 'STANDARD'
	 * @return string 'STANDARD'
	 */
	const VALUE_STANDARD = 'STANDARD';
	/**
	 * Constant for value 'ZONE001'
	 * @return string 'ZONE001'
	 */
	const VALUE_ZONE001 = 'ZONE001';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumDocTabContentType::VALUE_BARCODED
	 * @uses MytestEnumDocTabContentType::VALUE_MINIMUM
	 * @uses MytestEnumDocTabContentType::VALUE_STANDARD
	 * @uses MytestEnumDocTabContentType::VALUE_ZONE001
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumDocTabContentType::VALUE_BARCODED,MytestEnumDocTabContentType::VALUE_MINIMUM,MytestEnumDocTabContentType::VALUE_STANDARD,MytestEnumDocTabContentType::VALUE_ZONE001));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>