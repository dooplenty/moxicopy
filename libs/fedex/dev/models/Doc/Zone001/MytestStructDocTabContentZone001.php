<?php
/**
 * File for class MytestStructDocTabContentZone001
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructDocTabContentZone001 originally named DocTabContentZone001
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructDocTabContentZone001 extends MytestWsdlClass
{
	/**
	 * The DocTabZoneSpecifications
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : 12
	 * - minOccurs : 1
	 * @var MytestStructDocTabZoneSpecification
	 */
	public $DocTabZoneSpecifications;
	/**
	 * Constructor method for DocTabContentZone001
	 * @see parent::__construct()
	 * @param MytestStructDocTabZoneSpecification $_docTabZoneSpecifications
	 * @return MytestStructDocTabContentZone001
	 */
	public function __construct($_docTabZoneSpecifications)
	{
		parent::__construct(array('DocTabZoneSpecifications'=>$_docTabZoneSpecifications));
	}
	/**
	 * Get DocTabZoneSpecifications value
	 * @return MytestStructDocTabZoneSpecification
	 */
	public function getDocTabZoneSpecifications()
	{
		return $this->DocTabZoneSpecifications;
	}
	/**
	 * Set DocTabZoneSpecifications value
	 * @param MytestStructDocTabZoneSpecification $_docTabZoneSpecifications the DocTabZoneSpecifications
	 * @return MytestStructDocTabZoneSpecification
	 */
	public function setDocTabZoneSpecifications($_docTabZoneSpecifications)
	{
		return ($this->DocTabZoneSpecifications = $_docTabZoneSpecifications);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructDocTabContentZone001
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>