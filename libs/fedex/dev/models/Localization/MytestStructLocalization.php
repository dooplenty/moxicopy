<?php
/**
 * File for class MytestStructLocalization
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructLocalization originally named Localization
 * Documentation : Identifies the representation of human-readable text.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructLocalization extends MytestWsdlClass
{
	/**
	 * The LanguageCode
	 * Meta informations extracted from the WSDL
	 * - documentation : Two-letter code for language (e.g. EN, FR, etc.)
	 * - minOccurs : 1
	 * @var string
	 */
	public $LanguageCode;
	/**
	 * The LocaleCode
	 * Meta informations extracted from the WSDL
	 * - documentation : Two-letter code for the region (e.g. us, ca, etc..).
	 * - minOccurs : 0
	 * @var string
	 */
	public $LocaleCode;
	/**
	 * Constructor method for Localization
	 * @see parent::__construct()
	 * @param string $_languageCode
	 * @param string $_localeCode
	 * @return MytestStructLocalization
	 */
	public function __construct($_languageCode,$_localeCode = NULL)
	{
		parent::__construct(array('LanguageCode'=>$_languageCode,'LocaleCode'=>$_localeCode));
	}
	/**
	 * Get LanguageCode value
	 * @return string
	 */
	public function getLanguageCode()
	{
		return $this->LanguageCode;
	}
	/**
	 * Set LanguageCode value
	 * @param string $_languageCode the LanguageCode
	 * @return string
	 */
	public function setLanguageCode($_languageCode)
	{
		return ($this->LanguageCode = $_languageCode);
	}
	/**
	 * Get LocaleCode value
	 * @return string|null
	 */
	public function getLocaleCode()
	{
		return $this->LocaleCode;
	}
	/**
	 * Set LocaleCode value
	 * @param string $_localeCode the LocaleCode
	 * @return string
	 */
	public function setLocaleCode($_localeCode)
	{
		return ($this->LocaleCode = $_localeCode);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructLocalization
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>