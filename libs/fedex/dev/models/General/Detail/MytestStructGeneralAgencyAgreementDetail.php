<?php
/**
 * File for class MytestStructGeneralAgencyAgreementDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructGeneralAgencyAgreementDetail originally named GeneralAgencyAgreementDetail
 * Documentation : Data required to produce a General Agency Agreement document. Remaining content (business data) to be defined once requirements have been completed.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructGeneralAgencyAgreementDetail extends MytestWsdlClass
{
	/**
	 * The Format
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructShippingDocumentFormat
	 */
	public $Format;
	/**
	 * Constructor method for GeneralAgencyAgreementDetail
	 * @see parent::__construct()
	 * @param MytestStructShippingDocumentFormat $_format
	 * @return MytestStructGeneralAgencyAgreementDetail
	 */
	public function __construct($_format = NULL)
	{
		parent::__construct(array('Format'=>$_format));
	}
	/**
	 * Get Format value
	 * @return MytestStructShippingDocumentFormat|null
	 */
	public function getFormat()
	{
		return $this->Format;
	}
	/**
	 * Set Format value
	 * @param MytestStructShippingDocumentFormat $_format the Format
	 * @return MytestStructShippingDocumentFormat
	 */
	public function setFormat($_format)
	{
		return ($this->Format = $_format);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructGeneralAgencyAgreementDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>