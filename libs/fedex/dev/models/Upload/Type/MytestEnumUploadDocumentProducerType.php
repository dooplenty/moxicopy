<?php
/**
 * File for class MytestEnumUploadDocumentProducerType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumUploadDocumentProducerType originally named UploadDocumentProducerType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumUploadDocumentProducerType extends MytestWsdlClass
{
	/**
	 * Constant for value 'CUSTOMER'
	 * @return string 'CUSTOMER'
	 */
	const VALUE_CUSTOMER = 'CUSTOMER';
	/**
	 * Constant for value 'FEDEX_CLS'
	 * @return string 'FEDEX_CLS'
	 */
	const VALUE_FEDEX_CLS = 'FEDEX_CLS';
	/**
	 * Constant for value 'FEDEX_GTM'
	 * @return string 'FEDEX_GTM'
	 */
	const VALUE_FEDEX_GTM = 'FEDEX_GTM';
	/**
	 * Constant for value 'OTHER'
	 * @return string 'OTHER'
	 */
	const VALUE_OTHER = 'OTHER';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumUploadDocumentProducerType::VALUE_CUSTOMER
	 * @uses MytestEnumUploadDocumentProducerType::VALUE_FEDEX_CLS
	 * @uses MytestEnumUploadDocumentProducerType::VALUE_FEDEX_GTM
	 * @uses MytestEnumUploadDocumentProducerType::VALUE_OTHER
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumUploadDocumentProducerType::VALUE_CUSTOMER,MytestEnumUploadDocumentProducerType::VALUE_FEDEX_CLS,MytestEnumUploadDocumentProducerType::VALUE_FEDEX_GTM,MytestEnumUploadDocumentProducerType::VALUE_OTHER));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>