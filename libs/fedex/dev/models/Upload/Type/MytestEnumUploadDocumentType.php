<?php
/**
 * File for class MytestEnumUploadDocumentType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumUploadDocumentType originally named UploadDocumentType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumUploadDocumentType extends MytestWsdlClass
{
	/**
	 * Constant for value 'CERTIFICATE_OF_ORIGIN'
	 * @return string 'CERTIFICATE_OF_ORIGIN'
	 */
	const VALUE_CERTIFICATE_OF_ORIGIN = 'CERTIFICATE_OF_ORIGIN';
	/**
	 * Constant for value 'COMMERCIAL_INVOICE'
	 * @return string 'COMMERCIAL_INVOICE'
	 */
	const VALUE_COMMERCIAL_INVOICE = 'COMMERCIAL_INVOICE';
	/**
	 * Constant for value 'ETD_LABEL'
	 * @return string 'ETD_LABEL'
	 */
	const VALUE_ETD_LABEL = 'ETD_LABEL';
	/**
	 * Constant for value 'NAFTA_CERTIFICATE_OF_ORIGIN'
	 * @return string 'NAFTA_CERTIFICATE_OF_ORIGIN'
	 */
	const VALUE_NAFTA_CERTIFICATE_OF_ORIGIN = 'NAFTA_CERTIFICATE_OF_ORIGIN';
	/**
	 * Constant for value 'OTHER'
	 * @return string 'OTHER'
	 */
	const VALUE_OTHER = 'OTHER';
	/**
	 * Constant for value 'PRO_FORMA_INVOICE'
	 * @return string 'PRO_FORMA_INVOICE'
	 */
	const VALUE_PRO_FORMA_INVOICE = 'PRO_FORMA_INVOICE';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumUploadDocumentType::VALUE_CERTIFICATE_OF_ORIGIN
	 * @uses MytestEnumUploadDocumentType::VALUE_COMMERCIAL_INVOICE
	 * @uses MytestEnumUploadDocumentType::VALUE_ETD_LABEL
	 * @uses MytestEnumUploadDocumentType::VALUE_NAFTA_CERTIFICATE_OF_ORIGIN
	 * @uses MytestEnumUploadDocumentType::VALUE_OTHER
	 * @uses MytestEnumUploadDocumentType::VALUE_PRO_FORMA_INVOICE
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumUploadDocumentType::VALUE_CERTIFICATE_OF_ORIGIN,MytestEnumUploadDocumentType::VALUE_COMMERCIAL_INVOICE,MytestEnumUploadDocumentType::VALUE_ETD_LABEL,MytestEnumUploadDocumentType::VALUE_NAFTA_CERTIFICATE_OF_ORIGIN,MytestEnumUploadDocumentType::VALUE_OTHER,MytestEnumUploadDocumentType::VALUE_PRO_FORMA_INVOICE));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>