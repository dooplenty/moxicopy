<?php
/**
 * File for class MytestEnumUploadDocumentIdProducer
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumUploadDocumentIdProducer originally named UploadDocumentIdProducer
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumUploadDocumentIdProducer extends MytestWsdlClass
{
	/**
	 * Constant for value 'CUSTOMER'
	 * @return string 'CUSTOMER'
	 */
	const VALUE_CUSTOMER = 'CUSTOMER';
	/**
	 * Constant for value 'FEDEX_CSHP'
	 * @return string 'FEDEX_CSHP'
	 */
	const VALUE_FEDEX_CSHP = 'FEDEX_CSHP';
	/**
	 * Constant for value 'FEDEX_GTM'
	 * @return string 'FEDEX_GTM'
	 */
	const VALUE_FEDEX_GTM = 'FEDEX_GTM';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumUploadDocumentIdProducer::VALUE_CUSTOMER
	 * @uses MytestEnumUploadDocumentIdProducer::VALUE_FEDEX_CSHP
	 * @uses MytestEnumUploadDocumentIdProducer::VALUE_FEDEX_GTM
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumUploadDocumentIdProducer::VALUE_CUSTOMER,MytestEnumUploadDocumentIdProducer::VALUE_FEDEX_CSHP,MytestEnumUploadDocumentIdProducer::VALUE_FEDEX_GTM));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>