<?php
/**
 * File for class MytestStructUploadDocumentDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructUploadDocumentDetail originally named UploadDocumentDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructUploadDocumentDetail extends MytestWsdlClass
{
	/**
	 * The LineNumber
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $LineNumber;
	/**
	 * The CustomerReference
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $CustomerReference;
	/**
	 * The DocumentProducer
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumUploadDocumentProducerType
	 */
	public $DocumentProducer;
	/**
	 * The DocumentType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumUploadDocumentType
	 */
	public $DocumentType;
	/**
	 * The FileName
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $FileName;
	/**
	 * The DocumentContent
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var base64Binary
	 */
	public $DocumentContent;
	/**
	 * Constructor method for UploadDocumentDetail
	 * @see parent::__construct()
	 * @param nonNegativeInteger $_lineNumber
	 * @param string $_customerReference
	 * @param MytestEnumUploadDocumentProducerType $_documentProducer
	 * @param MytestEnumUploadDocumentType $_documentType
	 * @param string $_fileName
	 * @param base64Binary $_documentContent
	 * @return MytestStructUploadDocumentDetail
	 */
	public function __construct($_lineNumber = NULL,$_customerReference = NULL,$_documentProducer = NULL,$_documentType = NULL,$_fileName = NULL,$_documentContent = NULL)
	{
		parent::__construct(array('LineNumber'=>$_lineNumber,'CustomerReference'=>$_customerReference,'DocumentProducer'=>$_documentProducer,'DocumentType'=>$_documentType,'FileName'=>$_fileName,'DocumentContent'=>$_documentContent));
	}
	/**
	 * Get LineNumber value
	 * @return nonNegativeInteger|null
	 */
	public function getLineNumber()
	{
		return $this->LineNumber;
	}
	/**
	 * Set LineNumber value
	 * @param nonNegativeInteger $_lineNumber the LineNumber
	 * @return nonNegativeInteger
	 */
	public function setLineNumber($_lineNumber)
	{
		return ($this->LineNumber = $_lineNumber);
	}
	/**
	 * Get CustomerReference value
	 * @return string|null
	 */
	public function getCustomerReference()
	{
		return $this->CustomerReference;
	}
	/**
	 * Set CustomerReference value
	 * @param string $_customerReference the CustomerReference
	 * @return string
	 */
	public function setCustomerReference($_customerReference)
	{
		return ($this->CustomerReference = $_customerReference);
	}
	/**
	 * Get DocumentProducer value
	 * @return MytestEnumUploadDocumentProducerType|null
	 */
	public function getDocumentProducer()
	{
		return $this->DocumentProducer;
	}
	/**
	 * Set DocumentProducer value
	 * @uses MytestEnumUploadDocumentProducerType::valueIsValid()
	 * @param MytestEnumUploadDocumentProducerType $_documentProducer the DocumentProducer
	 * @return MytestEnumUploadDocumentProducerType
	 */
	public function setDocumentProducer($_documentProducer)
	{
		if(!MytestEnumUploadDocumentProducerType::valueIsValid($_documentProducer))
		{
			return false;
		}
		return ($this->DocumentProducer = $_documentProducer);
	}
	/**
	 * Get DocumentType value
	 * @return MytestEnumUploadDocumentType|null
	 */
	public function getDocumentType()
	{
		return $this->DocumentType;
	}
	/**
	 * Set DocumentType value
	 * @uses MytestEnumUploadDocumentType::valueIsValid()
	 * @param MytestEnumUploadDocumentType $_documentType the DocumentType
	 * @return MytestEnumUploadDocumentType
	 */
	public function setDocumentType($_documentType)
	{
		if(!MytestEnumUploadDocumentType::valueIsValid($_documentType))
		{
			return false;
		}
		return ($this->DocumentType = $_documentType);
	}
	/**
	 * Get FileName value
	 * @return string|null
	 */
	public function getFileName()
	{
		return $this->FileName;
	}
	/**
	 * Set FileName value
	 * @param string $_fileName the FileName
	 * @return string
	 */
	public function setFileName($_fileName)
	{
		return ($this->FileName = $_fileName);
	}
	/**
	 * Get DocumentContent value
	 * @return base64Binary|null
	 */
	public function getDocumentContent()
	{
		return $this->DocumentContent;
	}
	/**
	 * Set DocumentContent value
	 * @param base64Binary $_documentContent the DocumentContent
	 * @return base64Binary
	 */
	public function setDocumentContent($_documentContent)
	{
		return ($this->DocumentContent = $_documentContent);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructUploadDocumentDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>