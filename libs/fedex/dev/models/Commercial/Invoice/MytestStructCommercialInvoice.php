<?php
/**
 * File for class MytestStructCommercialInvoice
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCommercialInvoice originally named CommercialInvoice
 * Documentation : CommercialInvoice element is required for electronic upload of CI data. It will serve to create/transmit an Electronic Commercial Invoice through the FedEx Systems. Customers are responsible for printing their own Commercial Invoice.If you would likeFedEx to generate a Commercial Invoice and transmit it to Customs. for clearance purposes, you need to specify that in the ShippingDocumentSpecification element. If you would like a copy of the Commercial Invoice that FedEx generated returned to you in reply it needs to be specified in the ETDDetail/RequestedDocumentCopies element. Commercial Invoice support consists of maximum of 99 commodity line items.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCommercialInvoice extends MytestWsdlClass
{
	/**
	 * The Comments
	 * Meta informations extracted from the WSDL
	 * - documentation : Any comments that need to be communicated about this shipment.
	 * - maxOccurs : 99
	 * - minOccurs : 0
	 * @var string
	 */
	public $Comments;
	/**
	 * The FreightCharge
	 * Meta informations extracted from the WSDL
	 * - documentation : Any freight charges that are associated with this shipment.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $FreightCharge;
	/**
	 * The TaxesOrMiscellaneousCharge
	 * Meta informations extracted from the WSDL
	 * - documentation : Any taxes or miscellaneous charges(other than Freight charges or Insurance charges) that are associated with this shipment.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TaxesOrMiscellaneousCharge;
	/**
	 * The TaxesOrMiscellaneousChargeType
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies which kind of charge is being recorded in the preceding field.
	 * - minOccurs : 0
	 * @var MytestEnumTaxesOrMiscellaneousChargeType
	 */
	public $TaxesOrMiscellaneousChargeType;
	/**
	 * The PackingCosts
	 * Meta informations extracted from the WSDL
	 * - documentation : Any packing costs that are associated with this shipment.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $PackingCosts;
	/**
	 * The HandlingCosts
	 * Meta informations extracted from the WSDL
	 * - documentation : Any handling costs that are associated with this shipment.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $HandlingCosts;
	/**
	 * The SpecialInstructions
	 * Meta informations extracted from the WSDL
	 * - documentation : Free-form text.
	 * - minOccurs : 0
	 * @var string
	 */
	public $SpecialInstructions;
	/**
	 * The DeclarationStatement
	 * Meta informations extracted from the WSDL
	 * - documentation : Free-form text.
	 * - minOccurs : 0
	 * @var string
	 */
	public $DeclarationStatement;
	/**
	 * The PaymentTerms
	 * Meta informations extracted from the WSDL
	 * - documentation : Free-form text.
	 * - minOccurs : 0
	 * @var string
	 */
	public $PaymentTerms;
	/**
	 * The Purpose
	 * Meta informations extracted from the WSDL
	 * - documentation : The reason for the shipment. Note: SOLD is not a valid purpose for a Proforma Invoice.
	 * - minOccurs : 0
	 * @var MytestEnumPurposeOfShipmentType
	 */
	public $Purpose;
	/**
	 * The OriginatorName
	 * Meta informations extracted from the WSDL
	 * - documentation : Name of the International Expert that completed the Commercial Invoice different from Sender.
	 * - minOccurs : 0
	 * @var string
	 */
	public $OriginatorName;
	/**
	 * The TermsOfSale
	 * Meta informations extracted from the WSDL
	 * - documentation : Required for dutiable international Express or Ground shipment. This field is not applicable to an international PIB(document) or a non-document which does not require a Commercial Invoice
	 * - minOccurs : 0
	 * @var MytestEnumTermsOfSaleType
	 */
	public $TermsOfSale;
	/**
	 * Constructor method for CommercialInvoice
	 * @see parent::__construct()
	 * @param string $_comments
	 * @param MytestStructMoney $_freightCharge
	 * @param MytestStructMoney $_taxesOrMiscellaneousCharge
	 * @param MytestEnumTaxesOrMiscellaneousChargeType $_taxesOrMiscellaneousChargeType
	 * @param MytestStructMoney $_packingCosts
	 * @param MytestStructMoney $_handlingCosts
	 * @param string $_specialInstructions
	 * @param string $_declarationStatement
	 * @param string $_paymentTerms
	 * @param MytestEnumPurposeOfShipmentType $_purpose
	 * @param string $_originatorName
	 * @param MytestEnumTermsOfSaleType $_termsOfSale
	 * @return MytestStructCommercialInvoice
	 */
	public function __construct($_comments = NULL,$_freightCharge = NULL,$_taxesOrMiscellaneousCharge = NULL,$_taxesOrMiscellaneousChargeType = NULL,$_packingCosts = NULL,$_handlingCosts = NULL,$_specialInstructions = NULL,$_declarationStatement = NULL,$_paymentTerms = NULL,$_purpose = NULL,$_originatorName = NULL,$_termsOfSale = NULL)
	{
		parent::__construct(array('Comments'=>$_comments,'FreightCharge'=>$_freightCharge,'TaxesOrMiscellaneousCharge'=>$_taxesOrMiscellaneousCharge,'TaxesOrMiscellaneousChargeType'=>$_taxesOrMiscellaneousChargeType,'PackingCosts'=>$_packingCosts,'HandlingCosts'=>$_handlingCosts,'SpecialInstructions'=>$_specialInstructions,'DeclarationStatement'=>$_declarationStatement,'PaymentTerms'=>$_paymentTerms,'Purpose'=>$_purpose,'OriginatorName'=>$_originatorName,'TermsOfSale'=>$_termsOfSale));
	}
	/**
	 * Get Comments value
	 * @return string|null
	 */
	public function getComments()
	{
		return $this->Comments;
	}
	/**
	 * Set Comments value
	 * @param string $_comments the Comments
	 * @return string
	 */
	public function setComments($_comments)
	{
		return ($this->Comments = $_comments);
	}
	/**
	 * Get FreightCharge value
	 * @return MytestStructMoney|null
	 */
	public function getFreightCharge()
	{
		return $this->FreightCharge;
	}
	/**
	 * Set FreightCharge value
	 * @param MytestStructMoney $_freightCharge the FreightCharge
	 * @return MytestStructMoney
	 */
	public function setFreightCharge($_freightCharge)
	{
		return ($this->FreightCharge = $_freightCharge);
	}
	/**
	 * Get TaxesOrMiscellaneousCharge value
	 * @return MytestStructMoney|null
	 */
	public function getTaxesOrMiscellaneousCharge()
	{
		return $this->TaxesOrMiscellaneousCharge;
	}
	/**
	 * Set TaxesOrMiscellaneousCharge value
	 * @param MytestStructMoney $_taxesOrMiscellaneousCharge the TaxesOrMiscellaneousCharge
	 * @return MytestStructMoney
	 */
	public function setTaxesOrMiscellaneousCharge($_taxesOrMiscellaneousCharge)
	{
		return ($this->TaxesOrMiscellaneousCharge = $_taxesOrMiscellaneousCharge);
	}
	/**
	 * Get TaxesOrMiscellaneousChargeType value
	 * @return MytestEnumTaxesOrMiscellaneousChargeType|null
	 */
	public function getTaxesOrMiscellaneousChargeType()
	{
		return $this->TaxesOrMiscellaneousChargeType;
	}
	/**
	 * Set TaxesOrMiscellaneousChargeType value
	 * @uses MytestEnumTaxesOrMiscellaneousChargeType::valueIsValid()
	 * @param MytestEnumTaxesOrMiscellaneousChargeType $_taxesOrMiscellaneousChargeType the TaxesOrMiscellaneousChargeType
	 * @return MytestEnumTaxesOrMiscellaneousChargeType
	 */
	public function setTaxesOrMiscellaneousChargeType($_taxesOrMiscellaneousChargeType)
	{
		if(!MytestEnumTaxesOrMiscellaneousChargeType::valueIsValid($_taxesOrMiscellaneousChargeType))
		{
			return false;
		}
		return ($this->TaxesOrMiscellaneousChargeType = $_taxesOrMiscellaneousChargeType);
	}
	/**
	 * Get PackingCosts value
	 * @return MytestStructMoney|null
	 */
	public function getPackingCosts()
	{
		return $this->PackingCosts;
	}
	/**
	 * Set PackingCosts value
	 * @param MytestStructMoney $_packingCosts the PackingCosts
	 * @return MytestStructMoney
	 */
	public function setPackingCosts($_packingCosts)
	{
		return ($this->PackingCosts = $_packingCosts);
	}
	/**
	 * Get HandlingCosts value
	 * @return MytestStructMoney|null
	 */
	public function getHandlingCosts()
	{
		return $this->HandlingCosts;
	}
	/**
	 * Set HandlingCosts value
	 * @param MytestStructMoney $_handlingCosts the HandlingCosts
	 * @return MytestStructMoney
	 */
	public function setHandlingCosts($_handlingCosts)
	{
		return ($this->HandlingCosts = $_handlingCosts);
	}
	/**
	 * Get SpecialInstructions value
	 * @return string|null
	 */
	public function getSpecialInstructions()
	{
		return $this->SpecialInstructions;
	}
	/**
	 * Set SpecialInstructions value
	 * @param string $_specialInstructions the SpecialInstructions
	 * @return string
	 */
	public function setSpecialInstructions($_specialInstructions)
	{
		return ($this->SpecialInstructions = $_specialInstructions);
	}
	/**
	 * Get DeclarationStatement value
	 * @return string|null
	 */
	public function getDeclarationStatement()
	{
		return $this->DeclarationStatement;
	}
	/**
	 * Set DeclarationStatement value
	 * @param string $_declarationStatement the DeclarationStatement
	 * @return string
	 */
	public function setDeclarationStatement($_declarationStatement)
	{
		return ($this->DeclarationStatement = $_declarationStatement);
	}
	/**
	 * Get PaymentTerms value
	 * @return string|null
	 */
	public function getPaymentTerms()
	{
		return $this->PaymentTerms;
	}
	/**
	 * Set PaymentTerms value
	 * @param string $_paymentTerms the PaymentTerms
	 * @return string
	 */
	public function setPaymentTerms($_paymentTerms)
	{
		return ($this->PaymentTerms = $_paymentTerms);
	}
	/**
	 * Get Purpose value
	 * @return MytestEnumPurposeOfShipmentType|null
	 */
	public function getPurpose()
	{
		return $this->Purpose;
	}
	/**
	 * Set Purpose value
	 * @uses MytestEnumPurposeOfShipmentType::valueIsValid()
	 * @param MytestEnumPurposeOfShipmentType $_purpose the Purpose
	 * @return MytestEnumPurposeOfShipmentType
	 */
	public function setPurpose($_purpose)
	{
		if(!MytestEnumPurposeOfShipmentType::valueIsValid($_purpose))
		{
			return false;
		}
		return ($this->Purpose = $_purpose);
	}
	/**
	 * Get OriginatorName value
	 * @return string|null
	 */
	public function getOriginatorName()
	{
		return $this->OriginatorName;
	}
	/**
	 * Set OriginatorName value
	 * @param string $_originatorName the OriginatorName
	 * @return string
	 */
	public function setOriginatorName($_originatorName)
	{
		return ($this->OriginatorName = $_originatorName);
	}
	/**
	 * Get TermsOfSale value
	 * @return MytestEnumTermsOfSaleType|null
	 */
	public function getTermsOfSale()
	{
		return $this->TermsOfSale;
	}
	/**
	 * Set TermsOfSale value
	 * @uses MytestEnumTermsOfSaleType::valueIsValid()
	 * @param MytestEnumTermsOfSaleType $_termsOfSale the TermsOfSale
	 * @return MytestEnumTermsOfSaleType
	 */
	public function setTermsOfSale($_termsOfSale)
	{
		if(!MytestEnumTermsOfSaleType::valueIsValid($_termsOfSale))
		{
			return false;
		}
		return ($this->TermsOfSale = $_termsOfSale);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCommercialInvoice
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>