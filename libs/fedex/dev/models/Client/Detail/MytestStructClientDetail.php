<?php
/**
 * File for class MytestStructClientDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructClientDetail originally named ClientDetail
 * Documentation : Descriptive data for the client submitting a transaction.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructClientDetail extends MytestWsdlClass
{
	/**
	 * The AccountNumber
	 * Meta informations extracted from the WSDL
	 * - documentation : The FedEx account number associated with this transaction.
	 * - minOccurs : 1
	 * @var string
	 */
	public $AccountNumber;
	/**
	 * The MeterNumber
	 * Meta informations extracted from the WSDL
	 * - documentation : This number is assigned by FedEx and identifies the unique device from which the request is originating
	 * - minOccurs : 1
	 * @var string
	 */
	public $MeterNumber;
	/**
	 * The IntegratorId
	 * Meta informations extracted from the WSDL
	 * - documentation : Only used in transactions which require identification of the Fed Ex Office integrator.
	 * - minOccurs : 0
	 * @var string
	 */
	public $IntegratorId;
	/**
	 * The Region
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates the region from which the transaction is submitted.
	 * - minOccurs : 0
	 * @var MytestEnumExpressRegionCode
	 */
	public $Region;
	/**
	 * The Localization
	 * Meta informations extracted from the WSDL
	 * - documentation : The language to be used for human-readable Notification.localizedMessages in responses to the request containing this ClientDetail object. Different requests from the same client may contain different Localization data. (Contrast with TransactionDetail.localization, which governs data payload language/translation.)
	 * - minOccurs : 0
	 * @var MytestStructLocalization
	 */
	public $Localization;
	/**
	 * Constructor method for ClientDetail
	 * @see parent::__construct()
	 * @param string $_accountNumber
	 * @param string $_meterNumber
	 * @param string $_integratorId
	 * @param MytestEnumExpressRegionCode $_region
	 * @param MytestStructLocalization $_localization
	 * @return MytestStructClientDetail
	 */
	public function __construct($_accountNumber,$_meterNumber,$_integratorId = NULL,$_region = NULL,$_localization = NULL)
	{
		parent::__construct(array('AccountNumber'=>$_accountNumber,'MeterNumber'=>$_meterNumber,'IntegratorId'=>$_integratorId,'Region'=>$_region,'Localization'=>$_localization));
	}
	/**
	 * Get AccountNumber value
	 * @return string
	 */
	public function getAccountNumber()
	{
		return $this->AccountNumber;
	}
	/**
	 * Set AccountNumber value
	 * @param string $_accountNumber the AccountNumber
	 * @return string
	 */
	public function setAccountNumber($_accountNumber)
	{
		return ($this->AccountNumber = $_accountNumber);
	}
	/**
	 * Get MeterNumber value
	 * @return string
	 */
	public function getMeterNumber()
	{
		return $this->MeterNumber;
	}
	/**
	 * Set MeterNumber value
	 * @param string $_meterNumber the MeterNumber
	 * @return string
	 */
	public function setMeterNumber($_meterNumber)
	{
		return ($this->MeterNumber = $_meterNumber);
	}
	/**
	 * Get IntegratorId value
	 * @return string|null
	 */
	public function getIntegratorId()
	{
		return $this->IntegratorId;
	}
	/**
	 * Set IntegratorId value
	 * @param string $_integratorId the IntegratorId
	 * @return string
	 */
	public function setIntegratorId($_integratorId)
	{
		return ($this->IntegratorId = $_integratorId);
	}
	/**
	 * Get Region value
	 * @return MytestEnumExpressRegionCode|null
	 */
	public function getRegion()
	{
		return $this->Region;
	}
	/**
	 * Set Region value
	 * @uses MytestEnumExpressRegionCode::valueIsValid()
	 * @param MytestEnumExpressRegionCode $_region the Region
	 * @return MytestEnumExpressRegionCode
	 */
	public function setRegion($_region)
	{
		if(!MytestEnumExpressRegionCode::valueIsValid($_region))
		{
			return false;
		}
		return ($this->Region = $_region);
	}
	/**
	 * Get Localization value
	 * @return MytestStructLocalization|null
	 */
	public function getLocalization()
	{
		return $this->Localization;
	}
	/**
	 * Set Localization value
	 * @param MytestStructLocalization $_localization the Localization
	 * @return MytestStructLocalization
	 */
	public function setLocalization($_localization)
	{
		return ($this->Localization = $_localization);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructClientDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>