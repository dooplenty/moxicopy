<?php
/**
 * File for class MytestEnumAdditionalLabelsType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumAdditionalLabelsType originally named AdditionalLabelsType
 * Documentation : Identifies the type of additional labels.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumAdditionalLabelsType extends MytestWsdlClass
{
	/**
	 * Constant for value 'BROKER'
	 * @return string 'BROKER'
	 */
	const VALUE_BROKER = 'BROKER';
	/**
	 * Constant for value 'CONSIGNEE'
	 * @return string 'CONSIGNEE'
	 */
	const VALUE_CONSIGNEE = 'CONSIGNEE';
	/**
	 * Constant for value 'CUSTOMS'
	 * @return string 'CUSTOMS'
	 */
	const VALUE_CUSTOMS = 'CUSTOMS';
	/**
	 * Constant for value 'DESTINATION'
	 * @return string 'DESTINATION'
	 */
	const VALUE_DESTINATION = 'DESTINATION';
	/**
	 * Constant for value 'MANIFEST'
	 * @return string 'MANIFEST'
	 */
	const VALUE_MANIFEST = 'MANIFEST';
	/**
	 * Constant for value 'ORIGIN'
	 * @return string 'ORIGIN'
	 */
	const VALUE_ORIGIN = 'ORIGIN';
	/**
	 * Constant for value 'RECIPIENT'
	 * @return string 'RECIPIENT'
	 */
	const VALUE_RECIPIENT = 'RECIPIENT';
	/**
	 * Constant for value 'SHIPPER'
	 * @return string 'SHIPPER'
	 */
	const VALUE_SHIPPER = 'SHIPPER';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumAdditionalLabelsType::VALUE_BROKER
	 * @uses MytestEnumAdditionalLabelsType::VALUE_CONSIGNEE
	 * @uses MytestEnumAdditionalLabelsType::VALUE_CUSTOMS
	 * @uses MytestEnumAdditionalLabelsType::VALUE_DESTINATION
	 * @uses MytestEnumAdditionalLabelsType::VALUE_MANIFEST
	 * @uses MytestEnumAdditionalLabelsType::VALUE_ORIGIN
	 * @uses MytestEnumAdditionalLabelsType::VALUE_RECIPIENT
	 * @uses MytestEnumAdditionalLabelsType::VALUE_SHIPPER
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumAdditionalLabelsType::VALUE_BROKER,MytestEnumAdditionalLabelsType::VALUE_CONSIGNEE,MytestEnumAdditionalLabelsType::VALUE_CUSTOMS,MytestEnumAdditionalLabelsType::VALUE_DESTINATION,MytestEnumAdditionalLabelsType::VALUE_MANIFEST,MytestEnumAdditionalLabelsType::VALUE_ORIGIN,MytestEnumAdditionalLabelsType::VALUE_RECIPIENT,MytestEnumAdditionalLabelsType::VALUE_SHIPPER));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>