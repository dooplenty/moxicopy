<?php
/**
 * File for class MytestStructAdditionalLabelsDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructAdditionalLabelsDetail originally named AdditionalLabelsDetail
 * Documentation : Specifies additional labels to be produced. All required labels for shipments will be produced without the need to request additional labels. These are only available as thermal labels.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructAdditionalLabelsDetail extends MytestWsdlClass
{
	/**
	 * The Type
	 * Meta informations extracted from the WSDL
	 * - documentation : The type of additional labels to return.
	 * - minOccurs : 0
	 * @var MytestEnumAdditionalLabelsType
	 */
	public $Type;
	/**
	 * The Count
	 * Meta informations extracted from the WSDL
	 * - documentation : The number of this type label to return
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $Count;
	/**
	 * Constructor method for AdditionalLabelsDetail
	 * @see parent::__construct()
	 * @param MytestEnumAdditionalLabelsType $_type
	 * @param nonNegativeInteger $_count
	 * @return MytestStructAdditionalLabelsDetail
	 */
	public function __construct($_type = NULL,$_count = NULL)
	{
		parent::__construct(array('Type'=>$_type,'Count'=>$_count));
	}
	/**
	 * Get Type value
	 * @return MytestEnumAdditionalLabelsType|null
	 */
	public function getType()
	{
		return $this->Type;
	}
	/**
	 * Set Type value
	 * @uses MytestEnumAdditionalLabelsType::valueIsValid()
	 * @param MytestEnumAdditionalLabelsType $_type the Type
	 * @return MytestEnumAdditionalLabelsType
	 */
	public function setType($_type)
	{
		if(!MytestEnumAdditionalLabelsType::valueIsValid($_type))
		{
			return false;
		}
		return ($this->Type = $_type);
	}
	/**
	 * Get Count value
	 * @return nonNegativeInteger|null
	 */
	public function getCount()
	{
		return $this->Count;
	}
	/**
	 * Set Count value
	 * @param nonNegativeInteger $_count the Count
	 * @return nonNegativeInteger
	 */
	public function setCount($_count)
	{
		return ($this->Count = $_count);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructAdditionalLabelsDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>