<?php
/**
 * File for class MytestStructRequestedShipment
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructRequestedShipment originally named RequestedShipment
 * Documentation : The descriptive data for the shipment being tendered to FedEx.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructRequestedShipment extends MytestWsdlClass
{
	/**
	 * The Shipper
	 * Meta informations extracted from the WSDL
	 * - documentation : Descriptive data identifying the party responsible for shipping the package. Shipper and Origin should have the same address.
	 * - minOccurs : 1
	 * @var MytestStructParty
	 */
	public $Shipper;
	/**
	 * The Recipient
	 * Meta informations extracted from the WSDL
	 * - documentation : Descriptive data identifying the party receiving the package.
	 * - minOccurs : 1
	 * @var MytestStructParty
	 */
	public $Recipient;
	/**
	 * The ShipTimestamp
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var dateTime
	 */
	public $ShipTimestamp;
	/**
	 * The DropoffType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumDropoffType
	 */
	public $DropoffType;
	/**
	 * The ServiceType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumServiceType
	 */
	public $ServiceType;
	/**
	 * The PackagingType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumPackagingType
	 */
	public $PackagingType;
	/**
	 * The TotalWeight
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructWeight
	 */
	public $TotalWeight;
	/**
	 * The TotalInsuredValue
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TotalInsuredValue;
	/**
	 * The PreferredCurrency
	 * Meta informations extracted from the WSDL
	 * - documentation : This attribute indicates the currency the caller requests to have used in all returned monetary values (when a choice is possible).
	 * - minOccurs : 0
	 * @var string
	 */
	public $PreferredCurrency;
	/**
	 * The RecipientLocationNumber
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $RecipientLocationNumber;
	/**
	 * The Origin
	 * Meta informations extracted from the WSDL
	 * - documentation : Physical starting address for the shipment, if different from shipper's address.
	 * - minOccurs : 0
	 * @var MytestStructContactAndAddress
	 */
	public $Origin;
	/**
	 * The ShippingChargesPayment
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructPayment
	 */
	public $ShippingChargesPayment;
	/**
	 * The SpecialServicesRequested
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructShipmentSpecialServicesRequested
	 */
	public $SpecialServicesRequested;
	/**
	 * The ExpressFreightDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructExpressFreightDetail
	 */
	public $ExpressFreightDetail;
	/**
	 * The FreightShipmentDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Data applicable to shipments using FEDEX_FREIGHT_ECONOMY and FEDEX_FREIGHT_PRIORITY services.
	 * - minOccurs : 0
	 * @var MytestStructFreightShipmentDetail
	 */
	public $FreightShipmentDetail;
	/**
	 * The DeliveryInstructions
	 * Meta informations extracted from the WSDL
	 * - documentation : Used with Ground Home Delivery and Freight.
	 * - minOccurs : 0
	 * @var string
	 */
	public $DeliveryInstructions;
	/**
	 * The VariableHandlingChargeDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructVariableHandlingChargeDetail
	 */
	public $VariableHandlingChargeDetail;
	/**
	 * The CustomsClearanceDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Customs clearance data, used for both international and intra-country shipping.
	 * - minOccurs : 0
	 * @var MytestStructCustomsClearanceDetail
	 */
	public $CustomsClearanceDetail;
	/**
	 * The PickupDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : For use in "process tag" transaction.
	 * - minOccurs : 0
	 * @var MytestStructPickupDetail
	 */
	public $PickupDetail;
	/**
	 * The SmartPostDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the characteristics of a shipment pertaining to SmartPost services.
	 * - minOccurs : 0
	 * @var MytestStructSmartPostShipmentDetail
	 */
	public $SmartPostDetail;
	/**
	 * The BlockInsightVisibility
	 * Meta informations extracted from the WSDL
	 * - documentation : If true, only the shipper/payor will have visibility of this shipment.
	 * - minOccurs : 0
	 * @var boolean
	 */
	public $BlockInsightVisibility;
	/**
	 * The LabelSpecification
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructLabelSpecification
	 */
	public $LabelSpecification;
	/**
	 * The ShippingDocumentSpecification
	 * Meta informations extracted from the WSDL
	 * - documentation : Contains data used to create additional (non-label) shipping documents.
	 * - minOccurs : 0
	 * @var MytestStructShippingDocumentSpecification
	 */
	public $ShippingDocumentSpecification;
	/**
	 * The RateRequestTypes
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies whether and what kind of rates the customer wishes to have quoted on this shipment. The reply will also be constrained by other data on the shipment and customer.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumRateRequestType
	 */
	public $RateRequestTypes;
	/**
	 * The EdtRequestType
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies whether the customer wishes to have Estimated Duties and Taxes provided with the rate quotation on this shipment. Only applies with shipments moving under international services.
	 * - minOccurs : 0
	 * @var MytestEnumEdtRequestType
	 */
	public $EdtRequestType;
	/**
	 * The PackageCount
	 * Meta informations extracted from the WSDL
	 * - documentation : The total number of packages in the entire shipment (even when the shipment spans multiple transactions.)
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $PackageCount;
	/**
	 * The ShipmentOnlyFields
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies which package-level data values are provided at the shipment-level only. The package-level data values types specified here will not be provided at the package-level.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumShipmentOnlyFieldsType
	 */
	public $ShipmentOnlyFields;
	/**
	 * The ConfigurationData
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies data structures that may be re-used multiple times with s single shipment.
	 * - minOccurs : 0
	 * @var MytestStructShipmentConfigurationData
	 */
	public $ConfigurationData;
	/**
	 * The RequestedPackageLineItems
	 * Meta informations extracted from the WSDL
	 * - documentation : One or more package-attribute descriptions, each of which describes an individual package, a group of identical packages, or (for the total-piece-total-weight case) common characteristics all packages in the shipment.
	 * - maxOccurs : 999
	 * - minOccurs : 0
	 * @var MytestStructRequestedPackageLineItem
	 */
	public $RequestedPackageLineItems;
	/**
	 * Constructor method for RequestedShipment
	 * @see parent::__construct()
	 * @param MytestStructParty $_shipper
	 * @param MytestStructParty $_recipient
	 * @param dateTime $_shipTimestamp
	 * @param MytestEnumDropoffType $_dropoffType
	 * @param MytestEnumServiceType $_serviceType
	 * @param MytestEnumPackagingType $_packagingType
	 * @param MytestStructWeight $_totalWeight
	 * @param MytestStructMoney $_totalInsuredValue
	 * @param string $_preferredCurrency
	 * @param string $_recipientLocationNumber
	 * @param MytestStructContactAndAddress $_origin
	 * @param MytestStructPayment $_shippingChargesPayment
	 * @param MytestStructShipmentSpecialServicesRequested $_specialServicesRequested
	 * @param MytestStructExpressFreightDetail $_expressFreightDetail
	 * @param MytestStructFreightShipmentDetail $_freightShipmentDetail
	 * @param string $_deliveryInstructions
	 * @param MytestStructVariableHandlingChargeDetail $_variableHandlingChargeDetail
	 * @param MytestStructCustomsClearanceDetail $_customsClearanceDetail
	 * @param MytestStructPickupDetail $_pickupDetail
	 * @param MytestStructSmartPostShipmentDetail $_smartPostDetail
	 * @param boolean $_blockInsightVisibility
	 * @param MytestStructLabelSpecification $_labelSpecification
	 * @param MytestStructShippingDocumentSpecification $_shippingDocumentSpecification
	 * @param MytestEnumRateRequestType $_rateRequestTypes
	 * @param MytestEnumEdtRequestType $_edtRequestType
	 * @param nonNegativeInteger $_packageCount
	 * @param MytestEnumShipmentOnlyFieldsType $_shipmentOnlyFields
	 * @param MytestStructShipmentConfigurationData $_configurationData
	 * @param MytestStructRequestedPackageLineItem $_requestedPackageLineItems
	 * @return MytestStructRequestedShipment
	 */
	public function __construct($_shipper,$_recipient,$_shipTimestamp = NULL,$_dropoffType = NULL,$_serviceType = NULL,$_packagingType = NULL,$_totalWeight = NULL,$_totalInsuredValue = NULL,$_preferredCurrency = NULL,$_recipientLocationNumber = NULL,$_origin = NULL,$_shippingChargesPayment = NULL,$_specialServicesRequested = NULL,$_expressFreightDetail = NULL,$_freightShipmentDetail = NULL,$_deliveryInstructions = NULL,$_variableHandlingChargeDetail = NULL,$_customsClearanceDetail = NULL,$_pickupDetail = NULL,$_smartPostDetail = NULL,$_blockInsightVisibility = NULL,$_labelSpecification = NULL,$_shippingDocumentSpecification = NULL,$_rateRequestTypes = NULL,$_edtRequestType = NULL,$_packageCount = NULL,$_shipmentOnlyFields = NULL,$_configurationData = NULL,$_requestedPackageLineItems = NULL)
	{
		parent::__construct(array('Shipper'=>$_shipper,'Recipient'=>$_recipient,'ShipTimestamp'=>$_shipTimestamp,'DropoffType'=>$_dropoffType,'ServiceType'=>$_serviceType,'PackagingType'=>$_packagingType,'TotalWeight'=>$_totalWeight,'TotalInsuredValue'=>$_totalInsuredValue,'PreferredCurrency'=>$_preferredCurrency,'RecipientLocationNumber'=>$_recipientLocationNumber,'Origin'=>$_origin,'ShippingChargesPayment'=>$_shippingChargesPayment,'SpecialServicesRequested'=>$_specialServicesRequested,'ExpressFreightDetail'=>$_expressFreightDetail,'FreightShipmentDetail'=>$_freightShipmentDetail,'DeliveryInstructions'=>$_deliveryInstructions,'VariableHandlingChargeDetail'=>$_variableHandlingChargeDetail,'CustomsClearanceDetail'=>$_customsClearanceDetail,'PickupDetail'=>$_pickupDetail,'SmartPostDetail'=>$_smartPostDetail,'BlockInsightVisibility'=>$_blockInsightVisibility,'LabelSpecification'=>$_labelSpecification,'ShippingDocumentSpecification'=>$_shippingDocumentSpecification,'RateRequestTypes'=>$_rateRequestTypes,'EdtRequestType'=>$_edtRequestType,'PackageCount'=>$_packageCount,'ShipmentOnlyFields'=>$_shipmentOnlyFields,'ConfigurationData'=>$_configurationData,'RequestedPackageLineItems'=>$_requestedPackageLineItems));
	}
	/**
	 * Get Shipper value
	 * @return MytestStructParty
	 */
	public function getShipper()
	{
		return $this->Shipper;
	}
	/**
	 * Set Shipper value
	 * @param MytestStructParty $_shipper the Shipper
	 * @return MytestStructParty
	 */
	public function setShipper($_shipper)
	{
		return ($this->Shipper = $_shipper);
	}
	/**
	 * Get Recipient value
	 * @return MytestStructParty
	 */
	public function getRecipient()
	{
		return $this->Recipient;
	}
	/**
	 * Set Recipient value
	 * @param MytestStructParty $_recipient the Recipient
	 * @return MytestStructParty
	 */
	public function setRecipient($_recipient)
	{
		return ($this->Recipient = $_recipient);
	}
	/**
	 * Get ShipTimestamp value
	 * @return dateTime|null
	 */
	public function getShipTimestamp()
	{
		return $this->ShipTimestamp;
	}
	/**
	 * Set ShipTimestamp value
	 * @param dateTime $_shipTimestamp the ShipTimestamp
	 * @return dateTime
	 */
	public function setShipTimestamp($_shipTimestamp)
	{
		return ($this->ShipTimestamp = $_shipTimestamp);
	}
	/**
	 * Get DropoffType value
	 * @return MytestEnumDropoffType|null
	 */
	public function getDropoffType()
	{
		return $this->DropoffType;
	}
	/**
	 * Set DropoffType value
	 * @uses MytestEnumDropoffType::valueIsValid()
	 * @param MytestEnumDropoffType $_dropoffType the DropoffType
	 * @return MytestEnumDropoffType
	 */
	public function setDropoffType($_dropoffType)
	{
		if(!MytestEnumDropoffType::valueIsValid($_dropoffType))
		{
			return false;
		}
		return ($this->DropoffType = $_dropoffType);
	}
	/**
	 * Get ServiceType value
	 * @return MytestEnumServiceType|null
	 */
	public function getServiceType()
	{
		return $this->ServiceType;
	}
	/**
	 * Set ServiceType value
	 * @uses MytestEnumServiceType::valueIsValid()
	 * @param MytestEnumServiceType $_serviceType the ServiceType
	 * @return MytestEnumServiceType
	 */
	public function setServiceType($_serviceType)
	{
		if(!MytestEnumServiceType::valueIsValid($_serviceType))
		{
			return false;
		}
		return ($this->ServiceType = $_serviceType);
	}
	/**
	 * Get PackagingType value
	 * @return MytestEnumPackagingType|null
	 */
	public function getPackagingType()
	{
		return $this->PackagingType;
	}
	/**
	 * Set PackagingType value
	 * @uses MytestEnumPackagingType::valueIsValid()
	 * @param MytestEnumPackagingType $_packagingType the PackagingType
	 * @return MytestEnumPackagingType
	 */
	public function setPackagingType($_packagingType)
	{
		if(!MytestEnumPackagingType::valueIsValid($_packagingType))
		{
			return false;
		}
		return ($this->PackagingType = $_packagingType);
	}
	/**
	 * Get TotalWeight value
	 * @return MytestStructWeight|null
	 */
	public function getTotalWeight()
	{
		return $this->TotalWeight;
	}
	/**
	 * Set TotalWeight value
	 * @param MytestStructWeight $_totalWeight the TotalWeight
	 * @return MytestStructWeight
	 */
	public function setTotalWeight($_totalWeight)
	{
		return ($this->TotalWeight = $_totalWeight);
	}
	/**
	 * Get TotalInsuredValue value
	 * @return MytestStructMoney|null
	 */
	public function getTotalInsuredValue()
	{
		return $this->TotalInsuredValue;
	}
	/**
	 * Set TotalInsuredValue value
	 * @param MytestStructMoney $_totalInsuredValue the TotalInsuredValue
	 * @return MytestStructMoney
	 */
	public function setTotalInsuredValue($_totalInsuredValue)
	{
		return ($this->TotalInsuredValue = $_totalInsuredValue);
	}
	/**
	 * Get PreferredCurrency value
	 * @return string|null
	 */
	public function getPreferredCurrency()
	{
		return $this->PreferredCurrency;
	}
	/**
	 * Set PreferredCurrency value
	 * @param string $_preferredCurrency the PreferredCurrency
	 * @return string
	 */
	public function setPreferredCurrency($_preferredCurrency)
	{
		return ($this->PreferredCurrency = $_preferredCurrency);
	}
	/**
	 * Get RecipientLocationNumber value
	 * @return string|null
	 */
	public function getRecipientLocationNumber()
	{
		return $this->RecipientLocationNumber;
	}
	/**
	 * Set RecipientLocationNumber value
	 * @param string $_recipientLocationNumber the RecipientLocationNumber
	 * @return string
	 */
	public function setRecipientLocationNumber($_recipientLocationNumber)
	{
		return ($this->RecipientLocationNumber = $_recipientLocationNumber);
	}
	/**
	 * Get Origin value
	 * @return MytestStructContactAndAddress|null
	 */
	public function getOrigin()
	{
		return $this->Origin;
	}
	/**
	 * Set Origin value
	 * @param MytestStructContactAndAddress $_origin the Origin
	 * @return MytestStructContactAndAddress
	 */
	public function setOrigin($_origin)
	{
		return ($this->Origin = $_origin);
	}
	/**
	 * Get ShippingChargesPayment value
	 * @return MytestStructPayment|null
	 */
	public function getShippingChargesPayment()
	{
		return $this->ShippingChargesPayment;
	}
	/**
	 * Set ShippingChargesPayment value
	 * @param MytestStructPayment $_shippingChargesPayment the ShippingChargesPayment
	 * @return MytestStructPayment
	 */
	public function setShippingChargesPayment($_shippingChargesPayment)
	{
		return ($this->ShippingChargesPayment = $_shippingChargesPayment);
	}
	/**
	 * Get SpecialServicesRequested value
	 * @return MytestStructShipmentSpecialServicesRequested|null
	 */
	public function getSpecialServicesRequested()
	{
		return $this->SpecialServicesRequested;
	}
	/**
	 * Set SpecialServicesRequested value
	 * @param MytestStructShipmentSpecialServicesRequested $_specialServicesRequested the SpecialServicesRequested
	 * @return MytestStructShipmentSpecialServicesRequested
	 */
	public function setSpecialServicesRequested($_specialServicesRequested)
	{
		return ($this->SpecialServicesRequested = $_specialServicesRequested);
	}
	/**
	 * Get ExpressFreightDetail value
	 * @return MytestStructExpressFreightDetail|null
	 */
	public function getExpressFreightDetail()
	{
		return $this->ExpressFreightDetail;
	}
	/**
	 * Set ExpressFreightDetail value
	 * @param MytestStructExpressFreightDetail $_expressFreightDetail the ExpressFreightDetail
	 * @return MytestStructExpressFreightDetail
	 */
	public function setExpressFreightDetail($_expressFreightDetail)
	{
		return ($this->ExpressFreightDetail = $_expressFreightDetail);
	}
	/**
	 * Get FreightShipmentDetail value
	 * @return MytestStructFreightShipmentDetail|null
	 */
	public function getFreightShipmentDetail()
	{
		return $this->FreightShipmentDetail;
	}
	/**
	 * Set FreightShipmentDetail value
	 * @param MytestStructFreightShipmentDetail $_freightShipmentDetail the FreightShipmentDetail
	 * @return MytestStructFreightShipmentDetail
	 */
	public function setFreightShipmentDetail($_freightShipmentDetail)
	{
		return ($this->FreightShipmentDetail = $_freightShipmentDetail);
	}
	/**
	 * Get DeliveryInstructions value
	 * @return string|null
	 */
	public function getDeliveryInstructions()
	{
		return $this->DeliveryInstructions;
	}
	/**
	 * Set DeliveryInstructions value
	 * @param string $_deliveryInstructions the DeliveryInstructions
	 * @return string
	 */
	public function setDeliveryInstructions($_deliveryInstructions)
	{
		return ($this->DeliveryInstructions = $_deliveryInstructions);
	}
	/**
	 * Get VariableHandlingChargeDetail value
	 * @return MytestStructVariableHandlingChargeDetail|null
	 */
	public function getVariableHandlingChargeDetail()
	{
		return $this->VariableHandlingChargeDetail;
	}
	/**
	 * Set VariableHandlingChargeDetail value
	 * @param MytestStructVariableHandlingChargeDetail $_variableHandlingChargeDetail the VariableHandlingChargeDetail
	 * @return MytestStructVariableHandlingChargeDetail
	 */
	public function setVariableHandlingChargeDetail($_variableHandlingChargeDetail)
	{
		return ($this->VariableHandlingChargeDetail = $_variableHandlingChargeDetail);
	}
	/**
	 * Get CustomsClearanceDetail value
	 * @return MytestStructCustomsClearanceDetail|null
	 */
	public function getCustomsClearanceDetail()
	{
		return $this->CustomsClearanceDetail;
	}
	/**
	 * Set CustomsClearanceDetail value
	 * @param MytestStructCustomsClearanceDetail $_customsClearanceDetail the CustomsClearanceDetail
	 * @return MytestStructCustomsClearanceDetail
	 */
	public function setCustomsClearanceDetail($_customsClearanceDetail)
	{
		return ($this->CustomsClearanceDetail = $_customsClearanceDetail);
	}
	/**
	 * Get PickupDetail value
	 * @return MytestStructPickupDetail|null
	 */
	public function getPickupDetail()
	{
		return $this->PickupDetail;
	}
	/**
	 * Set PickupDetail value
	 * @param MytestStructPickupDetail $_pickupDetail the PickupDetail
	 * @return MytestStructPickupDetail
	 */
	public function setPickupDetail($_pickupDetail)
	{
		return ($this->PickupDetail = $_pickupDetail);
	}
	/**
	 * Get SmartPostDetail value
	 * @return MytestStructSmartPostShipmentDetail|null
	 */
	public function getSmartPostDetail()
	{
		return $this->SmartPostDetail;
	}
	/**
	 * Set SmartPostDetail value
	 * @param MytestStructSmartPostShipmentDetail $_smartPostDetail the SmartPostDetail
	 * @return MytestStructSmartPostShipmentDetail
	 */
	public function setSmartPostDetail($_smartPostDetail)
	{
		return ($this->SmartPostDetail = $_smartPostDetail);
	}
	/**
	 * Get BlockInsightVisibility value
	 * @return boolean|null
	 */
	public function getBlockInsightVisibility()
	{
		return $this->BlockInsightVisibility;
	}
	/**
	 * Set BlockInsightVisibility value
	 * @param boolean $_blockInsightVisibility the BlockInsightVisibility
	 * @return boolean
	 */
	public function setBlockInsightVisibility($_blockInsightVisibility)
	{
		return ($this->BlockInsightVisibility = $_blockInsightVisibility);
	}
	/**
	 * Get LabelSpecification value
	 * @return MytestStructLabelSpecification|null
	 */
	public function getLabelSpecification()
	{
		return $this->LabelSpecification;
	}
	/**
	 * Set LabelSpecification value
	 * @param MytestStructLabelSpecification $_labelSpecification the LabelSpecification
	 * @return MytestStructLabelSpecification
	 */
	public function setLabelSpecification($_labelSpecification)
	{
		return ($this->LabelSpecification = $_labelSpecification);
	}
	/**
	 * Get ShippingDocumentSpecification value
	 * @return MytestStructShippingDocumentSpecification|null
	 */
	public function getShippingDocumentSpecification()
	{
		return $this->ShippingDocumentSpecification;
	}
	/**
	 * Set ShippingDocumentSpecification value
	 * @param MytestStructShippingDocumentSpecification $_shippingDocumentSpecification the ShippingDocumentSpecification
	 * @return MytestStructShippingDocumentSpecification
	 */
	public function setShippingDocumentSpecification($_shippingDocumentSpecification)
	{
		return ($this->ShippingDocumentSpecification = $_shippingDocumentSpecification);
	}
	/**
	 * Get RateRequestTypes value
	 * @return MytestEnumRateRequestType|null
	 */
	public function getRateRequestTypes()
	{
		return $this->RateRequestTypes;
	}
	/**
	 * Set RateRequestTypes value
	 * @uses MytestEnumRateRequestType::valueIsValid()
	 * @param MytestEnumRateRequestType $_rateRequestTypes the RateRequestTypes
	 * @return MytestEnumRateRequestType
	 */
	public function setRateRequestTypes($_rateRequestTypes)
	{
		if(!MytestEnumRateRequestType::valueIsValid($_rateRequestTypes))
		{
			return false;
		}
		return ($this->RateRequestTypes = $_rateRequestTypes);
	}
	/**
	 * Get EdtRequestType value
	 * @return MytestEnumEdtRequestType|null
	 */
	public function getEdtRequestType()
	{
		return $this->EdtRequestType;
	}
	/**
	 * Set EdtRequestType value
	 * @uses MytestEnumEdtRequestType::valueIsValid()
	 * @param MytestEnumEdtRequestType $_edtRequestType the EdtRequestType
	 * @return MytestEnumEdtRequestType
	 */
	public function setEdtRequestType($_edtRequestType)
	{
		if(!MytestEnumEdtRequestType::valueIsValid($_edtRequestType))
		{
			return false;
		}
		return ($this->EdtRequestType = $_edtRequestType);
	}
	/**
	 * Get PackageCount value
	 * @return nonNegativeInteger|null
	 */
	public function getPackageCount()
	{
		return $this->PackageCount;
	}
	/**
	 * Set PackageCount value
	 * @param nonNegativeInteger $_packageCount the PackageCount
	 * @return nonNegativeInteger
	 */
	public function setPackageCount($_packageCount)
	{
		return ($this->PackageCount = $_packageCount);
	}
	/**
	 * Get ShipmentOnlyFields value
	 * @return MytestEnumShipmentOnlyFieldsType|null
	 */
	public function getShipmentOnlyFields()
	{
		return $this->ShipmentOnlyFields;
	}
	/**
	 * Set ShipmentOnlyFields value
	 * @uses MytestEnumShipmentOnlyFieldsType::valueIsValid()
	 * @param MytestEnumShipmentOnlyFieldsType $_shipmentOnlyFields the ShipmentOnlyFields
	 * @return MytestEnumShipmentOnlyFieldsType
	 */
	public function setShipmentOnlyFields($_shipmentOnlyFields)
	{
		if(!MytestEnumShipmentOnlyFieldsType::valueIsValid($_shipmentOnlyFields))
		{
			return false;
		}
		return ($this->ShipmentOnlyFields = $_shipmentOnlyFields);
	}
	/**
	 * Get ConfigurationData value
	 * @return MytestStructShipmentConfigurationData|null
	 */
	public function getConfigurationData()
	{
		return $this->ConfigurationData;
	}
	/**
	 * Set ConfigurationData value
	 * @param MytestStructShipmentConfigurationData $_configurationData the ConfigurationData
	 * @return MytestStructShipmentConfigurationData
	 */
	public function setConfigurationData($_configurationData)
	{
		return ($this->ConfigurationData = $_configurationData);
	}
	/**
	 * Get RequestedPackageLineItems value
	 * @return MytestStructRequestedPackageLineItem|null
	 */
	public function getRequestedPackageLineItems()
	{
		return $this->RequestedPackageLineItems;
	}
	/**
	 * Set RequestedPackageLineItems value
	 * @param MytestStructRequestedPackageLineItem $_requestedPackageLineItems the RequestedPackageLineItems
	 * @return MytestStructRequestedPackageLineItem
	 */
	public function setRequestedPackageLineItems($_requestedPackageLineItems)
	{
		return ($this->RequestedPackageLineItems = $_requestedPackageLineItems);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructRequestedShipment
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>