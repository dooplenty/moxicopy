<?php
/**
 * File for class MytestStructRequestedPackageLineItem
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructRequestedPackageLineItem originally named RequestedPackageLineItem
 * Documentation : This class rationalizes RequestedPackage and RequestedPackageSummary from previous interfaces.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructRequestedPackageLineItem extends MytestWsdlClass
{
	/**
	 * The SequenceNumber
	 * Meta informations extracted from the WSDL
	 * - documentation : Used only with INDIVIDUAL_PACKAGE, as a unique identifier of each requested package.
	 * - minOccurs : 0
	 * @var positiveInteger
	 */
	public $SequenceNumber;
	/**
	 * The GroupNumber
	 * Meta informations extracted from the WSDL
	 * - documentation : Used only with PACKAGE_GROUPS, as a unique identifier of each group of identical packages.
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $GroupNumber;
	/**
	 * The GroupPackageCount
	 * Meta informations extracted from the WSDL
	 * - documentation : Used only with PACKAGE_GROUPS, as a count of packages within a group of identical packages.
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $GroupPackageCount;
	/**
	 * The VariableHandlingChargeDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructVariableHandlingChargeDetail
	 */
	public $VariableHandlingChargeDetail;
	/**
	 * The InsuredValue
	 * Meta informations extracted from the WSDL
	 * - documentation : Only used for INDIVIDUAL_PACKAGES and PACKAGE_GROUPS. Ignored for PACKAGE_SUMMARY, in which case totalInsuredValue and packageCount on the shipment will be used to determine this value.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $InsuredValue;
	/**
	 * The Weight
	 * Meta informations extracted from the WSDL
	 * - documentation : Only used for INDIVIDUAL_PACKAGES and PACKAGE_GROUPS. Ignored for PACKAGE_SUMMARY, in which case total weight and packageCount on the shipment will be used to determine this value.
	 * - minOccurs : 0
	 * @var MytestStructWeight
	 */
	public $Weight;
	/**
	 * The Dimensions
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructDimensions
	 */
	public $Dimensions;
	/**
	 * The PhysicalPackaging
	 * Meta informations extracted from the WSDL
	 * - documentation : Provides additional detail on how the customer has physically packaged this item. As of June 2009, required for packages moving under international and SmartPost services.
	 * - minOccurs : 0
	 * @var MytestEnumPhysicalPackagingType
	 */
	public $PhysicalPackaging;
	/**
	 * The ItemDescription
	 * Meta informations extracted from the WSDL
	 * - documentation : Human-readable text describing the package.
	 * - minOccurs : 0
	 * @var string
	 */
	public $ItemDescription;
	/**
	 * The CustomerReferences
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : 3
	 * - minOccurs : 0
	 * @var MytestStructCustomerReference
	 */
	public $CustomerReferences;
	/**
	 * The SpecialServicesRequested
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructPackageSpecialServicesRequested
	 */
	public $SpecialServicesRequested;
	/**
	 * The ContentRecords
	 * Meta informations extracted from the WSDL
	 * - documentation : Only used for INDIVIDUAL_PACKAGES and PACKAGE_GROUPS.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructContentRecord
	 */
	public $ContentRecords;
	/**
	 * Constructor method for RequestedPackageLineItem
	 * @see parent::__construct()
	 * @param positiveInteger $_sequenceNumber
	 * @param nonNegativeInteger $_groupNumber
	 * @param nonNegativeInteger $_groupPackageCount
	 * @param MytestStructVariableHandlingChargeDetail $_variableHandlingChargeDetail
	 * @param MytestStructMoney $_insuredValue
	 * @param MytestStructWeight $_weight
	 * @param MytestStructDimensions $_dimensions
	 * @param MytestEnumPhysicalPackagingType $_physicalPackaging
	 * @param string $_itemDescription
	 * @param MytestStructCustomerReference $_customerReferences
	 * @param MytestStructPackageSpecialServicesRequested $_specialServicesRequested
	 * @param MytestStructContentRecord $_contentRecords
	 * @return MytestStructRequestedPackageLineItem
	 */
	public function __construct($_sequenceNumber = NULL,$_groupNumber = NULL,$_groupPackageCount = NULL,$_variableHandlingChargeDetail = NULL,$_insuredValue = NULL,$_weight = NULL,$_dimensions = NULL,$_physicalPackaging = NULL,$_itemDescription = NULL,$_customerReferences = NULL,$_specialServicesRequested = NULL,$_contentRecords = NULL)
	{
		parent::__construct(array('SequenceNumber'=>$_sequenceNumber,'GroupNumber'=>$_groupNumber,'GroupPackageCount'=>$_groupPackageCount,'VariableHandlingChargeDetail'=>$_variableHandlingChargeDetail,'InsuredValue'=>$_insuredValue,'Weight'=>$_weight,'Dimensions'=>$_dimensions,'PhysicalPackaging'=>$_physicalPackaging,'ItemDescription'=>$_itemDescription,'CustomerReferences'=>$_customerReferences,'SpecialServicesRequested'=>$_specialServicesRequested,'ContentRecords'=>$_contentRecords));
	}
	/**
	 * Get SequenceNumber value
	 * @return positiveInteger|null
	 */
	public function getSequenceNumber()
	{
		return $this->SequenceNumber;
	}
	/**
	 * Set SequenceNumber value
	 * @param positiveInteger $_sequenceNumber the SequenceNumber
	 * @return positiveInteger
	 */
	public function setSequenceNumber($_sequenceNumber)
	{
		return ($this->SequenceNumber = $_sequenceNumber);
	}
	/**
	 * Get GroupNumber value
	 * @return nonNegativeInteger|null
	 */
	public function getGroupNumber()
	{
		return $this->GroupNumber;
	}
	/**
	 * Set GroupNumber value
	 * @param nonNegativeInteger $_groupNumber the GroupNumber
	 * @return nonNegativeInteger
	 */
	public function setGroupNumber($_groupNumber)
	{
		return ($this->GroupNumber = $_groupNumber);
	}
	/**
	 * Get GroupPackageCount value
	 * @return nonNegativeInteger|null
	 */
	public function getGroupPackageCount()
	{
		return $this->GroupPackageCount;
	}
	/**
	 * Set GroupPackageCount value
	 * @param nonNegativeInteger $_groupPackageCount the GroupPackageCount
	 * @return nonNegativeInteger
	 */
	public function setGroupPackageCount($_groupPackageCount)
	{
		return ($this->GroupPackageCount = $_groupPackageCount);
	}
	/**
	 * Get VariableHandlingChargeDetail value
	 * @return MytestStructVariableHandlingChargeDetail|null
	 */
	public function getVariableHandlingChargeDetail()
	{
		return $this->VariableHandlingChargeDetail;
	}
	/**
	 * Set VariableHandlingChargeDetail value
	 * @param MytestStructVariableHandlingChargeDetail $_variableHandlingChargeDetail the VariableHandlingChargeDetail
	 * @return MytestStructVariableHandlingChargeDetail
	 */
	public function setVariableHandlingChargeDetail($_variableHandlingChargeDetail)
	{
		return ($this->VariableHandlingChargeDetail = $_variableHandlingChargeDetail);
	}
	/**
	 * Get InsuredValue value
	 * @return MytestStructMoney|null
	 */
	public function getInsuredValue()
	{
		return $this->InsuredValue;
	}
	/**
	 * Set InsuredValue value
	 * @param MytestStructMoney $_insuredValue the InsuredValue
	 * @return MytestStructMoney
	 */
	public function setInsuredValue($_insuredValue)
	{
		return ($this->InsuredValue = $_insuredValue);
	}
	/**
	 * Get Weight value
	 * @return MytestStructWeight|null
	 */
	public function getWeight()
	{
		return $this->Weight;
	}
	/**
	 * Set Weight value
	 * @param MytestStructWeight $_weight the Weight
	 * @return MytestStructWeight
	 */
	public function setWeight($_weight)
	{
		return ($this->Weight = $_weight);
	}
	/**
	 * Get Dimensions value
	 * @return MytestStructDimensions|null
	 */
	public function getDimensions()
	{
		return $this->Dimensions;
	}
	/**
	 * Set Dimensions value
	 * @param MytestStructDimensions $_dimensions the Dimensions
	 * @return MytestStructDimensions
	 */
	public function setDimensions($_dimensions)
	{
		return ($this->Dimensions = $_dimensions);
	}
	/**
	 * Get PhysicalPackaging value
	 * @return MytestEnumPhysicalPackagingType|null
	 */
	public function getPhysicalPackaging()
	{
		return $this->PhysicalPackaging;
	}
	/**
	 * Set PhysicalPackaging value
	 * @uses MytestEnumPhysicalPackagingType::valueIsValid()
	 * @param MytestEnumPhysicalPackagingType $_physicalPackaging the PhysicalPackaging
	 * @return MytestEnumPhysicalPackagingType
	 */
	public function setPhysicalPackaging($_physicalPackaging)
	{
		if(!MytestEnumPhysicalPackagingType::valueIsValid($_physicalPackaging))
		{
			return false;
		}
		return ($this->PhysicalPackaging = $_physicalPackaging);
	}
	/**
	 * Get ItemDescription value
	 * @return string|null
	 */
	public function getItemDescription()
	{
		return $this->ItemDescription;
	}
	/**
	 * Set ItemDescription value
	 * @param string $_itemDescription the ItemDescription
	 * @return string
	 */
	public function setItemDescription($_itemDescription)
	{
		return ($this->ItemDescription = $_itemDescription);
	}
	/**
	 * Get CustomerReferences value
	 * @return MytestStructCustomerReference|null
	 */
	public function getCustomerReferences()
	{
		return $this->CustomerReferences;
	}
	/**
	 * Set CustomerReferences value
	 * @param MytestStructCustomerReference $_customerReferences the CustomerReferences
	 * @return MytestStructCustomerReference
	 */
	public function setCustomerReferences($_customerReferences)
	{
		return ($this->CustomerReferences = $_customerReferences);
	}
	/**
	 * Get SpecialServicesRequested value
	 * @return MytestStructPackageSpecialServicesRequested|null
	 */
	public function getSpecialServicesRequested()
	{
		return $this->SpecialServicesRequested;
	}
	/**
	 * Set SpecialServicesRequested value
	 * @param MytestStructPackageSpecialServicesRequested $_specialServicesRequested the SpecialServicesRequested
	 * @return MytestStructPackageSpecialServicesRequested
	 */
	public function setSpecialServicesRequested($_specialServicesRequested)
	{
		return ($this->SpecialServicesRequested = $_specialServicesRequested);
	}
	/**
	 * Get ContentRecords value
	 * @return MytestStructContentRecord|null
	 */
	public function getContentRecords()
	{
		return $this->ContentRecords;
	}
	/**
	 * Set ContentRecords value
	 * @param MytestStructContentRecord $_contentRecords the ContentRecords
	 * @return MytestStructContentRecord
	 */
	public function setContentRecords($_contentRecords)
	{
		return ($this->ContentRecords = $_contentRecords);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructRequestedPackageLineItem
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>