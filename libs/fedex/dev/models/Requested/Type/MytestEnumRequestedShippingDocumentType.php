<?php
/**
 * File for class MytestEnumRequestedShippingDocumentType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumRequestedShippingDocumentType originally named RequestedShippingDocumentType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumRequestedShippingDocumentType extends MytestWsdlClass
{
	/**
	 * Constant for value 'CERTIFICATE_OF_ORIGIN'
	 * @return string 'CERTIFICATE_OF_ORIGIN'
	 */
	const VALUE_CERTIFICATE_OF_ORIGIN = 'CERTIFICATE_OF_ORIGIN';
	/**
	 * Constant for value 'COMMERCIAL_INVOICE'
	 * @return string 'COMMERCIAL_INVOICE'
	 */
	const VALUE_COMMERCIAL_INVOICE = 'COMMERCIAL_INVOICE';
	/**
	 * Constant for value 'CUSTOMER_SPECIFIED_LABELS'
	 * @return string 'CUSTOMER_SPECIFIED_LABELS'
	 */
	const VALUE_CUSTOMER_SPECIFIED_LABELS = 'CUSTOMER_SPECIFIED_LABELS';
	/**
	 * Constant for value 'DANGEROUS_GOODS_SHIPPERS_DECLARATION'
	 * @return string 'DANGEROUS_GOODS_SHIPPERS_DECLARATION'
	 */
	const VALUE_DANGEROUS_GOODS_SHIPPERS_DECLARATION = 'DANGEROUS_GOODS_SHIPPERS_DECLARATION';
	/**
	 * Constant for value 'GENERAL_AGENCY_AGREEMENT'
	 * @return string 'GENERAL_AGENCY_AGREEMENT'
	 */
	const VALUE_GENERAL_AGENCY_AGREEMENT = 'GENERAL_AGENCY_AGREEMENT';
	/**
	 * Constant for value 'LABEL'
	 * @return string 'LABEL'
	 */
	const VALUE_LABEL = 'LABEL';
	/**
	 * Constant for value 'NAFTA_CERTIFICATE_OF_ORIGIN'
	 * @return string 'NAFTA_CERTIFICATE_OF_ORIGIN'
	 */
	const VALUE_NAFTA_CERTIFICATE_OF_ORIGIN = 'NAFTA_CERTIFICATE_OF_ORIGIN';
	/**
	 * Constant for value 'PRO_FORMA_INVOICE'
	 * @return string 'PRO_FORMA_INVOICE'
	 */
	const VALUE_PRO_FORMA_INVOICE = 'PRO_FORMA_INVOICE';
	/**
	 * Constant for value 'RETURN_INSTRUCTIONS'
	 * @return string 'RETURN_INSTRUCTIONS'
	 */
	const VALUE_RETURN_INSTRUCTIONS = 'RETURN_INSTRUCTIONS';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumRequestedShippingDocumentType::VALUE_CERTIFICATE_OF_ORIGIN
	 * @uses MytestEnumRequestedShippingDocumentType::VALUE_COMMERCIAL_INVOICE
	 * @uses MytestEnumRequestedShippingDocumentType::VALUE_CUSTOMER_SPECIFIED_LABELS
	 * @uses MytestEnumRequestedShippingDocumentType::VALUE_DANGEROUS_GOODS_SHIPPERS_DECLARATION
	 * @uses MytestEnumRequestedShippingDocumentType::VALUE_GENERAL_AGENCY_AGREEMENT
	 * @uses MytestEnumRequestedShippingDocumentType::VALUE_LABEL
	 * @uses MytestEnumRequestedShippingDocumentType::VALUE_NAFTA_CERTIFICATE_OF_ORIGIN
	 * @uses MytestEnumRequestedShippingDocumentType::VALUE_PRO_FORMA_INVOICE
	 * @uses MytestEnumRequestedShippingDocumentType::VALUE_RETURN_INSTRUCTIONS
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumRequestedShippingDocumentType::VALUE_CERTIFICATE_OF_ORIGIN,MytestEnumRequestedShippingDocumentType::VALUE_COMMERCIAL_INVOICE,MytestEnumRequestedShippingDocumentType::VALUE_CUSTOMER_SPECIFIED_LABELS,MytestEnumRequestedShippingDocumentType::VALUE_DANGEROUS_GOODS_SHIPPERS_DECLARATION,MytestEnumRequestedShippingDocumentType::VALUE_GENERAL_AGENCY_AGREEMENT,MytestEnumRequestedShippingDocumentType::VALUE_LABEL,MytestEnumRequestedShippingDocumentType::VALUE_NAFTA_CERTIFICATE_OF_ORIGIN,MytestEnumRequestedShippingDocumentType::VALUE_PRO_FORMA_INVOICE,MytestEnumRequestedShippingDocumentType::VALUE_RETURN_INSTRUCTIONS));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>