<?php
/**
 * File for class MytestEnumSecondaryBarcodeType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumSecondaryBarcodeType originally named SecondaryBarcodeType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumSecondaryBarcodeType extends MytestWsdlClass
{
	/**
	 * Constant for value 'COMMON_2D'
	 * @return string 'COMMON_2D'
	 */
	const VALUE_COMMON_2D = 'COMMON_2D';
	/**
	 * Constant for value 'NONE'
	 * @return string 'NONE'
	 */
	const VALUE_NONE = 'NONE';
	/**
	 * Constant for value 'SSCC_18'
	 * @return string 'SSCC_18'
	 */
	const VALUE_SSCC_18 = 'SSCC_18';
	/**
	 * Constant for value 'USPS'
	 * @return string 'USPS'
	 */
	const VALUE_USPS = 'USPS';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumSecondaryBarcodeType::VALUE_COMMON_2D
	 * @uses MytestEnumSecondaryBarcodeType::VALUE_NONE
	 * @uses MytestEnumSecondaryBarcodeType::VALUE_SSCC_18
	 * @uses MytestEnumSecondaryBarcodeType::VALUE_USPS
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumSecondaryBarcodeType::VALUE_COMMON_2D,MytestEnumSecondaryBarcodeType::VALUE_NONE,MytestEnumSecondaryBarcodeType::VALUE_SSCC_18,MytestEnumSecondaryBarcodeType::VALUE_USPS));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>