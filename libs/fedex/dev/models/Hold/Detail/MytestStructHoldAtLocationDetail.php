<?php
/**
 * File for class MytestStructHoldAtLocationDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructHoldAtLocationDetail originally named HoldAtLocationDetail
 * Documentation : Descriptive data required for a FedEx shipment that is to be held at the destination FedEx location for pickup by the recipient.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructHoldAtLocationDetail extends MytestWsdlClass
{
	/**
	 * The PhoneNumber
	 * Meta informations extracted from the WSDL
	 * - documentation : Contact phone number for recipient of shipment.
	 * - minOccurs : 0
	 * @var string
	 */
	public $PhoneNumber;
	/**
	 * The LocationContactAndAddress
	 * Meta informations extracted from the WSDL
	 * - documentation : Contact and address of FedEx facility at which shipment is to be held.
	 * - minOccurs : 0
	 * @var MytestStructContactAndAddress
	 */
	public $LocationContactAndAddress;
	/**
	 * The LocationType
	 * Meta informations extracted from the WSDL
	 * - documentation : Type of facility at which package/shipment is to be held.
	 * - minOccurs : 0
	 * @var MytestEnumFedExLocationType
	 */
	public $LocationType;
	/**
	 * The LocationId
	 * Meta informations extracted from the WSDL
	 * - documentation : Location identification (for facilities identified by an alphanumeric location code).
	 * - minOccurs : 0
	 * @var string
	 */
	public $LocationId;
	/**
	 * The LocationNumber
	 * Meta informations extracted from the WSDL
	 * - documentation : Location identification (for facilities identified by an numeric location code).
	 * - minOccurs : 0
	 * @var int
	 */
	public $LocationNumber;
	/**
	 * Constructor method for HoldAtLocationDetail
	 * @see parent::__construct()
	 * @param string $_phoneNumber
	 * @param MytestStructContactAndAddress $_locationContactAndAddress
	 * @param MytestEnumFedExLocationType $_locationType
	 * @param string $_locationId
	 * @param int $_locationNumber
	 * @return MytestStructHoldAtLocationDetail
	 */
	public function __construct($_phoneNumber = NULL,$_locationContactAndAddress = NULL,$_locationType = NULL,$_locationId = NULL,$_locationNumber = NULL)
	{
		parent::__construct(array('PhoneNumber'=>$_phoneNumber,'LocationContactAndAddress'=>$_locationContactAndAddress,'LocationType'=>$_locationType,'LocationId'=>$_locationId,'LocationNumber'=>$_locationNumber));
	}
	/**
	 * Get PhoneNumber value
	 * @return string|null
	 */
	public function getPhoneNumber()
	{
		return $this->PhoneNumber;
	}
	/**
	 * Set PhoneNumber value
	 * @param string $_phoneNumber the PhoneNumber
	 * @return string
	 */
	public function setPhoneNumber($_phoneNumber)
	{
		return ($this->PhoneNumber = $_phoneNumber);
	}
	/**
	 * Get LocationContactAndAddress value
	 * @return MytestStructContactAndAddress|null
	 */
	public function getLocationContactAndAddress()
	{
		return $this->LocationContactAndAddress;
	}
	/**
	 * Set LocationContactAndAddress value
	 * @param MytestStructContactAndAddress $_locationContactAndAddress the LocationContactAndAddress
	 * @return MytestStructContactAndAddress
	 */
	public function setLocationContactAndAddress($_locationContactAndAddress)
	{
		return ($this->LocationContactAndAddress = $_locationContactAndAddress);
	}
	/**
	 * Get LocationType value
	 * @return MytestEnumFedExLocationType|null
	 */
	public function getLocationType()
	{
		return $this->LocationType;
	}
	/**
	 * Set LocationType value
	 * @uses MytestEnumFedExLocationType::valueIsValid()
	 * @param MytestEnumFedExLocationType $_locationType the LocationType
	 * @return MytestEnumFedExLocationType
	 */
	public function setLocationType($_locationType)
	{
		if(!MytestEnumFedExLocationType::valueIsValid($_locationType))
		{
			return false;
		}
		return ($this->LocationType = $_locationType);
	}
	/**
	 * Get LocationId value
	 * @return string|null
	 */
	public function getLocationId()
	{
		return $this->LocationId;
	}
	/**
	 * Set LocationId value
	 * @param string $_locationId the LocationId
	 * @return string
	 */
	public function setLocationId($_locationId)
	{
		return ($this->LocationId = $_locationId);
	}
	/**
	 * Get LocationNumber value
	 * @return int|null
	 */
	public function getLocationNumber()
	{
		return $this->LocationNumber;
	}
	/**
	 * Set LocationNumber value
	 * @param int $_locationNumber the LocationNumber
	 * @return int
	 */
	public function setLocationNumber($_locationNumber)
	{
		return ($this->LocationNumber = $_locationNumber);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructHoldAtLocationDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>