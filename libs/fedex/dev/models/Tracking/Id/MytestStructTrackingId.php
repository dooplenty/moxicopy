<?php
/**
 * File for class MytestStructTrackingId
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructTrackingId originally named TrackingId
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructTrackingId extends MytestWsdlClass
{
	/**
	 * The TrackingNumber
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 1
	 * @var string
	 */
	public $TrackingNumber;
	/**
	 * The TrackingIdType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumTrackingIdType
	 */
	public $TrackingIdType;
	/**
	 * The FormId
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $FormId;
	/**
	 * Constructor method for TrackingId
	 * @see parent::__construct()
	 * @param string $_trackingNumber
	 * @param MytestEnumTrackingIdType $_trackingIdType
	 * @param string $_formId
	 * @return MytestStructTrackingId
	 */
	public function __construct($_trackingNumber,$_trackingIdType = NULL,$_formId = NULL)
	{
		parent::__construct(array('TrackingNumber'=>$_trackingNumber,'TrackingIdType'=>$_trackingIdType,'FormId'=>$_formId));
	}
	/**
	 * Get TrackingNumber value
	 * @return string
	 */
	public function getTrackingNumber()
	{
		return $this->TrackingNumber;
	}
	/**
	 * Set TrackingNumber value
	 * @param string $_trackingNumber the TrackingNumber
	 * @return string
	 */
	public function setTrackingNumber($_trackingNumber)
	{
		return ($this->TrackingNumber = $_trackingNumber);
	}
	/**
	 * Get TrackingIdType value
	 * @return MytestEnumTrackingIdType|null
	 */
	public function getTrackingIdType()
	{
		return $this->TrackingIdType;
	}
	/**
	 * Set TrackingIdType value
	 * @uses MytestEnumTrackingIdType::valueIsValid()
	 * @param MytestEnumTrackingIdType $_trackingIdType the TrackingIdType
	 * @return MytestEnumTrackingIdType
	 */
	public function setTrackingIdType($_trackingIdType)
	{
		if(!MytestEnumTrackingIdType::valueIsValid($_trackingIdType))
		{
			return false;
		}
		return ($this->TrackingIdType = $_trackingIdType);
	}
	/**
	 * Get FormId value
	 * @return string|null
	 */
	public function getFormId()
	{
		return $this->FormId;
	}
	/**
	 * Set FormId value
	 * @param string $_formId the FormId
	 * @return string
	 */
	public function setFormId($_formId)
	{
		return ($this->FormId = $_formId);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructTrackingId
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>