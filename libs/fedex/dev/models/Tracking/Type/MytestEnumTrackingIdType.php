<?php
/**
 * File for class MytestEnumTrackingIdType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumTrackingIdType originally named TrackingIdType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumTrackingIdType extends MytestWsdlClass
{
	/**
	 * Constant for value 'EXPRESS'
	 * @return string 'EXPRESS'
	 */
	const VALUE_EXPRESS = 'EXPRESS';
	/**
	 * Constant for value 'FEDEX'
	 * @return string 'FEDEX'
	 */
	const VALUE_FEDEX = 'FEDEX';
	/**
	 * Constant for value 'GROUND'
	 * @return string 'GROUND'
	 */
	const VALUE_GROUND = 'GROUND';
	/**
	 * Constant for value 'USPS'
	 * @return string 'USPS'
	 */
	const VALUE_USPS = 'USPS';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumTrackingIdType::VALUE_EXPRESS
	 * @uses MytestEnumTrackingIdType::VALUE_FEDEX
	 * @uses MytestEnumTrackingIdType::VALUE_GROUND
	 * @uses MytestEnumTrackingIdType::VALUE_USPS
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumTrackingIdType::VALUE_EXPRESS,MytestEnumTrackingIdType::VALUE_FEDEX,MytestEnumTrackingIdType::VALUE_GROUND,MytestEnumTrackingIdType::VALUE_USPS));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>