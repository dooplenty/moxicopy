<?php
/**
 * File for class MytestStructExportDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructExportDetail originally named ExportDetail
 * Documentation : Country specific details of an International shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructExportDetail extends MytestWsdlClass
{
	/**
	 * The B13AFilingOption
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumB13AFilingOptionType
	 */
	public $B13AFilingOption;
	/**
	 * The ExportComplianceStatement
	 * Meta informations extracted from the WSDL
	 * - documentation : General field for exporting-country-specific export data (e.g. B13A for CA, FTSR Exemption or AES Citation for US).
	 * - minOccurs : 0
	 * @var string
	 */
	public $ExportComplianceStatement;
	/**
	 * The PermitNumber
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $PermitNumber;
	/**
	 * The DestinationControlDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructDestinationControlDetail
	 */
	public $DestinationControlDetail;
	/**
	 * Constructor method for ExportDetail
	 * @see parent::__construct()
	 * @param MytestEnumB13AFilingOptionType $_b13AFilingOption
	 * @param string $_exportComplianceStatement
	 * @param string $_permitNumber
	 * @param MytestStructDestinationControlDetail $_destinationControlDetail
	 * @return MytestStructExportDetail
	 */
	public function __construct($_b13AFilingOption = NULL,$_exportComplianceStatement = NULL,$_permitNumber = NULL,$_destinationControlDetail = NULL)
	{
		parent::__construct(array('B13AFilingOption'=>$_b13AFilingOption,'ExportComplianceStatement'=>$_exportComplianceStatement,'PermitNumber'=>$_permitNumber,'DestinationControlDetail'=>$_destinationControlDetail));
	}
	/**
	 * Get B13AFilingOption value
	 * @return MytestEnumB13AFilingOptionType|null
	 */
	public function getB13AFilingOption()
	{
		return $this->B13AFilingOption;
	}
	/**
	 * Set B13AFilingOption value
	 * @uses MytestEnumB13AFilingOptionType::valueIsValid()
	 * @param MytestEnumB13AFilingOptionType $_b13AFilingOption the B13AFilingOption
	 * @return MytestEnumB13AFilingOptionType
	 */
	public function setB13AFilingOption($_b13AFilingOption)
	{
		if(!MytestEnumB13AFilingOptionType::valueIsValid($_b13AFilingOption))
		{
			return false;
		}
		return ($this->B13AFilingOption = $_b13AFilingOption);
	}
	/**
	 * Get ExportComplianceStatement value
	 * @return string|null
	 */
	public function getExportComplianceStatement()
	{
		return $this->ExportComplianceStatement;
	}
	/**
	 * Set ExportComplianceStatement value
	 * @param string $_exportComplianceStatement the ExportComplianceStatement
	 * @return string
	 */
	public function setExportComplianceStatement($_exportComplianceStatement)
	{
		return ($this->ExportComplianceStatement = $_exportComplianceStatement);
	}
	/**
	 * Get PermitNumber value
	 * @return string|null
	 */
	public function getPermitNumber()
	{
		return $this->PermitNumber;
	}
	/**
	 * Set PermitNumber value
	 * @param string $_permitNumber the PermitNumber
	 * @return string
	 */
	public function setPermitNumber($_permitNumber)
	{
		return ($this->PermitNumber = $_permitNumber);
	}
	/**
	 * Get DestinationControlDetail value
	 * @return MytestStructDestinationControlDetail|null
	 */
	public function getDestinationControlDetail()
	{
		return $this->DestinationControlDetail;
	}
	/**
	 * Set DestinationControlDetail value
	 * @param MytestStructDestinationControlDetail $_destinationControlDetail the DestinationControlDetail
	 * @return MytestStructDestinationControlDetail
	 */
	public function setDestinationControlDetail($_destinationControlDetail)
	{
		return ($this->DestinationControlDetail = $_destinationControlDetail);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructExportDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>