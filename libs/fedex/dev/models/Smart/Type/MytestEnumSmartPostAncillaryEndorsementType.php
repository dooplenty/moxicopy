<?php
/**
 * File for class MytestEnumSmartPostAncillaryEndorsementType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumSmartPostAncillaryEndorsementType originally named SmartPostAncillaryEndorsementType
 * Documentation : These values are mutually exclusive; at most one of them can be attached to a SmartPost shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumSmartPostAncillaryEndorsementType extends MytestWsdlClass
{
	/**
	 * Constant for value 'ADDRESS_CORRECTION'
	 * @return string 'ADDRESS_CORRECTION'
	 */
	const VALUE_ADDRESS_CORRECTION = 'ADDRESS_CORRECTION';
	/**
	 * Constant for value 'CARRIER_LEAVE_IF_NO_RESPONSE'
	 * @return string 'CARRIER_LEAVE_IF_NO_RESPONSE'
	 */
	const VALUE_CARRIER_LEAVE_IF_NO_RESPONSE = 'CARRIER_LEAVE_IF_NO_RESPONSE';
	/**
	 * Constant for value 'CHANGE_SERVICE'
	 * @return string 'CHANGE_SERVICE'
	 */
	const VALUE_CHANGE_SERVICE = 'CHANGE_SERVICE';
	/**
	 * Constant for value 'FORWARDING_SERVICE'
	 * @return string 'FORWARDING_SERVICE'
	 */
	const VALUE_FORWARDING_SERVICE = 'FORWARDING_SERVICE';
	/**
	 * Constant for value 'RETURN_SERVICE'
	 * @return string 'RETURN_SERVICE'
	 */
	const VALUE_RETURN_SERVICE = 'RETURN_SERVICE';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumSmartPostAncillaryEndorsementType::VALUE_ADDRESS_CORRECTION
	 * @uses MytestEnumSmartPostAncillaryEndorsementType::VALUE_CARRIER_LEAVE_IF_NO_RESPONSE
	 * @uses MytestEnumSmartPostAncillaryEndorsementType::VALUE_CHANGE_SERVICE
	 * @uses MytestEnumSmartPostAncillaryEndorsementType::VALUE_FORWARDING_SERVICE
	 * @uses MytestEnumSmartPostAncillaryEndorsementType::VALUE_RETURN_SERVICE
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumSmartPostAncillaryEndorsementType::VALUE_ADDRESS_CORRECTION,MytestEnumSmartPostAncillaryEndorsementType::VALUE_CARRIER_LEAVE_IF_NO_RESPONSE,MytestEnumSmartPostAncillaryEndorsementType::VALUE_CHANGE_SERVICE,MytestEnumSmartPostAncillaryEndorsementType::VALUE_FORWARDING_SERVICE,MytestEnumSmartPostAncillaryEndorsementType::VALUE_RETURN_SERVICE));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>