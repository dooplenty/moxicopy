<?php
/**
 * File for class MytestEnumSmartPostIndiciaType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumSmartPostIndiciaType originally named SmartPostIndiciaType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumSmartPostIndiciaType extends MytestWsdlClass
{
	/**
	 * Constant for value 'MEDIA_MAIL'
	 * @return string 'MEDIA_MAIL'
	 */
	const VALUE_MEDIA_MAIL = 'MEDIA_MAIL';
	/**
	 * Constant for value 'PARCEL_RETURN'
	 * @return string 'PARCEL_RETURN'
	 */
	const VALUE_PARCEL_RETURN = 'PARCEL_RETURN';
	/**
	 * Constant for value 'PARCEL_SELECT'
	 * @return string 'PARCEL_SELECT'
	 */
	const VALUE_PARCEL_SELECT = 'PARCEL_SELECT';
	/**
	 * Constant for value 'PRESORTED_BOUND_PRINTED_MATTER'
	 * @return string 'PRESORTED_BOUND_PRINTED_MATTER'
	 */
	const VALUE_PRESORTED_BOUND_PRINTED_MATTER = 'PRESORTED_BOUND_PRINTED_MATTER';
	/**
	 * Constant for value 'PRESORTED_STANDARD'
	 * @return string 'PRESORTED_STANDARD'
	 */
	const VALUE_PRESORTED_STANDARD = 'PRESORTED_STANDARD';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumSmartPostIndiciaType::VALUE_MEDIA_MAIL
	 * @uses MytestEnumSmartPostIndiciaType::VALUE_PARCEL_RETURN
	 * @uses MytestEnumSmartPostIndiciaType::VALUE_PARCEL_SELECT
	 * @uses MytestEnumSmartPostIndiciaType::VALUE_PRESORTED_BOUND_PRINTED_MATTER
	 * @uses MytestEnumSmartPostIndiciaType::VALUE_PRESORTED_STANDARD
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumSmartPostIndiciaType::VALUE_MEDIA_MAIL,MytestEnumSmartPostIndiciaType::VALUE_PARCEL_RETURN,MytestEnumSmartPostIndiciaType::VALUE_PARCEL_SELECT,MytestEnumSmartPostIndiciaType::VALUE_PRESORTED_BOUND_PRINTED_MATTER,MytestEnumSmartPostIndiciaType::VALUE_PRESORTED_STANDARD));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>