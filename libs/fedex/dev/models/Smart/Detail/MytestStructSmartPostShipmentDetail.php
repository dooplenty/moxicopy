<?php
/**
 * File for class MytestStructSmartPostShipmentDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructSmartPostShipmentDetail originally named SmartPostShipmentDetail
 * Documentation : Data required for shipments handled under the SMART_POST and GROUND_SMART_POST service types.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructSmartPostShipmentDetail extends MytestWsdlClass
{
	/**
	 * The Indicia
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumSmartPostIndiciaType
	 */
	public $Indicia;
	/**
	 * The AncillaryEndorsement
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumSmartPostAncillaryEndorsementType
	 */
	public $AncillaryEndorsement;
	/**
	 * The HubId
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $HubId;
	/**
	 * The CustomerManifestId
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $CustomerManifestId;
	/**
	 * Constructor method for SmartPostShipmentDetail
	 * @see parent::__construct()
	 * @param MytestEnumSmartPostIndiciaType $_indicia
	 * @param MytestEnumSmartPostAncillaryEndorsementType $_ancillaryEndorsement
	 * @param string $_hubId
	 * @param string $_customerManifestId
	 * @return MytestStructSmartPostShipmentDetail
	 */
	public function __construct($_indicia = NULL,$_ancillaryEndorsement = NULL,$_hubId = NULL,$_customerManifestId = NULL)
	{
		parent::__construct(array('Indicia'=>$_indicia,'AncillaryEndorsement'=>$_ancillaryEndorsement,'HubId'=>$_hubId,'CustomerManifestId'=>$_customerManifestId));
	}
	/**
	 * Get Indicia value
	 * @return MytestEnumSmartPostIndiciaType|null
	 */
	public function getIndicia()
	{
		return $this->Indicia;
	}
	/**
	 * Set Indicia value
	 * @uses MytestEnumSmartPostIndiciaType::valueIsValid()
	 * @param MytestEnumSmartPostIndiciaType $_indicia the Indicia
	 * @return MytestEnumSmartPostIndiciaType
	 */
	public function setIndicia($_indicia)
	{
		if(!MytestEnumSmartPostIndiciaType::valueIsValid($_indicia))
		{
			return false;
		}
		return ($this->Indicia = $_indicia);
	}
	/**
	 * Get AncillaryEndorsement value
	 * @return MytestEnumSmartPostAncillaryEndorsementType|null
	 */
	public function getAncillaryEndorsement()
	{
		return $this->AncillaryEndorsement;
	}
	/**
	 * Set AncillaryEndorsement value
	 * @uses MytestEnumSmartPostAncillaryEndorsementType::valueIsValid()
	 * @param MytestEnumSmartPostAncillaryEndorsementType $_ancillaryEndorsement the AncillaryEndorsement
	 * @return MytestEnumSmartPostAncillaryEndorsementType
	 */
	public function setAncillaryEndorsement($_ancillaryEndorsement)
	{
		if(!MytestEnumSmartPostAncillaryEndorsementType::valueIsValid($_ancillaryEndorsement))
		{
			return false;
		}
		return ($this->AncillaryEndorsement = $_ancillaryEndorsement);
	}
	/**
	 * Get HubId value
	 * @return string|null
	 */
	public function getHubId()
	{
		return $this->HubId;
	}
	/**
	 * Set HubId value
	 * @param string $_hubId the HubId
	 * @return string
	 */
	public function setHubId($_hubId)
	{
		return ($this->HubId = $_hubId);
	}
	/**
	 * Get CustomerManifestId value
	 * @return string|null
	 */
	public function getCustomerManifestId()
	{
		return $this->CustomerManifestId;
	}
	/**
	 * Set CustomerManifestId value
	 * @param string $_customerManifestId the CustomerManifestId
	 * @return string
	 */
	public function setCustomerManifestId($_customerManifestId)
	{
		return ($this->CustomerManifestId = $_customerManifestId);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructSmartPostShipmentDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>