<?php
/**
 * File for class MytestEnumReturnedRateType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumReturnedRateType originally named ReturnedRateType
 * Documentation : The "PAYOR..." rates are expressed in the currency identified in the payor's rate table(s). The "RATED..." rates are expressed in the currency of the origin country. Former "...COUNTER..." values have become "...RETAIL..." values, except for PAYOR_COUNTER and RATED_COUNTER, which have been removed.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumReturnedRateType extends MytestWsdlClass
{
	/**
	 * Constant for value 'PAYOR_ACCOUNT_PACKAGE'
	 * @return string 'PAYOR_ACCOUNT_PACKAGE'
	 */
	const VALUE_PAYOR_ACCOUNT_PACKAGE = 'PAYOR_ACCOUNT_PACKAGE';
	/**
	 * Constant for value 'PAYOR_ACCOUNT_SHIPMENT'
	 * @return string 'PAYOR_ACCOUNT_SHIPMENT'
	 */
	const VALUE_PAYOR_ACCOUNT_SHIPMENT = 'PAYOR_ACCOUNT_SHIPMENT';
	/**
	 * Constant for value 'PAYOR_LIST_PACKAGE'
	 * @return string 'PAYOR_LIST_PACKAGE'
	 */
	const VALUE_PAYOR_LIST_PACKAGE = 'PAYOR_LIST_PACKAGE';
	/**
	 * Constant for value 'PAYOR_LIST_SHIPMENT'
	 * @return string 'PAYOR_LIST_SHIPMENT'
	 */
	const VALUE_PAYOR_LIST_SHIPMENT = 'PAYOR_LIST_SHIPMENT';
	/**
	 * Constant for value 'PREFERRED_ACCOUNT_PACKAGE'
	 * @return string 'PREFERRED_ACCOUNT_PACKAGE'
	 */
	const VALUE_PREFERRED_ACCOUNT_PACKAGE = 'PREFERRED_ACCOUNT_PACKAGE';
	/**
	 * Constant for value 'PREFERRED_ACCOUNT_SHIPMENT'
	 * @return string 'PREFERRED_ACCOUNT_SHIPMENT'
	 */
	const VALUE_PREFERRED_ACCOUNT_SHIPMENT = 'PREFERRED_ACCOUNT_SHIPMENT';
	/**
	 * Constant for value 'PREFERRED_LIST_PACKAGE'
	 * @return string 'PREFERRED_LIST_PACKAGE'
	 */
	const VALUE_PREFERRED_LIST_PACKAGE = 'PREFERRED_LIST_PACKAGE';
	/**
	 * Constant for value 'PREFERRED_LIST_SHIPMENT'
	 * @return string 'PREFERRED_LIST_SHIPMENT'
	 */
	const VALUE_PREFERRED_LIST_SHIPMENT = 'PREFERRED_LIST_SHIPMENT';
	/**
	 * Constant for value 'RATED_ACCOUNT_PACKAGE'
	 * @return string 'RATED_ACCOUNT_PACKAGE'
	 */
	const VALUE_RATED_ACCOUNT_PACKAGE = 'RATED_ACCOUNT_PACKAGE';
	/**
	 * Constant for value 'RATED_ACCOUNT_SHIPMENT'
	 * @return string 'RATED_ACCOUNT_SHIPMENT'
	 */
	const VALUE_RATED_ACCOUNT_SHIPMENT = 'RATED_ACCOUNT_SHIPMENT';
	/**
	 * Constant for value 'RATED_LIST_PACKAGE'
	 * @return string 'RATED_LIST_PACKAGE'
	 */
	const VALUE_RATED_LIST_PACKAGE = 'RATED_LIST_PACKAGE';
	/**
	 * Constant for value 'RATED_LIST_SHIPMENT'
	 * @return string 'RATED_LIST_SHIPMENT'
	 */
	const VALUE_RATED_LIST_SHIPMENT = 'RATED_LIST_SHIPMENT';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumReturnedRateType::VALUE_PAYOR_ACCOUNT_PACKAGE
	 * @uses MytestEnumReturnedRateType::VALUE_PAYOR_ACCOUNT_SHIPMENT
	 * @uses MytestEnumReturnedRateType::VALUE_PAYOR_LIST_PACKAGE
	 * @uses MytestEnumReturnedRateType::VALUE_PAYOR_LIST_SHIPMENT
	 * @uses MytestEnumReturnedRateType::VALUE_PREFERRED_ACCOUNT_PACKAGE
	 * @uses MytestEnumReturnedRateType::VALUE_PREFERRED_ACCOUNT_SHIPMENT
	 * @uses MytestEnumReturnedRateType::VALUE_PREFERRED_LIST_PACKAGE
	 * @uses MytestEnumReturnedRateType::VALUE_PREFERRED_LIST_SHIPMENT
	 * @uses MytestEnumReturnedRateType::VALUE_RATED_ACCOUNT_PACKAGE
	 * @uses MytestEnumReturnedRateType::VALUE_RATED_ACCOUNT_SHIPMENT
	 * @uses MytestEnumReturnedRateType::VALUE_RATED_LIST_PACKAGE
	 * @uses MytestEnumReturnedRateType::VALUE_RATED_LIST_SHIPMENT
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumReturnedRateType::VALUE_PAYOR_ACCOUNT_PACKAGE,MytestEnumReturnedRateType::VALUE_PAYOR_ACCOUNT_SHIPMENT,MytestEnumReturnedRateType::VALUE_PAYOR_LIST_PACKAGE,MytestEnumReturnedRateType::VALUE_PAYOR_LIST_SHIPMENT,MytestEnumReturnedRateType::VALUE_PREFERRED_ACCOUNT_PACKAGE,MytestEnumReturnedRateType::VALUE_PREFERRED_ACCOUNT_SHIPMENT,MytestEnumReturnedRateType::VALUE_PREFERRED_LIST_PACKAGE,MytestEnumReturnedRateType::VALUE_PREFERRED_LIST_SHIPMENT,MytestEnumReturnedRateType::VALUE_RATED_ACCOUNT_PACKAGE,MytestEnumReturnedRateType::VALUE_RATED_ACCOUNT_SHIPMENT,MytestEnumReturnedRateType::VALUE_RATED_LIST_PACKAGE,MytestEnumReturnedRateType::VALUE_RATED_LIST_SHIPMENT));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>