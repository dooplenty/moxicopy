<?php
/**
 * File for class MytestStructPendingShipmentDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructPendingShipmentDetail originally named PendingShipmentDetail
 * Documentation : This information describes the kind of pending shipment being requested.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructPendingShipmentDetail extends MytestWsdlClass
{
	/**
	 * The Type
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 1
	 * @var MytestEnumPendingShipmentType
	 */
	public $Type;
	/**
	 * The ExpirationDate
	 * Meta informations extracted from the WSDL
	 * - documentation : Date after which the pending shipment will no longer be available for completion.
	 * - minOccurs : 0
	 * @var date
	 */
	public $ExpirationDate;
	/**
	 * The EmailLabelDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Only used with type of EMAIL.
	 * - minOccurs : 0
	 * @var MytestStructEMailLabelDetail
	 */
	public $EmailLabelDetail;
	/**
	 * Constructor method for PendingShipmentDetail
	 * @see parent::__construct()
	 * @param MytestEnumPendingShipmentType $_type
	 * @param date $_expirationDate
	 * @param MytestStructEMailLabelDetail $_emailLabelDetail
	 * @return MytestStructPendingShipmentDetail
	 */
	public function __construct($_type,$_expirationDate = NULL,$_emailLabelDetail = NULL)
	{
		parent::__construct(array('Type'=>$_type,'ExpirationDate'=>$_expirationDate,'EmailLabelDetail'=>$_emailLabelDetail));
	}
	/**
	 * Get Type value
	 * @return MytestEnumPendingShipmentType
	 */
	public function getType()
	{
		return $this->Type;
	}
	/**
	 * Set Type value
	 * @uses MytestEnumPendingShipmentType::valueIsValid()
	 * @param MytestEnumPendingShipmentType $_type the Type
	 * @return MytestEnumPendingShipmentType
	 */
	public function setType($_type)
	{
		if(!MytestEnumPendingShipmentType::valueIsValid($_type))
		{
			return false;
		}
		return ($this->Type = $_type);
	}
	/**
	 * Get ExpirationDate value
	 * @return date|null
	 */
	public function getExpirationDate()
	{
		return $this->ExpirationDate;
	}
	/**
	 * Set ExpirationDate value
	 * @param date $_expirationDate the ExpirationDate
	 * @return date
	 */
	public function setExpirationDate($_expirationDate)
	{
		return ($this->ExpirationDate = $_expirationDate);
	}
	/**
	 * Get EmailLabelDetail value
	 * @return MytestStructEMailLabelDetail|null
	 */
	public function getEmailLabelDetail()
	{
		return $this->EmailLabelDetail;
	}
	/**
	 * Set EmailLabelDetail value
	 * @param MytestStructEMailLabelDetail $_emailLabelDetail the EmailLabelDetail
	 * @return MytestStructEMailLabelDetail
	 */
	public function setEmailLabelDetail($_emailLabelDetail)
	{
		return ($this->EmailLabelDetail = $_emailLabelDetail);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructPendingShipmentDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>