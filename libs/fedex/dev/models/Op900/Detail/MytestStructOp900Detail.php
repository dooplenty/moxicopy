<?php
/**
 * File for class MytestStructOp900Detail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructOp900Detail originally named Op900Detail
 * Documentation : The instructions indicating how to print the OP-900 form for hazardous materials packages.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructOp900Detail extends MytestWsdlClass
{
	/**
	 * The Format
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies characteristics of a shipping document to be produced.
	 * - minOccurs : 0
	 * @var MytestStructShippingDocumentFormat
	 */
	public $Format;
	/**
	 * The Reference
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies which reference type (from the package's customer references) is to be used as the source for the reference on this OP-900.
	 * - minOccurs : 0
	 * @var MytestEnumCustomerReferenceType
	 */
	public $Reference;
	/**
	 * The CustomerImageUsages
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the usage and identification of customer supplied images to be used on this document.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructCustomerImageUsage
	 */
	public $CustomerImageUsages;
	/**
	 * The SignatureName
	 * Meta informations extracted from the WSDL
	 * - documentation : Data field to be used when a name is to be printed in the document instead of (or in addition to) a signature image.
	 * - minOccurs : 0
	 * @var string
	 */
	public $SignatureName;
	/**
	 * Constructor method for Op900Detail
	 * @see parent::__construct()
	 * @param MytestStructShippingDocumentFormat $_format
	 * @param MytestEnumCustomerReferenceType $_reference
	 * @param MytestStructCustomerImageUsage $_customerImageUsages
	 * @param string $_signatureName
	 * @return MytestStructOp900Detail
	 */
	public function __construct($_format = NULL,$_reference = NULL,$_customerImageUsages = NULL,$_signatureName = NULL)
	{
		parent::__construct(array('Format'=>$_format,'Reference'=>$_reference,'CustomerImageUsages'=>$_customerImageUsages,'SignatureName'=>$_signatureName));
	}
	/**
	 * Get Format value
	 * @return MytestStructShippingDocumentFormat|null
	 */
	public function getFormat()
	{
		return $this->Format;
	}
	/**
	 * Set Format value
	 * @param MytestStructShippingDocumentFormat $_format the Format
	 * @return MytestStructShippingDocumentFormat
	 */
	public function setFormat($_format)
	{
		return ($this->Format = $_format);
	}
	/**
	 * Get Reference value
	 * @return MytestEnumCustomerReferenceType|null
	 */
	public function getReference()
	{
		return $this->Reference;
	}
	/**
	 * Set Reference value
	 * @uses MytestEnumCustomerReferenceType::valueIsValid()
	 * @param MytestEnumCustomerReferenceType $_reference the Reference
	 * @return MytestEnumCustomerReferenceType
	 */
	public function setReference($_reference)
	{
		if(!MytestEnumCustomerReferenceType::valueIsValid($_reference))
		{
			return false;
		}
		return ($this->Reference = $_reference);
	}
	/**
	 * Get CustomerImageUsages value
	 * @return MytestStructCustomerImageUsage|null
	 */
	public function getCustomerImageUsages()
	{
		return $this->CustomerImageUsages;
	}
	/**
	 * Set CustomerImageUsages value
	 * @param MytestStructCustomerImageUsage $_customerImageUsages the CustomerImageUsages
	 * @return MytestStructCustomerImageUsage
	 */
	public function setCustomerImageUsages($_customerImageUsages)
	{
		return ($this->CustomerImageUsages = $_customerImageUsages);
	}
	/**
	 * Get SignatureName value
	 * @return string|null
	 */
	public function getSignatureName()
	{
		return $this->SignatureName;
	}
	/**
	 * Set SignatureName value
	 * @param string $_signatureName the SignatureName
	 * @return string
	 */
	public function setSignatureName($_signatureName)
	{
		return ($this->SignatureName = $_signatureName);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructOp900Detail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>