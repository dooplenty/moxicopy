<?php
/**
 * File for class MytestStructFreightBaseCharge
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructFreightBaseCharge originally named FreightBaseCharge
 * Documentation : Individual charge which contributes to the total base charge for the shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructFreightBaseCharge extends MytestWsdlClass
{
	/**
	 * The FreightClass
	 * Meta informations extracted from the WSDL
	 * - documentation : Freight class for this line item.
	 * - minOccurs : 0
	 * @var MytestEnumFreightClassType
	 */
	public $FreightClass;
	/**
	 * The RatedAsClass
	 * Meta informations extracted from the WSDL
	 * - documentation : Effective freight class used for rating this line item.
	 * - minOccurs : 0
	 * @var MytestEnumFreightClassType
	 */
	public $RatedAsClass;
	/**
	 * The NmfcCode
	 * Meta informations extracted from the WSDL
	 * - documentation : NMFC Code for commodity.
	 * - minOccurs : 0
	 * @var string
	 */
	public $NmfcCode;
	/**
	 * The Description
	 * Meta informations extracted from the WSDL
	 * - documentation : Customer-provided description for this commodity or class line.
	 * - minOccurs : 0
	 * @var string
	 */
	public $Description;
	/**
	 * The Weight
	 * Meta informations extracted from the WSDL
	 * - documentation : Weight for this commodity or class line.
	 * - minOccurs : 0
	 * @var MytestStructWeight
	 */
	public $Weight;
	/**
	 * The ChargeRate
	 * Meta informations extracted from the WSDL
	 * - documentation : Rate or factor applied to this line item.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $ChargeRate;
	/**
	 * The ChargeBasis
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the manner in which the chargeRate for this line item was applied.
	 * - minOccurs : 0
	 * @var MytestEnumFreightChargeBasisType
	 */
	public $ChargeBasis;
	/**
	 * The ExtendedAmount
	 * Meta informations extracted from the WSDL
	 * - documentation : The net or extended charge for this line item.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $ExtendedAmount;
	/**
	 * Constructor method for FreightBaseCharge
	 * @see parent::__construct()
	 * @param MytestEnumFreightClassType $_freightClass
	 * @param MytestEnumFreightClassType $_ratedAsClass
	 * @param string $_nmfcCode
	 * @param string $_description
	 * @param MytestStructWeight $_weight
	 * @param MytestStructMoney $_chargeRate
	 * @param MytestEnumFreightChargeBasisType $_chargeBasis
	 * @param MytestStructMoney $_extendedAmount
	 * @return MytestStructFreightBaseCharge
	 */
	public function __construct($_freightClass = NULL,$_ratedAsClass = NULL,$_nmfcCode = NULL,$_description = NULL,$_weight = NULL,$_chargeRate = NULL,$_chargeBasis = NULL,$_extendedAmount = NULL)
	{
		parent::__construct(array('FreightClass'=>$_freightClass,'RatedAsClass'=>$_ratedAsClass,'NmfcCode'=>$_nmfcCode,'Description'=>$_description,'Weight'=>$_weight,'ChargeRate'=>$_chargeRate,'ChargeBasis'=>$_chargeBasis,'ExtendedAmount'=>$_extendedAmount));
	}
	/**
	 * Get FreightClass value
	 * @return MytestEnumFreightClassType|null
	 */
	public function getFreightClass()
	{
		return $this->FreightClass;
	}
	/**
	 * Set FreightClass value
	 * @uses MytestEnumFreightClassType::valueIsValid()
	 * @param MytestEnumFreightClassType $_freightClass the FreightClass
	 * @return MytestEnumFreightClassType
	 */
	public function setFreightClass($_freightClass)
	{
		if(!MytestEnumFreightClassType::valueIsValid($_freightClass))
		{
			return false;
		}
		return ($this->FreightClass = $_freightClass);
	}
	/**
	 * Get RatedAsClass value
	 * @return MytestEnumFreightClassType|null
	 */
	public function getRatedAsClass()
	{
		return $this->RatedAsClass;
	}
	/**
	 * Set RatedAsClass value
	 * @uses MytestEnumFreightClassType::valueIsValid()
	 * @param MytestEnumFreightClassType $_ratedAsClass the RatedAsClass
	 * @return MytestEnumFreightClassType
	 */
	public function setRatedAsClass($_ratedAsClass)
	{
		if(!MytestEnumFreightClassType::valueIsValid($_ratedAsClass))
		{
			return false;
		}
		return ($this->RatedAsClass = $_ratedAsClass);
	}
	/**
	 * Get NmfcCode value
	 * @return string|null
	 */
	public function getNmfcCode()
	{
		return $this->NmfcCode;
	}
	/**
	 * Set NmfcCode value
	 * @param string $_nmfcCode the NmfcCode
	 * @return string
	 */
	public function setNmfcCode($_nmfcCode)
	{
		return ($this->NmfcCode = $_nmfcCode);
	}
	/**
	 * Get Description value
	 * @return string|null
	 */
	public function getDescription()
	{
		return $this->Description;
	}
	/**
	 * Set Description value
	 * @param string $_description the Description
	 * @return string
	 */
	public function setDescription($_description)
	{
		return ($this->Description = $_description);
	}
	/**
	 * Get Weight value
	 * @return MytestStructWeight|null
	 */
	public function getWeight()
	{
		return $this->Weight;
	}
	/**
	 * Set Weight value
	 * @param MytestStructWeight $_weight the Weight
	 * @return MytestStructWeight
	 */
	public function setWeight($_weight)
	{
		return ($this->Weight = $_weight);
	}
	/**
	 * Get ChargeRate value
	 * @return MytestStructMoney|null
	 */
	public function getChargeRate()
	{
		return $this->ChargeRate;
	}
	/**
	 * Set ChargeRate value
	 * @param MytestStructMoney $_chargeRate the ChargeRate
	 * @return MytestStructMoney
	 */
	public function setChargeRate($_chargeRate)
	{
		return ($this->ChargeRate = $_chargeRate);
	}
	/**
	 * Get ChargeBasis value
	 * @return MytestEnumFreightChargeBasisType|null
	 */
	public function getChargeBasis()
	{
		return $this->ChargeBasis;
	}
	/**
	 * Set ChargeBasis value
	 * @uses MytestEnumFreightChargeBasisType::valueIsValid()
	 * @param MytestEnumFreightChargeBasisType $_chargeBasis the ChargeBasis
	 * @return MytestEnumFreightChargeBasisType
	 */
	public function setChargeBasis($_chargeBasis)
	{
		if(!MytestEnumFreightChargeBasisType::valueIsValid($_chargeBasis))
		{
			return false;
		}
		return ($this->ChargeBasis = $_chargeBasis);
	}
	/**
	 * Get ExtendedAmount value
	 * @return MytestStructMoney|null
	 */
	public function getExtendedAmount()
	{
		return $this->ExtendedAmount;
	}
	/**
	 * Set ExtendedAmount value
	 * @param MytestStructMoney $_extendedAmount the ExtendedAmount
	 * @return MytestStructMoney
	 */
	public function setExtendedAmount($_extendedAmount)
	{
		return ($this->ExtendedAmount = $_extendedAmount);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructFreightBaseCharge
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>