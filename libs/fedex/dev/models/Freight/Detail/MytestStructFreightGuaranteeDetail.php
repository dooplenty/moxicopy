<?php
/**
 * File for class MytestStructFreightGuaranteeDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructFreightGuaranteeDetail originally named FreightGuaranteeDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructFreightGuaranteeDetail extends MytestWsdlClass
{
	/**
	 * The Type
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumFreightGuaranteeType
	 */
	public $Type;
	/**
	 * The Date
	 * Meta informations extracted from the WSDL
	 * - documentation : Date for all Freight guarantee types.
	 * - minOccurs : 0
	 * @var date
	 */
	public $Date;
	/**
	 * Constructor method for FreightGuaranteeDetail
	 * @see parent::__construct()
	 * @param MytestEnumFreightGuaranteeType $_type
	 * @param date $_date
	 * @return MytestStructFreightGuaranteeDetail
	 */
	public function __construct($_type = NULL,$_date = NULL)
	{
		parent::__construct(array('Type'=>$_type,'Date'=>$_date));
	}
	/**
	 * Get Type value
	 * @return MytestEnumFreightGuaranteeType|null
	 */
	public function getType()
	{
		return $this->Type;
	}
	/**
	 * Set Type value
	 * @uses MytestEnumFreightGuaranteeType::valueIsValid()
	 * @param MytestEnumFreightGuaranteeType $_type the Type
	 * @return MytestEnumFreightGuaranteeType
	 */
	public function setType($_type)
	{
		if(!MytestEnumFreightGuaranteeType::valueIsValid($_type))
		{
			return false;
		}
		return ($this->Type = $_type);
	}
	/**
	 * Get Date value
	 * @return date|null
	 */
	public function getDate()
	{
		return $this->Date;
	}
	/**
	 * Set Date value
	 * @param date $_date the Date
	 * @return date
	 */
	public function setDate($_date)
	{
		return ($this->Date = $_date);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructFreightGuaranteeDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>