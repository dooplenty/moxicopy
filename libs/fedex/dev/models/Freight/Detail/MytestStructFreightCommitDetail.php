<?php
/**
 * File for class MytestStructFreightCommitDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructFreightCommitDetail originally named FreightCommitDetail
 * Documentation : Information about the Freight Service Centers associated with this shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructFreightCommitDetail extends MytestWsdlClass
{
	/**
	 * The OriginDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Information about the origin Freight Service Center.
	 * - minOccurs : 0
	 * @var MytestStructFreightServiceCenterDetail
	 */
	public $OriginDetail;
	/**
	 * The DestinationDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Information about the destination Freight Service Center.
	 * - minOccurs : 0
	 * @var MytestStructFreightServiceCenterDetail
	 */
	public $DestinationDetail;
	/**
	 * The TotalDistance
	 * Meta informations extracted from the WSDL
	 * - documentation : The distance between the origin and destination FreightService Centers
	 * - minOccurs : 0
	 * @var MytestStructDistance
	 */
	public $TotalDistance;
	/**
	 * Constructor method for FreightCommitDetail
	 * @see parent::__construct()
	 * @param MytestStructFreightServiceCenterDetail $_originDetail
	 * @param MytestStructFreightServiceCenterDetail $_destinationDetail
	 * @param MytestStructDistance $_totalDistance
	 * @return MytestStructFreightCommitDetail
	 */
	public function __construct($_originDetail = NULL,$_destinationDetail = NULL,$_totalDistance = NULL)
	{
		parent::__construct(array('OriginDetail'=>$_originDetail,'DestinationDetail'=>$_destinationDetail,'TotalDistance'=>$_totalDistance));
	}
	/**
	 * Get OriginDetail value
	 * @return MytestStructFreightServiceCenterDetail|null
	 */
	public function getOriginDetail()
	{
		return $this->OriginDetail;
	}
	/**
	 * Set OriginDetail value
	 * @param MytestStructFreightServiceCenterDetail $_originDetail the OriginDetail
	 * @return MytestStructFreightServiceCenterDetail
	 */
	public function setOriginDetail($_originDetail)
	{
		return ($this->OriginDetail = $_originDetail);
	}
	/**
	 * Get DestinationDetail value
	 * @return MytestStructFreightServiceCenterDetail|null
	 */
	public function getDestinationDetail()
	{
		return $this->DestinationDetail;
	}
	/**
	 * Set DestinationDetail value
	 * @param MytestStructFreightServiceCenterDetail $_destinationDetail the DestinationDetail
	 * @return MytestStructFreightServiceCenterDetail
	 */
	public function setDestinationDetail($_destinationDetail)
	{
		return ($this->DestinationDetail = $_destinationDetail);
	}
	/**
	 * Get TotalDistance value
	 * @return MytestStructDistance|null
	 */
	public function getTotalDistance()
	{
		return $this->TotalDistance;
	}
	/**
	 * Set TotalDistance value
	 * @param MytestStructDistance $_totalDistance the TotalDistance
	 * @return MytestStructDistance
	 */
	public function setTotalDistance($_totalDistance)
	{
		return ($this->TotalDistance = $_totalDistance);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructFreightCommitDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>