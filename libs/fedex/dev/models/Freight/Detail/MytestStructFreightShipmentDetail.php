<?php
/**
 * File for class MytestStructFreightShipmentDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructFreightShipmentDetail originally named FreightShipmentDetail
 * Documentation : Data applicable to shipments using FEDEX_FREIGHT_ECONOMY and FEDEX_FREIGHT_PRIORITY services.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructFreightShipmentDetail extends MytestWsdlClass
{
	/**
	 * The FedExFreightAccountNumber
	 * Meta informations extracted from the WSDL
	 * - documentation : Account number used with FEDEX_FREIGHT service.
	 * - minOccurs : 0
	 * @var string
	 */
	public $FedExFreightAccountNumber;
	/**
	 * The FedExFreightBillingContactAndAddress
	 * Meta informations extracted from the WSDL
	 * - documentation : Used for validating FedEx Freight account number and (optionally) identifying third party payment on the bill of lading.
	 * - minOccurs : 0
	 * @var MytestStructContactAndAddress
	 */
	public $FedExFreightBillingContactAndAddress;
	/**
	 * The AlternateBilling
	 * Meta informations extracted from the WSDL
	 * - documentation : Used in connection with "Send Bill To" (SBT) identification of customer's account used for billing.
	 * - minOccurs : 0
	 * @var MytestStructParty
	 */
	public $AlternateBilling;
	/**
	 * The Role
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates the role of the party submitting the transaction.
	 * - minOccurs : 0
	 * @var MytestEnumFreightShipmentRoleType
	 */
	public $Role;
	/**
	 * The CollectTermsType
	 * Meta informations extracted from the WSDL
	 * - documentation : Designates the terms of the "collect" payment for a Freight Shipment.
	 * - minOccurs : 0
	 * @var MytestEnumFreightCollectTermsType
	 */
	public $CollectTermsType;
	/**
	 * The DeclaredValuePerUnit
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the declared value for the shipment
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $DeclaredValuePerUnit;
	/**
	 * The DeclaredValueUnits
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the declared value units corresponding to the above defined declared value
	 * - minOccurs : 0
	 * @var string
	 */
	public $DeclaredValueUnits;
	/**
	 * The LiabilityCoverageDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructLiabilityCoverageDetail
	 */
	public $LiabilityCoverageDetail;
	/**
	 * The Coupons
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifiers for promotional discounts offered to customers.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var string
	 */
	public $Coupons;
	/**
	 * The TotalHandlingUnits
	 * Meta informations extracted from the WSDL
	 * - documentation : Total number of individual handling units in the entire shipment (for unit pricing).
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $TotalHandlingUnits;
	/**
	 * The ClientDiscountPercent
	 * Meta informations extracted from the WSDL
	 * - documentation : Estimated discount rate provided by client for unsecured rate quote.
	 * - minOccurs : 0
	 * @var decimal
	 */
	public $ClientDiscountPercent;
	/**
	 * The PalletWeight
	 * Meta informations extracted from the WSDL
	 * - documentation : Total weight of pallets used in shipment.
	 * - minOccurs : 0
	 * @var MytestStructWeight
	 */
	public $PalletWeight;
	/**
	 * The ShipmentDimensions
	 * Meta informations extracted from the WSDL
	 * - documentation : Overall shipment dimensions.
	 * - minOccurs : 0
	 * @var MytestStructDimensions
	 */
	public $ShipmentDimensions;
	/**
	 * The Comment
	 * Meta informations extracted from the WSDL
	 * - documentation : Description for the shipment.
	 * - minOccurs : 0
	 * @var string
	 */
	public $Comment;
	/**
	 * The SpecialServicePayments
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies which party will pay surcharges for any special services which support split billing.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructFreightSpecialServicePayment
	 */
	public $SpecialServicePayments;
	/**
	 * The HazardousMaterialsOfferor
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $HazardousMaterialsOfferor;
	/**
	 * The LineItems
	 * Meta informations extracted from the WSDL
	 * - documentation : Details of the commodities in the shipment.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructFreightShipmentLineItem
	 */
	public $LineItems;
	/**
	 * Constructor method for FreightShipmentDetail
	 * @see parent::__construct()
	 * @param string $_fedExFreightAccountNumber
	 * @param MytestStructContactAndAddress $_fedExFreightBillingContactAndAddress
	 * @param MytestStructParty $_alternateBilling
	 * @param MytestEnumFreightShipmentRoleType $_role
	 * @param MytestEnumFreightCollectTermsType $_collectTermsType
	 * @param MytestStructMoney $_declaredValuePerUnit
	 * @param string $_declaredValueUnits
	 * @param MytestStructLiabilityCoverageDetail $_liabilityCoverageDetail
	 * @param string $_coupons
	 * @param nonNegativeInteger $_totalHandlingUnits
	 * @param decimal $_clientDiscountPercent
	 * @param MytestStructWeight $_palletWeight
	 * @param MytestStructDimensions $_shipmentDimensions
	 * @param string $_comment
	 * @param MytestStructFreightSpecialServicePayment $_specialServicePayments
	 * @param string $_hazardousMaterialsOfferor
	 * @param MytestStructFreightShipmentLineItem $_lineItems
	 * @return MytestStructFreightShipmentDetail
	 */
	public function __construct($_fedExFreightAccountNumber = NULL,$_fedExFreightBillingContactAndAddress = NULL,$_alternateBilling = NULL,$_role = NULL,$_collectTermsType = NULL,$_declaredValuePerUnit = NULL,$_declaredValueUnits = NULL,$_liabilityCoverageDetail = NULL,$_coupons = NULL,$_totalHandlingUnits = NULL,$_clientDiscountPercent = NULL,$_palletWeight = NULL,$_shipmentDimensions = NULL,$_comment = NULL,$_specialServicePayments = NULL,$_hazardousMaterialsOfferor = NULL,$_lineItems = NULL)
	{
		parent::__construct(array('FedExFreightAccountNumber'=>$_fedExFreightAccountNumber,'FedExFreightBillingContactAndAddress'=>$_fedExFreightBillingContactAndAddress,'AlternateBilling'=>$_alternateBilling,'Role'=>$_role,'CollectTermsType'=>$_collectTermsType,'DeclaredValuePerUnit'=>$_declaredValuePerUnit,'DeclaredValueUnits'=>$_declaredValueUnits,'LiabilityCoverageDetail'=>$_liabilityCoverageDetail,'Coupons'=>$_coupons,'TotalHandlingUnits'=>$_totalHandlingUnits,'ClientDiscountPercent'=>$_clientDiscountPercent,'PalletWeight'=>$_palletWeight,'ShipmentDimensions'=>$_shipmentDimensions,'Comment'=>$_comment,'SpecialServicePayments'=>$_specialServicePayments,'HazardousMaterialsOfferor'=>$_hazardousMaterialsOfferor,'LineItems'=>$_lineItems));
	}
	/**
	 * Get FedExFreightAccountNumber value
	 * @return string|null
	 */
	public function getFedExFreightAccountNumber()
	{
		return $this->FedExFreightAccountNumber;
	}
	/**
	 * Set FedExFreightAccountNumber value
	 * @param string $_fedExFreightAccountNumber the FedExFreightAccountNumber
	 * @return string
	 */
	public function setFedExFreightAccountNumber($_fedExFreightAccountNumber)
	{
		return ($this->FedExFreightAccountNumber = $_fedExFreightAccountNumber);
	}
	/**
	 * Get FedExFreightBillingContactAndAddress value
	 * @return MytestStructContactAndAddress|null
	 */
	public function getFedExFreightBillingContactAndAddress()
	{
		return $this->FedExFreightBillingContactAndAddress;
	}
	/**
	 * Set FedExFreightBillingContactAndAddress value
	 * @param MytestStructContactAndAddress $_fedExFreightBillingContactAndAddress the FedExFreightBillingContactAndAddress
	 * @return MytestStructContactAndAddress
	 */
	public function setFedExFreightBillingContactAndAddress($_fedExFreightBillingContactAndAddress)
	{
		return ($this->FedExFreightBillingContactAndAddress = $_fedExFreightBillingContactAndAddress);
	}
	/**
	 * Get AlternateBilling value
	 * @return MytestStructParty|null
	 */
	public function getAlternateBilling()
	{
		return $this->AlternateBilling;
	}
	/**
	 * Set AlternateBilling value
	 * @param MytestStructParty $_alternateBilling the AlternateBilling
	 * @return MytestStructParty
	 */
	public function setAlternateBilling($_alternateBilling)
	{
		return ($this->AlternateBilling = $_alternateBilling);
	}
	/**
	 * Get Role value
	 * @return MytestEnumFreightShipmentRoleType|null
	 */
	public function getRole()
	{
		return $this->Role;
	}
	/**
	 * Set Role value
	 * @uses MytestEnumFreightShipmentRoleType::valueIsValid()
	 * @param MytestEnumFreightShipmentRoleType $_role the Role
	 * @return MytestEnumFreightShipmentRoleType
	 */
	public function setRole($_role)
	{
		if(!MytestEnumFreightShipmentRoleType::valueIsValid($_role))
		{
			return false;
		}
		return ($this->Role = $_role);
	}
	/**
	 * Get CollectTermsType value
	 * @return MytestEnumFreightCollectTermsType|null
	 */
	public function getCollectTermsType()
	{
		return $this->CollectTermsType;
	}
	/**
	 * Set CollectTermsType value
	 * @uses MytestEnumFreightCollectTermsType::valueIsValid()
	 * @param MytestEnumFreightCollectTermsType $_collectTermsType the CollectTermsType
	 * @return MytestEnumFreightCollectTermsType
	 */
	public function setCollectTermsType($_collectTermsType)
	{
		if(!MytestEnumFreightCollectTermsType::valueIsValid($_collectTermsType))
		{
			return false;
		}
		return ($this->CollectTermsType = $_collectTermsType);
	}
	/**
	 * Get DeclaredValuePerUnit value
	 * @return MytestStructMoney|null
	 */
	public function getDeclaredValuePerUnit()
	{
		return $this->DeclaredValuePerUnit;
	}
	/**
	 * Set DeclaredValuePerUnit value
	 * @param MytestStructMoney $_declaredValuePerUnit the DeclaredValuePerUnit
	 * @return MytestStructMoney
	 */
	public function setDeclaredValuePerUnit($_declaredValuePerUnit)
	{
		return ($this->DeclaredValuePerUnit = $_declaredValuePerUnit);
	}
	/**
	 * Get DeclaredValueUnits value
	 * @return string|null
	 */
	public function getDeclaredValueUnits()
	{
		return $this->DeclaredValueUnits;
	}
	/**
	 * Set DeclaredValueUnits value
	 * @param string $_declaredValueUnits the DeclaredValueUnits
	 * @return string
	 */
	public function setDeclaredValueUnits($_declaredValueUnits)
	{
		return ($this->DeclaredValueUnits = $_declaredValueUnits);
	}
	/**
	 * Get LiabilityCoverageDetail value
	 * @return MytestStructLiabilityCoverageDetail|null
	 */
	public function getLiabilityCoverageDetail()
	{
		return $this->LiabilityCoverageDetail;
	}
	/**
	 * Set LiabilityCoverageDetail value
	 * @param MytestStructLiabilityCoverageDetail $_liabilityCoverageDetail the LiabilityCoverageDetail
	 * @return MytestStructLiabilityCoverageDetail
	 */
	public function setLiabilityCoverageDetail($_liabilityCoverageDetail)
	{
		return ($this->LiabilityCoverageDetail = $_liabilityCoverageDetail);
	}
	/**
	 * Get Coupons value
	 * @return string|null
	 */
	public function getCoupons()
	{
		return $this->Coupons;
	}
	/**
	 * Set Coupons value
	 * @param string $_coupons the Coupons
	 * @return string
	 */
	public function setCoupons($_coupons)
	{
		return ($this->Coupons = $_coupons);
	}
	/**
	 * Get TotalHandlingUnits value
	 * @return nonNegativeInteger|null
	 */
	public function getTotalHandlingUnits()
	{
		return $this->TotalHandlingUnits;
	}
	/**
	 * Set TotalHandlingUnits value
	 * @param nonNegativeInteger $_totalHandlingUnits the TotalHandlingUnits
	 * @return nonNegativeInteger
	 */
	public function setTotalHandlingUnits($_totalHandlingUnits)
	{
		return ($this->TotalHandlingUnits = $_totalHandlingUnits);
	}
	/**
	 * Get ClientDiscountPercent value
	 * @return decimal|null
	 */
	public function getClientDiscountPercent()
	{
		return $this->ClientDiscountPercent;
	}
	/**
	 * Set ClientDiscountPercent value
	 * @param decimal $_clientDiscountPercent the ClientDiscountPercent
	 * @return decimal
	 */
	public function setClientDiscountPercent($_clientDiscountPercent)
	{
		return ($this->ClientDiscountPercent = $_clientDiscountPercent);
	}
	/**
	 * Get PalletWeight value
	 * @return MytestStructWeight|null
	 */
	public function getPalletWeight()
	{
		return $this->PalletWeight;
	}
	/**
	 * Set PalletWeight value
	 * @param MytestStructWeight $_palletWeight the PalletWeight
	 * @return MytestStructWeight
	 */
	public function setPalletWeight($_palletWeight)
	{
		return ($this->PalletWeight = $_palletWeight);
	}
	/**
	 * Get ShipmentDimensions value
	 * @return MytestStructDimensions|null
	 */
	public function getShipmentDimensions()
	{
		return $this->ShipmentDimensions;
	}
	/**
	 * Set ShipmentDimensions value
	 * @param MytestStructDimensions $_shipmentDimensions the ShipmentDimensions
	 * @return MytestStructDimensions
	 */
	public function setShipmentDimensions($_shipmentDimensions)
	{
		return ($this->ShipmentDimensions = $_shipmentDimensions);
	}
	/**
	 * Get Comment value
	 * @return string|null
	 */
	public function getComment()
	{
		return $this->Comment;
	}
	/**
	 * Set Comment value
	 * @param string $_comment the Comment
	 * @return string
	 */
	public function setComment($_comment)
	{
		return ($this->Comment = $_comment);
	}
	/**
	 * Get SpecialServicePayments value
	 * @return MytestStructFreightSpecialServicePayment|null
	 */
	public function getSpecialServicePayments()
	{
		return $this->SpecialServicePayments;
	}
	/**
	 * Set SpecialServicePayments value
	 * @param MytestStructFreightSpecialServicePayment $_specialServicePayments the SpecialServicePayments
	 * @return MytestStructFreightSpecialServicePayment
	 */
	public function setSpecialServicePayments($_specialServicePayments)
	{
		return ($this->SpecialServicePayments = $_specialServicePayments);
	}
	/**
	 * Get HazardousMaterialsOfferor value
	 * @return string|null
	 */
	public function getHazardousMaterialsOfferor()
	{
		return $this->HazardousMaterialsOfferor;
	}
	/**
	 * Set HazardousMaterialsOfferor value
	 * @param string $_hazardousMaterialsOfferor the HazardousMaterialsOfferor
	 * @return string
	 */
	public function setHazardousMaterialsOfferor($_hazardousMaterialsOfferor)
	{
		return ($this->HazardousMaterialsOfferor = $_hazardousMaterialsOfferor);
	}
	/**
	 * Get LineItems value
	 * @return MytestStructFreightShipmentLineItem|null
	 */
	public function getLineItems()
	{
		return $this->LineItems;
	}
	/**
	 * Set LineItems value
	 * @param MytestStructFreightShipmentLineItem $_lineItems the LineItems
	 * @return MytestStructFreightShipmentLineItem
	 */
	public function setLineItems($_lineItems)
	{
		return ($this->LineItems = $_lineItems);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructFreightShipmentDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>