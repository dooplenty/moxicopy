<?php
/**
 * File for class MytestStructFreightServiceCenterDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructFreightServiceCenterDetail originally named FreightServiceCenterDetail
 * Documentation : This class describes the relationship between a customer-specified address and the FedEx Freight / FedEx National Freight Service Center that supports that address.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructFreightServiceCenterDetail extends MytestWsdlClass
{
	/**
	 * The InterlineCarrierCode
	 * Meta informations extracted from the WSDL
	 * - documentation : Freight Industry standard non-FedEx carrier identification
	 * - minOccurs : 0
	 * @var string
	 */
	public $InterlineCarrierCode;
	/**
	 * The InterlineCarrierName
	 * Meta informations extracted from the WSDL
	 * - documentation : The name of the Interline carrier.
	 * - minOccurs : 0
	 * @var string
	 */
	public $InterlineCarrierName;
	/**
	 * The AdditionalDays
	 * Meta informations extracted from the WSDL
	 * - documentation : Additional time it might take at the origin or destination to pickup or deliver the freight. This is usually due to the remoteness of the location. This time is included in the total transit time.
	 * - minOccurs : 0
	 * @var int
	 */
	public $AdditionalDays;
	/**
	 * The LocalService
	 * Meta informations extracted from the WSDL
	 * - documentation : Service branding which may be used for local pickup or delivery, distinct from service used for line-haul of customer's shipment.
	 * - minOccurs : 0
	 * @var MytestEnumServiceType
	 */
	public $LocalService;
	/**
	 * The LocalDistance
	 * Meta informations extracted from the WSDL
	 * - documentation : Distance between customer address (pickup or delivery) and the supporting Freight / National Freight service center.
	 * - minOccurs : 0
	 * @var MytestStructDistance
	 */
	public $LocalDistance;
	/**
	 * The LocalDuration
	 * Meta informations extracted from the WSDL
	 * - documentation : Time to travel between customer address (pickup or delivery) and the supporting Freight / National Freight service center.
	 * - minOccurs : 0
	 * @var duration
	 */
	public $LocalDuration;
	/**
	 * The LocalServiceScheduling
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies when/how the customer can arrange for pickup or delivery.
	 * - minOccurs : 0
	 * @var MytestEnumFreightServiceSchedulingType
	 */
	public $LocalServiceScheduling;
	/**
	 * The LimitedServiceDays
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies days of operation if localServiceScheduling is LIMITED.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumDayOfWeekType
	 */
	public $LimitedServiceDays;
	/**
	 * The GatewayLocationId
	 * Meta informations extracted from the WSDL
	 * - documentation : Freight service center that is a gateway on the border of Canada or Mexico.
	 * - minOccurs : 0
	 * @var string
	 */
	public $GatewayLocationId;
	/**
	 * The Location
	 * Meta informations extracted from the WSDL
	 * - documentation : Alphabetical code identifying a Freight Service Center
	 * - minOccurs : 0
	 * @var string
	 */
	public $Location;
	/**
	 * The ContactAndAddress
	 * Meta informations extracted from the WSDL
	 * - documentation : Freight service center Contact and Address
	 * - minOccurs : 0
	 * @var MytestStructContactAndAddress
	 */
	public $ContactAndAddress;
	/**
	 * Constructor method for FreightServiceCenterDetail
	 * @see parent::__construct()
	 * @param string $_interlineCarrierCode
	 * @param string $_interlineCarrierName
	 * @param int $_additionalDays
	 * @param MytestEnumServiceType $_localService
	 * @param MytestStructDistance $_localDistance
	 * @param duration $_localDuration
	 * @param MytestEnumFreightServiceSchedulingType $_localServiceScheduling
	 * @param MytestEnumDayOfWeekType $_limitedServiceDays
	 * @param string $_gatewayLocationId
	 * @param string $_location
	 * @param MytestStructContactAndAddress $_contactAndAddress
	 * @return MytestStructFreightServiceCenterDetail
	 */
	public function __construct($_interlineCarrierCode = NULL,$_interlineCarrierName = NULL,$_additionalDays = NULL,$_localService = NULL,$_localDistance = NULL,$_localDuration = NULL,$_localServiceScheduling = NULL,$_limitedServiceDays = NULL,$_gatewayLocationId = NULL,$_location = NULL,$_contactAndAddress = NULL)
	{
		parent::__construct(array('InterlineCarrierCode'=>$_interlineCarrierCode,'InterlineCarrierName'=>$_interlineCarrierName,'AdditionalDays'=>$_additionalDays,'LocalService'=>$_localService,'LocalDistance'=>$_localDistance,'LocalDuration'=>$_localDuration,'LocalServiceScheduling'=>$_localServiceScheduling,'LimitedServiceDays'=>$_limitedServiceDays,'GatewayLocationId'=>$_gatewayLocationId,'Location'=>$_location,'ContactAndAddress'=>$_contactAndAddress));
	}
	/**
	 * Get InterlineCarrierCode value
	 * @return string|null
	 */
	public function getInterlineCarrierCode()
	{
		return $this->InterlineCarrierCode;
	}
	/**
	 * Set InterlineCarrierCode value
	 * @param string $_interlineCarrierCode the InterlineCarrierCode
	 * @return string
	 */
	public function setInterlineCarrierCode($_interlineCarrierCode)
	{
		return ($this->InterlineCarrierCode = $_interlineCarrierCode);
	}
	/**
	 * Get InterlineCarrierName value
	 * @return string|null
	 */
	public function getInterlineCarrierName()
	{
		return $this->InterlineCarrierName;
	}
	/**
	 * Set InterlineCarrierName value
	 * @param string $_interlineCarrierName the InterlineCarrierName
	 * @return string
	 */
	public function setInterlineCarrierName($_interlineCarrierName)
	{
		return ($this->InterlineCarrierName = $_interlineCarrierName);
	}
	/**
	 * Get AdditionalDays value
	 * @return int|null
	 */
	public function getAdditionalDays()
	{
		return $this->AdditionalDays;
	}
	/**
	 * Set AdditionalDays value
	 * @param int $_additionalDays the AdditionalDays
	 * @return int
	 */
	public function setAdditionalDays($_additionalDays)
	{
		return ($this->AdditionalDays = $_additionalDays);
	}
	/**
	 * Get LocalService value
	 * @return MytestEnumServiceType|null
	 */
	public function getLocalService()
	{
		return $this->LocalService;
	}
	/**
	 * Set LocalService value
	 * @uses MytestEnumServiceType::valueIsValid()
	 * @param MytestEnumServiceType $_localService the LocalService
	 * @return MytestEnumServiceType
	 */
	public function setLocalService($_localService)
	{
		if(!MytestEnumServiceType::valueIsValid($_localService))
		{
			return false;
		}
		return ($this->LocalService = $_localService);
	}
	/**
	 * Get LocalDistance value
	 * @return MytestStructDistance|null
	 */
	public function getLocalDistance()
	{
		return $this->LocalDistance;
	}
	/**
	 * Set LocalDistance value
	 * @param MytestStructDistance $_localDistance the LocalDistance
	 * @return MytestStructDistance
	 */
	public function setLocalDistance($_localDistance)
	{
		return ($this->LocalDistance = $_localDistance);
	}
	/**
	 * Get LocalDuration value
	 * @return duration|null
	 */
	public function getLocalDuration()
	{
		return $this->LocalDuration;
	}
	/**
	 * Set LocalDuration value
	 * @param duration $_localDuration the LocalDuration
	 * @return duration
	 */
	public function setLocalDuration($_localDuration)
	{
		return ($this->LocalDuration = $_localDuration);
	}
	/**
	 * Get LocalServiceScheduling value
	 * @return MytestEnumFreightServiceSchedulingType|null
	 */
	public function getLocalServiceScheduling()
	{
		return $this->LocalServiceScheduling;
	}
	/**
	 * Set LocalServiceScheduling value
	 * @uses MytestEnumFreightServiceSchedulingType::valueIsValid()
	 * @param MytestEnumFreightServiceSchedulingType $_localServiceScheduling the LocalServiceScheduling
	 * @return MytestEnumFreightServiceSchedulingType
	 */
	public function setLocalServiceScheduling($_localServiceScheduling)
	{
		if(!MytestEnumFreightServiceSchedulingType::valueIsValid($_localServiceScheduling))
		{
			return false;
		}
		return ($this->LocalServiceScheduling = $_localServiceScheduling);
	}
	/**
	 * Get LimitedServiceDays value
	 * @return MytestEnumDayOfWeekType|null
	 */
	public function getLimitedServiceDays()
	{
		return $this->LimitedServiceDays;
	}
	/**
	 * Set LimitedServiceDays value
	 * @uses MytestEnumDayOfWeekType::valueIsValid()
	 * @param MytestEnumDayOfWeekType $_limitedServiceDays the LimitedServiceDays
	 * @return MytestEnumDayOfWeekType
	 */
	public function setLimitedServiceDays($_limitedServiceDays)
	{
		if(!MytestEnumDayOfWeekType::valueIsValid($_limitedServiceDays))
		{
			return false;
		}
		return ($this->LimitedServiceDays = $_limitedServiceDays);
	}
	/**
	 * Get GatewayLocationId value
	 * @return string|null
	 */
	public function getGatewayLocationId()
	{
		return $this->GatewayLocationId;
	}
	/**
	 * Set GatewayLocationId value
	 * @param string $_gatewayLocationId the GatewayLocationId
	 * @return string
	 */
	public function setGatewayLocationId($_gatewayLocationId)
	{
		return ($this->GatewayLocationId = $_gatewayLocationId);
	}
	/**
	 * Get Location value
	 * @return string|null
	 */
	public function getLocation()
	{
		return $this->Location;
	}
	/**
	 * Set Location value
	 * @param string $_location the Location
	 * @return string
	 */
	public function setLocation($_location)
	{
		return ($this->Location = $_location);
	}
	/**
	 * Get ContactAndAddress value
	 * @return MytestStructContactAndAddress|null
	 */
	public function getContactAndAddress()
	{
		return $this->ContactAndAddress;
	}
	/**
	 * Set ContactAndAddress value
	 * @param MytestStructContactAndAddress $_contactAndAddress the ContactAndAddress
	 * @return MytestStructContactAndAddress
	 */
	public function setContactAndAddress($_contactAndAddress)
	{
		return ($this->ContactAndAddress = $_contactAndAddress);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructFreightServiceCenterDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>