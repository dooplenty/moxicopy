<?php
/**
 * File for class MytestStructFreightRateDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructFreightRateDetail originally named FreightRateDetail
 * Documentation : Rate data specific to FedEx Freight or FedEx National Freight services.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructFreightRateDetail extends MytestWsdlClass
{
	/**
	 * The QuoteNumber
	 * Meta informations extracted from the WSDL
	 * - documentation : A unique identifier for a specific rate quotation.
	 * - minOccurs : 0
	 * @var string
	 */
	public $QuoteNumber;
	/**
	 * The QuoteType
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies whether the rate quote was automated or manual.
	 * - minOccurs : 0
	 * @var MytestEnumFreightRateQuoteType
	 */
	public $QuoteType;
	/**
	 * The BaseChargeCalculation
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies how total base charge is determined.
	 * - minOccurs : 0
	 * @var MytestEnumFreightBaseChargeCalculationType
	 */
	public $BaseChargeCalculation;
	/**
	 * The BaseCharges
	 * Meta informations extracted from the WSDL
	 * - documentation : Freight charges which accumulate to the total base charge for the shipment.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructFreightBaseCharge
	 */
	public $BaseCharges;
	/**
	 * The Notations
	 * Meta informations extracted from the WSDL
	 * - documentation : Human-readable descriptions of additional information on this shipment rating.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructFreightRateNotation
	 */
	public $Notations;
	/**
	 * Constructor method for FreightRateDetail
	 * @see parent::__construct()
	 * @param string $_quoteNumber
	 * @param MytestEnumFreightRateQuoteType $_quoteType
	 * @param MytestEnumFreightBaseChargeCalculationType $_baseChargeCalculation
	 * @param MytestStructFreightBaseCharge $_baseCharges
	 * @param MytestStructFreightRateNotation $_notations
	 * @return MytestStructFreightRateDetail
	 */
	public function __construct($_quoteNumber = NULL,$_quoteType = NULL,$_baseChargeCalculation = NULL,$_baseCharges = NULL,$_notations = NULL)
	{
		parent::__construct(array('QuoteNumber'=>$_quoteNumber,'QuoteType'=>$_quoteType,'BaseChargeCalculation'=>$_baseChargeCalculation,'BaseCharges'=>$_baseCharges,'Notations'=>$_notations));
	}
	/**
	 * Get QuoteNumber value
	 * @return string|null
	 */
	public function getQuoteNumber()
	{
		return $this->QuoteNumber;
	}
	/**
	 * Set QuoteNumber value
	 * @param string $_quoteNumber the QuoteNumber
	 * @return string
	 */
	public function setQuoteNumber($_quoteNumber)
	{
		return ($this->QuoteNumber = $_quoteNumber);
	}
	/**
	 * Get QuoteType value
	 * @return MytestEnumFreightRateQuoteType|null
	 */
	public function getQuoteType()
	{
		return $this->QuoteType;
	}
	/**
	 * Set QuoteType value
	 * @uses MytestEnumFreightRateQuoteType::valueIsValid()
	 * @param MytestEnumFreightRateQuoteType $_quoteType the QuoteType
	 * @return MytestEnumFreightRateQuoteType
	 */
	public function setQuoteType($_quoteType)
	{
		if(!MytestEnumFreightRateQuoteType::valueIsValid($_quoteType))
		{
			return false;
		}
		return ($this->QuoteType = $_quoteType);
	}
	/**
	 * Get BaseChargeCalculation value
	 * @return MytestEnumFreightBaseChargeCalculationType|null
	 */
	public function getBaseChargeCalculation()
	{
		return $this->BaseChargeCalculation;
	}
	/**
	 * Set BaseChargeCalculation value
	 * @uses MytestEnumFreightBaseChargeCalculationType::valueIsValid()
	 * @param MytestEnumFreightBaseChargeCalculationType $_baseChargeCalculation the BaseChargeCalculation
	 * @return MytestEnumFreightBaseChargeCalculationType
	 */
	public function setBaseChargeCalculation($_baseChargeCalculation)
	{
		if(!MytestEnumFreightBaseChargeCalculationType::valueIsValid($_baseChargeCalculation))
		{
			return false;
		}
		return ($this->BaseChargeCalculation = $_baseChargeCalculation);
	}
	/**
	 * Get BaseCharges value
	 * @return MytestStructFreightBaseCharge|null
	 */
	public function getBaseCharges()
	{
		return $this->BaseCharges;
	}
	/**
	 * Set BaseCharges value
	 * @param MytestStructFreightBaseCharge $_baseCharges the BaseCharges
	 * @return MytestStructFreightBaseCharge
	 */
	public function setBaseCharges($_baseCharges)
	{
		return ($this->BaseCharges = $_baseCharges);
	}
	/**
	 * Get Notations value
	 * @return MytestStructFreightRateNotation|null
	 */
	public function getNotations()
	{
		return $this->Notations;
	}
	/**
	 * Set Notations value
	 * @param MytestStructFreightRateNotation $_notations the Notations
	 * @return MytestStructFreightRateNotation
	 */
	public function setNotations($_notations)
	{
		return ($this->Notations = $_notations);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructFreightRateDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>