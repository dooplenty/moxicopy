<?php
/**
 * File for class MytestEnumFreightCollectTermsType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumFreightCollectTermsType originally named FreightCollectTermsType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumFreightCollectTermsType extends MytestWsdlClass
{
	/**
	 * Constant for value 'NON_RECOURSE_SHIPPER_SIGNED'
	 * @return string 'NON_RECOURSE_SHIPPER_SIGNED'
	 */
	const VALUE_NON_RECOURSE_SHIPPER_SIGNED = 'NON_RECOURSE_SHIPPER_SIGNED';
	/**
	 * Constant for value 'STANDARD'
	 * @return string 'STANDARD'
	 */
	const VALUE_STANDARD = 'STANDARD';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumFreightCollectTermsType::VALUE_NON_RECOURSE_SHIPPER_SIGNED
	 * @uses MytestEnumFreightCollectTermsType::VALUE_STANDARD
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumFreightCollectTermsType::VALUE_NON_RECOURSE_SHIPPER_SIGNED,MytestEnumFreightCollectTermsType::VALUE_STANDARD));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>