<?php
/**
 * File for class MytestEnumFreightBaseChargeCalculationType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumFreightBaseChargeCalculationType originally named FreightBaseChargeCalculationType
 * Documentation : Specifies the way in which base charges for a Freight shipment or shipment leg are calculated.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumFreightBaseChargeCalculationType extends MytestWsdlClass
{
	/**
	 * Constant for value 'LINE_ITEMS'
	 * @return string 'LINE_ITEMS'
	 */
	const VALUE_LINE_ITEMS = 'LINE_ITEMS';
	/**
	 * Constant for value 'UNIT_PRICING'
	 * @return string 'UNIT_PRICING'
	 */
	const VALUE_UNIT_PRICING = 'UNIT_PRICING';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumFreightBaseChargeCalculationType::VALUE_LINE_ITEMS
	 * @uses MytestEnumFreightBaseChargeCalculationType::VALUE_UNIT_PRICING
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumFreightBaseChargeCalculationType::VALUE_LINE_ITEMS,MytestEnumFreightBaseChargeCalculationType::VALUE_UNIT_PRICING));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>