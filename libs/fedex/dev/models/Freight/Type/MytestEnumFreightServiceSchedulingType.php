<?php
/**
 * File for class MytestEnumFreightServiceSchedulingType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumFreightServiceSchedulingType originally named FreightServiceSchedulingType
 * Documentation : Specifies the type of service scheduling offered from a Freight or National Freight Service Center to a customer-supplied address.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumFreightServiceSchedulingType extends MytestWsdlClass
{
	/**
	 * Constant for value 'LIMITED'
	 * @return string 'LIMITED'
	 */
	const VALUE_LIMITED = 'LIMITED';
	/**
	 * Constant for value 'STANDARD'
	 * @return string 'STANDARD'
	 */
	const VALUE_STANDARD = 'STANDARD';
	/**
	 * Constant for value 'WILL_CALL'
	 * @return string 'WILL_CALL'
	 */
	const VALUE_WILL_CALL = 'WILL_CALL';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumFreightServiceSchedulingType::VALUE_LIMITED
	 * @uses MytestEnumFreightServiceSchedulingType::VALUE_STANDARD
	 * @uses MytestEnumFreightServiceSchedulingType::VALUE_WILL_CALL
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumFreightServiceSchedulingType::VALUE_LIMITED,MytestEnumFreightServiceSchedulingType::VALUE_STANDARD,MytestEnumFreightServiceSchedulingType::VALUE_WILL_CALL));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>