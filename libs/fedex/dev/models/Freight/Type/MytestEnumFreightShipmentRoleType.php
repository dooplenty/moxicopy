<?php
/**
 * File for class MytestEnumFreightShipmentRoleType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumFreightShipmentRoleType originally named FreightShipmentRoleType
 * Documentation : Indicates the role of the party submitting the transaction.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumFreightShipmentRoleType extends MytestWsdlClass
{
	/**
	 * Constant for value 'CONSIGNEE'
	 * @return string 'CONSIGNEE'
	 */
	const VALUE_CONSIGNEE = 'CONSIGNEE';
	/**
	 * Constant for value 'SHIPPER'
	 * @return string 'SHIPPER'
	 */
	const VALUE_SHIPPER = 'SHIPPER';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumFreightShipmentRoleType::VALUE_CONSIGNEE
	 * @uses MytestEnumFreightShipmentRoleType::VALUE_SHIPPER
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumFreightShipmentRoleType::VALUE_CONSIGNEE,MytestEnumFreightShipmentRoleType::VALUE_SHIPPER));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>