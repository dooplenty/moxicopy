<?php
/**
 * File for class MytestEnumFreightChargeBasisType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumFreightChargeBasisType originally named FreightChargeBasisType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumFreightChargeBasisType extends MytestWsdlClass
{
	/**
	 * Constant for value 'CWT'
	 * @return string 'CWT'
	 */
	const VALUE_CWT = 'CWT';
	/**
	 * Constant for value 'FLAT'
	 * @return string 'FLAT'
	 */
	const VALUE_FLAT = 'FLAT';
	/**
	 * Constant for value 'MINIMUM'
	 * @return string 'MINIMUM'
	 */
	const VALUE_MINIMUM = 'MINIMUM';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumFreightChargeBasisType::VALUE_CWT
	 * @uses MytestEnumFreightChargeBasisType::VALUE_FLAT
	 * @uses MytestEnumFreightChargeBasisType::VALUE_MINIMUM
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumFreightChargeBasisType::VALUE_CWT,MytestEnumFreightChargeBasisType::VALUE_FLAT,MytestEnumFreightChargeBasisType::VALUE_MINIMUM));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>