<?php
/**
 * File for class MytestEnumFreightOnValueType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumFreightOnValueType originally named FreightOnValueType
 * Documentation : Identifies responsibilities with respect to loss, damage, etc.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumFreightOnValueType extends MytestWsdlClass
{
	/**
	 * Constant for value 'CARRIER_RISK'
	 * @return string 'CARRIER_RISK'
	 */
	const VALUE_CARRIER_RISK = 'CARRIER_RISK';
	/**
	 * Constant for value 'OWN_RISK'
	 * @return string 'OWN_RISK'
	 */
	const VALUE_OWN_RISK = 'OWN_RISK';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumFreightOnValueType::VALUE_CARRIER_RISK
	 * @uses MytestEnumFreightOnValueType::VALUE_OWN_RISK
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumFreightOnValueType::VALUE_CARRIER_RISK,MytestEnumFreightOnValueType::VALUE_OWN_RISK));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>