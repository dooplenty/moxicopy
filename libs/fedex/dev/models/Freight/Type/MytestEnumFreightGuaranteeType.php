<?php
/**
 * File for class MytestEnumFreightGuaranteeType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumFreightGuaranteeType originally named FreightGuaranteeType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumFreightGuaranteeType extends MytestWsdlClass
{
	/**
	 * Constant for value 'GUARANTEED_DATE'
	 * @return string 'GUARANTEED_DATE'
	 */
	const VALUE_GUARANTEED_DATE = 'GUARANTEED_DATE';
	/**
	 * Constant for value 'GUARANTEED_MORNING'
	 * @return string 'GUARANTEED_MORNING'
	 */
	const VALUE_GUARANTEED_MORNING = 'GUARANTEED_MORNING';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumFreightGuaranteeType::VALUE_GUARANTEED_DATE
	 * @uses MytestEnumFreightGuaranteeType::VALUE_GUARANTEED_MORNING
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumFreightGuaranteeType::VALUE_GUARANTEED_DATE,MytestEnumFreightGuaranteeType::VALUE_GUARANTEED_MORNING));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>