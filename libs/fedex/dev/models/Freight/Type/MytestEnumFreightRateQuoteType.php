<?php
/**
 * File for class MytestEnumFreightRateQuoteType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumFreightRateQuoteType originally named FreightRateQuoteType
 * Documentation : Specifies the type of rate quote
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumFreightRateQuoteType extends MytestWsdlClass
{
	/**
	 * Constant for value 'AUTOMATED'
	 * @return string 'AUTOMATED'
	 */
	const VALUE_AUTOMATED = 'AUTOMATED';
	/**
	 * Constant for value 'MANUAL'
	 * @return string 'MANUAL'
	 */
	const VALUE_MANUAL = 'MANUAL';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumFreightRateQuoteType::VALUE_AUTOMATED
	 * @uses MytestEnumFreightRateQuoteType::VALUE_MANUAL
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumFreightRateQuoteType::VALUE_AUTOMATED,MytestEnumFreightRateQuoteType::VALUE_MANUAL));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>