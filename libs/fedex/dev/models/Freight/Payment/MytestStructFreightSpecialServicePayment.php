<?php
/**
 * File for class MytestStructFreightSpecialServicePayment
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructFreightSpecialServicePayment originally named FreightSpecialServicePayment
 * Documentation : Specifies which party will be responsible for payment of any surcharges for Freight special services for which split billing is allowed.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructFreightSpecialServicePayment extends MytestWsdlClass
{
	/**
	 * The SpecialService
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the special service.
	 * - minOccurs : 0
	 * @var MytestEnumShipmentSpecialServiceType
	 */
	public $SpecialService;
	/**
	 * The PaymentType
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates who will pay for the special service.
	 * - minOccurs : 0
	 * @var MytestEnumFreightShipmentRoleType
	 */
	public $PaymentType;
	/**
	 * Constructor method for FreightSpecialServicePayment
	 * @see parent::__construct()
	 * @param MytestEnumShipmentSpecialServiceType $_specialService
	 * @param MytestEnumFreightShipmentRoleType $_paymentType
	 * @return MytestStructFreightSpecialServicePayment
	 */
	public function __construct($_specialService = NULL,$_paymentType = NULL)
	{
		parent::__construct(array('SpecialService'=>$_specialService,'PaymentType'=>$_paymentType));
	}
	/**
	 * Get SpecialService value
	 * @return MytestEnumShipmentSpecialServiceType|null
	 */
	public function getSpecialService()
	{
		return $this->SpecialService;
	}
	/**
	 * Set SpecialService value
	 * @uses MytestEnumShipmentSpecialServiceType::valueIsValid()
	 * @param MytestEnumShipmentSpecialServiceType $_specialService the SpecialService
	 * @return MytestEnumShipmentSpecialServiceType
	 */
	public function setSpecialService($_specialService)
	{
		if(!MytestEnumShipmentSpecialServiceType::valueIsValid($_specialService))
		{
			return false;
		}
		return ($this->SpecialService = $_specialService);
	}
	/**
	 * Get PaymentType value
	 * @return MytestEnumFreightShipmentRoleType|null
	 */
	public function getPaymentType()
	{
		return $this->PaymentType;
	}
	/**
	 * Set PaymentType value
	 * @uses MytestEnumFreightShipmentRoleType::valueIsValid()
	 * @param MytestEnumFreightShipmentRoleType $_paymentType the PaymentType
	 * @return MytestEnumFreightShipmentRoleType
	 */
	public function setPaymentType($_paymentType)
	{
		if(!MytestEnumFreightShipmentRoleType::valueIsValid($_paymentType))
		{
			return false;
		}
		return ($this->PaymentType = $_paymentType);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructFreightSpecialServicePayment
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>