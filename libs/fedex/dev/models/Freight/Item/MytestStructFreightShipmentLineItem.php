<?php
/**
 * File for class MytestStructFreightShipmentLineItem
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructFreightShipmentLineItem originally named FreightShipmentLineItem
 * Documentation : Description of an individual commodity or class of content in a shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructFreightShipmentLineItem extends MytestWsdlClass
{
	/**
	 * The FreightClass
	 * Meta informations extracted from the WSDL
	 * - documentation : Freight class for this line item.
	 * - minOccurs : 0
	 * @var MytestEnumFreightClassType
	 */
	public $FreightClass;
	/**
	 * The Packaging
	 * Meta informations extracted from the WSDL
	 * - documentation : Specification of handling-unit packaging for this commodity or class line.
	 * - minOccurs : 0
	 * @var MytestEnumPhysicalPackagingType
	 */
	public $Packaging;
	/**
	 * The Description
	 * Meta informations extracted from the WSDL
	 * - documentation : Customer-provided description for this commodity or class line.
	 * - minOccurs : 0
	 * @var string
	 */
	public $Description;
	/**
	 * The Weight
	 * Meta informations extracted from the WSDL
	 * - documentation : Weight for this commodity or class line.
	 * - minOccurs : 0
	 * @var MytestStructWeight
	 */
	public $Weight;
	/**
	 * The Dimensions
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructDimensions
	 */
	public $Dimensions;
	/**
	 * The Volume
	 * Meta informations extracted from the WSDL
	 * - documentation : Volume (cubic measure) for this commodity or class line.
	 * - minOccurs : 0
	 * @var MytestStructVolume
	 */
	public $Volume;
	/**
	 * Constructor method for FreightShipmentLineItem
	 * @see parent::__construct()
	 * @param MytestEnumFreightClassType $_freightClass
	 * @param MytestEnumPhysicalPackagingType $_packaging
	 * @param string $_description
	 * @param MytestStructWeight $_weight
	 * @param MytestStructDimensions $_dimensions
	 * @param MytestStructVolume $_volume
	 * @return MytestStructFreightShipmentLineItem
	 */
	public function __construct($_freightClass = NULL,$_packaging = NULL,$_description = NULL,$_weight = NULL,$_dimensions = NULL,$_volume = NULL)
	{
		parent::__construct(array('FreightClass'=>$_freightClass,'Packaging'=>$_packaging,'Description'=>$_description,'Weight'=>$_weight,'Dimensions'=>$_dimensions,'Volume'=>$_volume));
	}
	/**
	 * Get FreightClass value
	 * @return MytestEnumFreightClassType|null
	 */
	public function getFreightClass()
	{
		return $this->FreightClass;
	}
	/**
	 * Set FreightClass value
	 * @uses MytestEnumFreightClassType::valueIsValid()
	 * @param MytestEnumFreightClassType $_freightClass the FreightClass
	 * @return MytestEnumFreightClassType
	 */
	public function setFreightClass($_freightClass)
	{
		if(!MytestEnumFreightClassType::valueIsValid($_freightClass))
		{
			return false;
		}
		return ($this->FreightClass = $_freightClass);
	}
	/**
	 * Get Packaging value
	 * @return MytestEnumPhysicalPackagingType|null
	 */
	public function getPackaging()
	{
		return $this->Packaging;
	}
	/**
	 * Set Packaging value
	 * @uses MytestEnumPhysicalPackagingType::valueIsValid()
	 * @param MytestEnumPhysicalPackagingType $_packaging the Packaging
	 * @return MytestEnumPhysicalPackagingType
	 */
	public function setPackaging($_packaging)
	{
		if(!MytestEnumPhysicalPackagingType::valueIsValid($_packaging))
		{
			return false;
		}
		return ($this->Packaging = $_packaging);
	}
	/**
	 * Get Description value
	 * @return string|null
	 */
	public function getDescription()
	{
		return $this->Description;
	}
	/**
	 * Set Description value
	 * @param string $_description the Description
	 * @return string
	 */
	public function setDescription($_description)
	{
		return ($this->Description = $_description);
	}
	/**
	 * Get Weight value
	 * @return MytestStructWeight|null
	 */
	public function getWeight()
	{
		return $this->Weight;
	}
	/**
	 * Set Weight value
	 * @param MytestStructWeight $_weight the Weight
	 * @return MytestStructWeight
	 */
	public function setWeight($_weight)
	{
		return ($this->Weight = $_weight);
	}
	/**
	 * Get Dimensions value
	 * @return MytestStructDimensions|null
	 */
	public function getDimensions()
	{
		return $this->Dimensions;
	}
	/**
	 * Set Dimensions value
	 * @param MytestStructDimensions $_dimensions the Dimensions
	 * @return MytestStructDimensions
	 */
	public function setDimensions($_dimensions)
	{
		return ($this->Dimensions = $_dimensions);
	}
	/**
	 * Get Volume value
	 * @return MytestStructVolume|null
	 */
	public function getVolume()
	{
		return $this->Volume;
	}
	/**
	 * Set Volume value
	 * @param MytestStructVolume $_volume the Volume
	 * @return MytestStructVolume
	 */
	public function setVolume($_volume)
	{
		return ($this->Volume = $_volume);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructFreightShipmentLineItem
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>