<?php
/**
 * File for class MytestStructFreightRateNotation
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructFreightRateNotation originally named FreightRateNotation
 * Documentation : Additional non-monetary data returned with Freight rates.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructFreightRateNotation extends MytestWsdlClass
{
	/**
	 * The Code
	 * Meta informations extracted from the WSDL
	 * - documentation : Unique identifier for notation.
	 * - minOccurs : 0
	 * @var string
	 */
	public $Code;
	/**
	 * The Description
	 * Meta informations extracted from the WSDL
	 * - documentation : Human-readable explanation of notation.
	 * - minOccurs : 0
	 * @var string
	 */
	public $Description;
	/**
	 * Constructor method for FreightRateNotation
	 * @see parent::__construct()
	 * @param string $_code
	 * @param string $_description
	 * @return MytestStructFreightRateNotation
	 */
	public function __construct($_code = NULL,$_description = NULL)
	{
		parent::__construct(array('Code'=>$_code,'Description'=>$_description));
	}
	/**
	 * Get Code value
	 * @return string|null
	 */
	public function getCode()
	{
		return $this->Code;
	}
	/**
	 * Set Code value
	 * @param string $_code the Code
	 * @return string
	 */
	public function setCode($_code)
	{
		return ($this->Code = $_code);
	}
	/**
	 * Get Description value
	 * @return string|null
	 */
	public function getDescription()
	{
		return $this->Description;
	}
	/**
	 * Set Description value
	 * @param string $_description the Description
	 * @return string
	 */
	public function setDescription($_description)
	{
		return ($this->Description = $_description);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructFreightRateNotation
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>