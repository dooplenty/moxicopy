<?php
/**
 * File for class MytestStructPayor
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructPayor originally named Payor
 * Documentation : Descriptive data identifying the party responsible for payment for a service.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructPayor extends MytestWsdlClass
{
	/**
	 * The ResponsibleParty
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructParty
	 */
	public $ResponsibleParty;
	/**
	 * Constructor method for Payor
	 * @see parent::__construct()
	 * @param MytestStructParty $_responsibleParty
	 * @return MytestStructPayor
	 */
	public function __construct($_responsibleParty = NULL)
	{
		parent::__construct(array('ResponsibleParty'=>$_responsibleParty));
	}
	/**
	 * Get ResponsibleParty value
	 * @return MytestStructParty|null
	 */
	public function getResponsibleParty()
	{
		return $this->ResponsibleParty;
	}
	/**
	 * Set ResponsibleParty value
	 * @param MytestStructParty $_responsibleParty the ResponsibleParty
	 * @return MytestStructParty
	 */
	public function setResponsibleParty($_responsibleParty)
	{
		return ($this->ResponsibleParty = $_responsibleParty);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructPayor
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>