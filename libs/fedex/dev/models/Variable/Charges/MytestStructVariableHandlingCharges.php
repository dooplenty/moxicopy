<?php
/**
 * File for class MytestStructVariableHandlingCharges
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructVariableHandlingCharges originally named VariableHandlingCharges
 * Documentation : The variable handling charges calculated based on the type variable handling charges requested.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructVariableHandlingCharges extends MytestWsdlClass
{
	/**
	 * The VariableHandlingCharge
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $VariableHandlingCharge;
	/**
	 * The FixedVariableHandlingCharge
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $FixedVariableHandlingCharge;
	/**
	 * The PercentVariableHandlingCharge
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $PercentVariableHandlingCharge;
	/**
	 * The TotalCustomerCharge
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TotalCustomerCharge;
	/**
	 * Constructor method for VariableHandlingCharges
	 * @see parent::__construct()
	 * @param MytestStructMoney $_variableHandlingCharge
	 * @param MytestStructMoney $_fixedVariableHandlingCharge
	 * @param MytestStructMoney $_percentVariableHandlingCharge
	 * @param MytestStructMoney $_totalCustomerCharge
	 * @return MytestStructVariableHandlingCharges
	 */
	public function __construct($_variableHandlingCharge = NULL,$_fixedVariableHandlingCharge = NULL,$_percentVariableHandlingCharge = NULL,$_totalCustomerCharge = NULL)
	{
		parent::__construct(array('VariableHandlingCharge'=>$_variableHandlingCharge,'FixedVariableHandlingCharge'=>$_fixedVariableHandlingCharge,'PercentVariableHandlingCharge'=>$_percentVariableHandlingCharge,'TotalCustomerCharge'=>$_totalCustomerCharge));
	}
	/**
	 * Get VariableHandlingCharge value
	 * @return MytestStructMoney|null
	 */
	public function getVariableHandlingCharge()
	{
		return $this->VariableHandlingCharge;
	}
	/**
	 * Set VariableHandlingCharge value
	 * @param MytestStructMoney $_variableHandlingCharge the VariableHandlingCharge
	 * @return MytestStructMoney
	 */
	public function setVariableHandlingCharge($_variableHandlingCharge)
	{
		return ($this->VariableHandlingCharge = $_variableHandlingCharge);
	}
	/**
	 * Get FixedVariableHandlingCharge value
	 * @return MytestStructMoney|null
	 */
	public function getFixedVariableHandlingCharge()
	{
		return $this->FixedVariableHandlingCharge;
	}
	/**
	 * Set FixedVariableHandlingCharge value
	 * @param MytestStructMoney $_fixedVariableHandlingCharge the FixedVariableHandlingCharge
	 * @return MytestStructMoney
	 */
	public function setFixedVariableHandlingCharge($_fixedVariableHandlingCharge)
	{
		return ($this->FixedVariableHandlingCharge = $_fixedVariableHandlingCharge);
	}
	/**
	 * Get PercentVariableHandlingCharge value
	 * @return MytestStructMoney|null
	 */
	public function getPercentVariableHandlingCharge()
	{
		return $this->PercentVariableHandlingCharge;
	}
	/**
	 * Set PercentVariableHandlingCharge value
	 * @param MytestStructMoney $_percentVariableHandlingCharge the PercentVariableHandlingCharge
	 * @return MytestStructMoney
	 */
	public function setPercentVariableHandlingCharge($_percentVariableHandlingCharge)
	{
		return ($this->PercentVariableHandlingCharge = $_percentVariableHandlingCharge);
	}
	/**
	 * Get TotalCustomerCharge value
	 * @return MytestStructMoney|null
	 */
	public function getTotalCustomerCharge()
	{
		return $this->TotalCustomerCharge;
	}
	/**
	 * Set TotalCustomerCharge value
	 * @param MytestStructMoney $_totalCustomerCharge the TotalCustomerCharge
	 * @return MytestStructMoney
	 */
	public function setTotalCustomerCharge($_totalCustomerCharge)
	{
		return ($this->TotalCustomerCharge = $_totalCustomerCharge);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructVariableHandlingCharges
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>