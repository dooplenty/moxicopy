<?php
/**
 * File for class MytestStructVariableHandlingChargeDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructVariableHandlingChargeDetail originally named VariableHandlingChargeDetail
 * Documentation : This definition of variable handling charge detail is intended for use in Jan 2011 corp load.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructVariableHandlingChargeDetail extends MytestWsdlClass
{
	/**
	 * The FixedValue
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $FixedValue;
	/**
	 * The PercentValue
	 * Meta informations extracted from the WSDL
	 * - documentation : Actual percentage (10 means 10%, which is a mutiplier of 0.1)
	 * - minOccurs : 0
	 * @var decimal
	 */
	public $PercentValue;
	/**
	 * The RateElementBasis
	 * Meta informations extracted from the WSDL
	 * - documentation : Select the value from a set of rate data to which the percentage is applied.
	 * - minOccurs : 0
	 * @var MytestEnumRateElementBasisType
	 */
	public $RateElementBasis;
	/**
	 * The RateTypeBasis
	 * Meta informations extracted from the WSDL
	 * - documentation : Select the type of rate from which the element is to be selected.
	 * - minOccurs : 0
	 * @var MytestEnumRateTypeBasisType
	 */
	public $RateTypeBasis;
	/**
	 * Constructor method for VariableHandlingChargeDetail
	 * @see parent::__construct()
	 * @param MytestStructMoney $_fixedValue
	 * @param decimal $_percentValue
	 * @param MytestEnumRateElementBasisType $_rateElementBasis
	 * @param MytestEnumRateTypeBasisType $_rateTypeBasis
	 * @return MytestStructVariableHandlingChargeDetail
	 */
	public function __construct($_fixedValue = NULL,$_percentValue = NULL,$_rateElementBasis = NULL,$_rateTypeBasis = NULL)
	{
		parent::__construct(array('FixedValue'=>$_fixedValue,'PercentValue'=>$_percentValue,'RateElementBasis'=>$_rateElementBasis,'RateTypeBasis'=>$_rateTypeBasis));
	}
	/**
	 * Get FixedValue value
	 * @return MytestStructMoney|null
	 */
	public function getFixedValue()
	{
		return $this->FixedValue;
	}
	/**
	 * Set FixedValue value
	 * @param MytestStructMoney $_fixedValue the FixedValue
	 * @return MytestStructMoney
	 */
	public function setFixedValue($_fixedValue)
	{
		return ($this->FixedValue = $_fixedValue);
	}
	/**
	 * Get PercentValue value
	 * @return decimal|null
	 */
	public function getPercentValue()
	{
		return $this->PercentValue;
	}
	/**
	 * Set PercentValue value
	 * @param decimal $_percentValue the PercentValue
	 * @return decimal
	 */
	public function setPercentValue($_percentValue)
	{
		return ($this->PercentValue = $_percentValue);
	}
	/**
	 * Get RateElementBasis value
	 * @return MytestEnumRateElementBasisType|null
	 */
	public function getRateElementBasis()
	{
		return $this->RateElementBasis;
	}
	/**
	 * Set RateElementBasis value
	 * @uses MytestEnumRateElementBasisType::valueIsValid()
	 * @param MytestEnumRateElementBasisType $_rateElementBasis the RateElementBasis
	 * @return MytestEnumRateElementBasisType
	 */
	public function setRateElementBasis($_rateElementBasis)
	{
		if(!MytestEnumRateElementBasisType::valueIsValid($_rateElementBasis))
		{
			return false;
		}
		return ($this->RateElementBasis = $_rateElementBasis);
	}
	/**
	 * Get RateTypeBasis value
	 * @return MytestEnumRateTypeBasisType|null
	 */
	public function getRateTypeBasis()
	{
		return $this->RateTypeBasis;
	}
	/**
	 * Set RateTypeBasis value
	 * @uses MytestEnumRateTypeBasisType::valueIsValid()
	 * @param MytestEnumRateTypeBasisType $_rateTypeBasis the RateTypeBasis
	 * @return MytestEnumRateTypeBasisType
	 */
	public function setRateTypeBasis($_rateTypeBasis)
	{
		if(!MytestEnumRateTypeBasisType::valueIsValid($_rateTypeBasis))
		{
			return false;
		}
		return ($this->RateTypeBasis = $_rateTypeBasis);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructVariableHandlingChargeDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>