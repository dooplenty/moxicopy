<?php
/**
 * File for class MytestStructRateRequest
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructRateRequest originally named RateRequest
 * Documentation : Descriptive data sent to FedEx by a customer in order to rate a package/shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructRateRequest extends MytestWsdlClass
{
	/**
	 * The WebAuthenticationDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Descriptive data to be used in authentication of the sender's identity (and right to use FedEx web services).
	 * - minOccurs : 1
	 * @var MytestStructWebAuthenticationDetail
	 */
	public $WebAuthenticationDetail;
	/**
	 * The ClientDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Descriptive data identifying the client submitting the transaction.
	 * - minOccurs : 1
	 * @var MytestStructClientDetail
	 */
	public $ClientDetail;
	/**
	 * The Version
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the version/level of a service operation expected by a caller (in each request) and performed by the callee (in each reply).
	 * - minOccurs : 1
	 * @var MytestStructVersionId
	 */
	public $Version;
	/**
	 * The RequestedShipment
	 * Meta informations extracted from the WSDL
	 * - documentation : The shipment for which a rate quote (or rate-shopping comparison) is desired.
	 * - minOccurs : 1
	 * @var MytestStructRequestedShipment
	 */
	public $RequestedShipment;
	/**
	 * The TransactionDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructTransactionDetail
	 */
	public $TransactionDetail;
	/**
	 * The ReturnTransitAndCommit
	 * Meta informations extracted from the WSDL
	 * - documentation : Allows the caller to specify that the transit time and commit data are to be returned in the reply.
	 * - minOccurs : 0
	 * @var boolean
	 */
	public $ReturnTransitAndCommit;
	/**
	 * The CarrierCodes
	 * Meta informations extracted from the WSDL
	 * - documentation : Candidate carriers for rate-shopping use case. This field is only considered if requestedShipment/serviceType is omitted.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumCarrierCodeType
	 */
	public $CarrierCodes;
	/**
	 * The VariableOptions
	 * Meta informations extracted from the WSDL
	 * - documentation : Contains zero or more service options whose combinations are to be considered when replying with available services.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumServiceOptionType
	 */
	public $VariableOptions;
	/**
	 * Constructor method for RateRequest
	 * @see parent::__construct()
	 * @param MytestStructWebAuthenticationDetail $_webAuthenticationDetail
	 * @param MytestStructClientDetail $_clientDetail
	 * @param MytestStructVersionId $_version
	 * @param MytestStructRequestedShipment $_requestedShipment
	 * @param MytestStructTransactionDetail $_transactionDetail
	 * @param boolean $_returnTransitAndCommit
	 * @param MytestEnumCarrierCodeType $_carrierCodes
	 * @param MytestEnumServiceOptionType $_variableOptions
	 * @return MytestStructRateRequest
	 */
	public function __construct($_webAuthenticationDetail,$_clientDetail,$_version,$_requestedShipment,$_transactionDetail = NULL,$_returnTransitAndCommit = NULL,$_carrierCodes = NULL,$_variableOptions = NULL)
	{
		parent::__construct(array('WebAuthenticationDetail'=>$_webAuthenticationDetail,'ClientDetail'=>$_clientDetail,'Version'=>$_version,'RequestedShipment'=>$_requestedShipment,'TransactionDetail'=>$_transactionDetail,'ReturnTransitAndCommit'=>$_returnTransitAndCommit,'CarrierCodes'=>$_carrierCodes,'VariableOptions'=>$_variableOptions));
	}
	/**
	 * Get WebAuthenticationDetail value
	 * @return MytestStructWebAuthenticationDetail
	 */
	public function getWebAuthenticationDetail()
	{
		return $this->WebAuthenticationDetail;
	}
	/**
	 * Set WebAuthenticationDetail value
	 * @param MytestStructWebAuthenticationDetail $_webAuthenticationDetail the WebAuthenticationDetail
	 * @return MytestStructWebAuthenticationDetail
	 */
	public function setWebAuthenticationDetail($_webAuthenticationDetail)
	{
		return ($this->WebAuthenticationDetail = $_webAuthenticationDetail);
	}
	/**
	 * Get ClientDetail value
	 * @return MytestStructClientDetail
	 */
	public function getClientDetail()
	{
		return $this->ClientDetail;
	}
	/**
	 * Set ClientDetail value
	 * @param MytestStructClientDetail $_clientDetail the ClientDetail
	 * @return MytestStructClientDetail
	 */
	public function setClientDetail($_clientDetail)
	{
		return ($this->ClientDetail = $_clientDetail);
	}
	/**
	 * Get Version value
	 * @return MytestStructVersionId
	 */
	public function getVersion()
	{
		return $this->Version;
	}
	/**
	 * Set Version value
	 * @param MytestStructVersionId $_version the Version
	 * @return MytestStructVersionId
	 */
	public function setVersion($_version)
	{
		return ($this->Version = $_version);
	}
	/**
	 * Get RequestedShipment value
	 * @return MytestStructRequestedShipment
	 */
	public function getRequestedShipment()
	{
		return $this->RequestedShipment;
	}
	/**
	 * Set RequestedShipment value
	 * @param MytestStructRequestedShipment $_requestedShipment the RequestedShipment
	 * @return MytestStructRequestedShipment
	 */
	public function setRequestedShipment($_requestedShipment)
	{
		return ($this->RequestedShipment = $_requestedShipment);
	}
	/**
	 * Get TransactionDetail value
	 * @return MytestStructTransactionDetail|null
	 */
	public function getTransactionDetail()
	{
		return $this->TransactionDetail;
	}
	/**
	 * Set TransactionDetail value
	 * @param MytestStructTransactionDetail $_transactionDetail the TransactionDetail
	 * @return MytestStructTransactionDetail
	 */
	public function setTransactionDetail($_transactionDetail)
	{
		return ($this->TransactionDetail = $_transactionDetail);
	}
	/**
	 * Get ReturnTransitAndCommit value
	 * @return boolean|null
	 */
	public function getReturnTransitAndCommit()
	{
		return $this->ReturnTransitAndCommit;
	}
	/**
	 * Set ReturnTransitAndCommit value
	 * @param boolean $_returnTransitAndCommit the ReturnTransitAndCommit
	 * @return boolean
	 */
	public function setReturnTransitAndCommit($_returnTransitAndCommit)
	{
		return ($this->ReturnTransitAndCommit = $_returnTransitAndCommit);
	}
	/**
	 * Get CarrierCodes value
	 * @return MytestEnumCarrierCodeType|null
	 */
	public function getCarrierCodes()
	{
		return $this->CarrierCodes;
	}
	/**
	 * Set CarrierCodes value
	 * @uses MytestEnumCarrierCodeType::valueIsValid()
	 * @param MytestEnumCarrierCodeType $_carrierCodes the CarrierCodes
	 * @return MytestEnumCarrierCodeType
	 */
	public function setCarrierCodes($_carrierCodes)
	{
		if(!MytestEnumCarrierCodeType::valueIsValid($_carrierCodes))
		{
			return false;
		}
		return ($this->CarrierCodes = $_carrierCodes);
	}
	/**
	 * Get VariableOptions value
	 * @return MytestEnumServiceOptionType|null
	 */
	public function getVariableOptions()
	{
		return $this->VariableOptions;
	}
	/**
	 * Set VariableOptions value
	 * @uses MytestEnumServiceOptionType::valueIsValid()
	 * @param MytestEnumServiceOptionType $_variableOptions the VariableOptions
	 * @return MytestEnumServiceOptionType
	 */
	public function setVariableOptions($_variableOptions)
	{
		if(!MytestEnumServiceOptionType::valueIsValid($_variableOptions))
		{
			return false;
		}
		return ($this->VariableOptions = $_variableOptions);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructRateRequest
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>