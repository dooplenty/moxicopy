<?php
/**
 * File for class MytestEnumRateTypeBasisType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumRateTypeBasisType originally named RateTypeBasisType
 * Documentation : Select the type of rate from which the element is to be selected.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumRateTypeBasisType extends MytestWsdlClass
{
	/**
	 * Constant for value 'ACCOUNT'
	 * @return string 'ACCOUNT'
	 */
	const VALUE_ACCOUNT = 'ACCOUNT';
	/**
	 * Constant for value 'LIST'
	 * @return string 'LIST'
	 */
	const VALUE_LIST = 'LIST';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumRateTypeBasisType::VALUE_ACCOUNT
	 * @uses MytestEnumRateTypeBasisType::VALUE_LIST
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumRateTypeBasisType::VALUE_ACCOUNT,MytestEnumRateTypeBasisType::VALUE_LIST));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>