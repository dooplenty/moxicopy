<?php
/**
 * File for class MytestEnumRateRequestType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumRateRequestType originally named RateRequestType
 * Documentation : Indicates the type of rates to be returned.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumRateRequestType extends MytestWsdlClass
{
	/**
	 * Constant for value 'ACCOUNT'
	 * @return string 'ACCOUNT'
	 */
	const VALUE_ACCOUNT = 'ACCOUNT';
	/**
	 * Constant for value 'LIST'
	 * @return string 'LIST'
	 */
	const VALUE_LIST = 'LIST';
	/**
	 * Constant for value 'PREFERRED'
	 * @return string 'PREFERRED'
	 */
	const VALUE_PREFERRED = 'PREFERRED';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumRateRequestType::VALUE_ACCOUNT
	 * @uses MytestEnumRateRequestType::VALUE_LIST
	 * @uses MytestEnumRateRequestType::VALUE_PREFERRED
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumRateRequestType::VALUE_ACCOUNT,MytestEnumRateRequestType::VALUE_LIST,MytestEnumRateRequestType::VALUE_PREFERRED));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>