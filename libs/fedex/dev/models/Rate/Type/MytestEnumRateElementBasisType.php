<?php
/**
 * File for class MytestEnumRateElementBasisType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumRateElementBasisType originally named RateElementBasisType
 * Documentation : Selects the value from a set of rate data to which the percentage is applied.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumRateElementBasisType extends MytestWsdlClass
{
	/**
	 * Constant for value 'BASE_CHARGE'
	 * @return string 'BASE_CHARGE'
	 */
	const VALUE_BASE_CHARGE = 'BASE_CHARGE';
	/**
	 * Constant for value 'NET_CHARGE'
	 * @return string 'NET_CHARGE'
	 */
	const VALUE_NET_CHARGE = 'NET_CHARGE';
	/**
	 * Constant for value 'NET_CHARGE_EXCLUDING_TAXES'
	 * @return string 'NET_CHARGE_EXCLUDING_TAXES'
	 */
	const VALUE_NET_CHARGE_EXCLUDING_TAXES = 'NET_CHARGE_EXCLUDING_TAXES';
	/**
	 * Constant for value 'NET_FREIGHT'
	 * @return string 'NET_FREIGHT'
	 */
	const VALUE_NET_FREIGHT = 'NET_FREIGHT';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumRateElementBasisType::VALUE_BASE_CHARGE
	 * @uses MytestEnumRateElementBasisType::VALUE_NET_CHARGE
	 * @uses MytestEnumRateElementBasisType::VALUE_NET_CHARGE_EXCLUDING_TAXES
	 * @uses MytestEnumRateElementBasisType::VALUE_NET_FREIGHT
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumRateElementBasisType::VALUE_BASE_CHARGE,MytestEnumRateElementBasisType::VALUE_NET_CHARGE,MytestEnumRateElementBasisType::VALUE_NET_CHARGE_EXCLUDING_TAXES,MytestEnumRateElementBasisType::VALUE_NET_FREIGHT));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>