<?php
/**
 * File for class MytestEnumRateDimensionalDivisorType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumRateDimensionalDivisorType originally named RateDimensionalDivisorType
 * Documentation : Indicates the reason that a dim divisor value was chose.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumRateDimensionalDivisorType extends MytestWsdlClass
{
	/**
	 * Constant for value 'COUNTRY'
	 * @return string 'COUNTRY'
	 */
	const VALUE_COUNTRY = 'COUNTRY';
	/**
	 * Constant for value 'CUSTOMER'
	 * @return string 'CUSTOMER'
	 */
	const VALUE_CUSTOMER = 'CUSTOMER';
	/**
	 * Constant for value 'OTHER'
	 * @return string 'OTHER'
	 */
	const VALUE_OTHER = 'OTHER';
	/**
	 * Constant for value 'PRODUCT'
	 * @return string 'PRODUCT'
	 */
	const VALUE_PRODUCT = 'PRODUCT';
	/**
	 * Constant for value 'WAIVED'
	 * @return string 'WAIVED'
	 */
	const VALUE_WAIVED = 'WAIVED';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumRateDimensionalDivisorType::VALUE_COUNTRY
	 * @uses MytestEnumRateDimensionalDivisorType::VALUE_CUSTOMER
	 * @uses MytestEnumRateDimensionalDivisorType::VALUE_OTHER
	 * @uses MytestEnumRateDimensionalDivisorType::VALUE_PRODUCT
	 * @uses MytestEnumRateDimensionalDivisorType::VALUE_WAIVED
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumRateDimensionalDivisorType::VALUE_COUNTRY,MytestEnumRateDimensionalDivisorType::VALUE_CUSTOMER,MytestEnumRateDimensionalDivisorType::VALUE_OTHER,MytestEnumRateDimensionalDivisorType::VALUE_PRODUCT,MytestEnumRateDimensionalDivisorType::VALUE_WAIVED));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>