<?php
/**
 * File for class MytestEnumRateDiscountType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumRateDiscountType originally named RateDiscountType
 * Documentation : Identifies the type of discount applied to the shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumRateDiscountType extends MytestWsdlClass
{
	/**
	 * Constant for value 'BONUS'
	 * @return string 'BONUS'
	 */
	const VALUE_BONUS = 'BONUS';
	/**
	 * Constant for value 'COUPON'
	 * @return string 'COUPON'
	 */
	const VALUE_COUPON = 'COUPON';
	/**
	 * Constant for value 'EARNED'
	 * @return string 'EARNED'
	 */
	const VALUE_EARNED = 'EARNED';
	/**
	 * Constant for value 'OTHER'
	 * @return string 'OTHER'
	 */
	const VALUE_OTHER = 'OTHER';
	/**
	 * Constant for value 'VOLUME'
	 * @return string 'VOLUME'
	 */
	const VALUE_VOLUME = 'VOLUME';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumRateDiscountType::VALUE_BONUS
	 * @uses MytestEnumRateDiscountType::VALUE_COUPON
	 * @uses MytestEnumRateDiscountType::VALUE_EARNED
	 * @uses MytestEnumRateDiscountType::VALUE_OTHER
	 * @uses MytestEnumRateDiscountType::VALUE_VOLUME
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumRateDiscountType::VALUE_BONUS,MytestEnumRateDiscountType::VALUE_COUPON,MytestEnumRateDiscountType::VALUE_EARNED,MytestEnumRateDiscountType::VALUE_OTHER,MytestEnumRateDiscountType::VALUE_VOLUME));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>