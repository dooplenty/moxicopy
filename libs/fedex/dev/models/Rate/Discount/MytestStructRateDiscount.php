<?php
/**
 * File for class MytestStructRateDiscount
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructRateDiscount originally named RateDiscount
 * Documentation : Identifies a discount applied to the shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructRateDiscount extends MytestWsdlClass
{
	/**
	 * The RateDiscountType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumRateDiscountType
	 */
	public $RateDiscountType;
	/**
	 * The Description
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Description;
	/**
	 * The Amount
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $Amount;
	/**
	 * The Percent
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var decimal
	 */
	public $Percent;
	/**
	 * Constructor method for RateDiscount
	 * @see parent::__construct()
	 * @param MytestEnumRateDiscountType $_rateDiscountType
	 * @param string $_description
	 * @param MytestStructMoney $_amount
	 * @param decimal $_percent
	 * @return MytestStructRateDiscount
	 */
	public function __construct($_rateDiscountType = NULL,$_description = NULL,$_amount = NULL,$_percent = NULL)
	{
		parent::__construct(array('RateDiscountType'=>$_rateDiscountType,'Description'=>$_description,'Amount'=>$_amount,'Percent'=>$_percent));
	}
	/**
	 * Get RateDiscountType value
	 * @return MytestEnumRateDiscountType|null
	 */
	public function getRateDiscountType()
	{
		return $this->RateDiscountType;
	}
	/**
	 * Set RateDiscountType value
	 * @uses MytestEnumRateDiscountType::valueIsValid()
	 * @param MytestEnumRateDiscountType $_rateDiscountType the RateDiscountType
	 * @return MytestEnumRateDiscountType
	 */
	public function setRateDiscountType($_rateDiscountType)
	{
		if(!MytestEnumRateDiscountType::valueIsValid($_rateDiscountType))
		{
			return false;
		}
		return ($this->RateDiscountType = $_rateDiscountType);
	}
	/**
	 * Get Description value
	 * @return string|null
	 */
	public function getDescription()
	{
		return $this->Description;
	}
	/**
	 * Set Description value
	 * @param string $_description the Description
	 * @return string
	 */
	public function setDescription($_description)
	{
		return ($this->Description = $_description);
	}
	/**
	 * Get Amount value
	 * @return MytestStructMoney|null
	 */
	public function getAmount()
	{
		return $this->Amount;
	}
	/**
	 * Set Amount value
	 * @param MytestStructMoney $_amount the Amount
	 * @return MytestStructMoney
	 */
	public function setAmount($_amount)
	{
		return ($this->Amount = $_amount);
	}
	/**
	 * Get Percent value
	 * @return decimal|null
	 */
	public function getPercent()
	{
		return $this->Percent;
	}
	/**
	 * Set Percent value
	 * @param decimal $_percent the Percent
	 * @return decimal
	 */
	public function setPercent($_percent)
	{
		return ($this->Percent = $_percent);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructRateDiscount
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>