<?php
/**
 * File for class MytestStructRateReplyDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructRateReplyDetail originally named RateReplyDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructRateReplyDetail extends MytestWsdlClass
{
	/**
	 * The ServiceType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumServiceType
	 */
	public $ServiceType;
	/**
	 * The PackagingType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumPackagingType
	 */
	public $PackagingType;
	/**
	 * The AppliedOptions
	 * Meta informations extracted from the WSDL
	 * - documentation : Shows the specific combination of service options combined with the service type that produced this committment in the set returned to the caller.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumServiceOptionType
	 */
	public $AppliedOptions;
	/**
	 * The AppliedSubOptions
	 * Meta informations extracted from the WSDL
	 * - documentation : Supporting detail for applied options identified in preceding field.
	 * - minOccurs : 0
	 * @var MytestStructServiceSubOptionDetail
	 */
	public $AppliedSubOptions;
	/**
	 * The DeliveryStation
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $DeliveryStation;
	/**
	 * The DeliveryDayOfWeek
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumDayOfWeekType
	 */
	public $DeliveryDayOfWeek;
	/**
	 * The DeliveryTimestamp
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var dateTime
	 */
	public $DeliveryTimestamp;
	/**
	 * The CommitDetails
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructCommitDetail
	 */
	public $CommitDetails;
	/**
	 * The DestinationAirportId
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $DestinationAirportId;
	/**
	 * The IneligibleForMoneyBackGuarantee
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var boolean
	 */
	public $IneligibleForMoneyBackGuarantee;
	/**
	 * The OriginServiceArea
	 * Meta informations extracted from the WSDL
	 * - documentation : Not populated by FAST service in Jan07.
	 * - minOccurs : 0
	 * @var string
	 */
	public $OriginServiceArea;
	/**
	 * The DestinationServiceArea
	 * Meta informations extracted from the WSDL
	 * - documentation : Not populated by FAST service in Jan07.
	 * - minOccurs : 0
	 * @var string
	 */
	public $DestinationServiceArea;
	/**
	 * The TransitTime
	 * Meta informations extracted from the WSDL
	 * - documentation : Not populated by FAST service in Jan07.
	 * - minOccurs : 0
	 * @var MytestEnumTransitTimeType
	 */
	public $TransitTime;
	/**
	 * The MaximumTransitTime
	 * Meta informations extracted from the WSDL
	 * - documentation : Maximum expected transit time
	 * - minOccurs : 0
	 * @var MytestEnumTransitTimeType
	 */
	public $MaximumTransitTime;
	/**
	 * The SignatureOption
	 * Meta informations extracted from the WSDL
	 * - documentation : Not populated by FAST service in Jan07. Actual signature option applied, to allow for cases in wihch the original value conflicted with other service features in the shipment.
	 * - minOccurs : 0
	 * @var MytestEnumSignatureOptionType
	 */
	public $SignatureOption;
	/**
	 * The ActualRateType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumReturnedRateType
	 */
	public $ActualRateType;
	/**
	 * The RatedShipmentDetails
	 * Meta informations extracted from the WSDL
	 * - documentation : Each element contains all rate data for a single rate type.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructRatedShipmentDetail
	 */
	public $RatedShipmentDetails;
	/**
	 * Constructor method for RateReplyDetail
	 * @see parent::__construct()
	 * @param MytestEnumServiceType $_serviceType
	 * @param MytestEnumPackagingType $_packagingType
	 * @param MytestEnumServiceOptionType $_appliedOptions
	 * @param MytestStructServiceSubOptionDetail $_appliedSubOptions
	 * @param string $_deliveryStation
	 * @param MytestEnumDayOfWeekType $_deliveryDayOfWeek
	 * @param dateTime $_deliveryTimestamp
	 * @param MytestStructCommitDetail $_commitDetails
	 * @param string $_destinationAirportId
	 * @param boolean $_ineligibleForMoneyBackGuarantee
	 * @param string $_originServiceArea
	 * @param string $_destinationServiceArea
	 * @param MytestEnumTransitTimeType $_transitTime
	 * @param MytestEnumTransitTimeType $_maximumTransitTime
	 * @param MytestEnumSignatureOptionType $_signatureOption
	 * @param MytestEnumReturnedRateType $_actualRateType
	 * @param MytestStructRatedShipmentDetail $_ratedShipmentDetails
	 * @return MytestStructRateReplyDetail
	 */
	public function __construct($_serviceType = NULL,$_packagingType = NULL,$_appliedOptions = NULL,$_appliedSubOptions = NULL,$_deliveryStation = NULL,$_deliveryDayOfWeek = NULL,$_deliveryTimestamp = NULL,$_commitDetails = NULL,$_destinationAirportId = NULL,$_ineligibleForMoneyBackGuarantee = NULL,$_originServiceArea = NULL,$_destinationServiceArea = NULL,$_transitTime = NULL,$_maximumTransitTime = NULL,$_signatureOption = NULL,$_actualRateType = NULL,$_ratedShipmentDetails = NULL)
	{
		parent::__construct(array('ServiceType'=>$_serviceType,'PackagingType'=>$_packagingType,'AppliedOptions'=>$_appliedOptions,'AppliedSubOptions'=>$_appliedSubOptions,'DeliveryStation'=>$_deliveryStation,'DeliveryDayOfWeek'=>$_deliveryDayOfWeek,'DeliveryTimestamp'=>$_deliveryTimestamp,'CommitDetails'=>$_commitDetails,'DestinationAirportId'=>$_destinationAirportId,'IneligibleForMoneyBackGuarantee'=>$_ineligibleForMoneyBackGuarantee,'OriginServiceArea'=>$_originServiceArea,'DestinationServiceArea'=>$_destinationServiceArea,'TransitTime'=>$_transitTime,'MaximumTransitTime'=>$_maximumTransitTime,'SignatureOption'=>$_signatureOption,'ActualRateType'=>$_actualRateType,'RatedShipmentDetails'=>$_ratedShipmentDetails));
	}
	/**
	 * Get ServiceType value
	 * @return MytestEnumServiceType|null
	 */
	public function getServiceType()
	{
		return $this->ServiceType;
	}
	/**
	 * Set ServiceType value
	 * @uses MytestEnumServiceType::valueIsValid()
	 * @param MytestEnumServiceType $_serviceType the ServiceType
	 * @return MytestEnumServiceType
	 */
	public function setServiceType($_serviceType)
	{
		if(!MytestEnumServiceType::valueIsValid($_serviceType))
		{
			return false;
		}
		return ($this->ServiceType = $_serviceType);
	}
	/**
	 * Get PackagingType value
	 * @return MytestEnumPackagingType|null
	 */
	public function getPackagingType()
	{
		return $this->PackagingType;
	}
	/**
	 * Set PackagingType value
	 * @uses MytestEnumPackagingType::valueIsValid()
	 * @param MytestEnumPackagingType $_packagingType the PackagingType
	 * @return MytestEnumPackagingType
	 */
	public function setPackagingType($_packagingType)
	{
		if(!MytestEnumPackagingType::valueIsValid($_packagingType))
		{
			return false;
		}
		return ($this->PackagingType = $_packagingType);
	}
	/**
	 * Get AppliedOptions value
	 * @return MytestEnumServiceOptionType|null
	 */
	public function getAppliedOptions()
	{
		return $this->AppliedOptions;
	}
	/**
	 * Set AppliedOptions value
	 * @uses MytestEnumServiceOptionType::valueIsValid()
	 * @param MytestEnumServiceOptionType $_appliedOptions the AppliedOptions
	 * @return MytestEnumServiceOptionType
	 */
	public function setAppliedOptions($_appliedOptions)
	{
		if(!MytestEnumServiceOptionType::valueIsValid($_appliedOptions))
		{
			return false;
		}
		return ($this->AppliedOptions = $_appliedOptions);
	}
	/**
	 * Get AppliedSubOptions value
	 * @return MytestStructServiceSubOptionDetail|null
	 */
	public function getAppliedSubOptions()
	{
		return $this->AppliedSubOptions;
	}
	/**
	 * Set AppliedSubOptions value
	 * @param MytestStructServiceSubOptionDetail $_appliedSubOptions the AppliedSubOptions
	 * @return MytestStructServiceSubOptionDetail
	 */
	public function setAppliedSubOptions($_appliedSubOptions)
	{
		return ($this->AppliedSubOptions = $_appliedSubOptions);
	}
	/**
	 * Get DeliveryStation value
	 * @return string|null
	 */
	public function getDeliveryStation()
	{
		return $this->DeliveryStation;
	}
	/**
	 * Set DeliveryStation value
	 * @param string $_deliveryStation the DeliveryStation
	 * @return string
	 */
	public function setDeliveryStation($_deliveryStation)
	{
		return ($this->DeliveryStation = $_deliveryStation);
	}
	/**
	 * Get DeliveryDayOfWeek value
	 * @return MytestEnumDayOfWeekType|null
	 */
	public function getDeliveryDayOfWeek()
	{
		return $this->DeliveryDayOfWeek;
	}
	/**
	 * Set DeliveryDayOfWeek value
	 * @uses MytestEnumDayOfWeekType::valueIsValid()
	 * @param MytestEnumDayOfWeekType $_deliveryDayOfWeek the DeliveryDayOfWeek
	 * @return MytestEnumDayOfWeekType
	 */
	public function setDeliveryDayOfWeek($_deliveryDayOfWeek)
	{
		if(!MytestEnumDayOfWeekType::valueIsValid($_deliveryDayOfWeek))
		{
			return false;
		}
		return ($this->DeliveryDayOfWeek = $_deliveryDayOfWeek);
	}
	/**
	 * Get DeliveryTimestamp value
	 * @return dateTime|null
	 */
	public function getDeliveryTimestamp()
	{
		return $this->DeliveryTimestamp;
	}
	/**
	 * Set DeliveryTimestamp value
	 * @param dateTime $_deliveryTimestamp the DeliveryTimestamp
	 * @return dateTime
	 */
	public function setDeliveryTimestamp($_deliveryTimestamp)
	{
		return ($this->DeliveryTimestamp = $_deliveryTimestamp);
	}
	/**
	 * Get CommitDetails value
	 * @return MytestStructCommitDetail|null
	 */
	public function getCommitDetails()
	{
		return $this->CommitDetails;
	}
	/**
	 * Set CommitDetails value
	 * @param MytestStructCommitDetail $_commitDetails the CommitDetails
	 * @return MytestStructCommitDetail
	 */
	public function setCommitDetails($_commitDetails)
	{
		return ($this->CommitDetails = $_commitDetails);
	}
	/**
	 * Get DestinationAirportId value
	 * @return string|null
	 */
	public function getDestinationAirportId()
	{
		return $this->DestinationAirportId;
	}
	/**
	 * Set DestinationAirportId value
	 * @param string $_destinationAirportId the DestinationAirportId
	 * @return string
	 */
	public function setDestinationAirportId($_destinationAirportId)
	{
		return ($this->DestinationAirportId = $_destinationAirportId);
	}
	/**
	 * Get IneligibleForMoneyBackGuarantee value
	 * @return boolean|null
	 */
	public function getIneligibleForMoneyBackGuarantee()
	{
		return $this->IneligibleForMoneyBackGuarantee;
	}
	/**
	 * Set IneligibleForMoneyBackGuarantee value
	 * @param boolean $_ineligibleForMoneyBackGuarantee the IneligibleForMoneyBackGuarantee
	 * @return boolean
	 */
	public function setIneligibleForMoneyBackGuarantee($_ineligibleForMoneyBackGuarantee)
	{
		return ($this->IneligibleForMoneyBackGuarantee = $_ineligibleForMoneyBackGuarantee);
	}
	/**
	 * Get OriginServiceArea value
	 * @return string|null
	 */
	public function getOriginServiceArea()
	{
		return $this->OriginServiceArea;
	}
	/**
	 * Set OriginServiceArea value
	 * @param string $_originServiceArea the OriginServiceArea
	 * @return string
	 */
	public function setOriginServiceArea($_originServiceArea)
	{
		return ($this->OriginServiceArea = $_originServiceArea);
	}
	/**
	 * Get DestinationServiceArea value
	 * @return string|null
	 */
	public function getDestinationServiceArea()
	{
		return $this->DestinationServiceArea;
	}
	/**
	 * Set DestinationServiceArea value
	 * @param string $_destinationServiceArea the DestinationServiceArea
	 * @return string
	 */
	public function setDestinationServiceArea($_destinationServiceArea)
	{
		return ($this->DestinationServiceArea = $_destinationServiceArea);
	}
	/**
	 * Get TransitTime value
	 * @return MytestEnumTransitTimeType|null
	 */
	public function getTransitTime()
	{
		return $this->TransitTime;
	}
	/**
	 * Set TransitTime value
	 * @uses MytestEnumTransitTimeType::valueIsValid()
	 * @param MytestEnumTransitTimeType $_transitTime the TransitTime
	 * @return MytestEnumTransitTimeType
	 */
	public function setTransitTime($_transitTime)
	{
		if(!MytestEnumTransitTimeType::valueIsValid($_transitTime))
		{
			return false;
		}
		return ($this->TransitTime = $_transitTime);
	}
	/**
	 * Get MaximumTransitTime value
	 * @return MytestEnumTransitTimeType|null
	 */
	public function getMaximumTransitTime()
	{
		return $this->MaximumTransitTime;
	}
	/**
	 * Set MaximumTransitTime value
	 * @uses MytestEnumTransitTimeType::valueIsValid()
	 * @param MytestEnumTransitTimeType $_maximumTransitTime the MaximumTransitTime
	 * @return MytestEnumTransitTimeType
	 */
	public function setMaximumTransitTime($_maximumTransitTime)
	{
		if(!MytestEnumTransitTimeType::valueIsValid($_maximumTransitTime))
		{
			return false;
		}
		return ($this->MaximumTransitTime = $_maximumTransitTime);
	}
	/**
	 * Get SignatureOption value
	 * @return MytestEnumSignatureOptionType|null
	 */
	public function getSignatureOption()
	{
		return $this->SignatureOption;
	}
	/**
	 * Set SignatureOption value
	 * @uses MytestEnumSignatureOptionType::valueIsValid()
	 * @param MytestEnumSignatureOptionType $_signatureOption the SignatureOption
	 * @return MytestEnumSignatureOptionType
	 */
	public function setSignatureOption($_signatureOption)
	{
		if(!MytestEnumSignatureOptionType::valueIsValid($_signatureOption))
		{
			return false;
		}
		return ($this->SignatureOption = $_signatureOption);
	}
	/**
	 * Get ActualRateType value
	 * @return MytestEnumReturnedRateType|null
	 */
	public function getActualRateType()
	{
		return $this->ActualRateType;
	}
	/**
	 * Set ActualRateType value
	 * @uses MytestEnumReturnedRateType::valueIsValid()
	 * @param MytestEnumReturnedRateType $_actualRateType the ActualRateType
	 * @return MytestEnumReturnedRateType
	 */
	public function setActualRateType($_actualRateType)
	{
		if(!MytestEnumReturnedRateType::valueIsValid($_actualRateType))
		{
			return false;
		}
		return ($this->ActualRateType = $_actualRateType);
	}
	/**
	 * Get RatedShipmentDetails value
	 * @return MytestStructRatedShipmentDetail|null
	 */
	public function getRatedShipmentDetails()
	{
		return $this->RatedShipmentDetails;
	}
	/**
	 * Set RatedShipmentDetails value
	 * @param MytestStructRatedShipmentDetail $_ratedShipmentDetails the RatedShipmentDetails
	 * @return MytestStructRatedShipmentDetail
	 */
	public function setRatedShipmentDetails($_ratedShipmentDetails)
	{
		return ($this->RatedShipmentDetails = $_ratedShipmentDetails);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructRateReplyDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>