<?php
/**
 * File for class MytestStructRateReply
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructRateReply originally named RateReply
 * Documentation : The response to a RateRequest. The Notifications indicate whether the request was successful or not.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructRateReply extends MytestWsdlClass
{
	/**
	 * The HighestSeverity
	 * Meta informations extracted from the WSDL
	 * - documentation : This indicates the highest level of severity of all the notifications returned in this reply.
	 * - minOccurs : 1
	 * @var MytestEnumNotificationSeverityType
	 */
	public $HighestSeverity;
	/**
	 * The Notifications
	 * Meta informations extracted from the WSDL
	 * - documentation : The descriptive data regarding the results of the submitted transaction.
	 * - maxOccurs : unbounded
	 * - minOccurs : 1
	 * @var MytestStructNotification
	 */
	public $Notifications;
	/**
	 * The Version
	 * Meta informations extracted from the WSDL
	 * - documentation : The version of this reply.
	 * - minOccurs : 1
	 * @var MytestStructVersionId
	 */
	public $Version;
	/**
	 * The TransactionDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Contains the CustomerTransactionId that was sent in the request.
	 * - minOccurs : 0
	 * @var MytestStructTransactionDetail
	 */
	public $TransactionDetail;
	/**
	 * The RateReplyDetails
	 * Meta informations extracted from the WSDL
	 * - documentation : Each element contains all rate data for a single service. If service was specified in the request, there will be a single entry in this array; if service was omitted in the request, there will be a separate entry in this array for each service being compared.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructRateReplyDetail
	 */
	public $RateReplyDetails;
	/**
	 * Constructor method for RateReply
	 * @see parent::__construct()
	 * @param MytestEnumNotificationSeverityType $_highestSeverity
	 * @param MytestStructNotification $_notifications
	 * @param MytestStructVersionId $_version
	 * @param MytestStructTransactionDetail $_transactionDetail
	 * @param MytestStructRateReplyDetail $_rateReplyDetails
	 * @return MytestStructRateReply
	 */
	public function __construct($_highestSeverity,$_notifications,$_version,$_transactionDetail = NULL,$_rateReplyDetails = NULL)
	{
		parent::__construct(array('HighestSeverity'=>$_highestSeverity,'Notifications'=>$_notifications,'Version'=>$_version,'TransactionDetail'=>$_transactionDetail,'RateReplyDetails'=>$_rateReplyDetails));
	}
	/**
	 * Get HighestSeverity value
	 * @return MytestEnumNotificationSeverityType
	 */
	public function getHighestSeverity()
	{
		return $this->HighestSeverity;
	}
	/**
	 * Set HighestSeverity value
	 * @uses MytestEnumNotificationSeverityType::valueIsValid()
	 * @param MytestEnumNotificationSeverityType $_highestSeverity the HighestSeverity
	 * @return MytestEnumNotificationSeverityType
	 */
	public function setHighestSeverity($_highestSeverity)
	{
		if(!MytestEnumNotificationSeverityType::valueIsValid($_highestSeverity))
		{
			return false;
		}
		return ($this->HighestSeverity = $_highestSeverity);
	}
	/**
	 * Get Notifications value
	 * @return MytestStructNotification
	 */
	public function getNotifications()
	{
		return $this->Notifications;
	}
	/**
	 * Set Notifications value
	 * @param MytestStructNotification $_notifications the Notifications
	 * @return MytestStructNotification
	 */
	public function setNotifications($_notifications)
	{
		return ($this->Notifications = $_notifications);
	}
	/**
	 * Get Version value
	 * @return MytestStructVersionId
	 */
	public function getVersion()
	{
		return $this->Version;
	}
	/**
	 * Set Version value
	 * @param MytestStructVersionId $_version the Version
	 * @return MytestStructVersionId
	 */
	public function setVersion($_version)
	{
		return ($this->Version = $_version);
	}
	/**
	 * Get TransactionDetail value
	 * @return MytestStructTransactionDetail|null
	 */
	public function getTransactionDetail()
	{
		return $this->TransactionDetail;
	}
	/**
	 * Set TransactionDetail value
	 * @param MytestStructTransactionDetail $_transactionDetail the TransactionDetail
	 * @return MytestStructTransactionDetail
	 */
	public function setTransactionDetail($_transactionDetail)
	{
		return ($this->TransactionDetail = $_transactionDetail);
	}
	/**
	 * Get RateReplyDetails value
	 * @return MytestStructRateReplyDetail|null
	 */
	public function getRateReplyDetails()
	{
		return $this->RateReplyDetails;
	}
	/**
	 * Set RateReplyDetails value
	 * @param MytestStructRateReplyDetail $_rateReplyDetails the RateReplyDetails
	 * @return MytestStructRateReplyDetail
	 */
	public function setRateReplyDetails($_rateReplyDetails)
	{
		return ($this->RateReplyDetails = $_rateReplyDetails);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructRateReply
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>