<?php
/**
 * File for class MytestEnumChargeBasisLevelType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumChargeBasisLevelType originally named ChargeBasisLevelType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumChargeBasisLevelType extends MytestWsdlClass
{
	/**
	 * Constant for value 'CURRENT_PACKAGE'
	 * @return string 'CURRENT_PACKAGE'
	 */
	const VALUE_CURRENT_PACKAGE = 'CURRENT_PACKAGE';
	/**
	 * Constant for value 'SUM_OF_PACKAGES'
	 * @return string 'SUM_OF_PACKAGES'
	 */
	const VALUE_SUM_OF_PACKAGES = 'SUM_OF_PACKAGES';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumChargeBasisLevelType::VALUE_CURRENT_PACKAGE
	 * @uses MytestEnumChargeBasisLevelType::VALUE_SUM_OF_PACKAGES
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumChargeBasisLevelType::VALUE_CURRENT_PACKAGE,MytestEnumChargeBasisLevelType::VALUE_SUM_OF_PACKAGES));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>