<?php
/**
 * File for class MytestStructVolume
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructVolume originally named Volume
 * Documentation : Three-dimensional volume/cubic measurement.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructVolume extends MytestWsdlClass
{
	/**
	 * The Units
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumVolumeUnits
	 */
	public $Units;
	/**
	 * The Value
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var decimal
	 */
	public $Value;
	/**
	 * Constructor method for Volume
	 * @see parent::__construct()
	 * @param MytestEnumVolumeUnits $_units
	 * @param decimal $_value
	 * @return MytestStructVolume
	 */
	public function __construct($_units = NULL,$_value = NULL)
	{
		parent::__construct(array('Units'=>$_units,'Value'=>$_value));
	}
	/**
	 * Get Units value
	 * @return MytestEnumVolumeUnits|null
	 */
	public function getUnits()
	{
		return $this->Units;
	}
	/**
	 * Set Units value
	 * @uses MytestEnumVolumeUnits::valueIsValid()
	 * @param MytestEnumVolumeUnits $_units the Units
	 * @return MytestEnumVolumeUnits
	 */
	public function setUnits($_units)
	{
		if(!MytestEnumVolumeUnits::valueIsValid($_units))
		{
			return false;
		}
		return ($this->Units = $_units);
	}
	/**
	 * Get Value value
	 * @return decimal|null
	 */
	public function getValue()
	{
		return $this->Value;
	}
	/**
	 * Set Value value
	 * @param decimal $_value the Value
	 * @return decimal
	 */
	public function setValue($_value)
	{
		return ($this->Value = $_value);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructVolume
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>