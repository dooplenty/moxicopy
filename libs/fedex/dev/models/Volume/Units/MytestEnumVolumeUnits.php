<?php
/**
 * File for class MytestEnumVolumeUnits
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumVolumeUnits originally named VolumeUnits
 * Documentation : Units of three-dimensional volume/cubic measure.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumVolumeUnits extends MytestWsdlClass
{
	/**
	 * Constant for value 'CUBIC_FT'
	 * @return string 'CUBIC_FT'
	 */
	const VALUE_CUBIC_FT = 'CUBIC_FT';
	/**
	 * Constant for value 'CUBIC_M'
	 * @return string 'CUBIC_M'
	 */
	const VALUE_CUBIC_M = 'CUBIC_M';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumVolumeUnits::VALUE_CUBIC_FT
	 * @uses MytestEnumVolumeUnits::VALUE_CUBIC_M
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumVolumeUnits::VALUE_CUBIC_FT,MytestEnumVolumeUnits::VALUE_CUBIC_M));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>