<?php
/**
 * File for class MytestStructBrokerDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructBrokerDetail originally named BrokerDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructBrokerDetail extends MytestWsdlClass
{
	/**
	 * The Type
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumBrokerType
	 */
	public $Type;
	/**
	 * The Broker
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructParty
	 */
	public $Broker;
	/**
	 * Constructor method for BrokerDetail
	 * @see parent::__construct()
	 * @param MytestEnumBrokerType $_type
	 * @param MytestStructParty $_broker
	 * @return MytestStructBrokerDetail
	 */
	public function __construct($_type = NULL,$_broker = NULL)
	{
		parent::__construct(array('Type'=>$_type,'Broker'=>$_broker));
	}
	/**
	 * Get Type value
	 * @return MytestEnumBrokerType|null
	 */
	public function getType()
	{
		return $this->Type;
	}
	/**
	 * Set Type value
	 * @uses MytestEnumBrokerType::valueIsValid()
	 * @param MytestEnumBrokerType $_type the Type
	 * @return MytestEnumBrokerType
	 */
	public function setType($_type)
	{
		if(!MytestEnumBrokerType::valueIsValid($_type))
		{
			return false;
		}
		return ($this->Type = $_type);
	}
	/**
	 * Get Broker value
	 * @return MytestStructParty|null
	 */
	public function getBroker()
	{
		return $this->Broker;
	}
	/**
	 * Set Broker value
	 * @param MytestStructParty $_broker the Broker
	 * @return MytestStructParty
	 */
	public function setBroker($_broker)
	{
		return ($this->Broker = $_broker);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructBrokerDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>