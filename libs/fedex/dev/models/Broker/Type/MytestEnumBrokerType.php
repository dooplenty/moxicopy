<?php
/**
 * File for class MytestEnumBrokerType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumBrokerType originally named BrokerType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumBrokerType extends MytestWsdlClass
{
	/**
	 * Constant for value 'EXPORT'
	 * @return string 'EXPORT'
	 */
	const VALUE_EXPORT = 'EXPORT';
	/**
	 * Constant for value 'IMPORT'
	 * @return string 'IMPORT'
	 */
	const VALUE_IMPORT = 'IMPORT';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumBrokerType::VALUE_EXPORT
	 * @uses MytestEnumBrokerType::VALUE_IMPORT
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumBrokerType::VALUE_EXPORT,MytestEnumBrokerType::VALUE_IMPORT));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>