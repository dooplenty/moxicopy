<?php
/**
 * File for class MytestStructContact
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructContact originally named Contact
 * Documentation : The descriptive data for a point-of-contact person.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructContact extends MytestWsdlClass
{
	/**
	 * The ContactId
	 * Meta informations extracted from the WSDL
	 * - documentation : Client provided identifier corresponding to this contact information.
	 * - minOccurs : 0
	 * @var string
	 */
	public $ContactId;
	/**
	 * The PersonName
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the contact person's name.
	 * - minOccurs : 0
	 * @var string
	 */
	public $PersonName;
	/**
	 * The Title
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the contact person's title.
	 * - minOccurs : 0
	 * @var string
	 */
	public $Title;
	/**
	 * The CompanyName
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the company this contact is associated with.
	 * - minOccurs : 0
	 * @var string
	 */
	public $CompanyName;
	/**
	 * The PhoneNumber
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the phone number associated with this contact.
	 * - minOccurs : 0
	 * @var string
	 */
	public $PhoneNumber;
	/**
	 * The PhoneExtension
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the phone extension associated with this contact.
	 * - minOccurs : 0
	 * @var string
	 */
	public $PhoneExtension;
	/**
	 * The TollFreePhoneNumber
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies a toll free number, if any, associated with this contact.
	 * - minOccurs : 0
	 * @var string
	 */
	public $TollFreePhoneNumber;
	/**
	 * The PagerNumber
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the pager number associated with this contact.
	 * - minOccurs : 0
	 * @var string
	 */
	public $PagerNumber;
	/**
	 * The FaxNumber
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the fax number associated with this contact.
	 * - minOccurs : 0
	 * @var string
	 */
	public $FaxNumber;
	/**
	 * The EMailAddress
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the email address associated with this contact.
	 * - minOccurs : 0
	 * @var string
	 */
	public $EMailAddress;
	/**
	 * Constructor method for Contact
	 * @see parent::__construct()
	 * @param string $_contactId
	 * @param string $_personName
	 * @param string $_title
	 * @param string $_companyName
	 * @param string $_phoneNumber
	 * @param string $_phoneExtension
	 * @param string $_tollFreePhoneNumber
	 * @param string $_pagerNumber
	 * @param string $_faxNumber
	 * @param string $_eMailAddress
	 * @return MytestStructContact
	 */
	public function __construct($_contactId = NULL,$_personName = NULL,$_title = NULL,$_companyName = NULL,$_phoneNumber = NULL,$_phoneExtension = NULL,$_tollFreePhoneNumber = NULL,$_pagerNumber = NULL,$_faxNumber = NULL,$_eMailAddress = NULL)
	{
		parent::__construct(array('ContactId'=>$_contactId,'PersonName'=>$_personName,'Title'=>$_title,'CompanyName'=>$_companyName,'PhoneNumber'=>$_phoneNumber,'PhoneExtension'=>$_phoneExtension,'TollFreePhoneNumber'=>$_tollFreePhoneNumber,'PagerNumber'=>$_pagerNumber,'FaxNumber'=>$_faxNumber,'EMailAddress'=>$_eMailAddress));
	}
	/**
	 * Get ContactId value
	 * @return string|null
	 */
	public function getContactId()
	{
		return $this->ContactId;
	}
	/**
	 * Set ContactId value
	 * @param string $_contactId the ContactId
	 * @return string
	 */
	public function setContactId($_contactId)
	{
		return ($this->ContactId = $_contactId);
	}
	/**
	 * Get PersonName value
	 * @return string|null
	 */
	public function getPersonName()
	{
		return $this->PersonName;
	}
	/**
	 * Set PersonName value
	 * @param string $_personName the PersonName
	 * @return string
	 */
	public function setPersonName($_personName)
	{
		return ($this->PersonName = $_personName);
	}
	/**
	 * Get Title value
	 * @return string|null
	 */
	public function getTitle()
	{
		return $this->Title;
	}
	/**
	 * Set Title value
	 * @param string $_title the Title
	 * @return string
	 */
	public function setTitle($_title)
	{
		return ($this->Title = $_title);
	}
	/**
	 * Get CompanyName value
	 * @return string|null
	 */
	public function getCompanyName()
	{
		return $this->CompanyName;
	}
	/**
	 * Set CompanyName value
	 * @param string $_companyName the CompanyName
	 * @return string
	 */
	public function setCompanyName($_companyName)
	{
		return ($this->CompanyName = $_companyName);
	}
	/**
	 * Get PhoneNumber value
	 * @return string|null
	 */
	public function getPhoneNumber()
	{
		return $this->PhoneNumber;
	}
	/**
	 * Set PhoneNumber value
	 * @param string $_phoneNumber the PhoneNumber
	 * @return string
	 */
	public function setPhoneNumber($_phoneNumber)
	{
		return ($this->PhoneNumber = $_phoneNumber);
	}
	/**
	 * Get PhoneExtension value
	 * @return string|null
	 */
	public function getPhoneExtension()
	{
		return $this->PhoneExtension;
	}
	/**
	 * Set PhoneExtension value
	 * @param string $_phoneExtension the PhoneExtension
	 * @return string
	 */
	public function setPhoneExtension($_phoneExtension)
	{
		return ($this->PhoneExtension = $_phoneExtension);
	}
	/**
	 * Get TollFreePhoneNumber value
	 * @return string|null
	 */
	public function getTollFreePhoneNumber()
	{
		return $this->TollFreePhoneNumber;
	}
	/**
	 * Set TollFreePhoneNumber value
	 * @param string $_tollFreePhoneNumber the TollFreePhoneNumber
	 * @return string
	 */
	public function setTollFreePhoneNumber($_tollFreePhoneNumber)
	{
		return ($this->TollFreePhoneNumber = $_tollFreePhoneNumber);
	}
	/**
	 * Get PagerNumber value
	 * @return string|null
	 */
	public function getPagerNumber()
	{
		return $this->PagerNumber;
	}
	/**
	 * Set PagerNumber value
	 * @param string $_pagerNumber the PagerNumber
	 * @return string
	 */
	public function setPagerNumber($_pagerNumber)
	{
		return ($this->PagerNumber = $_pagerNumber);
	}
	/**
	 * Get FaxNumber value
	 * @return string|null
	 */
	public function getFaxNumber()
	{
		return $this->FaxNumber;
	}
	/**
	 * Set FaxNumber value
	 * @param string $_faxNumber the FaxNumber
	 * @return string
	 */
	public function setFaxNumber($_faxNumber)
	{
		return ($this->FaxNumber = $_faxNumber);
	}
	/**
	 * Get EMailAddress value
	 * @return string|null
	 */
	public function getEMailAddress()
	{
		return $this->EMailAddress;
	}
	/**
	 * Set EMailAddress value
	 * @param string $_eMailAddress the EMailAddress
	 * @return string
	 */
	public function setEMailAddress($_eMailAddress)
	{
		return ($this->EMailAddress = $_eMailAddress);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructContact
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>