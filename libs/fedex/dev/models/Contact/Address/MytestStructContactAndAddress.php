<?php
/**
 * File for class MytestStructContactAndAddress
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructContactAndAddress originally named ContactAndAddress
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructContactAndAddress extends MytestWsdlClass
{
	/**
	 * The Contact
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructContact
	 */
	public $Contact;
	/**
	 * The Address
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructAddress
	 */
	public $Address;
	/**
	 * Constructor method for ContactAndAddress
	 * @see parent::__construct()
	 * @param MytestStructContact $_contact
	 * @param MytestStructAddress $_address
	 * @return MytestStructContactAndAddress
	 */
	public function __construct($_contact = NULL,$_address = NULL)
	{
		parent::__construct(array('Contact'=>$_contact,'Address'=>$_address));
	}
	/**
	 * Get Contact value
	 * @return MytestStructContact|null
	 */
	public function getContact()
	{
		return $this->Contact;
	}
	/**
	 * Set Contact value
	 * @param MytestStructContact $_contact the Contact
	 * @return MytestStructContact
	 */
	public function setContact($_contact)
	{
		return ($this->Contact = $_contact);
	}
	/**
	 * Get Address value
	 * @return MytestStructAddress|null
	 */
	public function getAddress()
	{
		return $this->Address;
	}
	/**
	 * Set Address value
	 * @param MytestStructAddress $_address the Address
	 * @return MytestStructAddress
	 */
	public function setAddress($_address)
	{
		return ($this->Address = $_address);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructContactAndAddress
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>