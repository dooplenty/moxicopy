<?php
/**
 * File for class MytestEnumDayOfWeekType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumDayOfWeekType originally named DayOfWeekType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumDayOfWeekType extends MytestWsdlClass
{
	/**
	 * Constant for value 'FRI'
	 * @return string 'FRI'
	 */
	const VALUE_FRI = 'FRI';
	/**
	 * Constant for value 'MON'
	 * @return string 'MON'
	 */
	const VALUE_MON = 'MON';
	/**
	 * Constant for value 'SAT'
	 * @return string 'SAT'
	 */
	const VALUE_SAT = 'SAT';
	/**
	 * Constant for value 'SUN'
	 * @return string 'SUN'
	 */
	const VALUE_SUN = 'SUN';
	/**
	 * Constant for value 'THU'
	 * @return string 'THU'
	 */
	const VALUE_THU = 'THU';
	/**
	 * Constant for value 'TUE'
	 * @return string 'TUE'
	 */
	const VALUE_TUE = 'TUE';
	/**
	 * Constant for value 'WED'
	 * @return string 'WED'
	 */
	const VALUE_WED = 'WED';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumDayOfWeekType::VALUE_FRI
	 * @uses MytestEnumDayOfWeekType::VALUE_MON
	 * @uses MytestEnumDayOfWeekType::VALUE_SAT
	 * @uses MytestEnumDayOfWeekType::VALUE_SUN
	 * @uses MytestEnumDayOfWeekType::VALUE_THU
	 * @uses MytestEnumDayOfWeekType::VALUE_TUE
	 * @uses MytestEnumDayOfWeekType::VALUE_WED
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumDayOfWeekType::VALUE_FRI,MytestEnumDayOfWeekType::VALUE_MON,MytestEnumDayOfWeekType::VALUE_SAT,MytestEnumDayOfWeekType::VALUE_SUN,MytestEnumDayOfWeekType::VALUE_THU,MytestEnumDayOfWeekType::VALUE_TUE,MytestEnumDayOfWeekType::VALUE_WED));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>