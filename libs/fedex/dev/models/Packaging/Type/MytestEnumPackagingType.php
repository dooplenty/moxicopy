<?php
/**
 * File for class MytestEnumPackagingType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumPackagingType originally named PackagingType
 * Documentation : Identifies the packaging used by the requestor for the package. See PackagingType for list of valid enumerated values.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumPackagingType extends MytestWsdlClass
{
	/**
	 * Constant for value 'FEDEX_10KG_BOX'
	 * @return string 'FEDEX_10KG_BOX'
	 */
	const VALUE_FEDEX_10KG_BOX = 'FEDEX_10KG_BOX';
	/**
	 * Constant for value 'FEDEX_25KG_BOX'
	 * @return string 'FEDEX_25KG_BOX'
	 */
	const VALUE_FEDEX_25KG_BOX = 'FEDEX_25KG_BOX';
	/**
	 * Constant for value 'FEDEX_BOX'
	 * @return string 'FEDEX_BOX'
	 */
	const VALUE_FEDEX_BOX = 'FEDEX_BOX';
	/**
	 * Constant for value 'FEDEX_ENVELOPE'
	 * @return string 'FEDEX_ENVELOPE'
	 */
	const VALUE_FEDEX_ENVELOPE = 'FEDEX_ENVELOPE';
	/**
	 * Constant for value 'FEDEX_PAK'
	 * @return string 'FEDEX_PAK'
	 */
	const VALUE_FEDEX_PAK = 'FEDEX_PAK';
	/**
	 * Constant for value 'FEDEX_TUBE'
	 * @return string 'FEDEX_TUBE'
	 */
	const VALUE_FEDEX_TUBE = 'FEDEX_TUBE';
	/**
	 * Constant for value 'YOUR_PACKAGING'
	 * @return string 'YOUR_PACKAGING'
	 */
	const VALUE_YOUR_PACKAGING = 'YOUR_PACKAGING';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumPackagingType::VALUE_FEDEX_10KG_BOX
	 * @uses MytestEnumPackagingType::VALUE_FEDEX_25KG_BOX
	 * @uses MytestEnumPackagingType::VALUE_FEDEX_BOX
	 * @uses MytestEnumPackagingType::VALUE_FEDEX_ENVELOPE
	 * @uses MytestEnumPackagingType::VALUE_FEDEX_PAK
	 * @uses MytestEnumPackagingType::VALUE_FEDEX_TUBE
	 * @uses MytestEnumPackagingType::VALUE_YOUR_PACKAGING
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumPackagingType::VALUE_FEDEX_10KG_BOX,MytestEnumPackagingType::VALUE_FEDEX_25KG_BOX,MytestEnumPackagingType::VALUE_FEDEX_BOX,MytestEnumPackagingType::VALUE_FEDEX_ENVELOPE,MytestEnumPackagingType::VALUE_FEDEX_PAK,MytestEnumPackagingType::VALUE_FEDEX_TUBE,MytestEnumPackagingType::VALUE_YOUR_PACKAGING));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>