<?php
/**
 * File for class MytestStructCurrencyExchangeRate
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCurrencyExchangeRate originally named CurrencyExchangeRate
 * Documentation : Specifies the currency exchange performed on financial amounts for this rate.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCurrencyExchangeRate extends MytestWsdlClass
{
	/**
	 * The FromCurrency
	 * Meta informations extracted from the WSDL
	 * - documentation : The currency code for the original (converted FROM) currency.
	 * - minOccurs : 0
	 * @var string
	 */
	public $FromCurrency;
	/**
	 * The IntoCurrency
	 * Meta informations extracted from the WSDL
	 * - documentation : The currency code for the final (converted INTO) currency.
	 * - minOccurs : 0
	 * @var string
	 */
	public $IntoCurrency;
	/**
	 * The Rate
	 * Meta informations extracted from the WSDL
	 * - documentation : Multiplier used to convert fromCurrency units to intoCurrency units.
	 * - minOccurs : 0
	 * @var decimal
	 */
	public $Rate;
	/**
	 * Constructor method for CurrencyExchangeRate
	 * @see parent::__construct()
	 * @param string $_fromCurrency
	 * @param string $_intoCurrency
	 * @param decimal $_rate
	 * @return MytestStructCurrencyExchangeRate
	 */
	public function __construct($_fromCurrency = NULL,$_intoCurrency = NULL,$_rate = NULL)
	{
		parent::__construct(array('FromCurrency'=>$_fromCurrency,'IntoCurrency'=>$_intoCurrency,'Rate'=>$_rate));
	}
	/**
	 * Get FromCurrency value
	 * @return string|null
	 */
	public function getFromCurrency()
	{
		return $this->FromCurrency;
	}
	/**
	 * Set FromCurrency value
	 * @param string $_fromCurrency the FromCurrency
	 * @return string
	 */
	public function setFromCurrency($_fromCurrency)
	{
		return ($this->FromCurrency = $_fromCurrency);
	}
	/**
	 * Get IntoCurrency value
	 * @return string|null
	 */
	public function getIntoCurrency()
	{
		return $this->IntoCurrency;
	}
	/**
	 * Set IntoCurrency value
	 * @param string $_intoCurrency the IntoCurrency
	 * @return string
	 */
	public function setIntoCurrency($_intoCurrency)
	{
		return ($this->IntoCurrency = $_intoCurrency);
	}
	/**
	 * Get Rate value
	 * @return decimal|null
	 */
	public function getRate()
	{
		return $this->Rate;
	}
	/**
	 * Set Rate value
	 * @param decimal $_rate the Rate
	 * @return decimal
	 */
	public function setRate($_rate)
	{
		return ($this->Rate = $_rate);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCurrencyExchangeRate
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>