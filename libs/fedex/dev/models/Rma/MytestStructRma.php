<?php
/**
 * File for class MytestStructRma
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructRma originally named Rma
 * Documentation : June 2011 ITG 121203 IR-RMA number has been removed from this structure and added as a new customer reference type. The structure remains because of the reason field below.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructRma extends MytestWsdlClass
{
	/**
	 * The Reason
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Reason;
	/**
	 * Constructor method for Rma
	 * @see parent::__construct()
	 * @param string $_reason
	 * @return MytestStructRma
	 */
	public function __construct($_reason = NULL)
	{
		parent::__construct(array('Reason'=>$_reason));
	}
	/**
	 * Get Reason value
	 * @return string|null
	 */
	public function getReason()
	{
		return $this->Reason;
	}
	/**
	 * Set Reason value
	 * @param string $_reason the Reason
	 * @return string
	 */
	public function setReason($_reason)
	{
		return ($this->Reason = $_reason);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructRma
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>