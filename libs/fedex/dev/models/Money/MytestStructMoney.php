<?php
/**
 * File for class MytestStructMoney
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructMoney originally named Money
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructMoney extends MytestWsdlClass
{
	/**
	 * The Currency
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Currency;
	/**
	 * The Amount
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var decimal
	 */
	public $Amount;
	/**
	 * Constructor method for Money
	 * @see parent::__construct()
	 * @param string $_currency
	 * @param decimal $_amount
	 * @return MytestStructMoney
	 */
	public function __construct($_currency = NULL,$_amount = NULL)
	{
		parent::__construct(array('Currency'=>$_currency,'Amount'=>$_amount));
	}
	/**
	 * Get Currency value
	 * @return string|null
	 */
	public function getCurrency()
	{
		return $this->Currency;
	}
	/**
	 * Set Currency value
	 * @param string $_currency the Currency
	 * @return string
	 */
	public function setCurrency($_currency)
	{
		return ($this->Currency = $_currency);
	}
	/**
	 * Get Amount value
	 * @return decimal|null
	 */
	public function getAmount()
	{
		return $this->Amount;
	}
	/**
	 * Set Amount value
	 * @param decimal $_amount the Amount
	 * @return decimal
	 */
	public function setAmount($_amount)
	{
		return ($this->Amount = $_amount);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructMoney
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>