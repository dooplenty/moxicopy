<?php
/**
 * File for class MytestStructCommodity
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCommodity originally named Commodity
 * Documentation : For international multiple piece shipments, commodity information must be passed in the Master and on each child transaction. If this shipment cotains more than four commodities line items, the four highest valued should be included in the first 4 occurances for this request.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCommodity extends MytestWsdlClass
{
	/**
	 * The Name
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Name;
	/**
	 * The NumberOfPieces
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $NumberOfPieces;
	/**
	 * The Description
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Description;
	/**
	 * The CountryOfManufacture
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $CountryOfManufacture;
	/**
	 * The HarmonizedCode
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $HarmonizedCode;
	/**
	 * The Weight
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructWeight
	 */
	public $Weight;
	/**
	 * The Quantity
	 * Meta informations extracted from the WSDL
	 * - documentation : This field is used for enterprise transactions.
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $Quantity;
	/**
	 * The QuantityUnits
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $QuantityUnits;
	/**
	 * The AdditionalMeasures
	 * Meta informations extracted from the WSDL
	 * - documentation : Contains only additional quantitative information other than weight and quantity to calculate duties and taxes.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructMeasure
	 */
	public $AdditionalMeasures;
	/**
	 * The UnitPrice
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $UnitPrice;
	/**
	 * The CustomsValue
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $CustomsValue;
	/**
	 * The ExciseConditions
	 * Meta informations extracted from the WSDL
	 * - documentation : Defines additional characteristic of commodity used to calculate duties and taxes
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructEdtExciseCondition
	 */
	public $ExciseConditions;
	/**
	 * The ExportLicenseNumber
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $ExportLicenseNumber;
	/**
	 * The ExportLicenseExpirationDate
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var date
	 */
	public $ExportLicenseExpirationDate;
	/**
	 * The CIMarksAndNumbers
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $CIMarksAndNumbers;
	/**
	 * The PartNumber
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $PartNumber;
	/**
	 * The NaftaDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : All data required for this commodity in NAFTA Certificate of Origin.
	 * - minOccurs : 0
	 * @var MytestStructNaftaCommodityDetail
	 */
	public $NaftaDetail;
	/**
	 * Constructor method for Commodity
	 * @see parent::__construct()
	 * @param string $_name
	 * @param nonNegativeInteger $_numberOfPieces
	 * @param string $_description
	 * @param string $_countryOfManufacture
	 * @param string $_harmonizedCode
	 * @param MytestStructWeight $_weight
	 * @param nonNegativeInteger $_quantity
	 * @param string $_quantityUnits
	 * @param MytestStructMeasure $_additionalMeasures
	 * @param MytestStructMoney $_unitPrice
	 * @param MytestStructMoney $_customsValue
	 * @param MytestStructEdtExciseCondition $_exciseConditions
	 * @param string $_exportLicenseNumber
	 * @param date $_exportLicenseExpirationDate
	 * @param string $_cIMarksAndNumbers
	 * @param string $_partNumber
	 * @param MytestStructNaftaCommodityDetail $_naftaDetail
	 * @return MytestStructCommodity
	 */
	public function __construct($_name = NULL,$_numberOfPieces = NULL,$_description = NULL,$_countryOfManufacture = NULL,$_harmonizedCode = NULL,$_weight = NULL,$_quantity = NULL,$_quantityUnits = NULL,$_additionalMeasures = NULL,$_unitPrice = NULL,$_customsValue = NULL,$_exciseConditions = NULL,$_exportLicenseNumber = NULL,$_exportLicenseExpirationDate = NULL,$_cIMarksAndNumbers = NULL,$_partNumber = NULL,$_naftaDetail = NULL)
	{
		parent::__construct(array('Name'=>$_name,'NumberOfPieces'=>$_numberOfPieces,'Description'=>$_description,'CountryOfManufacture'=>$_countryOfManufacture,'HarmonizedCode'=>$_harmonizedCode,'Weight'=>$_weight,'Quantity'=>$_quantity,'QuantityUnits'=>$_quantityUnits,'AdditionalMeasures'=>$_additionalMeasures,'UnitPrice'=>$_unitPrice,'CustomsValue'=>$_customsValue,'ExciseConditions'=>$_exciseConditions,'ExportLicenseNumber'=>$_exportLicenseNumber,'ExportLicenseExpirationDate'=>$_exportLicenseExpirationDate,'CIMarksAndNumbers'=>$_cIMarksAndNumbers,'PartNumber'=>$_partNumber,'NaftaDetail'=>$_naftaDetail));
	}
	/**
	 * Get Name value
	 * @return string|null
	 */
	public function getName()
	{
		return $this->Name;
	}
	/**
	 * Set Name value
	 * @param string $_name the Name
	 * @return string
	 */
	public function setName($_name)
	{
		return ($this->Name = $_name);
	}
	/**
	 * Get NumberOfPieces value
	 * @return nonNegativeInteger|null
	 */
	public function getNumberOfPieces()
	{
		return $this->NumberOfPieces;
	}
	/**
	 * Set NumberOfPieces value
	 * @param nonNegativeInteger $_numberOfPieces the NumberOfPieces
	 * @return nonNegativeInteger
	 */
	public function setNumberOfPieces($_numberOfPieces)
	{
		return ($this->NumberOfPieces = $_numberOfPieces);
	}
	/**
	 * Get Description value
	 * @return string|null
	 */
	public function getDescription()
	{
		return $this->Description;
	}
	/**
	 * Set Description value
	 * @param string $_description the Description
	 * @return string
	 */
	public function setDescription($_description)
	{
		return ($this->Description = $_description);
	}
	/**
	 * Get CountryOfManufacture value
	 * @return string|null
	 */
	public function getCountryOfManufacture()
	{
		return $this->CountryOfManufacture;
	}
	/**
	 * Set CountryOfManufacture value
	 * @param string $_countryOfManufacture the CountryOfManufacture
	 * @return string
	 */
	public function setCountryOfManufacture($_countryOfManufacture)
	{
		return ($this->CountryOfManufacture = $_countryOfManufacture);
	}
	/**
	 * Get HarmonizedCode value
	 * @return string|null
	 */
	public function getHarmonizedCode()
	{
		return $this->HarmonizedCode;
	}
	/**
	 * Set HarmonizedCode value
	 * @param string $_harmonizedCode the HarmonizedCode
	 * @return string
	 */
	public function setHarmonizedCode($_harmonizedCode)
	{
		return ($this->HarmonizedCode = $_harmonizedCode);
	}
	/**
	 * Get Weight value
	 * @return MytestStructWeight|null
	 */
	public function getWeight()
	{
		return $this->Weight;
	}
	/**
	 * Set Weight value
	 * @param MytestStructWeight $_weight the Weight
	 * @return MytestStructWeight
	 */
	public function setWeight($_weight)
	{
		return ($this->Weight = $_weight);
	}
	/**
	 * Get Quantity value
	 * @return nonNegativeInteger|null
	 */
	public function getQuantity()
	{
		return $this->Quantity;
	}
	/**
	 * Set Quantity value
	 * @param nonNegativeInteger $_quantity the Quantity
	 * @return nonNegativeInteger
	 */
	public function setQuantity($_quantity)
	{
		return ($this->Quantity = $_quantity);
	}
	/**
	 * Get QuantityUnits value
	 * @return string|null
	 */
	public function getQuantityUnits()
	{
		return $this->QuantityUnits;
	}
	/**
	 * Set QuantityUnits value
	 * @param string $_quantityUnits the QuantityUnits
	 * @return string
	 */
	public function setQuantityUnits($_quantityUnits)
	{
		return ($this->QuantityUnits = $_quantityUnits);
	}
	/**
	 * Get AdditionalMeasures value
	 * @return MytestStructMeasure|null
	 */
	public function getAdditionalMeasures()
	{
		return $this->AdditionalMeasures;
	}
	/**
	 * Set AdditionalMeasures value
	 * @param MytestStructMeasure $_additionalMeasures the AdditionalMeasures
	 * @return MytestStructMeasure
	 */
	public function setAdditionalMeasures($_additionalMeasures)
	{
		return ($this->AdditionalMeasures = $_additionalMeasures);
	}
	/**
	 * Get UnitPrice value
	 * @return MytestStructMoney|null
	 */
	public function getUnitPrice()
	{
		return $this->UnitPrice;
	}
	/**
	 * Set UnitPrice value
	 * @param MytestStructMoney $_unitPrice the UnitPrice
	 * @return MytestStructMoney
	 */
	public function setUnitPrice($_unitPrice)
	{
		return ($this->UnitPrice = $_unitPrice);
	}
	/**
	 * Get CustomsValue value
	 * @return MytestStructMoney|null
	 */
	public function getCustomsValue()
	{
		return $this->CustomsValue;
	}
	/**
	 * Set CustomsValue value
	 * @param MytestStructMoney $_customsValue the CustomsValue
	 * @return MytestStructMoney
	 */
	public function setCustomsValue($_customsValue)
	{
		return ($this->CustomsValue = $_customsValue);
	}
	/**
	 * Get ExciseConditions value
	 * @return MytestStructEdtExciseCondition|null
	 */
	public function getExciseConditions()
	{
		return $this->ExciseConditions;
	}
	/**
	 * Set ExciseConditions value
	 * @param MytestStructEdtExciseCondition $_exciseConditions the ExciseConditions
	 * @return MytestStructEdtExciseCondition
	 */
	public function setExciseConditions($_exciseConditions)
	{
		return ($this->ExciseConditions = $_exciseConditions);
	}
	/**
	 * Get ExportLicenseNumber value
	 * @return string|null
	 */
	public function getExportLicenseNumber()
	{
		return $this->ExportLicenseNumber;
	}
	/**
	 * Set ExportLicenseNumber value
	 * @param string $_exportLicenseNumber the ExportLicenseNumber
	 * @return string
	 */
	public function setExportLicenseNumber($_exportLicenseNumber)
	{
		return ($this->ExportLicenseNumber = $_exportLicenseNumber);
	}
	/**
	 * Get ExportLicenseExpirationDate value
	 * @return date|null
	 */
	public function getExportLicenseExpirationDate()
	{
		return $this->ExportLicenseExpirationDate;
	}
	/**
	 * Set ExportLicenseExpirationDate value
	 * @param date $_exportLicenseExpirationDate the ExportLicenseExpirationDate
	 * @return date
	 */
	public function setExportLicenseExpirationDate($_exportLicenseExpirationDate)
	{
		return ($this->ExportLicenseExpirationDate = $_exportLicenseExpirationDate);
	}
	/**
	 * Get CIMarksAndNumbers value
	 * @return string|null
	 */
	public function getCIMarksAndNumbers()
	{
		return $this->CIMarksAndNumbers;
	}
	/**
	 * Set CIMarksAndNumbers value
	 * @param string $_cIMarksAndNumbers the CIMarksAndNumbers
	 * @return string
	 */
	public function setCIMarksAndNumbers($_cIMarksAndNumbers)
	{
		return ($this->CIMarksAndNumbers = $_cIMarksAndNumbers);
	}
	/**
	 * Get PartNumber value
	 * @return string|null
	 */
	public function getPartNumber()
	{
		return $this->PartNumber;
	}
	/**
	 * Set PartNumber value
	 * @param string $_partNumber the PartNumber
	 * @return string
	 */
	public function setPartNumber($_partNumber)
	{
		return ($this->PartNumber = $_partNumber);
	}
	/**
	 * Get NaftaDetail value
	 * @return MytestStructNaftaCommodityDetail|null
	 */
	public function getNaftaDetail()
	{
		return $this->NaftaDetail;
	}
	/**
	 * Set NaftaDetail value
	 * @param MytestStructNaftaCommodityDetail $_naftaDetail the NaftaDetail
	 * @return MytestStructNaftaCommodityDetail
	 */
	public function setNaftaDetail($_naftaDetail)
	{
		return ($this->NaftaDetail = $_naftaDetail);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCommodity
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>