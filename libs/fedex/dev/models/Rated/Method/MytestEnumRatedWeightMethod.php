<?php
/**
 * File for class MytestEnumRatedWeightMethod
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumRatedWeightMethod originally named RatedWeightMethod
 * Documentation : The method used to calculate the weight to be used in rating the package..
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumRatedWeightMethod extends MytestWsdlClass
{
	/**
	 * Constant for value 'ACTUAL'
	 * @return string 'ACTUAL'
	 */
	const VALUE_ACTUAL = 'ACTUAL';
	/**
	 * Constant for value 'AVERAGE_PACKAGE_WEIGHT_MINIMUM'
	 * @return string 'AVERAGE_PACKAGE_WEIGHT_MINIMUM'
	 */
	const VALUE_AVERAGE_PACKAGE_WEIGHT_MINIMUM = 'AVERAGE_PACKAGE_WEIGHT_MINIMUM';
	/**
	 * Constant for value 'BALLOON'
	 * @return string 'BALLOON'
	 */
	const VALUE_BALLOON = 'BALLOON';
	/**
	 * Constant for value 'DIM'
	 * @return string 'DIM'
	 */
	const VALUE_DIM = 'DIM';
	/**
	 * Constant for value 'FREIGHT_MINIMUM'
	 * @return string 'FREIGHT_MINIMUM'
	 */
	const VALUE_FREIGHT_MINIMUM = 'FREIGHT_MINIMUM';
	/**
	 * Constant for value 'MIXED'
	 * @return string 'MIXED'
	 */
	const VALUE_MIXED = 'MIXED';
	/**
	 * Constant for value 'OVERSIZE'
	 * @return string 'OVERSIZE'
	 */
	const VALUE_OVERSIZE = 'OVERSIZE';
	/**
	 * Constant for value 'OVERSIZE_1'
	 * @return string 'OVERSIZE_1'
	 */
	const VALUE_OVERSIZE_1 = 'OVERSIZE_1';
	/**
	 * Constant for value 'OVERSIZE_2'
	 * @return string 'OVERSIZE_2'
	 */
	const VALUE_OVERSIZE_2 = 'OVERSIZE_2';
	/**
	 * Constant for value 'OVERSIZE_3'
	 * @return string 'OVERSIZE_3'
	 */
	const VALUE_OVERSIZE_3 = 'OVERSIZE_3';
	/**
	 * Constant for value 'PACKAGING_MINIMUM'
	 * @return string 'PACKAGING_MINIMUM'
	 */
	const VALUE_PACKAGING_MINIMUM = 'PACKAGING_MINIMUM';
	/**
	 * Constant for value 'WEIGHT_BREAK'
	 * @return string 'WEIGHT_BREAK'
	 */
	const VALUE_WEIGHT_BREAK = 'WEIGHT_BREAK';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumRatedWeightMethod::VALUE_ACTUAL
	 * @uses MytestEnumRatedWeightMethod::VALUE_AVERAGE_PACKAGE_WEIGHT_MINIMUM
	 * @uses MytestEnumRatedWeightMethod::VALUE_BALLOON
	 * @uses MytestEnumRatedWeightMethod::VALUE_DIM
	 * @uses MytestEnumRatedWeightMethod::VALUE_FREIGHT_MINIMUM
	 * @uses MytestEnumRatedWeightMethod::VALUE_MIXED
	 * @uses MytestEnumRatedWeightMethod::VALUE_OVERSIZE
	 * @uses MytestEnumRatedWeightMethod::VALUE_OVERSIZE_1
	 * @uses MytestEnumRatedWeightMethod::VALUE_OVERSIZE_2
	 * @uses MytestEnumRatedWeightMethod::VALUE_OVERSIZE_3
	 * @uses MytestEnumRatedWeightMethod::VALUE_PACKAGING_MINIMUM
	 * @uses MytestEnumRatedWeightMethod::VALUE_WEIGHT_BREAK
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumRatedWeightMethod::VALUE_ACTUAL,MytestEnumRatedWeightMethod::VALUE_AVERAGE_PACKAGE_WEIGHT_MINIMUM,MytestEnumRatedWeightMethod::VALUE_BALLOON,MytestEnumRatedWeightMethod::VALUE_DIM,MytestEnumRatedWeightMethod::VALUE_FREIGHT_MINIMUM,MytestEnumRatedWeightMethod::VALUE_MIXED,MytestEnumRatedWeightMethod::VALUE_OVERSIZE,MytestEnumRatedWeightMethod::VALUE_OVERSIZE_1,MytestEnumRatedWeightMethod::VALUE_OVERSIZE_2,MytestEnumRatedWeightMethod::VALUE_OVERSIZE_3,MytestEnumRatedWeightMethod::VALUE_PACKAGING_MINIMUM,MytestEnumRatedWeightMethod::VALUE_WEIGHT_BREAK));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>