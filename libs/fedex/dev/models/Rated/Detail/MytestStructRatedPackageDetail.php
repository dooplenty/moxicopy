<?php
/**
 * File for class MytestStructRatedPackageDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructRatedPackageDetail originally named RatedPackageDetail
 * Documentation : If requesting rates using the PackageDetails element (one package at a time) in the request, the rates for each package will be returned in this element. Currently total piece total weight rates are also retuned in this element.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructRatedPackageDetail extends MytestWsdlClass
{
	/**
	 * The TrackingIds
	 * Meta informations extracted from the WSDL
	 * - documentation : Echoed from the corresponding package in the rate request (if provided).
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructTrackingId
	 */
	public $TrackingIds;
	/**
	 * The GroupNumber
	 * Meta informations extracted from the WSDL
	 * - documentation : Used with request containing PACKAGE_GROUPS, to identify which group of identical packages was used to produce a reply item.
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $GroupNumber;
	/**
	 * The EffectiveNetDiscount
	 * Meta informations extracted from the WSDL
	 * - documentation : The difference between "list" and "account" net charge.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $EffectiveNetDiscount;
	/**
	 * The AdjustedCodCollectionAmount
	 * Meta informations extracted from the WSDL
	 * - documentation : Ground COD is shipment level.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $AdjustedCodCollectionAmount;
	/**
	 * The OversizeClass
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumOversizeClassType
	 */
	public $OversizeClass;
	/**
	 * The PackageRateDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Rate data that are tied to a specific package and rate type combination.
	 * - minOccurs : 0
	 * @var MytestStructPackageRateDetail
	 */
	public $PackageRateDetail;
	/**
	 * Constructor method for RatedPackageDetail
	 * @see parent::__construct()
	 * @param MytestStructTrackingId $_trackingIds
	 * @param nonNegativeInteger $_groupNumber
	 * @param MytestStructMoney $_effectiveNetDiscount
	 * @param MytestStructMoney $_adjustedCodCollectionAmount
	 * @param MytestEnumOversizeClassType $_oversizeClass
	 * @param MytestStructPackageRateDetail $_packageRateDetail
	 * @return MytestStructRatedPackageDetail
	 */
	public function __construct($_trackingIds = NULL,$_groupNumber = NULL,$_effectiveNetDiscount = NULL,$_adjustedCodCollectionAmount = NULL,$_oversizeClass = NULL,$_packageRateDetail = NULL)
	{
		parent::__construct(array('TrackingIds'=>$_trackingIds,'GroupNumber'=>$_groupNumber,'EffectiveNetDiscount'=>$_effectiveNetDiscount,'AdjustedCodCollectionAmount'=>$_adjustedCodCollectionAmount,'OversizeClass'=>$_oversizeClass,'PackageRateDetail'=>$_packageRateDetail));
	}
	/**
	 * Get TrackingIds value
	 * @return MytestStructTrackingId|null
	 */
	public function getTrackingIds()
	{
		return $this->TrackingIds;
	}
	/**
	 * Set TrackingIds value
	 * @param MytestStructTrackingId $_trackingIds the TrackingIds
	 * @return MytestStructTrackingId
	 */
	public function setTrackingIds($_trackingIds)
	{
		return ($this->TrackingIds = $_trackingIds);
	}
	/**
	 * Get GroupNumber value
	 * @return nonNegativeInteger|null
	 */
	public function getGroupNumber()
	{
		return $this->GroupNumber;
	}
	/**
	 * Set GroupNumber value
	 * @param nonNegativeInteger $_groupNumber the GroupNumber
	 * @return nonNegativeInteger
	 */
	public function setGroupNumber($_groupNumber)
	{
		return ($this->GroupNumber = $_groupNumber);
	}
	/**
	 * Get EffectiveNetDiscount value
	 * @return MytestStructMoney|null
	 */
	public function getEffectiveNetDiscount()
	{
		return $this->EffectiveNetDiscount;
	}
	/**
	 * Set EffectiveNetDiscount value
	 * @param MytestStructMoney $_effectiveNetDiscount the EffectiveNetDiscount
	 * @return MytestStructMoney
	 */
	public function setEffectiveNetDiscount($_effectiveNetDiscount)
	{
		return ($this->EffectiveNetDiscount = $_effectiveNetDiscount);
	}
	/**
	 * Get AdjustedCodCollectionAmount value
	 * @return MytestStructMoney|null
	 */
	public function getAdjustedCodCollectionAmount()
	{
		return $this->AdjustedCodCollectionAmount;
	}
	/**
	 * Set AdjustedCodCollectionAmount value
	 * @param MytestStructMoney $_adjustedCodCollectionAmount the AdjustedCodCollectionAmount
	 * @return MytestStructMoney
	 */
	public function setAdjustedCodCollectionAmount($_adjustedCodCollectionAmount)
	{
		return ($this->AdjustedCodCollectionAmount = $_adjustedCodCollectionAmount);
	}
	/**
	 * Get OversizeClass value
	 * @return MytestEnumOversizeClassType|null
	 */
	public function getOversizeClass()
	{
		return $this->OversizeClass;
	}
	/**
	 * Set OversizeClass value
	 * @uses MytestEnumOversizeClassType::valueIsValid()
	 * @param MytestEnumOversizeClassType $_oversizeClass the OversizeClass
	 * @return MytestEnumOversizeClassType
	 */
	public function setOversizeClass($_oversizeClass)
	{
		if(!MytestEnumOversizeClassType::valueIsValid($_oversizeClass))
		{
			return false;
		}
		return ($this->OversizeClass = $_oversizeClass);
	}
	/**
	 * Get PackageRateDetail value
	 * @return MytestStructPackageRateDetail|null
	 */
	public function getPackageRateDetail()
	{
		return $this->PackageRateDetail;
	}
	/**
	 * Set PackageRateDetail value
	 * @param MytestStructPackageRateDetail $_packageRateDetail the PackageRateDetail
	 * @return MytestStructPackageRateDetail
	 */
	public function setPackageRateDetail($_packageRateDetail)
	{
		return ($this->PackageRateDetail = $_packageRateDetail);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructRatedPackageDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>