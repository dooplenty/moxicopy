<?php
/**
 * File for class MytestStructRatedShipmentDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructRatedShipmentDetail originally named RatedShipmentDetail
 * Documentation : This class groups the shipment and package rating data for a specific rate type for use in a rating reply, which groups result data by rate type.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructRatedShipmentDetail extends MytestWsdlClass
{
	/**
	 * The EffectiveNetDiscount
	 * Meta informations extracted from the WSDL
	 * - documentation : The difference between "list" and "account" total net charge.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $EffectiveNetDiscount;
	/**
	 * The AdjustedCodCollectionAmount
	 * Meta informations extracted from the WSDL
	 * - documentation : Express COD is shipment level.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $AdjustedCodCollectionAmount;
	/**
	 * The ShipmentRateDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : The shipment-level totals for this rate type.
	 * - minOccurs : 0
	 * @var MytestStructShipmentRateDetail
	 */
	public $ShipmentRateDetail;
	/**
	 * The RatedPackages
	 * Meta informations extracted from the WSDL
	 * - documentation : The package-level data for this rate type.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructRatedPackageDetail
	 */
	public $RatedPackages;
	/**
	 * Constructor method for RatedShipmentDetail
	 * @see parent::__construct()
	 * @param MytestStructMoney $_effectiveNetDiscount
	 * @param MytestStructMoney $_adjustedCodCollectionAmount
	 * @param MytestStructShipmentRateDetail $_shipmentRateDetail
	 * @param MytestStructRatedPackageDetail $_ratedPackages
	 * @return MytestStructRatedShipmentDetail
	 */
	public function __construct($_effectiveNetDiscount = NULL,$_adjustedCodCollectionAmount = NULL,$_shipmentRateDetail = NULL,$_ratedPackages = NULL)
	{
		parent::__construct(array('EffectiveNetDiscount'=>$_effectiveNetDiscount,'AdjustedCodCollectionAmount'=>$_adjustedCodCollectionAmount,'ShipmentRateDetail'=>$_shipmentRateDetail,'RatedPackages'=>$_ratedPackages));
	}
	/**
	 * Get EffectiveNetDiscount value
	 * @return MytestStructMoney|null
	 */
	public function getEffectiveNetDiscount()
	{
		return $this->EffectiveNetDiscount;
	}
	/**
	 * Set EffectiveNetDiscount value
	 * @param MytestStructMoney $_effectiveNetDiscount the EffectiveNetDiscount
	 * @return MytestStructMoney
	 */
	public function setEffectiveNetDiscount($_effectiveNetDiscount)
	{
		return ($this->EffectiveNetDiscount = $_effectiveNetDiscount);
	}
	/**
	 * Get AdjustedCodCollectionAmount value
	 * @return MytestStructMoney|null
	 */
	public function getAdjustedCodCollectionAmount()
	{
		return $this->AdjustedCodCollectionAmount;
	}
	/**
	 * Set AdjustedCodCollectionAmount value
	 * @param MytestStructMoney $_adjustedCodCollectionAmount the AdjustedCodCollectionAmount
	 * @return MytestStructMoney
	 */
	public function setAdjustedCodCollectionAmount($_adjustedCodCollectionAmount)
	{
		return ($this->AdjustedCodCollectionAmount = $_adjustedCodCollectionAmount);
	}
	/**
	 * Get ShipmentRateDetail value
	 * @return MytestStructShipmentRateDetail|null
	 */
	public function getShipmentRateDetail()
	{
		return $this->ShipmentRateDetail;
	}
	/**
	 * Set ShipmentRateDetail value
	 * @param MytestStructShipmentRateDetail $_shipmentRateDetail the ShipmentRateDetail
	 * @return MytestStructShipmentRateDetail
	 */
	public function setShipmentRateDetail($_shipmentRateDetail)
	{
		return ($this->ShipmentRateDetail = $_shipmentRateDetail);
	}
	/**
	 * Get RatedPackages value
	 * @return MytestStructRatedPackageDetail|null
	 */
	public function getRatedPackages()
	{
		return $this->RatedPackages;
	}
	/**
	 * Set RatedPackages value
	 * @param MytestStructRatedPackageDetail $_ratedPackages the RatedPackages
	 * @return MytestStructRatedPackageDetail
	 */
	public function setRatedPackages($_ratedPackages)
	{
		return ($this->RatedPackages = $_ratedPackages);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructRatedShipmentDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>