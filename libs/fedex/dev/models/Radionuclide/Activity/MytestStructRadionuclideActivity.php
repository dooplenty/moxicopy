<?php
/**
 * File for class MytestStructRadionuclideActivity
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructRadionuclideActivity originally named RadionuclideActivity
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructRadionuclideActivity extends MytestWsdlClass
{
	/**
	 * The Value
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var decimal
	 */
	public $Value;
	/**
	 * The UnitOfMeasure
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumRadioactivityUnitOfMeasure
	 */
	public $UnitOfMeasure;
	/**
	 * Constructor method for RadionuclideActivity
	 * @see parent::__construct()
	 * @param decimal $_value
	 * @param MytestEnumRadioactivityUnitOfMeasure $_unitOfMeasure
	 * @return MytestStructRadionuclideActivity
	 */
	public function __construct($_value = NULL,$_unitOfMeasure = NULL)
	{
		parent::__construct(array('Value'=>$_value,'UnitOfMeasure'=>$_unitOfMeasure));
	}
	/**
	 * Get Value value
	 * @return decimal|null
	 */
	public function getValue()
	{
		return $this->Value;
	}
	/**
	 * Set Value value
	 * @param decimal $_value the Value
	 * @return decimal
	 */
	public function setValue($_value)
	{
		return ($this->Value = $_value);
	}
	/**
	 * Get UnitOfMeasure value
	 * @return MytestEnumRadioactivityUnitOfMeasure|null
	 */
	public function getUnitOfMeasure()
	{
		return $this->UnitOfMeasure;
	}
	/**
	 * Set UnitOfMeasure value
	 * @uses MytestEnumRadioactivityUnitOfMeasure::valueIsValid()
	 * @param MytestEnumRadioactivityUnitOfMeasure $_unitOfMeasure the UnitOfMeasure
	 * @return MytestEnumRadioactivityUnitOfMeasure
	 */
	public function setUnitOfMeasure($_unitOfMeasure)
	{
		if(!MytestEnumRadioactivityUnitOfMeasure::valueIsValid($_unitOfMeasure))
		{
			return false;
		}
		return ($this->UnitOfMeasure = $_unitOfMeasure);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructRadionuclideActivity
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>