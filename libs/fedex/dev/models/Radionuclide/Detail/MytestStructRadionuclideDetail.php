<?php
/**
 * File for class MytestStructRadionuclideDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructRadionuclideDetail originally named RadionuclideDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructRadionuclideDetail extends MytestWsdlClass
{
	/**
	 * The Radionuclide
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Radionuclide;
	/**
	 * The Activity
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructRadionuclideActivity
	 */
	public $Activity;
	/**
	 * The ExceptedPackagingIsReportableQuantity
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates whether packaging type "EXCEPTED" or "EXCEPTED_PACKAGE" is for radioactive material in reportable quantity.
	 * - minOccurs : 0
	 * @var boolean
	 */
	public $ExceptedPackagingIsReportableQuantity;
	/**
	 * The PhysicalForm
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumPhysicalFormType
	 */
	public $PhysicalForm;
	/**
	 * The ChemicalForm
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $ChemicalForm;
	/**
	 * Constructor method for RadionuclideDetail
	 * @see parent::__construct()
	 * @param string $_radionuclide
	 * @param MytestStructRadionuclideActivity $_activity
	 * @param boolean $_exceptedPackagingIsReportableQuantity
	 * @param MytestEnumPhysicalFormType $_physicalForm
	 * @param string $_chemicalForm
	 * @return MytestStructRadionuclideDetail
	 */
	public function __construct($_radionuclide = NULL,$_activity = NULL,$_exceptedPackagingIsReportableQuantity = NULL,$_physicalForm = NULL,$_chemicalForm = NULL)
	{
		parent::__construct(array('Radionuclide'=>$_radionuclide,'Activity'=>$_activity,'ExceptedPackagingIsReportableQuantity'=>$_exceptedPackagingIsReportableQuantity,'PhysicalForm'=>$_physicalForm,'ChemicalForm'=>$_chemicalForm));
	}
	/**
	 * Get Radionuclide value
	 * @return string|null
	 */
	public function getRadionuclide()
	{
		return $this->Radionuclide;
	}
	/**
	 * Set Radionuclide value
	 * @param string $_radionuclide the Radionuclide
	 * @return string
	 */
	public function setRadionuclide($_radionuclide)
	{
		return ($this->Radionuclide = $_radionuclide);
	}
	/**
	 * Get Activity value
	 * @return MytestStructRadionuclideActivity|null
	 */
	public function getActivity()
	{
		return $this->Activity;
	}
	/**
	 * Set Activity value
	 * @param MytestStructRadionuclideActivity $_activity the Activity
	 * @return MytestStructRadionuclideActivity
	 */
	public function setActivity($_activity)
	{
		return ($this->Activity = $_activity);
	}
	/**
	 * Get ExceptedPackagingIsReportableQuantity value
	 * @return boolean|null
	 */
	public function getExceptedPackagingIsReportableQuantity()
	{
		return $this->ExceptedPackagingIsReportableQuantity;
	}
	/**
	 * Set ExceptedPackagingIsReportableQuantity value
	 * @param boolean $_exceptedPackagingIsReportableQuantity the ExceptedPackagingIsReportableQuantity
	 * @return boolean
	 */
	public function setExceptedPackagingIsReportableQuantity($_exceptedPackagingIsReportableQuantity)
	{
		return ($this->ExceptedPackagingIsReportableQuantity = $_exceptedPackagingIsReportableQuantity);
	}
	/**
	 * Get PhysicalForm value
	 * @return MytestEnumPhysicalFormType|null
	 */
	public function getPhysicalForm()
	{
		return $this->PhysicalForm;
	}
	/**
	 * Set PhysicalForm value
	 * @uses MytestEnumPhysicalFormType::valueIsValid()
	 * @param MytestEnumPhysicalFormType $_physicalForm the PhysicalForm
	 * @return MytestEnumPhysicalFormType
	 */
	public function setPhysicalForm($_physicalForm)
	{
		if(!MytestEnumPhysicalFormType::valueIsValid($_physicalForm))
		{
			return false;
		}
		return ($this->PhysicalForm = $_physicalForm);
	}
	/**
	 * Get ChemicalForm value
	 * @return string|null
	 */
	public function getChemicalForm()
	{
		return $this->ChemicalForm;
	}
	/**
	 * Set ChemicalForm value
	 * @param string $_chemicalForm the ChemicalForm
	 * @return string
	 */
	public function setChemicalForm($_chemicalForm)
	{
		return ($this->ChemicalForm = $_chemicalForm);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructRadionuclideDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>