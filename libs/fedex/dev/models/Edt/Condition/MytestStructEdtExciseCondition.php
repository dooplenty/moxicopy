<?php
/**
 * File for class MytestStructEdtExciseCondition
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructEdtExciseCondition originally named EdtExciseCondition
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructEdtExciseCondition extends MytestWsdlClass
{
	/**
	 * The Category
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Category;
	/**
	 * The Value
	 * Meta informations extracted from the WSDL
	 * - documentation : Customer-declared value, with data type and legal values depending on excise condition, used in defining the taxable value of the item.
	 * - minOccurs : 0
	 * @var string
	 */
	public $Value;
	/**
	 * Constructor method for EdtExciseCondition
	 * @see parent::__construct()
	 * @param string $_category
	 * @param string $_value
	 * @return MytestStructEdtExciseCondition
	 */
	public function __construct($_category = NULL,$_value = NULL)
	{
		parent::__construct(array('Category'=>$_category,'Value'=>$_value));
	}
	/**
	 * Get Category value
	 * @return string|null
	 */
	public function getCategory()
	{
		return $this->Category;
	}
	/**
	 * Set Category value
	 * @param string $_category the Category
	 * @return string
	 */
	public function setCategory($_category)
	{
		return ($this->Category = $_category);
	}
	/**
	 * Get Value value
	 * @return string|null
	 */
	public function getValue()
	{
		return $this->Value;
	}
	/**
	 * Set Value value
	 * @param string $_value the Value
	 * @return string
	 */
	public function setValue($_value)
	{
		return ($this->Value = $_value);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructEdtExciseCondition
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>