<?php
/**
 * File for class MytestStructEdtTaxDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructEdtTaxDetail originally named EdtTaxDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructEdtTaxDetail extends MytestWsdlClass
{
	/**
	 * The TaxType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumEdtTaxType
	 */
	public $TaxType;
	/**
	 * The EffectiveDate
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var date
	 */
	public $EffectiveDate;
	/**
	 * The Name
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Name;
	/**
	 * The TaxableValue
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TaxableValue;
	/**
	 * The Description
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Description;
	/**
	 * The Formula
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Formula;
	/**
	 * The Amount
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $Amount;
	/**
	 * Constructor method for EdtTaxDetail
	 * @see parent::__construct()
	 * @param MytestEnumEdtTaxType $_taxType
	 * @param date $_effectiveDate
	 * @param string $_name
	 * @param MytestStructMoney $_taxableValue
	 * @param string $_description
	 * @param string $_formula
	 * @param MytestStructMoney $_amount
	 * @return MytestStructEdtTaxDetail
	 */
	public function __construct($_taxType = NULL,$_effectiveDate = NULL,$_name = NULL,$_taxableValue = NULL,$_description = NULL,$_formula = NULL,$_amount = NULL)
	{
		parent::__construct(array('TaxType'=>$_taxType,'EffectiveDate'=>$_effectiveDate,'Name'=>$_name,'TaxableValue'=>$_taxableValue,'Description'=>$_description,'Formula'=>$_formula,'Amount'=>$_amount));
	}
	/**
	 * Get TaxType value
	 * @return MytestEnumEdtTaxType|null
	 */
	public function getTaxType()
	{
		return $this->TaxType;
	}
	/**
	 * Set TaxType value
	 * @uses MytestEnumEdtTaxType::valueIsValid()
	 * @param MytestEnumEdtTaxType $_taxType the TaxType
	 * @return MytestEnumEdtTaxType
	 */
	public function setTaxType($_taxType)
	{
		if(!MytestEnumEdtTaxType::valueIsValid($_taxType))
		{
			return false;
		}
		return ($this->TaxType = $_taxType);
	}
	/**
	 * Get EffectiveDate value
	 * @return date|null
	 */
	public function getEffectiveDate()
	{
		return $this->EffectiveDate;
	}
	/**
	 * Set EffectiveDate value
	 * @param date $_effectiveDate the EffectiveDate
	 * @return date
	 */
	public function setEffectiveDate($_effectiveDate)
	{
		return ($this->EffectiveDate = $_effectiveDate);
	}
	/**
	 * Get Name value
	 * @return string|null
	 */
	public function getName()
	{
		return $this->Name;
	}
	/**
	 * Set Name value
	 * @param string $_name the Name
	 * @return string
	 */
	public function setName($_name)
	{
		return ($this->Name = $_name);
	}
	/**
	 * Get TaxableValue value
	 * @return MytestStructMoney|null
	 */
	public function getTaxableValue()
	{
		return $this->TaxableValue;
	}
	/**
	 * Set TaxableValue value
	 * @param MytestStructMoney $_taxableValue the TaxableValue
	 * @return MytestStructMoney
	 */
	public function setTaxableValue($_taxableValue)
	{
		return ($this->TaxableValue = $_taxableValue);
	}
	/**
	 * Get Description value
	 * @return string|null
	 */
	public function getDescription()
	{
		return $this->Description;
	}
	/**
	 * Set Description value
	 * @param string $_description the Description
	 * @return string
	 */
	public function setDescription($_description)
	{
		return ($this->Description = $_description);
	}
	/**
	 * Get Formula value
	 * @return string|null
	 */
	public function getFormula()
	{
		return $this->Formula;
	}
	/**
	 * Set Formula value
	 * @param string $_formula the Formula
	 * @return string
	 */
	public function setFormula($_formula)
	{
		return ($this->Formula = $_formula);
	}
	/**
	 * Get Amount value
	 * @return MytestStructMoney|null
	 */
	public function getAmount()
	{
		return $this->Amount;
	}
	/**
	 * Set Amount value
	 * @param MytestStructMoney $_amount the Amount
	 * @return MytestStructMoney
	 */
	public function setAmount($_amount)
	{
		return ($this->Amount = $_amount);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructEdtTaxDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>