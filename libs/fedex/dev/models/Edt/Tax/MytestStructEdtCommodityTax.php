<?php
/**
 * File for class MytestStructEdtCommodityTax
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructEdtCommodityTax originally named EdtCommodityTax
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructEdtCommodityTax extends MytestWsdlClass
{
	/**
	 * The HarmonizedCode
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $HarmonizedCode;
	/**
	 * The Taxes
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructEdtTaxDetail
	 */
	public $Taxes;
	/**
	 * Constructor method for EdtCommodityTax
	 * @see parent::__construct()
	 * @param string $_harmonizedCode
	 * @param MytestStructEdtTaxDetail $_taxes
	 * @return MytestStructEdtCommodityTax
	 */
	public function __construct($_harmonizedCode = NULL,$_taxes = NULL)
	{
		parent::__construct(array('HarmonizedCode'=>$_harmonizedCode,'Taxes'=>$_taxes));
	}
	/**
	 * Get HarmonizedCode value
	 * @return string|null
	 */
	public function getHarmonizedCode()
	{
		return $this->HarmonizedCode;
	}
	/**
	 * Set HarmonizedCode value
	 * @param string $_harmonizedCode the HarmonizedCode
	 * @return string
	 */
	public function setHarmonizedCode($_harmonizedCode)
	{
		return ($this->HarmonizedCode = $_harmonizedCode);
	}
	/**
	 * Get Taxes value
	 * @return MytestStructEdtTaxDetail|null
	 */
	public function getTaxes()
	{
		return $this->Taxes;
	}
	/**
	 * Set Taxes value
	 * @param MytestStructEdtTaxDetail $_taxes the Taxes
	 * @return MytestStructEdtTaxDetail
	 */
	public function setTaxes($_taxes)
	{
		return ($this->Taxes = $_taxes);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructEdtCommodityTax
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>