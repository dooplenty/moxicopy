<?php
/**
 * File for class MytestEnumEdtTaxType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumEdtTaxType originally named EdtTaxType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumEdtTaxType extends MytestWsdlClass
{
	/**
	 * Constant for value 'ADDITIONAL_TAXES'
	 * @return string 'ADDITIONAL_TAXES'
	 */
	const VALUE_ADDITIONAL_TAXES = 'ADDITIONAL_TAXES';
	/**
	 * Constant for value 'CONSULAR_INVOICE_FEE'
	 * @return string 'CONSULAR_INVOICE_FEE'
	 */
	const VALUE_CONSULAR_INVOICE_FEE = 'CONSULAR_INVOICE_FEE';
	/**
	 * Constant for value 'CUSTOMS_SURCHARGES'
	 * @return string 'CUSTOMS_SURCHARGES'
	 */
	const VALUE_CUSTOMS_SURCHARGES = 'CUSTOMS_SURCHARGES';
	/**
	 * Constant for value 'DUTY'
	 * @return string 'DUTY'
	 */
	const VALUE_DUTY = 'DUTY';
	/**
	 * Constant for value 'EXCISE_TAX'
	 * @return string 'EXCISE_TAX'
	 */
	const VALUE_EXCISE_TAX = 'EXCISE_TAX';
	/**
	 * Constant for value 'FOREIGN_EXCHANGE_TAX'
	 * @return string 'FOREIGN_EXCHANGE_TAX'
	 */
	const VALUE_FOREIGN_EXCHANGE_TAX = 'FOREIGN_EXCHANGE_TAX';
	/**
	 * Constant for value 'GENERAL_SALES_TAX'
	 * @return string 'GENERAL_SALES_TAX'
	 */
	const VALUE_GENERAL_SALES_TAX = 'GENERAL_SALES_TAX';
	/**
	 * Constant for value 'IMPORT_LICENSE_FEE'
	 * @return string 'IMPORT_LICENSE_FEE'
	 */
	const VALUE_IMPORT_LICENSE_FEE = 'IMPORT_LICENSE_FEE';
	/**
	 * Constant for value 'INTERNAL_ADDITIONAL_TAXES'
	 * @return string 'INTERNAL_ADDITIONAL_TAXES'
	 */
	const VALUE_INTERNAL_ADDITIONAL_TAXES = 'INTERNAL_ADDITIONAL_TAXES';
	/**
	 * Constant for value 'INTERNAL_SENSITIVE_PRODUCTS_TAX'
	 * @return string 'INTERNAL_SENSITIVE_PRODUCTS_TAX'
	 */
	const VALUE_INTERNAL_SENSITIVE_PRODUCTS_TAX = 'INTERNAL_SENSITIVE_PRODUCTS_TAX';
	/**
	 * Constant for value 'OTHER'
	 * @return string 'OTHER'
	 */
	const VALUE_OTHER = 'OTHER';
	/**
	 * Constant for value 'SENSITIVE_PRODUCTS_TAX'
	 * @return string 'SENSITIVE_PRODUCTS_TAX'
	 */
	const VALUE_SENSITIVE_PRODUCTS_TAX = 'SENSITIVE_PRODUCTS_TAX';
	/**
	 * Constant for value 'STAMP_TAX'
	 * @return string 'STAMP_TAX'
	 */
	const VALUE_STAMP_TAX = 'STAMP_TAX';
	/**
	 * Constant for value 'STATISTICAL_TAX'
	 * @return string 'STATISTICAL_TAX'
	 */
	const VALUE_STATISTICAL_TAX = 'STATISTICAL_TAX';
	/**
	 * Constant for value 'TRANSPORT_FACILITIES_TAX'
	 * @return string 'TRANSPORT_FACILITIES_TAX'
	 */
	const VALUE_TRANSPORT_FACILITIES_TAX = 'TRANSPORT_FACILITIES_TAX';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumEdtTaxType::VALUE_ADDITIONAL_TAXES
	 * @uses MytestEnumEdtTaxType::VALUE_CONSULAR_INVOICE_FEE
	 * @uses MytestEnumEdtTaxType::VALUE_CUSTOMS_SURCHARGES
	 * @uses MytestEnumEdtTaxType::VALUE_DUTY
	 * @uses MytestEnumEdtTaxType::VALUE_EXCISE_TAX
	 * @uses MytestEnumEdtTaxType::VALUE_FOREIGN_EXCHANGE_TAX
	 * @uses MytestEnumEdtTaxType::VALUE_GENERAL_SALES_TAX
	 * @uses MytestEnumEdtTaxType::VALUE_IMPORT_LICENSE_FEE
	 * @uses MytestEnumEdtTaxType::VALUE_INTERNAL_ADDITIONAL_TAXES
	 * @uses MytestEnumEdtTaxType::VALUE_INTERNAL_SENSITIVE_PRODUCTS_TAX
	 * @uses MytestEnumEdtTaxType::VALUE_OTHER
	 * @uses MytestEnumEdtTaxType::VALUE_SENSITIVE_PRODUCTS_TAX
	 * @uses MytestEnumEdtTaxType::VALUE_STAMP_TAX
	 * @uses MytestEnumEdtTaxType::VALUE_STATISTICAL_TAX
	 * @uses MytestEnumEdtTaxType::VALUE_TRANSPORT_FACILITIES_TAX
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumEdtTaxType::VALUE_ADDITIONAL_TAXES,MytestEnumEdtTaxType::VALUE_CONSULAR_INVOICE_FEE,MytestEnumEdtTaxType::VALUE_CUSTOMS_SURCHARGES,MytestEnumEdtTaxType::VALUE_DUTY,MytestEnumEdtTaxType::VALUE_EXCISE_TAX,MytestEnumEdtTaxType::VALUE_FOREIGN_EXCHANGE_TAX,MytestEnumEdtTaxType::VALUE_GENERAL_SALES_TAX,MytestEnumEdtTaxType::VALUE_IMPORT_LICENSE_FEE,MytestEnumEdtTaxType::VALUE_INTERNAL_ADDITIONAL_TAXES,MytestEnumEdtTaxType::VALUE_INTERNAL_SENSITIVE_PRODUCTS_TAX,MytestEnumEdtTaxType::VALUE_OTHER,MytestEnumEdtTaxType::VALUE_SENSITIVE_PRODUCTS_TAX,MytestEnumEdtTaxType::VALUE_STAMP_TAX,MytestEnumEdtTaxType::VALUE_STATISTICAL_TAX,MytestEnumEdtTaxType::VALUE_TRANSPORT_FACILITIES_TAX));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>