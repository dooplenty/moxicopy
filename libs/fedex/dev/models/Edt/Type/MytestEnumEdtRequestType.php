<?php
/**
 * File for class MytestEnumEdtRequestType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumEdtRequestType originally named EdtRequestType
 * Documentation : Specifies the types of Estimated Duties and Taxes to be included in a rate quotation for an international shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumEdtRequestType extends MytestWsdlClass
{
	/**
	 * Constant for value 'ALL'
	 * @return string 'ALL'
	 */
	const VALUE_ALL = 'ALL';
	/**
	 * Constant for value 'NONE'
	 * @return string 'NONE'
	 */
	const VALUE_NONE = 'NONE';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumEdtRequestType::VALUE_ALL
	 * @uses MytestEnumEdtRequestType::VALUE_NONE
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumEdtRequestType::VALUE_ALL,MytestEnumEdtRequestType::VALUE_NONE));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>