<?php
/**
 * File for class MytestStructTaxpayerIdentification
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructTaxpayerIdentification originally named TaxpayerIdentification
 * Documentation : The descriptive data for taxpayer identification information.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructTaxpayerIdentification extends MytestWsdlClass
{
	/**
	 * The TinType
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the category of the taxpayer identification number. See TinType for the list of values.
	 * - minOccurs : 1
	 * @var MytestEnumTinType
	 */
	public $TinType;
	/**
	 * The Number
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the taxpayer identification number.
	 * - minOccurs : 1
	 * @var string
	 */
	public $Number;
	/**
	 * The Usage
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the usage of Tax Identification Number in Shipment processing
	 * - minOccurs : 0
	 * @var string
	 */
	public $Usage;
	/**
	 * Constructor method for TaxpayerIdentification
	 * @see parent::__construct()
	 * @param MytestEnumTinType $_tinType
	 * @param string $_number
	 * @param string $_usage
	 * @return MytestStructTaxpayerIdentification
	 */
	public function __construct($_tinType,$_number,$_usage = NULL)
	{
		parent::__construct(array('TinType'=>$_tinType,'Number'=>$_number,'Usage'=>$_usage));
	}
	/**
	 * Get TinType value
	 * @return MytestEnumTinType
	 */
	public function getTinType()
	{
		return $this->TinType;
	}
	/**
	 * Set TinType value
	 * @uses MytestEnumTinType::valueIsValid()
	 * @param MytestEnumTinType $_tinType the TinType
	 * @return MytestEnumTinType
	 */
	public function setTinType($_tinType)
	{
		if(!MytestEnumTinType::valueIsValid($_tinType))
		{
			return false;
		}
		return ($this->TinType = $_tinType);
	}
	/**
	 * Get Number value
	 * @return string
	 */
	public function getNumber()
	{
		return $this->Number;
	}
	/**
	 * Set Number value
	 * @param string $_number the Number
	 * @return string
	 */
	public function setNumber($_number)
	{
		return ($this->Number = $_number);
	}
	/**
	 * Get Usage value
	 * @return string|null
	 */
	public function getUsage()
	{
		return $this->Usage;
	}
	/**
	 * Set Usage value
	 * @param string $_usage the Usage
	 * @return string
	 */
	public function setUsage($_usage)
	{
		return ($this->Usage = $_usage);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructTaxpayerIdentification
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>