<?php
/**
 * File for class MytestStructLinearMeasure
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructLinearMeasure originally named LinearMeasure
 * Documentation : Represents a one-dimensional measurement in small units (e.g. suitable for measuring a package or document), contrasted with Distance, which represents a large one-dimensional measurement (e.g. distance between cities).
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructLinearMeasure extends MytestWsdlClass
{
	/**
	 * The Value
	 * Meta informations extracted from the WSDL
	 * - documentation : The numerical quantity of this measurement.
	 * - minOccurs : 0
	 * @var decimal
	 */
	public $Value;
	/**
	 * The Units
	 * Meta informations extracted from the WSDL
	 * - documentation : The units for this measurement.
	 * - minOccurs : 0
	 * @var MytestEnumLinearUnits
	 */
	public $Units;
	/**
	 * Constructor method for LinearMeasure
	 * @see parent::__construct()
	 * @param decimal $_value
	 * @param MytestEnumLinearUnits $_units
	 * @return MytestStructLinearMeasure
	 */
	public function __construct($_value = NULL,$_units = NULL)
	{
		parent::__construct(array('Value'=>$_value,'Units'=>$_units));
	}
	/**
	 * Get Value value
	 * @return decimal|null
	 */
	public function getValue()
	{
		return $this->Value;
	}
	/**
	 * Set Value value
	 * @param decimal $_value the Value
	 * @return decimal
	 */
	public function setValue($_value)
	{
		return ($this->Value = $_value);
	}
	/**
	 * Get Units value
	 * @return MytestEnumLinearUnits|null
	 */
	public function getUnits()
	{
		return $this->Units;
	}
	/**
	 * Set Units value
	 * @uses MytestEnumLinearUnits::valueIsValid()
	 * @param MytestEnumLinearUnits $_units the Units
	 * @return MytestEnumLinearUnits
	 */
	public function setUnits($_units)
	{
		if(!MytestEnumLinearUnits::valueIsValid($_units))
		{
			return false;
		}
		return ($this->Units = $_units);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructLinearMeasure
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>