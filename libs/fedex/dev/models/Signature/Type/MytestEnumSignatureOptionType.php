<?php
/**
 * File for class MytestEnumSignatureOptionType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumSignatureOptionType originally named SignatureOptionType
 * Documentation : Identifies the delivery signature services options offered by FedEx.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumSignatureOptionType extends MytestWsdlClass
{
	/**
	 * Constant for value 'ADULT'
	 * @return string 'ADULT'
	 */
	const VALUE_ADULT = 'ADULT';
	/**
	 * Constant for value 'DIRECT'
	 * @return string 'DIRECT'
	 */
	const VALUE_DIRECT = 'DIRECT';
	/**
	 * Constant for value 'INDIRECT'
	 * @return string 'INDIRECT'
	 */
	const VALUE_INDIRECT = 'INDIRECT';
	/**
	 * Constant for value 'NO_SIGNATURE_REQUIRED'
	 * @return string 'NO_SIGNATURE_REQUIRED'
	 */
	const VALUE_NO_SIGNATURE_REQUIRED = 'NO_SIGNATURE_REQUIRED';
	/**
	 * Constant for value 'SERVICE_DEFAULT'
	 * @return string 'SERVICE_DEFAULT'
	 */
	const VALUE_SERVICE_DEFAULT = 'SERVICE_DEFAULT';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumSignatureOptionType::VALUE_ADULT
	 * @uses MytestEnumSignatureOptionType::VALUE_DIRECT
	 * @uses MytestEnumSignatureOptionType::VALUE_INDIRECT
	 * @uses MytestEnumSignatureOptionType::VALUE_NO_SIGNATURE_REQUIRED
	 * @uses MytestEnumSignatureOptionType::VALUE_SERVICE_DEFAULT
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumSignatureOptionType::VALUE_ADULT,MytestEnumSignatureOptionType::VALUE_DIRECT,MytestEnumSignatureOptionType::VALUE_INDIRECT,MytestEnumSignatureOptionType::VALUE_NO_SIGNATURE_REQUIRED,MytestEnumSignatureOptionType::VALUE_SERVICE_DEFAULT));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>