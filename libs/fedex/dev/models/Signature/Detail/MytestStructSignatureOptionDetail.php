<?php
/**
 * File for class MytestStructSignatureOptionDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructSignatureOptionDetail originally named SignatureOptionDetail
 * Documentation : The descriptive data required for FedEx delivery signature services.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructSignatureOptionDetail extends MytestWsdlClass
{
	/**
	 * The OptionType
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the delivery signature services option selected by the customer for this shipment. See OptionType for the list of valid values.
	 * - minOccurs : 1
	 * @var MytestEnumSignatureOptionType
	 */
	public $OptionType;
	/**
	 * The SignatureReleaseNumber
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $SignatureReleaseNumber;
	/**
	 * Constructor method for SignatureOptionDetail
	 * @see parent::__construct()
	 * @param MytestEnumSignatureOptionType $_optionType
	 * @param string $_signatureReleaseNumber
	 * @return MytestStructSignatureOptionDetail
	 */
	public function __construct($_optionType,$_signatureReleaseNumber = NULL)
	{
		parent::__construct(array('OptionType'=>$_optionType,'SignatureReleaseNumber'=>$_signatureReleaseNumber));
	}
	/**
	 * Get OptionType value
	 * @return MytestEnumSignatureOptionType
	 */
	public function getOptionType()
	{
		return $this->OptionType;
	}
	/**
	 * Set OptionType value
	 * @uses MytestEnumSignatureOptionType::valueIsValid()
	 * @param MytestEnumSignatureOptionType $_optionType the OptionType
	 * @return MytestEnumSignatureOptionType
	 */
	public function setOptionType($_optionType)
	{
		if(!MytestEnumSignatureOptionType::valueIsValid($_optionType))
		{
			return false;
		}
		return ($this->OptionType = $_optionType);
	}
	/**
	 * Get SignatureReleaseNumber value
	 * @return string|null
	 */
	public function getSignatureReleaseNumber()
	{
		return $this->SignatureReleaseNumber;
	}
	/**
	 * Set SignatureReleaseNumber value
	 * @param string $_signatureReleaseNumber the SignatureReleaseNumber
	 * @return string
	 */
	public function setSignatureReleaseNumber($_signatureReleaseNumber)
	{
		return ($this->SignatureReleaseNumber = $_signatureReleaseNumber);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructSignatureOptionDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>