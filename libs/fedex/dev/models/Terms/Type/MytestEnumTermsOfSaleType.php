<?php
/**
 * File for class MytestEnumTermsOfSaleType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumTermsOfSaleType originally named TermsOfSaleType
 * Documentation : Required for dutiable international express or ground shipment. This field is not applicable to an international PIB (document) or a non-document which does not require a commercial invoice express shipment. CFR_OR_CPT (Cost and Freight/Carriage Paid TO) CIF_OR_CIP (Cost Insurance and Freight/Carraige Insurance Paid) DDP (Delivered Duty Paid) DDU (Delivered Duty Unpaid) EXW (Ex Works) FOB_OR_FCA (Free On Board/Free Carrier)
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumTermsOfSaleType extends MytestWsdlClass
{
	/**
	 * Constant for value 'CFR_OR_CPT'
	 * @return string 'CFR_OR_CPT'
	 */
	const VALUE_CFR_OR_CPT = 'CFR_OR_CPT';
	/**
	 * Constant for value 'CIF_OR_CIP'
	 * @return string 'CIF_OR_CIP'
	 */
	const VALUE_CIF_OR_CIP = 'CIF_OR_CIP';
	/**
	 * Constant for value 'DAP'
	 * @return string 'DAP'
	 */
	const VALUE_DAP = 'DAP';
	/**
	 * Constant for value 'DAT'
	 * @return string 'DAT'
	 */
	const VALUE_DAT = 'DAT';
	/**
	 * Constant for value 'DDP'
	 * @return string 'DDP'
	 */
	const VALUE_DDP = 'DDP';
	/**
	 * Constant for value 'DDU'
	 * @return string 'DDU'
	 */
	const VALUE_DDU = 'DDU';
	/**
	 * Constant for value 'EXW'
	 * @return string 'EXW'
	 */
	const VALUE_EXW = 'EXW';
	/**
	 * Constant for value 'FOB_OR_FCA'
	 * @return string 'FOB_OR_FCA'
	 */
	const VALUE_FOB_OR_FCA = 'FOB_OR_FCA';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumTermsOfSaleType::VALUE_CFR_OR_CPT
	 * @uses MytestEnumTermsOfSaleType::VALUE_CIF_OR_CIP
	 * @uses MytestEnumTermsOfSaleType::VALUE_DAP
	 * @uses MytestEnumTermsOfSaleType::VALUE_DAT
	 * @uses MytestEnumTermsOfSaleType::VALUE_DDP
	 * @uses MytestEnumTermsOfSaleType::VALUE_DDU
	 * @uses MytestEnumTermsOfSaleType::VALUE_EXW
	 * @uses MytestEnumTermsOfSaleType::VALUE_FOB_OR_FCA
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumTermsOfSaleType::VALUE_CFR_OR_CPT,MytestEnumTermsOfSaleType::VALUE_CIF_OR_CIP,MytestEnumTermsOfSaleType::VALUE_DAP,MytestEnumTermsOfSaleType::VALUE_DAT,MytestEnumTermsOfSaleType::VALUE_DDP,MytestEnumTermsOfSaleType::VALUE_DDU,MytestEnumTermsOfSaleType::VALUE_EXW,MytestEnumTermsOfSaleType::VALUE_FOB_OR_FCA));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>