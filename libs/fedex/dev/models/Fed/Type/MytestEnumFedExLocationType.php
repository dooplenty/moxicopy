<?php
/**
 * File for class MytestEnumFedExLocationType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumFedExLocationType originally named FedExLocationType
 * Documentation : Identifies a kind of FedEx facility.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumFedExLocationType extends MytestWsdlClass
{
	/**
	 * Constant for value 'FEDEX_EXPRESS_STATION'
	 * @return string 'FEDEX_EXPRESS_STATION'
	 */
	const VALUE_FEDEX_EXPRESS_STATION = 'FEDEX_EXPRESS_STATION';
	/**
	 * Constant for value 'FEDEX_FREIGHT_SERVICE_CENTER'
	 * @return string 'FEDEX_FREIGHT_SERVICE_CENTER'
	 */
	const VALUE_FEDEX_FREIGHT_SERVICE_CENTER = 'FEDEX_FREIGHT_SERVICE_CENTER';
	/**
	 * Constant for value 'FEDEX_GROUND_TERMINAL'
	 * @return string 'FEDEX_GROUND_TERMINAL'
	 */
	const VALUE_FEDEX_GROUND_TERMINAL = 'FEDEX_GROUND_TERMINAL';
	/**
	 * Constant for value 'FEDEX_HOME_DELIVERY_STATION'
	 * @return string 'FEDEX_HOME_DELIVERY_STATION'
	 */
	const VALUE_FEDEX_HOME_DELIVERY_STATION = 'FEDEX_HOME_DELIVERY_STATION';
	/**
	 * Constant for value 'FEDEX_OFFICE'
	 * @return string 'FEDEX_OFFICE'
	 */
	const VALUE_FEDEX_OFFICE = 'FEDEX_OFFICE';
	/**
	 * Constant for value 'FEDEX_SMART_POST_HUB'
	 * @return string 'FEDEX_SMART_POST_HUB'
	 */
	const VALUE_FEDEX_SMART_POST_HUB = 'FEDEX_SMART_POST_HUB';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumFedExLocationType::VALUE_FEDEX_EXPRESS_STATION
	 * @uses MytestEnumFedExLocationType::VALUE_FEDEX_FREIGHT_SERVICE_CENTER
	 * @uses MytestEnumFedExLocationType::VALUE_FEDEX_GROUND_TERMINAL
	 * @uses MytestEnumFedExLocationType::VALUE_FEDEX_HOME_DELIVERY_STATION
	 * @uses MytestEnumFedExLocationType::VALUE_FEDEX_OFFICE
	 * @uses MytestEnumFedExLocationType::VALUE_FEDEX_SMART_POST_HUB
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumFedExLocationType::VALUE_FEDEX_EXPRESS_STATION,MytestEnumFedExLocationType::VALUE_FEDEX_FREIGHT_SERVICE_CENTER,MytestEnumFedExLocationType::VALUE_FEDEX_GROUND_TERMINAL,MytestEnumFedExLocationType::VALUE_FEDEX_HOME_DELIVERY_STATION,MytestEnumFedExLocationType::VALUE_FEDEX_OFFICE,MytestEnumFedExLocationType::VALUE_FEDEX_SMART_POST_HUB));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>