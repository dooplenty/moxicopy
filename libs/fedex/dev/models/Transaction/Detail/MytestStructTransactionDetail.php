<?php
/**
 * File for class MytestStructTransactionDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructTransactionDetail originally named TransactionDetail
 * Documentation : Descriptive data for this customer transaction. The TransactionDetail from the request is echoed back to the caller in the corresponding reply.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructTransactionDetail extends MytestWsdlClass
{
	/**
	 * The CustomerTransactionId
	 * Meta informations extracted from the WSDL
	 * - documentation : Free form text to be echoed back in the reply. Used to match requests and replies.
	 * - minOccurs : 0
	 * @var string
	 */
	public $CustomerTransactionId;
	/**
	 * The Localization
	 * Meta informations extracted from the WSDL
	 * - documentation : Governs data payload language/translations (contrasted with ClientDetail.localization, which governs Notification.localizedMessage language selection).
	 * - minOccurs : 0
	 * @var MytestStructLocalization
	 */
	public $Localization;
	/**
	 * Constructor method for TransactionDetail
	 * @see parent::__construct()
	 * @param string $_customerTransactionId
	 * @param MytestStructLocalization $_localization
	 * @return MytestStructTransactionDetail
	 */
	public function __construct($_customerTransactionId = NULL,$_localization = NULL)
	{
		parent::__construct(array('CustomerTransactionId'=>$_customerTransactionId,'Localization'=>$_localization));
	}
	/**
	 * Get CustomerTransactionId value
	 * @return string|null
	 */
	public function getCustomerTransactionId()
	{
		return $this->CustomerTransactionId;
	}
	/**
	 * Set CustomerTransactionId value
	 * @param string $_customerTransactionId the CustomerTransactionId
	 * @return string
	 */
	public function setCustomerTransactionId($_customerTransactionId)
	{
		return ($this->CustomerTransactionId = $_customerTransactionId);
	}
	/**
	 * Get Localization value
	 * @return MytestStructLocalization|null
	 */
	public function getLocalization()
	{
		return $this->Localization;
	}
	/**
	 * Set Localization value
	 * @param MytestStructLocalization $_localization the Localization
	 * @return MytestStructLocalization
	 */
	public function setLocalization($_localization)
	{
		return ($this->Localization = $_localization);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructTransactionDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>