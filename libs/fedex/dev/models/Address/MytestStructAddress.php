<?php
/**
 * File for class MytestStructAddress
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructAddress originally named Address
 * Documentation : Descriptive data for a physical location. May be used as an actual physical address (place to which one could go), or as a container of "address parts" which should be handled as a unit (such as a city-state-ZIP combination within the US).
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructAddress extends MytestWsdlClass
{
	/**
	 * The CountryCode
	 * Meta informations extracted from the WSDL
	 * - documentation : The two-letter code used to identify a country.
	 * - minOccurs : 1
	 * @var string
	 */
	public $CountryCode;
	/**
	 * The StreetLines
	 * Meta informations extracted from the WSDL
	 * - documentation : Combination of number, street name, etc. At least one line is required for a valid physical address; empty lines should not be included.
	 * - maxOccurs : 2
	 * - minOccurs : 0
	 * @var string
	 */
	public $StreetLines;
	/**
	 * The City
	 * Meta informations extracted from the WSDL
	 * - documentation : Name of city, town, etc.
	 * - minOccurs : 0
	 * @var string
	 */
	public $City;
	/**
	 * The StateOrProvinceCode
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifying abbreviation for US state, Canada province, etc. Format and presence of this field will vary, depending on country.
	 * - minOccurs : 0
	 * @var string
	 */
	public $StateOrProvinceCode;
	/**
	 * The PostalCode
	 * Meta informations extracted from the WSDL
	 * - documentation : Identification of a region (usually small) for mail/package delivery. Format and presence of this field will vary, depending on country.
	 * - minOccurs : 0
	 * @var string
	 */
	public $PostalCode;
	/**
	 * The UrbanizationCode
	 * Meta informations extracted from the WSDL
	 * - documentation : Relevant only to addresses in Puerto Rico.
	 * - minOccurs : 0
	 * @var string
	 */
	public $UrbanizationCode;
	/**
	 * The CountryName
	 * Meta informations extracted from the WSDL
	 * - documentation : The fully spelt out name of a country.
	 * - minOccurs : 0
	 * @var string
	 */
	public $CountryName;
	/**
	 * The Residential
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates whether this address residential (as opposed to commercial).
	 * - minOccurs : 0
	 * @var boolean
	 */
	public $Residential;
	/**
	 * Constructor method for Address
	 * @see parent::__construct()
	 * @param string $_countryCode
	 * @param string $_streetLines
	 * @param string $_city
	 * @param string $_stateOrProvinceCode
	 * @param string $_postalCode
	 * @param string $_urbanizationCode
	 * @param string $_countryName
	 * @param boolean $_residential
	 * @return MytestStructAddress
	 */
	public function __construct($_countryCode,$_streetLines = NULL,$_city = NULL,$_stateOrProvinceCode = NULL,$_postalCode = NULL,$_urbanizationCode = NULL,$_countryName = NULL,$_residential = NULL)
	{
		parent::__construct(array('CountryCode'=>$_countryCode,'StreetLines'=>$_streetLines,'City'=>$_city,'StateOrProvinceCode'=>$_stateOrProvinceCode,'PostalCode'=>$_postalCode,'UrbanizationCode'=>$_urbanizationCode,'CountryName'=>$_countryName,'Residential'=>$_residential));
	}
	/**
	 * Get CountryCode value
	 * @return string
	 */
	public function getCountryCode()
	{
		return $this->CountryCode;
	}
	/**
	 * Set CountryCode value
	 * @param string $_countryCode the CountryCode
	 * @return string
	 */
	public function setCountryCode($_countryCode)
	{
		return ($this->CountryCode = $_countryCode);
	}
	/**
	 * Get StreetLines value
	 * @return string|null
	 */
	public function getStreetLines()
	{
		return $this->StreetLines;
	}
	/**
	 * Set StreetLines value
	 * @param string $_streetLines the StreetLines
	 * @return string
	 */
	public function setStreetLines($_streetLines)
	{
		return ($this->StreetLines = $_streetLines);
	}
	/**
	 * Get City value
	 * @return string|null
	 */
	public function getCity()
	{
		return $this->City;
	}
	/**
	 * Set City value
	 * @param string $_city the City
	 * @return string
	 */
	public function setCity($_city)
	{
		return ($this->City = $_city);
	}
	/**
	 * Get StateOrProvinceCode value
	 * @return string|null
	 */
	public function getStateOrProvinceCode()
	{
		return $this->StateOrProvinceCode;
	}
	/**
	 * Set StateOrProvinceCode value
	 * @param string $_stateOrProvinceCode the StateOrProvinceCode
	 * @return string
	 */
	public function setStateOrProvinceCode($_stateOrProvinceCode)
	{
		return ($this->StateOrProvinceCode = $_stateOrProvinceCode);
	}
	/**
	 * Get PostalCode value
	 * @return string|null
	 */
	public function getPostalCode()
	{
		return $this->PostalCode;
	}
	/**
	 * Set PostalCode value
	 * @param string $_postalCode the PostalCode
	 * @return string
	 */
	public function setPostalCode($_postalCode)
	{
		return ($this->PostalCode = $_postalCode);
	}
	/**
	 * Get UrbanizationCode value
	 * @return string|null
	 */
	public function getUrbanizationCode()
	{
		return $this->UrbanizationCode;
	}
	/**
	 * Set UrbanizationCode value
	 * @param string $_urbanizationCode the UrbanizationCode
	 * @return string
	 */
	public function setUrbanizationCode($_urbanizationCode)
	{
		return ($this->UrbanizationCode = $_urbanizationCode);
	}
	/**
	 * Get CountryName value
	 * @return string|null
	 */
	public function getCountryName()
	{
		return $this->CountryName;
	}
	/**
	 * Set CountryName value
	 * @param string $_countryName the CountryName
	 * @return string
	 */
	public function setCountryName($_countryName)
	{
		return ($this->CountryName = $_countryName);
	}
	/**
	 * Get Residential value
	 * @return boolean|null
	 */
	public function getResidential()
	{
		return $this->Residential;
	}
	/**
	 * Set Residential value
	 * @param boolean $_residential the Residential
	 * @return boolean
	 */
	public function setResidential($_residential)
	{
		return ($this->Residential = $_residential);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructAddress
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>