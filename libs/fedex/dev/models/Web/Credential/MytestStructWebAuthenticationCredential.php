<?php
/**
 * File for class MytestStructWebAuthenticationCredential
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructWebAuthenticationCredential originally named WebAuthenticationCredential
 * Documentation : Two part authentication string used for the sender's identity
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructWebAuthenticationCredential extends MytestWsdlClass
{
	/**
	 * The Key
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifying part of authentication credential. This value is provided by FedEx after registration
	 * - minOccurs : 1
	 * @var string
	 */
	public $Key;
	/**
	 * The Password
	 * Meta informations extracted from the WSDL
	 * - documentation : Secret part of authentication key. This value is provided by FedEx after registration.
	 * - minOccurs : 1
	 * @var string
	 */
	public $Password;
	/**
	 * Constructor method for WebAuthenticationCredential
	 * @see parent::__construct()
	 * @param string $_key
	 * @param string $_password
	 * @return MytestStructWebAuthenticationCredential
	 */
	public function __construct($_key,$_password)
	{
		parent::__construct(array('Key'=>$_key,'Password'=>$_password));
	}
	/**
	 * Get Key value
	 * @return string
	 */
	public function getKey()
	{
		return $this->Key;
	}
	/**
	 * Set Key value
	 * @param string $_key the Key
	 * @return string
	 */
	public function setKey($_key)
	{
		return ($this->Key = $_key);
	}
	/**
	 * Get Password value
	 * @return string
	 */
	public function getPassword()
	{
		return $this->Password;
	}
	/**
	 * Set Password value
	 * @param string $_password the Password
	 * @return string
	 */
	public function setPassword($_password)
	{
		return ($this->Password = $_password);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructWebAuthenticationCredential
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>