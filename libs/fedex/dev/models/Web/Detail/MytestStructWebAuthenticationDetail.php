<?php
/**
 * File for class MytestStructWebAuthenticationDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructWebAuthenticationDetail originally named WebAuthenticationDetail
 * Documentation : Used in authentication of the sender's identity.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructWebAuthenticationDetail extends MytestWsdlClass
{
	/**
	 * The UserCredential
	 * Meta informations extracted from the WSDL
	 * - documentation : Credential used to authenticate a specific software application. This value is provided by FedEx after registration.
	 * - minOccurs : 1
	 * @var MytestStructWebAuthenticationCredential
	 */
	public $UserCredential;
	/**
	 * Constructor method for WebAuthenticationDetail
	 * @see parent::__construct()
	 * @param MytestStructWebAuthenticationCredential $_userCredential
	 * @return MytestStructWebAuthenticationDetail
	 */
	public function __construct($_userCredential)
	{
		parent::__construct(array('UserCredential'=>$_userCredential));
	}
	/**
	 * Get UserCredential value
	 * @return MytestStructWebAuthenticationCredential
	 */
	public function getUserCredential()
	{
		return $this->UserCredential;
	}
	/**
	 * Set UserCredential value
	 * @param MytestStructWebAuthenticationCredential $_userCredential the UserCredential
	 * @return MytestStructWebAuthenticationCredential
	 */
	public function setUserCredential($_userCredential)
	{
		return ($this->UserCredential = $_userCredential);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructWebAuthenticationDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>