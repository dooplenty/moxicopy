<?php
/**
 * File for class MytestEnumWeightUnits
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumWeightUnits originally named WeightUnits
 * Documentation : Identifies the unit of measure associated with a weight value. See WeightUnits for the list of valid enumerated values.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumWeightUnits extends MytestWsdlClass
{
	/**
	 * Constant for value 'KG'
	 * @return string 'KG'
	 */
	const VALUE_KG = 'KG';
	/**
	 * Constant for value 'LB'
	 * @return string 'LB'
	 */
	const VALUE_LB = 'LB';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumWeightUnits::VALUE_KG
	 * @uses MytestEnumWeightUnits::VALUE_LB
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumWeightUnits::VALUE_KG,MytestEnumWeightUnits::VALUE_LB));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>