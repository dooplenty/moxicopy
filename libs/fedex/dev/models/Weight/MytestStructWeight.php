<?php
/**
 * File for class MytestStructWeight
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructWeight originally named Weight
 * Documentation : The descriptive data for the heaviness of an object.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructWeight extends MytestWsdlClass
{
	/**
	 * The Units
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the unit of measure associated with a weight value.
	 * - minOccurs : 1
	 * @var MytestEnumWeightUnits
	 */
	public $Units;
	/**
	 * The Value
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the weight value of a package/shipment.
	 * - minOccurs : 1
	 * @var decimal
	 */
	public $Value;
	/**
	 * Constructor method for Weight
	 * @see parent::__construct()
	 * @param MytestEnumWeightUnits $_units
	 * @param decimal $_value
	 * @return MytestStructWeight
	 */
	public function __construct($_units,$_value)
	{
		parent::__construct(array('Units'=>$_units,'Value'=>$_value));
	}
	/**
	 * Get Units value
	 * @return MytestEnumWeightUnits
	 */
	public function getUnits()
	{
		return $this->Units;
	}
	/**
	 * Set Units value
	 * @uses MytestEnumWeightUnits::valueIsValid()
	 * @param MytestEnumWeightUnits $_units the Units
	 * @return MytestEnumWeightUnits
	 */
	public function setUnits($_units)
	{
		if(!MytestEnumWeightUnits::valueIsValid($_units))
		{
			return false;
		}
		return ($this->Units = $_units);
	}
	/**
	 * Get Value value
	 * @return decimal
	 */
	public function getValue()
	{
		return $this->Value;
	}
	/**
	 * Set Value value
	 * @param decimal $_value the Value
	 * @return decimal
	 */
	public function setValue($_value)
	{
		return ($this->Value = $_value);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructWeight
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>