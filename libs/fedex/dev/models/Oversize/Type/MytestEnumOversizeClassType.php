<?php
/**
 * File for class MytestEnumOversizeClassType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumOversizeClassType originally named OversizeClassType
 * Documentation : The Oversize classification for a package.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumOversizeClassType extends MytestWsdlClass
{
	/**
	 * Constant for value 'OVERSIZE_1'
	 * @return string 'OVERSIZE_1'
	 */
	const VALUE_OVERSIZE_1 = 'OVERSIZE_1';
	/**
	 * Constant for value 'OVERSIZE_2'
	 * @return string 'OVERSIZE_2'
	 */
	const VALUE_OVERSIZE_2 = 'OVERSIZE_2';
	/**
	 * Constant for value 'OVERSIZE_3'
	 * @return string 'OVERSIZE_3'
	 */
	const VALUE_OVERSIZE_3 = 'OVERSIZE_3';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumOversizeClassType::VALUE_OVERSIZE_1
	 * @uses MytestEnumOversizeClassType::VALUE_OVERSIZE_2
	 * @uses MytestEnumOversizeClassType::VALUE_OVERSIZE_3
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumOversizeClassType::VALUE_OVERSIZE_1,MytestEnumOversizeClassType::VALUE_OVERSIZE_2,MytestEnumOversizeClassType::VALUE_OVERSIZE_3));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>