<?php
/**
 * File for class MytestEnumImageId
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumImageId originally named ImageId
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumImageId extends MytestWsdlClass
{
	/**
	 * Constant for value 'IMAGE_1'
	 * @return string 'IMAGE_1'
	 */
	const VALUE_IMAGE_1 = 'IMAGE_1';
	/**
	 * Constant for value 'IMAGE_2'
	 * @return string 'IMAGE_2'
	 */
	const VALUE_IMAGE_2 = 'IMAGE_2';
	/**
	 * Constant for value 'IMAGE_3'
	 * @return string 'IMAGE_3'
	 */
	const VALUE_IMAGE_3 = 'IMAGE_3';
	/**
	 * Constant for value 'IMAGE_4'
	 * @return string 'IMAGE_4'
	 */
	const VALUE_IMAGE_4 = 'IMAGE_4';
	/**
	 * Constant for value 'IMAGE_5'
	 * @return string 'IMAGE_5'
	 */
	const VALUE_IMAGE_5 = 'IMAGE_5';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumImageId::VALUE_IMAGE_1
	 * @uses MytestEnumImageId::VALUE_IMAGE_2
	 * @uses MytestEnumImageId::VALUE_IMAGE_3
	 * @uses MytestEnumImageId::VALUE_IMAGE_4
	 * @uses MytestEnumImageId::VALUE_IMAGE_5
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumImageId::VALUE_IMAGE_1,MytestEnumImageId::VALUE_IMAGE_2,MytestEnumImageId::VALUE_IMAGE_3,MytestEnumImageId::VALUE_IMAGE_4,MytestEnumImageId::VALUE_IMAGE_5));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>