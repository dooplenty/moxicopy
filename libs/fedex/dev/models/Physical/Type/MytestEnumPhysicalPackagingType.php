<?php
/**
 * File for class MytestEnumPhysicalPackagingType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumPhysicalPackagingType originally named PhysicalPackagingType
 * Documentation : This enumeration rationalizes the former FedEx Express international "admissibility package" types (based on ANSI X.12) and the FedEx Freight packaging types. The values represented are those common to both carriers.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumPhysicalPackagingType extends MytestWsdlClass
{
	/**
	 * Constant for value 'BAG'
	 * @return string 'BAG'
	 */
	const VALUE_BAG = 'BAG';
	/**
	 * Constant for value 'BARREL'
	 * @return string 'BARREL'
	 */
	const VALUE_BARREL = 'BARREL';
	/**
	 * Constant for value 'BASKET'
	 * @return string 'BASKET'
	 */
	const VALUE_BASKET = 'BASKET';
	/**
	 * Constant for value 'BOX'
	 * @return string 'BOX'
	 */
	const VALUE_BOX = 'BOX';
	/**
	 * Constant for value 'BUCKET'
	 * @return string 'BUCKET'
	 */
	const VALUE_BUCKET = 'BUCKET';
	/**
	 * Constant for value 'BUNDLE'
	 * @return string 'BUNDLE'
	 */
	const VALUE_BUNDLE = 'BUNDLE';
	/**
	 * Constant for value 'CARTON'
	 * @return string 'CARTON'
	 */
	const VALUE_CARTON = 'CARTON';
	/**
	 * Constant for value 'CASE'
	 * @return string 'CASE'
	 */
	const VALUE_CASE = 'CASE';
	/**
	 * Constant for value 'CONTAINER'
	 * @return string 'CONTAINER'
	 */
	const VALUE_CONTAINER = 'CONTAINER';
	/**
	 * Constant for value 'CRATE'
	 * @return string 'CRATE'
	 */
	const VALUE_CRATE = 'CRATE';
	/**
	 * Constant for value 'CYLINDER'
	 * @return string 'CYLINDER'
	 */
	const VALUE_CYLINDER = 'CYLINDER';
	/**
	 * Constant for value 'DRUM'
	 * @return string 'DRUM'
	 */
	const VALUE_DRUM = 'DRUM';
	/**
	 * Constant for value 'ENVELOPE'
	 * @return string 'ENVELOPE'
	 */
	const VALUE_ENVELOPE = 'ENVELOPE';
	/**
	 * Constant for value 'HAMPER'
	 * @return string 'HAMPER'
	 */
	const VALUE_HAMPER = 'HAMPER';
	/**
	 * Constant for value 'OTHER'
	 * @return string 'OTHER'
	 */
	const VALUE_OTHER = 'OTHER';
	/**
	 * Constant for value 'PAIL'
	 * @return string 'PAIL'
	 */
	const VALUE_PAIL = 'PAIL';
	/**
	 * Constant for value 'PALLET'
	 * @return string 'PALLET'
	 */
	const VALUE_PALLET = 'PALLET';
	/**
	 * Constant for value 'PIECE'
	 * @return string 'PIECE'
	 */
	const VALUE_PIECE = 'PIECE';
	/**
	 * Constant for value 'REEL'
	 * @return string 'REEL'
	 */
	const VALUE_REEL = 'REEL';
	/**
	 * Constant for value 'ROLL'
	 * @return string 'ROLL'
	 */
	const VALUE_ROLL = 'ROLL';
	/**
	 * Constant for value 'SKID'
	 * @return string 'SKID'
	 */
	const VALUE_SKID = 'SKID';
	/**
	 * Constant for value 'TANK'
	 * @return string 'TANK'
	 */
	const VALUE_TANK = 'TANK';
	/**
	 * Constant for value 'TUBE'
	 * @return string 'TUBE'
	 */
	const VALUE_TUBE = 'TUBE';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumPhysicalPackagingType::VALUE_BAG
	 * @uses MytestEnumPhysicalPackagingType::VALUE_BARREL
	 * @uses MytestEnumPhysicalPackagingType::VALUE_BASKET
	 * @uses MytestEnumPhysicalPackagingType::VALUE_BOX
	 * @uses MytestEnumPhysicalPackagingType::VALUE_BUCKET
	 * @uses MytestEnumPhysicalPackagingType::VALUE_BUNDLE
	 * @uses MytestEnumPhysicalPackagingType::VALUE_CARTON
	 * @uses MytestEnumPhysicalPackagingType::VALUE_CASE
	 * @uses MytestEnumPhysicalPackagingType::VALUE_CONTAINER
	 * @uses MytestEnumPhysicalPackagingType::VALUE_CRATE
	 * @uses MytestEnumPhysicalPackagingType::VALUE_CYLINDER
	 * @uses MytestEnumPhysicalPackagingType::VALUE_DRUM
	 * @uses MytestEnumPhysicalPackagingType::VALUE_ENVELOPE
	 * @uses MytestEnumPhysicalPackagingType::VALUE_HAMPER
	 * @uses MytestEnumPhysicalPackagingType::VALUE_OTHER
	 * @uses MytestEnumPhysicalPackagingType::VALUE_PAIL
	 * @uses MytestEnumPhysicalPackagingType::VALUE_PALLET
	 * @uses MytestEnumPhysicalPackagingType::VALUE_PIECE
	 * @uses MytestEnumPhysicalPackagingType::VALUE_REEL
	 * @uses MytestEnumPhysicalPackagingType::VALUE_ROLL
	 * @uses MytestEnumPhysicalPackagingType::VALUE_SKID
	 * @uses MytestEnumPhysicalPackagingType::VALUE_TANK
	 * @uses MytestEnumPhysicalPackagingType::VALUE_TUBE
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumPhysicalPackagingType::VALUE_BAG,MytestEnumPhysicalPackagingType::VALUE_BARREL,MytestEnumPhysicalPackagingType::VALUE_BASKET,MytestEnumPhysicalPackagingType::VALUE_BOX,MytestEnumPhysicalPackagingType::VALUE_BUCKET,MytestEnumPhysicalPackagingType::VALUE_BUNDLE,MytestEnumPhysicalPackagingType::VALUE_CARTON,MytestEnumPhysicalPackagingType::VALUE_CASE,MytestEnumPhysicalPackagingType::VALUE_CONTAINER,MytestEnumPhysicalPackagingType::VALUE_CRATE,MytestEnumPhysicalPackagingType::VALUE_CYLINDER,MytestEnumPhysicalPackagingType::VALUE_DRUM,MytestEnumPhysicalPackagingType::VALUE_ENVELOPE,MytestEnumPhysicalPackagingType::VALUE_HAMPER,MytestEnumPhysicalPackagingType::VALUE_OTHER,MytestEnumPhysicalPackagingType::VALUE_PAIL,MytestEnumPhysicalPackagingType::VALUE_PALLET,MytestEnumPhysicalPackagingType::VALUE_PIECE,MytestEnumPhysicalPackagingType::VALUE_REEL,MytestEnumPhysicalPackagingType::VALUE_ROLL,MytestEnumPhysicalPackagingType::VALUE_SKID,MytestEnumPhysicalPackagingType::VALUE_TANK,MytestEnumPhysicalPackagingType::VALUE_TUBE));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>