<?php
/**
 * File for class MytestEnumPhysicalFormType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumPhysicalFormType originally named PhysicalFormType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumPhysicalFormType extends MytestWsdlClass
{
	/**
	 * Constant for value 'GAS'
	 * @return string 'GAS'
	 */
	const VALUE_GAS = 'GAS';
	/**
	 * Constant for value 'LIQUID'
	 * @return string 'LIQUID'
	 */
	const VALUE_LIQUID = 'LIQUID';
	/**
	 * Constant for value 'SOLID'
	 * @return string 'SOLID'
	 */
	const VALUE_SOLID = 'SOLID';
	/**
	 * Constant for value 'SPECIAL'
	 * @return string 'SPECIAL'
	 */
	const VALUE_SPECIAL = 'SPECIAL';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumPhysicalFormType::VALUE_GAS
	 * @uses MytestEnumPhysicalFormType::VALUE_LIQUID
	 * @uses MytestEnumPhysicalFormType::VALUE_SOLID
	 * @uses MytestEnumPhysicalFormType::VALUE_SPECIAL
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumPhysicalFormType::VALUE_GAS,MytestEnumPhysicalFormType::VALUE_LIQUID,MytestEnumPhysicalFormType::VALUE_SOLID,MytestEnumPhysicalFormType::VALUE_SPECIAL));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>