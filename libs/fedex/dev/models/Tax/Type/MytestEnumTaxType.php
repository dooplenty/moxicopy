<?php
/**
 * File for class MytestEnumTaxType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumTaxType originally named TaxType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumTaxType extends MytestWsdlClass
{
	/**
	 * Constant for value 'EXPORT'
	 * @return string 'EXPORT'
	 */
	const VALUE_EXPORT = 'EXPORT';
	/**
	 * Constant for value 'GST'
	 * @return string 'GST'
	 */
	const VALUE_GST = 'GST';
	/**
	 * Constant for value 'HST'
	 * @return string 'HST'
	 */
	const VALUE_HST = 'HST';
	/**
	 * Constant for value 'INTRACOUNTRY'
	 * @return string 'INTRACOUNTRY'
	 */
	const VALUE_INTRACOUNTRY = 'INTRACOUNTRY';
	/**
	 * Constant for value 'OTHER'
	 * @return string 'OTHER'
	 */
	const VALUE_OTHER = 'OTHER';
	/**
	 * Constant for value 'PST'
	 * @return string 'PST'
	 */
	const VALUE_PST = 'PST';
	/**
	 * Constant for value 'VAT'
	 * @return string 'VAT'
	 */
	const VALUE_VAT = 'VAT';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumTaxType::VALUE_EXPORT
	 * @uses MytestEnumTaxType::VALUE_GST
	 * @uses MytestEnumTaxType::VALUE_HST
	 * @uses MytestEnumTaxType::VALUE_INTRACOUNTRY
	 * @uses MytestEnumTaxType::VALUE_OTHER
	 * @uses MytestEnumTaxType::VALUE_PST
	 * @uses MytestEnumTaxType::VALUE_VAT
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumTaxType::VALUE_EXPORT,MytestEnumTaxType::VALUE_GST,MytestEnumTaxType::VALUE_HST,MytestEnumTaxType::VALUE_INTRACOUNTRY,MytestEnumTaxType::VALUE_OTHER,MytestEnumTaxType::VALUE_PST,MytestEnumTaxType::VALUE_VAT));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>