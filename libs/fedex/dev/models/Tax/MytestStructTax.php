<?php
/**
 * File for class MytestStructTax
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructTax originally named Tax
 * Documentation : Identifies each tax applied to the shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructTax extends MytestWsdlClass
{
	/**
	 * The TaxType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumTaxType
	 */
	public $TaxType;
	/**
	 * The Description
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Description;
	/**
	 * The Amount
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $Amount;
	/**
	 * Constructor method for Tax
	 * @see parent::__construct()
	 * @param MytestEnumTaxType $_taxType
	 * @param string $_description
	 * @param MytestStructMoney $_amount
	 * @return MytestStructTax
	 */
	public function __construct($_taxType = NULL,$_description = NULL,$_amount = NULL)
	{
		parent::__construct(array('TaxType'=>$_taxType,'Description'=>$_description,'Amount'=>$_amount));
	}
	/**
	 * Get TaxType value
	 * @return MytestEnumTaxType|null
	 */
	public function getTaxType()
	{
		return $this->TaxType;
	}
	/**
	 * Set TaxType value
	 * @uses MytestEnumTaxType::valueIsValid()
	 * @param MytestEnumTaxType $_taxType the TaxType
	 * @return MytestEnumTaxType
	 */
	public function setTaxType($_taxType)
	{
		if(!MytestEnumTaxType::valueIsValid($_taxType))
		{
			return false;
		}
		return ($this->TaxType = $_taxType);
	}
	/**
	 * Get Description value
	 * @return string|null
	 */
	public function getDescription()
	{
		return $this->Description;
	}
	/**
	 * Set Description value
	 * @param string $_description the Description
	 * @return string
	 */
	public function setDescription($_description)
	{
		return ($this->Description = $_description);
	}
	/**
	 * Get Amount value
	 * @return MytestStructMoney|null
	 */
	public function getAmount()
	{
		return $this->Amount;
	}
	/**
	 * Set Amount value
	 * @param MytestStructMoney $_amount the Amount
	 * @return MytestStructMoney
	 */
	public function setAmount($_amount)
	{
		return ($this->Amount = $_amount);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructTax
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>