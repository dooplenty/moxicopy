<?php
/**
 * File for class MytestStructFlatbedTrailerDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructFlatbedTrailerDetail originally named FlatbedTrailerDetail
 * Documentation : Specifies the optional features/characteristics requested for a Freight shipment utilizing a flatbed trailer.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructFlatbedTrailerDetail extends MytestWsdlClass
{
	/**
	 * The Options
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumFlatbedTrailerOption
	 */
	public $Options;
	/**
	 * Constructor method for FlatbedTrailerDetail
	 * @see parent::__construct()
	 * @param MytestEnumFlatbedTrailerOption $_options
	 * @return MytestStructFlatbedTrailerDetail
	 */
	public function __construct($_options = NULL)
	{
		parent::__construct(array('Options'=>$_options));
	}
	/**
	 * Get Options value
	 * @return MytestEnumFlatbedTrailerOption|null
	 */
	public function getOptions()
	{
		return $this->Options;
	}
	/**
	 * Set Options value
	 * @uses MytestEnumFlatbedTrailerOption::valueIsValid()
	 * @param MytestEnumFlatbedTrailerOption $_options the Options
	 * @return MytestEnumFlatbedTrailerOption
	 */
	public function setOptions($_options)
	{
		if(!MytestEnumFlatbedTrailerOption::valueIsValid($_options))
		{
			return false;
		}
		return ($this->Options = $_options);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructFlatbedTrailerDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>