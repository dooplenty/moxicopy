<?php
/**
 * File for class MytestEnumFlatbedTrailerOption
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumFlatbedTrailerOption originally named FlatbedTrailerOption
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumFlatbedTrailerOption extends MytestWsdlClass
{
	/**
	 * Constant for value 'OVER_DIMENSION'
	 * @return string 'OVER_DIMENSION'
	 */
	const VALUE_OVER_DIMENSION = 'OVER_DIMENSION';
	/**
	 * Constant for value 'TARP'
	 * @return string 'TARP'
	 */
	const VALUE_TARP = 'TARP';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumFlatbedTrailerOption::VALUE_OVER_DIMENSION
	 * @uses MytestEnumFlatbedTrailerOption::VALUE_TARP
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumFlatbedTrailerOption::VALUE_OVER_DIMENSION,MytestEnumFlatbedTrailerOption::VALUE_TARP));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>