<?php
/**
 * File for class MytestEnumB13AFilingOptionType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumB13AFilingOptionType originally named B13AFilingOptionType
 * Documentation : Specifies which filing option is being exercised by the customer. Required for non-document shipments originating in Canada destined for any country other than Canada, the United States, Puerto Rico or the U.S. Virgin Islands.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumB13AFilingOptionType extends MytestWsdlClass
{
	/**
	 * Constant for value 'FEDEX_TO_STAMP'
	 * @return string 'FEDEX_TO_STAMP'
	 */
	const VALUE_FEDEX_TO_STAMP = 'FEDEX_TO_STAMP';
	/**
	 * Constant for value 'FILED_ELECTRONICALLY'
	 * @return string 'FILED_ELECTRONICALLY'
	 */
	const VALUE_FILED_ELECTRONICALLY = 'FILED_ELECTRONICALLY';
	/**
	 * Constant for value 'MANUALLY_ATTACHED'
	 * @return string 'MANUALLY_ATTACHED'
	 */
	const VALUE_MANUALLY_ATTACHED = 'MANUALLY_ATTACHED';
	/**
	 * Constant for value 'NOT_REQUIRED'
	 * @return string 'NOT_REQUIRED'
	 */
	const VALUE_NOT_REQUIRED = 'NOT_REQUIRED';
	/**
	 * Constant for value 'SUMMARY_REPORTING'
	 * @return string 'SUMMARY_REPORTING'
	 */
	const VALUE_SUMMARY_REPORTING = 'SUMMARY_REPORTING';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumB13AFilingOptionType::VALUE_FEDEX_TO_STAMP
	 * @uses MytestEnumB13AFilingOptionType::VALUE_FILED_ELECTRONICALLY
	 * @uses MytestEnumB13AFilingOptionType::VALUE_MANUALLY_ATTACHED
	 * @uses MytestEnumB13AFilingOptionType::VALUE_NOT_REQUIRED
	 * @uses MytestEnumB13AFilingOptionType::VALUE_SUMMARY_REPORTING
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumB13AFilingOptionType::VALUE_FEDEX_TO_STAMP,MytestEnumB13AFilingOptionType::VALUE_FILED_ELECTRONICALLY,MytestEnumB13AFilingOptionType::VALUE_MANUALLY_ATTACHED,MytestEnumB13AFilingOptionType::VALUE_NOT_REQUIRED,MytestEnumB13AFilingOptionType::VALUE_SUMMARY_REPORTING));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>