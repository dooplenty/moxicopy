<?php
/**
 * File for class MytestEnumMinimumChargeType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumMinimumChargeType originally named MinimumChargeType
 * Documentation : Internal FedEx use only.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumMinimumChargeType extends MytestWsdlClass
{
	/**
	 * Constant for value 'CUSTOMER'
	 * @return string 'CUSTOMER'
	 */
	const VALUE_CUSTOMER = 'CUSTOMER';
	/**
	 * Constant for value 'CUSTOMER_FREIGHT_WEIGHT'
	 * @return string 'CUSTOMER_FREIGHT_WEIGHT'
	 */
	const VALUE_CUSTOMER_FREIGHT_WEIGHT = 'CUSTOMER_FREIGHT_WEIGHT';
	/**
	 * Constant for value 'EARNED_DISCOUNT'
	 * @return string 'EARNED_DISCOUNT'
	 */
	const VALUE_EARNED_DISCOUNT = 'EARNED_DISCOUNT';
	/**
	 * Constant for value 'MIXED'
	 * @return string 'MIXED'
	 */
	const VALUE_MIXED = 'MIXED';
	/**
	 * Constant for value 'RATE_SCALE'
	 * @return string 'RATE_SCALE'
	 */
	const VALUE_RATE_SCALE = 'RATE_SCALE';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumMinimumChargeType::VALUE_CUSTOMER
	 * @uses MytestEnumMinimumChargeType::VALUE_CUSTOMER_FREIGHT_WEIGHT
	 * @uses MytestEnumMinimumChargeType::VALUE_EARNED_DISCOUNT
	 * @uses MytestEnumMinimumChargeType::VALUE_MIXED
	 * @uses MytestEnumMinimumChargeType::VALUE_RATE_SCALE
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumMinimumChargeType::VALUE_CUSTOMER,MytestEnumMinimumChargeType::VALUE_CUSTOMER_FREIGHT_WEIGHT,MytestEnumMinimumChargeType::VALUE_EARNED_DISCOUNT,MytestEnumMinimumChargeType::VALUE_MIXED,MytestEnumMinimumChargeType::VALUE_RATE_SCALE));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>