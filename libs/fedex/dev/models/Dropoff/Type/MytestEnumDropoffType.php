<?php
/**
 * File for class MytestEnumDropoffType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumDropoffType originally named DropoffType
 * Documentation : Identifies the method by which the package is to be tendered to FedEx. This element does not dispatch a courier for package pickup.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumDropoffType extends MytestWsdlClass
{
	/**
	 * Constant for value 'BUSINESS_SERVICE_CENTER'
	 * @return string 'BUSINESS_SERVICE_CENTER'
	 */
	const VALUE_BUSINESS_SERVICE_CENTER = 'BUSINESS_SERVICE_CENTER';
	/**
	 * Constant for value 'DROP_BOX'
	 * @return string 'DROP_BOX'
	 */
	const VALUE_DROP_BOX = 'DROP_BOX';
	/**
	 * Constant for value 'REGULAR_PICKUP'
	 * @return string 'REGULAR_PICKUP'
	 */
	const VALUE_REGULAR_PICKUP = 'REGULAR_PICKUP';
	/**
	 * Constant for value 'REQUEST_COURIER'
	 * @return string 'REQUEST_COURIER'
	 */
	const VALUE_REQUEST_COURIER = 'REQUEST_COURIER';
	/**
	 * Constant for value 'STATION'
	 * @return string 'STATION'
	 */
	const VALUE_STATION = 'STATION';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumDropoffType::VALUE_BUSINESS_SERVICE_CENTER
	 * @uses MytestEnumDropoffType::VALUE_DROP_BOX
	 * @uses MytestEnumDropoffType::VALUE_REGULAR_PICKUP
	 * @uses MytestEnumDropoffType::VALUE_REQUEST_COURIER
	 * @uses MytestEnumDropoffType::VALUE_STATION
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumDropoffType::VALUE_BUSINESS_SERVICE_CENTER,MytestEnumDropoffType::VALUE_DROP_BOX,MytestEnumDropoffType::VALUE_REGULAR_PICKUP,MytestEnumDropoffType::VALUE_REQUEST_COURIER,MytestEnumDropoffType::VALUE_STATION));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>