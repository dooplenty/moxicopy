<?php
/**
 * File for class MytestEnumTinType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumTinType originally named TinType
 * Documentation : Identifies the category of the taxpayer identification number.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumTinType extends MytestWsdlClass
{
	/**
	 * Constant for value 'BUSINESS_NATIONAL'
	 * @return string 'BUSINESS_NATIONAL'
	 */
	const VALUE_BUSINESS_NATIONAL = 'BUSINESS_NATIONAL';
	/**
	 * Constant for value 'BUSINESS_STATE'
	 * @return string 'BUSINESS_STATE'
	 */
	const VALUE_BUSINESS_STATE = 'BUSINESS_STATE';
	/**
	 * Constant for value 'PERSONAL_NATIONAL'
	 * @return string 'PERSONAL_NATIONAL'
	 */
	const VALUE_PERSONAL_NATIONAL = 'PERSONAL_NATIONAL';
	/**
	 * Constant for value 'PERSONAL_STATE'
	 * @return string 'PERSONAL_STATE'
	 */
	const VALUE_PERSONAL_STATE = 'PERSONAL_STATE';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumTinType::VALUE_BUSINESS_NATIONAL
	 * @uses MytestEnumTinType::VALUE_BUSINESS_STATE
	 * @uses MytestEnumTinType::VALUE_PERSONAL_NATIONAL
	 * @uses MytestEnumTinType::VALUE_PERSONAL_STATE
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumTinType::VALUE_BUSINESS_NATIONAL,MytestEnumTinType::VALUE_BUSINESS_STATE,MytestEnumTinType::VALUE_PERSONAL_NATIONAL,MytestEnumTinType::VALUE_PERSONAL_STATE));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>