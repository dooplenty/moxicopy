<?php
/**
 * File for class MytestEnumSpecialRatingAppliedType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumSpecialRatingAppliedType originally named SpecialRatingAppliedType
 * Documentation : Indicates which special rating cases applied to this shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumSpecialRatingAppliedType extends MytestWsdlClass
{
	/**
	 * Constant for value 'FIXED_FUEL_SURCHARGE'
	 * @return string 'FIXED_FUEL_SURCHARGE'
	 */
	const VALUE_FIXED_FUEL_SURCHARGE = 'FIXED_FUEL_SURCHARGE';
	/**
	 * Constant for value 'IMPORT_PRICING'
	 * @return string 'IMPORT_PRICING'
	 */
	const VALUE_IMPORT_PRICING = 'IMPORT_PRICING';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumSpecialRatingAppliedType::VALUE_FIXED_FUEL_SURCHARGE
	 * @uses MytestEnumSpecialRatingAppliedType::VALUE_IMPORT_PRICING
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumSpecialRatingAppliedType::VALUE_FIXED_FUEL_SURCHARGE,MytestEnumSpecialRatingAppliedType::VALUE_IMPORT_PRICING));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>