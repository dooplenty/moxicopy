<?php
/**
 * File for class MytestEnumRecipientCustomsIdType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumRecipientCustomsIdType originally named RecipientCustomsIdType
 * Documentation : Type of Brazilian taxpayer identifier provided in Recipient/TaxPayerIdentification/Number. For shipments bound for Brazil this overrides the value in Recipient/TaxPayerIdentification/TinType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumRecipientCustomsIdType extends MytestWsdlClass
{
	/**
	 * Constant for value 'COMPANY'
	 * @return string 'COMPANY'
	 */
	const VALUE_COMPANY = 'COMPANY';
	/**
	 * Constant for value 'INDIVIDUAL'
	 * @return string 'INDIVIDUAL'
	 */
	const VALUE_INDIVIDUAL = 'INDIVIDUAL';
	/**
	 * Constant for value 'PASSPORT'
	 * @return string 'PASSPORT'
	 */
	const VALUE_PASSPORT = 'PASSPORT';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumRecipientCustomsIdType::VALUE_COMPANY
	 * @uses MytestEnumRecipientCustomsIdType::VALUE_INDIVIDUAL
	 * @uses MytestEnumRecipientCustomsIdType::VALUE_PASSPORT
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumRecipientCustomsIdType::VALUE_COMPANY,MytestEnumRecipientCustomsIdType::VALUE_INDIVIDUAL,MytestEnumRecipientCustomsIdType::VALUE_PASSPORT));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>