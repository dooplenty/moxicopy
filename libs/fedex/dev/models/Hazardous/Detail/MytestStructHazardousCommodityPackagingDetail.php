<?php
/**
 * File for class MytestStructHazardousCommodityPackagingDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructHazardousCommodityPackagingDetail originally named HazardousCommodityPackagingDetail
 * Documentation : Identifies number and type of packaging units for hazardous commodities.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructHazardousCommodityPackagingDetail extends MytestWsdlClass
{
	/**
	 * The Count
	 * Meta informations extracted from the WSDL
	 * - documentation : Number of units of the type below.
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $Count;
	/**
	 * The Units
	 * Meta informations extracted from the WSDL
	 * - documentation : Units in which the hazardous commodity is packaged.
	 * - minOccurs : 0
	 * @var string
	 */
	public $Units;
	/**
	 * Constructor method for HazardousCommodityPackagingDetail
	 * @see parent::__construct()
	 * @param nonNegativeInteger $_count
	 * @param string $_units
	 * @return MytestStructHazardousCommodityPackagingDetail
	 */
	public function __construct($_count = NULL,$_units = NULL)
	{
		parent::__construct(array('Count'=>$_count,'Units'=>$_units));
	}
	/**
	 * Get Count value
	 * @return nonNegativeInteger|null
	 */
	public function getCount()
	{
		return $this->Count;
	}
	/**
	 * Set Count value
	 * @param nonNegativeInteger $_count the Count
	 * @return nonNegativeInteger
	 */
	public function setCount($_count)
	{
		return ($this->Count = $_count);
	}
	/**
	 * Get Units value
	 * @return string|null
	 */
	public function getUnits()
	{
		return $this->Units;
	}
	/**
	 * Set Units value
	 * @param string $_units the Units
	 * @return string
	 */
	public function setUnits($_units)
	{
		return ($this->Units = $_units);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructHazardousCommodityPackagingDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>