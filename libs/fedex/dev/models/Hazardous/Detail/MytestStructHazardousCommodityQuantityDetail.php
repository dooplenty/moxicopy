<?php
/**
 * File for class MytestStructHazardousCommodityQuantityDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructHazardousCommodityQuantityDetail originally named HazardousCommodityQuantityDetail
 * Documentation : Identifies amount and units for quantity of hazardous commodities.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructHazardousCommodityQuantityDetail extends MytestWsdlClass
{
	/**
	 * The Amount
	 * Meta informations extracted from the WSDL
	 * - documentation : Number of units of the type below.
	 * - minOccurs : 0
	 * @var decimal
	 */
	public $Amount;
	/**
	 * The Units
	 * Meta informations extracted from the WSDL
	 * - documentation : Units by which the hazardous commodity is measured. For IATA commodity, the units values are restricted based on regulation type.
	 * - minOccurs : 0
	 * @var string
	 */
	public $Units;
	/**
	 * The QuantityType
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies which measure of quantity is to be validated.
	 * - minOccurs : 0
	 * @var MytestEnumHazardousCommodityQuantityType
	 */
	public $QuantityType;
	/**
	 * Constructor method for HazardousCommodityQuantityDetail
	 * @see parent::__construct()
	 * @param decimal $_amount
	 * @param string $_units
	 * @param MytestEnumHazardousCommodityQuantityType $_quantityType
	 * @return MytestStructHazardousCommodityQuantityDetail
	 */
	public function __construct($_amount = NULL,$_units = NULL,$_quantityType = NULL)
	{
		parent::__construct(array('Amount'=>$_amount,'Units'=>$_units,'QuantityType'=>$_quantityType));
	}
	/**
	 * Get Amount value
	 * @return decimal|null
	 */
	public function getAmount()
	{
		return $this->Amount;
	}
	/**
	 * Set Amount value
	 * @param decimal $_amount the Amount
	 * @return decimal
	 */
	public function setAmount($_amount)
	{
		return ($this->Amount = $_amount);
	}
	/**
	 * Get Units value
	 * @return string|null
	 */
	public function getUnits()
	{
		return $this->Units;
	}
	/**
	 * Set Units value
	 * @param string $_units the Units
	 * @return string
	 */
	public function setUnits($_units)
	{
		return ($this->Units = $_units);
	}
	/**
	 * Get QuantityType value
	 * @return MytestEnumHazardousCommodityQuantityType|null
	 */
	public function getQuantityType()
	{
		return $this->QuantityType;
	}
	/**
	 * Set QuantityType value
	 * @uses MytestEnumHazardousCommodityQuantityType::valueIsValid()
	 * @param MytestEnumHazardousCommodityQuantityType $_quantityType the QuantityType
	 * @return MytestEnumHazardousCommodityQuantityType
	 */
	public function setQuantityType($_quantityType)
	{
		if(!MytestEnumHazardousCommodityQuantityType::valueIsValid($_quantityType))
		{
			return false;
		}
		return ($this->QuantityType = $_quantityType);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructHazardousCommodityQuantityDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>