<?php
/**
 * File for class MytestStructHazardousCommodityPackingDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructHazardousCommodityPackingDetail originally named HazardousCommodityPackingDetail
 * Documentation : Specifies documentation and limits for validation of an individual packing group/category.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructHazardousCommodityPackingDetail extends MytestWsdlClass
{
	/**
	 * The CargoAircraftOnly
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var boolean
	 */
	public $CargoAircraftOnly;
	/**
	 * The PackingInstructions
	 * Meta informations extracted from the WSDL
	 * - documentation : Coded specification for how commodity is to be packed.
	 * - minOccurs : 0
	 * @var string
	 */
	public $PackingInstructions;
	/**
	 * Constructor method for HazardousCommodityPackingDetail
	 * @see parent::__construct()
	 * @param boolean $_cargoAircraftOnly
	 * @param string $_packingInstructions
	 * @return MytestStructHazardousCommodityPackingDetail
	 */
	public function __construct($_cargoAircraftOnly = NULL,$_packingInstructions = NULL)
	{
		parent::__construct(array('CargoAircraftOnly'=>$_cargoAircraftOnly,'PackingInstructions'=>$_packingInstructions));
	}
	/**
	 * Get CargoAircraftOnly value
	 * @return boolean|null
	 */
	public function getCargoAircraftOnly()
	{
		return $this->CargoAircraftOnly;
	}
	/**
	 * Set CargoAircraftOnly value
	 * @param boolean $_cargoAircraftOnly the CargoAircraftOnly
	 * @return boolean
	 */
	public function setCargoAircraftOnly($_cargoAircraftOnly)
	{
		return ($this->CargoAircraftOnly = $_cargoAircraftOnly);
	}
	/**
	 * Get PackingInstructions value
	 * @return string|null
	 */
	public function getPackingInstructions()
	{
		return $this->PackingInstructions;
	}
	/**
	 * Set PackingInstructions value
	 * @param string $_packingInstructions the PackingInstructions
	 * @return string
	 */
	public function setPackingInstructions($_packingInstructions)
	{
		return ($this->PackingInstructions = $_packingInstructions);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructHazardousCommodityPackingDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>