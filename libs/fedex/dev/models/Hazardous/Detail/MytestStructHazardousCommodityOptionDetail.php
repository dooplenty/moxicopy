<?php
/**
 * File for class MytestStructHazardousCommodityOptionDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructHazardousCommodityOptionDetail originally named HazardousCommodityOptionDetail
 * Documentation : Customer-provided specifications for handling individual commodities.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructHazardousCommodityOptionDetail extends MytestWsdlClass
{
	/**
	 * The LabelTextOption
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies how the customer wishes the label text to be handled for this commodity in this package.
	 * - minOccurs : 0
	 * @var MytestEnumHazardousCommodityLabelTextOptionType
	 */
	public $LabelTextOption;
	/**
	 * The CustomerSuppliedLabelText
	 * Meta informations extracted from the WSDL
	 * - documentation : Text used in labeling the commodity under control of the labelTextOption field.
	 * - minOccurs : 0
	 * @var string
	 */
	public $CustomerSuppliedLabelText;
	/**
	 * Constructor method for HazardousCommodityOptionDetail
	 * @see parent::__construct()
	 * @param MytestEnumHazardousCommodityLabelTextOptionType $_labelTextOption
	 * @param string $_customerSuppliedLabelText
	 * @return MytestStructHazardousCommodityOptionDetail
	 */
	public function __construct($_labelTextOption = NULL,$_customerSuppliedLabelText = NULL)
	{
		parent::__construct(array('LabelTextOption'=>$_labelTextOption,'CustomerSuppliedLabelText'=>$_customerSuppliedLabelText));
	}
	/**
	 * Get LabelTextOption value
	 * @return MytestEnumHazardousCommodityLabelTextOptionType|null
	 */
	public function getLabelTextOption()
	{
		return $this->LabelTextOption;
	}
	/**
	 * Set LabelTextOption value
	 * @uses MytestEnumHazardousCommodityLabelTextOptionType::valueIsValid()
	 * @param MytestEnumHazardousCommodityLabelTextOptionType $_labelTextOption the LabelTextOption
	 * @return MytestEnumHazardousCommodityLabelTextOptionType
	 */
	public function setLabelTextOption($_labelTextOption)
	{
		if(!MytestEnumHazardousCommodityLabelTextOptionType::valueIsValid($_labelTextOption))
		{
			return false;
		}
		return ($this->LabelTextOption = $_labelTextOption);
	}
	/**
	 * Get CustomerSuppliedLabelText value
	 * @return string|null
	 */
	public function getCustomerSuppliedLabelText()
	{
		return $this->CustomerSuppliedLabelText;
	}
	/**
	 * Set CustomerSuppliedLabelText value
	 * @param string $_customerSuppliedLabelText the CustomerSuppliedLabelText
	 * @return string
	 */
	public function setCustomerSuppliedLabelText($_customerSuppliedLabelText)
	{
		return ($this->CustomerSuppliedLabelText = $_customerSuppliedLabelText);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructHazardousCommodityOptionDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>