<?php
/**
 * File for class MytestEnumHazardousCommodityPackingGroupType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumHazardousCommodityPackingGroupType originally named HazardousCommodityPackingGroupType
 * Documentation : Identifies DOT packing group for a hazardous commodity.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumHazardousCommodityPackingGroupType extends MytestWsdlClass
{
	/**
	 * Constant for value 'DEFAULT'
	 * @return string 'DEFAULT'
	 */
	const VALUE_DEFAULT = 'DEFAULT';
	/**
	 * Constant for value 'I'
	 * @return string 'I'
	 */
	const VALUE_I = 'I';
	/**
	 * Constant for value 'II'
	 * @return string 'II'
	 */
	const VALUE_II = 'II';
	/**
	 * Constant for value 'III'
	 * @return string 'III'
	 */
	const VALUE_III = 'III';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumHazardousCommodityPackingGroupType::VALUE_DEFAULT
	 * @uses MytestEnumHazardousCommodityPackingGroupType::VALUE_I
	 * @uses MytestEnumHazardousCommodityPackingGroupType::VALUE_II
	 * @uses MytestEnumHazardousCommodityPackingGroupType::VALUE_III
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumHazardousCommodityPackingGroupType::VALUE_DEFAULT,MytestEnumHazardousCommodityPackingGroupType::VALUE_I,MytestEnumHazardousCommodityPackingGroupType::VALUE_II,MytestEnumHazardousCommodityPackingGroupType::VALUE_III));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>