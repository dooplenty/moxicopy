<?php
/**
 * File for class MytestEnumHazardousCommodityOptionType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumHazardousCommodityOptionType originally named HazardousCommodityOptionType
 * Documentation : Indicates which kind of hazardous content (as defined by DOT) is being reported.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumHazardousCommodityOptionType extends MytestWsdlClass
{
	/**
	 * Constant for value 'HAZARDOUS_MATERIALS'
	 * @return string 'HAZARDOUS_MATERIALS'
	 */
	const VALUE_HAZARDOUS_MATERIALS = 'HAZARDOUS_MATERIALS';
	/**
	 * Constant for value 'LITHIUM_BATTERY_EXCEPTION'
	 * @return string 'LITHIUM_BATTERY_EXCEPTION'
	 */
	const VALUE_LITHIUM_BATTERY_EXCEPTION = 'LITHIUM_BATTERY_EXCEPTION';
	/**
	 * Constant for value 'ORM_D'
	 * @return string 'ORM_D'
	 */
	const VALUE_ORM_D = 'ORM_D';
	/**
	 * Constant for value 'REPORTABLE_QUANTITIES'
	 * @return string 'REPORTABLE_QUANTITIES'
	 */
	const VALUE_REPORTABLE_QUANTITIES = 'REPORTABLE_QUANTITIES';
	/**
	 * Constant for value 'SMALL_QUANTITY_EXCEPTION'
	 * @return string 'SMALL_QUANTITY_EXCEPTION'
	 */
	const VALUE_SMALL_QUANTITY_EXCEPTION = 'SMALL_QUANTITY_EXCEPTION';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumHazardousCommodityOptionType::VALUE_HAZARDOUS_MATERIALS
	 * @uses MytestEnumHazardousCommodityOptionType::VALUE_LITHIUM_BATTERY_EXCEPTION
	 * @uses MytestEnumHazardousCommodityOptionType::VALUE_ORM_D
	 * @uses MytestEnumHazardousCommodityOptionType::VALUE_REPORTABLE_QUANTITIES
	 * @uses MytestEnumHazardousCommodityOptionType::VALUE_SMALL_QUANTITY_EXCEPTION
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumHazardousCommodityOptionType::VALUE_HAZARDOUS_MATERIALS,MytestEnumHazardousCommodityOptionType::VALUE_LITHIUM_BATTERY_EXCEPTION,MytestEnumHazardousCommodityOptionType::VALUE_ORM_D,MytestEnumHazardousCommodityOptionType::VALUE_REPORTABLE_QUANTITIES,MytestEnumHazardousCommodityOptionType::VALUE_SMALL_QUANTITY_EXCEPTION));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>