<?php
/**
 * File for class MytestEnumHazardousCommodityLabelTextOptionType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumHazardousCommodityLabelTextOptionType originally named HazardousCommodityLabelTextOptionType
 * Documentation : Specifies how the commodity is to be labeled.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumHazardousCommodityLabelTextOptionType extends MytestWsdlClass
{
	/**
	 * Constant for value 'APPEND'
	 * @return string 'APPEND'
	 */
	const VALUE_APPEND = 'APPEND';
	/**
	 * Constant for value 'OVERRIDE'
	 * @return string 'OVERRIDE'
	 */
	const VALUE_OVERRIDE = 'OVERRIDE';
	/**
	 * Constant for value 'STANDARD'
	 * @return string 'STANDARD'
	 */
	const VALUE_STANDARD = 'STANDARD';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumHazardousCommodityLabelTextOptionType::VALUE_APPEND
	 * @uses MytestEnumHazardousCommodityLabelTextOptionType::VALUE_OVERRIDE
	 * @uses MytestEnumHazardousCommodityLabelTextOptionType::VALUE_STANDARD
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumHazardousCommodityLabelTextOptionType::VALUE_APPEND,MytestEnumHazardousCommodityLabelTextOptionType::VALUE_OVERRIDE,MytestEnumHazardousCommodityLabelTextOptionType::VALUE_STANDARD));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>