<?php
/**
 * File for class MytestEnumHazardousCommodityQuantityType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumHazardousCommodityQuantityType originally named HazardousCommodityQuantityType
 * Documentation : Specifies the measure of quantity to be validated against a prescribed limit.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumHazardousCommodityQuantityType extends MytestWsdlClass
{
	/**
	 * Constant for value 'GROSS'
	 * @return string 'GROSS'
	 */
	const VALUE_GROSS = 'GROSS';
	/**
	 * Constant for value 'NET'
	 * @return string 'NET'
	 */
	const VALUE_NET = 'NET';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumHazardousCommodityQuantityType::VALUE_GROSS
	 * @uses MytestEnumHazardousCommodityQuantityType::VALUE_NET
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumHazardousCommodityQuantityType::VALUE_GROSS,MytestEnumHazardousCommodityQuantityType::VALUE_NET));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>