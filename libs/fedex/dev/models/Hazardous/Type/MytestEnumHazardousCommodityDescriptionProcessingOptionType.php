<?php
/**
 * File for class MytestEnumHazardousCommodityDescriptionProcessingOptionType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumHazardousCommodityDescriptionProcessingOptionType originally named HazardousCommodityDescriptionProcessingOptionType
 * Documentation : Specifies any special processing to be applied to the dangerous goods commodity description validation.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumHazardousCommodityDescriptionProcessingOptionType extends MytestWsdlClass
{
	/**
	 * Constant for value 'INCLUDE_SPECIAL_PROVISIONS'
	 * @return string 'INCLUDE_SPECIAL_PROVISIONS'
	 */
	const VALUE_INCLUDE_SPECIAL_PROVISIONS = 'INCLUDE_SPECIAL_PROVISIONS';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumHazardousCommodityDescriptionProcessingOptionType::VALUE_INCLUDE_SPECIAL_PROVISIONS
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumHazardousCommodityDescriptionProcessingOptionType::VALUE_INCLUDE_SPECIAL_PROVISIONS));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>