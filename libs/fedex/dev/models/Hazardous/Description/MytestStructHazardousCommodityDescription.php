<?php
/**
 * File for class MytestStructHazardousCommodityDescription
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructHazardousCommodityDescription originally named HazardousCommodityDescription
 * Documentation : Identifies and describes an individual hazardous commodity.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructHazardousCommodityDescription extends MytestWsdlClass
{
	/**
	 * The Id
	 * Meta informations extracted from the WSDL
	 * - documentation : Regulatory identifier for a commodity (e.g. "UN ID" value).
	 * - minOccurs : 0
	 * @var string
	 */
	public $Id;
	/**
	 * The SequenceNumber
	 * Meta informations extracted from the WSDL
	 * - documentation : In conjunction with the regulatory identifier, this field uniquely identifies a specific hazardous materials commodity.
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $SequenceNumber;
	/**
	 * The PackingGroup
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumHazardousCommodityPackingGroupType
	 */
	public $PackingGroup;
	/**
	 * The PackingDetails
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructHazardousCommodityPackingDetail
	 */
	public $PackingDetails;
	/**
	 * The ReportableQuantity
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var boolean
	 */
	public $ReportableQuantity;
	/**
	 * The ProperShippingName
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $ProperShippingName;
	/**
	 * The TechnicalName
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $TechnicalName;
	/**
	 * The Percentage
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var decimal
	 */
	public $Percentage;
	/**
	 * The HazardClass
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $HazardClass;
	/**
	 * The SubsidiaryClasses
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var string
	 */
	public $SubsidiaryClasses;
	/**
	 * The LabelText
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $LabelText;
	/**
	 * The ProcessingOptions
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates any special processing options to be applied to the description of the dangerous goods commodity.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumHazardousCommodityDescriptionProcessingOptionType
	 */
	public $ProcessingOptions;
	/**
	 * The Authorization
	 * Meta informations extracted from the WSDL
	 * - documentation : Information related to quantity limitations and operator or state variations as may be applicable to the dangerous goods commodity.
	 * - minOccurs : 0
	 * @var string
	 */
	public $Authorization;
	/**
	 * Constructor method for HazardousCommodityDescription
	 * @see parent::__construct()
	 * @param string $_id
	 * @param nonNegativeInteger $_sequenceNumber
	 * @param MytestEnumHazardousCommodityPackingGroupType $_packingGroup
	 * @param MytestStructHazardousCommodityPackingDetail $_packingDetails
	 * @param boolean $_reportableQuantity
	 * @param string $_properShippingName
	 * @param string $_technicalName
	 * @param decimal $_percentage
	 * @param string $_hazardClass
	 * @param string $_subsidiaryClasses
	 * @param string $_labelText
	 * @param MytestEnumHazardousCommodityDescriptionProcessingOptionType $_processingOptions
	 * @param string $_authorization
	 * @return MytestStructHazardousCommodityDescription
	 */
	public function __construct($_id = NULL,$_sequenceNumber = NULL,$_packingGroup = NULL,$_packingDetails = NULL,$_reportableQuantity = NULL,$_properShippingName = NULL,$_technicalName = NULL,$_percentage = NULL,$_hazardClass = NULL,$_subsidiaryClasses = NULL,$_labelText = NULL,$_processingOptions = NULL,$_authorization = NULL)
	{
		parent::__construct(array('Id'=>$_id,'SequenceNumber'=>$_sequenceNumber,'PackingGroup'=>$_packingGroup,'PackingDetails'=>$_packingDetails,'ReportableQuantity'=>$_reportableQuantity,'ProperShippingName'=>$_properShippingName,'TechnicalName'=>$_technicalName,'Percentage'=>$_percentage,'HazardClass'=>$_hazardClass,'SubsidiaryClasses'=>$_subsidiaryClasses,'LabelText'=>$_labelText,'ProcessingOptions'=>$_processingOptions,'Authorization'=>$_authorization));
	}
	/**
	 * Get Id value
	 * @return string|null
	 */
	public function getId()
	{
		return $this->Id;
	}
	/**
	 * Set Id value
	 * @param string $_id the Id
	 * @return string
	 */
	public function setId($_id)
	{
		return ($this->Id = $_id);
	}
	/**
	 * Get SequenceNumber value
	 * @return nonNegativeInteger|null
	 */
	public function getSequenceNumber()
	{
		return $this->SequenceNumber;
	}
	/**
	 * Set SequenceNumber value
	 * @param nonNegativeInteger $_sequenceNumber the SequenceNumber
	 * @return nonNegativeInteger
	 */
	public function setSequenceNumber($_sequenceNumber)
	{
		return ($this->SequenceNumber = $_sequenceNumber);
	}
	/**
	 * Get PackingGroup value
	 * @return MytestEnumHazardousCommodityPackingGroupType|null
	 */
	public function getPackingGroup()
	{
		return $this->PackingGroup;
	}
	/**
	 * Set PackingGroup value
	 * @uses MytestEnumHazardousCommodityPackingGroupType::valueIsValid()
	 * @param MytestEnumHazardousCommodityPackingGroupType $_packingGroup the PackingGroup
	 * @return MytestEnumHazardousCommodityPackingGroupType
	 */
	public function setPackingGroup($_packingGroup)
	{
		if(!MytestEnumHazardousCommodityPackingGroupType::valueIsValid($_packingGroup))
		{
			return false;
		}
		return ($this->PackingGroup = $_packingGroup);
	}
	/**
	 * Get PackingDetails value
	 * @return MytestStructHazardousCommodityPackingDetail|null
	 */
	public function getPackingDetails()
	{
		return $this->PackingDetails;
	}
	/**
	 * Set PackingDetails value
	 * @param MytestStructHazardousCommodityPackingDetail $_packingDetails the PackingDetails
	 * @return MytestStructHazardousCommodityPackingDetail
	 */
	public function setPackingDetails($_packingDetails)
	{
		return ($this->PackingDetails = $_packingDetails);
	}
	/**
	 * Get ReportableQuantity value
	 * @return boolean|null
	 */
	public function getReportableQuantity()
	{
		return $this->ReportableQuantity;
	}
	/**
	 * Set ReportableQuantity value
	 * @param boolean $_reportableQuantity the ReportableQuantity
	 * @return boolean
	 */
	public function setReportableQuantity($_reportableQuantity)
	{
		return ($this->ReportableQuantity = $_reportableQuantity);
	}
	/**
	 * Get ProperShippingName value
	 * @return string|null
	 */
	public function getProperShippingName()
	{
		return $this->ProperShippingName;
	}
	/**
	 * Set ProperShippingName value
	 * @param string $_properShippingName the ProperShippingName
	 * @return string
	 */
	public function setProperShippingName($_properShippingName)
	{
		return ($this->ProperShippingName = $_properShippingName);
	}
	/**
	 * Get TechnicalName value
	 * @return string|null
	 */
	public function getTechnicalName()
	{
		return $this->TechnicalName;
	}
	/**
	 * Set TechnicalName value
	 * @param string $_technicalName the TechnicalName
	 * @return string
	 */
	public function setTechnicalName($_technicalName)
	{
		return ($this->TechnicalName = $_technicalName);
	}
	/**
	 * Get Percentage value
	 * @return decimal|null
	 */
	public function getPercentage()
	{
		return $this->Percentage;
	}
	/**
	 * Set Percentage value
	 * @param decimal $_percentage the Percentage
	 * @return decimal
	 */
	public function setPercentage($_percentage)
	{
		return ($this->Percentage = $_percentage);
	}
	/**
	 * Get HazardClass value
	 * @return string|null
	 */
	public function getHazardClass()
	{
		return $this->HazardClass;
	}
	/**
	 * Set HazardClass value
	 * @param string $_hazardClass the HazardClass
	 * @return string
	 */
	public function setHazardClass($_hazardClass)
	{
		return ($this->HazardClass = $_hazardClass);
	}
	/**
	 * Get SubsidiaryClasses value
	 * @return string|null
	 */
	public function getSubsidiaryClasses()
	{
		return $this->SubsidiaryClasses;
	}
	/**
	 * Set SubsidiaryClasses value
	 * @param string $_subsidiaryClasses the SubsidiaryClasses
	 * @return string
	 */
	public function setSubsidiaryClasses($_subsidiaryClasses)
	{
		return ($this->SubsidiaryClasses = $_subsidiaryClasses);
	}
	/**
	 * Get LabelText value
	 * @return string|null
	 */
	public function getLabelText()
	{
		return $this->LabelText;
	}
	/**
	 * Set LabelText value
	 * @param string $_labelText the LabelText
	 * @return string
	 */
	public function setLabelText($_labelText)
	{
		return ($this->LabelText = $_labelText);
	}
	/**
	 * Get ProcessingOptions value
	 * @return MytestEnumHazardousCommodityDescriptionProcessingOptionType|null
	 */
	public function getProcessingOptions()
	{
		return $this->ProcessingOptions;
	}
	/**
	 * Set ProcessingOptions value
	 * @uses MytestEnumHazardousCommodityDescriptionProcessingOptionType::valueIsValid()
	 * @param MytestEnumHazardousCommodityDescriptionProcessingOptionType $_processingOptions the ProcessingOptions
	 * @return MytestEnumHazardousCommodityDescriptionProcessingOptionType
	 */
	public function setProcessingOptions($_processingOptions)
	{
		if(!MytestEnumHazardousCommodityDescriptionProcessingOptionType::valueIsValid($_processingOptions))
		{
			return false;
		}
		return ($this->ProcessingOptions = $_processingOptions);
	}
	/**
	 * Get Authorization value
	 * @return string|null
	 */
	public function getAuthorization()
	{
		return $this->Authorization;
	}
	/**
	 * Set Authorization value
	 * @param string $_authorization the Authorization
	 * @return string
	 */
	public function setAuthorization($_authorization)
	{
		return ($this->Authorization = $_authorization);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructHazardousCommodityDescription
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>