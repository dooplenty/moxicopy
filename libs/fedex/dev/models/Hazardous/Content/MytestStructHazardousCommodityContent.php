<?php
/**
 * File for class MytestStructHazardousCommodityContent
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructHazardousCommodityContent originally named HazardousCommodityContent
 * Documentation : Documents the kind and quantity of an individual hazardous commodity in a package.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructHazardousCommodityContent extends MytestWsdlClass
{
	/**
	 * The Description
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies and describes an individual hazardous commodity.
	 * - minOccurs : 0
	 * @var MytestStructHazardousCommodityDescription
	 */
	public $Description;
	/**
	 * The Quantity
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the amount of the commodity in alternate units.
	 * - minOccurs : 0
	 * @var MytestStructHazardousCommodityQuantityDetail
	 */
	public $Quantity;
	/**
	 * The Options
	 * Meta informations extracted from the WSDL
	 * - documentation : Customer-provided specifications for handling individual commodities.
	 * - minOccurs : 0
	 * @var MytestStructHazardousCommodityOptionDetail
	 */
	public $Options;
	/**
	 * The RadionuclideDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the details of any radio active materials within the commodity.
	 * - minOccurs : 0
	 * @var MytestStructRadionuclideDetail
	 */
	public $RadionuclideDetail;
	/**
	 * Constructor method for HazardousCommodityContent
	 * @see parent::__construct()
	 * @param MytestStructHazardousCommodityDescription $_description
	 * @param MytestStructHazardousCommodityQuantityDetail $_quantity
	 * @param MytestStructHazardousCommodityOptionDetail $_options
	 * @param MytestStructRadionuclideDetail $_radionuclideDetail
	 * @return MytestStructHazardousCommodityContent
	 */
	public function __construct($_description = NULL,$_quantity = NULL,$_options = NULL,$_radionuclideDetail = NULL)
	{
		parent::__construct(array('Description'=>$_description,'Quantity'=>$_quantity,'Options'=>$_options,'RadionuclideDetail'=>$_radionuclideDetail));
	}
	/**
	 * Get Description value
	 * @return MytestStructHazardousCommodityDescription|null
	 */
	public function getDescription()
	{
		return $this->Description;
	}
	/**
	 * Set Description value
	 * @param MytestStructHazardousCommodityDescription $_description the Description
	 * @return MytestStructHazardousCommodityDescription
	 */
	public function setDescription($_description)
	{
		return ($this->Description = $_description);
	}
	/**
	 * Get Quantity value
	 * @return MytestStructHazardousCommodityQuantityDetail|null
	 */
	public function getQuantity()
	{
		return $this->Quantity;
	}
	/**
	 * Set Quantity value
	 * @param MytestStructHazardousCommodityQuantityDetail $_quantity the Quantity
	 * @return MytestStructHazardousCommodityQuantityDetail
	 */
	public function setQuantity($_quantity)
	{
		return ($this->Quantity = $_quantity);
	}
	/**
	 * Get Options value
	 * @return MytestStructHazardousCommodityOptionDetail|null
	 */
	public function getOptions()
	{
		return $this->Options;
	}
	/**
	 * Set Options value
	 * @param MytestStructHazardousCommodityOptionDetail $_options the Options
	 * @return MytestStructHazardousCommodityOptionDetail
	 */
	public function setOptions($_options)
	{
		return ($this->Options = $_options);
	}
	/**
	 * Get RadionuclideDetail value
	 * @return MytestStructRadionuclideDetail|null
	 */
	public function getRadionuclideDetail()
	{
		return $this->RadionuclideDetail;
	}
	/**
	 * Set RadionuclideDetail value
	 * @param MytestStructRadionuclideDetail $_radionuclideDetail the RadionuclideDetail
	 * @return MytestStructRadionuclideDetail
	 */
	public function setRadionuclideDetail($_radionuclideDetail)
	{
		return ($this->RadionuclideDetail = $_radionuclideDetail);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructHazardousCommodityContent
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>