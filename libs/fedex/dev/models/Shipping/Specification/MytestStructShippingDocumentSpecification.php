<?php
/**
 * File for class MytestStructShippingDocumentSpecification
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructShippingDocumentSpecification originally named ShippingDocumentSpecification
 * Documentation : Contains all data required for additional (non-label) shipping documents to be produced in conjunction with a specific shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructShippingDocumentSpecification extends MytestWsdlClass
{
	/**
	 * The ShippingDocumentTypes
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates the types of shipping documents requested by the shipper.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumRequestedShippingDocumentType
	 */
	public $ShippingDocumentTypes;
	/**
	 * The CertificateOfOrigin
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructCertificateOfOriginDetail
	 */
	public $CertificateOfOrigin;
	/**
	 * The CommercialInvoiceDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructCommercialInvoiceDetail
	 */
	public $CommercialInvoiceDetail;
	/**
	 * The CustomPackageDocumentDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the production of each package-level custom document (the same specification is used for all packages).
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructCustomDocumentDetail
	 */
	public $CustomPackageDocumentDetail;
	/**
	 * The CustomShipmentDocumentDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the production of a shipment-level custom document.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructCustomDocumentDetail
	 */
	public $CustomShipmentDocumentDetail;
	/**
	 * The GeneralAgencyAgreementDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructGeneralAgencyAgreementDetail
	 */
	public $GeneralAgencyAgreementDetail;
	/**
	 * The NaftaCertificateOfOriginDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructNaftaCertificateOfOriginDetail
	 */
	public $NaftaCertificateOfOriginDetail;
	/**
	 * The Op900Detail
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the production of the OP-900 document for hazardous materials packages.
	 * - minOccurs : 0
	 * @var MytestStructOp900Detail
	 */
	public $Op900Detail;
	/**
	 * The DangerousGoodsShippersDeclarationDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the production of the 1421c document for dangerous goods shipment.
	 * - minOccurs : 0
	 * @var MytestStructDangerousGoodsShippersDeclarationDetail
	 */
	public $DangerousGoodsShippersDeclarationDetail;
	/**
	 * The ReturnInstructionsDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the production of the return instructions document.
	 * - minOccurs : 0
	 * @var MytestStructReturnInstructionsDetail
	 */
	public $ReturnInstructionsDetail;
	/**
	 * Constructor method for ShippingDocumentSpecification
	 * @see parent::__construct()
	 * @param MytestEnumRequestedShippingDocumentType $_shippingDocumentTypes
	 * @param MytestStructCertificateOfOriginDetail $_certificateOfOrigin
	 * @param MytestStructCommercialInvoiceDetail $_commercialInvoiceDetail
	 * @param MytestStructCustomDocumentDetail $_customPackageDocumentDetail
	 * @param MytestStructCustomDocumentDetail $_customShipmentDocumentDetail
	 * @param MytestStructGeneralAgencyAgreementDetail $_generalAgencyAgreementDetail
	 * @param MytestStructNaftaCertificateOfOriginDetail $_naftaCertificateOfOriginDetail
	 * @param MytestStructOp900Detail $_op900Detail
	 * @param MytestStructDangerousGoodsShippersDeclarationDetail $_dangerousGoodsShippersDeclarationDetail
	 * @param MytestStructReturnInstructionsDetail $_returnInstructionsDetail
	 * @return MytestStructShippingDocumentSpecification
	 */
	public function __construct($_shippingDocumentTypes = NULL,$_certificateOfOrigin = NULL,$_commercialInvoiceDetail = NULL,$_customPackageDocumentDetail = NULL,$_customShipmentDocumentDetail = NULL,$_generalAgencyAgreementDetail = NULL,$_naftaCertificateOfOriginDetail = NULL,$_op900Detail = NULL,$_dangerousGoodsShippersDeclarationDetail = NULL,$_returnInstructionsDetail = NULL)
	{
		parent::__construct(array('ShippingDocumentTypes'=>$_shippingDocumentTypes,'CertificateOfOrigin'=>$_certificateOfOrigin,'CommercialInvoiceDetail'=>$_commercialInvoiceDetail,'CustomPackageDocumentDetail'=>$_customPackageDocumentDetail,'CustomShipmentDocumentDetail'=>$_customShipmentDocumentDetail,'GeneralAgencyAgreementDetail'=>$_generalAgencyAgreementDetail,'NaftaCertificateOfOriginDetail'=>$_naftaCertificateOfOriginDetail,'Op900Detail'=>$_op900Detail,'DangerousGoodsShippersDeclarationDetail'=>$_dangerousGoodsShippersDeclarationDetail,'ReturnInstructionsDetail'=>$_returnInstructionsDetail));
	}
	/**
	 * Get ShippingDocumentTypes value
	 * @return MytestEnumRequestedShippingDocumentType|null
	 */
	public function getShippingDocumentTypes()
	{
		return $this->ShippingDocumentTypes;
	}
	/**
	 * Set ShippingDocumentTypes value
	 * @uses MytestEnumRequestedShippingDocumentType::valueIsValid()
	 * @param MytestEnumRequestedShippingDocumentType $_shippingDocumentTypes the ShippingDocumentTypes
	 * @return MytestEnumRequestedShippingDocumentType
	 */
	public function setShippingDocumentTypes($_shippingDocumentTypes)
	{
		if(!MytestEnumRequestedShippingDocumentType::valueIsValid($_shippingDocumentTypes))
		{
			return false;
		}
		return ($this->ShippingDocumentTypes = $_shippingDocumentTypes);
	}
	/**
	 * Get CertificateOfOrigin value
	 * @return MytestStructCertificateOfOriginDetail|null
	 */
	public function getCertificateOfOrigin()
	{
		return $this->CertificateOfOrigin;
	}
	/**
	 * Set CertificateOfOrigin value
	 * @param MytestStructCertificateOfOriginDetail $_certificateOfOrigin the CertificateOfOrigin
	 * @return MytestStructCertificateOfOriginDetail
	 */
	public function setCertificateOfOrigin($_certificateOfOrigin)
	{
		return ($this->CertificateOfOrigin = $_certificateOfOrigin);
	}
	/**
	 * Get CommercialInvoiceDetail value
	 * @return MytestStructCommercialInvoiceDetail|null
	 */
	public function getCommercialInvoiceDetail()
	{
		return $this->CommercialInvoiceDetail;
	}
	/**
	 * Set CommercialInvoiceDetail value
	 * @param MytestStructCommercialInvoiceDetail $_commercialInvoiceDetail the CommercialInvoiceDetail
	 * @return MytestStructCommercialInvoiceDetail
	 */
	public function setCommercialInvoiceDetail($_commercialInvoiceDetail)
	{
		return ($this->CommercialInvoiceDetail = $_commercialInvoiceDetail);
	}
	/**
	 * Get CustomPackageDocumentDetail value
	 * @return MytestStructCustomDocumentDetail|null
	 */
	public function getCustomPackageDocumentDetail()
	{
		return $this->CustomPackageDocumentDetail;
	}
	/**
	 * Set CustomPackageDocumentDetail value
	 * @param MytestStructCustomDocumentDetail $_customPackageDocumentDetail the CustomPackageDocumentDetail
	 * @return MytestStructCustomDocumentDetail
	 */
	public function setCustomPackageDocumentDetail($_customPackageDocumentDetail)
	{
		return ($this->CustomPackageDocumentDetail = $_customPackageDocumentDetail);
	}
	/**
	 * Get CustomShipmentDocumentDetail value
	 * @return MytestStructCustomDocumentDetail|null
	 */
	public function getCustomShipmentDocumentDetail()
	{
		return $this->CustomShipmentDocumentDetail;
	}
	/**
	 * Set CustomShipmentDocumentDetail value
	 * @param MytestStructCustomDocumentDetail $_customShipmentDocumentDetail the CustomShipmentDocumentDetail
	 * @return MytestStructCustomDocumentDetail
	 */
	public function setCustomShipmentDocumentDetail($_customShipmentDocumentDetail)
	{
		return ($this->CustomShipmentDocumentDetail = $_customShipmentDocumentDetail);
	}
	/**
	 * Get GeneralAgencyAgreementDetail value
	 * @return MytestStructGeneralAgencyAgreementDetail|null
	 */
	public function getGeneralAgencyAgreementDetail()
	{
		return $this->GeneralAgencyAgreementDetail;
	}
	/**
	 * Set GeneralAgencyAgreementDetail value
	 * @param MytestStructGeneralAgencyAgreementDetail $_generalAgencyAgreementDetail the GeneralAgencyAgreementDetail
	 * @return MytestStructGeneralAgencyAgreementDetail
	 */
	public function setGeneralAgencyAgreementDetail($_generalAgencyAgreementDetail)
	{
		return ($this->GeneralAgencyAgreementDetail = $_generalAgencyAgreementDetail);
	}
	/**
	 * Get NaftaCertificateOfOriginDetail value
	 * @return MytestStructNaftaCertificateOfOriginDetail|null
	 */
	public function getNaftaCertificateOfOriginDetail()
	{
		return $this->NaftaCertificateOfOriginDetail;
	}
	/**
	 * Set NaftaCertificateOfOriginDetail value
	 * @param MytestStructNaftaCertificateOfOriginDetail $_naftaCertificateOfOriginDetail the NaftaCertificateOfOriginDetail
	 * @return MytestStructNaftaCertificateOfOriginDetail
	 */
	public function setNaftaCertificateOfOriginDetail($_naftaCertificateOfOriginDetail)
	{
		return ($this->NaftaCertificateOfOriginDetail = $_naftaCertificateOfOriginDetail);
	}
	/**
	 * Get Op900Detail value
	 * @return MytestStructOp900Detail|null
	 */
	public function getOp900Detail()
	{
		return $this->Op900Detail;
	}
	/**
	 * Set Op900Detail value
	 * @param MytestStructOp900Detail $_op900Detail the Op900Detail
	 * @return MytestStructOp900Detail
	 */
	public function setOp900Detail($_op900Detail)
	{
		return ($this->Op900Detail = $_op900Detail);
	}
	/**
	 * Get DangerousGoodsShippersDeclarationDetail value
	 * @return MytestStructDangerousGoodsShippersDeclarationDetail|null
	 */
	public function getDangerousGoodsShippersDeclarationDetail()
	{
		return $this->DangerousGoodsShippersDeclarationDetail;
	}
	/**
	 * Set DangerousGoodsShippersDeclarationDetail value
	 * @param MytestStructDangerousGoodsShippersDeclarationDetail $_dangerousGoodsShippersDeclarationDetail the DangerousGoodsShippersDeclarationDetail
	 * @return MytestStructDangerousGoodsShippersDeclarationDetail
	 */
	public function setDangerousGoodsShippersDeclarationDetail($_dangerousGoodsShippersDeclarationDetail)
	{
		return ($this->DangerousGoodsShippersDeclarationDetail = $_dangerousGoodsShippersDeclarationDetail);
	}
	/**
	 * Get ReturnInstructionsDetail value
	 * @return MytestStructReturnInstructionsDetail|null
	 */
	public function getReturnInstructionsDetail()
	{
		return $this->ReturnInstructionsDetail;
	}
	/**
	 * Set ReturnInstructionsDetail value
	 * @param MytestStructReturnInstructionsDetail $_returnInstructionsDetail the ReturnInstructionsDetail
	 * @return MytestStructReturnInstructionsDetail
	 */
	public function setReturnInstructionsDetail($_returnInstructionsDetail)
	{
		return ($this->ReturnInstructionsDetail = $_returnInstructionsDetail);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructShippingDocumentSpecification
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>