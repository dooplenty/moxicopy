<?php
/**
 * File for class MytestStructShippingDocumentEMailRecipient
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructShippingDocumentEMailRecipient originally named ShippingDocumentEMailRecipient
 * Documentation : Specifies an individual recipient of e-mailed shipping document(s).
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructShippingDocumentEMailRecipient extends MytestWsdlClass
{
	/**
	 * The RecipientType
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the relationship of this recipient in the shipment.
	 * - minOccurs : 0
	 * @var MytestEnumEMailNotificationRecipientType
	 */
	public $RecipientType;
	/**
	 * The Address
	 * Meta informations extracted from the WSDL
	 * - documentation : Address to which the document is to be sent.
	 * - minOccurs : 0
	 * @var string
	 */
	public $Address;
	/**
	 * Constructor method for ShippingDocumentEMailRecipient
	 * @see parent::__construct()
	 * @param MytestEnumEMailNotificationRecipientType $_recipientType
	 * @param string $_address
	 * @return MytestStructShippingDocumentEMailRecipient
	 */
	public function __construct($_recipientType = NULL,$_address = NULL)
	{
		parent::__construct(array('RecipientType'=>$_recipientType,'Address'=>$_address));
	}
	/**
	 * Get RecipientType value
	 * @return MytestEnumEMailNotificationRecipientType|null
	 */
	public function getRecipientType()
	{
		return $this->RecipientType;
	}
	/**
	 * Set RecipientType value
	 * @uses MytestEnumEMailNotificationRecipientType::valueIsValid()
	 * @param MytestEnumEMailNotificationRecipientType $_recipientType the RecipientType
	 * @return MytestEnumEMailNotificationRecipientType
	 */
	public function setRecipientType($_recipientType)
	{
		if(!MytestEnumEMailNotificationRecipientType::valueIsValid($_recipientType))
		{
			return false;
		}
		return ($this->RecipientType = $_recipientType);
	}
	/**
	 * Get Address value
	 * @return string|null
	 */
	public function getAddress()
	{
		return $this->Address;
	}
	/**
	 * Set Address value
	 * @param string $_address the Address
	 * @return string
	 */
	public function setAddress($_address)
	{
		return ($this->Address = $_address);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructShippingDocumentEMailRecipient
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>