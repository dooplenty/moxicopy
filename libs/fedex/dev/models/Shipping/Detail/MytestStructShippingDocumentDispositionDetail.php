<?php
/**
 * File for class MytestStructShippingDocumentDispositionDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructShippingDocumentDispositionDetail originally named ShippingDocumentDispositionDetail
 * Documentation : Each occurrence of this class specifies a particular way in which a kind of shipping document is to be produced and provided.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructShippingDocumentDispositionDetail extends MytestWsdlClass
{
	/**
	 * The DispositionType
	 * Meta informations extracted from the WSDL
	 * - documentation : Values in this field specify how to create and return the document.
	 * - minOccurs : 0
	 * @var MytestEnumShippingDocumentDispositionType
	 */
	public $DispositionType;
	/**
	 * The Grouping
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies how to organize all documents of this type.
	 * - minOccurs : 0
	 * @var MytestEnumShippingDocumentGroupingType
	 */
	public $Grouping;
	/**
	 * The EMailDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies how to e-mail document images.
	 * - minOccurs : 0
	 * @var MytestStructShippingDocumentEMailDetail
	 */
	public $EMailDetail;
	/**
	 * The PrintDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies how a queued document is to be printed.
	 * - minOccurs : 0
	 * @var MytestStructShippingDocumentPrintDetail
	 */
	public $PrintDetail;
	/**
	 * Constructor method for ShippingDocumentDispositionDetail
	 * @see parent::__construct()
	 * @param MytestEnumShippingDocumentDispositionType $_dispositionType
	 * @param MytestEnumShippingDocumentGroupingType $_grouping
	 * @param MytestStructShippingDocumentEMailDetail $_eMailDetail
	 * @param MytestStructShippingDocumentPrintDetail $_printDetail
	 * @return MytestStructShippingDocumentDispositionDetail
	 */
	public function __construct($_dispositionType = NULL,$_grouping = NULL,$_eMailDetail = NULL,$_printDetail = NULL)
	{
		parent::__construct(array('DispositionType'=>$_dispositionType,'Grouping'=>$_grouping,'EMailDetail'=>$_eMailDetail,'PrintDetail'=>$_printDetail));
	}
	/**
	 * Get DispositionType value
	 * @return MytestEnumShippingDocumentDispositionType|null
	 */
	public function getDispositionType()
	{
		return $this->DispositionType;
	}
	/**
	 * Set DispositionType value
	 * @uses MytestEnumShippingDocumentDispositionType::valueIsValid()
	 * @param MytestEnumShippingDocumentDispositionType $_dispositionType the DispositionType
	 * @return MytestEnumShippingDocumentDispositionType
	 */
	public function setDispositionType($_dispositionType)
	{
		if(!MytestEnumShippingDocumentDispositionType::valueIsValid($_dispositionType))
		{
			return false;
		}
		return ($this->DispositionType = $_dispositionType);
	}
	/**
	 * Get Grouping value
	 * @return MytestEnumShippingDocumentGroupingType|null
	 */
	public function getGrouping()
	{
		return $this->Grouping;
	}
	/**
	 * Set Grouping value
	 * @uses MytestEnumShippingDocumentGroupingType::valueIsValid()
	 * @param MytestEnumShippingDocumentGroupingType $_grouping the Grouping
	 * @return MytestEnumShippingDocumentGroupingType
	 */
	public function setGrouping($_grouping)
	{
		if(!MytestEnumShippingDocumentGroupingType::valueIsValid($_grouping))
		{
			return false;
		}
		return ($this->Grouping = $_grouping);
	}
	/**
	 * Get EMailDetail value
	 * @return MytestStructShippingDocumentEMailDetail|null
	 */
	public function getEMailDetail()
	{
		return $this->EMailDetail;
	}
	/**
	 * Set EMailDetail value
	 * @param MytestStructShippingDocumentEMailDetail $_eMailDetail the EMailDetail
	 * @return MytestStructShippingDocumentEMailDetail
	 */
	public function setEMailDetail($_eMailDetail)
	{
		return ($this->EMailDetail = $_eMailDetail);
	}
	/**
	 * Get PrintDetail value
	 * @return MytestStructShippingDocumentPrintDetail|null
	 */
	public function getPrintDetail()
	{
		return $this->PrintDetail;
	}
	/**
	 * Set PrintDetail value
	 * @param MytestStructShippingDocumentPrintDetail $_printDetail the PrintDetail
	 * @return MytestStructShippingDocumentPrintDetail
	 */
	public function setPrintDetail($_printDetail)
	{
		return ($this->PrintDetail = $_printDetail);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructShippingDocumentDispositionDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>