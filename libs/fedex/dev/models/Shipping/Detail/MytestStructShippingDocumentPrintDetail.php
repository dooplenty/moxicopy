<?php
/**
 * File for class MytestStructShippingDocumentPrintDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructShippingDocumentPrintDetail originally named ShippingDocumentPrintDetail
 * Documentation : Specifies printing options for a shipping document.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructShippingDocumentPrintDetail extends MytestWsdlClass
{
	/**
	 * The PrinterId
	 * Meta informations extracted from the WSDL
	 * - documentation : Provides environment-specific printer identification.
	 * - minOccurs : 0
	 * @var string
	 */
	public $PrinterId;
	/**
	 * Constructor method for ShippingDocumentPrintDetail
	 * @see parent::__construct()
	 * @param string $_printerId
	 * @return MytestStructShippingDocumentPrintDetail
	 */
	public function __construct($_printerId = NULL)
	{
		parent::__construct(array('PrinterId'=>$_printerId));
	}
	/**
	 * Get PrinterId value
	 * @return string|null
	 */
	public function getPrinterId()
	{
		return $this->PrinterId;
	}
	/**
	 * Set PrinterId value
	 * @param string $_printerId the PrinterId
	 * @return string
	 */
	public function setPrinterId($_printerId)
	{
		return ($this->PrinterId = $_printerId);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructShippingDocumentPrintDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>