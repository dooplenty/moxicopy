<?php
/**
 * File for class MytestStructShippingDocumentEMailDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructShippingDocumentEMailDetail originally named ShippingDocumentEMailDetail
 * Documentation : Specifies how to e-mail shipping documents.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructShippingDocumentEMailDetail extends MytestWsdlClass
{
	/**
	 * The EMailRecipients
	 * Meta informations extracted from the WSDL
	 * - documentation : Provides the roles and email addresses for e-mail recipients.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructShippingDocumentEMailRecipient
	 */
	public $EMailRecipients;
	/**
	 * The Grouping
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the convention by which documents are to be grouped as e-mail attachments.
	 * - minOccurs : 0
	 * @var MytestEnumShippingDocumentEMailGroupingType
	 */
	public $Grouping;
	/**
	 * Constructor method for ShippingDocumentEMailDetail
	 * @see parent::__construct()
	 * @param MytestStructShippingDocumentEMailRecipient $_eMailRecipients
	 * @param MytestEnumShippingDocumentEMailGroupingType $_grouping
	 * @return MytestStructShippingDocumentEMailDetail
	 */
	public function __construct($_eMailRecipients = NULL,$_grouping = NULL)
	{
		parent::__construct(array('EMailRecipients'=>$_eMailRecipients,'Grouping'=>$_grouping));
	}
	/**
	 * Get EMailRecipients value
	 * @return MytestStructShippingDocumentEMailRecipient|null
	 */
	public function getEMailRecipients()
	{
		return $this->EMailRecipients;
	}
	/**
	 * Set EMailRecipients value
	 * @param MytestStructShippingDocumentEMailRecipient $_eMailRecipients the EMailRecipients
	 * @return MytestStructShippingDocumentEMailRecipient
	 */
	public function setEMailRecipients($_eMailRecipients)
	{
		return ($this->EMailRecipients = $_eMailRecipients);
	}
	/**
	 * Get Grouping value
	 * @return MytestEnumShippingDocumentEMailGroupingType|null
	 */
	public function getGrouping()
	{
		return $this->Grouping;
	}
	/**
	 * Set Grouping value
	 * @uses MytestEnumShippingDocumentEMailGroupingType::valueIsValid()
	 * @param MytestEnumShippingDocumentEMailGroupingType $_grouping the Grouping
	 * @return MytestEnumShippingDocumentEMailGroupingType
	 */
	public function setGrouping($_grouping)
	{
		if(!MytestEnumShippingDocumentEMailGroupingType::valueIsValid($_grouping))
		{
			return false;
		}
		return ($this->Grouping = $_grouping);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructShippingDocumentEMailDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>