<?php
/**
 * File for class MytestStructShippingDocumentFormat
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructShippingDocumentFormat originally named ShippingDocumentFormat
 * Documentation : Specifies characteristics of a shipping document to be produced.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructShippingDocumentFormat extends MytestWsdlClass
{
	/**
	 * The Dispositions
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies how to create, organize, and return the document.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructShippingDocumentDispositionDetail
	 */
	public $Dispositions;
	/**
	 * The TopOfPageOffset
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies how far down the page to move the beginning of the image; allows for printing on letterhead and other pre-printed stock.
	 * - minOccurs : 0
	 * @var MytestStructLinearMeasure
	 */
	public $TopOfPageOffset;
	/**
	 * The ImageType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumShippingDocumentImageType
	 */
	public $ImageType;
	/**
	 * The StockType
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumShippingDocumentStockType
	 */
	public $StockType;
	/**
	 * The ProvideInstructions
	 * Meta informations extracted from the WSDL
	 * - documentation : For those shipping document types which have both a "form" and "instructions" component (e.g. NAFTA Certificate of Origin and General Agency Agreement), this field indicates whether to provide the instructions.
	 * - minOccurs : 0
	 * @var boolean
	 */
	public $ProvideInstructions;
	/**
	 * The Localization
	 * Meta informations extracted from the WSDL
	 * - documentation : Governs the language to be used for this individual document, independently from other content returned for the same shipment.
	 * - minOccurs : 0
	 * @var MytestStructLocalization
	 */
	public $Localization;
	/**
	 * Constructor method for ShippingDocumentFormat
	 * @see parent::__construct()
	 * @param MytestStructShippingDocumentDispositionDetail $_dispositions
	 * @param MytestStructLinearMeasure $_topOfPageOffset
	 * @param MytestEnumShippingDocumentImageType $_imageType
	 * @param MytestEnumShippingDocumentStockType $_stockType
	 * @param boolean $_provideInstructions
	 * @param MytestStructLocalization $_localization
	 * @return MytestStructShippingDocumentFormat
	 */
	public function __construct($_dispositions = NULL,$_topOfPageOffset = NULL,$_imageType = NULL,$_stockType = NULL,$_provideInstructions = NULL,$_localization = NULL)
	{
		parent::__construct(array('Dispositions'=>$_dispositions,'TopOfPageOffset'=>$_topOfPageOffset,'ImageType'=>$_imageType,'StockType'=>$_stockType,'ProvideInstructions'=>$_provideInstructions,'Localization'=>$_localization));
	}
	/**
	 * Get Dispositions value
	 * @return MytestStructShippingDocumentDispositionDetail|null
	 */
	public function getDispositions()
	{
		return $this->Dispositions;
	}
	/**
	 * Set Dispositions value
	 * @param MytestStructShippingDocumentDispositionDetail $_dispositions the Dispositions
	 * @return MytestStructShippingDocumentDispositionDetail
	 */
	public function setDispositions($_dispositions)
	{
		return ($this->Dispositions = $_dispositions);
	}
	/**
	 * Get TopOfPageOffset value
	 * @return MytestStructLinearMeasure|null
	 */
	public function getTopOfPageOffset()
	{
		return $this->TopOfPageOffset;
	}
	/**
	 * Set TopOfPageOffset value
	 * @param MytestStructLinearMeasure $_topOfPageOffset the TopOfPageOffset
	 * @return MytestStructLinearMeasure
	 */
	public function setTopOfPageOffset($_topOfPageOffset)
	{
		return ($this->TopOfPageOffset = $_topOfPageOffset);
	}
	/**
	 * Get ImageType value
	 * @return MytestEnumShippingDocumentImageType|null
	 */
	public function getImageType()
	{
		return $this->ImageType;
	}
	/**
	 * Set ImageType value
	 * @uses MytestEnumShippingDocumentImageType::valueIsValid()
	 * @param MytestEnumShippingDocumentImageType $_imageType the ImageType
	 * @return MytestEnumShippingDocumentImageType
	 */
	public function setImageType($_imageType)
	{
		if(!MytestEnumShippingDocumentImageType::valueIsValid($_imageType))
		{
			return false;
		}
		return ($this->ImageType = $_imageType);
	}
	/**
	 * Get StockType value
	 * @return MytestEnumShippingDocumentStockType|null
	 */
	public function getStockType()
	{
		return $this->StockType;
	}
	/**
	 * Set StockType value
	 * @uses MytestEnumShippingDocumentStockType::valueIsValid()
	 * @param MytestEnumShippingDocumentStockType $_stockType the StockType
	 * @return MytestEnumShippingDocumentStockType
	 */
	public function setStockType($_stockType)
	{
		if(!MytestEnumShippingDocumentStockType::valueIsValid($_stockType))
		{
			return false;
		}
		return ($this->StockType = $_stockType);
	}
	/**
	 * Get ProvideInstructions value
	 * @return boolean|null
	 */
	public function getProvideInstructions()
	{
		return $this->ProvideInstructions;
	}
	/**
	 * Set ProvideInstructions value
	 * @param boolean $_provideInstructions the ProvideInstructions
	 * @return boolean
	 */
	public function setProvideInstructions($_provideInstructions)
	{
		return ($this->ProvideInstructions = $_provideInstructions);
	}
	/**
	 * Get Localization value
	 * @return MytestStructLocalization|null
	 */
	public function getLocalization()
	{
		return $this->Localization;
	}
	/**
	 * Set Localization value
	 * @param MytestStructLocalization $_localization the Localization
	 * @return MytestStructLocalization
	 */
	public function setLocalization($_localization)
	{
		return ($this->Localization = $_localization);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructShippingDocumentFormat
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>