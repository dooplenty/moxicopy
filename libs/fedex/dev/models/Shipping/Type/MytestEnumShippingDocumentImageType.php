<?php
/**
 * File for class MytestEnumShippingDocumentImageType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumShippingDocumentImageType originally named ShippingDocumentImageType
 * Documentation : Specifies the image format used for a shipping document.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumShippingDocumentImageType extends MytestWsdlClass
{
	/**
	 * Constant for value 'DPL'
	 * @return string 'DPL'
	 */
	const VALUE_DPL = 'DPL';
	/**
	 * Constant for value 'EPL2'
	 * @return string 'EPL2'
	 */
	const VALUE_EPL2 = 'EPL2';
	/**
	 * Constant for value 'PDF'
	 * @return string 'PDF'
	 */
	const VALUE_PDF = 'PDF';
	/**
	 * Constant for value 'PNG'
	 * @return string 'PNG'
	 */
	const VALUE_PNG = 'PNG';
	/**
	 * Constant for value 'ZPLII'
	 * @return string 'ZPLII'
	 */
	const VALUE_ZPLII = 'ZPLII';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumShippingDocumentImageType::VALUE_DPL
	 * @uses MytestEnumShippingDocumentImageType::VALUE_EPL2
	 * @uses MytestEnumShippingDocumentImageType::VALUE_PDF
	 * @uses MytestEnumShippingDocumentImageType::VALUE_PNG
	 * @uses MytestEnumShippingDocumentImageType::VALUE_ZPLII
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumShippingDocumentImageType::VALUE_DPL,MytestEnumShippingDocumentImageType::VALUE_EPL2,MytestEnumShippingDocumentImageType::VALUE_PDF,MytestEnumShippingDocumentImageType::VALUE_PNG,MytestEnumShippingDocumentImageType::VALUE_ZPLII));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>