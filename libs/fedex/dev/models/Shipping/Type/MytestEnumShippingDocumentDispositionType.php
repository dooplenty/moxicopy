<?php
/**
 * File for class MytestEnumShippingDocumentDispositionType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumShippingDocumentDispositionType originally named ShippingDocumentDispositionType
 * Documentation : Specifies how to return a shipping document to the caller.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumShippingDocumentDispositionType extends MytestWsdlClass
{
	/**
	 * Constant for value 'CONFIRMED'
	 * @return string 'CONFIRMED'
	 */
	const VALUE_CONFIRMED = 'CONFIRMED';
	/**
	 * Constant for value 'DEFERRED_RETURNED'
	 * @return string 'DEFERRED_RETURNED'
	 */
	const VALUE_DEFERRED_RETURNED = 'DEFERRED_RETURNED';
	/**
	 * Constant for value 'DEFERRED_STORED'
	 * @return string 'DEFERRED_STORED'
	 */
	const VALUE_DEFERRED_STORED = 'DEFERRED_STORED';
	/**
	 * Constant for value 'EMAILED'
	 * @return string 'EMAILED'
	 */
	const VALUE_EMAILED = 'EMAILED';
	/**
	 * Constant for value 'QUEUED'
	 * @return string 'QUEUED'
	 */
	const VALUE_QUEUED = 'QUEUED';
	/**
	 * Constant for value 'RETURNED'
	 * @return string 'RETURNED'
	 */
	const VALUE_RETURNED = 'RETURNED';
	/**
	 * Constant for value 'STORED'
	 * @return string 'STORED'
	 */
	const VALUE_STORED = 'STORED';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumShippingDocumentDispositionType::VALUE_CONFIRMED
	 * @uses MytestEnumShippingDocumentDispositionType::VALUE_DEFERRED_RETURNED
	 * @uses MytestEnumShippingDocumentDispositionType::VALUE_DEFERRED_STORED
	 * @uses MytestEnumShippingDocumentDispositionType::VALUE_EMAILED
	 * @uses MytestEnumShippingDocumentDispositionType::VALUE_QUEUED
	 * @uses MytestEnumShippingDocumentDispositionType::VALUE_RETURNED
	 * @uses MytestEnumShippingDocumentDispositionType::VALUE_STORED
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumShippingDocumentDispositionType::VALUE_CONFIRMED,MytestEnumShippingDocumentDispositionType::VALUE_DEFERRED_RETURNED,MytestEnumShippingDocumentDispositionType::VALUE_DEFERRED_STORED,MytestEnumShippingDocumentDispositionType::VALUE_EMAILED,MytestEnumShippingDocumentDispositionType::VALUE_QUEUED,MytestEnumShippingDocumentDispositionType::VALUE_RETURNED,MytestEnumShippingDocumentDispositionType::VALUE_STORED));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>