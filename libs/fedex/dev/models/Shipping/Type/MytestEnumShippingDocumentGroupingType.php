<?php
/**
 * File for class MytestEnumShippingDocumentGroupingType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumShippingDocumentGroupingType originally named ShippingDocumentGroupingType
 * Documentation : Specifies how to organize all shipping documents of the same type.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumShippingDocumentGroupingType extends MytestWsdlClass
{
	/**
	 * Constant for value 'CONSOLIDATED_BY_DOCUMENT_TYPE'
	 * @return string 'CONSOLIDATED_BY_DOCUMENT_TYPE'
	 */
	const VALUE_CONSOLIDATED_BY_DOCUMENT_TYPE = 'CONSOLIDATED_BY_DOCUMENT_TYPE';
	/**
	 * Constant for value 'INDIVIDUAL'
	 * @return string 'INDIVIDUAL'
	 */
	const VALUE_INDIVIDUAL = 'INDIVIDUAL';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumShippingDocumentGroupingType::VALUE_CONSOLIDATED_BY_DOCUMENT_TYPE
	 * @uses MytestEnumShippingDocumentGroupingType::VALUE_INDIVIDUAL
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumShippingDocumentGroupingType::VALUE_CONSOLIDATED_BY_DOCUMENT_TYPE,MytestEnumShippingDocumentGroupingType::VALUE_INDIVIDUAL));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>