<?php
/**
 * File for class MytestEnumShippingDocumentStockType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumShippingDocumentStockType originally named ShippingDocumentStockType
 * Documentation : Specifies the type of paper (stock) on which a document will be printed.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumShippingDocumentStockType extends MytestWsdlClass
{
	/**
	 * Constant for value 'OP_900_LG_B'
	 * @return string 'OP_900_LG_B'
	 */
	const VALUE_OP_900_LG_B = 'OP_900_LG_B';
	/**
	 * Constant for value 'OP_900_LL_B'
	 * @return string 'OP_900_LL_B'
	 */
	const VALUE_OP_900_LL_B = 'OP_900_LL_B';
	/**
	 * Constant for value 'PAPER_4X6'
	 * @return string 'PAPER_4X6'
	 */
	const VALUE_PAPER_4X6 = 'PAPER_4X6';
	/**
	 * Constant for value 'PAPER_LETTER'
	 * @return string 'PAPER_LETTER'
	 */
	const VALUE_PAPER_LETTER = 'PAPER_LETTER';
	/**
	 * Constant for value 'STOCK_4X6'
	 * @return string 'STOCK_4X6'
	 */
	const VALUE_STOCK_4X6 = 'STOCK_4X6';
	/**
	 * Constant for value 'STOCK_4X6.75_LEADING_DOC_TAB'
	 * @return string 'STOCK_4X6.75_LEADING_DOC_TAB'
	 */
	const VALUE_STOCK_4X6_75_LEADING_DOC_TAB = 'STOCK_4X6.75_LEADING_DOC_TAB';
	/**
	 * Constant for value 'STOCK_4X6.75_TRAILING_DOC_TAB'
	 * @return string 'STOCK_4X6.75_TRAILING_DOC_TAB'
	 */
	const VALUE_STOCK_4X6_75_TRAILING_DOC_TAB = 'STOCK_4X6.75_TRAILING_DOC_TAB';
	/**
	 * Constant for value 'STOCK_4X8'
	 * @return string 'STOCK_4X8'
	 */
	const VALUE_STOCK_4X8 = 'STOCK_4X8';
	/**
	 * Constant for value 'STOCK_4X9_LEADING_DOC_TAB'
	 * @return string 'STOCK_4X9_LEADING_DOC_TAB'
	 */
	const VALUE_STOCK_4X9_LEADING_DOC_TAB = 'STOCK_4X9_LEADING_DOC_TAB';
	/**
	 * Constant for value 'STOCK_4X9_TRAILING_DOC_TAB'
	 * @return string 'STOCK_4X9_TRAILING_DOC_TAB'
	 */
	const VALUE_STOCK_4X9_TRAILING_DOC_TAB = 'STOCK_4X9_TRAILING_DOC_TAB';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumShippingDocumentStockType::VALUE_OP_900_LG_B
	 * @uses MytestEnumShippingDocumentStockType::VALUE_OP_900_LL_B
	 * @uses MytestEnumShippingDocumentStockType::VALUE_PAPER_4X6
	 * @uses MytestEnumShippingDocumentStockType::VALUE_PAPER_LETTER
	 * @uses MytestEnumShippingDocumentStockType::VALUE_STOCK_4X6
	 * @uses MytestEnumShippingDocumentStockType::VALUE_STOCK_4X6_75_LEADING_DOC_TAB
	 * @uses MytestEnumShippingDocumentStockType::VALUE_STOCK_4X6_75_TRAILING_DOC_TAB
	 * @uses MytestEnumShippingDocumentStockType::VALUE_STOCK_4X8
	 * @uses MytestEnumShippingDocumentStockType::VALUE_STOCK_4X9_LEADING_DOC_TAB
	 * @uses MytestEnumShippingDocumentStockType::VALUE_STOCK_4X9_TRAILING_DOC_TAB
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumShippingDocumentStockType::VALUE_OP_900_LG_B,MytestEnumShippingDocumentStockType::VALUE_OP_900_LL_B,MytestEnumShippingDocumentStockType::VALUE_PAPER_4X6,MytestEnumShippingDocumentStockType::VALUE_PAPER_LETTER,MytestEnumShippingDocumentStockType::VALUE_STOCK_4X6,MytestEnumShippingDocumentStockType::VALUE_STOCK_4X6_75_LEADING_DOC_TAB,MytestEnumShippingDocumentStockType::VALUE_STOCK_4X6_75_TRAILING_DOC_TAB,MytestEnumShippingDocumentStockType::VALUE_STOCK_4X8,MytestEnumShippingDocumentStockType::VALUE_STOCK_4X9_LEADING_DOC_TAB,MytestEnumShippingDocumentStockType::VALUE_STOCK_4X9_TRAILING_DOC_TAB));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>