<?php
/**
 * File for class MytestEnumRequiredShippingDocumentType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumRequiredShippingDocumentType originally named RequiredShippingDocumentType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumRequiredShippingDocumentType extends MytestWsdlClass
{
	/**
	 * Constant for value 'CANADIAN_B13A'
	 * @return string 'CANADIAN_B13A'
	 */
	const VALUE_CANADIAN_B13A = 'CANADIAN_B13A';
	/**
	 * Constant for value 'CERTIFICATE_OF_ORIGIN'
	 * @return string 'CERTIFICATE_OF_ORIGIN'
	 */
	const VALUE_CERTIFICATE_OF_ORIGIN = 'CERTIFICATE_OF_ORIGIN';
	/**
	 * Constant for value 'COMMERCIAL_INVOICE'
	 * @return string 'COMMERCIAL_INVOICE'
	 */
	const VALUE_COMMERCIAL_INVOICE = 'COMMERCIAL_INVOICE';
	/**
	 * Constant for value 'INTERNATIONAL_AIRWAY_BILL'
	 * @return string 'INTERNATIONAL_AIRWAY_BILL'
	 */
	const VALUE_INTERNATIONAL_AIRWAY_BILL = 'INTERNATIONAL_AIRWAY_BILL';
	/**
	 * Constant for value 'MAIL_SERVICE_AIRWAY_BILL'
	 * @return string 'MAIL_SERVICE_AIRWAY_BILL'
	 */
	const VALUE_MAIL_SERVICE_AIRWAY_BILL = 'MAIL_SERVICE_AIRWAY_BILL';
	/**
	 * Constant for value 'SHIPPERS_EXPORT_DECLARATION'
	 * @return string 'SHIPPERS_EXPORT_DECLARATION'
	 */
	const VALUE_SHIPPERS_EXPORT_DECLARATION = 'SHIPPERS_EXPORT_DECLARATION';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumRequiredShippingDocumentType::VALUE_CANADIAN_B13A
	 * @uses MytestEnumRequiredShippingDocumentType::VALUE_CERTIFICATE_OF_ORIGIN
	 * @uses MytestEnumRequiredShippingDocumentType::VALUE_COMMERCIAL_INVOICE
	 * @uses MytestEnumRequiredShippingDocumentType::VALUE_INTERNATIONAL_AIRWAY_BILL
	 * @uses MytestEnumRequiredShippingDocumentType::VALUE_MAIL_SERVICE_AIRWAY_BILL
	 * @uses MytestEnumRequiredShippingDocumentType::VALUE_SHIPPERS_EXPORT_DECLARATION
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumRequiredShippingDocumentType::VALUE_CANADIAN_B13A,MytestEnumRequiredShippingDocumentType::VALUE_CERTIFICATE_OF_ORIGIN,MytestEnumRequiredShippingDocumentType::VALUE_COMMERCIAL_INVOICE,MytestEnumRequiredShippingDocumentType::VALUE_INTERNATIONAL_AIRWAY_BILL,MytestEnumRequiredShippingDocumentType::VALUE_MAIL_SERVICE_AIRWAY_BILL,MytestEnumRequiredShippingDocumentType::VALUE_SHIPPERS_EXPORT_DECLARATION));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>