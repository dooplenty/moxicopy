<?php
/**
 * File for class MytestStructEtdDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructEtdDetail originally named EtdDetail
 * Documentation : Electronic Trade document references used with the ETD special service.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructEtdDetail extends MytestWsdlClass
{
	/**
	 * The RequestedDocumentCopies
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates the types of shipping documents produced for the shipper by FedEx (see ShippingDocumentSpecification) which should be copied back to the shipper in the shipment result data.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumRequestedShippingDocumentType
	 */
	public $RequestedDocumentCopies;
	/**
	 * The Documents
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructUploadDocumentDetail
	 */
	public $Documents;
	/**
	 * The DocumentReferences
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructUploadDocumentReferenceDetail
	 */
	public $DocumentReferences;
	/**
	 * Constructor method for EtdDetail
	 * @see parent::__construct()
	 * @param MytestEnumRequestedShippingDocumentType $_requestedDocumentCopies
	 * @param MytestStructUploadDocumentDetail $_documents
	 * @param MytestStructUploadDocumentReferenceDetail $_documentReferences
	 * @return MytestStructEtdDetail
	 */
	public function __construct($_requestedDocumentCopies = NULL,$_documents = NULL,$_documentReferences = NULL)
	{
		parent::__construct(array('RequestedDocumentCopies'=>$_requestedDocumentCopies,'Documents'=>$_documents,'DocumentReferences'=>$_documentReferences));
	}
	/**
	 * Get RequestedDocumentCopies value
	 * @return MytestEnumRequestedShippingDocumentType|null
	 */
	public function getRequestedDocumentCopies()
	{
		return $this->RequestedDocumentCopies;
	}
	/**
	 * Set RequestedDocumentCopies value
	 * @uses MytestEnumRequestedShippingDocumentType::valueIsValid()
	 * @param MytestEnumRequestedShippingDocumentType $_requestedDocumentCopies the RequestedDocumentCopies
	 * @return MytestEnumRequestedShippingDocumentType
	 */
	public function setRequestedDocumentCopies($_requestedDocumentCopies)
	{
		if(!MytestEnumRequestedShippingDocumentType::valueIsValid($_requestedDocumentCopies))
		{
			return false;
		}
		return ($this->RequestedDocumentCopies = $_requestedDocumentCopies);
	}
	/**
	 * Get Documents value
	 * @return MytestStructUploadDocumentDetail|null
	 */
	public function getDocuments()
	{
		return $this->Documents;
	}
	/**
	 * Set Documents value
	 * @param MytestStructUploadDocumentDetail $_documents the Documents
	 * @return MytestStructUploadDocumentDetail
	 */
	public function setDocuments($_documents)
	{
		return ($this->Documents = $_documents);
	}
	/**
	 * Get DocumentReferences value
	 * @return MytestStructUploadDocumentReferenceDetail|null
	 */
	public function getDocumentReferences()
	{
		return $this->DocumentReferences;
	}
	/**
	 * Set DocumentReferences value
	 * @param MytestStructUploadDocumentReferenceDetail $_documentReferences the DocumentReferences
	 * @return MytestStructUploadDocumentReferenceDetail
	 */
	public function setDocumentReferences($_documentReferences)
	{
		return ($this->DocumentReferences = $_documentReferences);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructEtdDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>