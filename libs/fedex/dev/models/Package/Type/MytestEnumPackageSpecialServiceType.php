<?php
/**
 * File for class MytestEnumPackageSpecialServiceType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumPackageSpecialServiceType originally named PackageSpecialServiceType
 * Documentation : Identifies the collection of special services offered by FedEx.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumPackageSpecialServiceType extends MytestWsdlClass
{
	/**
	 * Constant for value 'ALCOHOL'
	 * @return string 'ALCOHOL'
	 */
	const VALUE_ALCOHOL = 'ALCOHOL';
	/**
	 * Constant for value 'APPOINTMENT_DELIVERY'
	 * @return string 'APPOINTMENT_DELIVERY'
	 */
	const VALUE_APPOINTMENT_DELIVERY = 'APPOINTMENT_DELIVERY';
	/**
	 * Constant for value 'COD'
	 * @return string 'COD'
	 */
	const VALUE_COD = 'COD';
	/**
	 * Constant for value 'DANGEROUS_GOODS'
	 * @return string 'DANGEROUS_GOODS'
	 */
	const VALUE_DANGEROUS_GOODS = 'DANGEROUS_GOODS';
	/**
	 * Constant for value 'DRY_ICE'
	 * @return string 'DRY_ICE'
	 */
	const VALUE_DRY_ICE = 'DRY_ICE';
	/**
	 * Constant for value 'NON_STANDARD_CONTAINER'
	 * @return string 'NON_STANDARD_CONTAINER'
	 */
	const VALUE_NON_STANDARD_CONTAINER = 'NON_STANDARD_CONTAINER';
	/**
	 * Constant for value 'PRIORITY_ALERT'
	 * @return string 'PRIORITY_ALERT'
	 */
	const VALUE_PRIORITY_ALERT = 'PRIORITY_ALERT';
	/**
	 * Constant for value 'SIGNATURE_OPTION'
	 * @return string 'SIGNATURE_OPTION'
	 */
	const VALUE_SIGNATURE_OPTION = 'SIGNATURE_OPTION';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumPackageSpecialServiceType::VALUE_ALCOHOL
	 * @uses MytestEnumPackageSpecialServiceType::VALUE_APPOINTMENT_DELIVERY
	 * @uses MytestEnumPackageSpecialServiceType::VALUE_COD
	 * @uses MytestEnumPackageSpecialServiceType::VALUE_DANGEROUS_GOODS
	 * @uses MytestEnumPackageSpecialServiceType::VALUE_DRY_ICE
	 * @uses MytestEnumPackageSpecialServiceType::VALUE_NON_STANDARD_CONTAINER
	 * @uses MytestEnumPackageSpecialServiceType::VALUE_PRIORITY_ALERT
	 * @uses MytestEnumPackageSpecialServiceType::VALUE_SIGNATURE_OPTION
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumPackageSpecialServiceType::VALUE_ALCOHOL,MytestEnumPackageSpecialServiceType::VALUE_APPOINTMENT_DELIVERY,MytestEnumPackageSpecialServiceType::VALUE_COD,MytestEnumPackageSpecialServiceType::VALUE_DANGEROUS_GOODS,MytestEnumPackageSpecialServiceType::VALUE_DRY_ICE,MytestEnumPackageSpecialServiceType::VALUE_NON_STANDARD_CONTAINER,MytestEnumPackageSpecialServiceType::VALUE_PRIORITY_ALERT,MytestEnumPackageSpecialServiceType::VALUE_SIGNATURE_OPTION));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>