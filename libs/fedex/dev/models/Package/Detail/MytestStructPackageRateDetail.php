<?php
/**
 * File for class MytestStructPackageRateDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructPackageRateDetail originally named PackageRateDetail
 * Documentation : Data for a package's rates, as calculated per a specific rate type.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructPackageRateDetail extends MytestWsdlClass
{
	/**
	 * The RateType
	 * Meta informations extracted from the WSDL
	 * - documentation : Type used for this specific set of rate data.
	 * - minOccurs : 0
	 * @var MytestEnumReturnedRateType
	 */
	public $RateType;
	/**
	 * The RatedWeightMethod
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates which weight was used.
	 * - minOccurs : 0
	 * @var MytestEnumRatedWeightMethod
	 */
	public $RatedWeightMethod;
	/**
	 * The MinimumChargeType
	 * Meta informations extracted from the WSDL
	 * - documentation : INTERNAL FEDEX USE ONLY.
	 * - minOccurs : 0
	 * @var MytestEnumMinimumChargeType
	 */
	public $MinimumChargeType;
	/**
	 * The BillingWeight
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructWeight
	 */
	public $BillingWeight;
	/**
	 * The DimWeight
	 * Meta informations extracted from the WSDL
	 * - documentation : The dimensional weight of this package (if greater than actual).
	 * - minOccurs : 0
	 * @var MytestStructWeight
	 */
	public $DimWeight;
	/**
	 * The OversizeWeight
	 * Meta informations extracted from the WSDL
	 * - documentation : The oversize weight of this package (if the package is oversize).
	 * - minOccurs : 0
	 * @var MytestStructWeight
	 */
	public $OversizeWeight;
	/**
	 * The BaseCharge
	 * Meta informations extracted from the WSDL
	 * - documentation : The transportation charge only (prior to any discounts applied) for this package.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $BaseCharge;
	/**
	 * The TotalFreightDiscounts
	 * Meta informations extracted from the WSDL
	 * - documentation : The sum of all discounts on this package.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TotalFreightDiscounts;
	/**
	 * The NetFreight
	 * Meta informations extracted from the WSDL
	 * - documentation : This package's baseCharge - totalFreightDiscounts.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $NetFreight;
	/**
	 * The TotalSurcharges
	 * Meta informations extracted from the WSDL
	 * - documentation : The sum of all surcharges on this package.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TotalSurcharges;
	/**
	 * The NetFedExCharge
	 * Meta informations extracted from the WSDL
	 * - documentation : This package's netFreight + totalSurcharges (not including totalTaxes).
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $NetFedExCharge;
	/**
	 * The TotalTaxes
	 * Meta informations extracted from the WSDL
	 * - documentation : The sum of all taxes on this package.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TotalTaxes;
	/**
	 * The NetCharge
	 * Meta informations extracted from the WSDL
	 * - documentation : This package's netFreight + totalSurcharges + totalTaxes.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $NetCharge;
	/**
	 * The TotalRebates
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TotalRebates;
	/**
	 * The FreightDiscounts
	 * Meta informations extracted from the WSDL
	 * - documentation : All rate discounts that apply to this package.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructRateDiscount
	 */
	public $FreightDiscounts;
	/**
	 * The Rebates
	 * Meta informations extracted from the WSDL
	 * - documentation : All rebates that apply to this package.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructRebate
	 */
	public $Rebates;
	/**
	 * The Surcharges
	 * Meta informations extracted from the WSDL
	 * - documentation : All surcharges that apply to this package (either because of characteristics of the package itself, or because it is carrying per-shipment surcharges for the shipment of which it is a part).
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructSurcharge
	 */
	public $Surcharges;
	/**
	 * The Taxes
	 * Meta informations extracted from the WSDL
	 * - documentation : All taxes applicable (or distributed to) this package.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructTax
	 */
	public $Taxes;
	/**
	 * The VariableHandlingCharges
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructVariableHandlingCharges
	 */
	public $VariableHandlingCharges;
	/**
	 * Constructor method for PackageRateDetail
	 * @see parent::__construct()
	 * @param MytestEnumReturnedRateType $_rateType
	 * @param MytestEnumRatedWeightMethod $_ratedWeightMethod
	 * @param MytestEnumMinimumChargeType $_minimumChargeType
	 * @param MytestStructWeight $_billingWeight
	 * @param MytestStructWeight $_dimWeight
	 * @param MytestStructWeight $_oversizeWeight
	 * @param MytestStructMoney $_baseCharge
	 * @param MytestStructMoney $_totalFreightDiscounts
	 * @param MytestStructMoney $_netFreight
	 * @param MytestStructMoney $_totalSurcharges
	 * @param MytestStructMoney $_netFedExCharge
	 * @param MytestStructMoney $_totalTaxes
	 * @param MytestStructMoney $_netCharge
	 * @param MytestStructMoney $_totalRebates
	 * @param MytestStructRateDiscount $_freightDiscounts
	 * @param MytestStructRebate $_rebates
	 * @param MytestStructSurcharge $_surcharges
	 * @param MytestStructTax $_taxes
	 * @param MytestStructVariableHandlingCharges $_variableHandlingCharges
	 * @return MytestStructPackageRateDetail
	 */
	public function __construct($_rateType = NULL,$_ratedWeightMethod = NULL,$_minimumChargeType = NULL,$_billingWeight = NULL,$_dimWeight = NULL,$_oversizeWeight = NULL,$_baseCharge = NULL,$_totalFreightDiscounts = NULL,$_netFreight = NULL,$_totalSurcharges = NULL,$_netFedExCharge = NULL,$_totalTaxes = NULL,$_netCharge = NULL,$_totalRebates = NULL,$_freightDiscounts = NULL,$_rebates = NULL,$_surcharges = NULL,$_taxes = NULL,$_variableHandlingCharges = NULL)
	{
		parent::__construct(array('RateType'=>$_rateType,'RatedWeightMethod'=>$_ratedWeightMethod,'MinimumChargeType'=>$_minimumChargeType,'BillingWeight'=>$_billingWeight,'DimWeight'=>$_dimWeight,'OversizeWeight'=>$_oversizeWeight,'BaseCharge'=>$_baseCharge,'TotalFreightDiscounts'=>$_totalFreightDiscounts,'NetFreight'=>$_netFreight,'TotalSurcharges'=>$_totalSurcharges,'NetFedExCharge'=>$_netFedExCharge,'TotalTaxes'=>$_totalTaxes,'NetCharge'=>$_netCharge,'TotalRebates'=>$_totalRebates,'FreightDiscounts'=>$_freightDiscounts,'Rebates'=>$_rebates,'Surcharges'=>$_surcharges,'Taxes'=>$_taxes,'VariableHandlingCharges'=>$_variableHandlingCharges));
	}
	/**
	 * Get RateType value
	 * @return MytestEnumReturnedRateType|null
	 */
	public function getRateType()
	{
		return $this->RateType;
	}
	/**
	 * Set RateType value
	 * @uses MytestEnumReturnedRateType::valueIsValid()
	 * @param MytestEnumReturnedRateType $_rateType the RateType
	 * @return MytestEnumReturnedRateType
	 */
	public function setRateType($_rateType)
	{
		if(!MytestEnumReturnedRateType::valueIsValid($_rateType))
		{
			return false;
		}
		return ($this->RateType = $_rateType);
	}
	/**
	 * Get RatedWeightMethod value
	 * @return MytestEnumRatedWeightMethod|null
	 */
	public function getRatedWeightMethod()
	{
		return $this->RatedWeightMethod;
	}
	/**
	 * Set RatedWeightMethod value
	 * @uses MytestEnumRatedWeightMethod::valueIsValid()
	 * @param MytestEnumRatedWeightMethod $_ratedWeightMethod the RatedWeightMethod
	 * @return MytestEnumRatedWeightMethod
	 */
	public function setRatedWeightMethod($_ratedWeightMethod)
	{
		if(!MytestEnumRatedWeightMethod::valueIsValid($_ratedWeightMethod))
		{
			return false;
		}
		return ($this->RatedWeightMethod = $_ratedWeightMethod);
	}
	/**
	 * Get MinimumChargeType value
	 * @return MytestEnumMinimumChargeType|null
	 */
	public function getMinimumChargeType()
	{
		return $this->MinimumChargeType;
	}
	/**
	 * Set MinimumChargeType value
	 * @uses MytestEnumMinimumChargeType::valueIsValid()
	 * @param MytestEnumMinimumChargeType $_minimumChargeType the MinimumChargeType
	 * @return MytestEnumMinimumChargeType
	 */
	public function setMinimumChargeType($_minimumChargeType)
	{
		if(!MytestEnumMinimumChargeType::valueIsValid($_minimumChargeType))
		{
			return false;
		}
		return ($this->MinimumChargeType = $_minimumChargeType);
	}
	/**
	 * Get BillingWeight value
	 * @return MytestStructWeight|null
	 */
	public function getBillingWeight()
	{
		return $this->BillingWeight;
	}
	/**
	 * Set BillingWeight value
	 * @param MytestStructWeight $_billingWeight the BillingWeight
	 * @return MytestStructWeight
	 */
	public function setBillingWeight($_billingWeight)
	{
		return ($this->BillingWeight = $_billingWeight);
	}
	/**
	 * Get DimWeight value
	 * @return MytestStructWeight|null
	 */
	public function getDimWeight()
	{
		return $this->DimWeight;
	}
	/**
	 * Set DimWeight value
	 * @param MytestStructWeight $_dimWeight the DimWeight
	 * @return MytestStructWeight
	 */
	public function setDimWeight($_dimWeight)
	{
		return ($this->DimWeight = $_dimWeight);
	}
	/**
	 * Get OversizeWeight value
	 * @return MytestStructWeight|null
	 */
	public function getOversizeWeight()
	{
		return $this->OversizeWeight;
	}
	/**
	 * Set OversizeWeight value
	 * @param MytestStructWeight $_oversizeWeight the OversizeWeight
	 * @return MytestStructWeight
	 */
	public function setOversizeWeight($_oversizeWeight)
	{
		return ($this->OversizeWeight = $_oversizeWeight);
	}
	/**
	 * Get BaseCharge value
	 * @return MytestStructMoney|null
	 */
	public function getBaseCharge()
	{
		return $this->BaseCharge;
	}
	/**
	 * Set BaseCharge value
	 * @param MytestStructMoney $_baseCharge the BaseCharge
	 * @return MytestStructMoney
	 */
	public function setBaseCharge($_baseCharge)
	{
		return ($this->BaseCharge = $_baseCharge);
	}
	/**
	 * Get TotalFreightDiscounts value
	 * @return MytestStructMoney|null
	 */
	public function getTotalFreightDiscounts()
	{
		return $this->TotalFreightDiscounts;
	}
	/**
	 * Set TotalFreightDiscounts value
	 * @param MytestStructMoney $_totalFreightDiscounts the TotalFreightDiscounts
	 * @return MytestStructMoney
	 */
	public function setTotalFreightDiscounts($_totalFreightDiscounts)
	{
		return ($this->TotalFreightDiscounts = $_totalFreightDiscounts);
	}
	/**
	 * Get NetFreight value
	 * @return MytestStructMoney|null
	 */
	public function getNetFreight()
	{
		return $this->NetFreight;
	}
	/**
	 * Set NetFreight value
	 * @param MytestStructMoney $_netFreight the NetFreight
	 * @return MytestStructMoney
	 */
	public function setNetFreight($_netFreight)
	{
		return ($this->NetFreight = $_netFreight);
	}
	/**
	 * Get TotalSurcharges value
	 * @return MytestStructMoney|null
	 */
	public function getTotalSurcharges()
	{
		return $this->TotalSurcharges;
	}
	/**
	 * Set TotalSurcharges value
	 * @param MytestStructMoney $_totalSurcharges the TotalSurcharges
	 * @return MytestStructMoney
	 */
	public function setTotalSurcharges($_totalSurcharges)
	{
		return ($this->TotalSurcharges = $_totalSurcharges);
	}
	/**
	 * Get NetFedExCharge value
	 * @return MytestStructMoney|null
	 */
	public function getNetFedExCharge()
	{
		return $this->NetFedExCharge;
	}
	/**
	 * Set NetFedExCharge value
	 * @param MytestStructMoney $_netFedExCharge the NetFedExCharge
	 * @return MytestStructMoney
	 */
	public function setNetFedExCharge($_netFedExCharge)
	{
		return ($this->NetFedExCharge = $_netFedExCharge);
	}
	/**
	 * Get TotalTaxes value
	 * @return MytestStructMoney|null
	 */
	public function getTotalTaxes()
	{
		return $this->TotalTaxes;
	}
	/**
	 * Set TotalTaxes value
	 * @param MytestStructMoney $_totalTaxes the TotalTaxes
	 * @return MytestStructMoney
	 */
	public function setTotalTaxes($_totalTaxes)
	{
		return ($this->TotalTaxes = $_totalTaxes);
	}
	/**
	 * Get NetCharge value
	 * @return MytestStructMoney|null
	 */
	public function getNetCharge()
	{
		return $this->NetCharge;
	}
	/**
	 * Set NetCharge value
	 * @param MytestStructMoney $_netCharge the NetCharge
	 * @return MytestStructMoney
	 */
	public function setNetCharge($_netCharge)
	{
		return ($this->NetCharge = $_netCharge);
	}
	/**
	 * Get TotalRebates value
	 * @return MytestStructMoney|null
	 */
	public function getTotalRebates()
	{
		return $this->TotalRebates;
	}
	/**
	 * Set TotalRebates value
	 * @param MytestStructMoney $_totalRebates the TotalRebates
	 * @return MytestStructMoney
	 */
	public function setTotalRebates($_totalRebates)
	{
		return ($this->TotalRebates = $_totalRebates);
	}
	/**
	 * Get FreightDiscounts value
	 * @return MytestStructRateDiscount|null
	 */
	public function getFreightDiscounts()
	{
		return $this->FreightDiscounts;
	}
	/**
	 * Set FreightDiscounts value
	 * @param MytestStructRateDiscount $_freightDiscounts the FreightDiscounts
	 * @return MytestStructRateDiscount
	 */
	public function setFreightDiscounts($_freightDiscounts)
	{
		return ($this->FreightDiscounts = $_freightDiscounts);
	}
	/**
	 * Get Rebates value
	 * @return MytestStructRebate|null
	 */
	public function getRebates()
	{
		return $this->Rebates;
	}
	/**
	 * Set Rebates value
	 * @param MytestStructRebate $_rebates the Rebates
	 * @return MytestStructRebate
	 */
	public function setRebates($_rebates)
	{
		return ($this->Rebates = $_rebates);
	}
	/**
	 * Get Surcharges value
	 * @return MytestStructSurcharge|null
	 */
	public function getSurcharges()
	{
		return $this->Surcharges;
	}
	/**
	 * Set Surcharges value
	 * @param MytestStructSurcharge $_surcharges the Surcharges
	 * @return MytestStructSurcharge
	 */
	public function setSurcharges($_surcharges)
	{
		return ($this->Surcharges = $_surcharges);
	}
	/**
	 * Get Taxes value
	 * @return MytestStructTax|null
	 */
	public function getTaxes()
	{
		return $this->Taxes;
	}
	/**
	 * Set Taxes value
	 * @param MytestStructTax $_taxes the Taxes
	 * @return MytestStructTax
	 */
	public function setTaxes($_taxes)
	{
		return ($this->Taxes = $_taxes);
	}
	/**
	 * Get VariableHandlingCharges value
	 * @return MytestStructVariableHandlingCharges|null
	 */
	public function getVariableHandlingCharges()
	{
		return $this->VariableHandlingCharges;
	}
	/**
	 * Set VariableHandlingCharges value
	 * @param MytestStructVariableHandlingCharges $_variableHandlingCharges the VariableHandlingCharges
	 * @return MytestStructVariableHandlingCharges
	 */
	public function setVariableHandlingCharges($_variableHandlingCharges)
	{
		return ($this->VariableHandlingCharges = $_variableHandlingCharges);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructPackageRateDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>