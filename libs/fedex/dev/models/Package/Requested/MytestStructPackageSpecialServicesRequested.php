<?php
/**
 * File for class MytestStructPackageSpecialServicesRequested
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructPackageSpecialServicesRequested originally named PackageSpecialServicesRequested
 * Documentation : These special services are available at the package level for some or all service types. If the shipper is requesting a special service which requires additional data, the package special service type must be present in the specialServiceTypes collection, and the supporting detail must be provided in the appropriate sub-object below.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructPackageSpecialServicesRequested extends MytestWsdlClass
{
	/**
	 * The SpecialServiceTypes
	 * Meta informations extracted from the WSDL
	 * - documentation : The types of all special services requested for the enclosing shipment or package.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumPackageSpecialServiceType
	 */
	public $SpecialServiceTypes;
	/**
	 * The CodDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : For use with FedEx Ground services only; COD must be present in shipment's special services.
	 * - minOccurs : 0
	 * @var MytestStructCodDetail
	 */
	public $CodDetail;
	/**
	 * The DangerousGoodsDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructDangerousGoodsDetail
	 */
	public $DangerousGoodsDetail;
	/**
	 * The DryIceWeight
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructWeight
	 */
	public $DryIceWeight;
	/**
	 * The SignatureOptionDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructSignatureOptionDetail
	 */
	public $SignatureOptionDetail;
	/**
	 * The PriorityAlertDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructPriorityAlertDetail
	 */
	public $PriorityAlertDetail;
	/**
	 * Constructor method for PackageSpecialServicesRequested
	 * @see parent::__construct()
	 * @param MytestEnumPackageSpecialServiceType $_specialServiceTypes
	 * @param MytestStructCodDetail $_codDetail
	 * @param MytestStructDangerousGoodsDetail $_dangerousGoodsDetail
	 * @param MytestStructWeight $_dryIceWeight
	 * @param MytestStructSignatureOptionDetail $_signatureOptionDetail
	 * @param MytestStructPriorityAlertDetail $_priorityAlertDetail
	 * @return MytestStructPackageSpecialServicesRequested
	 */
	public function __construct($_specialServiceTypes = NULL,$_codDetail = NULL,$_dangerousGoodsDetail = NULL,$_dryIceWeight = NULL,$_signatureOptionDetail = NULL,$_priorityAlertDetail = NULL)
	{
		parent::__construct(array('SpecialServiceTypes'=>$_specialServiceTypes,'CodDetail'=>$_codDetail,'DangerousGoodsDetail'=>$_dangerousGoodsDetail,'DryIceWeight'=>$_dryIceWeight,'SignatureOptionDetail'=>$_signatureOptionDetail,'PriorityAlertDetail'=>$_priorityAlertDetail));
	}
	/**
	 * Get SpecialServiceTypes value
	 * @return MytestEnumPackageSpecialServiceType|null
	 */
	public function getSpecialServiceTypes()
	{
		return $this->SpecialServiceTypes;
	}
	/**
	 * Set SpecialServiceTypes value
	 * @uses MytestEnumPackageSpecialServiceType::valueIsValid()
	 * @param MytestEnumPackageSpecialServiceType $_specialServiceTypes the SpecialServiceTypes
	 * @return MytestEnumPackageSpecialServiceType
	 */
	public function setSpecialServiceTypes($_specialServiceTypes)
	{
		if(!MytestEnumPackageSpecialServiceType::valueIsValid($_specialServiceTypes))
		{
			return false;
		}
		return ($this->SpecialServiceTypes = $_specialServiceTypes);
	}
	/**
	 * Get CodDetail value
	 * @return MytestStructCodDetail|null
	 */
	public function getCodDetail()
	{
		return $this->CodDetail;
	}
	/**
	 * Set CodDetail value
	 * @param MytestStructCodDetail $_codDetail the CodDetail
	 * @return MytestStructCodDetail
	 */
	public function setCodDetail($_codDetail)
	{
		return ($this->CodDetail = $_codDetail);
	}
	/**
	 * Get DangerousGoodsDetail value
	 * @return MytestStructDangerousGoodsDetail|null
	 */
	public function getDangerousGoodsDetail()
	{
		return $this->DangerousGoodsDetail;
	}
	/**
	 * Set DangerousGoodsDetail value
	 * @param MytestStructDangerousGoodsDetail $_dangerousGoodsDetail the DangerousGoodsDetail
	 * @return MytestStructDangerousGoodsDetail
	 */
	public function setDangerousGoodsDetail($_dangerousGoodsDetail)
	{
		return ($this->DangerousGoodsDetail = $_dangerousGoodsDetail);
	}
	/**
	 * Get DryIceWeight value
	 * @return MytestStructWeight|null
	 */
	public function getDryIceWeight()
	{
		return $this->DryIceWeight;
	}
	/**
	 * Set DryIceWeight value
	 * @param MytestStructWeight $_dryIceWeight the DryIceWeight
	 * @return MytestStructWeight
	 */
	public function setDryIceWeight($_dryIceWeight)
	{
		return ($this->DryIceWeight = $_dryIceWeight);
	}
	/**
	 * Get SignatureOptionDetail value
	 * @return MytestStructSignatureOptionDetail|null
	 */
	public function getSignatureOptionDetail()
	{
		return $this->SignatureOptionDetail;
	}
	/**
	 * Set SignatureOptionDetail value
	 * @param MytestStructSignatureOptionDetail $_signatureOptionDetail the SignatureOptionDetail
	 * @return MytestStructSignatureOptionDetail
	 */
	public function setSignatureOptionDetail($_signatureOptionDetail)
	{
		return ($this->SignatureOptionDetail = $_signatureOptionDetail);
	}
	/**
	 * Get PriorityAlertDetail value
	 * @return MytestStructPriorityAlertDetail|null
	 */
	public function getPriorityAlertDetail()
	{
		return $this->PriorityAlertDetail;
	}
	/**
	 * Set PriorityAlertDetail value
	 * @param MytestStructPriorityAlertDetail $_priorityAlertDetail the PriorityAlertDetail
	 * @return MytestStructPriorityAlertDetail
	 */
	public function setPriorityAlertDetail($_priorityAlertDetail)
	{
		return ($this->PriorityAlertDetail = $_priorityAlertDetail);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructPackageSpecialServicesRequested
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>