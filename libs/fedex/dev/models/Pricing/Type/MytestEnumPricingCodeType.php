<?php
/**
 * File for class MytestEnumPricingCodeType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumPricingCodeType originally named PricingCodeType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumPricingCodeType extends MytestWsdlClass
{
	/**
	 * Constant for value 'ACTUAL'
	 * @return string 'ACTUAL'
	 */
	const VALUE_ACTUAL = 'ACTUAL';
	/**
	 * Constant for value 'ALTERNATE'
	 * @return string 'ALTERNATE'
	 */
	const VALUE_ALTERNATE = 'ALTERNATE';
	/**
	 * Constant for value 'BASE'
	 * @return string 'BASE'
	 */
	const VALUE_BASE = 'BASE';
	/**
	 * Constant for value 'HUNDREDWEIGHT'
	 * @return string 'HUNDREDWEIGHT'
	 */
	const VALUE_HUNDREDWEIGHT = 'HUNDREDWEIGHT';
	/**
	 * Constant for value 'HUNDREDWEIGHT_ALTERNATE'
	 * @return string 'HUNDREDWEIGHT_ALTERNATE'
	 */
	const VALUE_HUNDREDWEIGHT_ALTERNATE = 'HUNDREDWEIGHT_ALTERNATE';
	/**
	 * Constant for value 'INTERNATIONAL_DISTRIBUTION'
	 * @return string 'INTERNATIONAL_DISTRIBUTION'
	 */
	const VALUE_INTERNATIONAL_DISTRIBUTION = 'INTERNATIONAL_DISTRIBUTION';
	/**
	 * Constant for value 'INTERNATIONAL_ECONOMY_SERVICE'
	 * @return string 'INTERNATIONAL_ECONOMY_SERVICE'
	 */
	const VALUE_INTERNATIONAL_ECONOMY_SERVICE = 'INTERNATIONAL_ECONOMY_SERVICE';
	/**
	 * Constant for value 'LTL_FREIGHT'
	 * @return string 'LTL_FREIGHT'
	 */
	const VALUE_LTL_FREIGHT = 'LTL_FREIGHT';
	/**
	 * Constant for value 'PACKAGE'
	 * @return string 'PACKAGE'
	 */
	const VALUE_PACKAGE = 'PACKAGE';
	/**
	 * Constant for value 'SHIPMENT'
	 * @return string 'SHIPMENT'
	 */
	const VALUE_SHIPMENT = 'SHIPMENT';
	/**
	 * Constant for value 'SHIPMENT_FIVE_POUND_OPTIONAL'
	 * @return string 'SHIPMENT_FIVE_POUND_OPTIONAL'
	 */
	const VALUE_SHIPMENT_FIVE_POUND_OPTIONAL = 'SHIPMENT_FIVE_POUND_OPTIONAL';
	/**
	 * Constant for value 'SHIPMENT_OPTIONAL'
	 * @return string 'SHIPMENT_OPTIONAL'
	 */
	const VALUE_SHIPMENT_OPTIONAL = 'SHIPMENT_OPTIONAL';
	/**
	 * Constant for value 'SPECIAL'
	 * @return string 'SPECIAL'
	 */
	const VALUE_SPECIAL = 'SPECIAL';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumPricingCodeType::VALUE_ACTUAL
	 * @uses MytestEnumPricingCodeType::VALUE_ALTERNATE
	 * @uses MytestEnumPricingCodeType::VALUE_BASE
	 * @uses MytestEnumPricingCodeType::VALUE_HUNDREDWEIGHT
	 * @uses MytestEnumPricingCodeType::VALUE_HUNDREDWEIGHT_ALTERNATE
	 * @uses MytestEnumPricingCodeType::VALUE_INTERNATIONAL_DISTRIBUTION
	 * @uses MytestEnumPricingCodeType::VALUE_INTERNATIONAL_ECONOMY_SERVICE
	 * @uses MytestEnumPricingCodeType::VALUE_LTL_FREIGHT
	 * @uses MytestEnumPricingCodeType::VALUE_PACKAGE
	 * @uses MytestEnumPricingCodeType::VALUE_SHIPMENT
	 * @uses MytestEnumPricingCodeType::VALUE_SHIPMENT_FIVE_POUND_OPTIONAL
	 * @uses MytestEnumPricingCodeType::VALUE_SHIPMENT_OPTIONAL
	 * @uses MytestEnumPricingCodeType::VALUE_SPECIAL
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumPricingCodeType::VALUE_ACTUAL,MytestEnumPricingCodeType::VALUE_ALTERNATE,MytestEnumPricingCodeType::VALUE_BASE,MytestEnumPricingCodeType::VALUE_HUNDREDWEIGHT,MytestEnumPricingCodeType::VALUE_HUNDREDWEIGHT_ALTERNATE,MytestEnumPricingCodeType::VALUE_INTERNATIONAL_DISTRIBUTION,MytestEnumPricingCodeType::VALUE_INTERNATIONAL_ECONOMY_SERVICE,MytestEnumPricingCodeType::VALUE_LTL_FREIGHT,MytestEnumPricingCodeType::VALUE_PACKAGE,MytestEnumPricingCodeType::VALUE_SHIPMENT,MytestEnumPricingCodeType::VALUE_SHIPMENT_FIVE_POUND_OPTIONAL,MytestEnumPricingCodeType::VALUE_SHIPMENT_OPTIONAL,MytestEnumPricingCodeType::VALUE_SPECIAL));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>