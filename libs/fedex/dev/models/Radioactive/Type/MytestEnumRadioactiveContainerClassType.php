<?php
/**
 * File for class MytestEnumRadioactiveContainerClassType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumRadioactiveContainerClassType originally named RadioactiveContainerClassType
 * Documentation : Indicates the packaging type of the container used to package radioactive hazardous materials.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumRadioactiveContainerClassType extends MytestWsdlClass
{
	/**
	 * Constant for value 'EXCEPTED_PACKAGE'
	 * @return string 'EXCEPTED_PACKAGE'
	 */
	const VALUE_EXCEPTED_PACKAGE = 'EXCEPTED_PACKAGE';
	/**
	 * Constant for value 'INDUSTRIAL_IP1'
	 * @return string 'INDUSTRIAL_IP1'
	 */
	const VALUE_INDUSTRIAL_IP1 = 'INDUSTRIAL_IP1';
	/**
	 * Constant for value 'INDUSTRIAL_IP2'
	 * @return string 'INDUSTRIAL_IP2'
	 */
	const VALUE_INDUSTRIAL_IP2 = 'INDUSTRIAL_IP2';
	/**
	 * Constant for value 'INDUSTRIAL_IP3'
	 * @return string 'INDUSTRIAL_IP3'
	 */
	const VALUE_INDUSTRIAL_IP3 = 'INDUSTRIAL_IP3';
	/**
	 * Constant for value 'TYPE_A'
	 * @return string 'TYPE_A'
	 */
	const VALUE_TYPE_A = 'TYPE_A';
	/**
	 * Constant for value 'TYPE_B_M'
	 * @return string 'TYPE_B_M'
	 */
	const VALUE_TYPE_B_M = 'TYPE_B_M';
	/**
	 * Constant for value 'TYPE_B_U'
	 * @return string 'TYPE_B_U'
	 */
	const VALUE_TYPE_B_U = 'TYPE_B_U';
	/**
	 * Constant for value 'TYPE_C'
	 * @return string 'TYPE_C'
	 */
	const VALUE_TYPE_C = 'TYPE_C';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumRadioactiveContainerClassType::VALUE_EXCEPTED_PACKAGE
	 * @uses MytestEnumRadioactiveContainerClassType::VALUE_INDUSTRIAL_IP1
	 * @uses MytestEnumRadioactiveContainerClassType::VALUE_INDUSTRIAL_IP2
	 * @uses MytestEnumRadioactiveContainerClassType::VALUE_INDUSTRIAL_IP3
	 * @uses MytestEnumRadioactiveContainerClassType::VALUE_TYPE_A
	 * @uses MytestEnumRadioactiveContainerClassType::VALUE_TYPE_B_M
	 * @uses MytestEnumRadioactiveContainerClassType::VALUE_TYPE_B_U
	 * @uses MytestEnumRadioactiveContainerClassType::VALUE_TYPE_C
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumRadioactiveContainerClassType::VALUE_EXCEPTED_PACKAGE,MytestEnumRadioactiveContainerClassType::VALUE_INDUSTRIAL_IP1,MytestEnumRadioactiveContainerClassType::VALUE_INDUSTRIAL_IP2,MytestEnumRadioactiveContainerClassType::VALUE_INDUSTRIAL_IP3,MytestEnumRadioactiveContainerClassType::VALUE_TYPE_A,MytestEnumRadioactiveContainerClassType::VALUE_TYPE_B_M,MytestEnumRadioactiveContainerClassType::VALUE_TYPE_B_U,MytestEnumRadioactiveContainerClassType::VALUE_TYPE_C));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>