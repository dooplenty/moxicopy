<?php
/**
 * File for class MytestStructParty
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructParty originally named Party
 * Documentation : The descriptive data for a person or company entitiy doing business with FedEx.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructParty extends MytestWsdlClass
{
	/**
	 * The AccountNumber
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $AccountNumber;
	/**
	 * The Tins
	 * Meta informations extracted from the WSDL
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructTaxpayerIdentification
	 */
	public $Tins;
	/**
	 * The Contact
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructContact
	 */
	public $Contact;
	/**
	 * The Address
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructAddress
	 */
	public $Address;
	/**
	 * Constructor method for Party
	 * @see parent::__construct()
	 * @param string $_accountNumber
	 * @param MytestStructTaxpayerIdentification $_tins
	 * @param MytestStructContact $_contact
	 * @param MytestStructAddress $_address
	 * @return MytestStructParty
	 */
	public function __construct($_accountNumber = NULL,$_tins = NULL,$_contact = NULL,$_address = NULL)
	{
		parent::__construct(array('AccountNumber'=>$_accountNumber,'Tins'=>$_tins,'Contact'=>$_contact,'Address'=>$_address));
	}
	/**
	 * Get AccountNumber value
	 * @return string|null
	 */
	public function getAccountNumber()
	{
		return $this->AccountNumber;
	}
	/**
	 * Set AccountNumber value
	 * @param string $_accountNumber the AccountNumber
	 * @return string
	 */
	public function setAccountNumber($_accountNumber)
	{
		return ($this->AccountNumber = $_accountNumber);
	}
	/**
	 * Get Tins value
	 * @return MytestStructTaxpayerIdentification|null
	 */
	public function getTins()
	{
		return $this->Tins;
	}
	/**
	 * Set Tins value
	 * @param MytestStructTaxpayerIdentification $_tins the Tins
	 * @return MytestStructTaxpayerIdentification
	 */
	public function setTins($_tins)
	{
		return ($this->Tins = $_tins);
	}
	/**
	 * Get Contact value
	 * @return MytestStructContact|null
	 */
	public function getContact()
	{
		return $this->Contact;
	}
	/**
	 * Set Contact value
	 * @param MytestStructContact $_contact the Contact
	 * @return MytestStructContact
	 */
	public function setContact($_contact)
	{
		return ($this->Contact = $_contact);
	}
	/**
	 * Get Address value
	 * @return MytestStructAddress|null
	 */
	public function getAddress()
	{
		return $this->Address;
	}
	/**
	 * Set Address value
	 * @param MytestStructAddress $_address the Address
	 * @return MytestStructAddress
	 */
	public function setAddress($_address)
	{
		return ($this->Address = $_address);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructParty
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>