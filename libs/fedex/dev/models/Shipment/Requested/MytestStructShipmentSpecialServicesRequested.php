<?php
/**
 * File for class MytestStructShipmentSpecialServicesRequested
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructShipmentSpecialServicesRequested originally named ShipmentSpecialServicesRequested
 * Documentation : These special services are available at the shipment level for some or all service types. If the shipper is requesting a special service which requires additional data (such as the COD amount), the shipment special service type must be present in the specialServiceTypes collection, and the supporting detail must be provided in the appropriate sub-object below.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructShipmentSpecialServicesRequested extends MytestWsdlClass
{
	/**
	 * The SpecialServiceTypes
	 * Meta informations extracted from the WSDL
	 * - documentation : The types of all special services requested for the enclosing shipment (or other shipment-level transaction).
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumShipmentSpecialServiceType
	 */
	public $SpecialServiceTypes;
	/**
	 * The CodDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructCodDetail
	 */
	public $CodDetail;
	/**
	 * The DeliveryOnInvoiceAcceptanceDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructDeliveryOnInvoiceAcceptanceDetail
	 */
	public $DeliveryOnInvoiceAcceptanceDetail;
	/**
	 * The HoldAtLocationDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructHoldAtLocationDetail
	 */
	public $HoldAtLocationDetail;
	/**
	 * The EMailNotificationDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructEMailNotificationDetail
	 */
	public $EMailNotificationDetail;
	/**
	 * The ReturnShipmentDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructReturnShipmentDetail
	 */
	public $ReturnShipmentDetail;
	/**
	 * The PendingShipmentDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : This field should be populated for pending shipments (e.g. e-mail label) It is required by a PENDING_SHIPMENT special service type.
	 * - minOccurs : 0
	 * @var MytestStructPendingShipmentDetail
	 */
	public $PendingShipmentDetail;
	/**
	 * The InternationalControlledExportDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructInternationalControlledExportDetail
	 */
	public $InternationalControlledExportDetail;
	/**
	 * The InternationalTrafficInArmsRegulationsDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructInternationalTrafficInArmsRegulationsDetail
	 */
	public $InternationalTrafficInArmsRegulationsDetail;
	/**
	 * The ShipmentDryIceDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructShipmentDryIceDetail
	 */
	public $ShipmentDryIceDetail;
	/**
	 * The HomeDeliveryPremiumDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructHomeDeliveryPremiumDetail
	 */
	public $HomeDeliveryPremiumDetail;
	/**
	 * The FlatbedTrailerDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructFlatbedTrailerDetail
	 */
	public $FlatbedTrailerDetail;
	/**
	 * The FreightGuaranteeDetail
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructFreightGuaranteeDetail
	 */
	public $FreightGuaranteeDetail;
	/**
	 * The EtdDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Electronic Trade document references.
	 * - minOccurs : 0
	 * @var MytestStructEtdDetail
	 */
	public $EtdDetail;
	/**
	 * The CustomDeliveryWindowDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Specification for date or range of dates on which delivery is to be attempted.
	 * - minOccurs : 0
	 * @var MytestStructCustomDeliveryWindowDetail
	 */
	public $CustomDeliveryWindowDetail;
	/**
	 * Constructor method for ShipmentSpecialServicesRequested
	 * @see parent::__construct()
	 * @param MytestEnumShipmentSpecialServiceType $_specialServiceTypes
	 * @param MytestStructCodDetail $_codDetail
	 * @param MytestStructDeliveryOnInvoiceAcceptanceDetail $_deliveryOnInvoiceAcceptanceDetail
	 * @param MytestStructHoldAtLocationDetail $_holdAtLocationDetail
	 * @param MytestStructEMailNotificationDetail $_eMailNotificationDetail
	 * @param MytestStructReturnShipmentDetail $_returnShipmentDetail
	 * @param MytestStructPendingShipmentDetail $_pendingShipmentDetail
	 * @param MytestStructInternationalControlledExportDetail $_internationalControlledExportDetail
	 * @param MytestStructInternationalTrafficInArmsRegulationsDetail $_internationalTrafficInArmsRegulationsDetail
	 * @param MytestStructShipmentDryIceDetail $_shipmentDryIceDetail
	 * @param MytestStructHomeDeliveryPremiumDetail $_homeDeliveryPremiumDetail
	 * @param MytestStructFlatbedTrailerDetail $_flatbedTrailerDetail
	 * @param MytestStructFreightGuaranteeDetail $_freightGuaranteeDetail
	 * @param MytestStructEtdDetail $_etdDetail
	 * @param MytestStructCustomDeliveryWindowDetail $_customDeliveryWindowDetail
	 * @return MytestStructShipmentSpecialServicesRequested
	 */
	public function __construct($_specialServiceTypes = NULL,$_codDetail = NULL,$_deliveryOnInvoiceAcceptanceDetail = NULL,$_holdAtLocationDetail = NULL,$_eMailNotificationDetail = NULL,$_returnShipmentDetail = NULL,$_pendingShipmentDetail = NULL,$_internationalControlledExportDetail = NULL,$_internationalTrafficInArmsRegulationsDetail = NULL,$_shipmentDryIceDetail = NULL,$_homeDeliveryPremiumDetail = NULL,$_flatbedTrailerDetail = NULL,$_freightGuaranteeDetail = NULL,$_etdDetail = NULL,$_customDeliveryWindowDetail = NULL)
	{
		parent::__construct(array('SpecialServiceTypes'=>$_specialServiceTypes,'CodDetail'=>$_codDetail,'DeliveryOnInvoiceAcceptanceDetail'=>$_deliveryOnInvoiceAcceptanceDetail,'HoldAtLocationDetail'=>$_holdAtLocationDetail,'EMailNotificationDetail'=>$_eMailNotificationDetail,'ReturnShipmentDetail'=>$_returnShipmentDetail,'PendingShipmentDetail'=>$_pendingShipmentDetail,'InternationalControlledExportDetail'=>$_internationalControlledExportDetail,'InternationalTrafficInArmsRegulationsDetail'=>$_internationalTrafficInArmsRegulationsDetail,'ShipmentDryIceDetail'=>$_shipmentDryIceDetail,'HomeDeliveryPremiumDetail'=>$_homeDeliveryPremiumDetail,'FlatbedTrailerDetail'=>$_flatbedTrailerDetail,'FreightGuaranteeDetail'=>$_freightGuaranteeDetail,'EtdDetail'=>$_etdDetail,'CustomDeliveryWindowDetail'=>$_customDeliveryWindowDetail));
	}
	/**
	 * Get SpecialServiceTypes value
	 * @return MytestEnumShipmentSpecialServiceType|null
	 */
	public function getSpecialServiceTypes()
	{
		return $this->SpecialServiceTypes;
	}
	/**
	 * Set SpecialServiceTypes value
	 * @uses MytestEnumShipmentSpecialServiceType::valueIsValid()
	 * @param MytestEnumShipmentSpecialServiceType $_specialServiceTypes the SpecialServiceTypes
	 * @return MytestEnumShipmentSpecialServiceType
	 */
	public function setSpecialServiceTypes($_specialServiceTypes)
	{
		if(!MytestEnumShipmentSpecialServiceType::valueIsValid($_specialServiceTypes))
		{
			return false;
		}
		return ($this->SpecialServiceTypes = $_specialServiceTypes);
	}
	/**
	 * Get CodDetail value
	 * @return MytestStructCodDetail|null
	 */
	public function getCodDetail()
	{
		return $this->CodDetail;
	}
	/**
	 * Set CodDetail value
	 * @param MytestStructCodDetail $_codDetail the CodDetail
	 * @return MytestStructCodDetail
	 */
	public function setCodDetail($_codDetail)
	{
		return ($this->CodDetail = $_codDetail);
	}
	/**
	 * Get DeliveryOnInvoiceAcceptanceDetail value
	 * @return MytestStructDeliveryOnInvoiceAcceptanceDetail|null
	 */
	public function getDeliveryOnInvoiceAcceptanceDetail()
	{
		return $this->DeliveryOnInvoiceAcceptanceDetail;
	}
	/**
	 * Set DeliveryOnInvoiceAcceptanceDetail value
	 * @param MytestStructDeliveryOnInvoiceAcceptanceDetail $_deliveryOnInvoiceAcceptanceDetail the DeliveryOnInvoiceAcceptanceDetail
	 * @return MytestStructDeliveryOnInvoiceAcceptanceDetail
	 */
	public function setDeliveryOnInvoiceAcceptanceDetail($_deliveryOnInvoiceAcceptanceDetail)
	{
		return ($this->DeliveryOnInvoiceAcceptanceDetail = $_deliveryOnInvoiceAcceptanceDetail);
	}
	/**
	 * Get HoldAtLocationDetail value
	 * @return MytestStructHoldAtLocationDetail|null
	 */
	public function getHoldAtLocationDetail()
	{
		return $this->HoldAtLocationDetail;
	}
	/**
	 * Set HoldAtLocationDetail value
	 * @param MytestStructHoldAtLocationDetail $_holdAtLocationDetail the HoldAtLocationDetail
	 * @return MytestStructHoldAtLocationDetail
	 */
	public function setHoldAtLocationDetail($_holdAtLocationDetail)
	{
		return ($this->HoldAtLocationDetail = $_holdAtLocationDetail);
	}
	/**
	 * Get EMailNotificationDetail value
	 * @return MytestStructEMailNotificationDetail|null
	 */
	public function getEMailNotificationDetail()
	{
		return $this->EMailNotificationDetail;
	}
	/**
	 * Set EMailNotificationDetail value
	 * @param MytestStructEMailNotificationDetail $_eMailNotificationDetail the EMailNotificationDetail
	 * @return MytestStructEMailNotificationDetail
	 */
	public function setEMailNotificationDetail($_eMailNotificationDetail)
	{
		return ($this->EMailNotificationDetail = $_eMailNotificationDetail);
	}
	/**
	 * Get ReturnShipmentDetail value
	 * @return MytestStructReturnShipmentDetail|null
	 */
	public function getReturnShipmentDetail()
	{
		return $this->ReturnShipmentDetail;
	}
	/**
	 * Set ReturnShipmentDetail value
	 * @param MytestStructReturnShipmentDetail $_returnShipmentDetail the ReturnShipmentDetail
	 * @return MytestStructReturnShipmentDetail
	 */
	public function setReturnShipmentDetail($_returnShipmentDetail)
	{
		return ($this->ReturnShipmentDetail = $_returnShipmentDetail);
	}
	/**
	 * Get PendingShipmentDetail value
	 * @return MytestStructPendingShipmentDetail|null
	 */
	public function getPendingShipmentDetail()
	{
		return $this->PendingShipmentDetail;
	}
	/**
	 * Set PendingShipmentDetail value
	 * @param MytestStructPendingShipmentDetail $_pendingShipmentDetail the PendingShipmentDetail
	 * @return MytestStructPendingShipmentDetail
	 */
	public function setPendingShipmentDetail($_pendingShipmentDetail)
	{
		return ($this->PendingShipmentDetail = $_pendingShipmentDetail);
	}
	/**
	 * Get InternationalControlledExportDetail value
	 * @return MytestStructInternationalControlledExportDetail|null
	 */
	public function getInternationalControlledExportDetail()
	{
		return $this->InternationalControlledExportDetail;
	}
	/**
	 * Set InternationalControlledExportDetail value
	 * @param MytestStructInternationalControlledExportDetail $_internationalControlledExportDetail the InternationalControlledExportDetail
	 * @return MytestStructInternationalControlledExportDetail
	 */
	public function setInternationalControlledExportDetail($_internationalControlledExportDetail)
	{
		return ($this->InternationalControlledExportDetail = $_internationalControlledExportDetail);
	}
	/**
	 * Get InternationalTrafficInArmsRegulationsDetail value
	 * @return MytestStructInternationalTrafficInArmsRegulationsDetail|null
	 */
	public function getInternationalTrafficInArmsRegulationsDetail()
	{
		return $this->InternationalTrafficInArmsRegulationsDetail;
	}
	/**
	 * Set InternationalTrafficInArmsRegulationsDetail value
	 * @param MytestStructInternationalTrafficInArmsRegulationsDetail $_internationalTrafficInArmsRegulationsDetail the InternationalTrafficInArmsRegulationsDetail
	 * @return MytestStructInternationalTrafficInArmsRegulationsDetail
	 */
	public function setInternationalTrafficInArmsRegulationsDetail($_internationalTrafficInArmsRegulationsDetail)
	{
		return ($this->InternationalTrafficInArmsRegulationsDetail = $_internationalTrafficInArmsRegulationsDetail);
	}
	/**
	 * Get ShipmentDryIceDetail value
	 * @return MytestStructShipmentDryIceDetail|null
	 */
	public function getShipmentDryIceDetail()
	{
		return $this->ShipmentDryIceDetail;
	}
	/**
	 * Set ShipmentDryIceDetail value
	 * @param MytestStructShipmentDryIceDetail $_shipmentDryIceDetail the ShipmentDryIceDetail
	 * @return MytestStructShipmentDryIceDetail
	 */
	public function setShipmentDryIceDetail($_shipmentDryIceDetail)
	{
		return ($this->ShipmentDryIceDetail = $_shipmentDryIceDetail);
	}
	/**
	 * Get HomeDeliveryPremiumDetail value
	 * @return MytestStructHomeDeliveryPremiumDetail|null
	 */
	public function getHomeDeliveryPremiumDetail()
	{
		return $this->HomeDeliveryPremiumDetail;
	}
	/**
	 * Set HomeDeliveryPremiumDetail value
	 * @param MytestStructHomeDeliveryPremiumDetail $_homeDeliveryPremiumDetail the HomeDeliveryPremiumDetail
	 * @return MytestStructHomeDeliveryPremiumDetail
	 */
	public function setHomeDeliveryPremiumDetail($_homeDeliveryPremiumDetail)
	{
		return ($this->HomeDeliveryPremiumDetail = $_homeDeliveryPremiumDetail);
	}
	/**
	 * Get FlatbedTrailerDetail value
	 * @return MytestStructFlatbedTrailerDetail|null
	 */
	public function getFlatbedTrailerDetail()
	{
		return $this->FlatbedTrailerDetail;
	}
	/**
	 * Set FlatbedTrailerDetail value
	 * @param MytestStructFlatbedTrailerDetail $_flatbedTrailerDetail the FlatbedTrailerDetail
	 * @return MytestStructFlatbedTrailerDetail
	 */
	public function setFlatbedTrailerDetail($_flatbedTrailerDetail)
	{
		return ($this->FlatbedTrailerDetail = $_flatbedTrailerDetail);
	}
	/**
	 * Get FreightGuaranteeDetail value
	 * @return MytestStructFreightGuaranteeDetail|null
	 */
	public function getFreightGuaranteeDetail()
	{
		return $this->FreightGuaranteeDetail;
	}
	/**
	 * Set FreightGuaranteeDetail value
	 * @param MytestStructFreightGuaranteeDetail $_freightGuaranteeDetail the FreightGuaranteeDetail
	 * @return MytestStructFreightGuaranteeDetail
	 */
	public function setFreightGuaranteeDetail($_freightGuaranteeDetail)
	{
		return ($this->FreightGuaranteeDetail = $_freightGuaranteeDetail);
	}
	/**
	 * Get EtdDetail value
	 * @return MytestStructEtdDetail|null
	 */
	public function getEtdDetail()
	{
		return $this->EtdDetail;
	}
	/**
	 * Set EtdDetail value
	 * @param MytestStructEtdDetail $_etdDetail the EtdDetail
	 * @return MytestStructEtdDetail
	 */
	public function setEtdDetail($_etdDetail)
	{
		return ($this->EtdDetail = $_etdDetail);
	}
	/**
	 * Get CustomDeliveryWindowDetail value
	 * @return MytestStructCustomDeliveryWindowDetail|null
	 */
	public function getCustomDeliveryWindowDetail()
	{
		return $this->CustomDeliveryWindowDetail;
	}
	/**
	 * Set CustomDeliveryWindowDetail value
	 * @param MytestStructCustomDeliveryWindowDetail $_customDeliveryWindowDetail the CustomDeliveryWindowDetail
	 * @return MytestStructCustomDeliveryWindowDetail
	 */
	public function setCustomDeliveryWindowDetail($_customDeliveryWindowDetail)
	{
		return ($this->CustomDeliveryWindowDetail = $_customDeliveryWindowDetail);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructShipmentSpecialServicesRequested
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>