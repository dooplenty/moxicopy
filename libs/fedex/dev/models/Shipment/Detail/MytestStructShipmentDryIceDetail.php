<?php
/**
 * File for class MytestStructShipmentDryIceDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructShipmentDryIceDetail originally named ShipmentDryIceDetail
 * Documentation : Shipment-level totals of dry ice data across all packages.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructShipmentDryIceDetail extends MytestWsdlClass
{
	/**
	 * The PackageCount
	 * Meta informations extracted from the WSDL
	 * - documentation : Total number of packages in the shipment that contain dry ice.
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $PackageCount;
	/**
	 * The TotalWeight
	 * Meta informations extracted from the WSDL
	 * - documentation : Total shipment dry ice weight for all packages.
	 * - minOccurs : 0
	 * @var MytestStructWeight
	 */
	public $TotalWeight;
	/**
	 * Constructor method for ShipmentDryIceDetail
	 * @see parent::__construct()
	 * @param nonNegativeInteger $_packageCount
	 * @param MytestStructWeight $_totalWeight
	 * @return MytestStructShipmentDryIceDetail
	 */
	public function __construct($_packageCount = NULL,$_totalWeight = NULL)
	{
		parent::__construct(array('PackageCount'=>$_packageCount,'TotalWeight'=>$_totalWeight));
	}
	/**
	 * Get PackageCount value
	 * @return nonNegativeInteger|null
	 */
	public function getPackageCount()
	{
		return $this->PackageCount;
	}
	/**
	 * Set PackageCount value
	 * @param nonNegativeInteger $_packageCount the PackageCount
	 * @return nonNegativeInteger
	 */
	public function setPackageCount($_packageCount)
	{
		return ($this->PackageCount = $_packageCount);
	}
	/**
	 * Get TotalWeight value
	 * @return MytestStructWeight|null
	 */
	public function getTotalWeight()
	{
		return $this->TotalWeight;
	}
	/**
	 * Set TotalWeight value
	 * @param MytestStructWeight $_totalWeight the TotalWeight
	 * @return MytestStructWeight
	 */
	public function setTotalWeight($_totalWeight)
	{
		return ($this->TotalWeight = $_totalWeight);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructShipmentDryIceDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>