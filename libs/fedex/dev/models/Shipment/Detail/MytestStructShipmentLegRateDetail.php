<?php
/**
 * File for class MytestStructShipmentLegRateDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructShipmentLegRateDetail originally named ShipmentLegRateDetail
 * Documentation : Data for a single leg of a shipment's total/summary rates, as calculated per a specific rate type.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructShipmentLegRateDetail extends MytestWsdlClass
{
	/**
	 * The LegDescription
	 * Meta informations extracted from the WSDL
	 * - documentation : Human-readable text describing the shipment leg.
	 * - minOccurs : 0
	 * @var string
	 */
	public $LegDescription;
	/**
	 * The LegOrigin
	 * Meta informations extracted from the WSDL
	 * - documentation : Origin for this leg.
	 * - minOccurs : 0
	 * @var MytestStructAddress
	 */
	public $LegOrigin;
	/**
	 * The LegOriginLocationId
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the location id the origin of shipment leg.
	 * - minOccurs : 0
	 * @var string
	 */
	public $LegOriginLocationId;
	/**
	 * The LegDestination
	 * Meta informations extracted from the WSDL
	 * - documentation : Destination for this leg.
	 * - minOccurs : 0
	 * @var MytestStructAddress
	 */
	public $LegDestination;
	/**
	 * The LegDestinationLocationId
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the location id the destination of shipment leg.
	 * - minOccurs : 0
	 * @var string
	 */
	public $LegDestinationLocationId;
	/**
	 * The RateType
	 * Meta informations extracted from the WSDL
	 * - documentation : Type used for this specific set of rate data.
	 * - minOccurs : 0
	 * @var MytestEnumReturnedRateType
	 */
	public $RateType;
	/**
	 * The RateScale
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates the rate scale used.
	 * - minOccurs : 0
	 * @var string
	 */
	public $RateScale;
	/**
	 * The RateZone
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates the rate zone used (based on origin and destination).
	 * - minOccurs : 0
	 * @var string
	 */
	public $RateZone;
	/**
	 * The PricingCode
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumPricingCodeType
	 */
	public $PricingCode;
	/**
	 * The RatedWeightMethod
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates which weight was used.
	 * - minOccurs : 0
	 * @var MytestEnumRatedWeightMethod
	 */
	public $RatedWeightMethod;
	/**
	 * The MinimumChargeType
	 * Meta informations extracted from the WSDL
	 * - documentation : INTERNAL FEDEX USE ONLY.
	 * - minOccurs : 0
	 * @var MytestEnumMinimumChargeType
	 */
	public $MinimumChargeType;
	/**
	 * The CurrencyExchangeRate
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the currency exchange performed on financial amounts for this rate.
	 * - minOccurs : 0
	 * @var MytestStructCurrencyExchangeRate
	 */
	public $CurrencyExchangeRate;
	/**
	 * The SpecialRatingApplied
	 * Meta informations extracted from the WSDL
	 * - documentation : Indicates which special rating cases applied to this shipment.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumSpecialRatingAppliedType
	 */
	public $SpecialRatingApplied;
	/**
	 * The DimDivisor
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $DimDivisor;
	/**
	 * The DimDivisorType
	 * Meta informations extracted from the WSDL
	 * - documentation : Identifies the type of dim divisor that was applied.
	 * - minOccurs : 0
	 * @var MytestEnumRateDimensionalDivisorType
	 */
	public $DimDivisorType;
	/**
	 * The FuelSurchargePercent
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var decimal
	 */
	public $FuelSurchargePercent;
	/**
	 * The TotalBillingWeight
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructWeight
	 */
	public $TotalBillingWeight;
	/**
	 * The TotalDimWeight
	 * Meta informations extracted from the WSDL
	 * - documentation : Sum of dimensional weights for all packages.
	 * - minOccurs : 0
	 * @var MytestStructWeight
	 */
	public $TotalDimWeight;
	/**
	 * The TotalBaseCharge
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TotalBaseCharge;
	/**
	 * The TotalFreightDiscounts
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TotalFreightDiscounts;
	/**
	 * The TotalNetFreight
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TotalNetFreight;
	/**
	 * The TotalSurcharges
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TotalSurcharges;
	/**
	 * The TotalNetFedExCharge
	 * Meta informations extracted from the WSDL
	 * - documentation : This shipment's totalNetFreight + totalSurcharges (not including totalTaxes).
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TotalNetFedExCharge;
	/**
	 * The TotalTaxes
	 * Meta informations extracted from the WSDL
	 * - documentation : Total of the transportation-based taxes.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TotalTaxes;
	/**
	 * The TotalNetCharge
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TotalNetCharge;
	/**
	 * The TotalRebates
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TotalRebates;
	/**
	 * The TotalDutiesAndTaxes
	 * Meta informations extracted from the WSDL
	 * - documentation : Total of all values under this shipment's dutiesAndTaxes; only provided if estimated duties and taxes were calculated for this shipment.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TotalDutiesAndTaxes;
	/**
	 * The TotalNetChargeWithDutiesAndTaxes
	 * Meta informations extracted from the WSDL
	 * - documentation : This shipment's totalNetCharge + totalDutiesAndTaxes; only provided if estimated duties and taxes were calculated for this shipment AND duties, taxes and transportation charges are all paid by the same sender's account.
	 * - minOccurs : 0
	 * @var MytestStructMoney
	 */
	public $TotalNetChargeWithDutiesAndTaxes;
	/**
	 * The FreightRateDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Rate data specific to FedEx Freight and FedEx National Freight services.
	 * - minOccurs : 0
	 * @var MytestStructFreightRateDetail
	 */
	public $FreightRateDetail;
	/**
	 * The FreightDiscounts
	 * Meta informations extracted from the WSDL
	 * - documentation : All rate discounts that apply to this shipment.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructRateDiscount
	 */
	public $FreightDiscounts;
	/**
	 * The Rebates
	 * Meta informations extracted from the WSDL
	 * - documentation : All rebates that apply to this shipment.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructRebate
	 */
	public $Rebates;
	/**
	 * The Surcharges
	 * Meta informations extracted from the WSDL
	 * - documentation : All surcharges that apply to this shipment.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructSurcharge
	 */
	public $Surcharges;
	/**
	 * The Taxes
	 * Meta informations extracted from the WSDL
	 * - documentation : All transportation-based taxes applicable to this shipment.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructTax
	 */
	public $Taxes;
	/**
	 * The DutiesAndTaxes
	 * Meta informations extracted from the WSDL
	 * - documentation : All commodity-based duties and taxes applicable to this shipment.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructEdtCommodityTax
	 */
	public $DutiesAndTaxes;
	/**
	 * The VariableHandlingCharges
	 * Meta informations extracted from the WSDL
	 * - documentation : The "order level" variable handling charges.
	 * - minOccurs : 0
	 * @var MytestStructVariableHandlingCharges
	 */
	public $VariableHandlingCharges;
	/**
	 * The TotalVariableHandlingCharges
	 * Meta informations extracted from the WSDL
	 * - documentation : The total of all variable handling charges at both shipment (order) and package level.
	 * - minOccurs : 0
	 * @var MytestStructVariableHandlingCharges
	 */
	public $TotalVariableHandlingCharges;
	/**
	 * Constructor method for ShipmentLegRateDetail
	 * @see parent::__construct()
	 * @param string $_legDescription
	 * @param MytestStructAddress $_legOrigin
	 * @param string $_legOriginLocationId
	 * @param MytestStructAddress $_legDestination
	 * @param string $_legDestinationLocationId
	 * @param MytestEnumReturnedRateType $_rateType
	 * @param string $_rateScale
	 * @param string $_rateZone
	 * @param MytestEnumPricingCodeType $_pricingCode
	 * @param MytestEnumRatedWeightMethod $_ratedWeightMethod
	 * @param MytestEnumMinimumChargeType $_minimumChargeType
	 * @param MytestStructCurrencyExchangeRate $_currencyExchangeRate
	 * @param MytestEnumSpecialRatingAppliedType $_specialRatingApplied
	 * @param nonNegativeInteger $_dimDivisor
	 * @param MytestEnumRateDimensionalDivisorType $_dimDivisorType
	 * @param decimal $_fuelSurchargePercent
	 * @param MytestStructWeight $_totalBillingWeight
	 * @param MytestStructWeight $_totalDimWeight
	 * @param MytestStructMoney $_totalBaseCharge
	 * @param MytestStructMoney $_totalFreightDiscounts
	 * @param MytestStructMoney $_totalNetFreight
	 * @param MytestStructMoney $_totalSurcharges
	 * @param MytestStructMoney $_totalNetFedExCharge
	 * @param MytestStructMoney $_totalTaxes
	 * @param MytestStructMoney $_totalNetCharge
	 * @param MytestStructMoney $_totalRebates
	 * @param MytestStructMoney $_totalDutiesAndTaxes
	 * @param MytestStructMoney $_totalNetChargeWithDutiesAndTaxes
	 * @param MytestStructFreightRateDetail $_freightRateDetail
	 * @param MytestStructRateDiscount $_freightDiscounts
	 * @param MytestStructRebate $_rebates
	 * @param MytestStructSurcharge $_surcharges
	 * @param MytestStructTax $_taxes
	 * @param MytestStructEdtCommodityTax $_dutiesAndTaxes
	 * @param MytestStructVariableHandlingCharges $_variableHandlingCharges
	 * @param MytestStructVariableHandlingCharges $_totalVariableHandlingCharges
	 * @return MytestStructShipmentLegRateDetail
	 */
	public function __construct($_legDescription = NULL,$_legOrigin = NULL,$_legOriginLocationId = NULL,$_legDestination = NULL,$_legDestinationLocationId = NULL,$_rateType = NULL,$_rateScale = NULL,$_rateZone = NULL,$_pricingCode = NULL,$_ratedWeightMethod = NULL,$_minimumChargeType = NULL,$_currencyExchangeRate = NULL,$_specialRatingApplied = NULL,$_dimDivisor = NULL,$_dimDivisorType = NULL,$_fuelSurchargePercent = NULL,$_totalBillingWeight = NULL,$_totalDimWeight = NULL,$_totalBaseCharge = NULL,$_totalFreightDiscounts = NULL,$_totalNetFreight = NULL,$_totalSurcharges = NULL,$_totalNetFedExCharge = NULL,$_totalTaxes = NULL,$_totalNetCharge = NULL,$_totalRebates = NULL,$_totalDutiesAndTaxes = NULL,$_totalNetChargeWithDutiesAndTaxes = NULL,$_freightRateDetail = NULL,$_freightDiscounts = NULL,$_rebates = NULL,$_surcharges = NULL,$_taxes = NULL,$_dutiesAndTaxes = NULL,$_variableHandlingCharges = NULL,$_totalVariableHandlingCharges = NULL)
	{
		parent::__construct(array('LegDescription'=>$_legDescription,'LegOrigin'=>$_legOrigin,'LegOriginLocationId'=>$_legOriginLocationId,'LegDestination'=>$_legDestination,'LegDestinationLocationId'=>$_legDestinationLocationId,'RateType'=>$_rateType,'RateScale'=>$_rateScale,'RateZone'=>$_rateZone,'PricingCode'=>$_pricingCode,'RatedWeightMethod'=>$_ratedWeightMethod,'MinimumChargeType'=>$_minimumChargeType,'CurrencyExchangeRate'=>$_currencyExchangeRate,'SpecialRatingApplied'=>$_specialRatingApplied,'DimDivisor'=>$_dimDivisor,'DimDivisorType'=>$_dimDivisorType,'FuelSurchargePercent'=>$_fuelSurchargePercent,'TotalBillingWeight'=>$_totalBillingWeight,'TotalDimWeight'=>$_totalDimWeight,'TotalBaseCharge'=>$_totalBaseCharge,'TotalFreightDiscounts'=>$_totalFreightDiscounts,'TotalNetFreight'=>$_totalNetFreight,'TotalSurcharges'=>$_totalSurcharges,'TotalNetFedExCharge'=>$_totalNetFedExCharge,'TotalTaxes'=>$_totalTaxes,'TotalNetCharge'=>$_totalNetCharge,'TotalRebates'=>$_totalRebates,'TotalDutiesAndTaxes'=>$_totalDutiesAndTaxes,'TotalNetChargeWithDutiesAndTaxes'=>$_totalNetChargeWithDutiesAndTaxes,'FreightRateDetail'=>$_freightRateDetail,'FreightDiscounts'=>$_freightDiscounts,'Rebates'=>$_rebates,'Surcharges'=>$_surcharges,'Taxes'=>$_taxes,'DutiesAndTaxes'=>$_dutiesAndTaxes,'VariableHandlingCharges'=>$_variableHandlingCharges,'TotalVariableHandlingCharges'=>$_totalVariableHandlingCharges));
	}
	/**
	 * Get LegDescription value
	 * @return string|null
	 */
	public function getLegDescription()
	{
		return $this->LegDescription;
	}
	/**
	 * Set LegDescription value
	 * @param string $_legDescription the LegDescription
	 * @return string
	 */
	public function setLegDescription($_legDescription)
	{
		return ($this->LegDescription = $_legDescription);
	}
	/**
	 * Get LegOrigin value
	 * @return MytestStructAddress|null
	 */
	public function getLegOrigin()
	{
		return $this->LegOrigin;
	}
	/**
	 * Set LegOrigin value
	 * @param MytestStructAddress $_legOrigin the LegOrigin
	 * @return MytestStructAddress
	 */
	public function setLegOrigin($_legOrigin)
	{
		return ($this->LegOrigin = $_legOrigin);
	}
	/**
	 * Get LegOriginLocationId value
	 * @return string|null
	 */
	public function getLegOriginLocationId()
	{
		return $this->LegOriginLocationId;
	}
	/**
	 * Set LegOriginLocationId value
	 * @param string $_legOriginLocationId the LegOriginLocationId
	 * @return string
	 */
	public function setLegOriginLocationId($_legOriginLocationId)
	{
		return ($this->LegOriginLocationId = $_legOriginLocationId);
	}
	/**
	 * Get LegDestination value
	 * @return MytestStructAddress|null
	 */
	public function getLegDestination()
	{
		return $this->LegDestination;
	}
	/**
	 * Set LegDestination value
	 * @param MytestStructAddress $_legDestination the LegDestination
	 * @return MytestStructAddress
	 */
	public function setLegDestination($_legDestination)
	{
		return ($this->LegDestination = $_legDestination);
	}
	/**
	 * Get LegDestinationLocationId value
	 * @return string|null
	 */
	public function getLegDestinationLocationId()
	{
		return $this->LegDestinationLocationId;
	}
	/**
	 * Set LegDestinationLocationId value
	 * @param string $_legDestinationLocationId the LegDestinationLocationId
	 * @return string
	 */
	public function setLegDestinationLocationId($_legDestinationLocationId)
	{
		return ($this->LegDestinationLocationId = $_legDestinationLocationId);
	}
	/**
	 * Get RateType value
	 * @return MytestEnumReturnedRateType|null
	 */
	public function getRateType()
	{
		return $this->RateType;
	}
	/**
	 * Set RateType value
	 * @uses MytestEnumReturnedRateType::valueIsValid()
	 * @param MytestEnumReturnedRateType $_rateType the RateType
	 * @return MytestEnumReturnedRateType
	 */
	public function setRateType($_rateType)
	{
		if(!MytestEnumReturnedRateType::valueIsValid($_rateType))
		{
			return false;
		}
		return ($this->RateType = $_rateType);
	}
	/**
	 * Get RateScale value
	 * @return string|null
	 */
	public function getRateScale()
	{
		return $this->RateScale;
	}
	/**
	 * Set RateScale value
	 * @param string $_rateScale the RateScale
	 * @return string
	 */
	public function setRateScale($_rateScale)
	{
		return ($this->RateScale = $_rateScale);
	}
	/**
	 * Get RateZone value
	 * @return string|null
	 */
	public function getRateZone()
	{
		return $this->RateZone;
	}
	/**
	 * Set RateZone value
	 * @param string $_rateZone the RateZone
	 * @return string
	 */
	public function setRateZone($_rateZone)
	{
		return ($this->RateZone = $_rateZone);
	}
	/**
	 * Get PricingCode value
	 * @return MytestEnumPricingCodeType|null
	 */
	public function getPricingCode()
	{
		return $this->PricingCode;
	}
	/**
	 * Set PricingCode value
	 * @uses MytestEnumPricingCodeType::valueIsValid()
	 * @param MytestEnumPricingCodeType $_pricingCode the PricingCode
	 * @return MytestEnumPricingCodeType
	 */
	public function setPricingCode($_pricingCode)
	{
		if(!MytestEnumPricingCodeType::valueIsValid($_pricingCode))
		{
			return false;
		}
		return ($this->PricingCode = $_pricingCode);
	}
	/**
	 * Get RatedWeightMethod value
	 * @return MytestEnumRatedWeightMethod|null
	 */
	public function getRatedWeightMethod()
	{
		return $this->RatedWeightMethod;
	}
	/**
	 * Set RatedWeightMethod value
	 * @uses MytestEnumRatedWeightMethod::valueIsValid()
	 * @param MytestEnumRatedWeightMethod $_ratedWeightMethod the RatedWeightMethod
	 * @return MytestEnumRatedWeightMethod
	 */
	public function setRatedWeightMethod($_ratedWeightMethod)
	{
		if(!MytestEnumRatedWeightMethod::valueIsValid($_ratedWeightMethod))
		{
			return false;
		}
		return ($this->RatedWeightMethod = $_ratedWeightMethod);
	}
	/**
	 * Get MinimumChargeType value
	 * @return MytestEnumMinimumChargeType|null
	 */
	public function getMinimumChargeType()
	{
		return $this->MinimumChargeType;
	}
	/**
	 * Set MinimumChargeType value
	 * @uses MytestEnumMinimumChargeType::valueIsValid()
	 * @param MytestEnumMinimumChargeType $_minimumChargeType the MinimumChargeType
	 * @return MytestEnumMinimumChargeType
	 */
	public function setMinimumChargeType($_minimumChargeType)
	{
		if(!MytestEnumMinimumChargeType::valueIsValid($_minimumChargeType))
		{
			return false;
		}
		return ($this->MinimumChargeType = $_minimumChargeType);
	}
	/**
	 * Get CurrencyExchangeRate value
	 * @return MytestStructCurrencyExchangeRate|null
	 */
	public function getCurrencyExchangeRate()
	{
		return $this->CurrencyExchangeRate;
	}
	/**
	 * Set CurrencyExchangeRate value
	 * @param MytestStructCurrencyExchangeRate $_currencyExchangeRate the CurrencyExchangeRate
	 * @return MytestStructCurrencyExchangeRate
	 */
	public function setCurrencyExchangeRate($_currencyExchangeRate)
	{
		return ($this->CurrencyExchangeRate = $_currencyExchangeRate);
	}
	/**
	 * Get SpecialRatingApplied value
	 * @return MytestEnumSpecialRatingAppliedType|null
	 */
	public function getSpecialRatingApplied()
	{
		return $this->SpecialRatingApplied;
	}
	/**
	 * Set SpecialRatingApplied value
	 * @uses MytestEnumSpecialRatingAppliedType::valueIsValid()
	 * @param MytestEnumSpecialRatingAppliedType $_specialRatingApplied the SpecialRatingApplied
	 * @return MytestEnumSpecialRatingAppliedType
	 */
	public function setSpecialRatingApplied($_specialRatingApplied)
	{
		if(!MytestEnumSpecialRatingAppliedType::valueIsValid($_specialRatingApplied))
		{
			return false;
		}
		return ($this->SpecialRatingApplied = $_specialRatingApplied);
	}
	/**
	 * Get DimDivisor value
	 * @return nonNegativeInteger|null
	 */
	public function getDimDivisor()
	{
		return $this->DimDivisor;
	}
	/**
	 * Set DimDivisor value
	 * @param nonNegativeInteger $_dimDivisor the DimDivisor
	 * @return nonNegativeInteger
	 */
	public function setDimDivisor($_dimDivisor)
	{
		return ($this->DimDivisor = $_dimDivisor);
	}
	/**
	 * Get DimDivisorType value
	 * @return MytestEnumRateDimensionalDivisorType|null
	 */
	public function getDimDivisorType()
	{
		return $this->DimDivisorType;
	}
	/**
	 * Set DimDivisorType value
	 * @uses MytestEnumRateDimensionalDivisorType::valueIsValid()
	 * @param MytestEnumRateDimensionalDivisorType $_dimDivisorType the DimDivisorType
	 * @return MytestEnumRateDimensionalDivisorType
	 */
	public function setDimDivisorType($_dimDivisorType)
	{
		if(!MytestEnumRateDimensionalDivisorType::valueIsValid($_dimDivisorType))
		{
			return false;
		}
		return ($this->DimDivisorType = $_dimDivisorType);
	}
	/**
	 * Get FuelSurchargePercent value
	 * @return decimal|null
	 */
	public function getFuelSurchargePercent()
	{
		return $this->FuelSurchargePercent;
	}
	/**
	 * Set FuelSurchargePercent value
	 * @param decimal $_fuelSurchargePercent the FuelSurchargePercent
	 * @return decimal
	 */
	public function setFuelSurchargePercent($_fuelSurchargePercent)
	{
		return ($this->FuelSurchargePercent = $_fuelSurchargePercent);
	}
	/**
	 * Get TotalBillingWeight value
	 * @return MytestStructWeight|null
	 */
	public function getTotalBillingWeight()
	{
		return $this->TotalBillingWeight;
	}
	/**
	 * Set TotalBillingWeight value
	 * @param MytestStructWeight $_totalBillingWeight the TotalBillingWeight
	 * @return MytestStructWeight
	 */
	public function setTotalBillingWeight($_totalBillingWeight)
	{
		return ($this->TotalBillingWeight = $_totalBillingWeight);
	}
	/**
	 * Get TotalDimWeight value
	 * @return MytestStructWeight|null
	 */
	public function getTotalDimWeight()
	{
		return $this->TotalDimWeight;
	}
	/**
	 * Set TotalDimWeight value
	 * @param MytestStructWeight $_totalDimWeight the TotalDimWeight
	 * @return MytestStructWeight
	 */
	public function setTotalDimWeight($_totalDimWeight)
	{
		return ($this->TotalDimWeight = $_totalDimWeight);
	}
	/**
	 * Get TotalBaseCharge value
	 * @return MytestStructMoney|null
	 */
	public function getTotalBaseCharge()
	{
		return $this->TotalBaseCharge;
	}
	/**
	 * Set TotalBaseCharge value
	 * @param MytestStructMoney $_totalBaseCharge the TotalBaseCharge
	 * @return MytestStructMoney
	 */
	public function setTotalBaseCharge($_totalBaseCharge)
	{
		return ($this->TotalBaseCharge = $_totalBaseCharge);
	}
	/**
	 * Get TotalFreightDiscounts value
	 * @return MytestStructMoney|null
	 */
	public function getTotalFreightDiscounts()
	{
		return $this->TotalFreightDiscounts;
	}
	/**
	 * Set TotalFreightDiscounts value
	 * @param MytestStructMoney $_totalFreightDiscounts the TotalFreightDiscounts
	 * @return MytestStructMoney
	 */
	public function setTotalFreightDiscounts($_totalFreightDiscounts)
	{
		return ($this->TotalFreightDiscounts = $_totalFreightDiscounts);
	}
	/**
	 * Get TotalNetFreight value
	 * @return MytestStructMoney|null
	 */
	public function getTotalNetFreight()
	{
		return $this->TotalNetFreight;
	}
	/**
	 * Set TotalNetFreight value
	 * @param MytestStructMoney $_totalNetFreight the TotalNetFreight
	 * @return MytestStructMoney
	 */
	public function setTotalNetFreight($_totalNetFreight)
	{
		return ($this->TotalNetFreight = $_totalNetFreight);
	}
	/**
	 * Get TotalSurcharges value
	 * @return MytestStructMoney|null
	 */
	public function getTotalSurcharges()
	{
		return $this->TotalSurcharges;
	}
	/**
	 * Set TotalSurcharges value
	 * @param MytestStructMoney $_totalSurcharges the TotalSurcharges
	 * @return MytestStructMoney
	 */
	public function setTotalSurcharges($_totalSurcharges)
	{
		return ($this->TotalSurcharges = $_totalSurcharges);
	}
	/**
	 * Get TotalNetFedExCharge value
	 * @return MytestStructMoney|null
	 */
	public function getTotalNetFedExCharge()
	{
		return $this->TotalNetFedExCharge;
	}
	/**
	 * Set TotalNetFedExCharge value
	 * @param MytestStructMoney $_totalNetFedExCharge the TotalNetFedExCharge
	 * @return MytestStructMoney
	 */
	public function setTotalNetFedExCharge($_totalNetFedExCharge)
	{
		return ($this->TotalNetFedExCharge = $_totalNetFedExCharge);
	}
	/**
	 * Get TotalTaxes value
	 * @return MytestStructMoney|null
	 */
	public function getTotalTaxes()
	{
		return $this->TotalTaxes;
	}
	/**
	 * Set TotalTaxes value
	 * @param MytestStructMoney $_totalTaxes the TotalTaxes
	 * @return MytestStructMoney
	 */
	public function setTotalTaxes($_totalTaxes)
	{
		return ($this->TotalTaxes = $_totalTaxes);
	}
	/**
	 * Get TotalNetCharge value
	 * @return MytestStructMoney|null
	 */
	public function getTotalNetCharge()
	{
		return $this->TotalNetCharge;
	}
	/**
	 * Set TotalNetCharge value
	 * @param MytestStructMoney $_totalNetCharge the TotalNetCharge
	 * @return MytestStructMoney
	 */
	public function setTotalNetCharge($_totalNetCharge)
	{
		return ($this->TotalNetCharge = $_totalNetCharge);
	}
	/**
	 * Get TotalRebates value
	 * @return MytestStructMoney|null
	 */
	public function getTotalRebates()
	{
		return $this->TotalRebates;
	}
	/**
	 * Set TotalRebates value
	 * @param MytestStructMoney $_totalRebates the TotalRebates
	 * @return MytestStructMoney
	 */
	public function setTotalRebates($_totalRebates)
	{
		return ($this->TotalRebates = $_totalRebates);
	}
	/**
	 * Get TotalDutiesAndTaxes value
	 * @return MytestStructMoney|null
	 */
	public function getTotalDutiesAndTaxes()
	{
		return $this->TotalDutiesAndTaxes;
	}
	/**
	 * Set TotalDutiesAndTaxes value
	 * @param MytestStructMoney $_totalDutiesAndTaxes the TotalDutiesAndTaxes
	 * @return MytestStructMoney
	 */
	public function setTotalDutiesAndTaxes($_totalDutiesAndTaxes)
	{
		return ($this->TotalDutiesAndTaxes = $_totalDutiesAndTaxes);
	}
	/**
	 * Get TotalNetChargeWithDutiesAndTaxes value
	 * @return MytestStructMoney|null
	 */
	public function getTotalNetChargeWithDutiesAndTaxes()
	{
		return $this->TotalNetChargeWithDutiesAndTaxes;
	}
	/**
	 * Set TotalNetChargeWithDutiesAndTaxes value
	 * @param MytestStructMoney $_totalNetChargeWithDutiesAndTaxes the TotalNetChargeWithDutiesAndTaxes
	 * @return MytestStructMoney
	 */
	public function setTotalNetChargeWithDutiesAndTaxes($_totalNetChargeWithDutiesAndTaxes)
	{
		return ($this->TotalNetChargeWithDutiesAndTaxes = $_totalNetChargeWithDutiesAndTaxes);
	}
	/**
	 * Get FreightRateDetail value
	 * @return MytestStructFreightRateDetail|null
	 */
	public function getFreightRateDetail()
	{
		return $this->FreightRateDetail;
	}
	/**
	 * Set FreightRateDetail value
	 * @param MytestStructFreightRateDetail $_freightRateDetail the FreightRateDetail
	 * @return MytestStructFreightRateDetail
	 */
	public function setFreightRateDetail($_freightRateDetail)
	{
		return ($this->FreightRateDetail = $_freightRateDetail);
	}
	/**
	 * Get FreightDiscounts value
	 * @return MytestStructRateDiscount|null
	 */
	public function getFreightDiscounts()
	{
		return $this->FreightDiscounts;
	}
	/**
	 * Set FreightDiscounts value
	 * @param MytestStructRateDiscount $_freightDiscounts the FreightDiscounts
	 * @return MytestStructRateDiscount
	 */
	public function setFreightDiscounts($_freightDiscounts)
	{
		return ($this->FreightDiscounts = $_freightDiscounts);
	}
	/**
	 * Get Rebates value
	 * @return MytestStructRebate|null
	 */
	public function getRebates()
	{
		return $this->Rebates;
	}
	/**
	 * Set Rebates value
	 * @param MytestStructRebate $_rebates the Rebates
	 * @return MytestStructRebate
	 */
	public function setRebates($_rebates)
	{
		return ($this->Rebates = $_rebates);
	}
	/**
	 * Get Surcharges value
	 * @return MytestStructSurcharge|null
	 */
	public function getSurcharges()
	{
		return $this->Surcharges;
	}
	/**
	 * Set Surcharges value
	 * @param MytestStructSurcharge $_surcharges the Surcharges
	 * @return MytestStructSurcharge
	 */
	public function setSurcharges($_surcharges)
	{
		return ($this->Surcharges = $_surcharges);
	}
	/**
	 * Get Taxes value
	 * @return MytestStructTax|null
	 */
	public function getTaxes()
	{
		return $this->Taxes;
	}
	/**
	 * Set Taxes value
	 * @param MytestStructTax $_taxes the Taxes
	 * @return MytestStructTax
	 */
	public function setTaxes($_taxes)
	{
		return ($this->Taxes = $_taxes);
	}
	/**
	 * Get DutiesAndTaxes value
	 * @return MytestStructEdtCommodityTax|null
	 */
	public function getDutiesAndTaxes()
	{
		return $this->DutiesAndTaxes;
	}
	/**
	 * Set DutiesAndTaxes value
	 * @param MytestStructEdtCommodityTax $_dutiesAndTaxes the DutiesAndTaxes
	 * @return MytestStructEdtCommodityTax
	 */
	public function setDutiesAndTaxes($_dutiesAndTaxes)
	{
		return ($this->DutiesAndTaxes = $_dutiesAndTaxes);
	}
	/**
	 * Get VariableHandlingCharges value
	 * @return MytestStructVariableHandlingCharges|null
	 */
	public function getVariableHandlingCharges()
	{
		return $this->VariableHandlingCharges;
	}
	/**
	 * Set VariableHandlingCharges value
	 * @param MytestStructVariableHandlingCharges $_variableHandlingCharges the VariableHandlingCharges
	 * @return MytestStructVariableHandlingCharges
	 */
	public function setVariableHandlingCharges($_variableHandlingCharges)
	{
		return ($this->VariableHandlingCharges = $_variableHandlingCharges);
	}
	/**
	 * Get TotalVariableHandlingCharges value
	 * @return MytestStructVariableHandlingCharges|null
	 */
	public function getTotalVariableHandlingCharges()
	{
		return $this->TotalVariableHandlingCharges;
	}
	/**
	 * Set TotalVariableHandlingCharges value
	 * @param MytestStructVariableHandlingCharges $_totalVariableHandlingCharges the TotalVariableHandlingCharges
	 * @return MytestStructVariableHandlingCharges
	 */
	public function setTotalVariableHandlingCharges($_totalVariableHandlingCharges)
	{
		return ($this->TotalVariableHandlingCharges = $_totalVariableHandlingCharges);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructShipmentLegRateDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>