<?php
/**
 * File for class MytestStructShipmentConfigurationData
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructShipmentConfigurationData originally named ShipmentConfigurationData
 * Documentation : Specifies data structures that may be re-used multiple times with s single shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructShipmentConfigurationData extends MytestWsdlClass
{
	/**
	 * The DangerousGoodsPackageConfigurations
	 * Meta informations extracted from the WSDL
	 * - documentation : Specifies the data that is common to dangerous goods packages in the shipment. This is populated when the shipment contains packages with identical dangerous goods commodities.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructDangerousGoodsDetail
	 */
	public $DangerousGoodsPackageConfigurations;
	/**
	 * Constructor method for ShipmentConfigurationData
	 * @see parent::__construct()
	 * @param MytestStructDangerousGoodsDetail $_dangerousGoodsPackageConfigurations
	 * @return MytestStructShipmentConfigurationData
	 */
	public function __construct($_dangerousGoodsPackageConfigurations = NULL)
	{
		parent::__construct(array('DangerousGoodsPackageConfigurations'=>$_dangerousGoodsPackageConfigurations));
	}
	/**
	 * Get DangerousGoodsPackageConfigurations value
	 * @return MytestStructDangerousGoodsDetail|null
	 */
	public function getDangerousGoodsPackageConfigurations()
	{
		return $this->DangerousGoodsPackageConfigurations;
	}
	/**
	 * Set DangerousGoodsPackageConfigurations value
	 * @param MytestStructDangerousGoodsDetail $_dangerousGoodsPackageConfigurations the DangerousGoodsPackageConfigurations
	 * @return MytestStructDangerousGoodsDetail
	 */
	public function setDangerousGoodsPackageConfigurations($_dangerousGoodsPackageConfigurations)
	{
		return ($this->DangerousGoodsPackageConfigurations = $_dangerousGoodsPackageConfigurations);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructShipmentConfigurationData
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>