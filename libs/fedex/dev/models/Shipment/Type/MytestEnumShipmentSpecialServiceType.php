<?php
/**
 * File for class MytestEnumShipmentSpecialServiceType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumShipmentSpecialServiceType originally named ShipmentSpecialServiceType
 * Documentation : Identifies the collection of special service offered by FedEx.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumShipmentSpecialServiceType extends MytestWsdlClass
{
	/**
	 * Constant for value 'BROKER_SELECT_OPTION'
	 * @return string 'BROKER_SELECT_OPTION'
	 */
	const VALUE_BROKER_SELECT_OPTION = 'BROKER_SELECT_OPTION';
	/**
	 * Constant for value 'CALL_BEFORE_DELIVERY'
	 * @return string 'CALL_BEFORE_DELIVERY'
	 */
	const VALUE_CALL_BEFORE_DELIVERY = 'CALL_BEFORE_DELIVERY';
	/**
	 * Constant for value 'COD'
	 * @return string 'COD'
	 */
	const VALUE_COD = 'COD';
	/**
	 * Constant for value 'CUSTOM_DELIVERY_WINDOW'
	 * @return string 'CUSTOM_DELIVERY_WINDOW'
	 */
	const VALUE_CUSTOM_DELIVERY_WINDOW = 'CUSTOM_DELIVERY_WINDOW';
	/**
	 * Constant for value 'DANGEROUS_GOODS'
	 * @return string 'DANGEROUS_GOODS'
	 */
	const VALUE_DANGEROUS_GOODS = 'DANGEROUS_GOODS';
	/**
	 * Constant for value 'DO_NOT_BREAK_DOWN_PALLETS'
	 * @return string 'DO_NOT_BREAK_DOWN_PALLETS'
	 */
	const VALUE_DO_NOT_BREAK_DOWN_PALLETS = 'DO_NOT_BREAK_DOWN_PALLETS';
	/**
	 * Constant for value 'DO_NOT_STACK_PALLETS'
	 * @return string 'DO_NOT_STACK_PALLETS'
	 */
	const VALUE_DO_NOT_STACK_PALLETS = 'DO_NOT_STACK_PALLETS';
	/**
	 * Constant for value 'DRY_ICE'
	 * @return string 'DRY_ICE'
	 */
	const VALUE_DRY_ICE = 'DRY_ICE';
	/**
	 * Constant for value 'EAST_COAST_SPECIAL'
	 * @return string 'EAST_COAST_SPECIAL'
	 */
	const VALUE_EAST_COAST_SPECIAL = 'EAST_COAST_SPECIAL';
	/**
	 * Constant for value 'ELECTRONIC_TRADE_DOCUMENTS'
	 * @return string 'ELECTRONIC_TRADE_DOCUMENTS'
	 */
	const VALUE_ELECTRONIC_TRADE_DOCUMENTS = 'ELECTRONIC_TRADE_DOCUMENTS';
	/**
	 * Constant for value 'EMAIL_NOTIFICATION'
	 * @return string 'EMAIL_NOTIFICATION'
	 */
	const VALUE_EMAIL_NOTIFICATION = 'EMAIL_NOTIFICATION';
	/**
	 * Constant for value 'EXTREME_LENGTH'
	 * @return string 'EXTREME_LENGTH'
	 */
	const VALUE_EXTREME_LENGTH = 'EXTREME_LENGTH';
	/**
	 * Constant for value 'FOOD'
	 * @return string 'FOOD'
	 */
	const VALUE_FOOD = 'FOOD';
	/**
	 * Constant for value 'FREIGHT_GUARANTEE'
	 * @return string 'FREIGHT_GUARANTEE'
	 */
	const VALUE_FREIGHT_GUARANTEE = 'FREIGHT_GUARANTEE';
	/**
	 * Constant for value 'FUTURE_DAY_SHIPMENT'
	 * @return string 'FUTURE_DAY_SHIPMENT'
	 */
	const VALUE_FUTURE_DAY_SHIPMENT = 'FUTURE_DAY_SHIPMENT';
	/**
	 * Constant for value 'HOLD_AT_LOCATION'
	 * @return string 'HOLD_AT_LOCATION'
	 */
	const VALUE_HOLD_AT_LOCATION = 'HOLD_AT_LOCATION';
	/**
	 * Constant for value 'HOME_DELIVERY_PREMIUM'
	 * @return string 'HOME_DELIVERY_PREMIUM'
	 */
	const VALUE_HOME_DELIVERY_PREMIUM = 'HOME_DELIVERY_PREMIUM';
	/**
	 * Constant for value 'INSIDE_DELIVERY'
	 * @return string 'INSIDE_DELIVERY'
	 */
	const VALUE_INSIDE_DELIVERY = 'INSIDE_DELIVERY';
	/**
	 * Constant for value 'INSIDE_PICKUP'
	 * @return string 'INSIDE_PICKUP'
	 */
	const VALUE_INSIDE_PICKUP = 'INSIDE_PICKUP';
	/**
	 * Constant for value 'INTERNATIONAL_CONTROLLED_EXPORT_SERVICE'
	 * @return string 'INTERNATIONAL_CONTROLLED_EXPORT_SERVICE'
	 */
	const VALUE_INTERNATIONAL_CONTROLLED_EXPORT_SERVICE = 'INTERNATIONAL_CONTROLLED_EXPORT_SERVICE';
	/**
	 * Constant for value 'INTERNATIONAL_TRAFFIC_IN_ARMS_REGULATIONS'
	 * @return string 'INTERNATIONAL_TRAFFIC_IN_ARMS_REGULATIONS'
	 */
	const VALUE_INTERNATIONAL_TRAFFIC_IN_ARMS_REGULATIONS = 'INTERNATIONAL_TRAFFIC_IN_ARMS_REGULATIONS';
	/**
	 * Constant for value 'LIFTGATE_DELIVERY'
	 * @return string 'LIFTGATE_DELIVERY'
	 */
	const VALUE_LIFTGATE_DELIVERY = 'LIFTGATE_DELIVERY';
	/**
	 * Constant for value 'LIFTGATE_PICKUP'
	 * @return string 'LIFTGATE_PICKUP'
	 */
	const VALUE_LIFTGATE_PICKUP = 'LIFTGATE_PICKUP';
	/**
	 * Constant for value 'LIMITED_ACCESS_DELIVERY'
	 * @return string 'LIMITED_ACCESS_DELIVERY'
	 */
	const VALUE_LIMITED_ACCESS_DELIVERY = 'LIMITED_ACCESS_DELIVERY';
	/**
	 * Constant for value 'LIMITED_ACCESS_PICKUP'
	 * @return string 'LIMITED_ACCESS_PICKUP'
	 */
	const VALUE_LIMITED_ACCESS_PICKUP = 'LIMITED_ACCESS_PICKUP';
	/**
	 * Constant for value 'PENDING_SHIPMENT'
	 * @return string 'PENDING_SHIPMENT'
	 */
	const VALUE_PENDING_SHIPMENT = 'PENDING_SHIPMENT';
	/**
	 * Constant for value 'POISON'
	 * @return string 'POISON'
	 */
	const VALUE_POISON = 'POISON';
	/**
	 * Constant for value 'PROTECTION_FROM_FREEZING'
	 * @return string 'PROTECTION_FROM_FREEZING'
	 */
	const VALUE_PROTECTION_FROM_FREEZING = 'PROTECTION_FROM_FREEZING';
	/**
	 * Constant for value 'RETURNS_CLEARANCE'
	 * @return string 'RETURNS_CLEARANCE'
	 */
	const VALUE_RETURNS_CLEARANCE = 'RETURNS_CLEARANCE';
	/**
	 * Constant for value 'RETURN_SHIPMENT'
	 * @return string 'RETURN_SHIPMENT'
	 */
	const VALUE_RETURN_SHIPMENT = 'RETURN_SHIPMENT';
	/**
	 * Constant for value 'SATURDAY_DELIVERY'
	 * @return string 'SATURDAY_DELIVERY'
	 */
	const VALUE_SATURDAY_DELIVERY = 'SATURDAY_DELIVERY';
	/**
	 * Constant for value 'SATURDAY_PICKUP'
	 * @return string 'SATURDAY_PICKUP'
	 */
	const VALUE_SATURDAY_PICKUP = 'SATURDAY_PICKUP';
	/**
	 * Constant for value 'TOP_LOAD'
	 * @return string 'TOP_LOAD'
	 */
	const VALUE_TOP_LOAD = 'TOP_LOAD';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_BROKER_SELECT_OPTION
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_CALL_BEFORE_DELIVERY
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_COD
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_CUSTOM_DELIVERY_WINDOW
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_DANGEROUS_GOODS
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_DO_NOT_BREAK_DOWN_PALLETS
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_DO_NOT_STACK_PALLETS
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_DRY_ICE
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_EAST_COAST_SPECIAL
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_ELECTRONIC_TRADE_DOCUMENTS
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_EMAIL_NOTIFICATION
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_EXTREME_LENGTH
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_FOOD
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_FREIGHT_GUARANTEE
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_FUTURE_DAY_SHIPMENT
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_HOLD_AT_LOCATION
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_HOME_DELIVERY_PREMIUM
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_INSIDE_DELIVERY
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_INSIDE_PICKUP
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_INTERNATIONAL_CONTROLLED_EXPORT_SERVICE
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_INTERNATIONAL_TRAFFIC_IN_ARMS_REGULATIONS
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_LIFTGATE_DELIVERY
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_LIFTGATE_PICKUP
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_LIMITED_ACCESS_DELIVERY
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_LIMITED_ACCESS_PICKUP
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_PENDING_SHIPMENT
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_POISON
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_PROTECTION_FROM_FREEZING
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_RETURNS_CLEARANCE
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_RETURN_SHIPMENT
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_SATURDAY_DELIVERY
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_SATURDAY_PICKUP
	 * @uses MytestEnumShipmentSpecialServiceType::VALUE_TOP_LOAD
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumShipmentSpecialServiceType::VALUE_BROKER_SELECT_OPTION,MytestEnumShipmentSpecialServiceType::VALUE_CALL_BEFORE_DELIVERY,MytestEnumShipmentSpecialServiceType::VALUE_COD,MytestEnumShipmentSpecialServiceType::VALUE_CUSTOM_DELIVERY_WINDOW,MytestEnumShipmentSpecialServiceType::VALUE_DANGEROUS_GOODS,MytestEnumShipmentSpecialServiceType::VALUE_DO_NOT_BREAK_DOWN_PALLETS,MytestEnumShipmentSpecialServiceType::VALUE_DO_NOT_STACK_PALLETS,MytestEnumShipmentSpecialServiceType::VALUE_DRY_ICE,MytestEnumShipmentSpecialServiceType::VALUE_EAST_COAST_SPECIAL,MytestEnumShipmentSpecialServiceType::VALUE_ELECTRONIC_TRADE_DOCUMENTS,MytestEnumShipmentSpecialServiceType::VALUE_EMAIL_NOTIFICATION,MytestEnumShipmentSpecialServiceType::VALUE_EXTREME_LENGTH,MytestEnumShipmentSpecialServiceType::VALUE_FOOD,MytestEnumShipmentSpecialServiceType::VALUE_FREIGHT_GUARANTEE,MytestEnumShipmentSpecialServiceType::VALUE_FUTURE_DAY_SHIPMENT,MytestEnumShipmentSpecialServiceType::VALUE_HOLD_AT_LOCATION,MytestEnumShipmentSpecialServiceType::VALUE_HOME_DELIVERY_PREMIUM,MytestEnumShipmentSpecialServiceType::VALUE_INSIDE_DELIVERY,MytestEnumShipmentSpecialServiceType::VALUE_INSIDE_PICKUP,MytestEnumShipmentSpecialServiceType::VALUE_INTERNATIONAL_CONTROLLED_EXPORT_SERVICE,MytestEnumShipmentSpecialServiceType::VALUE_INTERNATIONAL_TRAFFIC_IN_ARMS_REGULATIONS,MytestEnumShipmentSpecialServiceType::VALUE_LIFTGATE_DELIVERY,MytestEnumShipmentSpecialServiceType::VALUE_LIFTGATE_PICKUP,MytestEnumShipmentSpecialServiceType::VALUE_LIMITED_ACCESS_DELIVERY,MytestEnumShipmentSpecialServiceType::VALUE_LIMITED_ACCESS_PICKUP,MytestEnumShipmentSpecialServiceType::VALUE_PENDING_SHIPMENT,MytestEnumShipmentSpecialServiceType::VALUE_POISON,MytestEnumShipmentSpecialServiceType::VALUE_PROTECTION_FROM_FREEZING,MytestEnumShipmentSpecialServiceType::VALUE_RETURNS_CLEARANCE,MytestEnumShipmentSpecialServiceType::VALUE_RETURN_SHIPMENT,MytestEnumShipmentSpecialServiceType::VALUE_SATURDAY_DELIVERY,MytestEnumShipmentSpecialServiceType::VALUE_SATURDAY_PICKUP,MytestEnumShipmentSpecialServiceType::VALUE_TOP_LOAD));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>