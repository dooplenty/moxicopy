<?php
/**
 * File for class MytestEnumShipmentOnlyFieldsType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumShipmentOnlyFieldsType originally named ShipmentOnlyFieldsType
 * Documentation : These values identify which package-level data values will be provided at the shipment-level.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumShipmentOnlyFieldsType extends MytestWsdlClass
{
	/**
	 * Constant for value 'DIMENSIONS'
	 * @return string 'DIMENSIONS'
	 */
	const VALUE_DIMENSIONS = 'DIMENSIONS';
	/**
	 * Constant for value 'INSURED_VALUE'
	 * @return string 'INSURED_VALUE'
	 */
	const VALUE_INSURED_VALUE = 'INSURED_VALUE';
	/**
	 * Constant for value 'WEIGHT'
	 * @return string 'WEIGHT'
	 */
	const VALUE_WEIGHT = 'WEIGHT';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumShipmentOnlyFieldsType::VALUE_DIMENSIONS
	 * @uses MytestEnumShipmentOnlyFieldsType::VALUE_INSURED_VALUE
	 * @uses MytestEnumShipmentOnlyFieldsType::VALUE_WEIGHT
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumShipmentOnlyFieldsType::VALUE_DIMENSIONS,MytestEnumShipmentOnlyFieldsType::VALUE_INSURED_VALUE,MytestEnumShipmentOnlyFieldsType::VALUE_WEIGHT));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>