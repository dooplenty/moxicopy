<?php
/**
 * File for class MytestStructMeasure
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructMeasure originally named Measure
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructMeasure extends MytestWsdlClass
{
	/**
	 * The Quantity
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var decimal
	 */
	public $Quantity;
	/**
	 * The Units
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $Units;
	/**
	 * Constructor method for Measure
	 * @see parent::__construct()
	 * @param decimal $_quantity
	 * @param string $_units
	 * @return MytestStructMeasure
	 */
	public function __construct($_quantity = NULL,$_units = NULL)
	{
		parent::__construct(array('Quantity'=>$_quantity,'Units'=>$_units));
	}
	/**
	 * Get Quantity value
	 * @return decimal|null
	 */
	public function getQuantity()
	{
		return $this->Quantity;
	}
	/**
	 * Set Quantity value
	 * @param decimal $_quantity the Quantity
	 * @return decimal
	 */
	public function setQuantity($_quantity)
	{
		return ($this->Quantity = $_quantity);
	}
	/**
	 * Get Units value
	 * @return string|null
	 */
	public function getUnits()
	{
		return $this->Units;
	}
	/**
	 * Set Units value
	 * @param string $_units the Units
	 * @return string
	 */
	public function setUnits($_units)
	{
		return ($this->Units = $_units);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructMeasure
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>