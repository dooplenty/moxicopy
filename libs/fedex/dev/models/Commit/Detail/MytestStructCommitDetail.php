<?php
/**
 * File for class MytestStructCommitDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructCommitDetail originally named CommitDetail
 * Documentation : Information about the transit time and delivery commitment date and time.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructCommitDetail extends MytestWsdlClass
{
	/**
	 * The CommodityName
	 * Meta informations extracted from the WSDL
	 * - documentation : The Commodity applicable to this commitment.
	 * - minOccurs : 0
	 * @var string
	 */
	public $CommodityName;
	/**
	 * The ServiceType
	 * Meta informations extracted from the WSDL
	 * - documentation : The FedEx service type applicable to this commitment.
	 * - minOccurs : 0
	 * @var MytestEnumServiceType
	 */
	public $ServiceType;
	/**
	 * The AppliedOptions
	 * Meta informations extracted from the WSDL
	 * - documentation : Shows the specific combination of service options combined with the service type that produced this committment in the set returned to the caller.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumServiceOptionType
	 */
	public $AppliedOptions;
	/**
	 * The AppliedSubOptions
	 * Meta informations extracted from the WSDL
	 * - documentation : Supporting detail for applied options identified in preceding field.
	 * - minOccurs : 0
	 * @var MytestStructServiceSubOptionDetail
	 */
	public $AppliedSubOptions;
	/**
	 * The CommitTimestamp
	 * Meta informations extracted from the WSDL
	 * - documentation : THe delivery commitment date/time. Express Only.
	 * - minOccurs : 0
	 * @var dateTime
	 */
	public $CommitTimestamp;
	/**
	 * The DayOfWeek
	 * Meta informations extracted from the WSDL
	 * - documentation : The delivery commitment day of the week.
	 * - minOccurs : 0
	 * @var MytestEnumDayOfWeekType
	 */
	public $DayOfWeek;
	/**
	 * The TransitTime
	 * Meta informations extracted from the WSDL
	 * - documentation : The number of transit days; applies to Ground and LTL Freight; indicates minimum transit time for SmartPost.
	 * - minOccurs : 0
	 * @var MytestEnumTransitTimeType
	 */
	public $TransitTime;
	/**
	 * The MaximumTransitTime
	 * Meta informations extracted from the WSDL
	 * - documentation : Maximum number of transit days, for SmartPost shipments.
	 * - minOccurs : 0
	 * @var MytestEnumTransitTimeType
	 */
	public $MaximumTransitTime;
	/**
	 * The DestinationServiceArea
	 * Meta informations extracted from the WSDL
	 * - documentation : The service area code for the destination of this shipment. Express only.
	 * - minOccurs : 0
	 * @var string
	 */
	public $DestinationServiceArea;
	/**
	 * The BrokerAddress
	 * Meta informations extracted from the WSDL
	 * - documentation : The address of the broker to be used for this shipment.
	 * - minOccurs : 0
	 * @var MytestStructAddress
	 */
	public $BrokerAddress;
	/**
	 * The BrokerLocationId
	 * Meta informations extracted from the WSDL
	 * - documentation : The FedEx location identifier for the broker.
	 * - minOccurs : 0
	 * @var string
	 */
	public $BrokerLocationId;
	/**
	 * The BrokerCommitTimestamp
	 * Meta informations extracted from the WSDL
	 * - documentation : The delivery commitment date/time the shipment will arrive at the border.
	 * - minOccurs : 0
	 * @var dateTime
	 */
	public $BrokerCommitTimestamp;
	/**
	 * The BrokerCommitDayOfWeek
	 * Meta informations extracted from the WSDL
	 * - documentation : The delivery commitment day of the week the shipment will arrive at the border.
	 * - minOccurs : 0
	 * @var MytestEnumDayOfWeekType
	 */
	public $BrokerCommitDayOfWeek;
	/**
	 * The BrokerToDestinationDays
	 * Meta informations extracted from the WSDL
	 * - documentation : The number of days it will take for the shipment to make it from broker to destination
	 * - minOccurs : 0
	 * @var nonNegativeInteger
	 */
	public $BrokerToDestinationDays;
	/**
	 * The ProofOfDeliveryDate
	 * Meta informations extracted from the WSDL
	 * - documentation : The delivery commitment date for shipment served by GSP (Global Service Provider)
	 * - minOccurs : 0
	 * @var date
	 */
	public $ProofOfDeliveryDate;
	/**
	 * The ProofOfDeliveryDayOfWeek
	 * Meta informations extracted from the WSDL
	 * - documentation : The delivery commitment day of the week for the shipment served by GSP (Global Service Provider)
	 * - minOccurs : 0
	 * @var MytestEnumDayOfWeekType
	 */
	public $ProofOfDeliveryDayOfWeek;
	/**
	 * The CommitMessages
	 * Meta informations extracted from the WSDL
	 * - documentation : Messages concerning the ability to provide an accurate delivery commitment on an International commit quote. These could be messages providing information about why a commitment could not be returned or a successful message such as "REQUEST COMPLETED"
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructNotification
	 */
	public $CommitMessages;
	/**
	 * The DeliveryMessages
	 * Meta informations extracted from the WSDL
	 * - documentation : Messages concerning the delivery commitment on an International commit quote such as "0:00 A.M. IF NO CUSTOMS DELAY"
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var string
	 */
	public $DeliveryMessages;
	/**
	 * The DelayDetails
	 * Meta informations extracted from the WSDL
	 * - documentation : Information about why a shipment delivery is delayed and at what level (country/service etc.).
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestStructDelayDetail
	 */
	public $DelayDetails;
	/**
	 * The DocumentContent
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumInternationalDocumentContentType
	 */
	public $DocumentContent;
	/**
	 * The RequiredDocuments
	 * Meta informations extracted from the WSDL
	 * - documentation : Required documentation for this shipment.
	 * - maxOccurs : unbounded
	 * - minOccurs : 0
	 * @var MytestEnumRequiredShippingDocumentType
	 */
	public $RequiredDocuments;
	/**
	 * The FreightCommitDetail
	 * Meta informations extracted from the WSDL
	 * - documentation : Freight origin and destination city center information and total distance between origin and destination city centers.
	 * - minOccurs : 0
	 * @var MytestStructFreightCommitDetail
	 */
	public $FreightCommitDetail;
	/**
	 * Constructor method for CommitDetail
	 * @see parent::__construct()
	 * @param string $_commodityName
	 * @param MytestEnumServiceType $_serviceType
	 * @param MytestEnumServiceOptionType $_appliedOptions
	 * @param MytestStructServiceSubOptionDetail $_appliedSubOptions
	 * @param dateTime $_commitTimestamp
	 * @param MytestEnumDayOfWeekType $_dayOfWeek
	 * @param MytestEnumTransitTimeType $_transitTime
	 * @param MytestEnumTransitTimeType $_maximumTransitTime
	 * @param string $_destinationServiceArea
	 * @param MytestStructAddress $_brokerAddress
	 * @param string $_brokerLocationId
	 * @param dateTime $_brokerCommitTimestamp
	 * @param MytestEnumDayOfWeekType $_brokerCommitDayOfWeek
	 * @param nonNegativeInteger $_brokerToDestinationDays
	 * @param date $_proofOfDeliveryDate
	 * @param MytestEnumDayOfWeekType $_proofOfDeliveryDayOfWeek
	 * @param MytestStructNotification $_commitMessages
	 * @param string $_deliveryMessages
	 * @param MytestStructDelayDetail $_delayDetails
	 * @param MytestEnumInternationalDocumentContentType $_documentContent
	 * @param MytestEnumRequiredShippingDocumentType $_requiredDocuments
	 * @param MytestStructFreightCommitDetail $_freightCommitDetail
	 * @return MytestStructCommitDetail
	 */
	public function __construct($_commodityName = NULL,$_serviceType = NULL,$_appliedOptions = NULL,$_appliedSubOptions = NULL,$_commitTimestamp = NULL,$_dayOfWeek = NULL,$_transitTime = NULL,$_maximumTransitTime = NULL,$_destinationServiceArea = NULL,$_brokerAddress = NULL,$_brokerLocationId = NULL,$_brokerCommitTimestamp = NULL,$_brokerCommitDayOfWeek = NULL,$_brokerToDestinationDays = NULL,$_proofOfDeliveryDate = NULL,$_proofOfDeliveryDayOfWeek = NULL,$_commitMessages = NULL,$_deliveryMessages = NULL,$_delayDetails = NULL,$_documentContent = NULL,$_requiredDocuments = NULL,$_freightCommitDetail = NULL)
	{
		parent::__construct(array('CommodityName'=>$_commodityName,'ServiceType'=>$_serviceType,'AppliedOptions'=>$_appliedOptions,'AppliedSubOptions'=>$_appliedSubOptions,'CommitTimestamp'=>$_commitTimestamp,'DayOfWeek'=>$_dayOfWeek,'TransitTime'=>$_transitTime,'MaximumTransitTime'=>$_maximumTransitTime,'DestinationServiceArea'=>$_destinationServiceArea,'BrokerAddress'=>$_brokerAddress,'BrokerLocationId'=>$_brokerLocationId,'BrokerCommitTimestamp'=>$_brokerCommitTimestamp,'BrokerCommitDayOfWeek'=>$_brokerCommitDayOfWeek,'BrokerToDestinationDays'=>$_brokerToDestinationDays,'ProofOfDeliveryDate'=>$_proofOfDeliveryDate,'ProofOfDeliveryDayOfWeek'=>$_proofOfDeliveryDayOfWeek,'CommitMessages'=>$_commitMessages,'DeliveryMessages'=>$_deliveryMessages,'DelayDetails'=>$_delayDetails,'DocumentContent'=>$_documentContent,'RequiredDocuments'=>$_requiredDocuments,'FreightCommitDetail'=>$_freightCommitDetail));
	}
	/**
	 * Get CommodityName value
	 * @return string|null
	 */
	public function getCommodityName()
	{
		return $this->CommodityName;
	}
	/**
	 * Set CommodityName value
	 * @param string $_commodityName the CommodityName
	 * @return string
	 */
	public function setCommodityName($_commodityName)
	{
		return ($this->CommodityName = $_commodityName);
	}
	/**
	 * Get ServiceType value
	 * @return MytestEnumServiceType|null
	 */
	public function getServiceType()
	{
		return $this->ServiceType;
	}
	/**
	 * Set ServiceType value
	 * @uses MytestEnumServiceType::valueIsValid()
	 * @param MytestEnumServiceType $_serviceType the ServiceType
	 * @return MytestEnumServiceType
	 */
	public function setServiceType($_serviceType)
	{
		if(!MytestEnumServiceType::valueIsValid($_serviceType))
		{
			return false;
		}
		return ($this->ServiceType = $_serviceType);
	}
	/**
	 * Get AppliedOptions value
	 * @return MytestEnumServiceOptionType|null
	 */
	public function getAppliedOptions()
	{
		return $this->AppliedOptions;
	}
	/**
	 * Set AppliedOptions value
	 * @uses MytestEnumServiceOptionType::valueIsValid()
	 * @param MytestEnumServiceOptionType $_appliedOptions the AppliedOptions
	 * @return MytestEnumServiceOptionType
	 */
	public function setAppliedOptions($_appliedOptions)
	{
		if(!MytestEnumServiceOptionType::valueIsValid($_appliedOptions))
		{
			return false;
		}
		return ($this->AppliedOptions = $_appliedOptions);
	}
	/**
	 * Get AppliedSubOptions value
	 * @return MytestStructServiceSubOptionDetail|null
	 */
	public function getAppliedSubOptions()
	{
		return $this->AppliedSubOptions;
	}
	/**
	 * Set AppliedSubOptions value
	 * @param MytestStructServiceSubOptionDetail $_appliedSubOptions the AppliedSubOptions
	 * @return MytestStructServiceSubOptionDetail
	 */
	public function setAppliedSubOptions($_appliedSubOptions)
	{
		return ($this->AppliedSubOptions = $_appliedSubOptions);
	}
	/**
	 * Get CommitTimestamp value
	 * @return dateTime|null
	 */
	public function getCommitTimestamp()
	{
		return $this->CommitTimestamp;
	}
	/**
	 * Set CommitTimestamp value
	 * @param dateTime $_commitTimestamp the CommitTimestamp
	 * @return dateTime
	 */
	public function setCommitTimestamp($_commitTimestamp)
	{
		return ($this->CommitTimestamp = $_commitTimestamp);
	}
	/**
	 * Get DayOfWeek value
	 * @return MytestEnumDayOfWeekType|null
	 */
	public function getDayOfWeek()
	{
		return $this->DayOfWeek;
	}
	/**
	 * Set DayOfWeek value
	 * @uses MytestEnumDayOfWeekType::valueIsValid()
	 * @param MytestEnumDayOfWeekType $_dayOfWeek the DayOfWeek
	 * @return MytestEnumDayOfWeekType
	 */
	public function setDayOfWeek($_dayOfWeek)
	{
		if(!MytestEnumDayOfWeekType::valueIsValid($_dayOfWeek))
		{
			return false;
		}
		return ($this->DayOfWeek = $_dayOfWeek);
	}
	/**
	 * Get TransitTime value
	 * @return MytestEnumTransitTimeType|null
	 */
	public function getTransitTime()
	{
		return $this->TransitTime;
	}
	/**
	 * Set TransitTime value
	 * @uses MytestEnumTransitTimeType::valueIsValid()
	 * @param MytestEnumTransitTimeType $_transitTime the TransitTime
	 * @return MytestEnumTransitTimeType
	 */
	public function setTransitTime($_transitTime)
	{
		if(!MytestEnumTransitTimeType::valueIsValid($_transitTime))
		{
			return false;
		}
		return ($this->TransitTime = $_transitTime);
	}
	/**
	 * Get MaximumTransitTime value
	 * @return MytestEnumTransitTimeType|null
	 */
	public function getMaximumTransitTime()
	{
		return $this->MaximumTransitTime;
	}
	/**
	 * Set MaximumTransitTime value
	 * @uses MytestEnumTransitTimeType::valueIsValid()
	 * @param MytestEnumTransitTimeType $_maximumTransitTime the MaximumTransitTime
	 * @return MytestEnumTransitTimeType
	 */
	public function setMaximumTransitTime($_maximumTransitTime)
	{
		if(!MytestEnumTransitTimeType::valueIsValid($_maximumTransitTime))
		{
			return false;
		}
		return ($this->MaximumTransitTime = $_maximumTransitTime);
	}
	/**
	 * Get DestinationServiceArea value
	 * @return string|null
	 */
	public function getDestinationServiceArea()
	{
		return $this->DestinationServiceArea;
	}
	/**
	 * Set DestinationServiceArea value
	 * @param string $_destinationServiceArea the DestinationServiceArea
	 * @return string
	 */
	public function setDestinationServiceArea($_destinationServiceArea)
	{
		return ($this->DestinationServiceArea = $_destinationServiceArea);
	}
	/**
	 * Get BrokerAddress value
	 * @return MytestStructAddress|null
	 */
	public function getBrokerAddress()
	{
		return $this->BrokerAddress;
	}
	/**
	 * Set BrokerAddress value
	 * @param MytestStructAddress $_brokerAddress the BrokerAddress
	 * @return MytestStructAddress
	 */
	public function setBrokerAddress($_brokerAddress)
	{
		return ($this->BrokerAddress = $_brokerAddress);
	}
	/**
	 * Get BrokerLocationId value
	 * @return string|null
	 */
	public function getBrokerLocationId()
	{
		return $this->BrokerLocationId;
	}
	/**
	 * Set BrokerLocationId value
	 * @param string $_brokerLocationId the BrokerLocationId
	 * @return string
	 */
	public function setBrokerLocationId($_brokerLocationId)
	{
		return ($this->BrokerLocationId = $_brokerLocationId);
	}
	/**
	 * Get BrokerCommitTimestamp value
	 * @return dateTime|null
	 */
	public function getBrokerCommitTimestamp()
	{
		return $this->BrokerCommitTimestamp;
	}
	/**
	 * Set BrokerCommitTimestamp value
	 * @param dateTime $_brokerCommitTimestamp the BrokerCommitTimestamp
	 * @return dateTime
	 */
	public function setBrokerCommitTimestamp($_brokerCommitTimestamp)
	{
		return ($this->BrokerCommitTimestamp = $_brokerCommitTimestamp);
	}
	/**
	 * Get BrokerCommitDayOfWeek value
	 * @return MytestEnumDayOfWeekType|null
	 */
	public function getBrokerCommitDayOfWeek()
	{
		return $this->BrokerCommitDayOfWeek;
	}
	/**
	 * Set BrokerCommitDayOfWeek value
	 * @uses MytestEnumDayOfWeekType::valueIsValid()
	 * @param MytestEnumDayOfWeekType $_brokerCommitDayOfWeek the BrokerCommitDayOfWeek
	 * @return MytestEnumDayOfWeekType
	 */
	public function setBrokerCommitDayOfWeek($_brokerCommitDayOfWeek)
	{
		if(!MytestEnumDayOfWeekType::valueIsValid($_brokerCommitDayOfWeek))
		{
			return false;
		}
		return ($this->BrokerCommitDayOfWeek = $_brokerCommitDayOfWeek);
	}
	/**
	 * Get BrokerToDestinationDays value
	 * @return nonNegativeInteger|null
	 */
	public function getBrokerToDestinationDays()
	{
		return $this->BrokerToDestinationDays;
	}
	/**
	 * Set BrokerToDestinationDays value
	 * @param nonNegativeInteger $_brokerToDestinationDays the BrokerToDestinationDays
	 * @return nonNegativeInteger
	 */
	public function setBrokerToDestinationDays($_brokerToDestinationDays)
	{
		return ($this->BrokerToDestinationDays = $_brokerToDestinationDays);
	}
	/**
	 * Get ProofOfDeliveryDate value
	 * @return date|null
	 */
	public function getProofOfDeliveryDate()
	{
		return $this->ProofOfDeliveryDate;
	}
	/**
	 * Set ProofOfDeliveryDate value
	 * @param date $_proofOfDeliveryDate the ProofOfDeliveryDate
	 * @return date
	 */
	public function setProofOfDeliveryDate($_proofOfDeliveryDate)
	{
		return ($this->ProofOfDeliveryDate = $_proofOfDeliveryDate);
	}
	/**
	 * Get ProofOfDeliveryDayOfWeek value
	 * @return MytestEnumDayOfWeekType|null
	 */
	public function getProofOfDeliveryDayOfWeek()
	{
		return $this->ProofOfDeliveryDayOfWeek;
	}
	/**
	 * Set ProofOfDeliveryDayOfWeek value
	 * @uses MytestEnumDayOfWeekType::valueIsValid()
	 * @param MytestEnumDayOfWeekType $_proofOfDeliveryDayOfWeek the ProofOfDeliveryDayOfWeek
	 * @return MytestEnumDayOfWeekType
	 */
	public function setProofOfDeliveryDayOfWeek($_proofOfDeliveryDayOfWeek)
	{
		if(!MytestEnumDayOfWeekType::valueIsValid($_proofOfDeliveryDayOfWeek))
		{
			return false;
		}
		return ($this->ProofOfDeliveryDayOfWeek = $_proofOfDeliveryDayOfWeek);
	}
	/**
	 * Get CommitMessages value
	 * @return MytestStructNotification|null
	 */
	public function getCommitMessages()
	{
		return $this->CommitMessages;
	}
	/**
	 * Set CommitMessages value
	 * @param MytestStructNotification $_commitMessages the CommitMessages
	 * @return MytestStructNotification
	 */
	public function setCommitMessages($_commitMessages)
	{
		return ($this->CommitMessages = $_commitMessages);
	}
	/**
	 * Get DeliveryMessages value
	 * @return string|null
	 */
	public function getDeliveryMessages()
	{
		return $this->DeliveryMessages;
	}
	/**
	 * Set DeliveryMessages value
	 * @param string $_deliveryMessages the DeliveryMessages
	 * @return string
	 */
	public function setDeliveryMessages($_deliveryMessages)
	{
		return ($this->DeliveryMessages = $_deliveryMessages);
	}
	/**
	 * Get DelayDetails value
	 * @return MytestStructDelayDetail|null
	 */
	public function getDelayDetails()
	{
		return $this->DelayDetails;
	}
	/**
	 * Set DelayDetails value
	 * @param MytestStructDelayDetail $_delayDetails the DelayDetails
	 * @return MytestStructDelayDetail
	 */
	public function setDelayDetails($_delayDetails)
	{
		return ($this->DelayDetails = $_delayDetails);
	}
	/**
	 * Get DocumentContent value
	 * @return MytestEnumInternationalDocumentContentType|null
	 */
	public function getDocumentContent()
	{
		return $this->DocumentContent;
	}
	/**
	 * Set DocumentContent value
	 * @uses MytestEnumInternationalDocumentContentType::valueIsValid()
	 * @param MytestEnumInternationalDocumentContentType $_documentContent the DocumentContent
	 * @return MytestEnumInternationalDocumentContentType
	 */
	public function setDocumentContent($_documentContent)
	{
		if(!MytestEnumInternationalDocumentContentType::valueIsValid($_documentContent))
		{
			return false;
		}
		return ($this->DocumentContent = $_documentContent);
	}
	/**
	 * Get RequiredDocuments value
	 * @return MytestEnumRequiredShippingDocumentType|null
	 */
	public function getRequiredDocuments()
	{
		return $this->RequiredDocuments;
	}
	/**
	 * Set RequiredDocuments value
	 * @uses MytestEnumRequiredShippingDocumentType::valueIsValid()
	 * @param MytestEnumRequiredShippingDocumentType $_requiredDocuments the RequiredDocuments
	 * @return MytestEnumRequiredShippingDocumentType
	 */
	public function setRequiredDocuments($_requiredDocuments)
	{
		if(!MytestEnumRequiredShippingDocumentType::valueIsValid($_requiredDocuments))
		{
			return false;
		}
		return ($this->RequiredDocuments = $_requiredDocuments);
	}
	/**
	 * Get FreightCommitDetail value
	 * @return MytestStructFreightCommitDetail|null
	 */
	public function getFreightCommitDetail()
	{
		return $this->FreightCommitDetail;
	}
	/**
	 * Set FreightCommitDetail value
	 * @param MytestStructFreightCommitDetail $_freightCommitDetail the FreightCommitDetail
	 * @return MytestStructFreightCommitDetail
	 */
	public function setFreightCommitDetail($_freightCommitDetail)
	{
		return ($this->FreightCommitDetail = $_freightCommitDetail);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructCommitDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>