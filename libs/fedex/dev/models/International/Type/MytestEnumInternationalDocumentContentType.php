<?php
/**
 * File for class MytestEnumInternationalDocumentContentType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumInternationalDocumentContentType originally named InternationalDocumentContentType
 * Documentation : The type of International shipment.
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumInternationalDocumentContentType extends MytestWsdlClass
{
	/**
	 * Constant for value 'DOCUMENTS_ONLY'
	 * @return string 'DOCUMENTS_ONLY'
	 */
	const VALUE_DOCUMENTS_ONLY = 'DOCUMENTS_ONLY';
	/**
	 * Constant for value 'NON_DOCUMENTS'
	 * @return string 'NON_DOCUMENTS'
	 */
	const VALUE_NON_DOCUMENTS = 'NON_DOCUMENTS';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumInternationalDocumentContentType::VALUE_DOCUMENTS_ONLY
	 * @uses MytestEnumInternationalDocumentContentType::VALUE_NON_DOCUMENTS
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumInternationalDocumentContentType::VALUE_DOCUMENTS_ONLY,MytestEnumInternationalDocumentContentType::VALUE_NON_DOCUMENTS));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>