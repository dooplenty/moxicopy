<?php
/**
 * File for class MytestEnumInternationalControlledExportType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumInternationalControlledExportType originally named InternationalControlledExportType
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumInternationalControlledExportType extends MytestWsdlClass
{
	/**
	 * Constant for value 'DEA_036'
	 * @return string 'DEA_036'
	 */
	const VALUE_DEA_036 = 'DEA_036';
	/**
	 * Constant for value 'DEA_236'
	 * @return string 'DEA_236'
	 */
	const VALUE_DEA_236 = 'DEA_236';
	/**
	 * Constant for value 'DEA_486'
	 * @return string 'DEA_486'
	 */
	const VALUE_DEA_486 = 'DEA_486';
	/**
	 * Constant for value 'DSP_05'
	 * @return string 'DSP_05'
	 */
	const VALUE_DSP_05 = 'DSP_05';
	/**
	 * Constant for value 'DSP_61'
	 * @return string 'DSP_61'
	 */
	const VALUE_DSP_61 = 'DSP_61';
	/**
	 * Constant for value 'DSP_73'
	 * @return string 'DSP_73'
	 */
	const VALUE_DSP_73 = 'DSP_73';
	/**
	 * Constant for value 'DSP_85'
	 * @return string 'DSP_85'
	 */
	const VALUE_DSP_85 = 'DSP_85';
	/**
	 * Constant for value 'DSP_94'
	 * @return string 'DSP_94'
	 */
	const VALUE_DSP_94 = 'DSP_94';
	/**
	 * Constant for value 'DSP_LICENSE_AGREEMENT'
	 * @return string 'DSP_LICENSE_AGREEMENT'
	 */
	const VALUE_DSP_LICENSE_AGREEMENT = 'DSP_LICENSE_AGREEMENT';
	/**
	 * Constant for value 'FROM_FOREIGN_TRADE_ZONE'
	 * @return string 'FROM_FOREIGN_TRADE_ZONE'
	 */
	const VALUE_FROM_FOREIGN_TRADE_ZONE = 'FROM_FOREIGN_TRADE_ZONE';
	/**
	 * Constant for value 'WAREHOUSE_WITHDRAWAL'
	 * @return string 'WAREHOUSE_WITHDRAWAL'
	 */
	const VALUE_WAREHOUSE_WITHDRAWAL = 'WAREHOUSE_WITHDRAWAL';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumInternationalControlledExportType::VALUE_DEA_036
	 * @uses MytestEnumInternationalControlledExportType::VALUE_DEA_236
	 * @uses MytestEnumInternationalControlledExportType::VALUE_DEA_486
	 * @uses MytestEnumInternationalControlledExportType::VALUE_DSP_05
	 * @uses MytestEnumInternationalControlledExportType::VALUE_DSP_61
	 * @uses MytestEnumInternationalControlledExportType::VALUE_DSP_73
	 * @uses MytestEnumInternationalControlledExportType::VALUE_DSP_85
	 * @uses MytestEnumInternationalControlledExportType::VALUE_DSP_94
	 * @uses MytestEnumInternationalControlledExportType::VALUE_DSP_LICENSE_AGREEMENT
	 * @uses MytestEnumInternationalControlledExportType::VALUE_FROM_FOREIGN_TRADE_ZONE
	 * @uses MytestEnumInternationalControlledExportType::VALUE_WAREHOUSE_WITHDRAWAL
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumInternationalControlledExportType::VALUE_DEA_036,MytestEnumInternationalControlledExportType::VALUE_DEA_236,MytestEnumInternationalControlledExportType::VALUE_DEA_486,MytestEnumInternationalControlledExportType::VALUE_DSP_05,MytestEnumInternationalControlledExportType::VALUE_DSP_61,MytestEnumInternationalControlledExportType::VALUE_DSP_73,MytestEnumInternationalControlledExportType::VALUE_DSP_85,MytestEnumInternationalControlledExportType::VALUE_DSP_94,MytestEnumInternationalControlledExportType::VALUE_DSP_LICENSE_AGREEMENT,MytestEnumInternationalControlledExportType::VALUE_FROM_FOREIGN_TRADE_ZONE,MytestEnumInternationalControlledExportType::VALUE_WAREHOUSE_WITHDRAWAL));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>