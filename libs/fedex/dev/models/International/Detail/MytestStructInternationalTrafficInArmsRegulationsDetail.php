<?php
/**
 * File for class MytestStructInternationalTrafficInArmsRegulationsDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructInternationalTrafficInArmsRegulationsDetail originally named InternationalTrafficInArmsRegulationsDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructInternationalTrafficInArmsRegulationsDetail extends MytestWsdlClass
{
	/**
	 * The LicenseOrExemptionNumber
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $LicenseOrExemptionNumber;
	/**
	 * Constructor method for InternationalTrafficInArmsRegulationsDetail
	 * @see parent::__construct()
	 * @param string $_licenseOrExemptionNumber
	 * @return MytestStructInternationalTrafficInArmsRegulationsDetail
	 */
	public function __construct($_licenseOrExemptionNumber = NULL)
	{
		parent::__construct(array('LicenseOrExemptionNumber'=>$_licenseOrExemptionNumber));
	}
	/**
	 * Get LicenseOrExemptionNumber value
	 * @return string|null
	 */
	public function getLicenseOrExemptionNumber()
	{
		return $this->LicenseOrExemptionNumber;
	}
	/**
	 * Set LicenseOrExemptionNumber value
	 * @param string $_licenseOrExemptionNumber the LicenseOrExemptionNumber
	 * @return string
	 */
	public function setLicenseOrExemptionNumber($_licenseOrExemptionNumber)
	{
		return ($this->LicenseOrExemptionNumber = $_licenseOrExemptionNumber);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructInternationalTrafficInArmsRegulationsDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>