<?php
/**
 * File for class MytestStructInternationalControlledExportDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructInternationalControlledExportDetail originally named InternationalControlledExportDetail
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructInternationalControlledExportDetail extends MytestWsdlClass
{
	/**
	 * The Type
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumInternationalControlledExportType
	 */
	public $Type;
	/**
	 * The ForeignTradeZoneCode
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $ForeignTradeZoneCode;
	/**
	 * The EntryNumber
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $EntryNumber;
	/**
	 * The LicenseOrPermitNumber
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var string
	 */
	public $LicenseOrPermitNumber;
	/**
	 * The LicenseOrPermitExpirationDate
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var date
	 */
	public $LicenseOrPermitExpirationDate;
	/**
	 * Constructor method for InternationalControlledExportDetail
	 * @see parent::__construct()
	 * @param MytestEnumInternationalControlledExportType $_type
	 * @param string $_foreignTradeZoneCode
	 * @param string $_entryNumber
	 * @param string $_licenseOrPermitNumber
	 * @param date $_licenseOrPermitExpirationDate
	 * @return MytestStructInternationalControlledExportDetail
	 */
	public function __construct($_type = NULL,$_foreignTradeZoneCode = NULL,$_entryNumber = NULL,$_licenseOrPermitNumber = NULL,$_licenseOrPermitExpirationDate = NULL)
	{
		parent::__construct(array('Type'=>$_type,'ForeignTradeZoneCode'=>$_foreignTradeZoneCode,'EntryNumber'=>$_entryNumber,'LicenseOrPermitNumber'=>$_licenseOrPermitNumber,'LicenseOrPermitExpirationDate'=>$_licenseOrPermitExpirationDate));
	}
	/**
	 * Get Type value
	 * @return MytestEnumInternationalControlledExportType|null
	 */
	public function getType()
	{
		return $this->Type;
	}
	/**
	 * Set Type value
	 * @uses MytestEnumInternationalControlledExportType::valueIsValid()
	 * @param MytestEnumInternationalControlledExportType $_type the Type
	 * @return MytestEnumInternationalControlledExportType
	 */
	public function setType($_type)
	{
		if(!MytestEnumInternationalControlledExportType::valueIsValid($_type))
		{
			return false;
		}
		return ($this->Type = $_type);
	}
	/**
	 * Get ForeignTradeZoneCode value
	 * @return string|null
	 */
	public function getForeignTradeZoneCode()
	{
		return $this->ForeignTradeZoneCode;
	}
	/**
	 * Set ForeignTradeZoneCode value
	 * @param string $_foreignTradeZoneCode the ForeignTradeZoneCode
	 * @return string
	 */
	public function setForeignTradeZoneCode($_foreignTradeZoneCode)
	{
		return ($this->ForeignTradeZoneCode = $_foreignTradeZoneCode);
	}
	/**
	 * Get EntryNumber value
	 * @return string|null
	 */
	public function getEntryNumber()
	{
		return $this->EntryNumber;
	}
	/**
	 * Set EntryNumber value
	 * @param string $_entryNumber the EntryNumber
	 * @return string
	 */
	public function setEntryNumber($_entryNumber)
	{
		return ($this->EntryNumber = $_entryNumber);
	}
	/**
	 * Get LicenseOrPermitNumber value
	 * @return string|null
	 */
	public function getLicenseOrPermitNumber()
	{
		return $this->LicenseOrPermitNumber;
	}
	/**
	 * Set LicenseOrPermitNumber value
	 * @param string $_licenseOrPermitNumber the LicenseOrPermitNumber
	 * @return string
	 */
	public function setLicenseOrPermitNumber($_licenseOrPermitNumber)
	{
		return ($this->LicenseOrPermitNumber = $_licenseOrPermitNumber);
	}
	/**
	 * Get LicenseOrPermitExpirationDate value
	 * @return date|null
	 */
	public function getLicenseOrPermitExpirationDate()
	{
		return $this->LicenseOrPermitExpirationDate;
	}
	/**
	 * Set LicenseOrPermitExpirationDate value
	 * @param date $_licenseOrPermitExpirationDate the LicenseOrPermitExpirationDate
	 * @return date
	 */
	public function setLicenseOrPermitExpirationDate($_licenseOrPermitExpirationDate)
	{
		return ($this->LicenseOrPermitExpirationDate = $_licenseOrPermitExpirationDate);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructInternationalControlledExportDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>