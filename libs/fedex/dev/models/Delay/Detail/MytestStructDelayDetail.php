<?php
/**
 * File for class MytestStructDelayDetail
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestStructDelayDetail originally named DelayDetail
 * Documentation : Information about why a shipment delivery is delayed and at what level( country/service etc.).
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Structs
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestStructDelayDetail extends MytestWsdlClass
{
	/**
	 * The Date
	 * Meta informations extracted from the WSDL
	 * - documentation : The date of the delay
	 * - minOccurs : 0
	 * @var date
	 */
	public $Date;
	/**
	 * The DayOfWeek
	 * Meta informations extracted from the WSDL
	 * - minOccurs : 0
	 * @var MytestEnumDayOfWeekType
	 */
	public $DayOfWeek;
	/**
	 * The Level
	 * Meta informations extracted from the WSDL
	 * - documentation : The attribute of the shipment that caused the delay(e.g. Country, City, LocationId, Zip, service area, special handling )
	 * - minOccurs : 0
	 * @var MytestEnumDelayLevelType
	 */
	public $Level;
	/**
	 * The Point
	 * Meta informations extracted from the WSDL
	 * - documentation : The point where the delay is occurring (e.g. Origin, Destination, Broker location)
	 * - minOccurs : 0
	 * @var MytestEnumDelayPointType
	 */
	public $Point;
	/**
	 * The Type
	 * Meta informations extracted from the WSDL
	 * - documentation : The reason for the delay (e.g. holiday, weekend, etc.).
	 * - minOccurs : 0
	 * @var MytestEnumCommitmentDelayType
	 */
	public $Type;
	/**
	 * The Description
	 * Meta informations extracted from the WSDL
	 * - documentation : The name of the holiday in that country that is causing the delay.
	 * - minOccurs : 0
	 * @var string
	 */
	public $Description;
	/**
	 * Constructor method for DelayDetail
	 * @see parent::__construct()
	 * @param date $_date
	 * @param MytestEnumDayOfWeekType $_dayOfWeek
	 * @param MytestEnumDelayLevelType $_level
	 * @param MytestEnumDelayPointType $_point
	 * @param MytestEnumCommitmentDelayType $_type
	 * @param string $_description
	 * @return MytestStructDelayDetail
	 */
	public function __construct($_date = NULL,$_dayOfWeek = NULL,$_level = NULL,$_point = NULL,$_type = NULL,$_description = NULL)
	{
		parent::__construct(array('Date'=>$_date,'DayOfWeek'=>$_dayOfWeek,'Level'=>$_level,'Point'=>$_point,'Type'=>$_type,'Description'=>$_description));
	}
	/**
	 * Get Date value
	 * @return date|null
	 */
	public function getDate()
	{
		return $this->Date;
	}
	/**
	 * Set Date value
	 * @param date $_date the Date
	 * @return date
	 */
	public function setDate($_date)
	{
		return ($this->Date = $_date);
	}
	/**
	 * Get DayOfWeek value
	 * @return MytestEnumDayOfWeekType|null
	 */
	public function getDayOfWeek()
	{
		return $this->DayOfWeek;
	}
	/**
	 * Set DayOfWeek value
	 * @uses MytestEnumDayOfWeekType::valueIsValid()
	 * @param MytestEnumDayOfWeekType $_dayOfWeek the DayOfWeek
	 * @return MytestEnumDayOfWeekType
	 */
	public function setDayOfWeek($_dayOfWeek)
	{
		if(!MytestEnumDayOfWeekType::valueIsValid($_dayOfWeek))
		{
			return false;
		}
		return ($this->DayOfWeek = $_dayOfWeek);
	}
	/**
	 * Get Level value
	 * @return MytestEnumDelayLevelType|null
	 */
	public function getLevel()
	{
		return $this->Level;
	}
	/**
	 * Set Level value
	 * @uses MytestEnumDelayLevelType::valueIsValid()
	 * @param MytestEnumDelayLevelType $_level the Level
	 * @return MytestEnumDelayLevelType
	 */
	public function setLevel($_level)
	{
		if(!MytestEnumDelayLevelType::valueIsValid($_level))
		{
			return false;
		}
		return ($this->Level = $_level);
	}
	/**
	 * Get Point value
	 * @return MytestEnumDelayPointType|null
	 */
	public function getPoint()
	{
		return $this->Point;
	}
	/**
	 * Set Point value
	 * @uses MytestEnumDelayPointType::valueIsValid()
	 * @param MytestEnumDelayPointType $_point the Point
	 * @return MytestEnumDelayPointType
	 */
	public function setPoint($_point)
	{
		if(!MytestEnumDelayPointType::valueIsValid($_point))
		{
			return false;
		}
		return ($this->Point = $_point);
	}
	/**
	 * Get Type value
	 * @return MytestEnumCommitmentDelayType|null
	 */
	public function getType()
	{
		return $this->Type;
	}
	/**
	 * Set Type value
	 * @uses MytestEnumCommitmentDelayType::valueIsValid()
	 * @param MytestEnumCommitmentDelayType $_type the Type
	 * @return MytestEnumCommitmentDelayType
	 */
	public function setType($_type)
	{
		if(!MytestEnumCommitmentDelayType::valueIsValid($_type))
		{
			return false;
		}
		return ($this->Type = $_type);
	}
	/**
	 * Get Description value
	 * @return string|null
	 */
	public function getDescription()
	{
		return $this->Description;
	}
	/**
	 * Set Description value
	 * @param string $_description the Description
	 * @return string
	 */
	public function setDescription($_description)
	{
		return ($this->Description = $_description);
	}
	/**
	 * Method called when an object has been exported with var_export() functions
	 * It allows to return an object instantiated with the values
	 * @see MytestWsdlClass::__set_state()
	 * @uses MytestWsdlClass::__set_state()
	 * @param array $_array the exported values
	 * @return MytestStructDelayDetail
	 */
	public static function __set_state(array $_array,$_className = __CLASS__)
	{
		return parent::__set_state($_array,$_className);
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>