<?php
/**
 * File for class MytestEnumDelayLevelType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumDelayLevelType originally named DelayLevelType
 * Documentation : The attribute of the shipment that caused the delay(e.g. Country, City, LocationId, Zip, service area, special handling )
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumDelayLevelType extends MytestWsdlClass
{
	/**
	 * Constant for value 'CITY'
	 * @return string 'CITY'
	 */
	const VALUE_CITY = 'CITY';
	/**
	 * Constant for value 'COUNTRY'
	 * @return string 'COUNTRY'
	 */
	const VALUE_COUNTRY = 'COUNTRY';
	/**
	 * Constant for value 'LOCATION'
	 * @return string 'LOCATION'
	 */
	const VALUE_LOCATION = 'LOCATION';
	/**
	 * Constant for value 'POSTAL_CODE'
	 * @return string 'POSTAL_CODE'
	 */
	const VALUE_POSTAL_CODE = 'POSTAL_CODE';
	/**
	 * Constant for value 'SERVICE_AREA'
	 * @return string 'SERVICE_AREA'
	 */
	const VALUE_SERVICE_AREA = 'SERVICE_AREA';
	/**
	 * Constant for value 'SERVICE_AREA_SPECIAL_SERVICE'
	 * @return string 'SERVICE_AREA_SPECIAL_SERVICE'
	 */
	const VALUE_SERVICE_AREA_SPECIAL_SERVICE = 'SERVICE_AREA_SPECIAL_SERVICE';
	/**
	 * Constant for value 'SPECIAL_SERVICE'
	 * @return string 'SPECIAL_SERVICE'
	 */
	const VALUE_SPECIAL_SERVICE = 'SPECIAL_SERVICE';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumDelayLevelType::VALUE_CITY
	 * @uses MytestEnumDelayLevelType::VALUE_COUNTRY
	 * @uses MytestEnumDelayLevelType::VALUE_LOCATION
	 * @uses MytestEnumDelayLevelType::VALUE_POSTAL_CODE
	 * @uses MytestEnumDelayLevelType::VALUE_SERVICE_AREA
	 * @uses MytestEnumDelayLevelType::VALUE_SERVICE_AREA_SPECIAL_SERVICE
	 * @uses MytestEnumDelayLevelType::VALUE_SPECIAL_SERVICE
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumDelayLevelType::VALUE_CITY,MytestEnumDelayLevelType::VALUE_COUNTRY,MytestEnumDelayLevelType::VALUE_LOCATION,MytestEnumDelayLevelType::VALUE_POSTAL_CODE,MytestEnumDelayLevelType::VALUE_SERVICE_AREA,MytestEnumDelayLevelType::VALUE_SERVICE_AREA_SPECIAL_SERVICE,MytestEnumDelayLevelType::VALUE_SPECIAL_SERVICE));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>