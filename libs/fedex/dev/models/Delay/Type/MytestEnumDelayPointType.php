<?php
/**
 * File for class MytestEnumDelayPointType
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
/**
 * This class stands for MytestEnumDelayPointType originally named DelayPointType
 * Documentation : The point where the delay is occurring ( e.g. Origin, Destination, Broker location).
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/fc3a96514df1d40ccf591e0d9f3cf811/wsdl.xml
 * @package Mytest
 * @subpackage Enumerations
 * @author Mikaël DELSOL <contact@wsdltophp.com>
 * @date 2013-05-31
 */
class MytestEnumDelayPointType extends MytestWsdlClass
{
	/**
	 * Constant for value 'BROKER'
	 * @return string 'BROKER'
	 */
	const VALUE_BROKER = 'BROKER';
	/**
	 * Constant for value 'DESTINATION'
	 * @return string 'DESTINATION'
	 */
	const VALUE_DESTINATION = 'DESTINATION';
	/**
	 * Constant for value 'ORIGIN'
	 * @return string 'ORIGIN'
	 */
	const VALUE_ORIGIN = 'ORIGIN';
	/**
	 * Constant for value 'ORIGIN_DESTINATION_PAIR'
	 * @return string 'ORIGIN_DESTINATION_PAIR'
	 */
	const VALUE_ORIGIN_DESTINATION_PAIR = 'ORIGIN_DESTINATION_PAIR';
	/**
	 * Constant for value 'PROOF_OF_DELIVERY_POINT'
	 * @return string 'PROOF_OF_DELIVERY_POINT'
	 */
	const VALUE_PROOF_OF_DELIVERY_POINT = 'PROOF_OF_DELIVERY_POINT';
	/**
	 * Return true if value is allowed
	 * @uses MytestEnumDelayPointType::VALUE_BROKER
	 * @uses MytestEnumDelayPointType::VALUE_DESTINATION
	 * @uses MytestEnumDelayPointType::VALUE_ORIGIN
	 * @uses MytestEnumDelayPointType::VALUE_ORIGIN_DESTINATION_PAIR
	 * @uses MytestEnumDelayPointType::VALUE_PROOF_OF_DELIVERY_POINT
	 * @param mixed $_value value
	 * @return bool true|false
	 */
	public static function valueIsValid($_value)
	{
		return in_array($_value,array(MytestEnumDelayPointType::VALUE_BROKER,MytestEnumDelayPointType::VALUE_DESTINATION,MytestEnumDelayPointType::VALUE_ORIGIN,MytestEnumDelayPointType::VALUE_ORIGIN_DESTINATION_PAIR,MytestEnumDelayPointType::VALUE_PROOF_OF_DELIVERY_POINT));
	}
	/**
	 * Method returning the class name
	 * @return string __CLASS__
	 */
	public function __toString()
	{
		return __CLASS__;
	}
}
?>